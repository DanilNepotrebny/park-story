﻿Shader "Custom/NightTint"
{
  Properties
  {
    _Color("Color", Color) = (1,1,1,1)
    [MaterialToggle] PixelSnap("Pixel snap", Float) = 0
  }

    SubShader
  {
    Tags
    {
      "Queue" = "Transparent"
      "IgnoreProjector" = "True"
      "RenderType" = "Transparent"
      "PreviewType" = "Plane"
      "CanUseSpriteAtlas" = "True"
    }

    Fog { Mode Off }
    Cull Off
    ZWrite Off
    Blend SrcAlpha OneMinusSrcAlpha
    Lighting Off

    Pass
    {
      CGPROGRAM
        #define LIGHT_SOURCES_COUNT_MAX 5
        #pragma vertex vert
        #pragma fragment frag
        #pragma multi_compile DUMMY PIXELSNAP_ON
        #include "UnityCG.cginc"

        struct appdata_t
        {
          float4 vertex : POSITION;
          float4 color : COLOR;
        };

        struct v2f
        {
          float4 vertex : SV_POSITION;
          float3 worldPos : TEXCOORD0;
          fixed4 color : COLOR;
        };

        fixed4 _Color;

        float4 _LightSourcePositions[LIGHT_SOURCES_COUNT_MAX];
        float _LightSourceRadiuses[LIGHT_SOURCES_COUNT_MAX];
        float _LightSourceFadingLengths[LIGHT_SOURCES_COUNT_MAX];
        int _LightSourcesCount;
        float _NightFactor;

        v2f vert(appdata_t IN)
        {
          v2f OUT;
          OUT.vertex = UnityObjectToClipPos(IN.vertex);
          OUT.worldPos = mul(unity_ObjectToWorld, IN.vertex);
          OUT.color = IN.color;

          #ifdef PIXELSNAP_ON
          OUT.vertex = UnityPixelSnap(OUT.vertex);
          #endif

          return OUT;
        }

        fixed4 frag(v2f IN) : COLOR
        {
          fixed4 c = IN.color * _Color;
        
          float lightFactor = 1.0;
          for (int i = 0; i < _LightSourcesCount; i++)
          {
            float4 position = _LightSourcePositions[i];
            float outerRadius = _LightSourceRadiuses[i];
            float innerRadius = outerRadius - _LightSourceFadingLengths[i];

            float lightDistance = distance(IN.worldPos.xy, position.xy);

            lightFactor *= clamp((lightDistance - innerRadius) / (outerRadius - innerRadius), 0, 1);
          }
          
          c.a *= _NightFactor * lightFactor;
          return c;
        }
      ENDCG
    }
  }
}