// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

#ifndef SPINE_INCLUDED
#define SPINE_INCLUDED

#include "UnityCG.cginc"

struct appdata_t
{
	float4 vertex   : POSITION;
	float4 color    : COLOR;
	float2 texcoord : TEXCOORD0;
};

struct v2f
{
	float4 vertex   : SV_POSITION;
	fixed4 color : COLOR;
	half2 texcoord  : TEXCOORD0;
};

fixed4 _Color;

sampler2D _MainTex;

v2f SpineVert(appdata_t IN)
{
	v2f OUT;
	OUT.vertex = UnityObjectToClipPos(IN.vertex);
	OUT.texcoord = IN.texcoord;
	OUT.color = IN.color;

	#ifdef PIXELSNAP_ON
	OUT.vertex = UnityPixelSnap(OUT.vertex);
	#endif

	return OUT;
}

fixed4 SpineFragPremultiply(v2f IN) : COLOR
{
	return tex2D(_MainTex, IN.texcoord) * IN.color * float4(_Color.rgb * _Color.a, _Color.a);
}

fixed4 SpineFrag(v2f IN) : COLOR
{
	return tex2D(_MainTex, IN.texcoord) * IN.color * _Color;
}

#endif