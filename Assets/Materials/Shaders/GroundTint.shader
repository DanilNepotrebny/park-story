﻿Shader "Custom/GroundTint"
{
	Properties
	{
		_FadeSize("Fade size", Range(0.0, 1.0)) = .005
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Fog { Mode Off }
		Cull Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off

		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile DUMMY PIXELSNAP_ON
			#include "UnityCG.cginc"

			struct appdata_t
			{
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 texcoord  : TEXCOORD0;
				fixed4 color : COLOR;
			};

			float _FadeSize;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.color = IN.color;
				OUT.texcoord = IN.texcoord;

				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap(OUT.vertex); 
				#endif

				return OUT;
			}

			fixed4 frag(v2f IN) : COLOR
			{
				fixed4 color = IN.color;

				float factor = 1;
				if (IN.texcoord.x < _FadeSize)
				{
					factor *= lerp(0, 1, IN.texcoord.x / _FadeSize);
				}

				if ((1 - IN.texcoord.x) < _FadeSize)
				{
					factor *= lerp(0, 1, (1 - IN.texcoord.x) / _FadeSize);
				}

				if (IN.texcoord.y < _FadeSize)
				{
					factor *= lerp(0, 1, IN.texcoord.y / _FadeSize);
				}

				if ((1 - IN.texcoord.y) < _FadeSize)
				{
					factor *= lerp(0, 1, (1 - IN.texcoord.y) / _FadeSize);
				}

				color.a = lerp(0, color.a, factor);

				return color;
			}

			ENDCG
		}
	}
}