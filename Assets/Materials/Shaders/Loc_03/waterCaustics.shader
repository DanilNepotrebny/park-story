Shader "Custom/waterCaustics" {
    Properties {
        _Caustic ("Caustic", 2D) = "white" {}
        _Noise ("Noise", 2D) = "white" {}
        _Opacity ("Opacity", Range(0, 1)) = 0.4013406
		_WiggleTime ("Wiggle Speed", Range(0, 20)) = 10
        [PerRendererData]_MainTex ("MainTex", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
			"PreviewType" = "Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="Vertex"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
			#pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows

            uniform float4 _TimeEditor;
            uniform sampler2D _Caustic; uniform float4 _Caustic_ST;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform half _Opacity;
			uniform half _WiggleTime;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
				#ifdef PIXELSNAP_ON
					o.pos = UnityPixelSnap(o.pos);
				#endif
				return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                fixed4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = _MainTex_var.rgb;
                fixed4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
                half4 _time = _Time + _TimeEditor;
                fixed _timeMult = (_WiggleTime*_time.r);
                fixed2 _UVMult = ((2.0*i.uv0)*0.5);
                fixed2 _UVblend = ((((_Noise_var.b*(cos(_timeMult)/20.0))*-0.3+0.0)+_UVMult)+(((_Noise_var.g*(sin(_timeMult)/35.0))*-0.3+0.0)+_UVMult));
                fixed4 _caustic2 = tex2D(_Caustic,TRANSFORM_TEX(_UVblend, _Caustic));
                float tColor = (((_caustic2.r-(1.0 - 0.9))*_Opacity)*i.vertexColor.a);
                float3 finalColor = emissive + float3(tColor, tColor, tColor);
                return fixed4(finalColor,_MainTex_var.a);
            }
            ENDCG
        }
    }
}
