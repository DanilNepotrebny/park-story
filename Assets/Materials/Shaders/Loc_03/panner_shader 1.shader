// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.30 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.30;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:False,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:False,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1873,x:33258,y:32721,varname:node_1873,prsc:2|emission-1749-OUT,alpha-603-OUT;n:type:ShaderForge.SFN_Tex2d,id:4805,x:32553,y:32754,varname:_MainTex_copy,prsc:2,tex:6de13b2d7917cbf46b0cdb89fd991792,ntxv:0,isnm:False|UVIN-5475-OUT,TEX-7-TEX;n:type:ShaderForge.SFN_Multiply,id:1086,x:32818,y:32819,cmnt:RGB,varname:node_1086,prsc:2|A-4805-RGB,B-5983-RGB,C-5376-RGB;n:type:ShaderForge.SFN_Color,id:5983,x:32554,y:32917,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:5376,x:32554,y:33060,varname:node_5376,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1749,x:33057,y:32819,cmnt:Premultiply Alpha,varname:node_1749,prsc:2|A-1086-OUT,B-603-OUT;n:type:ShaderForge.SFN_Multiply,id:603,x:32790,y:33004,cmnt:A,varname:node_603,prsc:2|A-4805-A,B-5983-A,C-5376-A;n:type:ShaderForge.SFN_Rotator,id:2217,x:31811,y:32667,varname:node_2217,prsc:2|UVIN-5346-UVOUT,PIV-7556-OUT,ANG-3466-OUT,SPD-4612-OUT;n:type:ShaderForge.SFN_TexCoord,id:5346,x:31037,y:32669,varname:node_5346,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2dAsset,id:7,x:32309,y:32916,ptovrint:False,ptlb:Light_pattern_texture,ptin:_Light_pattern_texture,varname:_Light_pattern_texture,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:6de13b2d7917cbf46b0cdb89fd991792,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Vector2,id:7556,x:31517,y:32714,varname:node_7556,prsc:2,v1:0.5,v2:0.5;n:type:ShaderForge.SFN_ValueProperty,id:3466,x:31516,y:32932,ptovrint:False,ptlb:RotAngle,ptin:_RotAngle,varname:_RotAngle,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:4612,x:31517,y:32854,ptovrint:False,ptlb:RotSPD,ptin:_RotSPD,varname:_RotSPD,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:5475,x:32128,y:32672,cmnt:add panner effect,varname:node_5475,prsc:2|A-2217-UVOUT,B-3535-OUT;n:type:ShaderForge.SFN_Multiply,id:3535,x:31956,y:32832,cmnt:creates values for panner effect offset ,varname:node_3535,prsc:2|A-431-TSL,B-9199-OUT;n:type:ShaderForge.SFN_Append,id:9199,x:31849,y:33147,cmnt:combine 2 saparate node in one,varname:node_9199,prsc:2|A-1072-OUT,B-108-OUT;n:type:ShaderForge.SFN_Time,id:431,x:31517,y:32994,varname:node_431,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:1072,x:31517,y:33152,ptovrint:False,ptlb:Move X,ptin:_MoveX,varname:_MoveX,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:108,x:31517,y:33238,ptovrint:False,ptlb:Move Y,ptin:_MoveY,varname:_MoveY,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;proporder:5983-7-3466-4612-1072-108;pass:END;sub:END;*/

Shader "FX/Panner_shader_ADD1" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("2d sprite", 2D) = "white" {}
        _RotAngle ("RotAngle", Float ) = 0
        _RotSPD ("RotSPD", Float ) = 0
        _MoveX ("Move X", Float ) = 0
        _MoveY ("Move Y", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
           // #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _RotAngle;
            uniform float _RotSPD;
            uniform float _MoveX;
            uniform float _MoveY;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float node_2217_ang = _RotAngle;
                float node_2217_spd = _RotSPD;
                float node_2217_cos = cos(node_2217_spd*node_2217_ang);
                float node_2217_sin = sin(node_2217_spd*node_2217_ang);
                float2 node_2217_piv = float2(0.5,0.5);
                float2 node_2217 = (mul(i.uv0-node_2217_piv,float2x2( node_2217_cos, -node_2217_sin, node_2217_sin, node_2217_cos))+node_2217_piv);
                float4 node_431 = _Time + _TimeEditor;
                float2 node_5475 = (node_2217+(node_431.r*float2(_MoveX,_MoveY))); // add panner effect
                float4 _MainTex_copy = tex2D(_MainTex,TRANSFORM_TEX(node_5475, _MainTex));
                float node_603 = (_MainTex_copy.a*_Color.a*i.vertexColor.a); // A
                float3 emissive = ((_MainTex_copy.rgb*_Color.rgb*i.vertexColor.rgb)*node_603);
                float3 finalColor = emissive;
                return fixed4(finalColor,node_603);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
