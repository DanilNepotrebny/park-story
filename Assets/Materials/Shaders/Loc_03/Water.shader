﻿Shader "Eforb/Water"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Waves("Waves", 2D) = "white" {}
		_Noise("Noise", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)
		_WaveSpeed("WaveSpeed", Float) = 1
		_Saturation("Saturation", Float) = 0.3
		_MaskSaturation("MaskSaturation", Float) = 0
		_Multiplier("Multiplier", Float) = 1
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
		[MaterialToggle] _UseTexture("UseTexture", Float) = 1
	}

		SubShader
		{
		Tags
		{
		"Queue" = "Transparent"
		"IgnoreProjector" = "True"
		"RenderType" = "Transparent"
		"PreviewType" = "Plane"
		"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma multi_compile _ PIXELSNAP_ON
		#include "UnityCG.cginc"

		struct appdata_t
		{
			float4 vertex   : POSITION;
			float4 color    : COLOR;
			float2 texcoord : TEXCOORD0;
		};

	struct v2f
	{
		float4 vertex   : SV_POSITION;
		fixed4 color : COLOR;
		float2 texcoord  : TEXCOORD0;
		float4 posWorld : TEXCOORD1;
	};

	float _AlphaSplitEnabled;
	float _WaveSpeed;
	float _Saturation;
	float _Multiplier;
	float _MaskSaturation;
	float _UseTexture;
	fixed4 _Color;
	uniform sampler2D _MainTex;
	uniform float4 _MainTex_ST;

	uniform sampler2D _Waves;
	uniform float4 _Waves_ST;

	uniform sampler2D _Noise;
	uniform float4 _Noise_ST;

	v2f vert(appdata_t IN)
	{
		v2f OUT;
		OUT.vertex = UnityObjectToClipPos(IN.vertex);
		OUT.posWorld = mul(unity_ObjectToWorld, IN.vertex);
		OUT.texcoord = TRANSFORM_TEX(IN.texcoord, _MainTex);
		OUT.color = IN.color;

#ifdef PIXELSNAP_ON
		OUT.vertex = UnityPixelSnap(OUT.vertex);
#endif

		return OUT;
	}

	fixed4 SampleSpriteTexture(float2 uv)
	{
		fixed4 color = tex2D(_MainTex, uv);

#if UNITY_TEXTURE_ALPHASPLIT_ALLOWED
		if (_AlphaSplitEnabled)
			color.a = tex2D(_AlphaTex, uv).r;
#endif //UNITY_TEXTURE_ALPHASPLIT_ALLOWED

		if(_UseTexture == 0)
			color.rgb = _Color.rgb;

		return color;
	}

	fixed4 frag(v2f IN) : SV_Target
	{
		half angle = 0.05;
		float2 wavePivot = float2(0.5, 0.5);
		float2 append = IN.posWorld.xy;

		fixed4 mainColor = SampleSpriteTexture(IN.texcoord) * IN.color;

		float wavecos = cos(angle);
		float wavesin = sin(angle);	

		fixed4 noise = tex2D(_Noise, TRANSFORM_TEX(IN.texcoord, _Noise) + float2(_Time.x, _Time.x));
		fixed4 noise2 = tex2D(_Noise, TRANSFORM_TEX(IN.texcoord, _Noise) + float2(_Time.x, _Time.x) * -0.5);
//return noise;
		float2 wave1uv = mul((append + _Time.y * _WaveSpeed * float2(-0.05, 0)) - wavePivot, float2x2(wavecos, -wavesin, wavesin, wavecos)) + wavePivot;
		fixed4 wave1Color = tex2D(_Waves, TRANSFORM_TEX(wave1uv, _Waves)) * noise.r * noise.r;
		
		float2 wave2uv = mul((append + _Time.y * _WaveSpeed *  float2(-0.1, 0.02)) - wavePivot, float2x2(wavecos, -wavesin, wavesin, wavecos)) + wavePivot;
		fixed4 wave2Color = tex2D(_Waves, TRANSFORM_TEX(wave2uv, _Waves)) * (1 - noise2.r * noise2.r);

		float waveColor = lerp(mainColor.r, saturate((wave1Color.r + wave2Color.r) - _Saturation) - _MaskSaturation, 0.5) * _Multiplier;
		mainColor.rgb += waveColor;
		mainColor.rgb *= mainColor.a;
		return mainColor;
	}
		ENDCG
	}
	}
}