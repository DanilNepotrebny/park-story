﻿Shader "Custom/ClipMask"
{
    Properties
    {
        _ReferenceStencilValue("Reference Stencil Value", int) = 1
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
            "PreviewType" = "Plane"
            "CanUseSpriteAtlas" = "True"
        }

        Fog { Mode Off }
        Cull Off
        Lighting Off
        ZWrite Off
        Blend Off
        ColorMask 0

        Pass
        {
        	  Stencil
        	  {
                Ref [_ReferenceStencilValue]
                Comp always
                Pass replace
            }

        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata_t
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                return OUT;
            }

            fixed4 frag(v2f IN) : COLOR
            {
                return fixed4(1, 1, 1, 1);
            }
        ENDCG
        }
    }
}