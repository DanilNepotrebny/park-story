// Copyright � Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

Shader "DreamTeam/Unlit (Tint)" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Tint("Tint", Color) = (1,1,1,1)
	}

	SubShader 
	{
		Tags 
		{ 
			"RenderType"="Transparent" 
		}
		
		LOD 100

		Pass 
		{
			Tags 
			{ 
				"LightMode" = "Vertex" 
			}
        
			Lighting Off
			Blend SrcAlpha OneMinusSrcAlpha
			
			SetTexture [_MainTex] 
			{
				constantColor [_Tint]
				combine texture, constant
			}
		}
	}
}
