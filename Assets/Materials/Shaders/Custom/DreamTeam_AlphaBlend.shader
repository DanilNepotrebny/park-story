// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

Shader "DreamTeam/AlphaBlend" 
{
	Properties 
	{
		_MainTex ("Base (RGBA)", 2D) = "white" {}
		_Tint("Tint", Color) = (1,1,1,1)
	}

	Category 
	{
		Tags 
		{
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane" 
		}

		Blend SrcAlpha OneMinusSrcAlpha

		Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }

		BindChannels 
		{
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}

		SubShader 
		{
			Pass 
			{
				SetTexture [_MainTex] 
				{
					constantColor[_Tint]
					combine texture * constant
				}
			}
		}
	}
}
