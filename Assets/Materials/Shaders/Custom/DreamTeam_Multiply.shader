// Copyright � Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

Shader "DreamTeam/Multiply" 
{
	Properties 
	{
		_MainTex ("Particle Texture", 2D) = "white" {}
		_Tint("Tint", Color) = (1,1,1,1)
	}

	Category 
	{
		Tags 
		{ 
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane" 
		}
    
		Blend Zero SrcColor
		Cull Off Lighting Off ZWrite Off Fog { Color (1,1,1,1) }

		BindChannels 
		{
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}

		SubShader 
		{
			Pass
			{
				SetTexture [_MainTex] 
				{
					constantColor [_Tint]
					combine texture * primary, texture * constant
				}

				SetTexture [_MainTex] 
                {
                    constantColor (1,1,1,1)
                    combine previous lerp (previous) constant
                }
			}
		}
	}
}
