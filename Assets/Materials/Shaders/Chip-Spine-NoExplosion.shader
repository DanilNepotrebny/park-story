Shader "Custom/Chip-Spine-NoExplosion"
{
	Properties
	{
		_MainTex("Main Texture", 2D) = "white" {}
		[PerRendererData] _Color("Color", Color) = (1,1,1,1)
		_FlashColor("Flash Color", Color) = (1,1,1,1)
		_FlashAmount("Flash Amount",Range(0.0,1.0)) = 0.0
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Fog{ Mode Off }
		Cull Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off

		Pass
		{
			Stencil
			{
				Ref 1
				Comp Greater
				Pass Keep
			}

		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile DUMMY PIXELSNAP_ON
			#include "UnityCG.cginc"
			#include "Spine.cginc"
			#include "Flashing.cginc"

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);

				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap(OUT.vertex);
				#endif

				return OUT;
			}

			fixed4 frag(v2f IN) : COLOR
			{
				return FlashColor(SpineFrag(IN));
			}
		ENDCG
		}
	}
}