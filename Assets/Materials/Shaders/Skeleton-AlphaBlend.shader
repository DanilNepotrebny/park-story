Shader "Custom/Skeleton-AlphaBlend"
{
	Properties
	{
		_MainTex("Main Texture", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Fog{ Mode Off }
		Cull Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off

		Pass
		{
		CGPROGRAM
			#pragma vertex SpineVert
			#pragma fragment SpineFrag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "Spine.cginc"
		ENDCG
		}
	}
}