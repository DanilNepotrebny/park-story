﻿#ifndef EXPLOSION_INCLUDED
#define EXPLOSION_INCLUDED
	
#include "UnityCG.cginc"

uniform float4x4 _ExplosionEffectTransformation;

float4 ExplosionDisplace(float4 vertexPosition)
{
    float4 worldPos = mul(unity_ObjectToWorld, vertexPosition);
    worldPos = mul(_ExplosionEffectTransformation, worldPos);
    return mul(UNITY_MATRIX_VP, worldPos);
}

#endif // EXPLOSION_INCLUDED