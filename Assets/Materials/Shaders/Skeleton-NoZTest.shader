Shader "Custom/Skeleton-NoZTest"
{
	Properties
	{
		_MainTex("Main Texture", 2D) = "white" {}
		[PerRendererData] _Color("Color", Color) = (1,1,1,1)
		_FlashColor("Flash Color", Color) = (1,1,1,1)
		_Color("Color", Color) = (1,1,1,1)
		_FlashAmount("Flash Amount",Range(0.0,1.0)) = 0.0
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Fog{ Mode Off }
		Cull Off
		ZWrite Off
		ZTest Always
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off

		Pass
		{
		CGPROGRAM
			#pragma vertex SpineVert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "Spine.cginc"
			#include "Flashing.cginc"
			
			fixed4 frag(v2f IN) : COLOR
			{
				return FlashColor(SpineFrag(IN));
			}
		ENDCG
		}
	}
}