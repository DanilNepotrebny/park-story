﻿#ifndef FLASH_INCLUDED
#define FLASH_INCLUDED
			
fixed4 _FlashColor;
float _FlashAmount;

static const float EPSILON = 2.4414e-4;

fixed4 FlashColor(fixed4 color)
{
    color.rgb = lerp(color.rgb, _FlashColor.rgb, _FlashAmount);
    return color;
}

fixed4 FlashColorPremultiply(fixed4 color)
{
    float alpha = max(color.a, EPSILON);
    color.rgb = lerp(color.rgb / alpha, _FlashColor.rgb, _FlashAmount) * alpha;
    return color;
}

#endif // FLASH_INCLUDED