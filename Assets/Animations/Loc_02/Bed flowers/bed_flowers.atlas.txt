
bed_flowers.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 838, 938
  size: 89, 37
  orig: 89, 37
  offset: 0, 0
  index: -1
10
  rotate: true
  xy: 719, 705
  size: 87, 29
  orig: 87, 29
  offset: 0, 0
  index: -1
11
  rotate: false
  xy: 719, 796
  size: 76, 68
  orig: 76, 68
  offset: 0, 0
  index: -1
12
  rotate: true
  xy: 637, 545
  size: 35, 55
  orig: 35, 55
  offset: 0, 0
  index: -1
13
  rotate: false
  xy: 520, 327
  size: 36, 52
  orig: 36, 52
  offset: 0, 0
  index: -1
14
  rotate: false
  xy: 520, 455
  size: 79, 69
  orig: 79, 69
  offset: 0, 0
  index: -1
15
  rotate: false
  xy: 719, 868
  size: 112, 52
  orig: 112, 52
  offset: 0, 0
  index: -1
16
  rotate: true
  xy: 4, 36
  size: 83, 30
  orig: 83, 30
  offset: 0, 0
  index: -1
2
  rotate: false
  xy: 520, 528
  size: 113, 52
  orig: 113, 52
  offset: 0, 0
  index: -1
3
  rotate: false
  xy: 121, 277
  size: 85, 38
  orig: 85, 38
  offset: 0, 0
  index: -1
4
  rotate: false
  xy: 719, 924
  size: 115, 51
  orig: 115, 51
  offset: 0, 0
  index: -1
5
  rotate: false
  xy: 520, 383
  size: 74, 68
  orig: 74, 68
  offset: 0, 0
  index: -1
6
  rotate: false
  xy: 799, 811
  size: 31, 53
  orig: 31, 53
  offset: 0, 0
  index: -1
7
  rotate: true
  xy: 931, 942
  size: 33, 48
  orig: 33, 48
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 4, 123
  size: 81, 62
  orig: 81, 62
  offset: 0, 0
  index: -1
9
  rotate: false
  xy: 4, 262
  size: 113, 53
  orig: 113, 53
  offset: 0, 0
  index: -1
Stand_1_shadow
  rotate: false
  xy: 4, 189
  size: 91, 69
  orig: 91, 69
  offset: 0, 0
  index: -1
platform-bedflowers
  rotate: false
  xy: 4, 584
  size: 711, 391
  orig: 711, 391
  offset: 0, 0
  index: -1
stand
  rotate: false
  xy: 4, 319
  size: 512, 261
  orig: 512, 261
  offset: 0, 0
  index: -1

bed_flowers2.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
skin_1/gazon1_Flowers
  rotate: false
  xy: 4, 361
  size: 211, 121
  orig: 211, 121
  offset: 0, 0
  index: -1
skin_1/gazon1_Grass
  rotate: false
  xy: 4, 759
  size: 521, 253
  orig: 521, 253
  offset: 0, 0
  index: -1
skin_1/gazon1_Grass1
  rotate: false
  xy: 529, 775
  size: 421, 237
  orig: 421, 237
  offset: 0, 0
  index: -1
skin_1/gazon1_Grass_stand
  rotate: false
  xy: 4, 486
  size: 424, 269
  orig: 424, 269
  offset: 0, 0
  index: -1
skin_1/gazon1_Grass_stand sh
  rotate: true
  xy: 954, 903
  size: 109, 62
  orig: 109, 62
  offset: 0, 0
  index: -1
skin_1/gazon1_Stand_2
  rotate: true
  xy: 432, 601
  size: 154, 90
  orig: 154, 90
  offset: 0, 0
  index: -1
skin_1/gazon1_Stand_2_shadow
  rotate: false
  xy: 4, 257
  size: 141, 100
  orig: 141, 100
  offset: 0, 0
  index: -1

bed_flowers3.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
skin_2/Layer 1
  rotate: false
  xy: 774, 851
  size: 37, 31
  orig: 37, 31
  offset: 0, 0
  index: -1
skin_2/Layer 10
  rotate: false
  xy: 322, 479
  size: 33, 24
  orig: 33, 24
  offset: 0, 0
  index: -1
skin_2/Layer 11
  rotate: false
  xy: 4, 187
  size: 20, 19
  orig: 20, 19
  offset: 0, 0
  index: -1
skin_2/Layer 12
  rotate: true
  xy: 129, 295
  size: 23, 16
  orig: 23, 16
  offset: 0, 0
  index: -1
skin_2/Layer 13
  rotate: false
  xy: 774, 824
  size: 25, 23
  orig: 25, 23
  offset: 0, 0
  index: -1
skin_2/Layer 14
  rotate: false
  xy: 286, 403
  size: 19, 19
  orig: 19, 19
  offset: 0, 0
  index: -1
skin_2/Layer 15
  rotate: false
  xy: 88, 291
  size: 37, 27
  orig: 37, 27
  offset: 0, 0
  index: -1
skin_2/Layer 16
  rotate: true
  xy: 359, 479
  size: 24, 19
  orig: 24, 19
  offset: 0, 0
  index: -1
skin_2/Layer 17
  rotate: false
  xy: 815, 859
  size: 25, 23
  orig: 25, 23
  offset: 0, 0
  index: -1
skin_2/Layer 18
  rotate: false
  xy: 322, 433
  size: 20, 18
  orig: 20, 18
  offset: 0, 0
  index: -1
skin_2/Layer 19
  rotate: false
  xy: 699, 773
  size: 18, 15
  orig: 18, 15
  offset: 0, 0
  index: -1
skin_2/Layer 2
  rotate: false
  xy: 844, 865
  size: 22, 17
  orig: 22, 17
  offset: 0, 0
  index: -1
skin_2/Layer 20
  rotate: false
  xy: 870, 866
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
skin_2/Layer 21
  rotate: false
  xy: 4, 236
  size: 39, 26
  orig: 39, 26
  offset: 0, 0
  index: -1
skin_2/Layer 22
  rotate: false
  xy: 4, 170
  size: 15, 13
  orig: 15, 13
  offset: 0, 0
  index: -1
skin_2/Layer 23
  rotate: false
  xy: 222, 367
  size: 22, 23
  orig: 22, 23
  offset: 0, 0
  index: -1
skin_2/Layer 24
  rotate: false
  xy: 671, 772
  size: 24, 16
  orig: 24, 16
  offset: 0, 0
  index: -1
skin_2/Layer 25
  rotate: false
  xy: 258, 401
  size: 24, 21
  orig: 24, 21
  offset: 0, 0
  index: -1
skin_2/Layer 26
  rotate: true
  xy: 466, 558
  size: 24, 19
  orig: 24, 19
  offset: 0, 0
  index: -1
skin_2/Layer 27
  rotate: false
  xy: 635, 714
  size: 19, 15
  orig: 19, 15
  offset: 0, 0
  index: -1
skin_2/Layer 28
  rotate: false
  xy: 434, 554
  size: 28, 28
  orig: 28, 28
  offset: 0, 0
  index: -1
skin_2/Layer 29
  rotate: false
  xy: 349, 458
  size: 21, 17
  orig: 21, 17
  offset: 0, 0
  index: -1
skin_2/Layer 3
  rotate: false
  xy: 190, 339
  size: 25, 20
  orig: 25, 20
  offset: 0, 0
  index: -1
skin_2/Layer 30
  rotate: false
  xy: 384, 522
  size: 35, 22
  orig: 35, 22
  offset: 0, 0
  index: -1
skin_2/Layer 31
  rotate: false
  xy: 586, 686
  size: 26, 24
  orig: 26, 24
  offset: 0, 0
  index: -1
skin_2/Layer 32
  rotate: false
  xy: 616, 695
  size: 19, 15
  orig: 19, 15
  offset: 0, 0
  index: -1
skin_2/Layer 33
  rotate: false
  xy: 47, 240
  size: 24, 22
  orig: 24, 22
  offset: 0, 0
  index: -1
skin_2/Layer 34
  rotate: false
  xy: 149, 334
  size: 37, 25
  orig: 37, 25
  offset: 0, 0
  index: -1
skin_2/Layer 35
  rotate: false
  xy: 671, 750
  size: 19, 18
  orig: 19, 18
  offset: 0, 0
  index: -1
skin_2/Layer 36
  rotate: true
  xy: 560, 643
  size: 25, 18
  orig: 25, 18
  offset: 0, 0
  index: -1
skin_2/Layer 37
  rotate: false
  xy: 88, 270
  size: 29, 17
  orig: 29, 17
  offset: 0, 0
  index: -1
skin_2/Layer 38
  rotate: false
  xy: 716, 792
  size: 21, 16
  orig: 21, 16
  offset: 0, 0
  index: -1
skin_2/Layer 39
  rotate: false
  xy: 222, 394
  size: 32, 28
  orig: 32, 28
  offset: 0, 0
  index: -1
skin_2/Layer 4
  rotate: false
  xy: 322, 455
  size: 23, 20
  orig: 23, 20
  offset: 0, 0
  index: -1
skin_2/Layer 5
  rotate: false
  xy: 687, 792
  size: 25, 16
  orig: 25, 16
  offset: 0, 0
  index: -1
skin_2/Layer 6
  rotate: false
  xy: 149, 313
  size: 21, 17
  orig: 21, 17
  offset: 0, 0
  index: -1
skin_2/Layer 7
  rotate: false
  xy: 491, 601
  size: 30, 24
  orig: 30, 24
  offset: 0, 0
  index: -1
skin_2/Layer 8
  rotate: false
  xy: 4, 210
  size: 24, 22
  orig: 24, 22
  offset: 0, 0
  index: -1
skin_2/Layer 9
  rotate: false
  xy: 529, 642
  size: 27, 26
  orig: 27, 26
  offset: 0, 0
  index: -1
skin_2/gazon2_Flowers_BLUE
  rotate: false
  xy: 149, 363
  size: 69, 59
  orig: 69, 59
  offset: 0, 0
  index: -1
skin_2/gazon2_Flowers_BLUE_sh
  rotate: false
  xy: 322, 507
  size: 58, 37
  orig: 58, 37
  offset: 0, 0
  index: -1
skin_2/gazon2_Grass
  rotate: false
  xy: 4, 629
  size: 521, 253
  orig: 521, 253
  offset: 0, 0
  index: -1
skin_2/gazon2_Stand_2
  rotate: false
  xy: 529, 792
  size: 154, 90
  orig: 154, 90
  offset: 0, 0
  index: -1
skin_2/gazon2_Stand_2_shadow
  rotate: false
  xy: 4, 322
  size: 141, 100
  orig: 141, 100
  offset: 0, 0
  index: -1
skin_2/gazon2_klumbi_1
  rotate: false
  xy: 4, 426
  size: 314, 199
  orig: 314, 199
  offset: 0, 0
  index: -1
skin_2/gazon2_klumbi_1_sh
  rotate: false
  xy: 4, 266
  size: 80, 52
  orig: 80, 52
  offset: 0, 0
  index: -1
skin_2/gazon2_klumbi_2
  rotate: false
  xy: 529, 714
  size: 102, 74
  orig: 102, 74
  offset: 0, 0
  index: -1
skin_2/gazon2_klumbi_2_sh
  rotate: false
  xy: 687, 812
  size: 83, 70
  orig: 83, 70
  offset: 0, 0
  index: -1
skin_2/gazon2_sh1
  rotate: false
  xy: 434, 586
  size: 53, 39
  orig: 53, 39
  offset: 0, 0
  index: -1
skin_2/gazon2_sh2
  rotate: false
  xy: 529, 672
  size: 53, 38
  orig: 53, 38
  offset: 0, 0
  index: -1
skin_2/gazon2_sh3
  rotate: true
  xy: 635, 733
  size: 55, 32
  orig: 55, 32
  offset: 0, 0
  index: -1
skin_2/gazon2_shad
  rotate: false
  xy: 322, 548
  size: 108, 77
  orig: 108, 77
  offset: 0, 0
  index: -1

bed_flowers4.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
skin_3/gazon3_Flowers_pink
  rotate: false
  xy: 656, 909
  size: 65, 41
  orig: 65, 41
  offset: 0, 0
  index: -1
skin_3/gazon3_Flowers_pink_sh
  rotate: false
  xy: 162, 490
  size: 65, 30
  orig: 65, 30
  offset: 0, 0
  index: -1
skin_3/gazon3_Flowers_red1
  rotate: false
  xy: 529, 895
  size: 123, 121
  orig: 123, 121
  offset: 0, 0
  index: -1
skin_3/gazon3_Grass 1
  rotate: false
  xy: 4, 763
  size: 521, 253
  orig: 521, 253
  offset: 0, 0
  index: -1
skin_3/gazon3_Grass 2
  rotate: false
  xy: 4, 583
  size: 300, 176
  orig: 300, 176
  offset: 0, 0
  index: -1
skin_3/gazon3_Grass_stand
  rotate: false
  xy: 656, 954
  size: 169, 29
  orig: 169, 29
  offset: 0, 0
  index: -1
skin_3/gazon3_Grass_stand1
  rotate: false
  xy: 854, 990
  size: 126, 26
  orig: 126, 26
  offset: 0, 0
  index: -1
skin_3/gazon3_Layer 1
  rotate: false
  xy: 656, 987
  size: 194, 29
  orig: 194, 29
  offset: 0, 0
  index: -1
skin_3/gazon3_Layer 10
  rotate: false
  xy: 592, 847
  size: 59, 44
  orig: 59, 44
  offset: 0, 0
  index: -1
skin_3/gazon3_Layer 11
  rotate: false
  xy: 4, 303
  size: 65, 48
  orig: 65, 48
  offset: 0, 0
  index: -1
skin_3/gazon3_Layer 12
  rotate: false
  xy: 78, 437
  size: 65, 48
  orig: 65, 48
  offset: 0, 0
  index: -1
skin_3/gazon3_Layer 2
  rotate: false
  xy: 4, 355
  size: 70, 130
  orig: 70, 130
  offset: 0, 0
  index: -1
skin_3/gazon3_Layer 3
  rotate: true
  xy: 162, 524
  size: 55, 110
  orig: 55, 110
  offset: 0, 0
  index: -1
skin_3/gazon3_Layer 4
  rotate: false
  xy: 308, 630
  size: 132, 25
  orig: 132, 25
  offset: 0, 0
  index: -1
skin_3/gazon3_Layer 5
  rotate: false
  xy: 529, 780
  size: 59, 111
  orig: 59, 111
  offset: 0, 0
  index: -1
skin_3/gazon3_Layer 6
  rotate: false
  xy: 453, 665
  size: 53, 94
  orig: 53, 94
  offset: 0, 0
  index: -1
skin_3/gazon3_Layer 7
  rotate: false
  xy: 4, 265
  size: 49, 34
  orig: 49, 34
  offset: 0, 0
  index: -1
skin_3/gazon3_Layer 8
  rotate: true
  xy: 984, 959
  size: 57, 36
  orig: 57, 36
  offset: 0, 0
  index: -1
skin_3/gazon3_Layer 9
  rotate: false
  xy: 308, 592
  size: 59, 34
  orig: 59, 34
  offset: 0, 0
  index: -1
skin_3/gazon3_Stand_2
  rotate: false
  xy: 4, 489
  size: 154, 90
  orig: 154, 90
  offset: 0, 0
  index: -1
skin_3/gazon3_Stand_2_shadow
  rotate: false
  xy: 308, 659
  size: 141, 100
  orig: 141, 100
  offset: 0, 0
  index: -1
