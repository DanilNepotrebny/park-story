
FairyLights2.png
size: 256,256
format: RGBA8888
filter: Linear,Linear
repeat: none
rope
  rotate: false
  xy: 4, 4
  size: 27, 241
  orig: 27, 241
  offset: 0, 0
  index: -1
rope part
  rotate: false
  xy: 35, 147
  size: 4, 98
  orig: 4, 98
  offset: 0, 0
  index: -1

FairyLights22.png
size: 128,128
format: RGBA8888
filter: Linear,Linear
repeat: none
skin_0/old_1
  rotate: false
  xy: 4, 84
  size: 36, 36
  orig: 36, 36
  offset: 0, 0
  index: -1
skin_0/old_2
  rotate: false
  xy: 4, 44
  size: 36, 36
  orig: 36, 36
  offset: 0, 0
  index: -1
skin_0/old_3
  rotate: false
  xy: 44, 45
  size: 35, 35
  orig: 35, 35
  offset: 0, 0
  index: -1
skin_0/old_4
  rotate: false
  xy: 84, 85
  size: 35, 35
  orig: 35, 35
  offset: 0, 0
  index: -1
skin_0/old_5
  rotate: false
  xy: 44, 84
  size: 36, 36
  orig: 36, 36
  offset: 0, 0
  index: -1
skin_0/old_6
  rotate: false
  xy: 4, 4
  size: 36, 36
  orig: 36, 36
  offset: 0, 0
  index: -1
skin_0/old_7
  rotate: false
  xy: 44, 6
  size: 35, 35
  orig: 35, 35
  offset: 0, 0
  index: -1

FairyLights23.png
size: 256,256
format: RGBA8888
filter: Linear,Linear
repeat: none
skin_1/blue lamp
  rotate: false
  xy: 88, 170
  size: 79, 81
  orig: 79, 81
  offset: 0, 0
  index: -1
skin_1/bright blue lamp
  rotate: true
  xy: 171, 172
  size: 79, 81
  orig: 79, 81
  offset: 0, 0
  index: -1
skin_1/green lamp
  rotate: false
  xy: 4, 170
  size: 80, 81
  orig: 80, 81
  offset: 0, 0
  index: -1
skin_1/orange lamp
  rotate: false
  xy: 4, 4
  size: 75, 77
  orig: 75, 77
  offset: 0, 0
  index: -1
skin_1/red lamp
  rotate: false
  xy: 4, 85
  size: 80, 81
  orig: 80, 81
  offset: 0, 0
  index: -1
skin_1/yellow lamp
  rotate: false
  xy: 88, 89
  size: 75, 77
  orig: 75, 77
  offset: 0, 0
  index: -1

FairyLights24.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
skin_2/blue sphere
  rotate: false
  xy: 4, 228
  size: 106, 108
  orig: 106, 108
  offset: 0, 0
  index: -1
skin_2/green sphere
  rotate: false
  xy: 4, 116
  size: 106, 108
  orig: 106, 108
  offset: 0, 0
  index: -1
skin_2/particle
  rotate: false
  xy: 114, 190
  size: 34, 34
  orig: 34, 34
  offset: 0, 0
  index: -1
skin_2/red sphere
  rotate: false
  xy: 114, 228
  size: 106, 108
  orig: 106, 108
  offset: 0, 0
  index: -1
skin_2/yellow sphere
  rotate: false
  xy: 4, 4
  size: 106, 108
  orig: 106, 108
  offset: 0, 0
  index: -1

FairyLights25.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
skin_3/blue star
  rotate: false
  xy: 4, 264
  size: 142, 149
  orig: 142, 149
  offset: 0, 0
  index: -1
skin_3/green star
  rotate: false
  xy: 150, 271
  size: 141, 142
  orig: 141, 142
  offset: 0, 0
  index: -1
skin_3/red star
  rotate: false
  xy: 4, 116
  size: 141, 144
  orig: 141, 144
  offset: 0, 0
  index: -1
skin_3/yellow star
  rotate: false
  xy: 4, 4
  size: 106, 108
  orig: 106, 108
  offset: 0, 0
  index: -1
