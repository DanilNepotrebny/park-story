
radiopoint.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
radiopoint1/radiopoint1behind
  rotate: false
  xy: 4, 8
  size: 182, 171
  orig: 182, 171
  offset: 0, 0
  index: -1
radiopoint1/radiopoint1dinamic_L
  rotate: false
  xy: 1293, 1433
  size: 178, 137
  orig: 178, 137
  offset: 0, 0
  index: -1
radiopoint1/radiopoint1dinamic_R
  rotate: false
  xy: 1656, 1679
  size: 154, 127
  orig: 154, 127
  offset: 0, 0
  index: -1
radiopoint1/radiopoint1floor
  rotate: false
  xy: 4, 1193
  size: 586, 613
  orig: 586, 613
  offset: 0, 0
  index: -1
radiopoint1/radiopoint1front
  rotate: true
  xy: 594, 1207
  size: 180, 838
  orig: 180, 838
  offset: 0, 0
  index: -1
radiopoint1/radiopoint1left
  rotate: false
  xy: 467, 700
  size: 96, 489
  orig: 96, 489
  offset: 0, 0
  index: -1
radiopoint1/radiopoint1right
  rotate: true
  xy: 594, 1391
  size: 415, 695
  orig: 415, 695
  offset: 0, 0
  index: -1
radiopoint1/radiopoint1roof1
  rotate: true
  xy: 4, 496
  size: 693, 459
  orig: 693, 459
  offset: 0, 0
  index: -1
radiopoint1/radiopoint1roof2
  rotate: false
  xy: 4, 183
  size: 341, 309
  orig: 341, 309
  offset: 0, 0
  index: -1
radiopoint1/radiopoint1shad
  rotate: false
  xy: 467, 681
  size: 27, 15
  orig: 27, 15
  offset: 0, 0
  index: -1
radiopoint1/radiopoint1stick
  rotate: false
  xy: 349, 257
  size: 24, 235
  orig: 24, 235
  offset: 0, 0
  index: -1
radiopoint1/radiopoint1top_left
  rotate: false
  xy: 1293, 1574
  size: 359, 232
  orig: 359, 232
  offset: 0, 0
  index: -1

radiopoint2.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
radiopoint2/radiopoint2block
  rotate: false
  xy: 261, 221
  size: 57, 26
  orig: 57, 26
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2dinamic_L
  rotate: false
  xy: 1121, 1464
  size: 129, 95
  orig: 129, 95
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2dinamic_R
  rotate: false
  xy: 570, 1209
  size: 130, 96
  orig: 130, 96
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2first_floor
  rotate: false
  xy: 4, 356
  size: 513, 588
  orig: 513, 588
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2floor
  rotate: false
  xy: 4, 948
  size: 562, 611
  orig: 562, 611
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2ol
  rotate: false
  xy: 570, 1309
  size: 295, 56
  orig: 295, 56
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2pot
  rotate: false
  xy: 570, 1369
  size: 351, 190
  orig: 351, 190
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2pot2
  rotate: false
  xy: 4, 221
  size: 253, 131
  orig: 253, 131
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2pot3
  rotate: false
  xy: 925, 1444
  size: 192, 115
  orig: 192, 115
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2pot4
  rotate: false
  xy: 261, 251
  size: 175, 101
  orig: 175, 101
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2sphera
  rotate: false
  xy: 4, 4
  size: 57, 62
  orig: 57, 62
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2stick1
  rotate: false
  xy: 4, 70
  size: 141, 147
  orig: 141, 147
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2stick2
  rotate: true
  xy: 925, 1386
  size: 54, 111
  orig: 54, 111
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2stick3
  rotate: false
  xy: 521, 841
  size: 25, 103
  orig: 25, 103
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2stick4
  rotate: false
  xy: 149, 135
  size: 20, 82
  orig: 20, 82
  offset: 0, 0
  index: -1
radiopoint2/radiopoint2top
  rotate: false
  xy: 869, 1314
  size: 24, 51
  orig: 24, 51
  offset: 0, 0
  index: -1

radiopoint3.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
radiopoint3/radiopoint3dinamicL
  rotate: false
  xy: 830, 239
  size: 126, 73
  orig: 126, 73
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3dinamicR
  rotate: true
  xy: 803, 137
  size: 98, 108
  orig: 98, 108
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3floor2
  rotate: false
  xy: 4, 111
  size: 453, 506
  orig: 453, 506
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3floor3
  rotate: true
  xy: 461, 214
  size: 403, 187
  orig: 403, 187
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3floor4
  rotate: true
  xy: 847, 609
  size: 403, 169
  orig: 403, 169
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3key
  rotate: false
  xy: 847, 316
  size: 104, 289
  orig: 104, 289
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3note1
  rotate: true
  xy: 652, 155
  size: 95, 147
  orig: 95, 147
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3note2
  rotate: true
  xy: 461, 115
  size: 95, 180
  orig: 95, 180
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3note3
  rotate: true
  xy: 652, 254
  size: 119, 174
  orig: 119, 174
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3note4
  rotate: false
  xy: 955, 458
  size: 56, 147
  orig: 56, 147
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3plate
  rotate: false
  xy: 4, 621
  size: 663, 391
  orig: 663, 391
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3stick1
  rotate: true
  xy: 4, 9
  size: 21, 96
  orig: 21, 96
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3stick2
  rotate: true
  xy: 4, 85
  size: 22, 482
  orig: 22, 482
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3stick3
  rotate: true
  xy: 4, 58
  size: 23, 190
  orig: 23, 190
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3stick4
  rotate: true
  xy: 4, 34
  size: 20, 144
  orig: 20, 144
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3stick5
  rotate: true
  xy: 198, 61
  size: 20, 98
  orig: 20, 98
  offset: 0, 0
  index: -1
radiopoint3/radiopoint3tube1
  rotate: true
  xy: 671, 377
  size: 635, 172
  orig: 635, 172
  offset: 0, 0
  index: -1
