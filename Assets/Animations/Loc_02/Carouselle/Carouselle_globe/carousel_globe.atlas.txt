
carousel_globe.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
carousel_new/bottom_middle1
  rotate: false
  xy: 4, 739
  size: 1052, 557
  orig: 1052, 557
  offset: 0, 0
  index: -1
carousel_new/bottom_middle2
  rotate: false
  xy: 4, 300
  size: 815, 435
  orig: 815, 435
  offset: 0, 0
  index: -1
carousel_new/bottom_middle3
  rotate: true
  xy: 1649, 1508
  size: 330, 176
  orig: 330, 176
  offset: 0, 0
  index: -1
carousel_new/bottom_top
  rotate: true
  xy: 701, 67
  size: 229, 203
  orig: 229, 203
  offset: 0, 0
  index: -1
carousel_new/bottom_top_shadows
  rotate: false
  xy: 1143, 595
  size: 334, 159
  orig: 334, 159
  offset: 0, 0
  index: -1
carousel_new/center
  rotate: false
  xy: 1795, 1885
  size: 185, 158
  orig: 185, 158
  offset: 0, 0
  index: -1
carousel_new/fence
  rotate: false
  xy: 4, 1300
  size: 1311, 743
  orig: 1311, 743
  offset: 0, 0
  index: -1
carousel_new/globe
  rotate: false
  xy: 402, 4
  size: 295, 292
  orig: 295, 292
  offset: 0, 0
  index: -1
carousel_new/hand_01
  rotate: true
  xy: 1033, 374
  size: 361, 106
  orig: 361, 106
  offset: 0, 0
  index: -1
carousel_new/hand_02
  rotate: false
  xy: 1143, 342
  size: 346, 122
  orig: 346, 122
  offset: 0, 0
  index: -1
carousel_new/hand_03
  rotate: false
  xy: 1518, 1034
  size: 76, 166
  orig: 76, 166
  offset: 0, 0
  index: -1
carousel_new/hand_04
  rotate: false
  xy: 1143, 468
  size: 354, 123
  orig: 354, 123
  offset: 0, 0
  index: -1
carousel_new/hand_05
  rotate: true
  xy: 1829, 1550
  size: 331, 108
  orig: 331, 108
  offset: 0, 0
  index: -1
carousel_new/hand_06
  rotate: false
  xy: 1941, 1612
  size: 76, 269
  orig: 76, 269
  offset: 0, 0
  index: -1
carousel_new/light_blue
  rotate: false
  xy: 1598, 1054
  size: 137, 137
  orig: 137, 137
  offset: 0, 0
  index: -1
carousel_new/light_green
  rotate: false
  xy: 1739, 1054
  size: 137, 137
  orig: 137, 137
  offset: 0, 0
  index: -1
carousel_new/light_line
  rotate: true
  xy: 1060, 795
  size: 501, 150
  orig: 501, 150
  offset: 0, 0
  index: -1
carousel_new/light_round
  rotate: false
  xy: 1319, 1512
  size: 326, 326
  orig: 326, 326
  offset: 0, 0
  index: -1
carousel_new/light_yellow
  rotate: false
  xy: 1834, 1409
  size: 137, 137
  orig: 137, 137
  offset: 0, 0
  index: -1
carousel_new/ship_01
  rotate: true
  xy: 1640, 1195
  size: 309, 190
  orig: 309, 190
  offset: 0, 0
  index: -1
carousel_new/ship_02
  rotate: true
  xy: 823, 391
  size: 344, 206
  orig: 344, 206
  offset: 0, 0
  index: -1
carousel_new/ship_03
  rotate: false
  xy: 1319, 1842
  size: 472, 201
  orig: 472, 201
  offset: 0, 0
  index: -1
carousel_new/ship_04
  rotate: false
  xy: 1214, 991
  size: 300, 209
  orig: 300, 209
  offset: 0, 0
  index: -1
carousel_new/ship_05
  rotate: true
  xy: 1319, 1204
  size: 304, 317
  orig: 304, 317
  offset: 0, 0
  index: -1
carousel_new/ship_06
  rotate: false
  xy: 4, 51
  size: 394, 245
  orig: 394, 245
  offset: 0, 0
  index: -1
carousel_new/top
  rotate: false
  xy: 1214, 758
  size: 271, 229
  orig: 271, 229
  offset: 0, 0
  index: -1

carousel_globe2.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
carousel_pink/centerpink
  rotate: false
  xy: 1795, 1885
  size: 185, 158
  orig: 185, 158
  offset: 0, 0
  index: -1
carousel_pink/hand_01pink
  rotate: false
  xy: 1033, 377
  size: 361, 106
  orig: 361, 106
  offset: 0, 0
  index: -1
carousel_pink/hand_02pink
  rotate: false
  xy: 1033, 487
  size: 346, 122
  orig: 346, 122
  offset: 0, 0
  index: -1
carousel_pink/hand_03pink
  rotate: true
  xy: 1017, 184
  size: 77, 166
  orig: 77, 166
  offset: 0, 0
  index: -1
carousel_pink/hand_04pink
  rotate: false
  xy: 1033, 613
  size: 355, 122
  orig: 355, 122
  offset: 0, 0
  index: -1
carousel_pink/hand_05pink
  rotate: false
  xy: 1017, 265
  size: 331, 108
  orig: 331, 108
  offset: 0, 0
  index: -1
carousel_pink/hand_06pink
  rotate: true
  xy: 312, 32
  size: 76, 269
  orig: 76, 269
  offset: 0, 0
  index: -1
carousel_pink/idle_bottom_middle1
  rotate: false
  xy: 4, 764
  size: 1052, 532
  orig: 1052, 532
  offset: 0, 0
  index: -1
carousel_pink/idle_bottom_middle2
  rotate: false
  xy: 4, 325
  size: 815, 435
  orig: 815, 435
  offset: 0, 0
  index: -1
carousel_pink/idle_bottom_middle3
  rotate: true
  xy: 1618, 1508
  size: 330, 176
  orig: 330, 176
  offset: 0, 0
  index: -1
carousel_pink/idle_bottom_top
  rotate: true
  xy: 616, 92
  size: 229, 203
  orig: 229, 203
  offset: 0, 0
  index: -1
carousel_pink/idle_bottom_top_shadows
  rotate: false
  xy: 1060, 739
  size: 334, 159
  orig: 334, 159
  offset: 0, 0
  index: -1
carousel_pink/idle_fence
  rotate: false
  xy: 4, 1300
  size: 1311, 743
  orig: 1311, 743
  offset: 0, 0
  index: -1
carousel_pink/idle_globe
  rotate: false
  xy: 1319, 1546
  size: 295, 292
  orig: 295, 292
  offset: 0, 0
  index: -1
carousel_pink/idle_top
  rotate: false
  xy: 1319, 1313
  size: 271, 229
  orig: 271, 229
  offset: 0, 0
  index: -1
carousel_pink/ship_01pink
  rotate: true
  xy: 823, 103
  size: 309, 190
  orig: 309, 190
  offset: 0, 0
  index: -1
carousel_pink/ship_02pink
  rotate: true
  xy: 823, 416
  size: 344, 206
  orig: 344, 206
  offset: 0, 0
  index: -1
carousel_pink/ship_03pink
  rotate: false
  xy: 1319, 1842
  size: 472, 201
  orig: 472, 201
  offset: 0, 0
  index: -1
carousel_pink/ship_04pink
  rotate: false
  xy: 312, 112
  size: 300, 209
  orig: 300, 209
  offset: 0, 0
  index: -1
carousel_pink/ship_05pink
  rotate: false
  xy: 4, 4
  size: 304, 317
  orig: 304, 317
  offset: 0, 0
  index: -1
carousel_pink/ship_06pink
  rotate: true
  xy: 1060, 902
  size: 394, 245
  orig: 394, 245
  offset: 0, 0
  index: -1
