
John.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
L_collar
  rotate: true
  xy: 790, 17
  size: 102, 120
  orig: 102, 120
  offset: 0, 0
  index: -1
L_collar_shadow
  rotate: false
  xy: 786, 572
  size: 134, 151
  orig: 134, 151
  offset: 0, 0
  index: -1
L_ear
  rotate: true
  xy: 972, 12
  size: 15, 44
  orig: 15, 44
  offset: 0, 0
  index: -1
L_eye
  rotate: false
  xy: 997, 617
  size: 23, 23
  orig: 23, 23
  offset: 0, 0
  index: -1
L_eye_flare
  rotate: false
  xy: 216, 573
  size: 9, 6
  orig: 9, 6
  offset: 0, 0
  index: -1
L_eye_shadow
  rotate: true
  xy: 997, 644
  size: 32, 23
  orig: 32, 23
  offset: 0, 0
  index: -1
L_eye_white
  rotate: true
  xy: 503, 193
  size: 33, 28
  orig: 33, 28
  offset: 0, 0
  index: -1
L_eyebrow
  rotate: false
  xy: 216, 872
  size: 41, 47
  orig: 41, 47
  offset: 0, 0
  index: -1
L_sleeve
  rotate: false
  xy: 322, 181
  size: 177, 387
  orig: 177, 387
  offset: 0, 0
  index: -1
L_wrist
  rotate: true
  xy: 53, 923
  size: 97, 199
  orig: 97, 199
  offset: 0, 0
  index: -1
R_arm
  rotate: true
  xy: 786, 727
  size: 293, 222
  orig: 293, 222
  offset: 0, 0
  index: -1
R_eye_white
  rotate: true
  xy: 216, 583
  size: 56, 33
  orig: 56, 33
  offset: 0, 0
  index: -1
R_eyebrow
  rotate: false
  xy: 503, 516
  size: 81, 52
  orig: 81, 52
  offset: 0, 0
  index: -1
R_pants2
  rotate: false
  xy: 589, 123
  size: 421, 445
  orig: 421, 445
  offset: 0, 0
  index: -1
R_sleeve
  rotate: false
  xy: 53, 177
  size: 265, 391
  orig: 265, 391
  offset: 0, 0
  index: -1
arm_left_01
  rotate: true
  xy: 53, 577
  size: 342, 159
  orig: 342, 159
  offset: 0, 0
  index: -1
arm_right_cuff
  rotate: false
  xy: 398, 5
  size: 187, 172
  orig: 187, 172
  offset: 0, 0
  index: -1
arm_right_shadow
  rotate: false
  xy: 914, 31
  size: 98, 88
  orig: 98, 88
  offset: 0, 0
  index: -1
cable
  rotate: false
  xy: 4, 175
  size: 45, 845
  orig: 45, 845
  offset: 0, 0
  index: -1
chest
  rotate: true
  xy: 261, 572
  size: 448, 521
  orig: 448, 521
  offset: 0, 0
  index: -1
collar_back
  rotate: true
  xy: 261, 4
  size: 169, 133
  orig: 169, 133
  offset: 0, 0
  index: -1
finger1_left
  rotate: false
  xy: 924, 680
  size: 94, 43
  orig: 94, 43
  offset: 0, 0
  index: -1
finger1_right
  rotate: true
  xy: 924, 574
  size: 102, 69
  orig: 102, 69
  offset: 0, 0
  index: -1
finger2_right
  rotate: true
  xy: 216, 736
  size: 132, 39
  orig: 132, 39
  offset: 0, 0
  index: -1
finger3_left
  rotate: true
  xy: 216, 665
  size: 67, 39
  orig: 67, 39
  offset: 0, 0
  index: -1
finger4_left
  rotate: false
  xy: 216, 643
  size: 38, 18
  orig: 38, 18
  offset: 0, 0
  index: -1
finger4_right
  rotate: true
  xy: 589, 8
  size: 111, 32
  orig: 111, 32
  offset: 0, 0
  index: -1
hair_front
  rotate: true
  xy: 503, 378
  size: 134, 66
  orig: 134, 66
  offset: 0, 0
  index: -1
head_red
  rotate: true
  xy: 625, 13
  size: 106, 98
  orig: 106, 98
  offset: 0, 0
  index: -1
lips_back
  rotate: false
  xy: 997, 582
  size: 20, 31
  orig: 20, 31
  offset: 0, 0
  index: -1
neck
  rotate: true
  xy: 4, 5
  size: 166, 253
  orig: 166, 253
  offset: 0, 0
  index: -1
teeth_back
  rotate: true
  xy: 573, 499
  size: 13, 12
  orig: 13, 12
  offset: 0, 0
  index: -1
teeth_bottom
  rotate: false
  xy: 535, 194
  size: 50, 32
  orig: 50, 32
  offset: 0, 0
  index: -1
teeth_up
  rotate: false
  xy: 914, 12
  size: 54, 15
  orig: 54, 15
  offset: 0, 0
  index: -1
thumb_left
  rotate: false
  xy: 727, 13
  size: 59, 106
  orig: 59, 106
  offset: 0, 0
  index: -1
thumb_right
  rotate: true
  xy: 503, 230
  size: 144, 65
  orig: 144, 65
  offset: 0, 0
  index: -1

John2.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
L_arm
  rotate: false
  xy: 157, 523
  size: 91, 188
  orig: 91, 188
  offset: 0, 0
  index: -1
L_hair_back
  rotate: true
  xy: 858, 962
  size: 56, 118
  orig: 56, 118
  offset: 0, 0
  index: -1
L_sleeve_cuffs
  rotate: false
  xy: 254, 715
  size: 126, 101
  orig: 126, 101
  offset: 0, 0
  index: -1
R_collar_shadow
  rotate: false
  xy: 640, 899
  size: 121, 119
  orig: 121, 119
  offset: 0, 0
  index: -1
R_eye
  rotate: false
  xy: 157, 392
  size: 26, 26
  orig: 26, 26
  offset: 0, 0
  index: -1
R_eye_shadow
  rotate: false
  xy: 640, 866
  size: 56, 29
  orig: 56, 29
  offset: 0, 0
  index: -1
R_hair_back
  rotate: false
  xy: 346, 649
  size: 46, 62
  orig: 46, 62
  offset: 0, 0
  index: -1
chest_back
  rotate: false
  xy: 4, 401
  size: 149, 310
  orig: 149, 310
  offset: 0, 0
  index: -1
collar_right
  rotate: false
  xy: 254, 820
  size: 212, 198
  orig: 212, 198
  offset: 0, 0
  index: -1
finger2_left
  rotate: false
  xy: 252, 540
  size: 86, 44
  orig: 86, 44
  offset: 0, 0
  index: -1
finger3_right
  rotate: false
  xy: 470, 825
  size: 132, 36
  orig: 132, 36
  offset: 0, 0
  index: -1
hair
  rotate: true
  xy: 4, 137
  size: 260, 117
  orig: 260, 117
  offset: 0, 0
  index: -1
hand_left
  rotate: false
  xy: 252, 588
  size: 90, 123
  orig: 90, 123
  offset: 0, 0
  index: -1
hand_right
  rotate: false
  xy: 470, 865
  size: 166, 153
  orig: 166, 153
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 4, 715
  size: 246, 303
  orig: 246, 303
  offset: 0, 0
  index: -1
mouth
  rotate: true
  xy: 346, 587
  size: 58, 32
  orig: 58, 32
  offset: 0, 0
  index: -1
nose
  rotate: false
  xy: 384, 733
  size: 64, 83
  orig: 64, 83
  offset: 0, 0
  index: -1
phone_back
  rotate: false
  xy: 4, 4
  size: 90, 129
  orig: 90, 129
  offset: 0, 0
  index: -1
phone_front
  rotate: false
  xy: 765, 931
  size: 89, 87
  orig: 89, 87
  offset: 0, 0
  index: -1
teeth_low
  rotate: true
  xy: 125, 346
  size: 51, 28
  orig: 51, 28
  offset: 0, 0
  index: -1
upper_lip_both
  rotate: false
  xy: 157, 422
  size: 85, 97
  orig: 85, 97
  offset: 0, 0
  index: -1
