
lantern.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
1/lamp_01
  rotate: false
  xy: 206, 293
  size: 103, 103
  orig: 103, 103
  offset: 0, 0
  index: -1
1/lantern_01_bottom
  rotate: false
  xy: 105, 172
  size: 97, 224
  orig: 97, 224
  offset: 0, 0
  index: -1
1/lantern_01_top
  rotate: false
  xy: 4, 70
  size: 97, 326
  orig: 97, 326
  offset: 0, 0
  index: -1
1/shadow_lantern_01
  rotate: false
  xy: 4, 4
  size: 29, 62
  orig: 29, 62
  offset: 0, 0
  index: -1

lantern2.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
2/lamp_02
  rotate: false
  xy: 4, 262
  size: 108, 229
  orig: 108, 229
  offset: 0, 0
  index: -1
2/lantern_02_bottom
  rotate: false
  xy: 116, 278
  size: 118, 213
  orig: 118, 213
  offset: 0, 0
  index: -1
2/lantern_02_top
  rotate: false
  xy: 4, 4
  size: 34, 254
  orig: 34, 254
  offset: 0, 0
  index: -1

lantern3.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
3/lamp_03
  rotate: false
  xy: 4, 297
  size: 141, 189
  orig: 141, 189
  offset: 0, 0
  index: -1
3/lantern_031_bottom
  rotate: false
  xy: 357, 411
  size: 133, 75
  orig: 133, 75
  offset: 0, 0
  index: -1
3/lantern_03_bottom
  rotate: true
  xy: 149, 413
  size: 73, 204
  orig: 73, 204
  offset: 0, 0
  index: -1
3/lantern_03_top
  rotate: false
  xy: 4, 75
  size: 69, 218
  orig: 69, 218
  offset: 0, 0
  index: -1
3/shadows_03
  rotate: false
  xy: 149, 304
  size: 152, 105
  orig: 152, 105
  offset: 0, 0
  index: -1
