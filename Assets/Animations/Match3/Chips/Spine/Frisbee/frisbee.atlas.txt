
frisbee.png
size: 346,286
format: RGBA8888
filter: Linear,Linear
repeat: none
frisbee_1
  rotate: false
  xy: 4, 49
  size: 101, 101
  orig: 101, 101
  offset: 0, 0
  index: -1
frisbee_2
  rotate: false
  xy: 109, 49
  size: 100, 101
  orig: 100, 101
  offset: 0, 0
  index: -1
frisbee_3
  rotate: false
  xy: 136, 181
  size: 101, 101
  orig: 101, 101
  offset: 0, 0
  index: -1
frisbee_4
  rotate: false
  xy: 213, 76
  size: 101, 101
  orig: 101, 101
  offset: 0, 0
  index: -1
frisbee_5
  rotate: false
  xy: 241, 181
  size: 101, 101
  orig: 101, 101
  offset: 0, 0
  index: -1
frisbee_light
  rotate: false
  xy: 4, 154
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
frisbee_light2
  rotate: true
  xy: 213, 24
  size: 48, 55
  orig: 48, 55
  offset: 0, 0
  index: -1
shadows
  rotate: false
  xy: 4, 4
  size: 41, 41
  orig: 41, 41
  offset: 0, 0
  index: -1
