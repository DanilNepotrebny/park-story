
chains.png
size: 156,142
format: RGBA8888
filter: Linear,Linear
repeat: none
bunch
  rotate: false
  xy: 119, 4
  size: 28, 24
  orig: 28, 24
  offset: 0, 0
  index: -1
chains
  rotate: false
  xy: 4, 27
  size: 111, 111
  orig: 111, 111
  offset: 0, 0
  index: -1
light_line
  rotate: true
  xy: 119, 32
  size: 28, 30
  orig: 28, 30
  offset: 0, 0
  index: -1
light_round
  rotate: true
  xy: 119, 64
  size: 32, 33
  orig: 32, 33
  offset: 0, 0
  index: -1
pin
  rotate: false
  xy: 4, 7
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
ring
  rotate: true
  xy: 119, 100
  size: 38, 31
  orig: 38, 31
  offset: 0, 0
  index: -1
