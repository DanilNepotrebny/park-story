
firework.png
size: 218,267
format: RGBA8888
filter: Linear,Linear
repeat: none
Shine
  rotate: false
  xy: 104, 41
  size: 22, 22
  orig: 22, 22
  offset: 0, 0
  index: -1
blue
  rotate: false
  xy: 130, 168
  size: 58, 95
  orig: 58, 95
  offset: 0, 0
  index: -1
blue1 copy
  rotate: true
  xy: 130, 73
  size: 91, 18
  orig: 91, 18
  offset: 0, 0
  index: -1
green
  rotate: false
  xy: 4, 67
  size: 59, 96
  orig: 59, 96
  offset: 0, 0
  index: -1
green1 copy
  rotate: true
  xy: 152, 73
  size: 91, 18
  orig: 91, 18
  offset: 0, 0
  index: -1
pink
  rotate: false
  xy: 67, 167
  size: 59, 96
  orig: 59, 96
  offset: 0, 0
  index: -1
pink1 copy
  rotate: true
  xy: 174, 73
  size: 91, 18
  orig: 91, 18
  offset: 0, 0
  index: -1
purple
  rotate: false
  xy: 67, 67
  size: 59, 96
  orig: 59, 96
  offset: 0, 0
  index: -1
purple1 copy
  rotate: true
  xy: 196, 77
  size: 91, 18
  orig: 91, 18
  offset: 0, 0
  index: -1
red
  rotate: true
  xy: 4, 4
  size: 59, 96
  orig: 59, 96
  offset: 0, 0
  index: -1
red1 copy
  rotate: false
  xy: 104, 19
  size: 91, 18
  orig: 91, 18
  offset: 0, 0
  index: -1
spark
  rotate: false
  xy: 130, 45
  size: 19, 24
  orig: 19, 24
  offset: 0, 0
  index: -1
yellow
  rotate: false
  xy: 4, 167
  size: 59, 96
  orig: 59, 96
  offset: 0, 0
  index: -1
yellow1 copy
  rotate: true
  xy: 192, 172
  size: 91, 18
  orig: 91, 18
  offset: 0, 0
  index: -1
