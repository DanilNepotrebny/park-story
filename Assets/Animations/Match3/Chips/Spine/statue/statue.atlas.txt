
statue.png
size: 329,115
format: RGBA8888
filter: Linear,Linear
repeat: none
rabbit_1st_state
  rotate: false
  xy: 4, 4
  size: 109, 107
  orig: 109, 107
  offset: 0, 0
  index: -1
rabbit_2nd_state
  rotate: false
  xy: 117, 9
  size: 103, 102
  orig: 103, 102
  offset: 0, 0
  index: -1
rabbit_3d_state
  rotate: false
  xy: 224, 11
  size: 101, 100
  orig: 101, 100
  offset: 0, 0
  index: -1
