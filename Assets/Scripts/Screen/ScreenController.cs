﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Screen
{
    public class ScreenController : ScriptableObject
    {
        [SerializeField, RequiredField] private ScreenSettings _screenSettings;
        [SerializeField, RequiredField] private EditorScreenSettings _editorScreenSettings;

        public float PlaneDistance => _screenSettings.PlaneDistance;
        public Vector2Int ReferenceLandscapeResolution => _screenSettings.ReferenceLandscapeResolution;
        public float TabletScaleFactor => _screenSettings.TabletScaleFactor;
        public float ReferencePPU => _screenSettings.ReferencePPU;
        public int Width => UnityEngine.Screen.width;
        public int Height => UnityEngine.Screen.height;
        public DeviceType Type => GetDeviceType();
        public Rect SafeArea => GetSafeArea();
        public float ScreenSizeFactor => Mathf.Min(Height, Width) / (float)ReferenceLandscapeResolution.y;
        public float DPI => Application.isEditor ? _editorScreenSettings.SimulatedDpi : UnityEngine.Screen.dpi;
        public float DPIFactor => DPI / _screenSettings.ReferenceDpi;
        public ScreenOrientationNotifier.Orientation Orientation =>
            ScreenOrientationNotifier.CalculateOrientation(Width, Height);

        #if UNITY_EDITOR
        public EditorScreenSettings EditorScreenSettings => _editorScreenSettings;
        #endif

        private float MinTabletDisplayDiagonal => _screenSettings.MinTabletDisplayDiagonal;

        public float CalculateOrthographicFactor()
        {
            float referenceGenericOrtho = CalculateGenericOrtho(
                ReferenceLandscapeResolution.x,
                ReferenceLandscapeResolution.y);
            float currentGenericOrtho = CalculateGenericOrtho(Width, Height);

            return currentGenericOrtho / referenceGenericOrtho;
        }

        private float CalculateGenericOrtho(int screenWidth, int screenHeight)
        {
            return screenHeight / (2 * ReferencePPU * GetZoom(screenWidth, screenHeight));
        }

        private float GetZoom(int screenWidth, int screenHeight)
        {
            int currentMin = Mathf.Min(screenWidth, screenHeight);
            int referenceMin = Mathf.Min(ReferenceLandscapeResolution.x, ReferenceLandscapeResolution.y);

            return (float)currentMin / referenceMin;
        }

        private bool IsTablet()
        {
            float diagonal = Mathf.Sqrt(Width * Width + Height * Height) / DPI;

            return diagonal > MinTabletDisplayDiagonal ||
                Mathf.Approximately(diagonal, MinTabletDisplayDiagonal);
        }

        private DeviceType GetDeviceType()
        {
            if (Application.isEditor)
            {
                return _editorScreenSettings.SimulatedDeviceType;
            }

            return IsTablet() ? DeviceType.Tablet : DeviceType.Phone;
        }

        private Rect GetSafeArea()
        {
            if (Application.isEditor && _editorScreenSettings.SimulatedSafeArea != null)
            {
                SafeArea area = _editorScreenSettings.SimulatedSafeArea;
                switch (Orientation)
                {
                    case ScreenOrientationNotifier.Orientation.Portrait:
                        return area.RectPortrait;

                    case ScreenOrientationNotifier.Orientation.Landscape:
                        return area.RectLandscapeLeft; // TODO: Handle landscape right

                    default:
                        throw new ArgumentOutOfRangeException(nameof(Orientation), Orientation, null);
                }
            }

            return UnityEngine.Screen.safeArea;
        }
    }
}
