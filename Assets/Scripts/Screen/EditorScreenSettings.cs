﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;
#endif

namespace DreamTeam.Screen
{
    #if UNITY_EDITOR
    [InitializeOnLoad]
    #endif

    public class EditorScreenSettings : ScriptableObject
        #if UNITY_EDITOR
        , IActiveBuildTargetChanged
        #endif
    {
        [SerializeField, RequiredField] private List<DeviceInfo> _simulatingDevices;

        private int _currentFrame = _totalFramesToUpdateLastSimulatingDevice;
        private const int _totalFramesToUpdateLastSimulatingDevice = 10;
        private bool _updateLastSimulatingDevice = true;
        private int _lastSelectedGameViewSizeIndex = -1;
        private DeviceInfo _lastSimulatingDevice;

        #if UNITY_EDITOR
        private const GameViewSizeHelper.GameViewSizeType _gameViewSizeType =
            GameViewSizeHelper.GameViewSizeType.FixedResolution;
        private GameViewSizeGroupType _currentGroupType;
        #endif

        public int SimulatedDpi => _simulatingDevice != null ? _simulatingDevice.DPI : 264;
        public DeviceType SimulatedDeviceType => _simulatingDevice != null ? _simulatingDevice.Type : DeviceType.Tablet;
        public SafeArea SimulatedSafeArea => _simulatingDevice != null ? _simulatingDevice.SafeArea : null;

        private DeviceInfo _simulatingDevice => FindGameViewDevice();

        #if UNITY_EDITOR

        private ScreenController _screenController => SystemAssetAccessor<ScreenController>.Asset;

        public event Action SimulatingDeviceChanged;

        static EditorScreenSettings()
        {
            EditorApplication.delayCall += () =>
            {
                AssetDatabase.LoadAssetAtPath<EditorScreenSettings>("Assets/Settings/Screen/EditorScreenSettings.asset");
            };
        }

        protected void OnEnable()
        {
            Initialize();

            EditorApplication.update += OnUpdate;

            EditorApplication.delayCall += OnDelayCall;
        }

        protected void OnDisable()
        {
            EditorApplication.update -= OnUpdate;
        }

        private void Initialize()
        {
            RefreshCurrentGroupType();

            bool addedNewSizes = false;

            foreach (DeviceInfo device in _simulatingDevices)
            {
                Array orientations = Enum.GetValues(typeof(ScreenOrientationNotifier.Orientation));
                foreach (ScreenOrientationNotifier.Orientation orientation in orientations)
                {
                    int width, height;
                    device.DetermineWidthAndHeight(orientation, out width, out height);

                    string viewName = GetDeviceGameViewName(device, orientation);
                    if (!GameViewSizeHelper.Contains(_currentGroupType, _gameViewSizeType, width, height, viewName))
                    {
                        UnityEngine.Debug.Log($"Added custom view size '{viewName} ({width}x{height})'");
                        GameViewSizeHelper.AddCustomSize(_currentGroupType, _gameViewSizeType, width, height, viewName);
                        addedNewSizes = true;
                    }
                }
            }

            if (addedNewSizes)
            {
                GameViewSizeHelper.SaveGameViewSizeModifications();
            }
        }

        private void OnUpdate()
        {
            if (Application.isPlaying)
            {
                UpdateSimulatingDeviceCheck();
            }
        }

        private void OnDelayCall()
        {
            EditorApplication.delayCall += OnDelayCall;
            if (!Application.isPlaying)
            {
                UpdateSimulatingDeviceCheck();
            }
        }

        private void UpdateSimulatingDeviceCheck()
        {
            _currentFrame += 1;
            if (_currentFrame > _totalFramesToUpdateLastSimulatingDevice)
            {
                _currentFrame = 0;
                _updateLastSimulatingDevice = true;
            }
        }

        private void RefreshCurrentGroupType()
        {
            _currentGroupType = GameViewSizeHelper.GetCurrentGameViewSizeGroupType();
        }
        
        #endif // UNITY_EDITOR

        private DeviceInfo FindGameViewDevice()
        {
            if (!_updateLastSimulatingDevice)
            {
                return _lastSimulatingDevice;
            }
            _updateLastSimulatingDevice = false;

            #if UNITY_EDITOR
            {
                int selectedIndex = GameViewSizeHelper.GetSelectedGameViewSizeIndex(_currentGroupType);
                if (selectedIndex == _lastSelectedGameViewSizeIndex)
                {
                    return _lastSimulatingDevice;
                }
    
                _lastSelectedGameViewSizeIndex = selectedIndex;
                ScreenOrientationNotifier.Orientation orientation = _screenController.Orientation;
                string selectedViewName = GameViewSizeHelper.GetGameViewSizeBaseText(_currentGroupType, selectedIndex);
                
                _lastSimulatingDevice = _simulatingDevices.Find(
                    device => selectedViewName.Equals(GetDeviceGameViewName(device, orientation)));

                SimulatingDeviceChanged?.Invoke();
            }
            #endif

            return _lastSimulatingDevice;
        }

        private static string GetDeviceGameViewName(DeviceInfo device, ScreenOrientationNotifier.Orientation orientation)
        {
            return $"{device.Name}_{orientation.ToString()}";
        }

        #region IActiveBuildTargetChanged

        #if UNITY_EDITOR

        int IOrderedCallback.callbackOrder => 0;

        void IActiveBuildTargetChanged.OnActiveBuildTargetChanged(BuildTarget previousTarget, BuildTarget newTarget)
        {
            RefreshCurrentGroupType();
        }

        #endif

        #endregion
    }
}
