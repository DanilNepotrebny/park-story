﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Screen
{
    [CreateAssetMenu(fileName = "DeviceInfo", menuName = "Device/Device Info")]
    public class DeviceInfo : ScriptableObject
    {
        [SerializeField] private DeviceType _type;
        [SerializeField, Tooltip("Screen width in landscape orientation")] private int _width = 1024;
        [SerializeField, Tooltip("Screen height in landscape orientation")] private int _height = 768;
        [SerializeField] private int _dpi = 264;
        [SerializeField] private SafeArea _safeArea;

        public string Name => name;
        public DeviceType Type => _type;
        public int DPI => _dpi;
        public SafeArea SafeArea => _safeArea;

        // Screen width in landscape orientation
        public int Width => _width;

        // Screen height in landscape orientation
        public int Height => _height;

        public void DetermineWidthAndHeight(
            ScreenOrientationNotifier.Orientation orientation,
            out int width,
            out int height)
        {
            switch (orientation)
            {
                case ScreenOrientationNotifier.Orientation.Portrait:
                    width = Height;
                    height = Width;
                    break;

                case ScreenOrientationNotifier.Orientation.Landscape:
                    width = Width;
                    height = Height;
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(orientation), orientation, null);
            }
        }
    }

    public enum DeviceType
    {
        Phone,
        Tablet
    }
}
