﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Screen
{
    [CreateAssetMenu(fileName = "SafeArea", menuName = "Device/Safe Area")]
    public class SafeArea : ScriptableObject
    {
        [SerializeField] private Rect _rectPortrait;
        [SerializeField] private Rect _rectLandscapeLeft;
        [SerializeField] private Rect _rectLandscapeRight;

        public Rect RectPortrait => _rectPortrait;
        public Rect RectLandscapeLeft => _rectLandscapeLeft;
        public Rect RectLandscapeRight => _rectLandscapeRight;
    }
}
