﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Screen
{
    public class ScreenSettings : ScriptableObject
    {
        [SerializeField] private float _planeDistance;
        [SerializeField] private Vector2Int _referenceLandscapeResolution;
        [SerializeField] private float _minTabletDisplayDiagonal;
        [SerializeField] private float _tabletScaleFactor;
        [SerializeField] private int _referenceDPI = 264;
        [SerializeField] private float _referencePixelsPerUnit = 100;

        public float PlaneDistance => _planeDistance;
        public Vector2Int ReferenceLandscapeResolution => _referenceLandscapeResolution;
        public float MinTabletDisplayDiagonal => _minTabletDisplayDiagonal;
        public float TabletScaleFactor => _tabletScaleFactor;
        public int ReferenceDpi => _referenceDPI;
        public float ReferencePPU => _referencePixelsPerUnit;
    }
}
