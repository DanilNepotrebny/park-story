﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam
{
    /// <summary>
    /// Implements accelerated movement to target position. Can be effectively used for smooth sequentional movement (e.g. cell-by-cell
    /// movement of match-3 chip) by manual call of <see cref="Finish"/> method when movement is fully finished.
    /// </summary>
    public class Mover : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _acceleration;
        [SerializeField] private float _maxSpeed = float.MaxValue;
        
        private Vector3 _target;
        private bool _isInMovingState;
        private bool _isMoving;
        private Action<Mover> _onStopped;

        private float _currentSpeed;
        private float _currentMaxSpeed;
        private float _currentAcceleration;

        /// <summary>
        /// Returns true if object is moving. Will remain true until <see cref="Finish"/> is called.
        /// </summary>
        public bool IsMoving
        {
            get { return _isMoving; }
            private set
            {
                if (_isMoving != value)
                {
                    _isMoving = value;
                    MovingChanged?.Invoke(value);
                }
            }
        }

        /// <summary>
        /// Is game object in a moving state. For sequentional movement will be set to false after each part of movement is completed.
        /// </summary>
        public bool IsInMovingState
        {
            get { return _isInMovingState; }
            private set
            {
                if (_isInMovingState != value)
                {
                    _isInMovingState = value;
                    enabled = value;
                }
            }
        }

        /// <summary>
        /// Returns current movement speed (affected by acceleration)
        /// </summary>
        public float CurrentSpeed
        {
            get { return _currentSpeed; }
            set
            {
                value = Mathf.Min(value, _currentMaxSpeed);
                if (!Mathf.Approximately(_currentSpeed, value))
                {
                    _currentSpeed = value;
                    SpeedChanged?.Invoke(value);
                }
            }
        }

        public float MaxSpeed => _maxSpeed;

        /// <summary>
        /// Triggered when movement is started or finished.
        /// </summary>
        public event Action<bool> MovingChanged;

        public event Action<Vector3> MovingUpdated;

        public event Action<float> SpeedChanged;

        /// <summary>
        /// Moves game object to target location with current speed.
        /// </summary>
        public void MoveTo(Vector3 target, Action<Mover> onStopped = null)
        {
            IsMoving = true;
            IsInMovingState = true;
            _target = target;
            _onStopped = onStopped;
        }

        /// <summary>
        /// Finishes current movement with alignment by target position. Resets speed to default value.
        /// </summary>
        public void Finish()
        {
            transform.position = _target;
            Stop();
        }

        public void Stop()
        {
            IsMoving = false;
            ResetParameters();

            StopMovement();
        }

        /// <summary>
        /// Unity callback
        /// </summary>
        protected void Awake()
        {
            ResetParameters();
            enabled = false;
        }

        protected void FixedUpdate()
        {
            UnityEngine.Debug.Assert(IsInMovingState);

            Vector3 oldPosition = transform.position;

            Vector3 from = oldPosition;
            Vector3 to = _target;

            Vector3 offset = CurrentSpeed * (to - from).normalized * Time.deltaTime;
            
            transform.position += offset;
            CurrentSpeed += _currentAcceleration * Time.deltaTime;

            Vector3 targetOffset = _target - oldPosition;

            if (offset.sqrMagnitude > targetOffset.sqrMagnitude || offset == targetOffset)
            {
                StopMovement();
            }

            MovingUpdated?.Invoke(transform.position - oldPosition);
        }

        private void StopMovement()
        {
            IsInMovingState = false;

            if (_onStopped != null)
            {
                var oldCallback = _onStopped;
                _onStopped = null;
                oldCallback(this);
            }
        }

        private void ResetParameters()
        {
            _currentMaxSpeed = _maxSpeed;
            _currentAcceleration = _acceleration;
            CurrentSpeed = _speed;
        }
    }
}