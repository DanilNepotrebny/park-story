﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Serialization;

namespace DreamTeam.KeyWordsReplacement
{
    public abstract class KeyWordReplacementProvider : MonoBehaviour
    {
        [SerializeField, FormerlySerializedAs("_keyWord")]
        private string _keyWordBody = "key_word";

        public string KeyWord => _keyWordBody;
        protected abstract string ReplaceToValue { get; }

        public void Replace(string keyWordFormat, ref string input)
        {
            input = Regex.Replace(input, string.Format(keyWordFormat, _keyWordBody), ReplaceToValue);
        }
    }
}