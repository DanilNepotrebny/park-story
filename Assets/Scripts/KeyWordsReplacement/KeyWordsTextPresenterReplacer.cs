﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using DreamTeam.Utils.UI;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.KeyWordsReplacement
{
    public class KeyWordsTextPresenterReplacer : MonoBehaviour
    {
        [SerializeField, RequiredField] private Component _textPresenterComponent;
        [Inject] private KeyWordsReplacementSystem _keyWordsReplacementSystem;

        private ITextPresenter _textPresenter => _textPresenterComponent as ITextPresenter;

        protected void OnEnable()
        {
            if (_textPresenter != null)
            {
                _textPresenter.TextChanged += ReplaceKeyWords;
                ReplaceKeyWords();
            }
        }

        protected void OnDisable()
        {
            if (_textPresenter != null)
            {
                _textPresenter.TextChanged -= ReplaceKeyWords;
            }
        }

        protected void OnValidate()
        {
            if (_textPresenterComponent != null)
            {
                _textPresenterComponent = _textPresenter as Component;
                Assert.IsNotNull(
                        _textPresenterComponent,
                        $"{nameof(_textPresenterComponent)} must inplement {typeof(ITextPresenter)}"
                    );
            }
        }

        protected void ReplaceKeyWords()
        {
            _textPresenter.Text = _keyWordsReplacementSystem.ReplaceKeyWords(_textPresenter.Text);
        }
    }
}
