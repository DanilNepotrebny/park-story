﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.KeyWordsReplacement
{
    public class SimpleKeyWordReplacementProvider : KeyWordReplacementProvider
    {
        [SerializeField, Space] private string _replaceToValue;

        protected override string ReplaceToValue => _replaceToValue;
    }
}
