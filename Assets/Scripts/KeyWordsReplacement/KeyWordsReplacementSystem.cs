﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.KeyWordsReplacement
{
    public class KeyWordsReplacementSystem : MonoBehaviour
    {
        [SerializeField, ReadOnly, Tooltip("{0} - key word body.")]
        private string _keyWordFormat = @"\B@{0}\b";

        [SerializeField]
        private List<KeyWordReplacementProvider> _replacementProviders = new List<KeyWordReplacementProvider>();

        public string ReplaceKeyWords(string input)
        {
            string result = input;
            _replacementProviders.ForEach(provider => provider.Replace(_keyWordFormat, ref result));
            return result;
        }

        protected void Awake()
        {
            _replacementProviders.RemoveAll(provider => provider == null);
            _replacementProviders.Sort((A, B) => B.KeyWord.Length.CompareTo(A.KeyWord.Length));
        }

        protected void Reset()
        {
            _replacementProviders.Clear();
            _replacementProviders.AddRange(GetComponentsInChildren<KeyWordReplacementProvider>());
        }
    }
}