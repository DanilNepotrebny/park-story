﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam.KeyWordsReplacement
{
    public class MainLevelIndexReplacementProvider : KeyWordReplacementProvider
    {
        #if !NO_MATCH3

        [Inject] private LevelSystem _levelSystem;

        protected override string ReplaceToValue => _levelSystem.ActiveController.CurrentLevelIndex.ToString();

        #else

        protected override string ReplaceToValue => "1";

        #endif
    }
}