﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Zenject;

namespace DreamTeam.KeyWordsReplacement
{
    public class UserNameReplacementProvider : KeyWordReplacementProvider
    {
        [Inject] private UserSettings _userSettings;

        protected override string ReplaceToValue => _userSettings.UserName;
    }
}
