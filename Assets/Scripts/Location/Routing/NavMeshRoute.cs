﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using UnityEngine.AI;

namespace DreamTeam.Location.Routing
{
    public class NavMeshRoute : IRoute
    {
        private const string _walkableAreaName = "Walkable";

        private const int _startBufferSize = 8;
        private const int _bufferExtendFactor = 2;

        private readonly NavMeshPath _navMeshPath = new NavMeshPath();
        private Vector3[] _points = new Vector3[_startBufferSize];

        public Vector3 this[int index] => _points[index];

        public int PointsCount { get; private set; }

        public bool Find(Vector3 sourcePosition, Vector3 targetPosition)
        {
            int walkableAreaId = 1 >> NavMesh.GetAreaFromName(_walkableAreaName);

            NavMeshHit hit;
            if (!NavMesh.SamplePosition(targetPosition, out hit, 0.1f, walkableAreaId))
            {
                return false;
            }

            if (!NavMesh.CalculatePath(sourcePosition, targetPosition, walkableAreaId, _navMeshPath))
            {
                return false;
            }

            while (true)
            {
                PointsCount = _navMeshPath.GetCornersNonAlloc(_points);

                if (PointsCount == _points.Length)
                {
                    // Unity will not produce any errors if buffer size is too small, so we should always extend the buffer
                    // if the buffer is full and try to get a path again.
                    Array.Resize(ref _points, _points.Length * _bufferExtendFactor);
                    UnityEngine.Debug.Log($"Points buffer is increased to {_points.Length}");
                }
                else
                {
                    break;
                }
            }

            return true;
        }
    }
}