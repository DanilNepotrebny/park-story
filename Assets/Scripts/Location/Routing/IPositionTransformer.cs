﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Location.Routing
{
    public interface IPositionTransformer
    {
        void Transform(ref Vector3 position);
        void TransformBack(ref Vector3 position);
    }
}