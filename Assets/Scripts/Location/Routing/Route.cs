﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Location.Routing
{
    [Serializable]
    public class Route : IRoute
    {
        [SerializeField] private Vector3[] _points;

        public Vector3 this[int index]
        {
            get { return _points[index]; }
            set { _points[index] = value; }
        }

        public int PointsCount => _points?.Length ?? 0;
    }
}
