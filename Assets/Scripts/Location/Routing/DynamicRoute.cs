﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DreamTeam.Location.Routing
{
    public class DynamicRoute : IRoute
    {
        private List<Vector3> _points;

        public DynamicRoute(IEnumerable<Vector3> points)
        {
            _points = points.ToList();
        }

        public Vector3 this[int index]
        {
            get { return _points[index]; }
            set { _points[index] = value; }
        }

        public int PointsCount => _points?.Count ?? 0;
    }
}
