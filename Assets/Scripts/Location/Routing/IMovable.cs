﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Location.Routing
{
    public interface IMovable
    {
        Vector3 Position { get; set; }

        Vector3 Direction { get; set; }
    }
}