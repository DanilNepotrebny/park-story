﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Location.Routing
{
    public interface IRoute
    {
        Vector3 this[int index] { get; }

        int PointsCount { get; }
    }
}
