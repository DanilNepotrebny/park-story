﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Interaction;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location
{
    public class LocationEnvironmentInstaller : MonoInstaller<LocationEnvironmentInstaller>
    {
        [SerializeField] private InteractionObjectsCollection _interactionObjectsCollection;

        public override void InstallBindings()
        {
            if (_interactionObjectsCollection != null)
            {
                Container.Bind<InteractionObjectsCollection>().FromInstance(_interactionObjectsCollection);
            }
        }
    }
}