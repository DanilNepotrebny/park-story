﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Location.Isometry
{
    /// <summary>
    /// Used to mark gameobject inside isometric <see cref="Layer"/> that has to be treated as a one isometric
    /// object with multiple renderers. All renderers of this gameobject will share the same sorting order.
    /// </summary>
    [ExecuteInEditMode]
    public class IsometryObjectGroup : MonoBehaviour
    {
        #if UNITY_EDITOR

        public bool IsDestroying { get; private set; }

        protected void Awake()
        {
            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                this.DisposeImmediate();
                return;
            }

            UpdateLayer();
        }

        protected void OnDestroy()
        {
            if (!EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                IsDestroying = true;
                UpdateLayer();
            }
        }

        private void UpdateLayer()
        {
            var layer = GetComponentInParent<Layer>();
            if (layer != null)
            {
                layer.UpdateElements();
            }
        }

        #endif
    }
}