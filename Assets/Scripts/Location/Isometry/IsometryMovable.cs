﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Location.Routing;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Isometry
{
    public class IsometryMovable : MonoBehaviour, IMovable, IPositionTransformer
    {
        [Inject] private IsometrySystem _cachedIsometrySystem;

        [SerializeField, HideInInspector] private Vector3 _direction = Vector3.down;

        public Vector3 Position
        {
            get { return _isometrySystem.ScreenToIsometryMatrix * transform.position; }
            set { transform.position = _isometrySystem.IsometryToScreenMatrix * value; }
        }

        public Vector3 Direction
        {
            get { return _direction; }
            set
            {
                if (_direction != value)
                {
                    _direction = value;
                    DirectionChanged?.Invoke();
                }
            }
        }

        public event Action DirectionChanged;

        public void Transform(ref Vector3 worldPosition)
        {
            worldPosition = _isometrySystem.ScreenToIsometryMatrix * worldPosition;
        }

        public void TransformBack(ref Vector3 position)
        {
            position = _isometrySystem.IsometryToScreenMatrix * position;
        }

        public void ScreenLookAt(Vector3 position)
        {
            Direction = _isometrySystem.ScreenToIsometryMatrix * (position - transform.position);
        }

        private IsometrySystem _isometrySystem
        {
            get
            {
                #if UNITY_EDITOR
                {
                    if (!Application.isPlaying && _cachedIsometrySystem == null)
                    {
                        _cachedIsometrySystem = SystemAssetAccessor<IsometrySystem>.Instance;
                    }
                }
                #endif // UNITY_EDITOR

                return _cachedIsometrySystem;
            }
        }
    }
}