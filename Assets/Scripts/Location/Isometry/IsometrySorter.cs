﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Rendering;
using Zenject;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DreamTeam.Location.Isometry
{
    /// <summary>
    /// Sorts all child renderers depending on the distance from camera in isometric world.
    /// </summary>
    [ExecuteInEditMode]
    public class IsometrySorter : MonoBehaviour
    {
        [SerializeField] private PositionChangeNotifier _positionProvider;

        [Inject] private IsometrySystem _cachedIsometrySystem;

        private Renderer[] _renderers;
        private SortingGroup _sortingGroup;

        public PositionChangeNotifier PositionProvider
        {
            get { return _positionProvider; }
            set
            {
                if (_positionProvider != value)
                {
                    Unsubscribe();

                    _positionProvider = value;

                    SubscribeAndUpdate();
                }
            }
        }

        public void UpdatePosition(Transform tr = null)
        {
            if (PositionProvider == null)
            {
                return;
            }

            if (_sortingGroup == null && _renderers == null)
            {
                Initialize();
            }

            if (_isometrySystem != null)
            {
                if (_sortingGroup != null)
                {
                    RecordPositionChange(_sortingGroup);

                    _isometrySystem.SortByCamera(_sortingGroup, PositionProvider.transform.position);
                }
                else
                {
                    foreach (Renderer r in _renderers)
                    {
                        RecordPositionChange(r);

                        _isometrySystem.SortByCamera(r, PositionProvider.transform.position);
                    }
                }
            }
        }

        protected void Awake()
        {
            Initialize();
        }

        protected void OnEnable()
        {
            SubscribeAndUpdate();
        }

        protected void OnDisable()
        {
            Unsubscribe();
        }

        private void RecordPositionChange(Object obj)
        {
            #if UNITY_EDITOR
            {
                if (!Application.isPlaying)
                {
                    Undo.RecordObject(obj, "Updated isometry position");
                }
            }
            #endif
        }

        private void Initialize()
        {
            _sortingGroup = GetComponent<SortingGroup>();

            if (_sortingGroup == null)
            {
                _renderers = GetComponentsInChildren<Renderer>(true);
            }
            else
            {
                _renderers = null;
            }
        }

        private void Unsubscribe()
        {
            if (PositionProvider != null)
            {
                PositionProvider.PositionChanged -= UpdatePosition;
            }
        }

        private void SubscribeAndUpdate()
        {
            if (PositionProvider != null && isActiveAndEnabled)
            {
                UpdatePosition();
                PositionProvider.PositionChanged += UpdatePosition;
            }
        }

        private IsometrySystem _isometrySystem
        {
            get
            {
                #if UNITY_EDITOR
                {
                    if (!Application.isPlaying && _cachedIsometrySystem == null)
                    {
                        _cachedIsometrySystem = SystemAssetAccessor<IsometrySystem>.Instance;
                    }
                }
                #endif // UNITY_EDITOR

                return _cachedIsometrySystem;
            }
        }
    }
}