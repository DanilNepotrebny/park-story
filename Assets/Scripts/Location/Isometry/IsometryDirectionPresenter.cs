﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Isometry
{
    [ExecuteInEditMode]
    public class IsometryDirectionPresenter : MonoBehaviour
    {
        [SerializeField] private IsometryMovable _movable;

        [Inject] private IsometrySystem _cachedIsometrySystem;

        protected void OnEnable()
        {
            if (_movable != null)
            {
                UpdateDirection();
                _movable.DirectionChanged += UpdateDirection;
            }
        }

        protected void OnDisable()
        {
            if (_movable != null)
            {
                _movable.DirectionChanged -= UpdateDirection;
            }
        }

        private void UpdateDirection()
        {
            if (_movable != null)
            {
                _isometrySystem.LookAt3D(transform, _isometrySystem.IsometryToScreenMatrix * _movable.Direction);
            }
        }

        private IsometrySystem _isometrySystem
        {
            get
            {
                #if UNITY_EDITOR
                {
                    if (!Application.isPlaying && _cachedIsometrySystem == null)
                    {
                        _cachedIsometrySystem = SystemAssetAccessor<IsometrySystem>.Instance;
                    }
                }
                #endif // UNITY_EDITOR

                return _cachedIsometrySystem;
            }
        }
    }
}