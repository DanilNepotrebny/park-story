﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.Rendering;

namespace DreamTeam.Location.Isometry
{
    /// <summary>
    /// Tools for isometry and pseudo-isometry transformations.
    /// Isometry transformation is rotation around X-axis by the isometry angle.
    /// Pseudo-isometry transformation imitates rotation and projection via scale by X-axis.
    /// See <a href="https://drive.google.com/open?id=1GxWlCYAC7MM5h9lU-ocvV0Yg_Yrsz-sZ">a diagramm</a> for
    /// better understanding of how it works.
    /// </summary>
    [ExecuteInEditMode]
    public class IsometrySystem : MonoBehaviour, IInitable
    {
        [SerializeField] private float _isometryAngle = 60f;

        private const float _sortingOrderFactor = 10f;

        private Matrix4x4 _isometryToScreenMatrix = Matrix4x4.identity;
        private Matrix4x4 _screenTo3DMatrix;
        private Vector3 _upVector3D;

        /// <summary>
        /// Matrix for transformation from pseudo-isometry into standard Unity screen coordinate system.
        /// </summary>
        public Matrix4x4 IsometryToScreenMatrix => _isometryToScreenMatrix;

        /// <summary>
        /// Matrix for transformation from standard Unity screen coordinate system into pseudo-isometry.
        /// </summary>
        public Matrix4x4 ScreenToIsometryMatrix { get; private set; }

        /// <summary>
        /// Makes 3D model that is supposed to be on pseudo-isometric plane to look by specified direction (in screen coordinate system).
        /// </summary>
        public void LookAt3D(Transform t, Vector3 screenDirection)
        {
            t.rotation = Quaternion.LookRotation(_screenTo3DMatrix * screenDirection, _upVector3D);
        }

        /// <summary>
        /// Setups specified renderer so it will be drawn in the right order (between other renderers and sorting groups on isometric plane).
        /// </summary>
        public void SortByCamera(Renderer r, Vector3 origin)
        {
            r.sortingOrder = CalculateSortingOrder(origin);
        }

        /// <summary>
        /// Setups specified sorting group so it will be drawn in the right order (between other renderers and sorting groups on isometric plane).
        /// </summary>
        public void SortByCamera(SortingGroup g, Vector3 origin)
        {
            g.sortingOrder = CalculateSortingOrder(origin);
        }

        public static Vector3 IsometricAngleToDirection(float angle)
        {
            return Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.right;
        }

        public static float IsometricDirectionToAngle(Vector3 direction)
        {
            return Quaternion.FromToRotation(Vector3.right, direction).eulerAngles.z;
        }

        public void Init()
        {
            _isometryToScreenMatrix[1, 1] = Mathf.Cos(Mathf.Deg2Rad * _isometryAngle);
            ScreenToIsometryMatrix = _isometryToScreenMatrix.inverse;

            // ScreenTo3D matrix is needed to transform a vector in the screen space into a vector
            // in the world space and:
            // 1. Resulting vector should lay on the isometry plane.
            // To achieve that we rotate by isometry angle.
            _screenTo3DMatrix = Matrix4x4.Rotate(Quaternion.AngleAxis(_isometryAngle, Vector3.right));

            // 2. Resulting vector should give the source vector after projection onto screen plane.
            // To achieve that we multiply by ScreenToIsometryMatrix to fake the "unprojection" operation.
            _screenTo3DMatrix *= ScreenToIsometryMatrix;

            _upVector3D = Vector3.Cross(_screenTo3DMatrix * Vector3.up, _screenTo3DMatrix * Vector3.right);
        }

        protected void Awake()
        {
            if (!Application.isPlaying)
            {
                Init();
            }
        }

        private static int CalculateSortingOrder(Vector3 origin)
        {
            return (int)(-origin.y * _sortingOrderFactor);
        }
    }
}