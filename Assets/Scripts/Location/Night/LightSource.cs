﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Night
{
    [RequireComponent(typeof(Animator))]
    public class LightSource : MonoBehaviour
    {
        [SerializeField] private float _radius;
        [SerializeField] private float _fadingLength;
        [SerializeField] private string _enabledParameterName;
        [SerializeField] private string _normalizedTimeParameterName;
        [SerializeField] private bool _isEnabled;

        [Inject] private NightSystem _nightSystem;

        private Animator _animator;

        public Vector3 Position => transform.position;
        public float Radius => _radius;
        public float FadingLength => _fadingLength;

        public Bounds Bounds
        {
            get
            {
                float d = 2 * Radius;
                return new Bounds(Position, new Vector3(d, d));
            }
        }

        public bool IsEnabled => _isEnabled;

        protected void Awake()
        {
            _animator = GetComponent<Animator>();
            _nightSystem.RegisterEffectHandler(this);
        }

        protected void OnDestroy()
        {
            _nightSystem.UnregisterEffectHandler(this);
        }

        public void SetEnabled(bool isEnabled, float normalizedStateProgress = 1.0f)
        {
            _animator?.SetBool(_enabledParameterName, isEnabled);
            _animator?.SetFloat(_normalizedTimeParameterName, normalizedStateProgress);
        }
    }
}