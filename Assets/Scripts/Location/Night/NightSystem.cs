﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Location.Night
{
    public class NightSystem : MonoBehaviour
    {
        [SerializeField] private string _nightFactorUniformName = "_NightFactor";
        [SerializeField] private string _lightSourcesCountUniformName = "_LightSourcesCount";
        [SerializeField] private string _lightSourcePositionsUniformName = "_LightSourcePositions";
        [SerializeField] private string _lightSourceRadiusesUniformName = "_LightSourceRadiuses";
        [SerializeField] private string _lightSourceFadingLengthsUniformName = "_LightSourceFadingLengths";

        [SerializeField, Range(0, 1)] private float _nightFactor = 0;

        private const int _lightSourcesCountMax = 5;

        private List<LightSource> _lightSources = new List<LightSource>();

        private Vector4[] _lightSourcePositions = new Vector4[_lightSourcesCountMax];
        private float[] _lightSourceRadiuses = new float[_lightSourcesCountMax];
        private float[] _lightSourceFadingLengths = new float[_lightSourcesCountMax];

        private Plane[] _frustumPlanes = new Plane[6];

        public float NightFactor
        {
            get { return _nightFactor; }
            set { _nightFactor = value; }
        }

        public IEnumerable<LightSource> LightSources => _lightSources;

        public void RegisterEffectHandler(LightSource lightSource)
        {
            if (!_lightSources.Contains(lightSource))
            {
                _lightSources.Add(lightSource);
            }
        }

        public void UnregisterEffectHandler(LightSource lightSource)
        {
            if (_lightSources.Contains(lightSource))
            {
                _lightSources.Remove(lightSource);
            }
        }

        protected void OnDisable()
        {
            _nightFactor = 0;
        }

        protected void Update()
        {
            UpdateShaderUniforms();
        }

        private void UpdateShaderUniforms()
        {
            Shader.SetGlobalFloat(_nightFactorUniformName, _nightFactor);

            if (Camera.main != null)
            {
                GeometryUtility.CalculateFrustumPlanes(Camera.main, _frustumPlanes);

                int lightSourcesIdx = 0;
                foreach (LightSource ls in _lightSources)
                {
                    if (ls.IsEnabled && GeometryUtility.TestPlanesAABB(_frustumPlanes, ls.Bounds))
                    {
                        _lightSourcePositions[lightSourcesIdx] = new Vector4(
                            ls.Position.x,
                            ls.Position.y,
                            ls.Position.z,
                            1.0f);
                        _lightSourceRadiuses[lightSourcesIdx] = ls.Radius;
                        _lightSourceFadingLengths[lightSourcesIdx] = ls.FadingLength;
                        lightSourcesIdx++;

                        if (lightSourcesIdx == _lightSourcesCountMax)
                        {
                            UnityEngine.Debug.LogError(
                                $"Only {_lightSourcesCountMax} light sources are supported simultaneously on the screen. " +
                                "Some light sources are skipped.",
                                gameObject);
                            break;
                        }
                    }
                }

                Shader.SetGlobalInt(_lightSourcesCountUniformName, lightSourcesIdx);
                Shader.SetGlobalVectorArray(_lightSourcePositionsUniformName, _lightSourcePositions);
                Shader.SetGlobalFloatArray(_lightSourceRadiusesUniformName, _lightSourceRadiuses);
                Shader.SetGlobalFloatArray(_lightSourceFadingLengthsUniformName, _lightSourceFadingLengths);
            }
        }
    }
}