﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Mirroring;
using DreamTeam.Utils;
using TouchScript.Gestures;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location
{
    [RequireComponent(typeof(TapGesture))]
    public class LockedLocationMessageActivator : MonoBehaviour
    {
        [Inject] private MirrorObjectsRegistrator _registrator;

        [SerializeField] [MirrorId] private string _locationMessageMirrorId;
        [SerializeField] private string _locationMessageActivateTriggerName;
        [SerializeField] [Range(0f, 10f)] private float _delay = 5f;

        private TapGesture _tapGesture;
        private Animator _locationMessageAnimator;
        private float _delayCounter = 0f;

        protected void Awake()
        {
            _tapGesture = GetComponent<TapGesture>();
            _tapGesture.Tapped += OnTapped;
            _tapGesture.StateChanged += OnTapGestureStateChanged;

            _locationMessageAnimator = _registrator.GetInstanceObject<Animator>(_locationMessageMirrorId);
        }

        protected void OnDestroy()
        {
            _tapGesture.Tapped -= OnTapped;
            _tapGesture.StateChanged -= OnTapGestureStateChanged;
        }

        protected void FixedUpdate()
        {
            _delayCounter = Mathf.Clamp(_delayCounter - Time.fixedDeltaTime, 0f, _delayCounter);
        }

        private void OnTapped(object sender, EventArgs e)
        {
            if (Math.Abs(_delayCounter) < Mathf.Epsilon)
            {
                _delayCounter = _delay;
                _locationMessageAnimator.SetTrigger(_locationMessageActivateTriggerName);
                WorldToUITransformer transformer = _locationMessageAnimator.GetComponent<WorldToUITransformer>();
                if (transformer != null)
                {
                    transformer.worldPosition = Camera.main.ScreenToWorldPoint(_tapGesture.ScreenPosition);
                }
            }
        }

        private void OnTapGestureStateChanged(object sender, GestureStateChangeEventArgs e)
        {
            switch (e.State)
            {
                case Gesture.GestureState.Failed:
                    ((Gesture)sender).TryCancelGesture(Camera.main.transform);
                    break;
            }
        }
    }
}