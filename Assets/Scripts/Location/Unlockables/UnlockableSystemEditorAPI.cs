﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;

namespace DreamTeam.Location.Unlockables
{
    public partial class UnlockableSystem : UnlockableSystem.IEditorAPI
    {
        void IEditorAPI.AddUnlockable(Unlockable unlockable)
        {
            Array.Resize(ref _unlockables, _unlockables.Length + 1);
            _unlockables[_unlockables.Length - 1] = unlockable;
        }

        #region Inner types

        public interface IEditorAPI
        {
            void AddUnlockable(Unlockable unlockable);
        }

        #endregion
    }
}

#endif