﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.GameControls;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Unlockables
{
    public class GameControlsModeHandlerByUnlockable : BaseGameControlsModeHandler, IInitable
    {
        [SerializeField] private Unlockable _unlockable;

        [Inject] private UnlockableSystem _unlockableSystem;

        private Unlockable.State _unlockableState;

        public void Init()
        {
            _unlockableState = _unlockableSystem.GetUnlockableState(_unlockable);
            _unlockableState.StageChanged += UpdateState;
        }
      
        public override void Dispose()
        {
            base.Dispose();

            _unlockableState.StageChanged -= UpdateState;
            _unlockableState = null;
        }

        protected void Awake()
        {
            UpdateState(_unlockableState.Stage);
        }

        private void UpdateState(int unlockableStage)
        {
            if (!IsModeEnabled && unlockableStage == 0)
            {
                EnableMode();
            }
            else if (IsModeEnabled && unlockableStage > 0)
            {
                DisableMode();
            }
        }
    }
}
