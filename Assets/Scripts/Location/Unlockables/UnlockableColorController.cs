﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Unlockables
{
    public class UnlockableColorController : MonoBehaviour
    {
        [SerializeField] private Unlockable _unlockable;
        [SerializeField] private Color _lockedColor;
        [SerializeField] private Color _unlockedColor;

        [SerializeField] [Range(0f, 1f)] private float _colorFactor;

        [Inject] private UnlockableSystem _unlockableSystem;

        private List<Renderer> _renderers = new List<Renderer>();
        private Unlockable.State _state => _unlockableSystem.GetUnlockableState(_unlockable);
        private float _previousColorFactor = -1f;
        private MaterialPropertyBlock _propertyBlock;

        protected void Start()
        {
            _propertyBlock = new MaterialPropertyBlock();

            _colorFactor = Mathf.Clamp01(_state.Stage);
            _state.StageChanged += UpdateStage;

            GetComponentsInChildren<Renderer>(true, _renderers);
        }

        protected void OnDestroy()
        {
            _renderers.Clear();

            _state.StageChanged -= UpdateStage;
        }

        protected void OnTransformChildrenChanged()
        {
            GetComponentsInChildren<Renderer>(true, _renderers);
        }

        protected void Update()
        {
            if (Mathf.Abs(_colorFactor - _previousColorFactor) > Mathf.Epsilon)
            {
                _previousColorFactor = _colorFactor;
                SetMaterialsColor(Color.Lerp(_lockedColor, _unlockedColor, _colorFactor));
            }
        }

        private void SetMaterialsColor(Color color)
        {
            foreach (var objectRenderer in _renderers)
            {
                objectRenderer.GetPropertyBlock(_propertyBlock);
                _propertyBlock.SetColor("_Color", color);
                objectRenderer.SetPropertyBlock(_propertyBlock);
            }
        }

        private void UpdateStage(int stage)
        {
            _colorFactor = Mathf.Clamp01(stage);
        }
    }
}