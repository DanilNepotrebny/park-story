﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;

namespace DreamTeam.Location.Unlockables
{
    /// <summary>
    /// Handle of object on some location that can be repaired during the game progress.
    /// State of such object is saved between user sessions. To obtain the state use <see cref="UnlockableSystem.GetUnlockableState"/>.
    /// </summary>
    public class Unlockable : ScriptableObject
    {
        #region Inner types

        /// <summary>
        /// Runtime data and logic of the <see cref="Unlockable"/> object.
        /// </summary>
        public class State : ISynchronizable
        {
            private int _stage;
            private List<PreparationDelegate> _preparingListeners =
                new List<PreparationDelegate>();

            public int Stage
            {
                get { return _stage; }
                set
                {
                    if (_stage != value)
                    {
                        _stage = value;
                        StageChanged?.Invoke(value);
                    }
                }
            }

            public event Action<int> StageChanged;

            public void AddPreparingListener(
                    PreparationDelegate listener
                )
            {
                _preparingListeners.Add(listener);
            }

            public void RemovePreparingListener(
                    PreparationDelegate listener
                )
            {
                _preparingListeners.Remove(listener);
            }

            public BaseAsyncProcess PrepareStateChange(int pendingState)
            {
                return new GroupAsyncProcess(
                        _preparingListeners
                            .Select(listener => listener?.Invoke(pendingState))
                            .ToArray()
                    );
            }

            void ISynchronizable.Sync(Synchronization.States.State state)
            {
                state.SyncInt("stage", ref _stage);
            }

            #region Inner types

            public delegate BaseAsyncProcess PreparationDelegate(int pendingState);

            #endregion
        }

        #endregion
    }
}