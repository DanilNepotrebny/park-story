﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.ResourceLoading;
using UnityEngine;

namespace DreamTeam.Location.Unlockables
{
    public class StagedUnlockableController : UnlockableControllerBase, IResourceLoaderHub
    {
        [SerializeField] private GameObject[] _stages;

        public override void Init()
        {
            base.Init();

            UnlockableState.AddPreparingListener(OnUnlockableStatePreparing);
        }

        public override void Dispose()
        {
            base.Dispose();

            UnlockableState.RemovePreparingListener(OnUnlockableStatePreparing);
        }

        private BaseAsyncProcess OnUnlockableStatePreparing(int pendingstate)
        {
            return GetResourceLoaderHubForStage(pendingstate).Preload();
        }

        protected override void LockInternal()
        {
            UpdateActivity(-1);
        }

        protected override void SyncInternal()
        {
            UpdateActivity(CurrentDataStage);
        }

        protected override void UpdateToStageInternal(int stage)
        {
            UpdateActivity(stage);
        }

        protected override List<IStageModificationHandler> GetHandlers()
        {
            List<IStageModificationHandler> handlers = new List<IStageModificationHandler>();
            foreach (GameObject stage in _stages)
            {
                handlers.AddRange(stage.GetComponentsInChildren<IStageModificationHandler>());
            }

            return handlers;
        }

        private void UpdateActivity(int stage)
        {
            for (int i = 0; i < _stages.Length; i++)
            {
                GameObject obj = _stages[i];
                if (obj != null)
                {
                    obj.SetActive(stage == i);
                }
            }
        }

        private IResourceLoaderHub GetResourceLoaderHubForStage(int stage = -1)
        {
            return _stages[stage == -1 ? CurrentDataStage : stage]?.GetComponent<IResourceLoaderHub>();
        }

        #region IPreloaderHub

        BaseAsyncProcess IResourceLoader.Preload()
        {
            return GetResourceLoaderHubForStage()?.Preload();
        }

        public IResourceHandle Load()
        {
            return GetResourceLoaderHubForStage()?.Load();
        }

        #endregion
    }
}