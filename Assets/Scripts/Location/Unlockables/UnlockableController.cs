﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;

namespace DreamTeam.Location.Unlockables
{
    public class UnlockableController : UnlockableControllerBase
    {
        protected override void LockInternal()
        {
            gameObject.SetActive(false);
        }

        protected override void SyncInternal()
        {
            gameObject.SetActive(CurrentDataStage > 0);
        }

        protected override void UpdateToStageInternal(int stage)
        {
            gameObject.SetActive(stage > 0);
        }

        protected override List<IStageModificationHandler> GetHandlers()
        {
            return GetComponentsInChildren<IStageModificationHandler>().ToList();
        }
    }
}