﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Unlockables
{
    public abstract class UnlockableControllerBase : MonoBehaviour, IInitable, IDisposable
    {
        [SerializeField] private Unlockable _unlockable;

        [Inject] private UnlockableSystem _unlockableSystem;

        private int _currentControllerStage = -1;

        public int CurrentDataStage
        {
            get
            {
                int result = 0;
                if (EditorUtils.IsPlayingOrWillChangePlaymode())
                {
                    result = UnlockableState.Stage;
                }

                return result;
            }
        }

        protected Unlockable.State UnlockableState => _unlockableSystem.GetUnlockableState(_unlockable);

        // [TimelineLateUpdate] issue fix
        public void LockSafe()
        {
            StartCoroutine(CallNextFrame(Lock));
        }

        // [TimelineLateUpdate] issue fix
        public void SyncSafe()
        {
            StartCoroutine(CallNextFrame(Sync));
        }

        // [TimelineLateUpdate] issue fix
        public void IncrementStageSafe()
        {
            StartCoroutine(CallNextFrame(IncrementStage));
        }

        // [TimelineLateUpdate] issue fix
        public void DecrementStageSafe()
        {
            StartCoroutine(CallNextFrame(DecrementStage));
        }

        public void Lock()
        {
            if (_currentControllerStage > 0)
            {
                LockInternal();

                List<IStageModificationHandler> handlers = GetHandlers();
                foreach (IStageModificationHandler handler in handlers)
                {
                    handler.HandleStageUpdate(_currentControllerStage, -1);
                }

                _currentControllerStage = -1;
            }
        }

        public void Sync()
        {
            if (_currentControllerStage != CurrentDataStage)
            {
                SyncInternal();

                List<IStageModificationHandler> handlers = GetHandlers();
                foreach (IStageModificationHandler handler in handlers)
                {
                    handler.HandleStageUpdate(_currentControllerStage, CurrentDataStage);
                }

                _currentControllerStage = CurrentDataStage;
            }
        }

        public void IncrementStage()
        {
            UpdateToStageInternal(_currentControllerStage + 1);

            List<IStageModificationHandler> handlers = GetHandlers();
            foreach (IStageModificationHandler handler in handlers)
            {
                handler.HandleStageUpdate(_currentControllerStage, _currentControllerStage + 1);
            }

            ++_currentControllerStage;
        }

        public void DecrementStage()
        {
            UpdateToStageInternal(_currentControllerStage - 1);

            List<IStageModificationHandler> handlers = GetHandlers();
            foreach (IStageModificationHandler handler in handlers)
            {
                handler.HandleStageUpdate(_currentControllerStage, _currentControllerStage - 1);
            }

            --_currentControllerStage;
        }

        protected abstract void LockInternal();
        protected abstract void SyncInternal();
        protected abstract void UpdateToStageInternal(int stage);
        protected abstract List<IStageModificationHandler> GetHandlers();

        public virtual void Init()
        {
            UnlockableState.StageChanged += OnUnlockableStageChanged;

            _currentControllerStage = CurrentDataStage;

            SyncInternal();
        }

        public virtual void Dispose()
        {
            UnlockableState.StageChanged -= OnUnlockableStageChanged;
        }

        private void OnUnlockableStageChanged(int stage)
        {
            if (_currentControllerStage != stage)
            {
                UpdateToStageInternal(stage);

                _currentControllerStage = stage;
            }
        }

        private IEnumerator CallNextFrame(Action action)
        {
            yield return null;
            action?.Invoke();
        }

        public interface IStageModificationHandler
        {
            void HandleStageUpdate(int previousStage, int currentStage);
        }
    }
}