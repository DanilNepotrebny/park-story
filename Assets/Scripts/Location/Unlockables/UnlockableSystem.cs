﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Unlockables
{
    /// <summary>
    /// Contains and provides runtime state of <see cref="Unlockable"/> objects.
    /// </summary>
    public partial class UnlockableSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private Unlockable[] _unlockables;

        [Inject] private Instantiator _instantiator;
        [Inject] private SaveGroup _saveGroup;

        private Unlockable.State[] _states;

        public void Init()
        {
            _states = new Unlockable.State[_unlockables.Length];
            for (int i = 0; i < _states.Length; i++)
            {
                _states[i] = _instantiator.Instantiate<Unlockable.State>();
            }

            _saveGroup.RegisterSynchronizable("unlockables", this);
        }

        public Unlockable.State GetUnlockableState(Unlockable unlockable)
        {
            int idx = Array.IndexOf(_unlockables, unlockable);
            if (idx < 0)
            {
                throw new ArgumentOutOfRangeException(
                        $"Unlockable {unlockable.name} is not added to {nameof(UnlockableSystem)}",
                        nameof(unlockable)
                    );
            }

            return _states[idx];
        }

        void ISynchronizable.Sync(State state)
        {
            for (int i = 0; i < _unlockables.Length; i++)
            {
                state.SyncObject(_unlockables[i].name, _states[i]);
            }
        }
    }
}