﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Cinemachine;
using DreamTeam.CameraControllers;
using DreamTeam.Cutscenes;
using DreamTeam.EndlessGrind;
using DreamTeam.Location.ActionByLevel;
using DreamTeam.Location.Characters;
using DreamTeam.Location.Decorations;
using DreamTeam.Location.Decorations.UI;
using DreamTeam.Location.Friends;
using DreamTeam.Location.Interaction;
using DreamTeam.Location.Isometry;
using DreamTeam.Location.Night;
using DreamTeam.Location.Unlockables;
using DreamTeam.Mirroring;
using DreamTeam.Popups.FortuneWheel;
using DreamTeam.Quests;
using DreamTeam.Quests.Commands;
using DreamTeam.Tutorial;
using DreamTeam.UI.Dialogs;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location
{
    public class LocationInstaller : MonoInstaller<LocationInstaller>
    {
        public const string MetagameMusicAudioSourceId = "MetagameMusicAudioSourceId";

        [Space] [SerializeField] private Character _mainCharacter;
        [SerializeField] private IsometrySystem _isometrySystemPrefab;
        [SerializeField] private VirtualFriendsUnlockSystem _virtualFriendsUnlockSystemPrefab;
        [SerializeField] private EndlessGrindSystem _endlessGrindSystemPrefab;
        [SerializeField] private CinemachineBrain _locationCameraBrain;
        [SerializeField] private LocationCameraController _locationCameraController;
        [SerializeField] private AudioSource _audioSource;

        public override void InstallBindings()
        {
            Container.Bind<MirrorObjectsRegistrator>().AsSingle().NonLazy();

            Container.Bind<CutsceneSystem>().FromComponentInHierarchy().AsSingle();
            Container.Bind<DialogController>().FromComponentInHierarchy(null, true).AsSingle().NonLazy();
            Container.Bind<QuestManager>().FromComponentInHierarchy().AsSingle().NonLazy();

            Container.Bind<UnlockableSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<DecorationSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<DecorationCustomizationSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<DecorationSkinPanel>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<DecorationOverviewPanel>().FromComponentInHierarchy(null, true).AsSingle().NonLazy();
            Container.Bind<NightSystem>().FromComponentInHierarchy(null, true).AsSingle().NonLazy();
            Container.Bind<FortuneWheelSystem>().FromComponentInHierarchy().AsSingle().NonLazy();

            Container.Bind<LocationCameraTweener>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<TutorialTintController>().FromComponentInHierarchy(null, true).AsSingle().NonLazy();

            Container.Bind<Character>().WithId(Character.MainCharacterId).FromInstance(_mainCharacter);

            Container.Bind<VirtualFriendsUnlockSystem>()
                .FromComponentInNewPrefab(_virtualFriendsUnlockSystemPrefab)
                .WithGameObjectName(_virtualFriendsUnlockSystemPrefab.name)
                .AsSingle()
                .NonLazy();

            Container.Bind<IsometrySystem>()
                .FromComponentInNewPrefab(_isometrySystemPrefab)
                .WithGameObjectName(_isometrySystemPrefab.name)
                .AsSingle()
                .NonLazy();

            Container.Bind<EndlessGrindSystem>()
                .FromComponentInNewPrefab(_endlessGrindSystemPrefab)
                .WithGameObjectName(_endlessGrindSystemPrefab.name)
                .AsSingle()
                .NonLazy();

            Container.Bind<AudioSource>()
                .WithId(MetagameMusicAudioSourceId)
                .FromInstance(_audioSource);

            Container.Bind<PlayCutsceneCommandsRegistrator>()
                .FromInstance(new PlayCutsceneCommandsRegistrator())
                .AsSingle();

            Container.Bind<LocationCameraController>()
                .FromInstance(_locationCameraController)
                .AsSingle();

            Container.Bind<CinemachineBrain>()
                .FromInstance(_locationCameraBrain)
                .AsSingle();

            Container.BindInternals<Goal>();
            Container.BindInternals<Quest>();
            Container.BindInternals<QuestPack>();
            Container.BindInternals<QuestManager>();
            Container.BindInternals<PlayCutsceneCommand>();

            Container.BindInternals<Character>();
            Container.BindInternals<CharacterRouteMover>();
            Container.BindInternals<CharacterAI>();
            Container.BindInternals<InteractionObjectsCollection>();
            Container.BindInternals<InteractionObject>();

            Container.BindInternals<DecorationPresenter>();

            Container.BindInternals<ActorsByLevelManager>();

            Container.BindComponent<SaveGroup>(this);
        }
    }
}