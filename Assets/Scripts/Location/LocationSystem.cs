﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Location
{
    public class LocationSystem : MonoBehaviour
    {
        [SerializeField] private LocationData _currentLocation;

        public LocationData CurrentLocation => _currentLocation;
    }
}