﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Inventory.Prices;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location
{
    public class PriceCannotBePayedPopupShower : MonoBehaviour
    {
        [SerializeField] private CountedItemPrice _price;
        [SerializeField] private GamePlace _gamePlace;

        [Inject] private PriceInstanceContainer _priceInstanceContainer;

        protected void Start()
        {
            if (!_priceInstanceContainer.GetInstance(_price).CanBePayed)
            {
                _price.Item.ShowNotEnoughPopup(_gamePlace);
            }
        }
    }
}
