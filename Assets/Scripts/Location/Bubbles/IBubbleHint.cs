﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Location.Bubbles
{
    public interface IBubbleHint
    {
        Vector2 FollowPosition { get; }
        Vector2 CurrentPosition { get; }

        event Action BubblePositionChanged;
    }
}