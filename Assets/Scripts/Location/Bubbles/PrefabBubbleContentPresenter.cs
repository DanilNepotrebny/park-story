﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Location.Bubbles
{
    [RequireComponent(typeof(RectTransform))]
    public class PrefabBubbleContentPresenter : MonoBehaviour, IPresenter<PrefabBubbleArgs>
    {
        private RectTransform _model;

        public void Initialize(PrefabBubbleArgs model)
        {
            if (_model != null)
            {
                _model.gameObject.Dispose();
            }

            _model = model.Content;
            _model.SetParent(transform, false);
            _model.StretchToParrent();
        }
    }
}
