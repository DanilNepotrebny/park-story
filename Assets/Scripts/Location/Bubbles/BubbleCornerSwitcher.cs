﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Location.Bubbles
{
    public class BubbleCornerSwitcher : MonoBehaviour
    {
        [SerializeField, RequiredField, ReadOnly] private Component _bubbleComponent;
        [SerializeField] private List<AngleTargetSet> _angleTargets = new List<AngleTargetSet>();

        private IBubbleHint _bubble => _bubbleComponent as IBubbleHint;

        protected void OnEnable()
        {
            if (_bubble != null)
            {
                _bubble.BubblePositionChanged += SelectObject;
                SelectObject();
            }
        }

        protected void OnDisable()
        {
            if (_bubble != null)
            {
                _bubble.BubblePositionChanged -= SelectObject;
            }
        }

        protected void Reset()
        {
            _bubbleComponent = GetComponentInParent<IBubbleHint>() as Component;
        }

        protected void OnValidate()
        {
            if (_bubbleComponent != null)
            {
                _bubbleComponent = _bubble as Component;
                Assert.IsNotNull(
                        _bubbleComponent,
                        $"{nameof(_bubbleComponent)} must implement {nameof(IBubbleHint)}"
                    );
            }
        }

        private void SelectObject()
        {
            Vector2 direction = (_bubble.CurrentPosition - _bubble.FollowPosition).normalized;
            float angle = Vector2.SignedAngle(Vector2.up, direction);
            _angleTargets.ForEach(item => item.UpdateActivation(angle));
        }

        [Serializable]
        private struct AngleTargetSet
        {
            [SerializeField, RequiredField] private GameObject _target;
            [SerializeField] private List<AngleRanges> _angles;

            public void UpdateActivation(float angle)
            {
                _target?.SetActive(_angles.Any(item => angle.InRange(item.MinAngle, item.MaxAngle, false)));
            }
        }

        [Serializable]
        private struct AngleRanges
        {
            [SerializeField, Range(-180, 180), Tooltip("Min signed angle relative to up direction")]
            private int _minAngle;

            [SerializeField, Range(-180, 180), Tooltip("Min signed angle relative to up direction")]
            private int _maxAngle;

            public int MinAngle => _minAngle;
            public int MaxAngle => _maxAngle;
        }
    }
}