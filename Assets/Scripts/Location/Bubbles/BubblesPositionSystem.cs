﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Location.Bubbles
{
    public class BubblesPositionSystem : MonoBehaviour, IComparer<Vector2>, IComparer<float>
    {
        [SerializeField] private List<Vector2> _directions = new List<Vector2>();

        [SerializeField, Header("Gizmos:")] private bool _drawObstacles;
        [SerializeField] private bool _drawCorrectDisplacements;
        [SerializeField] private bool _drawIncorrectDisplacements;

        private const float _epsilon = 0.02f;

        private List<IBubbleObstacle> _obstacles = new List<IBubbleObstacle>();
        private HashSet<IBubbleObstacle> _visitedObstacles = new HashSet<IBubbleObstacle>();
        private List<DisplacementData> _correctDisplacements = new List<DisplacementData>();
        private List<DisplacementData> _incorrectDisplacements = new List<DisplacementData>();

        private Rect _cameraRect => Camera.main.GetWorldViewportRect(Camera.main.nearClipPlane);

        public void RegisterObstacle(IBubbleObstacle obstacle)
        {
            if (!_obstacles.Contains(obstacle))
            {
                _obstacles.Add(obstacle);
            }
        }

        public void UnregisterObstacle(IBubbleObstacle obstacle)
        {
            if (_obstacles.Contains(obstacle))
            {
                _obstacles.Remove(obstacle);
            }
        }

        public void CalculateBordersPosition(
            ref Rect borders,
            IEnumerable<IBubbleObstacle> ignoreList,
            Rect snapRect,
            Vector2 preferedDirection)
        {
            FindPossibleDisplacements(borders, ignoreList);

            IOrderedEnumerable<DisplacementData> sorted = _correctDisplacements
                .OrderBy(d => d.IsSnappedTo(snapRect) ? -1 : 0)
                .ThenBy(d => d.AngleBetween(preferedDirection))
                .ThenBy(d => d.Direction, this)
                .ThenBy(d => d.ShiftDistance, this);

            borders = sorted.Any() ? sorted.First().ShiftedRect : borders;
        }

        #if UNITY_EDITOR

        protected void OnDrawGizmos()
        {
            if (_drawIncorrectDisplacements)
            {
                Gizmos.color = new Color(1, 1, 0, 0.15f);
                foreach (DisplacementData displacement in _incorrectDisplacements)
                {
                    displacement.DrawGizmos();
                }
            }

            if (_drawCorrectDisplacements)
            {
                Gizmos.color = new Color(0, 0, 1, 0.15f);
                foreach (DisplacementData displacement in _correctDisplacements)
                {
                    displacement.DrawGizmos();
                }
            }

            if (_drawObstacles)
            {
                Gizmos.color = Color.red;
                foreach (IBubbleObstacle obstacle in _obstacles)
                {
                    Gizmos.DrawWireCube(obstacle.Borders.center, obstacle.Borders.size);
                }
            }
        }

        #endif

        private void FindPossibleDisplacements(Rect rect, IEnumerable<IBubbleObstacle> ignoreList)
        {
            _correctDisplacements.Clear();
            _incorrectDisplacements.Clear();

            foreach (Vector2 direction in _directions)
            {
                Rect currentRect = rect;
                _visitedObstacles.Clear();
                IEnumerable<IBubbleObstacle> currentOverlaps;

                do
                {
                    currentOverlaps = GetCurrentOverlaps(currentRect, ignoreList);
                    foreach (IBubbleObstacle obstacle in currentOverlaps)
                    {
                        _visitedObstacles.Add(obstacle);

                        var displacement = new DisplacementData(currentRect, obstacle.Borders, direction, _cameraRect);

                        if (!displacement.Equals(default(DisplacementData)))
                        {
                            if (!OvrelapsAnyObstacle(displacement.ShiftedRect, ignoreList))
                            {
                                _correctDisplacements.Add(displacement);
                            }
                            else
                            {
                                _incorrectDisplacements.Add(displacement);
                                currentRect = displacement.ShiftedRect;
                            }
                        }
                    }
                }
                while (currentOverlaps.Any());
            }
        }

        private IEnumerable<IBubbleObstacle> GetCurrentOverlaps(Rect rect, IEnumerable<IBubbleObstacle> ignoreList)
        {
            return _obstacles.Where(
                    item => item.Borders.Overlaps(rect, 2) &&
                        !_visitedObstacles.Contains(item) &&
                        !ignoreList.Contains(item)
                );
        }

        private bool OvrelapsAnyObstacle(Rect rect, IEnumerable<IBubbleObstacle> ignoreList)
        {
            return _obstacles.Any(obstacle => !ignoreList.Contains(obstacle) && obstacle.Borders.Overlaps(rect, 2));
        }

        private Vector2 GetClosestDirection(Vector2 direction)
        {
            Vector2 result = Vector2.zero;
            float minAngle = float.MaxValue;

            foreach (Vector2 vector2 in _directions)
            {
                float angle = Vector2.Angle(vector2, direction);
                if (angle < minAngle)
                {
                    minAngle = angle;
                    result = vector2;
                }
            }

            return result;
        }

        /// <summary>
        /// Displacements comparision by shift direction
        /// </summary>
        int IComparer<Vector2>.Compare(Vector2 directionA, Vector2 directionB)
        {
            Vector2 closestDirectionA = GetClosestDirection(directionA);
            Vector2 closestDirectionB = GetClosestDirection(directionB);

            int directionIndexA = _directions.IndexOf(closestDirectionA);
            int directionIndexB = _directions.IndexOf(closestDirectionB);

            return directionIndexA.CompareTo(directionIndexB);
        }

        /// <summary>
        /// Displacements comparision by shift direction
        /// </summary>
        int IComparer<float>.Compare(float distanceA, float distanceB)
        {
            return Math.Abs(distanceA - distanceB) > _epsilon ? distanceA.CompareTo(distanceB) : 0;
        }

        private struct DisplacementData
        {
            public float ShiftDistance { get; }
            public Vector2 Direction { get; }
            public Rect ShiftedRect { get; }

            public DisplacementData(Rect from, Rect to, Vector2 direction, Rect external)
            {
                if (!from.Overlaps(to))
                {
                    Direction = default(Vector2);
                    ShiftDistance = 0;
                    ShiftedRect = default(Rect);
                    return;
                }

                Direction = direction.normalized;
                ShiftDistance = from.GetDisplacementDistance(to, Direction);
                ShiftedRect = from.Translate(ShiftDistance * Direction).Incapsulate(external);
            }

            public bool IsSnappedTo(Rect rect)
            {
                return rect != default(Rect) && ShiftedRect.IsSnappedTo(rect, _epsilon);
            }

            public float AngleBetween(Vector2 direction)
            {
                return direction != default(Vector2) ? Vector2.Angle(Direction, direction) : 0;
            }

            #if UNITY_EDITOR

            public void DrawGizmos()
            {
                Gizmos.DrawCube(ShiftedRect.center, ShiftedRect.size);
                Gizmos.DrawWireCube(ShiftedRect.center, ShiftedRect.size);
            }

            #endif
        }
    }
}