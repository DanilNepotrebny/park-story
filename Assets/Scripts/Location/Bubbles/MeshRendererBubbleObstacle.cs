﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Location.Bubbles
{
    public class MeshRendererBubbleObstacle : BaseBubbleObstacle
    {
        [SerializeField, RequiredField] private Renderer _meshRenderer;

        public override Rect Borders => _borders;
        private Rect _borders => _meshRenderer.bounds.ToRect();
    }
}