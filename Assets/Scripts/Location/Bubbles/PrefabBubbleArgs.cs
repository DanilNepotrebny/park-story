﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Location.Bubbles
{
    [Serializable]
    public class PrefabBubbleArgs : BaseBubbleArgs<RectTransform>
    {
        public PrefabBubbleArgs(
            Transform followTarget,
            RectTransform content,
            Vector2 preferedDirection,
            IEnumerable<BaseBubbleObstacle> ignoreList,
            Vector2 shiftPosition = default(Vector2))
            : base(followTarget, content, preferedDirection, ignoreList, shiftPosition)
        {
        }
    }
}