﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Location.Bubbles
{
    public class TransformBubbleObstacle : BaseBubbleObstacle
    {
        [SerializeField] private Transform _center;
        [SerializeField] private Rect _borders = new Rect(Vector2.zero, Vector2.one);

        public override Rect Borders => _borders.WithCenter(_centerPosition + _borders.position);
        private Vector2 _centerPosition => _center != null ? _center.position : transform.position;

        protected void OnValidate()
        {
            if (_center == null)
            {
                _center = transform;
            }
        }
    }
}