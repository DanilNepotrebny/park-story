﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using DreamTeam.UI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Location.Bubbles
{
    [RequireComponent(typeof(Text))]
    public class TextBubbleContentPresenter : MonoBehaviour, IPresenter<TextBubbleArgs>
    {
        [Inject] private LocalizationSystem _localizationSystem;

        public void Initialize(TextBubbleArgs model)
        {
            GetComponent<Text>().text = _localizationSystem.Localize(model.Content, this);
        }
    }
}
