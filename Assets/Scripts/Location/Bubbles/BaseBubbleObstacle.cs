﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Bubbles
{
    public abstract class BaseBubbleObstacle : MonoBehaviour, IBubbleObstacle
    {
        [Inject] private BubblesPositionSystem _bubbleSystem;

        public abstract Rect Borders { get; }

        protected void OnEnable()
        {
            _bubbleSystem.RegisterObstacle(this);
        }

        protected void OnDisable()
        {
            _bubbleSystem.UnregisterObstacle(this);
        }

        protected void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(Borders.center, Borders.size);
        }
    }
}