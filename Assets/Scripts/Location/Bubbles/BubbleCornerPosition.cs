﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Location.Bubbles
{
    [RequireComponent(typeof(RectTransform))]
    public class BubbleCornerPosition : MonoBehaviour
    {
        [SerializeField, RequiredField, ReadOnly] private Component _bubbleComponent;
        [SerializeField, RequiredField] private RectTransform _cornerTransform;

        private RectTransform _rectTransform => transform as RectTransform;
        private IBubbleHint _bubble => _bubbleComponent as IBubbleHint;

        protected void Update()
        {
            if (_cornerTransform != null && _bubble != null)
            {
                _cornerTransform.position = _rectTransform.GetWorldBounds().ClosestPoint(_bubble.FollowPosition);
            }
        }

        protected void Reset()
        {
            _bubbleComponent = GetComponentInParent<IBubbleHint>() as Component;
        }

        protected void OnValidate()
        {
            if (_bubbleComponent != null)
            {
                _bubbleComponent = _bubble as Component;
                Assert.IsNotNull(
                        _bubbleComponent,
                        $"{nameof(_bubbleComponent)} must implement {nameof(IBubbleHint)}"
                    );
            }
        }
    }
}