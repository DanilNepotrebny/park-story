﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.UI;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Bubbles
{
    public abstract class BaseBubbleHint<TBubbleArgs, TContent> : BaseModelHint<TBubbleArgs>, IBubbleHint
        where TBubbleArgs : BaseBubbleArgs<TContent>
    {
        [Inject] private BubblesPositionSystem _bubblesSystem;

        private Transform _followTarget;
        private IBubbleObstacle _followObstacle;
        public List<IBubbleObstacle> _childObstaclesCached;
        private IEnumerable<IBubbleObstacle> _ignoreList;
        private Vector2 _previousPosition;
        private Vector2 _preferedDirection;
        private Vector2 _shiftPosition;

        public Vector2 CurrentPosition => WorldToRectPosition(transform.position);
        public Vector2 FollowPosition => _followTarget ? WorldToRectPosition(_followTarget.position) : Vector2.zero;
        private RectTransform _rectTransform => transform as RectTransform;
        private Rect _followRect => _followObstacle?.Borders ?? default(Rect);

        private ICollection<IBubbleObstacle> _childObstacles
        {
            get
            {
                if (_childObstaclesCached == null || !Application.isPlaying)
                {
                    _childObstaclesCached = new List<IBubbleObstacle>(GetComponentsInChildren<IBubbleObstacle>(true));
                }

                return _childObstaclesCached;
            }
        }

        private Rect _currentBorders
        {
            get
            {
                float xMin = float.MaxValue;
                float yMin = float.MaxValue;
                float xMax = float.MinValue;
                float yMax = float.MinValue;

                foreach (IBubbleObstacle obstacle in _childObstacles)
                {
                    xMin = Mathf.Min(xMin, obstacle.Borders.xMin);
                    yMin = Mathf.Min(yMin, obstacle.Borders.yMin);
                    xMax = Mathf.Max(xMax, obstacle.Borders.xMax);
                    yMax = Mathf.Max(yMax, obstacle.Borders.yMax);
                }

                return Rect.MinMaxRect(xMin, yMin, xMax, yMax);
            }
        }

        public event Action BubblePositionChanged;

        public override void Show(TBubbleArgs model, Transform parentTransform)
        {
            base.Show(model, parentTransform);

            _followTarget = model.FollowTarget;
            _preferedDirection = model.PreferedDirection;
            _followObstacle = _followTarget?.GetComponentInChildren<IBubbleObstacle>();
            _ignoreList = _childObstacles.Union(model.IgnoreList);
            _shiftPosition = model.ShiftPosition;

            Canvas.willRenderCanvases += OnWillRenderCanvases;
        }

        public override void Hide()
        {
            base.Hide();
            Canvas.willRenderCanvases -= OnWillRenderCanvases;
            _previousPosition = default(Vector2);
            _preferedDirection = default(Vector2);
        }

        protected void OnDestroy()
        {
            Canvas.willRenderCanvases -= OnWillRenderCanvases;
        }

        protected void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireCube(_currentBorders.center, _currentBorders.size);

            Gizmos.DrawSphere((Vector2)transform.position, 0.15f);
            Gizmos.DrawSphere(_currentBorders.center, 0.15f);
            Gizmos.DrawLine((Vector2)transform.position, _currentBorders.center);
        }

        private void OnWillRenderCanvases()
        {
            if (_followTarget != null && IsActive)
            {
                Rect borders = _currentBorders.WithCenter(FollowPosition);
                _bubblesSystem.CalculateBordersPosition(ref borders, _ignoreList, _followRect, _preferedDirection);

                transform.position = new Vector3(
                        borders.center.x + _shiftPosition.x,
                        borders.center.y + _shiftPosition.y,
                        transform.position.z
                    );

                if (_previousPosition != CurrentPosition)
                {
                    _previousPosition = CurrentPosition;
                    BubblePositionChanged?.Invoke();
                }
            }
        }

        private Vector2 WorldToRectPosition(Vector3 worldPosition)
        {
            Camera cam = Camera.main;
            Vector3 position = cam.WorldToScreenPoint(worldPosition);
            RectTransformUtility.ScreenPointToWorldPointInRectangle(_rectTransform, position, cam, out position);
            return position;
        }
    }
}