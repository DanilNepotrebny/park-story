﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Location.Bubbles
{
    [Serializable]
    public class BaseBubbleArgs<TContent>
    {
        [SerializeField, RequiredField] private Transform _followTarget;
        [SerializeField, RequiredField] private TContent _content;
        [SerializeField] private Vector2 _preferedDirection;
        [SerializeField, RequiredField] private List<BaseBubbleObstacle> _ignoreList;
        [SerializeField] private Vector2 _shiftPosition;

        public Transform FollowTarget => _followTarget;
        public TContent Content => _content;
        public Vector2 PreferedDirection => _preferedDirection;
        public IEnumerable<BaseBubbleObstacle> IgnoreList => _ignoreList;
        public Vector2 ShiftPosition => _shiftPosition;

        public BaseBubbleArgs(
            Transform followTarget,
            TContent content,
            Vector2 preferedDirection,
            IEnumerable<BaseBubbleObstacle> ignoreList,
            Vector2 shiftPosition = default(Vector2))
        {
            _followTarget = followTarget;
            _content = content;
            _preferedDirection = preferedDirection;
            _ignoreList = new List<BaseBubbleObstacle>(ignoreList);
            _shiftPosition = shiftPosition;
        }
    }
}