﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Location.Bubbles
{
    [RequireComponent(typeof(RectTransform))]
    public class RectTransformBubbleObstacle : BaseBubbleObstacle
    {
        public override Rect Borders => _borders;
        private RectTransform _rectTransform => transform as RectTransform;
        private Rect _borders => _rectTransform.GetWorldBounds().ToRect();
    }
}