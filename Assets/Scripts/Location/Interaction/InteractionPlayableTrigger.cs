﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Location.Characters;
using DreamTeam.Mirroring;
using DreamTeam.Mirroring.Resolvers;
using UnityEngine;
using UnityEngine.Playables;
using Zenject;
using Random = UnityEngine.Random;

namespace DreamTeam.Location.Interaction
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(PlayableDirector))]
    [RequireComponent(typeof(PlayableDirectorMirrorsResolver))]
    public class InteractionPlayableTrigger : InteractionTriggerBase
    {
        [SerializeField] private List<TriggerData> _triggerData;

        [Inject] private MirrorObjectsRegistrator _registrator;

        private PlayableDirector _cachedDirector = null;
        private PlayableDirectorMirrorsResolver _cachedResolver = null;
        private List<TriggerData> _sortedTriggerData;
        private bool _isInterrupted = false;

        private PlayableDirector _director
        {
            get
            {
                if (_cachedDirector == null)
                {
                    _cachedDirector = GetComponent<PlayableDirector>();
                }

                return _cachedDirector;
            }
        }

        private PlayableDirectorMirrorsResolver _resolver
        {
            get
            {
                if (_cachedResolver == null)
                {
                    _cachedResolver = GetComponent<PlayableDirectorMirrorsResolver>();
                }

                return _cachedResolver;
            }
        }

        public override void Interrupt()
        {
            _isInterrupted = true;

            if (_director.state == PlayState.Playing)
            {
                _director.Stop();
            }
        }

        public override IEnumerator Interact(Character character)
        {
            _isInterrupted = false;
            if (!_resolver.IsResolved)
            {
                _resolver.Resolve(_registrator, false);
            }

            _director.Stop();
            _director.timeUpdateMode = DirectorUpdateMode.Manual;
            _director.playableAsset = GetTimeline();
            _director.Play();

            PlayableGraph graph = _director.playableGraph;
            if (graph.IsValid())
            {
                double duration = _director.duration;
                while (duration > 0f && !_isInterrupted)
                {
                    float dt = Time.deltaTime;
                    graph.Evaluate(dt);
                    duration -= dt;
                    yield return null;
                }
            }

            _director.Stop();
        }

        protected void Awake()
        {
            _sortedTriggerData = _triggerData.OrderBy(data => data.Weight).ToList();
        }

        private PlayableAsset GetTimeline()
        {
            float sum = _sortedTriggerData.Sum(data => data.Weight);
            float roll = Random.Range(0f, sum);

            PlayableAsset selected = _sortedTriggerData[_sortedTriggerData.Count - 1].Asset;
            foreach (var data in _sortedTriggerData)
            {
                if (roll < data.Weight)
                {
                    selected = data.Asset;
                    break;
                }

                roll -= data.Weight;
            }

            return selected;
        }

        [Serializable]
        private struct TriggerData
        {
            public PlayableAsset Asset;
            public int Weight;
        }
    }
}