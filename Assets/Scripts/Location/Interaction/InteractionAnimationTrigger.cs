﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Location.Characters;
using UnityEngine;

namespace DreamTeam.Location.Interaction
{
    public class InteractionAnimationTrigger : InteractionTriggerBase
    {
        [SerializeField] private string _animationTriggerName;
        [SerializeField] private float _triggerWaitingTime = 1f;

        private bool _isInterrupted = false;

        public override void Interrupt()
        {
            _isInterrupted = true;
        }

        public override IEnumerator Interact(Character character)
        {
            _isInterrupted = false;
            character.Presenter.SetTrigger(_animationTriggerName);

            double duration = _triggerWaitingTime;
            while (duration > 0f && !_isInterrupted)
            {
                duration -= Time.deltaTime;
                yield return null;
            }
        }
    }
}
