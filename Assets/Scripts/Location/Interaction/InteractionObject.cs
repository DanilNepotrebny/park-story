﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using DreamTeam.Location.Characters;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Interaction
{
    public class InteractionObject : MonoBehaviour
    {
        [SerializeField] private CharacterData _interactingCharacterData;

        [Inject]
        private List<InteractionTriggerBase> _triggers;

        private InteractionTriggerBase _currentTrigger;

        public CharacterData InteractingCharacterData => _interactingCharacterData;

        public void Interrupt()
        {
            if (_currentTrigger != null)
            {
                _currentTrigger.Interrupt();
            }

            _currentTrigger = null;
        }

        public IEnumerator Interact(Character character)
        {
            List<InteractionTriggerBase> triggersCopy = new List<InteractionTriggerBase>(_triggers);
            while (triggersCopy.Count > 0)
            {
                _currentTrigger = triggersCopy[0];
                triggersCopy.RemoveAt(0);
                yield return _currentTrigger.Interact(character);
            }

            _currentTrigger = null;
        }

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<InteractionTriggerBase>().FromComponentSibling();
        }
    }
}