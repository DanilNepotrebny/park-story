﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Interaction
{
    public class InteractionObjectsCollection : MonoBehaviour
    {
        private List<InteractionObject> _interactionObjects;

        [Inject]
        private void Contruct(List<InteractionObject> interactionObjects)
        {
            _interactionObjects = interactionObjects;
        }

        public List<InteractionObject> InteractionObjects =>
            _interactionObjects
                .Where(interactionObject => interactionObject.gameObject.activeInHierarchy)
                .ToList();

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<InteractionObject>().FromComponentInChildren(null, true);
        }
    }
}