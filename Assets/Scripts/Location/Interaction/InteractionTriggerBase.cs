﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Location.Characters;
using UnityEngine;

namespace DreamTeam.Location.Interaction
{
    [RequireComponent(typeof(InteractionObject))]
    public abstract class InteractionTriggerBase : MonoBehaviour
    {
        public abstract void Interrupt();
        public abstract IEnumerator Interact(Character character);
    }
}