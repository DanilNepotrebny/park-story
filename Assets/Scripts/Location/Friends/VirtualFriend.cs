﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Friends
{
    [Serializable]
    public partial class VirtualFriend : ISynchronizable
    {
        [SerializeField, RequiredField] private CharacterData _character;
        [SerializeField] private Sprite _unlockedPortrait;
        [SerializeField] private Sprite _lockedPortrait;

        [Inject] private VirtualFriendsUnlockSystem _unlockSystem;

        private bool _isLocked = true;

        public CharacterData Character => _character;
        public Sprite Portrait => _isLocked ? _lockedPortrait : _unlockedPortrait;
        public string Name => _isLocked ? _unlockSystem.LockedFriendName : _character.Name;
        public bool IsLocked => _isLocked;

        public void Unlock()
        {
            if (_isLocked)
            {
                _isLocked = false;
            }
        }

        public void Sync(State state)
        {
            state.SyncBool("islocked", ref _isLocked, true);
        }
    }
}