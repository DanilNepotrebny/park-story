﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Debug;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Friends
{
    public partial class VirtualFriendsUnlockSystem
    {
        [Inject] private CheatPanel _cheatPanel;

        private static GUISkin _cheatGUISkin;

        protected void Start()
        {
            _cheatPanel.AddDrawer(CheatPanel.Category.VirtualFriends, OnDrawCheatGUI);
        }

        private void OnDestroy()
        {
            _cheatPanel.RemoveDrawer(OnDrawCheatGUI);
        }

        private void OnDrawCheatGUI(Rect rect)
        {
            GUISkin skin = GetGUISkin();
            GUILayout.BeginVertical(GUILayout.ExpandWidth(true));

            Rect drawRect = rect;
            foreach (VirtualFriend friend in _syncedFriends.Values)
            {
                var drawable = friend as ICheatPanelDrawable;

                drawRect.height = drawable.GetGUIHeight(drawRect, skin);
                drawable.DrawGUI(drawRect, skin);
                drawRect.y = drawRect.yMax;
            }

            GUILayout.EndVertical();
        }

        private GUISkin GetGUISkin()
        {
            if (_cheatGUISkin == null)
            {
                _cheatGUISkin = Instantiate(GUI.skin) as GUISkin;

                int baseFontSize = GUI.skin.button.fontSize;
                int fontSize = (int)(baseFontSize * 2);

                _cheatGUISkin.label.fontSize = baseFontSize;
                _cheatGUISkin.label.fixedHeight = fontSize;

                _cheatGUISkin.button.fontSize = baseFontSize;
                _cheatGUISkin.button.fixedHeight = fontSize;
            }

            return _cheatGUISkin;
        }
    }
}