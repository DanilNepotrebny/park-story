﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Friends
{
    public class VirtualFriendUnlocker : MonoBehaviour, IPresenter<CharacterData>
    {
        [Inject] private VirtualFriendsUnlockSystem _virtualFriends;

        public void Initialize(CharacterData model)
        {
            _virtualFriends[model]?.Unlock();
        }
    }
}