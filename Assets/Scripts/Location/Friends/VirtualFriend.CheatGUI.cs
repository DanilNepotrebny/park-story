﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Debug;
using UnityEngine;

namespace DreamTeam.Location.Friends
{
    public partial class VirtualFriend : ICheatPanelDrawable
    {
        void ICheatPanelDrawable.DrawGUI(Rect rect, GUISkin skin)
        {
            GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));

            GUIContent labelContent = new GUIContent(_character?.name, _unlockedPortrait?.texture);
            GUILayout.Label(labelContent, skin.label, GUILayout.ExpandWidth(false), GUILayout.MaxWidth(400));

            string isLockedLabel = _isLocked ? "Locked" : "Unlocked";
            _isLocked = GUILayout.Toggle(_isLocked, isLockedLabel, skin.button, GUILayout.ExpandWidth(false));
            GUILayout.FlexibleSpace();

            GUILayout.EndHorizontal();
        }

        float ICheatPanelDrawable.GetGUIHeight(Rect rect, GUISkin skin)
        {
            return skin.label.CalcHeight(new GUIContent(_character?.name), rect.width);
        }
    }
}