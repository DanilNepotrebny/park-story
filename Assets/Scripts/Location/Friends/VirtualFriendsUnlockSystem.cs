﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using DreamTeam.Localization;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Friends
{
    public partial class VirtualFriendsUnlockSystem : MonoBehaviour, ISynchronizable, IEnumerable<VirtualFriend>, IInitable
    {
        [SerializeField, LocalizationKey] private string _lockedFriendName;
        [SerializeField] private List<VirtualFriend> _friends = new List<VirtualFriend>();

        [Inject] private SaveGroup _saveGroup;

        private Dictionary<string, VirtualFriend> _syncedFriends = new Dictionary<string, VirtualFriend>();

        public string LockedFriendName => _lockedFriendName;
        public VirtualFriend this[CharacterData character] => _syncedFriends[character.name];

        [Inject]
        private void Construct(DiContainer diContainer)
        {
            _friends.ForEach(diContainer.Inject);
        }

        public void Sync(State state)
        {
            state.SyncObjectDictionary("friends", ref _syncedFriends);
        }

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("VirtualFriendsUnlockSystem", this);
            FillSyncedFriends();
        }

        private void FillSyncedFriends()
        {
            _syncedFriends.Clear();
            foreach (VirtualFriend friend in _friends)
            {
                if (!_syncedFriends.ContainsKey(friend.Character.name))
                {
                    _syncedFriends.Add(friend.Character.name, friend);
                }
            }
        }

        public IEnumerator<VirtualFriend> GetEnumerator()
        {
            return _syncedFriends.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _syncedFriends.Values.GetEnumerator();
        }
    }
}