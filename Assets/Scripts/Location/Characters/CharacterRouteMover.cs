﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Location.Routing;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Characters
{
    [RequireComponent(typeof(IMovable))]
    [RequireComponent(typeof(IPositionTransformer))]
    public class CharacterRouteMover : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _movingAngularSpeed;
        [SerializeField] private float _turningAngularSpeed;
        [SerializeField] private float _maxRotationInMoving;
        [SerializeField] private bool _debugDrawRoute = false;

        [Inject] private IMovable _movable;
        [Inject] private IPositionTransformer _positionTransformer;

        private IRoute _route;

        private int _currentTargetIdx = -1;
        
        private bool _isMovementActive;
        private bool _isMoving;
        private bool _isTurning;

        private float _speedFactor = 1.0f;

        public float SpeedFactor
        {
            get { return _speedFactor; }
            set
            {
                if (!Mathf.Approximately(_speedFactor, value))
                {
                    _speedFactor = value;
                    SpeedFactorChanged?.Invoke(value);
                }
            }
        }

        public bool IsMovementActive
        {
            get { return _isMovementActive; }

            private set
            {
                bool old = _isMovementActive;
                _isMovementActive = value;
                if (old != value)
                {
                    MovementActiveChanged?.Invoke(value);
                }

                if (!_isMovementActive)
                {
                    IsMoving = false;
                    IsTurning = false;
                }
            }
        }

        public bool IsMoving
        {
            get { return IsMovementActive && _isMoving; }

            private set
            {
                bool oldMoving = _isMoving;
                _isMoving = value;
                if (oldMoving != value)
                {
                    MovingChanged?.Invoke(value);
                }
            }
        }

        public bool IsTurning
        {
            get { return IsMovementActive && _isTurning; }

            private set
            {
                bool oldTurning = _isTurning;
                _isTurning = value;
                if (oldTurning != value)
                {
                    TurningChanged?.Invoke(value);
                }
            }
        }
        
        private Vector3 _targetPosition => _currentTargetIdx < _route.PointsCount ?
            GetTransformed(_route[_currentTargetIdx]) :
            Vector3.zero;

        private Vector3 _targetOffset => _targetPosition - _movable.Position;
        private Vector3 _targetDirection => _targetOffset.normalized;

        public event Action<bool> MovementActiveChanged;
        public event Action<bool> MovingChanged;
        public event Action<bool> TurningChanged;
        public event Action<float> SpeedFactorChanged;
        
        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<IMovable>().FromComponentSibling();
            yield return container.Bind<IPositionTransformer>().FromComponentSibling();
        }

        public void MoveByPath(IRoute route, float duration = 0f)
        {
            if (IsMovementActive)
            {
                Stop();
            }

            _currentTargetIdx = 0;
            _route = route;
            AlignByCurrentTarget();

            SpeedFactor = CalculateSpeedFactor(route, duration);

            IsMovementActive = true;

            ProceedToNextTarget();
        }

        public void Stop()
        {
            _currentTargetIdx = -1;
            _route = null;

            IsMovementActive = false;
        }

        protected void Update()
        {
            if (!IsMovementActive)
            {
                return;
            }

            IsMoving = RotationInMovingExcessAngle(_movable.Direction, _targetDirection) <= 0;
            IsTurning = _movable.Direction != _targetDirection;

            if (IsTurning)
            {
                float angularSpeed = IsMoving ? _movingAngularSpeed : _turningAngularSpeed;
                float delta = SpeedFactor * angularSpeed * Time.deltaTime;

                float currentAngle = Vector3.SignedAngle(Vector3.right, _movable.Direction, Vector3.forward);
                float targetAngle = Vector3.SignedAngle(Vector3.right, _targetDirection, Vector3.forward);
                float angle = Mathf.MoveTowardsAngle(currentAngle, targetAngle, delta * Mathf.Rad2Deg);

                _movable.Direction = Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.right;
            }

            if (IsMoving)
            {
                Vector3 oldPosition = _movable.Position;
                Vector3 offset = SpeedFactor * _speed * Time.deltaTime * _targetDirection;

                _movable.Position += offset;

                Vector3 targetOffset = _targetPosition - oldPosition;

                if (offset.sqrMagnitude > targetOffset.sqrMagnitude || offset == targetOffset)
                {
                    ProceedToNextTarget();
                }
            }
        }

        #if DEBUG
        protected void OnDrawGizmos()
        {
            if (_debugDrawRoute && _route != null)
            {
                Gizmos.color = Color.red;
                for (int i = 0; i < _route.PointsCount; i++)
                {
                    Gizmos.DrawCube(_route[i], Vector3.one * 0.2f);
                }

                for (int i = 0; i < _route.PointsCount - 1; i++)
                {
                    UnityEngine.Debug.DrawLine(_route[i], _route[i + 1], Color.red);
                }
            }
        }
        #endif // DEBUG

        private void ProceedToNextTarget()
        {
            if (_currentTargetIdx + 1 >= _route.PointsCount)
            {
                AlignByCurrentTarget();
                Stop();
                return;
            }

            _currentTargetIdx++;
        }
        
        private float CalculateSpeedFactor(IRoute route, float duration)
        {
            if (Mathf.Approximately(duration, 0))
            {
                return 1.0f;
            }

            float basePathTime = 0;

            Vector3 direction = _movable.Direction;

            for (int i = 1; i < route.PointsCount; ++i)
            {
                Vector3 targetDirection = GetTransformed(route[i]) - GetTransformed(route[i - 1]);
                float distance = targetDirection.magnitude;

                targetDirection.Normalize();

                basePathTime += distance / _speed;

                float excessAngle = RotationInMovingExcessAngle(direction, targetDirection);
                if (excessAngle > 0)
                {
                    basePathTime += excessAngle * Mathf.Deg2Rad / _turningAngularSpeed;
                }

                direction = targetDirection;
            }

            return basePathTime / duration;
        }

        private float RotationInMovingExcessAngle(Vector3 currentDirection, Vector3 targetDirection)
        {
            float angleDifference = Vector3.Angle(currentDirection, targetDirection);
            return angleDifference - _maxRotationInMoving;
        }
        
        private Vector3 GetTransformed(Vector3 position)
        {
            _positionTransformer?.Transform(ref position);
            return position;
        }

        private void AlignByCurrentTarget()
        {
            _movable.Position = GetTransformed(_route[_currentTargetIdx]);
        }
    }
}