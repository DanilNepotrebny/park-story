﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Location.Isometry;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Characters
{
    [RequireComponent(typeof(IsometryMovable))]
    [RequireComponent(typeof(IsometrySorter))]
    [RequireComponent(typeof(CharacterRouteMover))]
    [RequireComponent(typeof(CharacterRouteFinder))]
    [RequireComponent(typeof(CharacterAI))]
    public class Character : MonoBehaviour
    {
        public const string MainCharacterId = "MainCharacter";

        [SerializeField] private CharacterData _characterData;

        [Inject] private CharacterPresenter _presenter;
        [Inject] private CharacterRouteMover _routeMover;
        [Inject] private CharacterRouteFinder _routeFinder;

        public CharacterData Data => _characterData;
        public CharacterPresenter Presenter => _presenter;
        public CharacterRouteMover RouteMover => _routeMover;
        public CharacterRouteFinder RouteFinder => _routeFinder;

        public event Action<bool> MovementActiveChanged
        {
            add { _routeMover.MovementActiveChanged += value; }
            remove { _routeMover.MovementActiveChanged -= value; }
        }

        public event Action<bool> MovingChanged
        {
            add { _routeMover.MovingChanged += value; }
            remove { _routeMover.MovingChanged -= value; }
        }

        public event Action<bool> TurningChanged
        {
            add { _routeMover.TurningChanged += value; }
            remove { _routeMover.TurningChanged -= value; }
        }

        public event Action<float> SpeedFactorChanged
        {
            add { _routeMover.SpeedFactorChanged += value; }
            remove { _routeMover.SpeedFactorChanged -= value; }
        }

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<CharacterPresenter>().FromComponentInChildren(null, true);
            yield return container.Bind<CharacterRouteMover>().FromComponentSibling();
            yield return container.Bind<CharacterRouteFinder>().FromComponentSibling();
        }
    }
}