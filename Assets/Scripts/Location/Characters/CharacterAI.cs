﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.GameControls;
using DreamTeam.Location.Interaction;
using DreamTeam.Location.Routing;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Location.Characters
{
    [RequireComponent(typeof(Character))]
    public class CharacterAI : MonoBehaviour
    {
        [SerializeField] private bool _goesToCursor;
        [SerializeField] private float _timeoutMin;
        [SerializeField] private float _timeoutMax;
        [SerializeField] private Vector2 _initialPosition;
        [SerializeField] private AnimationCurve _memoryCurve;
        [SerializeField] [Range(0, 10)] private int _memorySize;
        [SerializeField] private float _nextInteractionObjectMinDistance;

        private Character _character;
        private List<InteractionObjectsCollection> _interactionObjectsCollections;

        private List<InteractionObject> _previousTargets = new List<InteractionObject>();
        private InteractionObject _currentTarget;
        private Coroutine _actionsCoroutine;

        [Inject]
        private void Construct(
                List<InteractionObjectsCollection> availableCollections,
                GameControlsSystem gameControlsSystem)
        {
            _character = GetComponent<Character>();
            _interactionObjectsCollections = availableCollections;
            transform.position = _initialPosition;
        }

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<Character>().FromComponentSibling();
        }

        protected void OnEnable()
        {
            Assert.IsNull(_actionsCoroutine);
            if (_actionsCoroutine == null)
            {
                _actionsCoroutine = StartCoroutine(ActionsCycle());
            }
        }

        protected void OnDisable()
        {
            if (_character.RouteMover.IsMovementActive)
            {
                _character.RouteMover.Stop();
            }
            else if (_currentTarget != null)
            {
                _currentTarget.Interrupt();
            }

            Assert.IsNotNull(_actionsCoroutine);
            if (_actionsCoroutine != null)
            {
                StopCoroutine(_actionsCoroutine);
                _actionsCoroutine = null;
            }
        }

        private IEnumerator ActionsCycle()
        {
            while (true)
            {
                #if DEBUG
                {
                    if (_goesToCursor)
                    {
                        yield return UpdateDebugMoveToTouch();
                    }
                    else
                    {
                        yield return Idle();
                        yield return Moving();
                        yield return Interacting();
                    }
                }
                #else
                {
                    yield return Idle();
                    yield return Moving();
                    yield return Interacting();
                }
                #endif // DEBUG
            }
        }

        private IEnumerator Idle()
        {
            float duration = Random.Range(_timeoutMin, _timeoutMax);
            yield return new WaitForSeconds(duration);
        }

        private IEnumerator Moving()
        {
            _currentTarget = FindNextObject();
            if (_currentTarget != null)
            {
                IRoute route;
                if (_character.RouteFinder.Find(_currentTarget.transform.position, out route))
                {
                    _character.RouteMover.MoveByPath(route);
                    yield return new WaitWhile(() => _character.RouteMover.IsMovementActive);
                }
                else
                {
                    UnityEngine.Debug.LogError($"Cannot find path to interaction object \"{_currentTarget.gameObject.name}\"!", gameObject);
                    _currentTarget = null;
                }
            }
        }

        private IEnumerator Interacting()
        {
            if (_currentTarget != null)
            {
                yield return _currentTarget.Interact(_character);
                _previousTargets.Insert(0, _currentTarget);
                _currentTarget = null;

                while (_previousTargets.Count > _memorySize)
                {
                    _previousTargets.RemoveAt(_previousTargets.Count - 1);
                }
            }
        }

        private InteractionObject FindNextObject()
        {
            List<KeyValuePair<InteractionObject, float>> selected = _interactionObjectsCollections
                .SelectMany(collection => collection.InteractionObjects)
                .Where(FilterObject)
                .Select(SelectObject)
                .ToList();

            InteractionObject result = null;
            if (selected.Count > 0)
            {
                result = RandomUtils.RandomElement(selected);
            }

            return result;
        }

        private bool FilterObject(InteractionObject obj)
        {
            return
                obj.gameObject.activeInHierarchy &&
                obj.InteractingCharacterData == _character.Data &&
                Vector3.Distance(_character.transform.position, obj.transform.position) > _nextInteractionObjectMinDistance;
        }

        private KeyValuePair<InteractionObject, float> SelectObject(InteractionObject interactionObject)
        {
            float weight = 1f / Vector3.Distance(_character.transform.position, interactionObject.transform.position);
            int index = _previousTargets.IndexOf(interactionObject);
            if (index >= 0)
            {
                float scale = _memoryCurve.Evaluate(index);
                weight *= scale;
            }

            return new KeyValuePair<InteractionObject, float>(interactionObject, weight);
        }

        #if DEBUG
        private IEnumerator UpdateDebugMoveToTouch()
        {
            if (Input.GetMouseButtonUp(1))
            {
                Vector2 targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                IRoute route;
                if (_character.RouteFinder.Find(targetPosition, out route))
                {
                    _character.RouteMover.MoveByPath(route);
                }
                else
                {
                    UnityEngine.Debug.LogError($"Cannot find path to position \"{targetPosition}\"!", gameObject);
                }
            }

            yield return null;
        }
        #endif // DEBUG
    }
}