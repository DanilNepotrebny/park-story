﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Location.Isometry;
using UnityEngine;

namespace DreamTeam.Location.Characters
{
    [RequireComponent(typeof(IsometryDirectionPresenter))]
    public class CharacterPresenter : MonoBehaviour
    {
        [SerializeField] private string _movingParameter = "IsMoving";
        [SerializeField] private string _turningParameter = "IsTurning";
        [SerializeField] private string _movingSpeedFactorParameter = "MovingSpeedMultiplier";
        [SerializeField] private Animator _animator;
        [SerializeField] private Color _tint;

        private Color? _currentMaterialsTint = null;
        private Character _character;
        private List<Renderer> _renderers = new List<Renderer>();
        private MaterialPropertyBlock _cachedPropertyBlock;

        private MaterialPropertyBlock _propertyBlock
        {
            get
            {
                if (_cachedPropertyBlock == null)
                {
                    _cachedPropertyBlock = new MaterialPropertyBlock();
                }

                return _cachedPropertyBlock;
            }
        }

        public Color TintColor
        {
            get { return _tint; }

            set
            {
                if (_tint != value)
                {
                    _tint = value;
                    UpdateMaterials(false);
                }
            }
        }

        protected void OnEnable()
        {
            GetComponentsInChildren<Renderer>(true, _renderers);
            UpdateMaterials(true);
        }

        protected void OnDisable()
        {
            _renderers.Clear();
        }

        protected void OnTransformChildrenChanged()
        {
            GetComponentsInChildren<Renderer>(true, _renderers);
            UpdateMaterials(true);
        }

        protected void OnValidate()
        {
            if (!Application.isPlaying)
            {
                GetComponentsInChildren<Renderer>(true, _renderers);
                UpdateMaterials(true);
            }
        }

        public void SetTrigger(string triggerName)
        {
            _animator.SetTrigger(triggerName);
        }

        protected void Start()
        {
            _character = GetComponentInParent<Character>();
            _character.MovingChanged += OnMovingChanged;
            _character.TurningChanged += OnTurningChanged;
            _character.SpeedFactorChanged += OnSpeedFactorChanged;
        }

        protected void Update()
        {
            UpdateMaterials(false);
        }

        private void UpdateMaterials(bool force)
        {
            if (_currentMaterialsTint != TintColor || force)
            {
                _currentMaterialsTint = TintColor;

                foreach (var meshRenderer in _renderers)
                {
                    meshRenderer.GetPropertyBlock(_propertyBlock);
                    _propertyBlock.SetColor("_Tint", TintColor);
                    meshRenderer.SetPropertyBlock(_propertyBlock);
                }
            }
        }

        private void OnSpeedFactorChanged(float speedFactor)
        {
            _animator.SetFloat(_movingSpeedFactorParameter, speedFactor);
        }

        private void OnMovingChanged(bool isMoving)
        {
            _animator.SetBool(_movingParameter, isMoving);
        }

        private void OnTurningChanged(bool isMoving)
        {
            _animator.SetBool(_turningParameter, isMoving);
        }
    }
}