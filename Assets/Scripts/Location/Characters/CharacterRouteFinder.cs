﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Routing;
using UnityEngine;

namespace DreamTeam.Location.Characters
{
    public class CharacterRouteFinder : MonoBehaviour
    {
        private NavMeshRoute _path;

        public bool Find(Vector2 targetPosition, out IRoute route)
        {
            bool result = _path.Find(new Vector3(transform.position.x, transform.position.y, 0f), targetPosition);

            route = null;
            if (result)
            {
                route = _path;
            }

            return result;
        }

        protected void Awake()
        {
            _path = new NavMeshRoute();
        }
    }
}