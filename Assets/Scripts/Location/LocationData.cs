﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Location
{
    [CreateAssetMenu(menuName = "Location/Location data")]
    public class LocationData : ScriptableObject
    {
        [SerializeField] private LocationRestoredData _restoredData;

        public LocationRestoredData RestoredData => _restoredData;
    }
}