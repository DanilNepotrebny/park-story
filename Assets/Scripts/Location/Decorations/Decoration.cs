﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Linq;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.Nodes;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Location.Decorations
{
    /// <summary>
    /// Handle of object on some location that can be customized during the game progress.
    /// State of such object is saved between user sessions. To obtain the state use <see cref="DecorationSystem.ReserveDecorationState"/>.
    /// </summary>
    [CreateAssetMenu(menuName = "Location/Decoration")]
    public class Decoration : ScriptableObject, ISynchronizer<DecorationSkin>
    {
        [SerializeField] private DecorationSkin[] _skins;
        [SerializeField] private DecorationSkin _initialSkin;
        [SerializeField] private bool _isSkinShared;
        [SerializeField] private bool _isPresentedIndividually;
        [SerializeField] private Sprite _overviewIcon;

        public DecorationSkin[] Skins => _skins;
        public DecorationSkin InitialSkin => _initialSkin;
        public bool IsSkinShared => _isSkinShared;
        public bool IsPresentedIndividually => _isPresentedIndividually;
        public Sprite OverviewIcon => _overviewIcon;

        public State CreateState(Instantiator instantiator)
        {
            return instantiator.Instantiate<State>(this);
        }

        Node ISynchronizer<DecorationSkin>.Serialize(DecorationSkin skin)
        {
            return new ValueNode(skin.name);
        }

        DecorationSkin ISynchronizer<DecorationSkin>.Deserialize(Node node)
        {
            string skinName = ((ValueNode)node).GetString();
            return _skins.First(s => s.name == skinName);
        }

        #region Inner types

        /// <summary>
        /// Runtime data and logic of the <see cref="Decoration"/> object.
        /// </summary>
        public class State : ISynchronizable
        {
            private DecorationSkin _currentSkin;

            public Decoration Decoration { get; }

            public DecorationSkin CurrentSkin
            {
                get { return _currentSkin; }
                set
                {
                    Assert.IsNotNull(value, "Can't set skin that is null.");
                    if (value != null && _currentSkin != value)
                    {
                        _currentSkin = value;
                        SkinChanged?.Invoke();
                    }
                }
            }

            public event Action SkinChanged;

            public State(Decoration decoration)
            {
                Decoration = decoration;

                _currentSkin = Decoration.InitialSkin;
            }

            void ISynchronizable.Sync(Synchronization.States.State state)
            {
                state.SyncObject("skin", ref _currentSkin, null, Decoration);
            }
        }

        #endregion
    }
}