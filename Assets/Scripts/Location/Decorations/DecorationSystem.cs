﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Decorations
{
    /// <summary>
    /// Contains and provides runtime state of <see cref="Decoration"/> objects.
    /// </summary>
    public class DecorationSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        private Dictionary<string, Decoration.State> _states = new Dictionary<string, Decoration.State>();

        [Inject] private Instantiator _instantiator;
        [Inject] private SaveGroup _saveGroup;

        public IEnumerable<Decoration.State> States => _states.Values;

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("decorations", this);
        }

        public Decoration.State ReserveDecorationState(Decoration decoration, string userId)
        {
            string stateId = decoration.name;

            if (!decoration.IsSkinShared)
            {
                stateId += "_" + userId;
            }

            if (_states.ContainsKey(stateId))
            {
                return _states[stateId];
            }

            Decoration.State state = decoration.CreateState(_instantiator);
            _states.Add(stateId, state);
            return state;
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncObjectDictionary("states", ref _states);
        }
    }
}