﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DreamTeam.Location.Decorations.UI;
using DreamTeam.UI;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Location.Decorations
{
    public class DecorationCustomizationActivator : MonoBehaviour, IPresenter<DecorationSkin>
    {
        [Inject] private DecorationSystem _decorationSystem;
        [Inject] private DecorationCustomizationSystem _customizationSystem;
        [Inject] private DecorationOverviewPanel _overviewPanel;

        private DecorationSkin _decorationSkin;

        void IPresenter<DecorationSkin>.Initialize(DecorationSkin model)
        {
            _decorationSkin = model;
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void ActivateCustomization()
        {
            IEnumerable<Decoration.State> states = _decorationSystem.States
                .Where(s => s.Decoration.Skins.Contains(_decorationSkin));

            int statesCount = states.Count();
            Assert.IsTrue(statesCount > 0, $"There are no decorations with the skin {_decorationSkin.name}.");

            if (statesCount == 1)
            {
                _customizationSystem.ActivateCustomization(states.First(), null, false, _decorationSkin);
            }
            else
            {
                _overviewPanel.Show(states, _decorationSkin);
            }
        }
    }
}