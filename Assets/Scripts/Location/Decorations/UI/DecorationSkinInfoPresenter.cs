﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using DreamTeam.UI;
using DreamTeam.UI.DataProviders;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Location.Decorations.UI
{
    public class DecorationSkinInfoPresenter : MonoBehaviour, IPresenter<DecorationSkin>
    {
        [SerializeField] private Text _title;
        [SerializeField] private GameObject _priceInfo;
        [SerializeField] private GameObject _boughtText;
        [SerializeField] private GameObject _priceGameObject;

        [Inject] private DecorationCustomizationSystem _customizationSystem;
        [Inject] private LocalizationSystem _localizationSystem;

        public void Initialize(DecorationSkin decorationSkin)
        {
            _title.text = _localizationSystem.Localize(((IDescriptionProvider)decorationSkin).Description, decorationSkin);

            _priceInfo.SetActive(!_customizationSystem.IsInitialCustomization);

            bool isNeedToBuy = !_customizationSystem.HasEnoughSkinCount(decorationSkin);

            _boughtText.SetActive(!isNeedToBuy);
            _priceGameObject.SetActive(isNeedToBuy);

            if (!_customizationSystem.IsInitialCustomization && isNeedToBuy)
            {
                GetComponent<PresenterHub>().Initialize(
                        _customizationSystem.GetSkinPrice(decorationSkin)
                    );
            }
        }
    }
}
