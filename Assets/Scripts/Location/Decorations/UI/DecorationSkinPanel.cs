﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Reflection;
using DreamTeam.UI;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Decorations.UI
{
    public class DecorationSkinPanel : MonoBehaviour
    {
        [SerializeField] private DecorationSkinButtonPresenter _skinButtonPrefab;
        [SerializeField] private DecorationSkinHint _skinInfoHint;
        [SerializeField] private SimpleAnimation _simpleAnimation;
        [SerializeField] private GameObject _buttonsPanel;
        [SerializeField] private GameObject _closeButton;

        [Inject] private Instantiator _instantiator;
        [Inject] private DecorationCustomizationSystem _customizationSystem;

        private Decoration.State _currentDecorationState;

        private List<DecorationSkinButtonPresenter> _skinButtons = new List<DecorationSkinButtonPresenter>();

        public bool IsShown => _currentDecorationState != null;

        public event Action<DecorationSkin> SkinSelected;
        public event Action SkinAccepted;
        public event Action Hiding;

        public void ShowDecoration(Decoration.State decorationState, bool couldBeCancelled)
        {
            if (decorationState == _currentDecorationState)
            {
                return;
            }

            if (_currentDecorationState != null)
            {
                ResetDecoration();
            }
            else
            {
                _simpleAnimation.gameObject.SetActive(true);
            }

            _closeButton.gameObject.SetActive(couldBeCancelled);

            _currentDecorationState = decorationState;

            foreach (DecorationSkin skin in _currentDecorationState.Decoration.Skins)
            {
                if (skin.IsLocked)
                {
                    continue;
                }

                DecorationSkinButtonPresenter button = _instantiator.Instantiate(_skinButtonPrefab);
                button.transform.SetParent(_buttonsPanel.transform, false);
                button.Selected += OnSkinButtonSelected;
                button.GetComponent<PresenterHub>().Initialize(skin);

                _skinButtons.Add(button);
            }

            _simpleAnimation.Play(SimpleAnimationType.Show);
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void Hide()
        {
            Hiding?.Invoke();

            _skinInfoHint.Hide();

            _simpleAnimation.Play(SimpleAnimationType.Hide);
        }

        public void OnSkinButtonSelected(DecorationSkinButtonPresenter buttonPresenter)
        {
            SkinSelected?.Invoke(buttonPresenter.DecorationSkin);

            bool isNeedToBuy = !_customizationSystem.HasEnoughSkinCount(buttonPresenter.DecorationSkin);
            if (isNeedToBuy && !_customizationSystem.IsInitialCustomization)
            {
                _skinInfoHint.Show(buttonPresenter.DecorationSkin, buttonPresenter.transform);
            }
            else
            {
                _skinInfoHint.Hide();
            }
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void AcceptSkin()
        {
            SkinAccepted?.Invoke();

            Hide();
        }

        protected void Awake()
        {
            _simpleAnimation.AnimationFinished += OnAnimationFinished;
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            if (SimpleAnimationType.Hide == animationType)
            {
                ResetDecoration();

                _simpleAnimation.gameObject.SetActive(false);
            }
        }

        private void ResetDecoration()
        {
            _skinInfoHint.Hide();

            _currentDecorationState = null;

            foreach (DecorationSkinButtonPresenter button in _skinButtons)
            {
                button.gameObject.Dispose();
            }

            _skinButtons.Clear();
        }
    }
}