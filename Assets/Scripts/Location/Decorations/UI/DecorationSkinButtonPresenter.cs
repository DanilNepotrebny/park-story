﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.UI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Location.Decorations.UI
{
    public class DecorationSkinButtonPresenter : MonoBehaviour, IPresenter<DecorationSkin>
    {
        [SerializeField] private Toggle _toggle;
        [SerializeField] private Image _icon;

        [Inject] private DecorationCustomizationSystem _customizationSystem;

        public DecorationSkin DecorationSkin { get; private set; }

        public event Action<DecorationSkinButtonPresenter> Selected;

        public void Initialize(DecorationSkin skin)
        {
            DecorationSkin = skin;
            _icon.sprite = skin.Icon;

            _toggle.isOn = _customizationSystem.PreviewSkin == DecorationSkin;
        }

        protected void OnSelectionChanged(bool isSelected)
        {
            if (isSelected)
            {
                Selected?.Invoke(this);
            }
        }

        protected void Awake()
        {
            _toggle.onValueChanged.AddListener(OnSelectionChanged);
        }
    }
}