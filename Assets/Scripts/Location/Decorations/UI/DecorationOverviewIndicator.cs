﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Reflection;
using DreamTeam.UI.SimpleAnimating;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Location.Decorations.UI
{
    [RequireComponent(typeof(SimpleAnimation))]
    public class DecorationOverviewIndicator : MonoBehaviour
    {
        [SerializeField] private Image _icon;

        private ISimpleAnimationPlayer _animPlayer;
        private Action _finishCallback;
        private DecorationPresenter _presenter;

        public void Initialize(DecorationPresenter presenter)
        {
            _presenter = presenter;
            _icon.sprite = presenter.DecorationState.Decoration.OverviewIcon;
            transform.position = presenter.IndicatorPosition;
        }

        public void Show()
        {
            gameObject.SetActive(true);
            _animPlayer.Play(SimpleAnimationType.Show);
        }

        public void Hide(Action finishCallback)
        {
            _finishCallback = finishCallback;
            _animPlayer.Play(SimpleAnimationType.Hide);
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void ActivateCustomization()
        {
            _presenter.ActivateCustomization();
        }

        protected void Awake()
        {
            _animPlayer = GetComponent<SimpleAnimation>();
            _animPlayer.AnimationFinished += OnAnimationFinished;
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType type)
        {
            if (type == SimpleAnimationType.Hide)
            {
                gameObject.SetActive(false);
                _finishCallback?.Invoke();
            }
        }
    }
}