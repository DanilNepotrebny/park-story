﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.UI;

namespace DreamTeam.Location.Decorations.UI
{
    public class DecorationSkinHint : BaseModelHint<CountedItem>
    {
    }
}