﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI.SimpleAnimating;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Location.Decorations.UI
{
    [RequireComponent(typeof(SimpleAnimation))]
    [RequireComponent(typeof(RectTransform))]
    public class DecorationLongPressIndicator : MonoBehaviour
    {
        [SerializeField] private Image _progressImage;

        private ISimpleAnimationPlayer _animPlayer;
        private bool _isHidden;
        private float _fillTime;
        private float _fillProgressTimer;
        private RectTransform _cachedRectTransform;

        private RectTransform _rectTransform
        {
            get
            {
                if (_cachedRectTransform == null)
                {
                    _cachedRectTransform = GetComponent<RectTransform>();
                }

                return _cachedRectTransform;
            }
        }

        public void SetFillTime(float fillTime)
        {
            _fillTime = fillTime;
        }

        public void Show(Vector2 screenPosition)
        {
            _isHidden = false;
            _fillProgressTimer = _fillTime;
            _rectTransform.anchoredPosition3D = CalculatePosition(screenPosition);

            gameObject.SetActive(true);
            _animPlayer.Play(SimpleAnimationType.Show);
        }

        public void Hide()
        {
            if (_isHidden)
            {
                return;
            }

            _isHidden = true;
            _fillProgressTimer = 0;

            if (gameObject.activeInHierarchy)
            {
                _animPlayer.Play(SimpleAnimationType.Hide);
            }
        }

        protected void Awake()
        {
            _animPlayer = GetComponent<SimpleAnimation>();
            _animPlayer.AnimationFinished += OnAnimationFinished;
        }

        protected void Update()
        {
            if (_fillProgressTimer <= 0)
            {
                return;
            }

            _fillProgressTimer -= Time.deltaTime;
            _progressImage.fillAmount = Mathf.Clamp(_fillTime - _fillProgressTimer, 0, _fillTime) / _fillTime;
        }

        private Vector3 CalculatePosition(Vector2 screenPosition)
        {
            Vector2 position;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform.parent as RectTransform, screenPosition, Camera.main, out position);
            return new Vector3(position.x, position.y, 0f);
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType type)
        {
            if (type == SimpleAnimationType.Hide)
            {
                gameObject.SetActive(false);
            }
        }
    }
}