﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DreamTeam.CameraControllers;
using DreamTeam.GameControls;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Location.Decorations.UI
{
    [RequireComponent(typeof(SimpleAnimation))]
    public class DecorationOverviewPanel : MonoBehaviour
    {
        [SerializeField, FormerlySerializedAs("_selectionMode")]
        private GameControlsMode _controlsMode;

        [SerializeField] private DecorationOverviewIndicator _indicatorPrefab;

        [Inject] private DecorationSystem _decorationSystem;
        [Inject] private DecorationCustomizationSystem _customizationSystem;
        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private Instantiator _instantiator;
        [Inject] private LocationCameraTweener _cameraTweener;

        private ISimpleAnimationPlayer _animPlayer;
        private ActionDisposable _modeDisableHandle;

        private List<DecorationOverviewIndicator> _indicators = new List<DecorationOverviewIndicator>();
        private IEnumerable<Decoration.State> _decorationStates;

        private bool _isHiding;

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void Show()
        {
            Show(null, null);
        }

        public void Show(DecorationSkin selectedSkin)
        {
            Show(null, selectedSkin);
        }

        public void Show(IEnumerable<Decoration.State> decorationStates, DecorationSkin selectedSkin)
        {
            _isHiding = false;

            gameObject.SetActive(true);

            _customizationSystem.CustomizationActivated += OnCustomizationActivated;

            Assert.IsNull(_modeDisableHandle, "_modeDisableHandle != null");
            _modeDisableHandle = _gameControlsSystem.EnableMode(_controlsMode);

            _decorationStates = decorationStates ?? _decorationSystem.States;

            EnablePresentersOverviewMode(selectedSkin);
            AlignCamera();

            _animPlayer.Play(SimpleAnimationType.Show);
        }

        private void AlignCamera()
        {
            DecorationPresenter focusedPresenter = GetPresenters()
                .Where(p => p.IsMain)
                .OrderBy(p => (p.CameraFocusPoint - _cameraTweener.transform.position).sqrMagnitude)
                .First();

            _cameraTweener.MoveTo(focusedPresenter.CameraFocusPoint);

            if (_customizationSystem.IsInitialCustomization &&
                focusedPresenter.PresentationOrthographicSize > 0)
            {
                _cameraTweener.ZoomTo(focusedPresenter.PresentationOrthographicSize);
            }
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void Hide()
        {
            if (_isHiding)
            {
                return;
            }

            _isHiding = true;
            _customizationSystem.CustomizationActivated -= OnCustomizationActivated;

            ReleaseModeDisableHandle();

            DisablePresentersOverviewMode();

            _animPlayer.Play(SimpleAnimationType.Hide);
        }

        protected void Awake()
        {
            _animPlayer = GetComponent<SimpleAnimation>();
            _animPlayer.AnimationFinished += OnAnimationFinished;
        }

        private void OnCustomizationActivated()
        {
            Hide();
            _customizationSystem.CustomizationDeactivated += OnCustomizationDeactivated;
        }

        private void OnCustomizationDeactivated()
        {
            _customizationSystem.CustomizationDeactivated -= OnCustomizationDeactivated;
            Show();
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType type)
        {
            if (type == SimpleAnimationType.Hide)
            {
                gameObject.SetActive(false);
            }
        }

        private IEnumerable<DecorationPresenter> GetPresenters()
        {
            foreach (Decoration.State state in _decorationStates)
            {
                IEnumerable<DecorationPresenter> presenters = _customizationSystem.GetPresenters(state);
                if (presenters != null)
                {
                    foreach (DecorationPresenter presenter in presenters)
                    {
                        yield return presenter;
                    }
                }
            }
        }

        private void EnablePresentersOverviewMode(DecorationSkin selectedSkin)
        {
            foreach (DecorationPresenter presenter in GetPresenters())
            {
                presenter.SetSelectedSkin(selectedSkin);
                presenter.SetOverviewModeEnabled(true);

                if (presenter.IsMain)
                {
                    DecorationOverviewIndicator prefab = presenter.UniqueOverviewIndicatorPrefab ?? _indicatorPrefab;

                    DecorationOverviewIndicator indicator = _instantiator.Instantiate(prefab);
                    indicator.Initialize(presenter);
                    indicator.Show();

                    _indicators.Add(indicator);
                }
            }
        }

        private void DisablePresentersOverviewMode()
        {
            foreach (Decoration.State state in _decorationStates)
            {
                IEnumerable<DecorationPresenter> presenters = _customizationSystem.GetPresenters(state);
                if (presenters != null)
                {
                    foreach (DecorationPresenter presenter in presenters)
                    {
                        presenter.SetSelectedSkin(null);
                        presenter.SetOverviewModeEnabled(false);
                    }
                }
            }

            foreach (DecorationOverviewIndicator indicator in _indicators)
            {
                indicator.Hide(() => indicator.gameObject.Dispose());
            }

            _indicators.Clear();
        }

        private void ReleaseModeDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }
    }
}