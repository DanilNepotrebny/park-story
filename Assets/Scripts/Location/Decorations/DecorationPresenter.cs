﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Location.Decorations.UI;
using DreamTeam.Location.Unlockables;
using DreamTeam.Utils;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.ResourceLoading;
using TouchScript.Gestures;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Decorations
{
    [RequireComponent(typeof(LongPressGesture))]
    [RequireComponent(typeof(TapGesture))]
    [RequireComponent(typeof(Collider2D))]
    public class DecorationPresenter :
        MonoBehaviour,
        UnlockableControllerBase.IStageModificationHandler,
        IResourceLoaderHub,
        IInitable,
        IDisposable
    {
        [SerializeField] private bool _isMain;
        [SerializeField] private Transform _cameraFocusPoint;
        [SerializeField] private Transform _characterPresentationPosition;
        [SerializeField] private Transform _characterPresentationLookAt;
        [SerializeField] private Transform _indicatorPoint;
        [SerializeField] private float _referencePresentationOrthographicSize = -1f;
        [SerializeField] private DecorationOverviewIndicator _uniqueOverviewIndicatorPrefab;

        [Inject] private DecorationController _controller;
        [Inject] private DecorationCustomizationSystem _customizationSystem;
        [Inject] private List<DecorationSkinPresenter> _skinPresenters;

        private LongPressGesture _longPressGesture;
        private TapGesture _tapGesture;
        private Collider2D _collider;

        private CustomizationType _customizationType = CustomizationType.Regular;
        private DecorationSkinPresenter _activeSkinPresenter;
        private float _longPressIndicatorTimer;

        private DecorationSkin _selectedSkin;

        private List<IResourceHandle> _skinCustomizationHandles = new List<IResourceHandle>();

        public bool IsMain => _isMain;
        public Decoration.State DecorationState => _controller.State;
        public DecorationOverviewIndicator UniqueOverviewIndicatorPrefab => _uniqueOverviewIndicatorPrefab;

        public Vector3 CameraFocusPoint
        {
            get
            {
                if (_cameraFocusPoint != null)
                {
                    return _cameraFocusPoint.position;
                }

                return _collider.bounds.center;
            }
        }

        public Vector3 CharacterPresentationPosition
        {
            get
            {
                if (_characterPresentationPosition != null)
                {
                    return _characterPresentationPosition.position;
                }

                return CameraFocusPoint;
            }
        }

        public Vector3 CharacterPresentationLookAt
        {
            get
            {
                if (_characterPresentationLookAt != null)
                {
                    return _characterPresentationLookAt.position;
                }

                return CameraFocusPoint;
            }
        }

        public Vector3 IndicatorPosition
        {
            get
            {
                if (_indicatorPoint != null)
                {
                    return _indicatorPoint.position;
                }

                return CameraFocusPoint;
            }
        }

        public float PresentationOrthographicSize => _referencePresentationOrthographicSize;

        public void Init()
        {
            _collider = GetComponent<Collider2D>();

            _longPressGesture = GetComponent<LongPressGesture>();
            _longPressGesture.LongPressed += OnLongPressed;
            _longPressGesture.StateChanged += OnLongPressGestureStateChanged;
            _longPressGesture.TimeToPress = _customizationSystem.ActivationLongPressTime;

            _tapGesture = GetComponent<TapGesture>();
            _tapGesture.Tapped += OnTapped;
            _tapGesture.StateChanged += OnTapGestureStateChanged;
            _tapGesture.enabled = false;

            _controller.State.SkinChanged += OnSkinChanged;

            _customizationSystem.RegisterDecorationPresenter(this);
        }

        public void Dispose()
        {
            _longPressGesture.LongPressed -= OnLongPressed;
            _longPressGesture.StateChanged -= OnLongPressGestureStateChanged;
            _tapGesture.Tapped -= OnTapped;

            _controller.State.SkinChanged -= OnSkinChanged;
        }

        public void OnPreviewSkinChanged()
        {
            RefreshSkins(false);
        }

        public void SetCustomizationModeActive(bool isCustomizationActive)
        {
            if (isCustomizationActive)
            {
                foreach (DecorationSkinPresenter skinPresenter in _skinPresenters)
                {
                    var resourceLoaderHub = skinPresenter.GetComponent<IResourceLoaderHub>();
                    if (resourceLoaderHub != null)
                    {
                        _skinCustomizationHandles.Add(resourceLoaderHub.Load());
                    }
                }
            }
            else
            {
                foreach (IResourceHandle handle in _skinCustomizationHandles)
                {
                    handle.Dispose();
                }

                _skinCustomizationHandles.Clear();
            }

            foreach (DecorationSkinPresenter skinPresenter in _skinPresenters)
            {
                skinPresenter.SetCustomizationModeActive(isCustomizationActive);
            }
        }

        public BaseAsyncProcess PreloadCustomizationSkins()
        {
            return new GroupAsyncProcess(
                    _skinPresenters
                        .Select(
                                skinPresenter => skinPresenter.GetComponent<IResourceLoaderHub>()?.Preload()
                            )
                        .ToArray()
                );
        }

        public void SetOverviewModeEnabled(bool isEnabled)
        {
            _tapGesture.enabled = isEnabled;
        }

        public void SetSelectedSkin(DecorationSkin selectedSkin)
        {
            _selectedSkin = selectedSkin;
        }

        public void SetAppearTrigger()
        {
            if (_activeSkinPresenter == null)
            {
                RefreshSkins(true);
            }

            _activeSkinPresenter.Appear();
        }

        public void SetCustomizationType(CustomizationType customizationType)
        {
            _customizationType = customizationType;
        }

        public void ActivateCustomization()
        {
            _customizationSystem.ActivateCustomization(DecorationState, this, _customizationType == CustomizationType.Initial, _selectedSkin);
        }

        void UnlockableControllerBase.IStageModificationHandler.HandleStageUpdate(int previousStage, int currentStage)
        {
            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                if (currentStage != previousStage)
                {
                    if (_activeSkinPresenter == null)
                    {
                        RefreshSkins(true);
                    }
                    
                    _activeSkinPresenter.Appear();
                }
            }
        }

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<DecorationController>().FromComponentInParents(false, true);
            yield return container.Bind<DecorationSkinPresenter>().FromComponentInChildren(null, true);
        }

        protected void Start()
        {
            if (_activeSkinPresenter == null)
            {
                RefreshSkins(true);
            }
        }

        protected void Update()
        {
            if (_longPressIndicatorTimer <= 0)
            {
                return;
            }

            _longPressIndicatorTimer -= Time.deltaTime;

            if (_longPressIndicatorTimer <= 0)
            {
                _customizationSystem.ActivationLongPressIndicator.Show(_longPressGesture.ActivePointers[0].Position);
            }
        }

        private void OnLongPressGestureStateChanged(object sender, GestureStateChangeEventArgs e)
        {
            switch (e.State)
            {
                case Gesture.GestureState.Possible:
                    _longPressIndicatorTimer = _customizationSystem.ActivationIndicatorShowDelay;
                    break;
                case Gesture.GestureState.Failed:
                    ((Gesture)sender).TryCancelGesture(Camera.main.transform);
                    break;
            }

            if (e.State != Gesture.GestureState.Possible)
            {
                _longPressIndicatorTimer = 0;
                _customizationSystem.ActivationLongPressIndicator.Hide();
            }
        }

        private void OnTapGestureStateChanged(object sender, GestureStateChangeEventArgs e)
        {
            switch (e.State)
            {
                case Gesture.GestureState.Failed:
                    ((Gesture)sender).TryCancelGesture(Camera.main.transform);
                    break;
            }
        }

        private void OnSkinChanged()
        {
            RefreshSkins(false);
        }

        private void OnLongPressed(object sender, EventArgs e)
        {
            ActivateCustomization();
        }

        private void OnTapped(object sender, EventArgs e)
        {
            ActivateCustomization();
        }

        private void RefreshSkins(bool isInitial)
        {
            DecorationSkin currentSkin = _customizationSystem.GetStatePreviewSkin(DecorationState);

            if (currentSkin == null)
            {
                currentSkin = DecorationState.CurrentSkin;
            }

            foreach (DecorationSkinPresenter skinPresenter in _skinPresenters)
            {
                bool isActive = skinPresenter.DecorationSkin == currentSkin;
                if (isActive)
                {
                    _activeSkinPresenter = skinPresenter;
                }

                if (skinPresenter.isActiveAndEnabled != isActive)
                {
                    skinPresenter.SetActive(isActive, isInitial);
                }
            }
        }

        private IResourceLoaderHub GetResourceLoaderHubForCurrentSkin()
        {
            return _skinPresenters
                .First(p => p.DecorationSkin == DecorationState.CurrentSkin)
                .GetComponent<IResourceLoaderHub>();
        }

        #region IPreloader

        BaseAsyncProcess IResourceLoader.Preload()
        {
            return GetResourceLoaderHubForCurrentSkin()?.Preload();
        }

        public IResourceHandle Load()
        {
            return GetResourceLoaderHubForCurrentSkin()?.Load();
        }

        #endregion

        #region Inner types

        public enum CustomizationType
        {
            Initial,
            Regular
        }

        #endregion
    }
}