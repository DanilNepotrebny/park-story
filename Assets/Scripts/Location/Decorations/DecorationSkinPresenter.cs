﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Location.Decorations
{
    public class DecorationSkinPresenter : MonoBehaviour
    {
        [SerializeField] private DecorationSkin _decorationSkin;
        [SerializeField] private AnimatorStateWatcher _animatorStateWatcher;
        [SerializeField] private string _previewActiveParameterName;
        [SerializeField] private string _pingTriggerName;
        [SerializeField] private string _appearTriggerName;

        private bool _isCustomizationModeActive;

        public DecorationSkin DecorationSkin => _decorationSkin;
        private bool _isActive => gameObject.activeSelf;

        public void SetActive(bool isActive, bool isInitial)
        {
            bool wasActive = _isActive;
            gameObject.SetActive(isActive);

            if (!isInitial && _isActive && (_isCustomizationModeActive || !wasActive))
            {
                _animatorStateWatcher.Animator.SetTrigger(_pingTriggerName);
            }

            UpdateCustomizationModeParameters();
        }

        public void Appear()
        {
            _animatorStateWatcher.Animator.SetTrigger(_appearTriggerName);
        }

        public void SetCustomizationModeActive(bool isCustomizationModeActive)
        {
            if (_isCustomizationModeActive != isCustomizationModeActive)
            {
                _isCustomizationModeActive = isCustomizationModeActive;

                UpdateCustomizationModeParameters();
            }
        }

        private void UpdateCustomizationModeParameters()
        {
            if (gameObject.activeSelf)
            {
                _animatorStateWatcher.Animator.SetBool(
                        _previewActiveParameterName,
                        _isCustomizationModeActive
                    );
            }
        }
    }
}