﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.CameraControllers;
using DreamTeam.GameControls;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Prices;
using DreamTeam.Location.Decorations.UI;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Location.Decorations
{
    public class DecorationCustomizationSystem : MonoBehaviour
    {
        [SerializeField] private GameControlsMode _regularControlsMode;
        [SerializeField] private GameControlsMode _initialControlsMode;
        [SerializeField] private DecorationLongPressIndicator _activationLongPressIndicator;
        [SerializeField] private float _activationLongPressTime;
        [SerializeField] private float _activationIndicatorShowDelay;

        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private LocationCameraTweener _cameraTweener;
        [Inject] private DecorationSkinPanel _skinPanel;
        [Inject] private ProductPackInstanceContainer _productPackInstanceContainer;

        private bool _isCustomizationDeactivating;

        private DecorationPresenter _initiatorPresenter;
        private Decoration.State _stateForCustomization;

        private GameControlsMode _selectionMode;

        private Dictionary<Decoration.State, List<DecorationPresenter>> _presentersMap =
            new Dictionary<Decoration.State, List<DecorationPresenter>>();

        private ActionDisposable _modeDisableHandle;

        public DecorationSkin PreviewSkin { get; private set; }

        public bool IsCustomizationActive => _stateForCustomization != null;
        public bool IsInitialCustomization { get; private set; }

        public DecorationLongPressIndicator ActivationLongPressIndicator => _activationLongPressIndicator;
        public float ActivationLongPressTime => _activationLongPressTime;
        public float ActivationIndicatorShowDelay => _activationIndicatorShowDelay;

        public event Action CustomizationActivated;
        public event Action CustomizationDeactivated;

        public void RegisterDecorationPresenter(DecorationPresenter presenter)
        {
            List<DecorationPresenter> presenters = GetPreviewPresenters(presenter.DecorationState);
            if (presenters == null)
            {
                presenters = new List<DecorationPresenter>();
                _presentersMap.Add(presenter.DecorationState, presenters);
            }

            presenters.Add(presenter);
        }

        public DecorationSkin GetStatePreviewSkin(Decoration.State decorationState)
        {
            if (_stateForCustomization == decorationState)
            {
                return PreviewSkin;
            }

            return null;
        }

        public void ActivateCustomization(
                Decoration.State state,
                DecorationPresenter initiator,
                bool isInitialCustomization,
                DecorationSkin selectedSkin = null
            )
        {
            if (_stateForCustomization == state)
            {
                return;
            }

            if (_stateForCustomization != null)
            {
                if (IsInitialCustomization)
                {
                    return;
                }

                DeactivateCustomization();
            }

            _initiatorPresenter = initiator;
            _stateForCustomization = state;
            IsInitialCustomization = isInitialCustomization;

            _selectionMode = IsInitialCustomization ? _initialControlsMode : _regularControlsMode;

            Assert.IsNull(_modeDisableHandle, "_modeDisableHandle != null");
            _modeDisableHandle = _gameControlsSystem.EnableMode(_selectionMode);

            foreach (DecorationPresenter presenter in GetPreviewPresenters())
            {
                presenter.SetCustomizationModeActive(true);
            }

            DecorationSkin previewSkin = selectedSkin ??
                _stateForCustomization.CurrentSkin ??
                _stateForCustomization.Decoration.Skins[0];

            SetPreviewSkin(previewSkin);

            _skinPanel.ShowDecoration(_stateForCustomization, !isInitialCustomization);

            CustomizationActivated?.Invoke();
        }

        public void SetPreviewSkin(DecorationSkin skin)
        {
            if (PreviewSkin != null)
            {
                ProductPack.Instance productPackInstance = _productPackInstanceContainer.GetInstance(
                        PreviewSkin.DefaultProductPack
                    );
                productPackInstance.Given -= OnSkinGiven;
            }

            PreviewSkin = skin;

            if (PreviewSkin != null)
            {
                AlignCameraAtDecorations();
            }

            foreach (DecorationPresenter presenter in GetPreviewPresenters())
            {
                presenter.OnPreviewSkinChanged();
            }
        }

        public bool HasEnoughSkinCount(DecorationSkin decorationSkin)
        {
            if (decorationSkin == _stateForCustomization.CurrentSkin)
            {
                return true;
            }

            return decorationSkin.Count > 0;
        }

        public Price GetSkinPrice(DecorationSkin decorationSkin)
        {
            return decorationSkin.DefaultProductPack.Price;
        }

        public IEnumerable<DecorationPresenter> GetPresenters(Decoration.State state)
        {
            if (_presentersMap.ContainsKey(state))
            {
                return _presentersMap[state];
            }

            return null;
        }

        protected void Awake()
        {
            _skinPanel.SkinSelected += SetPreviewSkin;
            _skinPanel.SkinAccepted += OnSkinAccepted;
            _skinPanel.Hiding += OnPanelHiding;

            ActivationLongPressIndicator.SetFillTime(_activationLongPressTime - _activationIndicatorShowDelay);
        }

        protected void OnDestroy()
        {
            ReleaseModeDisableHandle();
        }

        private void OnPanelHiding()
        {
            DeactivateCustomization();
        }

        private void OnSkinAccepted()
        {
            Assert.IsNotNull(_stateForCustomization, "Can't apply skin. Preview mode for decoration is not enabled.");
            Assert.IsNotNull(PreviewSkin, "Can't apply skin. Preview skin is not set.");

            if (_stateForCustomization != null && PreviewSkin != null)
            {
                if (IsInitialCustomization || HasEnoughSkinCount(PreviewSkin))
                {
                    ApplySkinAndDeactivate();
                }
                else
                {
                    ProductPack.Instance productPackInstance = _productPackInstanceContainer.GetInstance(
                            PreviewSkin.DefaultProductPack
                        );
                    productPackInstance.Given += OnSkinGiven;
                    productPackInstance.TryPurchase(GamePlace.Location);
                }
            }
        }

        private void OnSkinGiven(ProductPack.Instance pack, GamePlace gamePlace)
        {
            ApplySkinAndDeactivate();
        }

        private void ApplySkinAndDeactivate()
        {
            if (!IsInitialCustomization &&
                PreviewSkin != _stateForCustomization.CurrentSkin &&
                !PreviewSkin.TrySpend(1, GamePlace.Location))
            {
                UnityEngine.Debug.LogError("Can't spend skin '{_previewSkin.name}'.");
                return;
            }

            if (_stateForCustomization.CurrentSkin != null && _stateForCustomization.CurrentSkin != PreviewSkin)
            {
                _stateForCustomization.CurrentSkin.TryReceive(1, ReceivingType.Other, GamePlace.Location);
            }

            _stateForCustomization.CurrentSkin = PreviewSkin;

            DeactivateCustomization();
        }

        private void DeactivateCustomization()
        {
            if (!IsCustomizationActive || _isCustomizationDeactivating)
            {
                return;
            }

            _isCustomizationDeactivating = true;

            foreach (DecorationPresenter presenter in GetPreviewPresenters())
            {
                presenter.SetCustomizationModeActive(false);
            }

            SetPreviewSkin(null);

            Action finishCallback = () =>
            {
                _stateForCustomization = null;

                ReleaseModeDisableHandle();
                _selectionMode = null;

                _isCustomizationDeactivating = false;

                CustomizationDeactivated?.Invoke();
            };

            finishCallback();
        }

        private List<DecorationPresenter> GetPreviewPresenters(Decoration.State state = null)
        {
            if (state == null)
            {
                state = _stateForCustomization;
            }

            List<DecorationPresenter> presenters;
            if (_presentersMap.TryGetValue(state, out presenters))
            {
                return presenters;
            }

            return null;
        }

        private void AlignCameraAtDecorations()
        {
            bool hasFocusPoint = false;
            Vector3 focusPoint = Vector3.zero;
            float customOrthographicSize = -1f;

            IEnumerable<DecorationPresenter> presenters = GetPreviewPresenters();
            if (presenters != null)
            {
                if (_stateForCustomization.Decoration.IsPresentedIndividually)
                {
                    DecorationPresenter focusedPresenter = _initiatorPresenter ?? presenters.FirstOrDefault(p => p.IsMain);
                    if (focusedPresenter != null)
                    {
                        hasFocusPoint = true;
                        focusPoint = focusedPresenter.CameraFocusPoint;

                        if (IsInitialCustomization &&
                            focusedPresenter.PresentationOrthographicSize > 0)
                        {
                            customOrthographicSize = focusedPresenter.PresentationOrthographicSize;
                        }
                    }
                }
                else
                {
                    hasFocusPoint = true;
                    focusPoint = CalculateCameraFocusPoint(presenters);

                    if (IsInitialCustomization)
                    {
                        customOrthographicSize = CalculateCameraOrthographicSize(presenters);
                    }
                }
            }

            if (hasFocusPoint)
            {
                _cameraTweener.MoveTo(focusPoint);
            }

            if (customOrthographicSize > 0f)
            {
                _cameraTweener.ZoomTo(customOrthographicSize);
            }
        }

        private void ReleaseModeDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }

        private Vector3 CalculateCameraFocusPoint(IEnumerable<DecorationPresenter> presenters)
        {
            Vector3 result = Vector3.zero;

            int presentersCount = 0;
            Vector3 cameraFocusPoint = Vector3.zero;
            foreach (DecorationPresenter presenter in presenters)
            {
                cameraFocusPoint += presenter.CameraFocusPoint;
                ++presentersCount;
            }

            if (presentersCount > 0)
            {
                result = cameraFocusPoint / presentersCount;
            }

            return result;
        }

        private float CalculateCameraOrthographicSize(IEnumerable<DecorationPresenter> presenters)
        {
            float result = -1f;

            foreach (DecorationPresenter presenter in presenters)
            {
                if (presenter.PresentationOrthographicSize > result)
                {
                    result = presenter.PresentationOrthographicSize;
                }
            }

            return result;
        }

        public BaseAsyncProcess PrepareCustomization(Decoration.State state)
        {
            return new GroupAsyncProcess(
                    GetPresenters(state).Select(p => p.PreloadCustomizationSkins()).ToArray()
                );
        }
    }
}