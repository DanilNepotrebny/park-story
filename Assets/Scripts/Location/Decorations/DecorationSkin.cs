﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.UI.DataProviders;
using UnityEngine;
using UnityEngine.Serialization;

namespace DreamTeam.Location.Decorations
{
    public partial class DecorationSkin : CountedItem, IUnlockNotificationIconProvider, IDescriptionProvider
    {
        [SerializeField, FormerlySerializedAs("_title")] private string _description;
        [SerializeField] private Sprite _unlockNotificationIcon;
        
        string IDescriptionProvider.Description => _description;

        Sprite IUnlockNotificationIconProvider.UnlockNotificationIcon => _unlockNotificationIcon;
    }
}