﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

namespace DreamTeam.Location.Decorations
{
    public class DecorationController : MonoBehaviour
    {
        [SerializeField] private Decoration _decoration;

        [Inject] private DecorationSystem _decorationsSystem;

        public Decoration.State State { get; private set; }

        public Decoration Decoration => _decoration;
        public DecorationPresenter Presenter { get; private set; }

        [Inject]
        protected void Constuct(DecorationSystem decorationsSystem)
        {
            _decorationsSystem = decorationsSystem;

            State = _decorationsSystem.ReserveDecorationState(
                    Decoration,
                    name
                );

            Presenter = GetComponentInChildren<DecorationPresenter>(true);
        }
    }
}