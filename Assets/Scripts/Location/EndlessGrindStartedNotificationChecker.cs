﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.EndlessGrind;
using DreamTeam.Popups;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location
{
    public class EndlessGrindStartedNotificationChecker : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private SceneReference _notifiactionPopupScene;

        [Inject] private EndlessGrindSystem _endlessGrind;
        [Inject] private SaveGroup _saveGroup;
        [Inject] private PopupQueueSystem _popupSystem;

        private bool _shouldBeShown = true;

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("EndlessGrindStartedNotifiacation", this);
        }

        protected void Awake()
        {
            _endlessGrind.EventUpdating += OnEventUpdating;
            _endlessGrind.EventStoped += OnEventStoped;
        }

        protected void OnDestroy()
        {
            _endlessGrind.EventUpdating -= OnEventUpdating;
            _endlessGrind.EventStoped += OnEventStoped;
        }

        private void OnEventUpdating()
        {
            if (_shouldBeShown && _endlessGrind.IsGrindAvailable)
            {
                _popupSystem.Enqueue(_notifiactionPopupScene.Name);
            }

            _shouldBeShown = !_endlessGrind.IsGrindAvailable;
        }

        private void OnEventStoped()
        {
            _shouldBeShown = true;
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncBool("shouldBeShown", ref _shouldBeShown);
        }
    }
}