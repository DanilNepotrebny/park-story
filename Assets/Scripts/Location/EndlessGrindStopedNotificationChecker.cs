﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.EndlessGrind;
using DreamTeam.Inventory.Items;
using DreamTeam.Popups;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location
{
    public class EndlessGrindStopedNotificationChecker : MonoBehaviour
    {
        [SerializeField] private SceneReference _notificationPopup;
        [SerializeField, RequiredField] private CountedItem _tickets;

        [Inject] private PopupQueueSystem _popupQueueSystem;
        [Inject] private EndlessGrindSystem _endlessGrind;

        protected void Awake()
        {
            _endlessGrind.EventStoped += OnEventStoped;
        }

        protected void OnDestroy()
        {
            _endlessGrind.EventStoped -= OnEventStoped;
        }

        private void OnEventStoped()
        {
            EndlessGrindChest maxChest = FindMaxChest();

            _popupQueueSystem.Enqueue(
                    _notificationPopup.Name,
                    new SingleEndlessGrindChestPopup.Args { Chest = maxChest }
                );
        }

        private EndlessGrindChest FindMaxChest()
        {
            int maxPrice = int.MinValue;
            EndlessGrindChest maxChest = null;
            foreach (EndlessGrindChest chest in _endlessGrind.Chests)
            {
                var quantity = chest.ProductPack.Price as ICountedItemQuantity;
                if (quantity != null &&
                    quantity.Item == _tickets &&
                    quantity.Count.InRange(maxPrice, _tickets.Count))
                {
                    maxPrice = quantity.Count;
                    maxChest = chest;
                }
            }

            return maxChest;
        }
    }
}