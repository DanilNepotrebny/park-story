﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Levels.Rewards;
#endif
using DreamTeam.Popups;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location
{
    public class EndlessGrindRewardsChecker : MonoBehaviour
    {
        [SerializeField] private SceneReference _popupScene;

        #if !NO_MATCH3

        [Inject] private EndlessGrindLevelPackRewardsController _rewardsController;
        [Inject] private PopupQueueSystem _popupSystem;
        [Inject] private LevelSystem _levelSystem;

        protected void Awake()
        {
            _rewardsController.GivingRewards += OnGivingRewards;
        }

        protected void OnDestroy()
        {
            _rewardsController.GivingRewards -= OnGivingRewards;
        }

        protected void Start()
        {
            CheckRewards();
        }

        private void CheckRewards()
        {
            if (!_rewardsController.TryGiveRewards() &&
                _levelSystem.LastLoadedController != null &&
                _levelSystem.LastLoadedController.Type == LevelPackControllerType.EndlessGrind)
            {
                _popupSystem.Enqueue(_popupScene.Name);
            }
        }

        private void OnGivingRewards(IDeferredInvocationHandle handle)
        {
            IDeferredInvocationHandle handleLock = handle.Lock();
            var popupArgs = new Popup.Args
            {
                ShowedCallback = handleLock.Unlock
            };
            _popupSystem.Enqueue(_popupScene.Name, popupArgs);
        }

        #endif
    }
}