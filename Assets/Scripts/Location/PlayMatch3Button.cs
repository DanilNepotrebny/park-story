﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Inventory.Prices;
using DreamTeam.Popups;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;

#endif

namespace DreamTeam.Location
{
    public class PlayMatch3Button : ShowPopupOnButtonClicked
    {
        #if !NO_MATCH3
        [SerializeField] private LevelPackControllerType _levelPackControllerType;
        [SerializeField] private SceneReference _noLevelsPopup;
        #else
        // Life-hack to make Unity inspector serialize enum field without using actual type
        [SerializeField, ReadOnly] private int _levelPackControllerType;
        #endif

        [SerializeField, RequiredField] private CountedItemPrice _match3FailurePrice;

        #if !NO_MATCH3
        [Inject] private PriceInstanceContainer _priceInstanceContainer;
        [Inject] private LevelSystem _levelSystem;
        #endif

        protected void Awake()
        {
            #if NO_MATCH3
            {
                gameObject.Dispose();
            }
            #endif
        }

        protected override void OnButtonClicked()
        {
            #if !NO_MATCH3
            {
                BaseLevelPackController controller = _levelSystem.GetActiveControllerByType(_levelPackControllerType);
                Assert.IsNotNull(controller);

                if (controller.CurrentLevel == null)
                {
                    _popupSystem.Show(_noLevelsPopup.Name);
                }
                else if (_priceInstanceContainer.GetInstance(_match3FailurePrice).CanBePayed)
                {
                    _levelSystem.SetActiveControllerByType(_levelPackControllerType);
                    base.OnButtonClicked();
                }
                else
                {
                    _match3FailurePrice.Item.ShowNotEnoughPopup(GamePlace.PlayMatch3Button);
                }
            }
            #endif
        }
    }
}