﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Localization;
using UnityEngine;

namespace DreamTeam.Location
{
    [Serializable]
    public class LocationRestoredData
    {
        [SerializeField] private Sprite _image;
        [SerializeField, LocalizationKey] private string _message;

        public Sprite Image => _image;
        public string Message => _message;
    }
}