﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using DreamTeam.Popups.Bank;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Location
{
    [RequireComponent(typeof(Button))]
    public class OpenBankButtonBehaviour : MonoBehaviour
    {
        [Inject] private PopupSystem _popupSystem;

        [SerializeField] private SceneReference _bankPopup;

        private Button _button;

        protected void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(ShowBankPopup);
        }

        private void ShowBankPopup()
        {
            Popup popup = _popupSystem.GetPopup(_bankPopup.Name);
            if (popup == null || !popup.IsShown)
            {
                _button.onClick.RemoveListener(ShowBankPopup);

                _popupSystem.Show(
                        _bankPopup.Name,
                        new BankPopup.Args
                        {
                            CollectionType = BankPopup.Args.PacksCollectionType.Default,
                            ClosedCallback = () => _button.onClick.AddListener(ShowBankPopup)
                        }
                    );
            }
        }
    }
}