﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Notifications;
using Zenject;

namespace DreamTeam.Location.ActionByLevel
{
    public class ShowNotificationsPopupByLevel : ShowPopupByLevel
    {
        [Inject] private NotificationsSystem _notificationsSystem;

        protected override bool CanShowPopup()
        {
            return !_notificationsSystem.AreNotificationsEnabled;
        }
    }
}