﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Popups;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.ActionByLevel
{
    public class ShowPopupByLevel : ActorByLevelComponent
    {
        [SerializeField] private SceneReference _popup;

        [Inject] private PopupSystem _popupSystem;

        protected sealed override IEnumerator PlayInternal()
        {
            if (CanShowPopup())
            {
                _popupSystem.Show(_popup.Name);
                while (
                    _popupSystem.GetPopup(_popup.Name) != null &&
                    _popupSystem.GetPopup(_popup.Name).IsShown)
                {
                    yield return null;
                }
            }
        }

        protected virtual bool CanShowPopup()
        {
            return true;
        }
    }
}