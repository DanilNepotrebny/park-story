﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Cutscenes;
using DreamTeam.Utils;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.Coroutines;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.ActionByLevel
{
    public class CutscenePlayerByLevel : ActorByLevelComponent
    {
        [SerializeField] private SceneReference _cutscene;

        [Inject] private CutsceneSystem _cutsceneSystem;

        public override BaseAsyncProcess Preload()
        {
            return _cutsceneSystem.PreloadCutscene(_cutscene);
        }

        protected override IEnumerator PlayInternal()
        {
            _cutsceneSystem.PlayCutscene(_cutscene);
            yield return new WaitForEvent<SceneReference, CutsceneSystem>(_cutsceneSystem, nameof(_cutsceneSystem.CutsceneFinished));
        }
    }
}
