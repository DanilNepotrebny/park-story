﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.GameControls;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Location.ActionByLevel
{
    public abstract class ActorByLevelComponent : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private int _levelIndex;
        [SerializeField] private GameControlsMode _mode;

        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private SaveGroup _saveGroup;

        private bool _hasPlayed;
        private ActionDisposable _modeDisableHandle;

        #if !NO_MATCH3

        public int Order => transform.GetSiblingIndex();
        public bool HasPlayed => _hasPlayed;
        public int LevelIndex => _levelIndex;

        public void Init()
        {
            _saveGroup.RegisterSynchronizable(name, this);
        }

        [CanBeNull]
        public virtual BaseAsyncProcess Preload()
        {
            return null;
        }

        public IEnumerator Play()
        {
            if (HasPlayed)
            {
                UnityEngine.Debug.LogError($"Actor {name} has played previously!", gameObject);
                yield break;
            }

            if (_mode != null)
            {
                Assert.IsNull(_modeDisableHandle, "Previous game mode disable handle has not been released");
                _modeDisableHandle = _gameControlsSystem.EnableMode(_mode);
            }

            yield return PlayInternal();

            _hasPlayed = true;
            ReleaseModeDisableHandle();
        }

        #endif // !NO_MATCH3

        protected void OnDestroy()
        {
            ReleaseModeDisableHandle();
        }

        private void ReleaseModeDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }

        protected abstract IEnumerator PlayInternal();

        void ISynchronizable.Sync(State state)
        {
            state.SyncBool(nameof(_hasPlayed), ref _hasPlayed, false);
        }
    }
}
