﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.SceneLoading;
using DreamTeam.Utils;
using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif // !NO_MATCH3

namespace DreamTeam.Location.ActionByLevel
{
    public class ActorsByLevelManager : MonoBehaviour, ISceneLoadable
    {
        #if !NO_MATCH3

        private List<ActorByLevelComponent> _actors;
        private SingleLevelPackController _levelPackController;

        private Queue<ActorByLevelComponent> _currentQueue;
        private Coroutine _actorsPlayingRoutine;

        [Inject]
        private void Construct(
                List<ActorByLevelComponent> actors,
                [Inject(Id = GlobalInstaller.MainLevelPackControllerId)] SingleLevelPackController levelPackController
            )
        {
            _actors = actors.OrderBy(actor => actor.Order).ToList();
            _levelPackController = levelPackController;
        }

        protected void Awake()
        {
            _currentQueue = new Queue<ActorByLevelComponent>(GetCurrentActors());

            if (_currentQueue.Count > 0)
            {
                _actorsPlayingRoutine = StartCoroutine(ActorsPlayingRoutine());
            }
        }

        protected void OnDestroy()
        {
            if (_actorsPlayingRoutine != null)
            {
                StopCoroutine(_actorsPlayingRoutine);
            }
        }

        private IEnumerator ActorsPlayingRoutine()
        {
            while (_currentQueue.Count > 0)
            {
                ActorByLevelComponent actor = _currentQueue.Dequeue();
                yield return actor.Play();
            }

            _actorsPlayingRoutine = null;
        }

        private IEnumerable<ActorByLevelComponent> GetCurrentActors()
        {
            return _actors.Where(
                    actor => !actor.HasPlayed && actor.LevelIndex == _levelPackController.CurrentLevelIndex
                );
        }

        #endif // !NO_MATCH3

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<ActorByLevelComponent>().FromComponentInChildren(null, true);
        }

        BaseAsyncProcess ISceneLoadable.Load()
        {
            return new GroupAsyncProcess(GetCurrentActors().Select(actor => actor.Preload()));
        }
    }
}