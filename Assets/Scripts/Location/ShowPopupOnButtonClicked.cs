﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Location
{
    public class ShowPopupOnButtonClicked : MonoBehaviour
    {
        [SerializeField] [FormerlySerializedAs("_missionBriefingPopupScene")]
        private SceneReference _showPopupScene;

        [SerializeField, RequiredField] private Button _button;
        [SerializeField] private bool _closeOtherPopups;
        [SerializeField] private bool _restoreOtherPopups;

        [Inject] protected PopupSystem _popupSystem;

        private bool _shouldClearStashedPopups;

        protected void OnEnable()
        {
            _button.onClick.AddListener(OnButtonClicked);
        }

        protected void OnDisable()
        {
            _button.onClick.RemoveListener(OnButtonClicked);
        }

        protected void OnDestroy()
        {
            if (_shouldClearStashedPopups)
            {
                _popupSystem.ClearStashedPopups();
                _shouldClearStashedPopups = false;
            }
        }

        protected void OnValidate()
        {
            if (_showPopupScene == null)
            {
                UnityEngine.Debug.LogAssertion($"Missing {nameof(_showPopupScene)} reference", this);
            }
        }

        protected virtual void OnButtonClicked()
        {
            if (_closeOtherPopups)
            {
                if (_restoreOtherPopups)
                {
                    _popupSystem.StashAll(OnOtherPopupsHidden);
                    _shouldClearStashedPopups = true;
                }
                else
                {
                    _popupSystem.HideAll(OnOtherPopupsHidden);
                }
            }
            else
            {
                _popupSystem.Show(_showPopupScene.Name);
            }
        }

        private void OnOtherPopupsHidden()
        {
            var popupArgs = new Popup.Args();
            if (_restoreOtherPopups)
            {
                popupArgs.ClosedCallback = OnPopupClosed;
            }
            _popupSystem.Show(_showPopupScene.Name, popupArgs);
        }

        private void OnPopupClosed()
        {
            _popupSystem.UnstashAll();
            _shouldClearStashedPopups = false;
        }
    }
}