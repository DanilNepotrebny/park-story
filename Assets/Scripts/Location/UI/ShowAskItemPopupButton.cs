﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.SocialNetworking;

namespace DreamTeam.Location.UI
{
    public class ShowAskItemPopupButton : ShowInventoryExchangePopupButton
    {
        protected override bool IsValidProfile(SocialProfile profile)
        {
            return ExchangeSystem.CanAskItem(profile, Reward.Item, Reward.Count);
        }
    }
}