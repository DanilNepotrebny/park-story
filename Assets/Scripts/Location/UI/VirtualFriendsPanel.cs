﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Location.Friends;
using DreamTeam.UI;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Location.UI
{
    public class VirtualFriendsPanel : MonoBehaviour
    {
        [SerializeField, RequiredField] private PresenterHub _samplePresenter;

        [Inject] private VirtualFriendsUnlockSystem _virtualFriends;
        [Inject] private Instantiator _instantiator;

        private Dictionary<VirtualFriend, PresenterHub> _presenters = new Dictionary<VirtualFriend, PresenterHub>();

        protected void OnEnable()
        {
            ClearPresenters();
            InitializePresenters();
        }

        private void InitializePresenters()
        {
            foreach (VirtualFriend friend in _virtualFriends)
            {
                PresenterHub presenter;
                if (!_presenters.TryGetValue(friend, out presenter))
                {
                    GameObject presenterObject = _instantiator.Instantiate(_samplePresenter.gameObject);
                    RectTransform presenterTransform = presenterObject.GetComponent<RectTransform>();
                    presenterTransform.SetParent(gameObject.transform);
                    presenterTransform.anchoredPosition3D = Vector3.zero;
                    presenterTransform.localScale = Vector3.one;
                    presenter = presenterObject.GetComponent<PresenterHub>();
                    _presenters.Add(friend, presenter);
                }

                presenter.Initialize(friend);
            }
        }

        private void ClearPresenters()
        {
            foreach (VirtualFriend friend in _presenters.Keys)
            {
                if (!_virtualFriends.Contains(friend))
                {
                    _presenters[friend].gameObject.Dispose();
                    _presenters.Remove(friend);
                }
            }
        }
    }
}