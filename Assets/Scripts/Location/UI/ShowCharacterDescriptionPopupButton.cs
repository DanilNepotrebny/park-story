﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using DreamTeam.UI;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Location.UI
{
    [RequireComponent(typeof(Button))]
    public class ShowCharacterDescriptionPopupButton : MonoBehaviour, IPresenter<CharacterData>
    {
        [SerializeField] private SceneReference _popupScene;
        [Inject] private PopupSystem _popupSystem;

        private CharacterData _characterData;

        private Button _button => GetComponent<Button>();

        public void Initialize(CharacterData model)
        {
            _characterData = model;
        }

        protected void Awake()
        {
            _button.onClick.AddListener(OnButtonClicked);
        }

        protected void OnDestroy()
        {
            _button.onClick.RemoveListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            var args = new CharacterDescriptionPopup.Args(_characterData);
            _popupSystem.Show(_popupScene.Name, args);
        }
    }
}