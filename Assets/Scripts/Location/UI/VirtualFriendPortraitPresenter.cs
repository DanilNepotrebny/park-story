﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Friends;
using DreamTeam.UI;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Location.UI
{
    [RequireComponent(typeof(Image))]
    public class VirtualFriendPortraitPresenter : MonoBehaviour, IPresenter<VirtualFriend>
    {
        public void Initialize(VirtualFriend model)
        {
            GetComponent<Image>().sprite = model.Portrait;
        }
    }
}