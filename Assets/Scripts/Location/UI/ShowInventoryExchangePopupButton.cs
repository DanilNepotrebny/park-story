﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Popups;
using DreamTeam.SocialNetworking;
using DreamTeam.SocialNetworking.Facebook;
using DreamTeam.SocialNetworking.Facebook.Request;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Location.UI
{
    [RequireComponent(typeof(Button))]
    public abstract class ShowInventoryExchangePopupButton : MonoBehaviour
    {
        [SerializeField] private SceneReference _popupScene;
        [SerializeField, RequiredField] private CountedItemReward _reward;

        [Inject] private Popup _popup;
        [Inject] private PopupSystem _popupSystem;
        [Inject] private FacebookManager _facebookManager;
        [Inject] protected SocialProfileInventoryExchangeSystem ExchangeSystem { get; private set; }

        private Button _button;
        private LivesPopup _livesPopup => _popup as LivesPopup;

        protected CountedItemReward Reward => _reward;

        protected void OnEnable()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnButtonClicked);
            _button.interactable = true;
        }

        protected void OnDisable()
        {
            _button.onClick.RemoveListener(OnButtonClicked);
        }

        protected abstract bool IsValidProfile(SocialProfile profile);

        private void OnButtonClicked()
        {
            StartCoroutine(OnButtonClickedInternal());
        }

        private IEnumerator OnButtonClickedInternal()
        {
            _button.interactable = false;

            if (_facebookManager.IsLoggedIn)
            {
                ShowPopup();
            }
            else
            {
                RequestLogin request = _facebookManager.Login();
                yield return request;
                bool isLoggedIn = false;
                if (request.Result != null)
                {
                    if (!string.IsNullOrEmpty(request.Result.Error))
                    {
                        UnityEngine.Debug.LogError($"LOGIN {request.Result.Error}");
                    }
                    else if (request.Result.Cancelled)
                    {
                        UnityEngine.Debug.LogError("LOGIN cancel!");
                    }
                    else
                    {
                        UnityEngine.Debug.Log("LOGIN success!");
                        isLoggedIn = true;
                    }
                }
                else
                {
                    UnityEngine.Debug.LogError("LOGIN result is null!");
                }

                if (isLoggedIn)
                {
                    ShowPopup();
                }
                else if (_popup.IsShown)
                {
                    _popupSystem.Hide(_popup.SceneName);
                }
            }

            _button.interactable = true;
        }

        private void ShowPopup()
        {
            var args = new SocialProfilesCollectionPopup.Args()
            {
                FilterCondition = IsValidProfile
            };

            _livesPopup.ShowInventoryExchange(_popupScene, args);
        }
    }
}