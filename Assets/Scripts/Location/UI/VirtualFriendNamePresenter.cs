﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using DreamTeam.Location.Friends;
using DreamTeam.UI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Location.UI
{
    [RequireComponent(typeof(Text))]
    public class VirtualFriendNamePresenter : MonoBehaviour, IPresenter<VirtualFriend>
    {
        [SerializeField, Tooltip("{0} - character name")]
        private string _textFormat = "{0}";

        [Inject] private LocalizationSystem _localizationSystem;

        public void Initialize(VirtualFriend model)
        {
            GetComponent<Text>().text = string.Format(_textFormat, _localizationSystem.Localize(model.Name, this));
        }
    }
}