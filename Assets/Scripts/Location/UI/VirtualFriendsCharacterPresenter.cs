﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Friends;
using DreamTeam.UI;

namespace DreamTeam.Location.UI
{
    public class VirtualFriendsCharacterPresenter : PresenterProxy<VirtualFriend, CharacterData>
    {
        protected override CharacterData TargetModel => SourceModel.Character;
    }
}