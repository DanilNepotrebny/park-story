﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.GameControls
{
    public abstract class BaseGameControlsModeHandler : MonoBehaviour, IDisposable
    {
        [SerializeField, RequiredField] private GameControlsMode _mode;

        [Inject] protected GameControlsSystem _controlsSystem;

        protected GameControlsContext _overridenContext = null;

        private ActionDisposable _modeHandle;

        protected bool IsModeEnabled => _modeHandle != null;

        public virtual void Dispose()
        {
            DisableMode();
        }

        protected void EnableMode()
        {
            Assert.IsNull(_modeHandle, "Mode was not disposed"); 
            _modeHandle = _overridenContext == null ? _controlsSystem.EnableMode(_mode) : _overridenContext.EnableMode(_mode);
        }

        protected void DisableMode()
        {
            if (_modeHandle != null)
            {
                _modeHandle.Dispose();
                _modeHandle = null;
            }
        }
    }
}
