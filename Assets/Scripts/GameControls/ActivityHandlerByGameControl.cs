﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using Object = UnityEngine.Object;

namespace DreamTeam.GameControls
{
    public class ActivityHandlerByGameControl : BaseGameControlWatcher
    {
        [SerializeField] private bool _controlSelfGameObject;
        [SerializeField] private Behaviour[] _components;
        [SerializeField] private GameObject[] _gameObjects;

        private DeferredInvocation _deactivationInvocation;

        public bool IsActive { get; private set; }

        public event Action Activating;
        public event Action Activated;
        public event Action<IDeferredInvocationHandle> Deactivating;

        [Inject]
        private void Construct()
        {
            if (_components != null)
            {
                ValidateObjects(_components);
            }

            if (_gameObjects != null)
            {
                ValidateObjects(_gameObjects);
            }

            _deactivationInvocation = new DeferredInvocation(
                    () =>
                    {
                        if (this != null)
                        {
                            ApplyActivation(false);
                        }
                    }
                );
        }

        public override void Init()
        {
            base.Init();

            SetActive(CurrentState == GameControl.State.Unlocked, true);
        }

        protected override void OnGameControlStateChanged(GameControl.State newState)
        {
            SetActive(newState == GameControl.State.Unlocked, false);
        }

        private void ValidateObjects(IEnumerable<Object> objects)
        {
            foreach (Object obj in objects)
            {
                Assert.IsNotNull(obj, $"Found null object in {nameof(ActivityHandlerByGameControl)} in {gameObject.name}");
            }
        }

        private void SetActive(bool isActive, bool isForced)
        {
            if (isActive == IsActive && !isForced)
            {
                return;
            }

            IsActive = isActive;
            if (IsActive)
            {
                Activating?.Invoke();
                ApplyActivation(isActive);
                Activated?.Invoke();
            }
            else
            {
                using (IDeferredInvocationHandle handle = _deactivationInvocation.Start())
                {
                    Deactivating?.Invoke(handle);
                }
            }
        }

        private void ApplyActivation(bool isActive)
        {
            if (_controlSelfGameObject)
            {
                gameObject.SetActive(isActive);
            }

            if (_gameObjects != null)
            {
                foreach (GameObject go in _gameObjects)
                {
                    go?.SetActive(isActive);
                }
            }

            if (_components != null)
            {
                foreach (Behaviour behaviour in _components)
                {
                    if (behaviour != null)
                    {
                        behaviour.enabled = isActive;
                    }
                }
            }
        }
    }
}
