﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.GameControls
{
    public abstract class BaseGameControlWatcher : MonoBehaviour, IInitable, IDisposable
    {
        [SerializeField, RequiredField, FormerlySerializedAs("_selectionLock"), FormerlySerializedAs("_selectionItem")]
        private GameControl _gameControl;

        [Inject] private GameControlsSystem _gameControlsSystem;

        protected GameControl.State CurrentState => _gameControlsSystem.GetControlState(_gameControl);

        public virtual void Init()
        {
            _gameControlsSystem.AddGameControlStateChangeHandler(_gameControl, OnGameControlStateChanged);
        }

        public virtual void Dispose()
        {
            _gameControlsSystem.RemoveGameControlStateChangeHandler(_gameControl, OnGameControlStateChanged);
        }
        
        protected abstract void OnGameControlStateChanged(GameControl.State newState);
    }
}
