﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.GameControls
{
    using MapElementType = KeyValuePair<GameControl, GameControl.State>;

    [CreateAssetMenu(menuName = "GameControls/Game Controls Mode Override")]
    public class GameControlsModeOverride : GameControlsMode, IEnumerable<MapElementType>
    {
        [SerializeField, RequiredField] private GameControlsMode _overridingMode;

        public override IEnumerator<MapElementType> GetEnumerator()
        {
            var result = new HashSet<MapElementType>(ControlsMap, new MapElementComparer());
            foreach (MapElementType baseElement in _overridingMode)
            {
                GameControl baseControl = baseElement.Key;

                bool isUnique = true;
                foreach (MapElementType element in ControlsMap)
                {
                    GameControl control = element.Key;
                    if (control == baseControl ||
                        control.Contains(baseControl))
                    {
                        isUnique = false;
                        break;
                    }
                }

                if (isUnique)
                {
                    result.Add(baseElement);
                }
            }
            return result.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #region Inner types

        private class MapElementComparer : IEqualityComparer<MapElementType>
        {
            public bool Equals(MapElementType x, MapElementType y)
            {
                return x.Key == y.Key;
            }

            public int GetHashCode(MapElementType element)
            {
                return element.Key.GetHashCode();
            }
        }

        #endregion
    }
}
