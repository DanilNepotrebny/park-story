﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.GameControls
{
    public class ActivityAnimationPresenter : MonoBehaviour, IInitable, IDisposable
    {
        [SerializeField] private ActivityHandlerByGameControl _activityHandler;
        [SerializeField] private Component _animation;

        private ISimpleAnimationPlayer _animationPlayer;

        private IDeferredInvocationHandle _deactivationHandle;

        public void Init()
        {
            _animationPlayer = _animation as ISimpleAnimationPlayer;

            if (_animationPlayer != null)
            {
                _animationPlayer.AnimationStopped += OnAnimationStopped;
            }
            else
            {
                LogError($"Parameter '{nameof(_animationPlayer)}' is not set");
            }

            if (_activityHandler != null)
            {
                _activityHandler.Activating += OnActivating;
                _activityHandler.Activated += OnActivated;
                _activityHandler.Deactivating += OnDeactivating;
            }
            else
            {
                LogError($"Parameter '{nameof(_activityHandler)}' is not set");
            }
        }

        public void Dispose()
        {
            if (_activityHandler != null)
            {
                _activityHandler.Activating -= OnActivating;
                _activityHandler.Activated -= OnActivated;
                _activityHandler.Deactivating -= OnDeactivating;
            }
        }

        protected void OnEnable()
        {
            TryShow();
        }

        private void OnActivated()
        {
            TryShow();
        }

        private void TryShow()
        {
            if (!_activityHandler.IsActive ||
                !gameObject.activeInHierarchy)
            {
                return;
            }

            if (_animationPlayer.IsPlaying &&
                _animationPlayer.PlayingAnimationType == SimpleAnimationType.Show)
            {
                return;
            }

            _animationPlayer.Play(SimpleAnimationType.Show);
        }

        private void OnActivating()
        {
            UnlockDeactivation();
        }

        private void OnDeactivating(IDeferredInvocationHandle invocationHandle)
        {
            if (gameObject.activeInHierarchy)
            {
                _deactivationHandle = invocationHandle.Lock();
                _animationPlayer.Play(SimpleAnimationType.Hide);
            }
        }

        private void OnAnimationStopped(SimpleAnimationType animType)
        {
            if (animType == SimpleAnimationType.Hide)
            {
                UnlockDeactivation();
            }
        }

        private void UnlockDeactivation()
        {
            IDeferredInvocationHandle handle = _deactivationHandle;
            _deactivationHandle = null;
            handle?.Unlock();
        }

        private void LogError(string error)
        {
            UnityEngine.Debug.LogError($"[{nameof(ActivityAnimationPresenter)}] {error}", this);
        }
    }
}
