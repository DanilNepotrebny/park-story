﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.GameControls
{
    public class GameControlsModeHandler : BaseGameControlsModeHandler
    {
        protected void OnEnable()
        {
            EnableMode();
        }

        protected void OnDisable()
        {
            DisableMode();
        }
    }
}
