﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Text;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine.Assertions;

namespace DreamTeam.GameControls
{
    using ControlStatePair = KeyValuePair<GameControl, GameControl.State>;
    using ControlsModeActivityPair = KeyValuePair<GameControlsMode, bool>;

    public class GameControlsContext : IDisposable
    {
        private readonly HashSet<GameControlsSystem.ControlStateInfo> _changedControls =
            new HashSet<GameControlsSystem.ControlStateInfo>();

        private readonly Queue<ControlsModeActivityPair> _pendingModeChanges =
            new Queue<ControlsModeActivityPair>();

        private readonly Dictionary<GameControlsMode, int> _modeCounters =
            new Dictionary<GameControlsMode, int>();

        private readonly List<ControlsModeActivityPair> _modesToApplyOnActivation =
            new List<ControlsModeActivityPair>();

        private readonly GameControlsSystem _system;

        public string Name { get; }

        public bool IsActive => _system.ActiveContext == this;

        private bool IsApplyingMode => _changedControls.Count > 0;

        // TODO: IGameControlsSystemFriend API
        public event GameControlsStateChangedDelegate GameControlsStateChanged;

        public event GameControlsModeActivityChangedDelegate GameControlsModeActivityChanged;

        public GameControlsContext(string name, [NotNull] GameControlsSystem system)
        {
            Name = name;
            _system = system;
        }

        // TODO: IGameControlsSystemFriend API
        public void Activate()
        {
            SetModeEnabledInternal(_modesToApplyOnActivation);
            _modesToApplyOnActivation.Clear();
        }

        // TODO: IGameControlsSystemFriend API
        public void Deactivate()
        {
            _modesToApplyOnActivation.Clear();
            var modesToDisable = new List<ControlsModeActivityPair>();

            foreach (var pair in _modeCounters)
            {
                GameControlsMode mode = pair.Key;

                if (IsModeEnabled(mode))
                {
                    modesToDisable.Add(new ControlsModeActivityPair(mode, false));
                    _modesToApplyOnActivation.Add(new ControlsModeActivityPair(mode, true));
                }
            }

            SetModeEnabledInternal(modesToDisable);
        }

        // TODO: IGameControlsSystemFriend API
        public bool IsControlInState([NotNull] GameControl control, GameControl.State state)
        {
            GameControl.State current = GetControlState(control);
            return current == state;
        }

        // TODO: IGameControlsSystemFriend API
        public GameControl.State GetControlState([NotNull] GameControl control)
        {
            GameControlsSystem.ControlStateInfo stateInfo = _system.FindControlState(control);
            if (stateInfo != null)
            {
                return stateInfo.Current;
            }

            return GameControl.State.Locked;
        }

        // TODO: IGameControlsSystemFriend API
        public void DrawDebugState(StringBuilder builder)
        {
            builder.AppendLine("\nEnabled modes:");
            foreach (var pair in _modeCounters)
            {
                GameControlsMode controlsMode = pair.Key;
                int counter = pair.Value;

                if (counter > 0)
                {
                    builder.AppendLine(controlsMode.name);
                }
            }
        }

        /// <summary> Enable <param name="mode"/> with disable handle </summary>
        /// <param name="mode"> Mode to enable </param>
        /// <returns> Handle to disable the mode </returns>
        public ActionDisposable EnableMode([NotNull] GameControlsMode mode)
        {
            SetModeEnabled(mode, true);

            return new ActionDisposable(d => SetModeEnabled(mode, false));
        }

        public bool IsModeEnabled([NotNull] GameControlsMode mode)
        {
            return _modeCounters.ContainsKey(mode) && _modeCounters[mode] > 0;
        }

        public void Dispose()
        {
            var modes = new List<ControlsModeActivityPair>();

            foreach (var pair in _modeCounters)
            {
                GameControlsMode mode = pair.Key;
                if (IsModeEnabled(mode))
                {
                    modes.Add(new ControlsModeActivityPair(mode, false));
                }
            }

            SetModeEnabledInternal(modes);
        }

        /// <summary>
        /// Set <param name="mode"/> to be <param name="isEnabled"/> onto this context.
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="isEnabled"></param>
        /// <returns> Whether mode was enabled. Otherwise it will be active when this context become active </returns>
        /// <exception cref="InvalidOperationException"> Thrown when disabling already disabled mode </exception>
        private bool SetModeEnabled([NotNull] GameControlsMode mode, bool isEnabled)
        {
            var modeActivityPair = new ControlsModeActivityPair(mode, isEnabled);
            if (!IsActive)
            {
                if (_modesToApplyOnActivation.Exists(pair => pair.Key == mode))
                {
                    List<ControlsModeActivityPair> modeActivities = _modesToApplyOnActivation.FindAll(pair => pair.Key == mode);
                    if (modeActivities.Exists(pair => pair.Value == !isEnabled))
                    {
                        ControlsModeActivityPair pairToRemove = modeActivities.Find(pair => pair.Value == !isEnabled);
                        _modesToApplyOnActivation.Remove(pairToRemove);
                        return false;
                    }
                }

                _modesToApplyOnActivation.Add(modeActivityPair);
                return false;
            }

            return SetModeEnabledInternal(new List<ControlsModeActivityPair> { modeActivityPair });
        }

        private bool SetModeEnabledInternal([NotNull] List<ControlsModeActivityPair> modes)
        {
            if (IsApplyingMode)
            {
                modes.ForEach(mode => _pendingModeChanges.Enqueue(mode));
                return false;
            }

            var switchedModes = new List<GameControlsMode>();
            foreach (var pair in modes)
            {
                GameControlsMode mode = pair.Key;
                bool isEnabled = pair.Value;

                if (!_modeCounters.ContainsKey(mode))
                {
                    _modeCounters.Add(mode, 0);
                }

                bool wasModeEnabled = IsModeEnabled(mode);

                if (isEnabled)
                {
                    _modeCounters[mode]++;
                }
                else
                {
                    if (_modeCounters[mode] == 0)
                    {
                        UnityEngine.Debug.LogWarning($"Trying to disable already disabled mode {mode}");
                    }
                    else
                    {
                        _modeCounters[mode]--;
                    }
                }

                if (wasModeEnabled != IsModeEnabled(mode))
                {
                    switchedModes.Add(mode);
                }
            }

            switchedModes.ForEach(mode => GameControlsModeActivityChanged?.Invoke(mode, IsModeEnabled(mode)));
            InvalidateControlsState(switchedModes);

            FlushPendingModes();
            return true;
        }

        private void InvalidateControlsState([NotNull] List<GameControlsMode> switchedModes)
        {
            Assert.IsTrue(
                _changedControls.Count == 0,
                $"[{nameof(GameControlsContext)}]: " +
                $"On begin of controls invalidation there are still changed but not pruned controls");

            foreach (var mode in switchedModes)
            {
                bool isModeEnabled = IsModeEnabled(mode);
                foreach (var pair in mode)
                {
                    GameControl control = pair.Key;
                    ModestTree.Assert.IsNotNull(control, $"'{mode.name}' mode has null GameControl");
                    GameControl.State state = pair.Value;

                    var controlsQueue = new Queue<GameControl>();
                    controlsQueue.Enqueue(control);

                    while (controlsQueue.Count > 0)
                    {
                        GameControl c = controlsQueue.Dequeue();
                        GameControlsSystem.ControlStateInfo controlState = _system.FindControlState(c);
                        if (controlState == null)
                        {
                            controlState = new GameControlsSystem.ControlStateInfo(c, state);
                            _system.StoreControlState(controlState);

                            // adding to event invocation list only if initial state is not locked cause by default game control is locked
                            if (state != GameControl.State.Locked)
                            {
                                _changedControls.Add(controlState);
                            }
                        }
                        else
                        {
                            GameControl.State prevState = controlState.Current;
                            if (isModeEnabled)
                            {
                                controlState.Increment(state);
                            }
                            else
                            {
                                controlState.Decrement(state);
                            }

                            if (prevState != controlState.Current)
                            {
                                // cause we have two states - locked and unlocked
                                // state change of already changed control means it was switched back
                                if (!_changedControls.Contains(controlState))
                                {
                                    _changedControls.Add(controlState);
                                }
                                else
                                {
                                    _changedControls.Remove(controlState);
                                }
                            }
                        }

                        foreach (GameControl childControl in c.Childs)
                        {
                            controlsQueue.Enqueue(childControl);
                        }
                    }
                }
            }

            if (_changedControls.Count > 0)
            {
                var eventDataList = new List<ControlStatePair>(_changedControls.Count);
                foreach (GameControlsSystem.ControlStateInfo controlState in _changedControls)
                {
                    eventDataList.Add(new ControlStatePair(controlState.Control, controlState.Current));
                }

                GameControlsStateChanged?.Invoke(eventDataList);

                _changedControls.Clear();
            }
        }

        private void FlushPendingModes()
        {
            while (_pendingModeChanges.Count > 0)
            {
                ControlsModeActivityPair pair = _pendingModeChanges.Dequeue();
                SetModeEnabledInternal(new List<ControlsModeActivityPair> { pair });
            }
        }

        #region Inner types

        public delegate void GameControlsStateChangedDelegate(List<ControlStatePair> controlsNewState);

        public delegate void GameControlsModeActivityChangedDelegate([NotNull] GameControlsMode mode, bool isEnabled);

        #endregion Inner types
    }
}