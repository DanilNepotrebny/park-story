﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DreamTeam.Debug;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.GameControls
{
    public class GameControlsSystem : MonoBehaviour, IInitable
    {
        [Inject] private Instantiator _instantiator;
        [Inject] private DebugStatesSystem _debugStatesSystem;

        private readonly EventDictionary<GameControl, GameControl.State> _controlsStateChangedHandlers =
            new EventDictionary<GameControl, GameControl.State>();

        private readonly HashSet<ControlStateInfo> _controlsSet = new HashSet<ControlStateInfo>();

        private Stack<GameControlsContext> _contexts = new Stack<GameControlsContext>();

        public GameControlsContext ActiveContext => _contexts.Peek();

        public bool IsControlInState([NotNull] GameControl control, GameControl.State state)
        {
            return ActiveContext.IsControlInState(control, state);
        }

        public GameControl.State GetControlState([NotNull] GameControl control)
        {
            return ActiveContext.GetControlState(control);
        }

        /// <summary> Enable <param name="mode"/> in active context with disable handle </summary>
        /// <param name="mode"> Mode to enable </param>
        /// <returns> Handle to disable the mode </returns>
        public ActionDisposable EnableMode([NotNull] GameControlsMode mode)
        {
            return ActiveContext.EnableMode(mode);
        }

        /// <summary>
        /// Switch already enabled mode to a new mode. Will be performed cross switching to decrease possible locks
        /// switching between modes
        /// </summary>
        /// <param name="modeDisposable"> Already enabled mode's disable handle </param>
        /// <param name="mode"> Mode to activate </param>
        /// <returns> Disable handle for enabled mode </returns>
        public void SwitchMode([CanBeNull] ref ActionDisposable modeDisposable, [NotNull] GameControlsMode mode)
        {
            // ReSharper disable once UnusedVariable
            using (ActionDisposable prevDisposable = modeDisposable)
            {
                modeDisposable = EnableMode(mode);
            } // prevDisposable.Dispose() here
        }

        /// <summary>
        /// Is <param name="mode"/> enabled in active context
        /// </summary>
        public bool IsModeEnabled([NotNull] GameControlsMode mode)
        {
            return ActiveContext.IsModeEnabled(mode);
        }

        public void AddGameControlStateChangeHandler(
            [NotNull] GameControl control,
            [NotNull] Action<GameControl.State> handler)
        {
            _controlsStateChangedHandlers.Add(control, handler);
        }

        public void RemoveGameControlStateChangeHandler(
            [NotNull] GameControl control,
            [NotNull] Action<GameControl.State> handler)
        {
            _controlsStateChangedHandlers.Remove(control, handler);
        }

        /// <summary>
        /// Push <param name="context"/> on top (set active one). Previous context will be deactivated.
        /// Previously unlocked controls will be locked unless new context unlocks them.
        /// </summary>
        /// <returns> ActionDisposable to remove context from the system</returns>
        public ActionDisposable PushContext([NotNull] GameControlsContext context)
        {
            SubscribeToContext(context);
            context.Activate();

            ActiveContext.Deactivate();
            UnsubscribeFromContext(ActiveContext);

            _contexts.Push(context);

            return new ActionDisposable(
                d =>
                {
                    RemoveContext(context);
                    context.Dispose();
                });
        }

        // TODO: IGameControlsContextFriend API
        public ControlStateInfo FindControlState([NotNull] GameControl control)
        {
            return _controlsSet.FirstOrDefault(c => c.Control == control);
        }

        // TODO: IGameControlsContextFriend API
        public void StoreControlState([NotNull] ControlStateInfo controlState)
        {
            Assert.IsFalse(
                _controlsSet.Contains(controlState),
                $"Attempt to store already stored {nameof(GameControl)}");
            _controlsSet.Add(controlState);
        }

        public void Init()
        {
            var context = _instantiator.Instantiate<GameControlsContext>("Default", this);
            _contexts.Push(context);
            SubscribeToContext(ActiveContext);
        }

        protected void OnEnable()
        {
            _debugStatesSystem.RegisterWriter(nameof(GameControlsSystem), DrawDebugState);
        }

        protected void OnDisable()
        {
            _debugStatesSystem.UnregisterWriter(DrawDebugState);
        }

        private void RemoveContext([NotNull] GameControlsContext context)
        {
            bool isRemovingActiveOne = context == ActiveContext;

            var _contextList = _contexts.ToList();
            _contextList.Reverse();
            _contextList.Remove(context);

            _contexts = new Stack<GameControlsContext>(_contextList);

            if (isRemovingActiveOne)
            {
                SubscribeToContext(ActiveContext);
                ActiveContext.Activate();

                context.Deactivate();
                UnsubscribeFromContext(context);
            }
        }

        private void SubscribeToContext([NotNull] GameControlsContext context)
        {
            context.GameControlsStateChanged += OnGameControlsStateChanged;
        }

        private void UnsubscribeFromContext([NotNull] GameControlsContext context)
        {
            context.GameControlsStateChanged -= OnGameControlsStateChanged;
        }

        private void OnGameControlsStateChanged(List<KeyValuePair<GameControl, GameControl.State>> controlsNewState)
        {
            if (_controlsStateChangedHandlers == null)
            {
                return;
            }

            foreach (var pair in controlsNewState)
            {
                GameControl control = pair.Key;
                GameControl.State state = pair.Value;

                _controlsStateChangedHandlers.Invoke(control, state);
            }
        }

        private void DrawDebugState(StringBuilder builder)
        {
            builder.AppendParameter("Active context", ActiveContext.Name);

            ActiveContext.DrawDebugState(builder);

            builder.AppendLine("\nControls state (not presented are locked):");
            foreach (ControlStateInfo stateInfo in _controlsSet)
            {
                builder.AppendParameter(stateInfo.Control.name, stateInfo.Current.ToString());
            }

            builder.AppendLine("\nContexts stack:");
            foreach (GameControlsContext context in _contexts)
            {
                builder.AppendLine(context.Name);
            }
        }

        #region Inner types

        public class ControlStateInfo : IEqualityComparer<ControlStateInfo>
        {
            private bool _hasALock => _stateMap[GameControl.State.Locked] > 0;
            private bool _hasAnUnlock => _stateMap[GameControl.State.Unlocked] > 0;

            // TODO: Store some information to debug hanging locks/unlocks
            private readonly Dictionary<GameControl.State, int> _stateMap =
                new Dictionary<GameControl.State, int>();

            public GameControl Control { get; }

            public GameControl.State Current => _hasALock || !_hasAnUnlock ?
                GameControl.State.Locked :
                GameControl.State.Unlocked;

            public event StateChangedDelegate StateChanged;

            public ControlStateInfo([NotNull] GameControl control)
            {
                Control = control;

                foreach (GameControl.State state in Enum.GetValues(typeof(GameControl.State)))
                {
                    _stateMap.Add(state, 0);
                }
            }

            public ControlStateInfo([NotNull] GameControl control, GameControl.State initialState)
                : this(control)
            {
                Increment(initialState);
            }

            public void Increment(GameControl.State state)
            {
                GameControl.State prevState = Current;
                _stateMap[state] += 1;

                if (prevState != Current)
                {
                    StateChanged?.Invoke(this);
                }
            }

            public void Decrement(GameControl.State state)
            {
                int newValue = _stateMap[state] - 1;
                if (newValue < 0)
                {
                    UnityEngine.Debug.LogWarning(
                        $"Too many '{state}' action decrimentations on '{Control.name}' " +
                        $"{nameof(GameControl)}");
                }

                GameControl.State prevState = Current;
                _stateMap[state] = Mathf.Max(newValue, 0);

                if (prevState != Current)
                {
                    StateChanged?.Invoke(this);
                }
            }

            public bool Equals(ControlStateInfo x, ControlStateInfo y)
            {
                if (x != null && y != null)
                {
                    return x.Control.Equals(y.Control);
                }

                return x == null && y == null;
            }

            public int GetHashCode(ControlStateInfo stateInfo)
            {
                return stateInfo.Control.GetHashCode();
            }

            #region Inner types

            public delegate void StateChangedDelegate(ControlStateInfo updatedControl);

            #endregion Inner types
        }

        #endregion Inner types
    }
}