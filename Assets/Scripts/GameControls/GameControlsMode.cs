﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Serialization;

namespace DreamTeam.GameControls
{
    using MapElementType = KeyValuePair<GameControl, GameControl.State>;

    [CreateAssetMenu(menuName = "GameControls/Game Controls Mode")]
    public class GameControlsMode : ScriptableObject, IEnumerable<MapElementType>
    {
        [SerializeField, FormerlySerializedAs("_items")]
        private GameControlsMap _controls;
        
        protected GameControlsMap ControlsMap => _controls;

        public virtual IEnumerator<MapElementType> GetEnumerator()
        {
            return _controls.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        [Serializable]
        public class GameControlsMap : KeyValueList<GameControl, GameControl.State>
        {
        }
    }
}