﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.GameControls
{
    [CreateAssetMenu(menuName = "GameControls/Game Control")]
    public class GameControl : ScriptableObject
    {
        [SerializeField] private GameControl[] _childs;

        public GameControl[] Childs => _childs;

        public bool Contains(GameControl control)
        {
            foreach (GameControl child in _childs)
            {
                if (child == control ||
                    child.Contains(control))
                {
                    return true;
                }
            }

            return false;
        }

        #region Inner types

        public enum State
        {
            Locked,
            Unlocked
        }

        #endregion Inner types
    }
}