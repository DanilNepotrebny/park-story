﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.Wall
{
    public partial class Wall : MonoBehaviour, IWeightCriterion
    {
        [SerializeField] private float _destructionWeight;
        [SerializeField, HideInInspector] private WallSystem.Position _position;

        [Inject] private GridSystem _grid;
        [Inject] private WallSystem _wallSystem;

        private HealthController _healthController;
        private CellComponent _cell1, _cell2;
        private int _simultaneousImpactCount;
        private readonly List<CellComponent> _impactScheduledCells = new List<CellComponent>();

        public WallSystem.Position Position => _position;

        public WallPresenter Presenter { get; private set; }

        public event Action Impacting;

        public float CalculateWeight()
        {
            return _destructionWeight;
        }

        public bool WillBeDestroyed()
        {
            int impactingCells = _impactScheduledCells.Distinct().Count();
            return impactingCells == _simultaneousImpactCount;
        }

        protected void Awake()
        {
            Presenter = GetComponentInChildren<WallPresenter>();
            _healthController = GetComponent<HealthController>();
        }

        private void Start()
        {
            if (_healthController == null)
            {
                return;
            }

            _simultaneousImpactCount = 0;
            _cell1 = _grid.GetCell(Position.Point1);
            _cell2 = _grid.GetCell(Position.Point2);
            Assert.IsTrue(_cell1 != null || _cell2 != null,
                $"Wall must keep an eye at least on one cell to be able to self destroy");
            if (_cell1 != null)
            {
                _simultaneousImpactCount += 1;
                SubscribeToImpact(_cell1);
            }
            if (_cell2 != null)
            {
                _simultaneousImpactCount += 1;
                SubscribeToImpact(_cell2);
            }
        }

        protected void OnDestroy()
        {
            UnsubscribeFromImpact(_cell1);
            UnsubscribeFromImpact(_cell2);

            _wallSystem.RemoveWall(Position);
        }

        private void OnImpactScheduled(CellComponent cell, Impact impact)
        {
            if (_healthController.CanBeImpacted(impact.Flag))
            {
                impact.Cancelling += OnScheduledImpactCancelling;
                _impactScheduledCells.Add(cell);
            }
        }

        private void OnScheduledImpactCancelling(Impact impact)
        {
            impact.Cancelling -= OnScheduledImpactCancelling;
            _impactScheduledCells.Remove(impact.Cell);
        }

        private void OnCellImpacted(CellComponent cell, ChipComponent chip, ImpactFlag flag, int impactGroupId)
        {
            if (WillBeDestroyed())
            {
                DestroySelf();
            }
            else
            {
                _impactScheduledCells.Clear();
            }
        }

        private void DestroySelf()
        {
            UnsubscribeFromImpact(_cell1);
            UnsubscribeFromImpact(_cell2);

            Impacting?.Invoke();
            gameObject.Dispose();
        }

        private void SubscribeToImpact([NotNull] CellComponent cell)
        {
            cell.ImpactScheduled += OnImpactScheduled;
            cell.Impacted += OnCellImpacted;
        }

        private void UnsubscribeFromImpact([CanBeNull] CellComponent cell)
        {
            if (cell == null)
            {
                return;
            }

            cell.ImpactScheduled -= OnImpactScheduled;
            cell.Impacted -= OnCellImpacted;
        }
    }
}