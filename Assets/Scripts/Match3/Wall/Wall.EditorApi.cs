﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

namespace DreamTeam.Match3.Wall
{
    public partial class Wall : Wall.IEditorApi
    {
        WallSystem.Position IEditorApi.Position
        {
            get { return _position; }
            set { _position = value; }
        }

        #region Inner types

        public interface IEditorApi
        {
            WallSystem.Position Position { get; set; }
        }

        #endregion Inner types
    }
}

#endif // UNITY_EDITOR