﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using JetBrains.Annotations;
using ModestTree;
using UnityEngine;

namespace DreamTeam.Match3.Wall
{
    public partial class WallSystem : MonoBehaviour
    {
        [SerializeField, ReadOnly] private WallsList _walls = new WallsList(EqualityComparer<Position>.Default);

        public bool AddWall(Position position, [NotNull] Wall wall)
        {
            if (HasWall(position))
            {
                return false;
            }

            _walls.Add(position, wall);
            return true;
        }

        public Wall RemoveWall(Position position)
        {
            if (!HasWall(position))
            {
                return null;
            }

            Wall removingWall = _walls[position];
            _walls.RemoveAll(position);
            return removingWall;
        }

        public void ForEachWall([NotNull] Action<Wall> action)
        {
            _walls.ForEach(pair => action.Invoke(pair.Value));
        }

        public bool HasWall(Position position)
        {
            return _walls.ContainsKey(position);
        }

        public List<Wall> FindAssociated(Vector2Int position)
        {
            var list = new List<Wall>(4);

            Direction dir = Direction.Up;
            do
            {
                var associatedPosition = new Position(position, position + dir.GetOffset());
                if (HasWall(associatedPosition))
                {
                    list.Add(_walls[associatedPosition]);
                }

                dir = dir.Rotate90CW();
            }
            while (dir != Direction.Up);

            return list;
        }

        #region Inner types

        [Serializable] public struct Position : IEquatable<Position>
        {
            [SerializeField] private Vector2Int _point1;
            [SerializeField] private Vector2Int _point2;

            public Vector2Int Point1 => _point1;
            public Vector2Int Point2 => _point2;

            public Position(Vector2Int point1, Vector2Int point2)
            {
                _point1 = point1;
                _point2 = point2;
            }

            public bool Equals(Position other)
            {
                return Point1 == other.Point1 && Point2 == other.Point2 ||
                    Point1 == other.Point2 && Point2 == other.Point1;
            }
        }

        [Serializable] private class WallsList : KeyValueList<Position, Wall>
        {
            public WallsList(IEqualityComparer<Position> comparer)
                : base(comparer)
            {
            }
        }

        #endregion Inner types
    }
}
