﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3.Wall
{
    [ExecuteInEditMode]
    public partial class WallSystem : WallSystem.IEditorApi
    {
        bool IEditorApi.AddWall(Position position, Wall wall)
        {
            return AddWall(position, wall);
        }

        Wall IEditorApi.RemoveWall(Position position)
        {
            return RemoveWall(position);
        }

        bool IEditorApi.HasWall(Position position)
        {
            return HasWall(position);
        }

        protected void Update()
        {
            WallSystemProvider.SetProvider(this);
        }

        protected void OnDestroy()
        {
            WallSystemProvider.Reset();
        }

        #region Inner types

        public interface IEditorApi
        {
            GameObject gameObject { get; }
            bool AddWall(Position position, [NotNull] Wall wall);
            Wall RemoveWall(Position position);
            bool HasWall(Position position);
        }

        #endregion
    }

    public static class WallSystemProvider
    {
        private static WallSystem.IEditorApi _wallSystem;

        public static WallSystem.IEditorApi Get()
        {
            if (_wallSystem == null)
            {
                _wallSystem = GenerateWallSystem();
            }
            return _wallSystem;
        }

        internal static void SetProvider([NotNull] WallSystem.IEditorApi system)
        {
            Assert.IsTrue(_wallSystem == null || _wallSystem == system, $"Multiple {nameof(WallSystem)} assignment");
            _wallSystem = system;
        }

        internal static void Reset()
        {
            _wallSystem = null;
        }

        private static WallSystem GenerateWallSystem()
        {
            WallSystem wallSystem = null;

            Undo.IncrementCurrentGroup();
            int currentGroupId = Undo.GetCurrentGroup();
            {
                var wallSystemGO = new GameObject("WallSystem");
                Undo.RegisterCreatedObjectUndo(wallSystemGO, "Create WallSystem game object");

                wallSystem = Undo.AddComponent<WallSystem>(wallSystemGO);

                var levelGO = GameObject.Find("Level");
                Undo.SetTransformParent(wallSystemGO.transform, levelGO.transform, "Add WallSystem to Level");
            }
            Undo.CollapseUndoOperations(currentGroupId);

            return wallSystem;
        }
    }
}

#endif // UNITY_EDITOR
