﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using DreamTeam.Utils.Coroutines;
using DreamTeam.Utils.RendererSorting;
using Spine;
using Spine.Unity;
using UnityEngine;
using Zenject;
using AnimationState = Spine.AnimationState;

namespace DreamTeam.Match3.Wall
{
    [RequireComponent(typeof(Renderer))]
    public class WallPresenter : MonoBehaviour, ISortingOptionsProvider
    {
        [SerializeField, SpineAnimation] private string _consumptionAnimation;
        [SerializeField] private GameObject _consumptionVFXPrefab;

        [Inject] private Instantiator _instantiator;

        private Wall _wall;
        private Renderer _renderer;
        private ISimpleAnimationPlayer _animPlayer;
        private SkeletonAnimation _skeletonAnimation;

        public SortingOptions SortingOptions { get; private set; }

        public bool IsPlayingAnimation => _animPlayer.IsPlaying;

        public bool Play(SimpleAnimationType type)
        {
            if (type == SimpleAnimationType.Show)
            {
                gameObject.SetActive(true);
            }

            return _animPlayer.Play(type);
        }

        protected void Awake()
        {
            _wall = GetComponentInParent<Wall>();
            _renderer = GetComponent<Renderer>();
            _animPlayer = GetComponent<ISimpleAnimationPlayer>();
            _animPlayer.AnimationFinished += OnAnimationFinished;
            _skeletonAnimation = GetComponent<SkeletonAnimation>();

            SortingOptions = _instantiator.Instantiate<SortingOptions>(
                _renderer.sortingLayerID,
                _renderer.sortingOrder);
            SortingOptions.SortingLayerChanged += OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged += OnSortingOrderChanged;
        }

        protected void Start()
        {
            _wall.Impacting += () => StartCoroutine(OnWallImpacting());
        }

        protected void OnDestroy()
        {
            SortingOptions.SortingLayerChanged -= OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged -= OnSortingOrderChanged;
            SortingOptions.Dispose();
            SortingOptions = null;
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animType)
        {
            if (animType == SimpleAnimationType.Hide)
            {
                gameObject.SetActive(false);
            }
        }

        private IEnumerator OnWallImpacting()
        {
            transform.SetParent(null);

            _skeletonAnimation.state.SetAnimation(0, _consumptionAnimation, false);
            _instantiator.Instantiate(_consumptionVFXPrefab, transform.position);

            yield return new WaitForEvent<TrackEntry, AnimationState>(
                _skeletonAnimation.state,
                nameof(_skeletonAnimation.state.Complete));

            gameObject.Dispose();
        }

        private void OnSortingOrderChanged(SortingOptions options, int prevorder)
        {
            _renderer.sortingOrder = options.SortingOrder;
        }

        private void OnSortingLayerChanged(SortingOptions options)
        {
            _renderer.sortingLayerID = options.SortingLayerId;
        }
    }
}