﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(ChipPresenter))]
    public class ChipFallingPresenter : BaseChipMovementPresenter
    {
        [SerializeField] private string _fallingParameterName = "IsFalling";
        [SerializeField] private float _speedDecrease = 2f;
        [SerializeField] private float _minSpeed;

        [Inject] private GravitySystem _gravitySystem;

        private Mover _mover;

        public float CurrentSpeed => _mover.CurrentSpeed;

        public override ChipComponent.MovingType MovingType => ChipComponent.MovingType.Falling;
        
        protected override void Awake()
        {
            base.Awake();

            _mover = GetComponentInChildren<Mover>();
        }

        protected override void PrepareMovement()
        {
            Vector3 targetPosition = Chip.transform.position;

            Vector3 direction = (targetPosition - transform.position).normalized;

            ChipPresenter.SetAnimationDirection(Vector2Int.RoundToInt(direction).GetDirection());
            ChipPresenter.SetBool(_fallingParameterName, true);
        }

        protected override void StartMovement(Action finishCallback)
        {
            Vector3 targetPosition = Chip.transform.position;

            ChipComponent previous = _gravitySystem.FindPreviousFallingChip(Chip);

            if (previous != null && previous.Presenter != null)
            {
                var previousPresenter = previous.Presenter.GetComponent<ChipFallingPresenter>();

                if (previousPresenter != null)
                {
                    float speed = previousPresenter._mover.CurrentSpeed;
                    speed = Mathf.Max(speed - _speedDecrease, _minSpeed);

                    _mover.CurrentSpeed = speed;
                }
            }

            _mover.MoveTo(targetPosition, mover => finishCallback?.Invoke());
        }

        protected override void FinishMovement()
        {
            ChipPresenter.SetBool(_fallingParameterName, false);

            _mover.Finish();
        }
    }
}