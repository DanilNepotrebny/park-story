﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Audio;
using DreamTeam.Utils;
using DreamTeam.Utils.Spine;
using Spine;
using Spine.Unity;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Presenter for a chip that visually consists of diferrent parts with spine animations for each health amount.
    /// </summary>
    [RequireComponent(typeof(SpineAnimationsFinishNotifier))]
    public class SpineChipPartsPresenter : ChipPartsPresenter
    {
        [SerializeField, ReadOnly] private string _impactAnimTemplate;
        [SerializeField, ReadOnly] private string _consumptionAnimTemplate;

        [Header("Idle animation settings")]
        [SerializeField]
        private MinMaxRangedFloat _periodicAnimInterval = new MinMaxRangedFloat(0, 10, 2.5f, 7.5f);

        [SerializeField, ReadOnly] private string _idleAnimTemplate;
        [SerializeField, ReadOnly] private string _periodicAnimTemplate;

        private bool _canUpdateIdleAnimation = true;
        private bool _lockIdleAnimation = false;
        private float _periodicAnimationPlayTime = float.MaxValue;
        private SpineAnimationsFinishNotifier _animationsFinishNotifier;

        public void LockIdleAnimation()
        {
            _lockIdleAnimation = true;
        }

        public void UnlockIdleAnimation()
        {
            _lockIdleAnimation = false;
        }

        public void PlayAnimationForParts(string animationNameTemplate, Action completeCallback)
        {
            if (string.IsNullOrEmpty(animationNameTemplate))
            {
                completeCallback?.Invoke();
                return;
            }

            foreach (GameObject part in ActiveParts)
            {
                var skeletonAnimation = part.GetComponent<SkeletonAnimation>();
                if (skeletonAnimation != null)
                {
                    string animationName = GetAnimationName(animationNameTemplate, FindMinHealthForPart(part));
                    skeletonAnimation.AnimationState?.SetAnimation(0, animationName, false);
                }
            }

            _animationsFinishNotifier.WaitForAnimationsFinish(completeCallback);
        }

        protected override void Awake()
        {
            base.Awake();

            _animationsFinishNotifier = GetComponent<SpineAnimationsFinishNotifier>();

            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                SetPartConsumeAction(OnPartConsuming);
                UpdatePeriodicAnimationPlayTime(0);
            }
        }

        protected override void Update()
        {
            base.Update();

            if (EditorUtils.IsPlayingOrWillChangePlaymode() && _canUpdateIdleAnimation && !_lockIdleAnimation)
            {
                float rangedTime = Time.time % _periodicAnimInterval.Range;
                if (rangedTime.InRange(_periodicAnimInterval.MinValue, _periodicAnimInterval.MaxValue))
                {
                    UpdatePeriodicAnimation(rangedTime);
                }
                else
                {
                    UpdateIdleAnimation(rangedTime);
                }
            }
        }

        protected override void OnHealthChanged(int health)
        {
            base.OnHealthChanged(health);

            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                _canUpdateIdleAnimation = false;
                PlayAnimationForParts(
                        _impactAnimTemplate,
                        () =>
                        {
                            _canUpdateIdleAnimation = true;
                            UnlockIdleAnimation();
                        }
                    );
            }
        }

        private void UpdateIdleAnimation(float rangedTime)
        {
            _canUpdateIdleAnimation = false;
            PlayAnimationForParts(_idleAnimTemplate, () => _canUpdateIdleAnimation = true);
            UpdatePeriodicAnimationPlayTime(rangedTime);
        }

        private void UpdatePeriodicAnimation(float rangedTime)
        {
            if (rangedTime >= _periodicAnimationPlayTime)
            {
                UpdatePeriodicAnimationPlayTime(rangedTime);
                _canUpdateIdleAnimation = false;
                PlayAnimationForParts(_periodicAnimTemplate, () => _canUpdateIdleAnimation = true);
            }
        }

        private void UpdatePeriodicAnimationPlayTime(float rangedTime)
        {
            if (rangedTime.InRange(_periodicAnimInterval.MinValue, _periodicAnimInterval.MaxValue))
            {
                _periodicAnimationPlayTime = float.MaxValue;
            }
            else if (
                !_periodicAnimationPlayTime.InRange(_periodicAnimInterval.MinValue, _periodicAnimInterval.MaxValue))
            {
                _periodicAnimationPlayTime = Random.Range(
                        _periodicAnimInterval.MinValue,
                        _periodicAnimInterval.MaxValue
                    );
            }
        }

        private bool OnPartConsuming(GameObject part, int health)
        {
            part.transform.SetParent(null);

            var skeleton = part.GetComponent<SkeletonAnimation>();
            string animation = GetAnimationName(_consumptionAnimTemplate, health);
            var audio = part.GetComponent<AudioSystemPlayer>();

            var visualization = new PartsConsumeVisualization(skeleton, animation, audio);
            visualization.Execute(
                    () =>
                    {
                        _canUpdateIdleAnimation = true;
                        UnlockIdleAnimation();
                        part.Dispose();
                    }
                );
            _canUpdateIdleAnimation = visualization.IsPlaying;

            return visualization.IsPlaying;
        }

        private string GetAnimationName(string animationNameTemplate, int health)
        {
            return string.Format(animationNameTemplate, health);
        }

        private class PartsConsumeVisualization
        {
            private SkeletonAnimation _skeleton;
            private string _animation;
            private AudioSystemPlayer _audio;
            private TrackEntry _animTrack;
            private Action _completeCallback;

            private bool _isPlayingAnim => _animTrack != null && !_animTrack.IsComplete;
            private bool _isPlayingAudio => _audio != null && _audio.IsPlaying;

            public bool IsPlaying => _isPlayingAnim || _isPlayingAudio;

            public PartsConsumeVisualization(SkeletonAnimation skeleton, string animation, AudioSystemPlayer audio)
            {
                _skeleton = skeleton;
                _animation = animation;
                _audio = audio;
            }

            public void Execute(Action completeCallback)
            {
                _completeCallback = completeCallback;

                if (_skeleton != null)
                {
                    _animTrack = _skeleton.AnimationState.SetAnimation(0, _animation, false);
                    _animTrack.Complete += track => TryInvokeCompleteCallback();
                }

                if (_audio != null)
                {
                    _audio.Play();
                    _audio.Stoped += TryInvokeCompleteCallback;
                }

                TryInvokeCompleteCallback();
            }

            private void TryInvokeCompleteCallback()
            {
                if (!IsPlaying)
                {
                    _completeCallback?.Invoke();
                    _completeCallback = null;
                }
            }
        }
    }
}