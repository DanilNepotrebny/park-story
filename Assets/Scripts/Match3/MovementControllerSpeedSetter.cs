﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Utils.Movement;
using UnityEngine;

namespace DreamTeam.Match3
{
    public class MovementControllerSpeedSetter : MonoBehaviour
    {
        [SerializeField] private MovementController _controller;

        /// <summary>
        /// Set movement controller speed from chip speed
        /// </summary>
        /// <param name="presenter"> Target chip presenter </param>
        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void SetSpeed(ChipPresenter presenter)
        {
            _controller.Speed = presenter.FallingSpeed;
        }
    }
}