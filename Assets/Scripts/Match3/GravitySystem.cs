﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.Teleportation;
using DreamTeam.Match3.Wall;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Makes match-3 chips to fall under influence of gravity. Each cell can have own gravity force direction which describes
    /// where the chip falls from this cell. There are two types of falling - straight and side (aka diagonal). Straight falling is
    /// always in priority.
    /// </summary>
    public class GravitySystem : MonoBehaviour, IFieldDestabilizer
    {
        [Inject] private GridSystem _grid;
        [InjectOptional] private WallSystem _wallSystem;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        private readonly List<CellComponent> _cellsToProcess = new List<CellComponent>();
        private bool[,] _processedMap;

        private readonly Dictionary<Vector2Int, Teleport> _teleports = new Dictionary<Vector2Int, Teleport>();

        private List<FallingChip> _fallingChips = new List<FallingChip>();

        public bool IsStable => _cellsToProcess.Count == 0;

        public event Action Stabilized;
        public event Action Destabilized;

        [Inject]
        private void Construct(FieldStabilizationSystem fieldStabilization)
        {
            fieldStabilization.AddDestabilizer(this);
        }

        public void RegisterTeleport(Teleport teleport)
        {
            _teleports.Add(teleport.TargetCell.GridPosition, teleport);
        }

        public ChipComponent FindPreviousFallingChip(ChipComponent chip)
        {
            int chipIdx = _fallingChips.FindIndex(c => c.CellFrom == chip.Cell);
            return chipIdx >= 0 ? _fallingChips[chipIdx].Chip : null;
        }

        public IEnumerable<CellComponent> EnumerateDownstream(
                [NotNull] CellComponent startCell,
                bool checkSides,
                bool acceptEmpty,
                bool checkBarriers = true,
                bool checkWalls = true
            )
        {
            var cells = new List<CellComponent> { startCell };
            int current = 0;

            while (current < cells.Count)
            {
                CellComponent cellFrom = cells[current];
                current++;

                Direction gravity = cellFrom.Gravity;
                CellComponent cellTo;

                // TODO: Duplication with EnumerateFallingDown
                bool checkDownResult = CheckFallingDown(
                        cellFrom,
                        gravity,
                        false,
                        cells,
                        acceptEmpty,
                        out cellTo,
                        checkBarriers,
                        checkWalls
                    );
                if (checkDownResult)
                {
                    yield return cellTo;
                }
                else if (checkSides)
                {
                    bool checkFalling = CheckFallingDown(
                            cellFrom,
                            gravity.RotateCW(),
                            true,
                            cells,
                            acceptEmpty,
                            out cellTo,
                            checkBarriers,
                            checkWalls
                        );
                    if (checkFalling)
                    {
                        yield return cellTo;
                    }

                    checkFalling = CheckFallingDown(
                            cellFrom,
                            gravity.RotateCCW(),
                            true,
                            cells,
                            acceptEmpty,
                            out cellTo,
                            checkBarriers,
                            checkWalls
                        );
                    if (checkFalling)
                    {
                        yield return cellTo;
                    }
                }
            }
        }

        public IEnumerable<CellComponent> EnumerateFallingDown(
                [NotNull] CellComponent cellFrom,
                bool acceptEmpty,
                bool checkBarriers = true,
                bool checkWalls = true
            )
        {
            var cells = new List<CellComponent>();
            Direction gravity = cellFrom.Gravity;
            CellComponent cellTo;

            // TODO: Duplication with EnumerateDownstream
            bool checkFalling = CheckFallingDown(
                    cellFrom,
                    gravity,
                    false,
                    cells,
                    acceptEmpty,
                    out cellTo,
                    checkBarriers,
                    checkWalls
                );
            if (checkFalling)
            {
                yield return cellTo;
            }

            checkFalling = CheckFallingDown(
                    cellFrom,
                    gravity.RotateCW(),
                    true,
                    cells,
                    acceptEmpty,
                    out cellTo,
                    checkBarriers,
                    checkWalls
                );
            if (checkFalling)
            {
                yield return cellTo;
            }

            checkFalling = CheckFallingDown(
                    cellFrom,
                    gravity.RotateCCW(),
                    true,
                    cells,
                    acceptEmpty,
                    out cellTo,
                    checkBarriers,
                    checkWalls
                );
            if (checkFalling)
            {
                yield return cellTo;
            }
        }

        public IEnumerable<CellComponent> EnumerateUpstream(
            [NotNull] CellComponent startCell,
            bool checkSides,
            bool checkBarriers = true,
            bool includingStart = false)
        {
            var processedCells = new List<CellComponent>();
            var stack = new List<CellComponent> { startCell };

            while (stack.Count > 0)
            {
                CellComponent cell = stack[stack.Count - 1];
                stack.RemoveAt(stack.Count - 1);

                if (processedCells.Contains(cell))
                {
                    continue;
                }

                processedCells.Add(cell);

                if (includingStart || cell != startCell)
                {
                    yield return cell;
                }

                if (checkSides)
                {
                    FillCandidates(
                            cell,
                            stack,
                            false,
                            true,
                            checkBarriers,
                            true,
                            true,
                            true
                        );
                }

                FillCandidates(
                        cell,
                        stack,
                        true,
                        false,
                        checkBarriers,
                        true,
                        true,
                        true
                    );
            }
        }

        protected void Start()
        {
            foreach (CellComponent cell in _grid.Cells)
            {
                var generator = cell.GetComponentInChildren<GeneratorComponent>();
                if (generator != null)
                {
                    generator.ChipGenerated += OnChipGenerated;
                }

                cell.AddMovementStopHandler(CellComponent.MovementHandlerType.Falling, OnChipStopped);
                cell.ChipChanged += OnCellChipChanged;
                cell.Stabilized += () => CheckCell(cell);
            }

            _processedMap = new bool[_grid.Width, _grid.Height];

            foreach (CellComponent cell in _grid.Cells)
            {
                if (cell.Chip == null)
                {
                    AddToProcessing(cell);
                }
            }
        }

        protected void FixedUpdate()
        {
            if (_cellsToProcess.Count > 0)
            {
                while (_cellsToProcess.Count > 0)
                {
                    ProcessCell();
                }

                Array.Clear(_processedMap, 0, _processedMap.Length);

                CheckStable();
            }

            if (_fallingChips.Count > 0)
            {
                int i = 0;
                while (i < _fallingChips.Count)
                {
                    ChipComponent chip = _fallingChips[i].Chip;
                    if (!chip.IsInMovingState)
                    {
                        chip.EndMoving();

                        _fallingChips.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
            }
        }

        private void OnCellChipChanged(CellComponent cell, ChipComponent oldChip)
        {
            CheckCell(cell);
        }

        private void OnChipGenerated(CellComponent cell)
        {
            FallChip(cell.Chip, cell);
        }

        private void OnChipStopped(CellComponent cell, ChipComponent.MovingType type)
        {
            // Check next cell according to gravity
            CheckNeighbor(GetDownNeighbor(cell), true);

            // Check side neighbours for ourself and each upper cell (because side falling could have been blocked
            // by movement in downstream earlier, see ProcessSideCandidates).
            foreach (CellComponent nextCell in EnumerateUpstream(cell, checkSides: false, checkBarriers: true, includingStart: true))
            {
                Direction gravity = nextCell.Gravity;

                CheckNeighbor(GetCell(nextCell, gravity.RotateCW()), true);
                CheckNeighbor(GetCell(nextCell, gravity.RotateCCW()), true);
            }
        }

        private bool AddToProcessing(CellComponent cell)
        {
            if (IsCellProcessed(cell))
            {
                return false;
            }

            bool hadCellsToProcess = _cellsToProcess.Count > 0;

            int index = _cellsToProcess.FindIndex(c => c == cell);
            if (index >= 0)
            {
                _cellsToProcess.RemoveAt(index);
            }

            _cellsToProcess.Add(cell);

            if (!hadCellsToProcess)
            {
                Destabilized?.Invoke();
            }

            return true;
        }

        private bool IsCellProcessed(CellComponent cell)
        {
            Vector2Int position = cell.GridPosition;
            return _processedMap[position.x, position.y];
        }

        private void SetCellProcessed(CellComponent cell, bool isProcessed)
        {
            Vector2Int position = cell.GridPosition;
            _processedMap[position.x, position.y] = isProcessed;
        }

        private void ProcessCell()
        {
            CellComponent cell = _cellsToProcess[_cellsToProcess.Count - 1];
            ChipComponent chip = cell.Chip;

            if (chip == null || chip.CanFall)
            {
                // Check next by gravity first
                CellComponent nextCell = GetDownNeighbor(cell);
                if (nextCell != null && AddToProcessing(nextCell))
                {
                    return;
                }

                if (chip == null && cell.IsStable)
                {
                    var candidates = new List<CellComponent>();
                    FillCandidates(cell, candidates, true, false);

                    bool shouldWait;
                    ProcessSideCandidates(cell, candidates, out shouldWait);
                    if (shouldWait)
                    {
                        return;
                    }

                    if (candidates.Count != 0)
                    {
                        CellComponent cellFrom = candidates[_random.Next(candidates.Count)];
                        FallChip(cellFrom.Chip, cell);
                    }
                }
            }

            SetCellProcessed(cell, true);
            _cellsToProcess.Remove(cell);
        }

        private void ProcessSideCandidates(CellComponent cell, List<CellComponent> candidates, out bool shouldWait)
        {
            shouldWait = false;

            if (candidates.Count == 0 && !HasUpstream(cell, false))
            {
                FillCandidates(cell, candidates, false, true);

                for (int i = 0; i < candidates.Count;)
                {
                    CellComponent candidate = candidates[i];

                    if (HasActivityInDownstream(candidate, false) || IsSteady(candidate) && HasUpstream(cell, true))
                    {
                        candidates.RemoveAt(i);
                    }
                    else
                    {
                        if (CheckNeighbor(GetDownNeighbor(candidate), false))
                        {
                            // If some of the side candidates is added to processing we should defer processing of current cell
                            // until all the candidates are processed
                            shouldWait = true;
                        }

                        i++;
                    }
                }
            }
        }

        private void FallChip(ChipComponent chip, CellComponent cell)
        {
            var fallingChip = new FallingChip
            {
                CellFrom = chip.Cell,
                Chip = chip
            };

            int fallingChipIdx = _fallingChips.FindIndex(c => c.Chip == chip);
            if (fallingChipIdx < 0)
            {
                _fallingChips.Add(fallingChip);
            }
            else
            {
                _fallingChips[fallingChipIdx] = fallingChip;
            }

            var movingType = ChipComponent.MovingType.Falling;

            Teleport teleport = FindTeleportBetween(fallingChip.CellFrom, cell);
            if (teleport != null)
            {
                movingType = teleport.MovingType;
            }

            if (!chip.IsMoving)
            {
                chip.StartMoving(movingType);
            }

            cell.ChangeChip(chip, movingType);
        }

        // Does cell have teleport entry or exit
        private bool HasTeleport([NotNull] CellComponent cell)
        {
            return _teleports.Any(pair => pair.Value.SourceCell == cell || pair.Value.TargetCell == cell);
        }

        private CellComponent FindTeleportExit([NotNull] CellComponent cell)
        {
            foreach (var pair in _teleports)
            {
                Teleport teleport = pair.Value;
                if (teleport.SourceCell == cell)
                {
                    return teleport.TargetCell;
                }
            }

            return null;
        }

        private Teleport FindTeleportBetween(CellComponent cellFrom, CellComponent cellTo)
        {
            Teleport teleport;
            if (_teleports.TryGetValue(cellTo.GridPosition, out teleport) &&
                teleport.SourceCell == cellFrom)
            {
                return teleport;
            }

            return null;
        }

        private CellComponent GetDownNeighbor(CellComponent cell)
        {
            if (cell.Gravity != Direction.None)
            {
                CellComponent neighbor = FindTeleportExit(cell);

                if (neighbor != null)
                {
                    return neighbor;
                }

                neighbor = GetCell(cell, cell.Gravity);

                if (neighbor != null && IsFallingPossible(cell, neighbor, true, false))
                {
                    return neighbor;
                }
            }

            return null;
        }

        private bool CheckNeighbor([CanBeNull] CellComponent neighbor, bool mustBeEmpty)
        {
            if (neighbor != null && (!mustBeEmpty || neighbor.Chip == null && neighbor.IsStable))
            {
                return AddToProcessing(neighbor);
            }

            return false;
        }

        private void CheckCell(CellComponent cell)
        {
            if (cell.IsEmpty)
            {
                SetCellProcessed(cell, false);
                AddToProcessing(cell);
            }
            else
            {
                Direction gravity = cell.Gravity;
                CheckNeighbor(GetDownNeighbor(cell), true);
                CheckNeighbor(GetCell(cell, gravity.RotateCW()), true);
                CheckNeighbor(GetCell(cell, gravity.RotateCCW()), true);
            }
        }

        private void FillCandidates(
            [NotNull] CellComponent cell,
            List<CellComponent> candidates,
            bool checkStraight,
            bool checkSides,
            bool checkBarriers = true,
            bool acceptMoving = false,
            bool acceptEmpty = false,
            bool acceptBusy = false)
        {
            Teleport teleport;
            if (checkStraight && _teleports.TryGetValue(cell.GridPosition, out teleport))
            {
                if (teleport.SourceCell != null) // checking SourceCell cause 'cell' is TargetCell of the teleport
                {
                    bool addCandidate = !checkBarriers ||
                        CheckCellSuitable(teleport.SourceCell, acceptMoving, acceptEmpty, acceptBusy);

                    if (addCandidate && !candidates.Contains(teleport.SourceCell))
                    {
                        candidates.Add(teleport.SourceCell);
                    }
                }

                if (!checkSides)
                {
                    return;
                }

                // Teleport has a priority, stop searching in straight direction
                checkStraight = false;
            }

            var directionToCheck = Direction.Up;
            do
            {
                FillCandidates(
                        cell,
                        directionToCheck,
                        candidates,
                        checkStraight,
                        checkSides,
                        checkBarriers,
                        acceptMoving,
                        acceptEmpty,
                        acceptBusy
                    );
                directionToCheck = directionToCheck.RotateCW();
            }
            while (directionToCheck != Direction.Up);
        }

        private void FillCandidates(
                CellComponent cell,
                Direction from,
                List<CellComponent> candidates,
                bool checkStraight,
                bool checkSides,
                bool checkBarriers = true,
                bool acceptMoving = false,
                bool acceptEmpty = false,
                bool acceptBusy = false
            )
        {
            UnityEngine.Debug.Assert(checkStraight || checkSides);

            CellComponent cellFrom = GetCell(cell, from);
            if (cellFrom == null)
            {
                return;
            }

            if (checkBarriers && !CheckCellSuitable(cellFrom, acceptMoving, acceptEmpty, acceptBusy))
            {
                return;
            }

            if (IsFallingPossible(cellFrom, cell, checkStraight, checkSides, checkWalls: checkBarriers) &&
                !candidates.Contains(cellFrom))
            {
                candidates.Add(cellFrom);
            }
        }

        private bool CheckCellSuitable(
            [NotNull] CellComponent cell,
            bool acceptMoving,
            bool acceptEmpty,
            bool acceptBusy)
        {
            if (!acceptBusy && !cell.IsStable)
            {
                return false;
            }

            ChipComponent chip = cell.Chip;
            if (chip == null || chip.IsConsumed)
            {
                return acceptEmpty;
            }

            return chip.CanFall && (acceptMoving || !chip.IsInMovingState);
        }

        private bool IsFallingPossible(
                [NotNull] CellComponent cellFrom,
                [NotNull] CellComponent cellTo,
                bool checkStraight,
                bool checkSides,
                bool checkWalls = true)
        {
            UnityEngine.Debug.Assert(checkStraight || checkSides);

            Direction gravity = cellFrom.Gravity;

            if (gravity == cellTo.Gravity.GetMirrored())
            {
                // Can't fall from cell with opposite gravity
                return false;
            }

            if (checkWalls && _wallSystem != null)
            {
                if (checkStraight)
                {
                    var wallPos = new WallSystem.Position(cellFrom.GridPosition, cellTo.GridPosition);
                    if (_wallSystem.HasWall(wallPos))
                    {
                        return false;
                    }
                }

                if (checkSides)
                {
                    // Have to check different wall restrictions: W1-W2, W3-W4, W1-W3, W2-W4
                    // Here is example (one of the cases)
                    // +--------------------------+
                    // |            ||            |
                    // |            ||            |
                    // |            W2  cell from |
                    // |            ||            |
                    // |            ||            |
                    // +|====W3====|++|====W1====|+
                    // |            ||            |
                    // |            ||            |
                    // |  cell to   W4            |
                    // |            ||            |
                    // |            ||            |
                    // +--------------------------+

                    var wall1Pos = new WallSystem.Position(
                        cellFrom.GridPosition,
                        new Vector2Int(cellFrom.GridPosition.x, cellTo.GridPosition.y));
                    var wall2Pos = new WallSystem.Position(
                        cellFrom.GridPosition,
                        new Vector2Int(cellTo.GridPosition.x, cellFrom.GridPosition.y));
                    var wall3Pos = new WallSystem.Position(
                        cellTo.GridPosition,
                        new Vector2Int(cellTo.GridPosition.x, cellFrom.GridPosition.y));
                    var wall4Pos = new WallSystem.Position(
                        cellTo.GridPosition,
                        new Vector2Int(cellFrom.GridPosition.x, cellTo.GridPosition.y));
                    bool hasWall1 = _wallSystem.HasWall(wall1Pos);
                    bool hasWall2 = _wallSystem.HasWall(wall2Pos);
                    bool hasWall3 = _wallSystem.HasWall(wall3Pos);
                    bool hasWall4 = _wallSystem.HasWall(wall4Pos);
                    if (hasWall1 && hasWall2 || hasWall3 && hasWall4 || hasWall2 && hasWall4 || hasWall1 && hasWall3)
                    {
                        return false;
                    }
                }
            }

            if (checkStraight && FindTeleportBetween(cellFrom, cellTo) != null)
            {
                return true;
            }

            Direction suitableGravity = (cellTo.GridPosition - cellFrom.GridPosition).GetDirection();

            return checkStraight && gravity == suitableGravity ||
                checkSides && (gravity.RotateCW() == suitableGravity || gravity.RotateCCW() == suitableGravity);
        }

        private bool CheckFallingDown(
                CellComponent cellFrom,
                Direction direction,
                bool checkSides,
                List<CellComponent> processedCells,
                bool acceptEmpty,
                out CellComponent cellTo,
                bool checkBarriers = true,
                bool checkWalls = true
            )
        {
            cellTo = GetCell(cellFrom, direction);
            if (cellTo == null)
            {
                cellTo = FindTeleportExit(cellFrom);
                if (cellTo == null || processedCells.Contains(cellTo))
                {
                    return false;
                }

                processedCells.Add(cellTo);
                return true;
            }

            if (checkBarriers && !CheckCellSuitable(cellTo, true, acceptEmpty, true))
            {
                return false;
            }

            if (!IsFallingPossible(cellFrom, cellTo, !checkSides, checkSides, checkWalls: checkWalls) ||
                processedCells.Contains(cellTo))
            {
                return false;
            }

            processedCells.Add(cellTo);
            return true;
        }

        private bool HasUpstream(CellComponent cell, bool checkSides)
        {
            if (checkSides)
            {
                cell = EnumerateUpstream(cell, checkSides: false).FirstOrDefault();
                if (cell == null)
                {
                    return false;
                }

                if (cell.Chip != null)
                {
                    return true;
                }
            }

            foreach (CellComponent nextCell in EnumerateUpstream(cell, checkSides))
            {
                if (nextCell.Chip != null)
                {
                    return true;
                }
            }

            return false;
        }

        private bool CheckCellActivityInDownstream(CellComponent cell)
        {
            ChipComponent chip = cell.Chip;
            if (chip == null)
            {
                return true;
            }

            return !cell.IsStable && chip.IsTotallyConsumed || chip.IsInMovingState;
        }

        private bool HasActivityInDownstream(CellComponent cell, bool checkSides)
        {
            if (checkSides)
            {
                cell = EnumerateDownstream(cell, checkSides: false, acceptEmpty: false).FirstOrDefault();
                if (cell == null)
                {
                    return false;
                }

                if (CheckCellActivityInDownstream(cell))
                {
                    return true;
                }
            }

            foreach (CellComponent nextCell in EnumerateDownstream(cell, checkSides, acceptEmpty: false))
            {
                if (CheckCellActivityInDownstream(nextCell))
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsSteady(CellComponent cell)
        {
            // Check left, up-left and all of their left neighbours are steady.
            if (IsSteadyByNeighbours(cell, true))
            {
                return true;
            }

            // Check right, up-right and all of their right neighbours are steady.
            if (IsSteadyByNeighbours(cell, false))
            {
                return true;
            }

            return false;
        }

        private bool IsSteadyByNeighbours(CellComponent cell, bool isCW)
        {
            Direction gravity = cell.Gravity;
            Direction direction = isCW ? gravity.Rotate90CW() : gravity.Rotate90CCW();

            return IsSteadyByNeighbours(cell, direction, direction) &&
                IsSteadyByNeighbours(cell, isCW ? direction.RotateCW() : direction.RotateCCW(), direction);
        }

        private bool IsSteadyByNeighbours(CellComponent cell, Direction direction, Direction directionNext)
        {
            CellComponent neighbour = GetCell(cell, direction);

            if (neighbour == null)
            {
                return true;
            }

            ChipComponent chip = neighbour.Chip;
            if (chip != null && !chip.IsMoving)
            {
                return true;
            }

            if (chip == null && !HasUpstream(neighbour, false))
            {
                return false;
            }

            return IsSteadyByNeighbours(neighbour, directionNext, directionNext);
        }

        private void CheckStable()
        {
            if (IsStable)
            {
                Stabilized?.Invoke();
            }
        }

        private CellComponent GetCell(CellComponent cell, Direction direction)
        {
            return _grid.GetCell(cell.GridPosition + direction.GetOffset());
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }

        #region Inner types

        private struct FallingChip
        {
            public CellComponent CellFrom;
            public ChipComponent Chip;
        }

        #endregion
    }
}