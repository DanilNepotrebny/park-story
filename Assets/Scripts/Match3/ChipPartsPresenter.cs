﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DreamTeam.Match3
{
    /// <summary>
    /// Presenter for a chip that visually consists of diferrent parts for each health amount.
    /// </summary>
    [ExecuteInEditMode]
    public class ChipPartsPresenter : MonoBehaviour
    {
        [SerializeField] private Group[] _groups;
        [SerializeField] private bool _updateStateOnHealthChanges = true;

        [Inject] private Instantiator _instantiator;

        private ChipComponent _chip;
        private int _lastHealth;
        private HideAction _hideAction;

        private ChipPresenter _chipPresenter;

        public int ChipHealth => _chip != null ? _chip.Health : 0;

        protected GameObject[] ActiveParts => _groups[ChipHealth].parts;

        #if UNITY_EDITOR
        private bool _performUpdate;
        #endif

        /// <summary>
        /// Sets action that will be invoked at the moment of part hiding.
        /// </summary>
        public void SetPartConsumeAction(HideAction action)
        {
            _hideAction = action;
        }

        public int FindMinHealthForPart(GameObject part)
        {
            for (int i = 0; i < _groups.Length; i++)
            {
                if (_groups[i].HasPart(part))
                {
                    return i;
                }
            }

            throw new ArgumentException($"Part {part.name} doesn't belong to any group", nameof(part));
        }

        public void SetChipPresenter(ChipPresenter chipPresenter)
        {
            _chipPresenter = chipPresenter;
        }

        protected virtual void Awake()
        {
            SetChipPresenter(this.GetComponentInParent<ChipPresenter>(true));
        }

        protected void Start()
        {
            _chip = _chipPresenter.ChipComponent;

            if (_updateStateOnHealthChanges)
            {
                _chip.HealthController.ValueChanged += OnHealthChanged;
            }

            #if UNITY_EDITOR
            {
                _lastHealth = EditorUtils.IsPlayingOrWillChangePlaymode() ?
                    _chip.Health :
                    _chip.HealthController.GetEditorApi().MaxHealth;
            }
            #else
            {
                _lastHealth = _chip.Health;
            }
            #endif

            for (int i = 0; i < _groups.Length; i++)
            {
                foreach (var part in _groups[i].parts)
                {
                    // Hide parts for all healthes except the current one
                    if (i != _lastHealth)
                    {
                        part.SetActive(false);
                    }

                    #if DEBUG
                    int lastIdx = i;

                    // Check that there is no situation when part was hidden for a group and becomes visible again for an earlier group.
                    // This is prohibited because inhereted classes are able to delete hidden parts.
                    for (int j = i + 1; j < _groups.Length; j++)
                    {
                        if (_groups[j].HasPart(part))
                        {
                            if (j - lastIdx > 1)
                            {
                                UnityEngine.Debug.LogError(
                                    $"Part {part.name} is hidden for hp {j + 1} and shown again for hp {i + 1}. This is prohibited.");
                                break;
                            }

                            lastIdx = j;
                        }
                    }
                    #endif // DEBUG
                }
            }

            foreach (var obj in _groups[_lastHealth].parts)
            {
                obj.SetActive(true);
            }
        }

        protected virtual void Update()
        {
            #if UNITY_EDITOR
            {
                UpdateEditor();
            }
            #endif
        }

        #if UNITY_EDITOR
        private void OnEnable()
        {
            _performUpdate = PrefabUtility.GetPrefabType(gameObject) == PrefabType.PrefabInstance;
        }

        private void UpdateEditor()
        {
            if (!EditorApplication.isPlayingOrWillChangePlaymode || !_performUpdate)
            {
                return;
            }

            if (PrefabUtility.GetPrefabType(gameObject) != PrefabType.PrefabInstance)
            {
                return;
            }

            if (_chip == null)
            {
                _chip = GetComponentInParent<ChipComponent>();
            }

            int maxHealth = _chip.HealthController.GetEditorApi().MaxHealth;
            if (_lastHealth != maxHealth)
            {
                OnHealthChanged(maxHealth);
            }
        }

        private void OnDisable()
        {
            _performUpdate = false;
        }
        #endif // UNITY_EDITOR

        protected virtual void OnHealthChanged(int health)
        {
            if (_groups.Length < health)
            {
                UnityEngine.Debug.LogError($"There is no enough views for health value {health} in {name}.", this);
                return;
            }

            var hidingParts = new List<GameObject>();
            // Hide parts those are absent in current group
            for (int i = health; i <= _lastHealth; i++)
            {
                Group group = _groups[i];
                if (!group.handleHide)
                {
                    continue;
                }

                if (i == _lastHealth && group.hideParticlesPrefab != null)
                {
                    _instantiator.Instantiate(group.hideParticlesPrefab, transform.position);
                }

                GameObject[] parts = group.parts;
                foreach (GameObject part in parts)
                {
                    if (part == null ||
                        Array.IndexOf(ActiveParts, part) >= 0 ||
                        !part.activeInHierarchy ||
                        hidingParts.Contains(part))
                    {
                        continue;
                    }

                    bool isHiding = _hideAction?.Invoke(part, i) ?? false;
                    if (isHiding)
                    {
                        hidingParts.Add(part);
                    }
                    else
                    {
                        part.SetActive(false);
                    }
                }
            }

            // Show parts
            foreach (GameObject obj in ActiveParts)
            {
                obj.SetActive(true);
            }

            _lastHealth = health;
        }

        #region Inner classes

        public delegate bool HideAction(GameObject part, int health);

        [Serializable]
        private class Group
        {
            [SerializeField] public bool handleHide = true;
            [SerializeField] public ParticleSystem hideParticlesPrefab;
            [SerializeField] public GameObject[] parts;

            public bool HasPart(GameObject part)
            {
                return Array.IndexOf(parts, part) >= 0;
            }
        }

        #endregion Inner classes
    }
}