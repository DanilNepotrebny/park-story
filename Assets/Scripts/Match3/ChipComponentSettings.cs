﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.Requirements;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    [CreateAssetMenu(fileName = "ChipComponentSettings", menuName = "Match 3/Chips/Chip Settings")]
    public class ChipComponentSettings : ScriptableObject
    {
        [SerializeField] private bool _canBeSwapped = true;
        [SerializeField] private bool _canBeShuffled;
        [SerializeField] private bool _canFall = true;
        [SerializeField, EnumFlag] private ImpactFlag _impactFlags;
        [SerializeField] private float _baseMoveWeight = 1;
        [SerializeField] private float _requiredMoveWeight = 1000;
        [SerializeField] private RequirementHelper _helper;
        [SerializeField] private ChipTagSet _chipTagSet;

        public ChipTagSet ChipTagSet => _chipTagSet;

        public bool CanBeSwapped => _canBeSwapped;

        public bool CanBeShuffled => _canBeShuffled;

        public bool CanFall => _canFall;

        public ImpactFlag ImpactFlags => _impactFlags;

        public RequirementHelper Helper => _helper;

        public float BaseMoveWeight => _baseMoveWeight;

        public float RequiredMoveWeight => _requiredMoveWeight;
    }
}