﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if !NO_MATCH3

using System;
using System.Collections.Generic;
using DreamTeam.GameControls;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Match3.Levels;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using ModestTree;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Match3.Bot
{
    public class BotAgent : MonoBehaviour
    {
        [SerializeField, FormerlySerializedAs("_selectionLockAction")]
        private GameControl _controlAction;

        #if MATCH3_BOT_SYSTEM

        [Inject] private BotSystem _botSystem;
        [Inject] private SwapSystem _swapSystem;
        [Inject] private MovesSystem _movesSystem;
        [Inject] private ICurrentLevelInfo _currentLevelInfo;
        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private LevelCompletionSystem _levelCompletionSystem;
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;

        private readonly List<MovesSystem.Move> _moves = new List<MovesSystem.Move>();
        private bool _isReceivedStartEvent;
        private LTDescr _restartDescr;
        private bool _isInitialized;

        public bool IsStarted { get; private set; }

        public event Action Finished;

        public event Action LevelRestartNeeded;

        protected void Awake()
        {
            _gameControlsSystem.AddGameControlStateChangeHandler(_controlAction, OnGameControlStateChanged);
            _fieldStabilizationSystem.AddStabilizationHandler(TryMakeMove, FieldStabilizationHandlerOrder.Bot);

            _levelCompletionSystem.LevelSucceeded += OnLevelSucceeded;
            _levelCompletionSystem.LevelFailing += OnLevelFailing;

            if (!_botSystem.Handle(this))
            {
                gameObject.Dispose();
            }
        }

        protected void OnDestroy()
        {
            if (_restartDescr != null)
            {
                LeanTween.cancel(_restartDescr.id);
                _restartDescr = null;
            }

            _levelCompletionSystem.LevelFailing -= OnLevelFailing;
            _levelCompletionSystem.LevelSucceeded -= OnLevelSucceeded;

            _fieldStabilizationSystem.RemoveStabilizationHandler(TryMakeMove);
            _gameControlsSystem.RemoveGameControlStateChangeHandler(_controlAction, OnGameControlStateChanged);
        }

        protected void Start()
        {
            TimeSpan restartDuration = _botSystem.LevelRestartTime;
            Assert.IsNull(_restartDescr);
            _restartDescr = LeanTween.delayedCall((float)restartDuration.TotalSeconds, OnRestartTimerIsUp);

            _isReceivedStartEvent = true;
            StartBot();
        }

        protected void OnEnable()
        {
            StartBot();
        }

        protected void OnDisable()
        {
            StopBot();
        }

        private void StartBot()
        {
            if (IsStarted || !_isReceivedStartEvent)
            {
                return;
            }

            if (_botSystem.IsLimitlessModeEnabled)
            {
                _levelCompletionSystem.SetLimitationsCheck(() => false);
            }

            IsStarted = true;

            TryMakeMove();
        }

        private void StopBot()
        {
            if (!IsStarted)
            {
                return;
            }

            IsStarted = false;
        }

        private void OnRestartTimerIsUp()
        {
            string warning =
                $"Bot took too much time playing '{_currentLevelInfo.CurrentLevel.ScenePath}'. " +
                "Something might have happened. Restarting the level...";

            UnityEngine.Debug.LogWarning(warning);

            _restartDescr = null;
            LevelRestartNeeded?.Invoke();
        }

        private void OnGameControlStateChanged(GameControl.State state)
        {
            switch (state)
            {
                case GameControl.State.Locked:
                    StopBot();
                    break;
                case GameControl.State.Unlocked:
                    StartBot();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        private void OnLevelSucceeded()
        {
            enabled = false;
            Finished?.Invoke();
        }

        private void OnLevelFailing(IDeferredInvocationHandle failingHandle)
        {
            enabled = false;
            Finished?.Invoke();
        }

        private void TryMakeMove()
        {
            if (!enabled)
            {
                return;
            }

            bool isLocked = _gameControlsSystem.IsControlInState(_controlAction, GameControl.State.Locked);
            if (isLocked)
            {
                return;
            }

            if (!_fieldStabilizationSystem.IsFieldStable)
            {
                return;
            }

            _moves.Clear();
            bool hasAnyMove = _movesSystem.FindMoves(_moves);
            if (!hasAnyMove)
            {
                UnityEngine.Debug.LogWarning("Bot didn't find any move. Waiting for the next field stabilization...");
                return;
            }

            MovesSystem.Move move = _movesSystem.FindTheBestMove(_moves);

            bool isSuccessful = TryInvokingActivationMove(move);
            if (isSuccessful)
            {
                return;
            }

            isSuccessful = TryInvokingSwapMove(move);
            if (isSuccessful)
            {
                return;
            }

            UnityEngine.Debug.LogError(
                "This must not be reached. There is an error in logic. Some case or move might not be covered");
        }

        private bool TryInvokingActivationMove([NotNull] MovesSystem.Move move)
        {
            if (move.Target != null || move.Source == null)
            {
                return false;
            }

            var activator = move.Source.Chip.Presenter.GetComponent<ChipActivator>();
            if (activator == null)
            {
                return false;
            }

            activator.HandleChipActivation();
            return true;
        }

        private bool TryInvokingSwapMove([NotNull] MovesSystem.Move move)
        {
            if (move.Target == null || move.Source == null)
            {
                return false;
            }

            Vector3 moveOffset = move.Target.transform.position - move.Source.transform.position;
            Direction moveDir = _swapSystem.CalculateSwapDirection(new Vector2(moveOffset.x, moveOffset.y));

            _swapSystem.SwapChips(move.Source, moveDir);
            return true;
        }

        #endif // MATCH3_BOT_SYSTEM
    }
}

#endif