﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Match3;
using DreamTeam.Debug;
using DreamTeam.GameControls;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using DreamTeam.Popups;
using DreamTeam.SceneLoading;
using DreamTeam.Screen;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using ModestTree;
using UnityEngine;
using Zenject;
using Assert = UnityEngine.Assertions.Assert;

namespace DreamTeam.Match3.Bot
{
    public class BotSystem : MonoBehaviour
    {
        [SerializeField] private GameControlsMode _controlsMode;

        [SerializeField] private SceneReference _sceneToLoadOnBotFinished;

        [Header("Bot run")] [SerializeField, Range(0.1f, 100)]
        private float _timeScale = 100;

        [SerializeField] private int _targetFrameRate = 120;
        [SerializeField] private bool _clearConsoleOnLevelStart = true;

        [SerializeField, Tooltip("Timer duration after which bot will restart playing level. Time scale is applied")]
        private TimeTicks _levelRestartDuration;

        [Header("Appearence")] [SerializeField]
        private GUIStyle _ingameBotStateStyle = new GUIStyle();

        [SerializeField] private GUIStyle _cheatPanelLevelToggleStyle = new GUIStyle();

        #if MATCH3_BOT_SYSTEM && !NO_MATCH3

        [Inject] private CheatPanel _cheatPanel;
        [Inject] private LevelSystem _levelSystem;
        [Inject] private PopupSystem _popupSystem;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private LoadingSystem _loadingSystem;
        [Inject] private ScreenController _screen;

        public const string Title =
            "Level_name\tSuccessfulness\tReached_limitations\tLevel_money\tBonus_money\tMoves_spent";

        private BotAgent _botAgent;
        private PlayingLevel _lastLoadedLevel;
        private bool _isWaitingForAgent;
        private int _iterationsNumber;
        private float _prevTimeScale;
        private int _prevTargetFrameRate;

        private readonly HashSet<PlayingLevel> _playingLevelsSet =
            new HashSet<PlayingLevel>(new PlayingLevelSettingsComparer());

        private FileStream _outputStream;
        private StreamWriter _outputWriter;
        private string _lastOutputPath;
        private string _outputDirectoryPath;
        private BotRunInfo _currentBotRun;
        private bool _isLimitlessModeEnabled;
        private ActionDisposable _modeDisableHandle;

        private LevelPack _currentCheatlevelPack;

        public bool IsBotRunEnabled { get; private set; }

        public bool IsLimitlessModeEnabled => _currentBotRun?.IsLimitlessModeEnabled ?? false;

        public TimeTicks LevelRestartTime => _levelRestartDuration;

        public event Action BotRunFinished;

        public bool Handle([NotNull] BotAgent botAgent)
        {
            if (!_isWaitingForAgent)
            {
                return false;
            }

            _isWaitingForAgent = false;
            _botAgent = botAgent;
            _botAgent.Finished += () => StartCoroutine(OnBotAgentFinished());
            _botAgent.LevelRestartNeeded += RestartLevel;
            _botAgent.enabled = true;
            return true;
        }

        protected void Awake()
        {
            _outputDirectoryPath = $"{Application.persistentDataPath}/Bot/";
            if (!Directory.Exists(_outputDirectoryPath))
            {
                Directory.CreateDirectory(_outputDirectoryPath);
            }
        }

        protected void Start()
        {
            _cheatPanel.AddDrawer(CheatPanel.Category.Bot, DrawLevelsCheatGUI);
            _cheatPanel.ActivityChanged += OnCheatPanelActivityChanged;
            _analyticsSystem.AddSubscriber<LevelFailedEvent>(OnMatch3LevelFailed);
            _analyticsSystem.AddSubscriber<LevelSucceededEvent>(OnMatch3LevelSucceeded);

            // Disable OnGUI() get called until it is really needed
            enabled = false;
        }

        protected void OnDestroy()
        {
            _modeDisableHandle?.Dispose();
            DeinitializeOutput();
            _cheatPanel.RemoveDrawer(DrawLevelsCheatGUI);
        }

        protected void OnGUI()
        {
            if (_botAgent == null || _currentBotRun == null)
            {
                return;
            }

            string activity = _botAgent.enabled ? "ENABLED" : "DISABLED";
            string isStarted = _botAgent.IsStarted ? "STARTED" : "STOPPED";
            int playedLevelsNumber = _currentBotRun.LevelsSet.Sum(pl => pl.IsPlayed ? 1 : 0);
            string labelText =
                $"BOT {activity} | {isStarted}\n" +
                (_currentBotRun.IsLimitlessModeEnabled ? "Limitless mode\n" : "") +
                $"iteration {_currentBotRun.CurrentIterationNumber}/{_currentBotRun.IterationsNumber}\n" +
                $"level {playedLevelsNumber + 1}/{_currentBotRun.LevelsSet.Count}";

            Vector2 contentSize = _ingameBotStateStyle.CalcSize(new GUIContent(labelText));
            Rect panelRect = new Rect(_screen.Width - contentSize.x, 0, contentSize.x, contentSize.y);
            GUI.Label(panelRect, labelText, _ingameBotStateStyle);
        }

        private void StartBotRun()
        {
            enabled = true;

            _prevTargetFrameRate = Application.targetFrameRate;
            Application.targetFrameRate = _targetFrameRate;

            _prevTimeScale = Time.timeScale;
            Time.timeScale = _timeScale;

            _cheatPanel.Hide();
            _playingLevelsSet.ForEach(pl => pl.IsPlayed = false);

            _currentBotRun = new BotRunInfo(
                    _playingLevelsSet,
                    _iterationsNumber,
                    _isLimitlessModeEnabled
                );
            InitializeOutput();

            BotRunFinished += OnBotRunFinished;
            ReleaseModeDisableHandle();
            _modeDisableHandle = _gameControlsSystem.EnableMode(_controlsMode);
            IsBotRunEnabled = true;

            LoadLevel();
        }

        private void OnBotRunFinished()
        {
            Time.timeScale = _prevTimeScale;
            Application.targetFrameRate = _prevTargetFrameRate;

            IsBotRunEnabled = false;
            ReleaseModeDisableHandle();
            BotRunFinished -= OnBotRunFinished;

            _currentBotRun = null;
            DeinitializeOutput();

            _loadingSystem.LoadScene(_sceneToLoadOnBotFinished.AssetPath);

            enabled = false;
        }

        private void ReleaseModeDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }

        private void LoadLevel(PlayingLevel level = null)
        {
            if (level == null)
            {
                level = _currentBotRun.LevelsSet.FirstOrDefault(pl => !pl.IsPlayed);
            }

            if (level != null)
            {
                _currentBotRun.Advance();
                if (_currentBotRun.CurrentIterationNumber > _currentBotRun.IterationsNumber)
                {
                    _currentBotRun.ResetCurrentIteration();
                    level.IsPlayed = true;
                    LoadLevel();
                    return;
                }

                if (_clearConsoleOnLevelStart)
                {
                    EditorUtils.ClearConsole();
                }

                _isWaitingForAgent = true;
                _lastLoadedLevel = level;
                _loadingSystem.LoadScene(level.Level.ScenePath);
                return;
            }

            BotRunFinished?.Invoke();
        }

        private void RestartLevel()
        {
            Assert.IsNotNull(_lastLoadedLevel);
            LoadLevel(_lastLoadedLevel);
        }

        private void DrawLevelsCheatGUI(Rect areaRect)
        {
            if (_playingLevelsSet.Count == 0)
            {
                LevelSettings loadedLevel = _levelSystem.GetLoadedLevel();
                if (loadedLevel != null)
                {
                    const string text = "Start bot for current level";
                    if (GUILayout.Button(text))
                    {
                        _playingLevelsSet.Add(new PlayingLevel(loadedLevel));
                        StartBotRun();
                    }
                }
                else
                {
                    GUIStyle style = new GUIStyle(GUI.skin.label) { normal = { textColor = Color.red } };
                    const string text = "Select a level to be able to start the bot";
                    GUILayout.Label(text, style);
                }
            }
            else
            {
                if (_botAgent != null)
                {
                    _botAgent.enabled = false;
                }

                const string text = "Start bot for selected levels";
                if (GUILayout.Button(text))
                {
                    StartBotRun();
                }
            }

            GUILayout.BeginHorizontal();
            {
                const string iterLabelText = "Iterations number:";
                GUILayout.Label(iterLabelText);

                string iterInputStr = GUILayout.TextField(_iterationsNumber.ToString());
                int inputIterationsNumber = 0;
                if (int.TryParse(iterInputStr, out inputIterationsNumber))
                {
                    _iterationsNumber = Mathf.Clamp(inputIterationsNumber, 1, int.MaxValue);
                }

                _isLimitlessModeEnabled = GUILayout.Toggle(
                        _isLimitlessModeEnabled,
                        "Limitless mode",
                        _cheatPanelLevelToggleStyle
                    );
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginVertical();
            {
                if (_playingLevelsSet.Count > 0)
                {
                    const string text = "Clear selected";
                    if (GUILayout.Button(text))
                    {
                        _playingLevelsSet.Clear();
                    }
                }

                #if UNITY_EDITOR
                string fileManagerText = "Open dest folder";
                if (GUILayout.Button(fileManagerText))
                {
                    EditorUtils.ShowInFileManager(_outputDirectoryPath);
                }
                #endif

                if (_currentCheatlevelPack != null && GUILayout.Button("Add all to selection"))
                {
                    foreach (var level in _currentCheatlevelPack.Levels)
                    {
                        _playingLevelsSet.Add(new PlayingLevel(level));
                    }
                }

                LevelSystem.ICheatApi levelSystemCheatApi = _levelSystem;

                levelSystemCheatApi.DrawLevelsCheats(
                        ref _currentCheatlevelPack,
                        level =>
                        {
                            PlayingLevel pl = _playingLevelsSet.FirstOrDefault(
                                    playingLevel => playingLevel.Level == level
                                );
                            bool wasPressed = pl != null;
                            bool isPressed = GUILayout.Toggle(wasPressed, level.SceneName, _cheatPanelLevelToggleStyle);
                            if (isPressed)
                            {
                                _playingLevelsSet.Add(new PlayingLevel(level));
                            }
                            else
                            {
                                _playingLevelsSet.Remove(pl);
                            }
                        }
                    );
            }
            GUILayout.EndVertical();
        }

        private IEnumerator OnBotAgentFinished()
        {
            _botAgent = null;
            yield return new WaitWhile(() => _popupSystem.IsPopupLoading);

            _popupSystem.HideAll(() => LoadLevel());
        }

        private void OnCheatPanelActivityChanged(bool isEnabled)
        {
            if (_botAgent != null)
            {
                _botAgent.enabled = !isEnabled;
            }
        }

        private void OnMatch3LevelFailed(LevelFailedEvent failedEvent)
        {
            int movesAmount = failedEvent.LimitationInstances.OfType<MovesRequirement.Instance>()
                .Select(req => req.RequiredCount)
                .FirstOrDefault();
            string reachedLimitationInfo = GetReachedLimitationInfo(failedEvent.LimitationInstances);
            PrintToOutput(
                    $"{failedEvent.LevelInfo.CurrentLevel.name}\tfailure\t{reachedLimitationInfo}\t0\t0\t{movesAmount}"
                );
        }

        private void OnMatch3LevelSucceeded(LevelSucceededEvent succeededEvent)
        {
            string reachedLimitationInfo = GetReachedLimitationInfo(succeededEvent.LimitationInstances);
            PrintToOutput(
                    $"{succeededEvent.LevelInfo.CurrentLevel.name}\t" +
                    $"success\t" +
                    $"{reachedLimitationInfo}\t" +
                    $"{succeededEvent.LevelsMoneyEarned}\t" +
                    $"{succeededEvent.BonusMoneyEarned}\t" +
                    $"{succeededEvent.RemainingMoves}"
                );
        }

        private static string GetReachedLimitationInfo(
                IReadOnlyList<Requirement.Instance> limitationInstances
            )
        {
            string reachedLimitationsStr = "{";
            foreach (Requirement.Instance limitation in limitationInstances)
            {
                if (limitation.IsReached)
                {
                    reachedLimitationsStr += $"{limitation.BaseRequirement.GetType().Name}|";
                }
            }

            if (reachedLimitationsStr.EndsWith("|"))
            {
                reachedLimitationsStr =
                    reachedLimitationsStr.Remove(reachedLimitationsStr.Length - 1); // remove last '|'
            }

            reachedLimitationsStr += "}";
            return reachedLimitationsStr;
        }

        private void PrintToOutput(string str)
        {
            if (_outputWriter != null)
            {
                _outputWriter.WriteLine(str);
                _outputWriter.Flush();
            }
            else if (InitializeOutput(false))
            {
                _outputWriter.WriteLine(str);
                _outputWriter.Flush();
                DeinitializeOutput();
            }
        }

        private bool InitializeOutput(bool createNewFile = true)
        {
            DeinitializeOutput();

            if (createNewFile)
            {
                _lastOutputPath = Path.Combine(_outputDirectoryPath, $"bot_run_{DateTime.Now:dd-MM-yyyy HH-mm-ss}.txt");
                _outputStream = File.OpenWrite(_lastOutputPath);
                _outputWriter = new StreamWriter(_outputStream);

                if (IsLimitlessModeEnabled)
                {
                    PrintToOutput("With limitless mode");
                }

                PrintToOutput(Title);

                return true;
            }
            else if (_lastOutputPath != null)
            {
                _outputWriter = File.AppendText(_lastOutputPath);
                return true;
            }

            return false;
        }

        private void DeinitializeOutput()
        {
            _outputWriter?.Dispose();
            _outputWriter = null;
            _outputStream?.Dispose();
            _outputStream = null;
        }

        #region Inner types

        private class BotRunInfo
        {
            public HashSet<PlayingLevel> LevelsSet { get; }

            public int IterationsNumber { get; }

            public int CurrentIterationNumber { get; private set; }

            public bool IsLimitlessModeEnabled { get; }

            public BotRunInfo(HashSet<PlayingLevel> levels, int iterationsNumber, bool isLimitlessMode)
            {
                LevelsSet = levels;
                IterationsNumber = iterationsNumber;
                IsLimitlessModeEnabled = isLimitlessMode;
            }

            public void Advance()
            {
                CurrentIterationNumber += 1;
            }

            public void ResetCurrentIteration()
            {
                CurrentIterationNumber = 0;
            }
        }

        private class PlayingLevel
        {
            public LevelSettings Level { get; }
            public bool IsPlayed { get; set; }

            public PlayingLevel(LevelSettings level)
            {
                Level = level;
                IsPlayed = false;
            }
        }

        private struct PlayingLevelSettingsComparer : IEqualityComparer<PlayingLevel>
        {
            public bool Equals(PlayingLevel x, PlayingLevel y)
            {
                return x.Level.Equals(y.Level);
            }

            public int GetHashCode(PlayingLevel obj)
            {
                return obj.Level.GetHashCode();
            }
        }

        #endregion Inner types

        #endif
    }
}