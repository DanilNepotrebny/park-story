﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.GameControls;
using DreamTeam.Match3.Wall;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Match3
{
    public class SwapSystem : MonoBehaviour
    {
        [SerializeField] private float _detectionAngle = 30f;
        [SerializeField] private SwapEffect[] _swapEffects;

        [SerializeField, FormerlySerializedAs("_selectionMode")]
        private GameControlsMode _controlsMode;

        [Inject] private GridSystem _grid;
        [Inject] private Instantiator _instantiator;
        [Inject] private GameControlsSystem _gameControlsSystem;
        [InjectOptional] private WallSystem _wallSystem;

        private readonly List<SwapData> _swaps = new List<SwapData>();
        private readonly List<ISwapHandler> _swappedEventHandlers = new List<ISwapHandler>();
        private List<ISwapRestriction> _swapRestrictions = new List<ISwapRestriction>();
        private ActionDisposable _modeDisableHandle;

        public event Action SwapsFinished;
        public event SwapDeniedDelegate SwapDenied;
        public event Action<List<Vector2Int>> SwapSucceeding;
        public event Action<List<Vector2Int>> SwapSucceed;
        public event Action SwapingIn;
        public event Action SwapingBack;

        public void RegisterSwapHandler(ISwapHandler handler)
        {
            _swappedEventHandlers.Add(handler);
        }

        public void RegisterSwapRestriction([NotNull] ISwapRestriction restriction)
        {
            _swapRestrictions.Add(restriction);
        }

        public void UnregisterSwapRestriction([NotNull] ISwapRestriction restriction)
        {
            _swapRestrictions.Remove(restriction);
        }

        /// <summary> Checks whether specified cells can start swapping process with each other. </summary>
        public bool CanBeSwapped([NotNull] CellComponent from, [NotNull] CellComponent to)
        {
            bool blockByWall = _wallSystem?.HasWall(new WallSystem.Position(from.GridPosition, to.GridPosition)) ??
                false;

            return !(from.Chip == null && to.Chip == null) &&
                !blockByWall &&
                CanBeSwapped(from) &&
                CanBeSwapped(to) &&
                _swapRestrictions.All(restriction => restriction.CanBeSwapped(from, to)) &&
                Vector2Int.Distance(to.GridPosition, from.GridPosition) == 1;
        }

        public void SwapChips([NotNull] CellComponent cellFrom, Direction dir)
        {
            Vector2Int nextPos = _grid.GetGridPosition(cellFrom) + dir.GetOffset();

            CellComponent cellTo = _grid.GetCell(nextPos);
            if (cellTo == null || !CanBeSwapped(cellFrom, cellTo))
            {
                SwapDenied?.Invoke(cellFrom, dir);
                return;
            }

            bool isStarted = StartSwapping(new SwapData(this, cellFrom, cellTo));
            if (isStarted)
            {
                SwapingIn?.Invoke();
                CreateSwapEffects(cellFrom, cellTo);
            }
        }

        public Direction CalculateSwapDirection(Vector2 delta)
        {
            return delta.CalculateNearestAxis(_detectionAngle);
        }

        protected void Start()
        {
            foreach (CellComponent cell in _grid.Cells)
            {
                cell.AddMovementStopHandler(CellComponent.MovementHandlerType.Swapping, OnChipMovementStopped);
            }
        }

        protected void OnDestroy()
        {
            ReleaseModeDisableHandle();
        }

        private void OnChipMovementStopped(CellComponent cell, ChipComponent.MovingType type)
        {
            if (type != ChipComponent.MovingType.Swapping)
            {
                return;
            }

            SwapData data = _swaps.Find(d => d.CellFrom == cell || d.CellTo == cell);
            Assert.IsNotNull(data, $"Swap for cell {cell.name} is not found");
            data.OnChipSwapped();
        }

        private bool CanBeSwapped([NotNull] CellComponent cell)
        {
            // Note that we return true for an empty cell because empty cell can
            // take part in a swapping process.

            ChipComponent chip = cell.Chip;
            return chip == null || chip.CanBeSwapped && cell.IsStable && !chip.IsMoving;
        }

        private bool StartSwapping(SwapData swap)
        {
            foreach (SwapData existedSwap in _swaps)
            {
                if (swap.CellFrom == existedSwap.CellFrom ||
                    swap.CellTo == existedSwap.CellFrom)
                {
                    return false;
                }

                if (swap.CellFrom == existedSwap.CellTo ||
                    swap.CellTo == existedSwap.CellTo)
                {
                    return false;
                }
            }

            if (!_gameControlsSystem.IsModeEnabled(_controlsMode))
            {
                Assert.IsNull(_modeDisableHandle, "_modeDisableHandle != null");
                _modeDisableHandle = _gameControlsSystem.EnableMode(_controlsMode);
            }

            _swaps.Add(swap);
            swap.StartSwapping();
            return true;
        }

        private void CreateSwapEffects(CellComponent cellFrom, CellComponent cellTo)
        {
            foreach (SwapEffect swapEffect in _swapEffects)
            {
                Vector3 effectPosition = (cellFrom.transform.position + cellTo.transform.position) / 2f;
                GameObject effect = _instantiator.Instantiate(swapEffect.Prefab, effectPosition);

                if (!swapEffect.AlignToMovement)
                {
                    continue;
                }

                Vector2Int gridOffset = _grid.GetGridPosition(cellTo) - _grid.GetGridPosition(cellFrom);
                bool needsFlip;

                if (gridOffset.x == 0)
                {
                    effect.transform.Rotate(Vector3.forward, 90f);

                    needsFlip = gridOffset.y < 0;
                }
                else
                {
                    needsFlip = gridOffset.x < 0;
                }

                if (needsFlip)
                {
                    Vector3 scale = effect.transform.localScale;
                    effect.transform.localScale = new Vector3(-scale.x, scale.y, scale.z);
                }
            }
        }

        private void ProcessSwappingEnd(SwapData data)
        {
            _swaps.Remove(data);

            bool shouldSwapSucceed = false;

            foreach (ISwapHandler handler in _swappedEventHandlers)
            {
                shouldSwapSucceed |= handler.ShouldSwapSucceed(data.CellFrom, data.CellTo);
            }

            if (shouldSwapSucceed)
            {
                var swapCells = new List<Vector2Int> { data.CellFrom.GridPosition, data.CellTo.GridPosition };
                SwapSucceeding?.Invoke(swapCells);

                foreach (ISwapHandler handler in _swappedEventHandlers)
                {
                    if (handler.ShouldSwapSucceed(data.CellFrom, data.CellTo))
                    {
                        handler.HandleSwap(data.CellFrom, data.CellTo);
                    }
                }

                SwapSucceed?.Invoke(swapCells);
            }
            else if (data.CanSwapBack)
            {
                data.CanSwapBack = false;
                SwapingBack?.Invoke();
                StartSwapping(data);
            }

            if (_swaps.Count == 0)
            {
                Assert.IsTrue(_gameControlsSystem.IsModeEnabled(_controlsMode));
                ReleaseModeDisableHandle();
                SwapsFinished?.Invoke();
            }
        }

        private void ReleaseModeDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }

        #region Inner types

        public interface ISwapHandler
        {
            bool ShouldSwapSucceed([NotNull] CellComponent cellFrom, [NotNull] CellComponent cellTo);
            void HandleSwap([NotNull] CellComponent cellFrom, [NotNull] CellComponent cellTo);
        }

        [Serializable]
        private class SwapEffect
        {
            [SerializeField] public GameObject Prefab;
            [SerializeField] public bool AlignToMovement;
        }

        private class SwapData
        {
            public readonly CellComponent CellFrom;
            public readonly CellComponent CellTo;
            public bool CanSwapBack = true;

            private readonly SwapSystem _swapSystem;
            private int _counter;

            private ChipComponent _chipFrom;
            private ChipComponent _chipTo;

            public SwapData(
                    [NotNull] SwapSystem system,
                    [NotNull] CellComponent cellFrom,
                    [NotNull] CellComponent cellTo
                )
            {
                CellFrom = cellFrom;
                _chipFrom = CellFrom.Chip;

                CellTo = cellTo;
                _chipTo = CellTo.Chip;

                _swapSystem = system;
            }

            public void StartSwapping()
            {
                _counter = 0;

                StartMoving(CellFrom);
                StartMoving(CellTo);

                CellFrom.SwapChips(CellTo, ChipComponent.MovingType.Swapping);
            }

            public void OnChipSwapped()
            {
                _counter--;

                if (_counter == 0)
                {
                    EndSwapping();
                }
            }

            private void StartMoving(CellComponent cell)
            {
                cell.BeginProcess(CellComponent.ProcessType.Swapping);

                ChipComponent chip = cell.Chip;
                if (chip != null)
                {
                    chip.StartMoving(ChipComponent.MovingType.Swapping);
                    _counter++;
                }
            }

            private void EndSwapping()
            {
                Assert.IsTrue(_counter == 0);

                _swapSystem.ProcessSwappingEnd(this);

                CellFrom.EndProcess(CellComponent.ProcessType.Swapping);
                CellTo.EndProcess(CellComponent.ProcessType.Swapping);

                _chipFrom?.EndMoving();
                _chipTo?.EndMoving();
            }
        }

        public delegate void SwapDeniedDelegate(CellComponent cellFrom, Direction dir);

        #endregion Inner types
    }

    public interface ISwapRestriction
    {
        bool CanBeSwapped([NotNull] CellComponent from, [NotNull] CellComponent to);
    }
}