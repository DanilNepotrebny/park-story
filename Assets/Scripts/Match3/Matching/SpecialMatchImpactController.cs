﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.Matching
{
    /// <summary>
    /// Controls special match animation. This controller is setted to every single chip that should fly
    /// into created special chip.
    /// </summary>
    public class SpecialMatchImpactController : IImpactController, IFieldDestabilizer
    {
        private FieldStabilizationSystem _stabilizationSystem;
        private ChipComponent _specialChip;
        private List<ChipRecord> _records = new List<ChipRecord>();
        private bool _isAnimationPlaying;
        private CellComponent _pivotCell;
        private SpecialMatchPresenter _presenter;

        private int _movementCounter; // use SetMovementCounter(..)

        private IImpactController _replacementImpactController;

        private IDeferredInvocationHandle _containerImpactHandle;
        private CellComponent _containerCell;

        private readonly int _flashingAmountPropertyId = Shader.PropertyToID("_FlashAmount");

        public bool IsAnimationPlaying
        {
            get { return _isAnimationPlaying; }
            private set
            {
                if (_isAnimationPlaying == value)
                {
                    return;
                }

                _isAnimationPlaying = value;
                CheckStabilization();
            }
        }

        public bool IsStable => !IsAnimationPlaying && _movementCounter == 0;

        public event Action Stabilized;
        public event Action Destabilized;

        public SpecialMatchImpactController(
                Match match,
                ChipComponent specialChip,
                ChipComponent chipToReplace,
                SpecialMatchPresenter presenter,
                GridSystem grid,
                Instantiator instantiator,
                FieldStabilizationSystem stabilizationSystem
            )
        {
            _stabilizationSystem = stabilizationSystem;
            _stabilizationSystem.AddDestabilizer(this);

            _presenter = presenter;
            _specialChip = specialChip;

            _pivotCell = grid[match.Pivot.x, match.Pivot.y];
            _specialChip.transform.position = _pivotCell.transform.position;

            _replacementImpactController = _specialChip.Presenter.GetComponent<ImmediateReplacementPresenter>();

            foreach (Vector2Int pos in match)
            {
                ChipComponent chip = grid[pos.x, pos.y].Chip;
                UnityEngine.Debug.Assert(chip != null);

                chip.AddImpactController(this);
                chip.Impacted += OnImpacted;
            }

            instantiator.Instantiate(_presenter.MatchEffect, _specialChip.transform.position);

            IsAnimationPlaying = true;
            _specialChip.Presenter.SortingOptions.PushSortingLayer(_presenter.SpecialChipSortingLayer);
            _specialChip.Presenter.SetTrigger(_presenter.SpecialChipTrigger);
            _specialChip.Presenter.AddAnimationStateExitHandler(_presenter.SpecialChipExitState, OnAnimationEnd);

            _containerCell = chipToReplace?.Cell;
        }

        private void OnImpacted(ChipComponent chip)
        {
            chip.RemoveImpactController(this);
            chip.Impacted -= OnImpacted;
        }

        private void OnAnimationEnd()
        {
            IsAnimationPlaying = false;
            CheckProcessesEnded();
            _stabilizationSystem.RemoveDestabilizer(this);
        }

        private void OnMovementCompleted(ImpactFlag impactType, [NotNull] IDeferredInvocationHandle impactEndHandle)
        {
            SetMovementCounter(_movementCounter - 1);
            if (_movementCounter == 0)
            {
                foreach (ChipRecord record in _records)
                {
                    var chip = record.chip;
                    chip.Presenter.ResetMaterial();

                    // Using replacement controller to hide chip immediately except for the
                    // goal chips (which controller has higher priority)
                    chip.AddImpactController(_replacementImpactController);

                    chip.ExecuteNextImpactControllers(this, impactType, impactEndHandle);
                }
            }

            CheckProcessesEnded();
        }

        private void CheckProcessesEnded()
        {
            if (_movementCounter == 0 && !IsAnimationPlaying)
            {
                foreach (ChipRecord record in _records)
                {
                    record.Shutdown();
                }
                _records.Clear();

                _specialChip.Presenter.SortingOptions.PopSortingLayer();

                if (_containerImpactHandle != null)
                {
                    _containerImpactHandle.Unlock();
                    _containerImpactHandle = null;
                }
            }
        }

        private void CheckStabilization()
        {
            if (IsStable)
            {
                Stabilized?.Invoke();
            }
            else
            {
                Destabilized?.Invoke();
            }
        }

        private void SetMovementCounter(int value)
        {
            Assert.IsTrue(value >= 0, $"Trying to set movement counter negative value");
            if (value == _movementCounter)
            {
                return;
            }

            bool invokeStabilizationCheck = value == 0 || _movementCounter == 0;
            _movementCounter = value;

            if (invokeStabilizationCheck)
            {
                CheckStabilization();
            }
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }

        #region IImpactController

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.SpecialMatch;
        }

        void IImpactController.OnImpact(
            ChipComponent chip,
            ImpactFlag impactType,
            IDeferredInvocationHandle impactEndHandle,
            ref bool isModal)
        {
            if (chip.Cell == _containerCell)
            {
                // Rare case when pivot of the special match is a container. In this case we will insert special chip
                // into container. Here we lock impact of the container to defer insertion of a special chip into
                // container until whole special match animation is completed.
                _containerImpactHandle = impactEndHandle.Lock();
                return;
            }

            if (chip.IsTotallyConsumed)
            {
                isModal = true;

                _records.Add(new ChipRecord(chip, impactEndHandle));

                var descriptor = LeanTween.move(
                    chip.Presenter.gameObject,
                    _specialChip.transform.position,
                    _presenter.MovingTime);
                descriptor.setOnComplete(() => OnMovementCompleted(impactType, impactEndHandle));
                _presenter.MovingEasing.SetupDescriptor(descriptor);

                var chipPresenter = chip.Presenter;
                chipPresenter.SetMaterial(_presenter.ChipMaterial);
                descriptor.setOnUpdateRatio(
                        (value, ratio) =>
                        {
                            chipPresenter.SetMaterialFloat(
                                    _flashingAmountPropertyId,
                                    _presenter.FlashingEasing.Evaluate(ratio)
                                );
                        }
                    );

                SetMovementCounter(_movementCounter + 1);

                if (chip.Cell != _pivotCell)
                {
                    LeanTween.delayedCall(
                            _presenter.gameObject,
                            _presenter.CellReleaseDelay,
                            () =>
                            {
                                if (chip.Cell == _pivotCell)
                                {
                                    chip.Cell.Chip = null;
                                }
                            }
                        );
                }
            }
        }

        void IImpactController.Cancel()
        {
            // nothing to do
        }

        #endregion IImpactController

        #region Inner classes

        /// <summary>
        /// Factory for creating special match impact controllers. See <see cref="SpecialMatchImpactController"/> constructor for
        /// parameters.
        /// </summary>
        public class Factory : Factory<Match, ChipComponent, ChipComponent, SpecialMatchPresenter, SpecialMatchImpactController>
        {
        }

        private struct ChipRecord
        {
            public readonly ChipComponent chip;

            private IDeferredInvocationHandle _handle;

            public ChipRecord(ChipComponent chip, IDeferredInvocationHandle handle)
            {
                this.chip = chip;

                _handle = handle.Lock();
            }

            public void Shutdown()
            {
                _handle.Unlock();
                _handle = null;
            }
        }

        #endregion
    }
}