// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.FieldStabilization;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.Matching
{
    public partial class MatchSystem : MonoBehaviour, SwapSystem.ISwapHandler, IFieldDestabilizer
    {
        [SerializeField] private int _minMatchLength = 3;
        [SerializeField] private int _matchImpact = -1;
        [SerializeField] private float _matchTimeout = 0.1f;
        [SerializeField] private SpecialMatch[] _specialMatches;

        [Space]
        [SerializeField, Tooltip("2x2 matches")]
        private bool _squareMatchesEnabled;

        [SerializeField] private SpecialMatch _squareMatch;

        [Inject] private GridSystem _grid;
        [Inject] private GenerationSystem _generationSystem;
        [Inject] private ImpactSystem _impactSystem;

        private readonly Dictionary<ChipComponent, ChipComponent> _pendingSpecialChips =
            new Dictionary<ChipComponent, ChipComponent>();
        private readonly List<CellComponent> _cellsToCheck = new List<CellComponent>();
        private readonly List<PendingMatch> _pendingMatches = new List<PendingMatch>();

        private Action _stabilized;
        private Action _destabilized;
        private int _matchSeriesCount;

        private ChipComponent _swappedChip;

        private bool _isSystemStable;

        public int MinMatchLength => _minMatchLength;

        public bool HasMatches => _grid.Cells.Any(cell => HasMatch(cell.GridPosition));

        private bool _isStable
        {
            get { return _isSystemStable; }

            set
            {
                if (_isSystemStable != value)
                {
                    _isSystemStable = value;

                    if (_isSystemStable)
                    {
                        _stabilized?.Invoke();
                    }
                    else
                    {
                        _destabilized?.Invoke();
                    }
                }
            }
        }

        public event Action<int> MatchPerformed;

        /// <summary>
        /// Called before special match has occurred.
        /// </summary>
        public event SpecialMatchOccurringDelegate SpecialMatchOccurring;

        [Inject]
        private void Construct(
                GridSystem grid,
                GravitySystem gravity,
                GenerationSystem generationSystem,
                SwapSystem swapSystem,
                FieldStabilizationSystem fieldStabilizationSystem,
                ImpactSystem impactSystem
            )
        {
            _grid = grid;
            _generationSystem = generationSystem;
            _impactSystem = impactSystem;

            fieldStabilizationSystem.AddDestabilizer(this);
            swapSystem.RegisterSwapHandler(this);
        }

        /// <summary>
        /// Finds match at position
        /// </summary>
        public Match FindMatch(Vector2Int position)
        {
            Match match = FindMatch(position, true) ?? FindMatch(position, false);
            Match squareMatch = FindSquareMatch(position);

            return ChooseMatch(match, squareMatch);
        }

        /// <summary>
        /// Finds any match
        /// </summary>
        public Match FindMatch(Predicate<Match> validator)
        {
            foreach (CellComponent cell in _grid.Cells)
            {
                Match match = FindMatch(cell.GridPosition, true);
                if (match != null && !validator.Invoke(match))
                {
                    match = null;
                }

                if (match == null)
                {
                    match = FindMatch(cell.GridPosition, false);
                    if (match != null && !validator.Invoke(match))
                    {
                        match = null;
                    }
                }

                Match squareMatch = FindSquareMatch(cell.GridPosition);
                if (squareMatch != null && !validator.Invoke(squareMatch))
                {
                    squareMatch = null;
                }

                Match chosenMatch = ChooseMatch(match, squareMatch);
                if (chosenMatch != null)
                {
                    return chosenMatch;
                }
            }

            return null;
        }

        public bool IsMatchable(ChipComponent chip1, ChipComponent chip2)
        {
            if (chip1 == null || chip2 == null)
            {
                return false;
            }

            var color = chip1.Tags.Get<ColorChipTag>();
            return color != null && chip2.Tags.Has(color);
        }

        public List<Impact> ScheduleImpact([NotNull] IEnumerable<Vector2Int> positions)
        {
            var cells = new List<CellComponent>();

            foreach (Vector2Int pos in positions)
            {
                cells.Add(_grid[pos.x, pos.y]);
            }

            List<Impact> impacts = _impactSystem.ScheduleImpact(cells, _matchImpact, ImpactFlag.Match);
            return impacts;
        }

        public bool IsSpecialMatch([NotNull] Match match)
        {
            return GetSpecialMatchPrefab(match) != null;
        }

        protected void Awake()
        {
            Array.Sort(_specialMatches, new SpecialMatchComparer());

            if (_generationSystem != null)
            {
                _generationSystem.FieldGenerated += OnFieldGenerated;
            }
            foreach (CellComponent cell in _grid.Cells)
            {
                cell.AddMovementFinishHandler(CellComponent.MovementHandlerType.Matching, OnChipMovementFinished);
                cell.Stabilized += () => TryConsumeMatch(cell.GridPosition);
            }
        }

        protected void FixedUpdate()
        {
            for (int i = 0; i < _pendingMatches.Count;)
            {
                PendingMatch pendingMatch = _pendingMatches[i];
                if (pendingMatch.Update())
                {
                    _pendingMatches.RemoveAt(i);
                    UpdateStability();
                }
                else
                {
                    i++;
                }
            }

            int count = _cellsToCheck.Count;
            for (int i = 0; i < count; i++)
            {
                TryConsumeMatch(_cellsToCheck[i].GridPosition);
            }
            _cellsToCheck.RemoveRange(0, count);

            UpdateStability();
        }

        private void UpdateStability()
        {
            _isStable = _pendingMatches.Count == 0 && _cellsToCheck.Count == 0;

            if (_isStable)
            {
                _matchSeriesCount = 0;
            }
        }

        private bool HasMatch(Vector2Int position)
        {
            Match foundMatch = FindMatch(position);
            return foundMatch != null;
        }

        private Match ChooseMatch([CanBeNull] Match match, [CanBeNull] Match squareMatch)
        {
            if (match == null && squareMatch == null)
            {
                return null;
            }

            int matchLength = match?.Count ?? 0;
            if (matchLength >= _squareMatch.ChipCount)
            {
                return match;
            }

            int squareMatchLength = squareMatch?.Count ?? 0;

            return squareMatchLength > matchLength ? squareMatch : match;
        }

        private void TryConsumeMatch(Vector2Int position, bool allowsPending = true)
        {
            Match match = FindMatch(position);
            if (match == null)
            {
                return;
            }

            List<PendingMatch> foundPendingMatches = FindPendingMatches(match);

            PendingMatch pendingMatch = null;
            if (foundPendingMatches.Count > 1)
            {
                var pendingMatchesToCancel = new List<PendingMatch>();
                foreach (PendingMatch pm in foundPendingMatches)
                {
                    _pendingMatches.Remove(pm);
                    pendingMatchesToCancel.Add(pm);
                }
                foundPendingMatches.Clear();
                pendingMatch = new PendingMatch(this);
                _pendingMatches.Add(pendingMatch);
                pendingMatch.SetMatch(match);

                pendingMatchesToCancel.ForEach(p => p.Cancel());

                // we have to find match again, because after Cancel TryConsume might be called again
                match = FindMatch(position);
                if (match == null)
                {
                    return;
                }
            }
            else
            {
                pendingMatch = foundPendingMatches.FirstOrDefault();

                Match newPendingMatch = match;
                if (pendingMatch == null)
                {
                    pendingMatch = new PendingMatch(this);
                    _pendingMatches.Add(pendingMatch);
                }
                else
                {
                    if (pendingMatch.Match.IsSquareMatch && !match.IsSquareMatch)
                    {
                        newPendingMatch = ChooseMatch(match, pendingMatch.Match);
                    }
                    else if (!pendingMatch.Match.IsSquareMatch && match.IsSquareMatch)
                    {
                        newPendingMatch = ChooseMatch(pendingMatch.Match, match);
                    }
                    else
                    {
                        newPendingMatch = match.Count > pendingMatch.Match.Count ? match : pendingMatch.Match;
                    }
                }

                pendingMatch.SetMatch(newPendingMatch);
            }

            MatchPerformed?.Invoke(++_matchSeriesCount);

            if (!allowsPending)
            {
                pendingMatch.Consume();
                _pendingMatches.Remove(pendingMatch);
            }

            UpdateStability();
        }

        private void OnChipMovementFinished(CellComponent cell, ChipComponent.MovingType type)
        {
            if (!_cellsToCheck.Contains(cell))
            {
                _cellsToCheck.Add(cell);
            }

            UpdateStability();
        }

        private void OnFieldGenerated()
        {
            foreach (CellComponent cell in _grid.Cells)
            {
                Vector2Int pos = _grid.GetGridPosition(cell);
                TryConsumeMatch(pos);
            }
        }

        private ChipComponent GetSpecialMatchPrefab([NotNull] Match match)
        {
            if (match.IsSquareMatch)
            {
                return _squareMatch.SpecialChip;
            }

            ChipComponent prefab = null;
            foreach (SpecialMatch m in _specialMatches)
            {
                if (m.ChipCount > match.Count)
                {
                    break;
                }
                prefab = m.SpecialChip;
            }

            return prefab;
        }

        private void TryPrepareSpecialMatch([NotNull] Match match)
        {
            ChipComponent specialChipPrefab = GetSpecialMatchPrefab(match);
            if (specialChipPrefab == null)
            {
                return;
            }

            CellComponent pivotCell = _grid[match.Pivot.x, match.Pivot.y];
            ChipComponent pivotChip = pivotCell.Chip;
            if (_pendingSpecialChips.ContainsKey(pivotChip))
            {
                UnityEngine.Debug.LogError($"Preparing special match {match} but having similar one " +
                    $"at {pivotCell} in pending");
                return;
            }

            ChipComponent specialChip = _generationSystem.InstantiateChip(specialChipPrefab);

            // Rare case when the pivot of a match is a container with chip. In this case we'll replace that chip
            // with a special chip created. If container will remain alive after a match, special chip will be inside.
            ChipComponent chipToReplace = pivotCell.GetTopChip();
            if (chipToReplace == pivotChip)
            {
                chipToReplace = null;
            }

            SpecialMatchOccurring?.Invoke(match, specialChip, chipToReplace);

            if (chipToReplace != null)
            {
                chipToReplace.AddImpactFlags(ImpactFlag.Replace);
                chipToReplace.ConsumeSeparately(ImpactFlag.Replace);
            }

            Action<ChipComponent> impactCallback = null;
            impactCallback =
                chip =>
                {
                    chip.Impacted -= impactCallback;
                    OnSpecialMatchPivotChipImpacted(chip, pivotCell);
                };

            // we have to pass pivot cell alongside with chip cause at some circumstance when Impacted event occured
            // chip has no Cell. This might be during ultimate pending chip creation (when pending chip timeout
            // overwrites several times). During that overwrite in SpecialMatchImpactController cell become null.
            // No need to unsubscribe from event cause chip is destroying
            pivotChip.Impacted += impactCallback;

            _pendingSpecialChips.Add(pivotChip, specialChip);
        }

        private void OnSpecialMatchPivotChipImpacted([NotNull] ChipComponent chip, [NotNull] CellComponent cell)
        {
            ChipComponent specialChip = _pendingSpecialChips[chip];

            if (!chip.IsTotallyConsumed)
            {
                Assert.IsTrue(cell.CanInsertChip(), $"Can't insert chip into {cell.GridPosition}");
                cell.InsertChip(specialChip);
            }
            else
            {
                Assert.IsTrue(cell.Chip.IsConsumed,
                    $"Chip '{cell.Chip}' in '{cell.name}' is not consumed when creating special chip");
                cell.Chip = specialChip;
            }

            _pendingSpecialChips.Remove(chip);
        }

        private bool FindMatchPositions(Vector2Int position, bool isHorizontal, ref Vector2Int startPos, ref Direction direction, ref int matchLength)
        {
            CellComponent cell = _grid[position.x, position.y];
            if (cell == null)
            {
                return false;
            }

            ChipComponent chip = cell.Chip;
            if (chip == null)
            {
                return false;
            }

            Direction previousDirection = isHorizontal ? Direction.Left : Direction.Down;
            Direction nextDirection = isHorizontal ? Direction.Right : Direction.Up;

            int previousMatchLength = GetLinearMatchLength(position, chip, previousDirection);
            int nextMatchLength = GetLinearMatchLength(position, chip, nextDirection);

            matchLength = previousMatchLength + nextMatchLength - 1;
            if (matchLength < _minMatchLength)
            {
                return false;
            }

            startPos = position + previousDirection.GetOffset() * (previousMatchLength - 1);
            direction = nextDirection;

            return true;
        }

        private Match FindMatch(Vector2Int position, bool isHorizontal, Match match = null)
        {
            var startPos = new Vector2Int();
            var direction = new Direction();
            int matchLength = 0;

            if (!FindMatchPositions(position, isHorizontal, ref startPos, ref direction, ref matchLength))
            {
                return null;
            }

            bool isConnectedMatch = match != null;
            match = match ?? new Match(position);

            var newPositions = new List<Vector2Int>(matchLength) { position };
            Vector2Int directionOffset = direction.GetOffset();

            for (int i = 0; i < matchLength; i++)
            {
                Vector2Int pos = startPos + directionOffset * i;
                if (match.AddPosition(pos))
                {
                    newPositions.Add(pos);
                }
            }

            if (!isConnectedMatch)
            {
                // Check connected matches
                foreach (Vector2Int pos in newPositions)
                {
                    FindMatch(pos, !isHorizontal, match);
                }
            }

            return match;
        }

        private Match FindSquareMatch(Vector2Int position)
        {
            if (!_squareMatchesEnabled)
            {
                return null;
            }

            CellComponent pivotCell = _grid.GetCell(position);
            if (pivotCell == null || !CheckCellStability(pivotCell))
            {
                return null;
            }

            ChipComponent pivotChip = pivotCell.Chip;
            if (pivotChip == null)
            {
                return null;
            }

            Direction sideDir = Direction.Left, otherSideDir = Direction.None;
            const int rotationsAmount = 4, sideMaxLength = 2;
            bool squareFound = false;

            for (int r = 0; r < rotationsAmount; ++r)
            {
                otherSideDir = sideDir.Rotate90CW();

                int mainSideSize = GetLinearMatchLength(position, pivotChip, sideDir);
                if (mainSideSize >= sideMaxLength)
                {
                    Vector2Int offset = sideDir.GetOffset();
                    Vector2Int pos = position;
                    bool directionFailed = false;
                    for (int i = 0; i < sideMaxLength; ++i)
                    {
                        CellComponent cell = _grid.GetCell(pos);
                        if (cell == null ||
                            !CheckCellStability(cell) ||
                            cell.Chip == null)
                        {
                            directionFailed = true;
                            break;
                        }

                        int otherSideSize = GetLinearMatchLength(pos, cell.Chip, otherSideDir);
                        if (otherSideSize < sideMaxLength)
                        {
                            directionFailed = true;
                            break;
                        }
                        pos += offset;
                    }

                    if (!directionFailed)
                    {
                        squareFound = true;
                        break;
                    }
                }

                sideDir = sideDir.Rotate90CW();
            }

            if (!squareFound)
            {
                return null;
            }

            var match = new Match(position, true);

            Vector2Int sideDirOffset = sideDir.GetOffset();
            Vector2Int otherSideDirOffset = otherSideDir.GetOffset();

            // actually we search only for 2x2 squares
            match.AddPosition(position + sideDirOffset);
            match.AddPosition(position + otherSideDirOffset);
            match.AddPosition(position + sideDirOffset + otherSideDirOffset);

            // plus one non-squared aligned chip with the same color
            var color = pivotChip.Tags.Get<ColorChipTag>();
            ChipComponent backSideChip = _grid.GetCell(position - sideDirOffset)?.Chip;
            if (IsValidChip(backSideChip, color))
            {
                match.AddPosition(backSideChip.Cell.GridPosition);
                return match;
            }

            ChipComponent backOtherSideChip = _grid.GetCell(position - otherSideDirOffset)?.Chip;
            if (IsValidChip(backOtherSideChip, color))
            {
                match.AddPosition(backOtherSideChip.Cell.GridPosition);
                return match;
            }

            ChipComponent nextSideChip =
                _grid.GetCell(position + new Vector2Int(2 * sideDirOffset.x, 2 * sideDirOffset.y))?.Chip;
            if (IsValidChip(nextSideChip, color))
            {
                match.AddPosition(nextSideChip.Cell.GridPosition);
                return match;
            }

            ChipComponent nextOtherSideChip =
                _grid.GetCell(position + new Vector2Int(2 * otherSideDirOffset.x, 2 * otherSideDirOffset.y))?.Chip;
            if (IsValidChip(nextOtherSideChip, color))
            {
                match.AddPosition(nextOtherSideChip.Cell.GridPosition);
                return match;
            }

            return match;
        }

        private int GetLinearMatchLength(Vector2Int position, [NotNull] ChipComponent chip, Direction direction)
        {
            if (chip == null)
            {
                throw new ArgumentNullException(nameof(chip));
            }

            int matchLength = 1;
            var color = chip.Tags.Get<ColorChipTag>();
            if (color == null)
            {
                return matchLength;
            }

            if (!IsValidChip(chip, color))
            {
                return matchLength;
            }

            while (true)
            {
                position += direction.GetOffset();

                CellComponent nextCell = _grid.GetCell(position);
                if (nextCell == null)
                {
                    break;
                }

                ChipComponent nextChip = nextCell.Chip;
                if (!IsValidChip(nextChip, color))
                {
                    break;
                }

                matchLength++;
            }

            return matchLength;
        }

        private bool IsValidChip([CanBeNull] ChipComponent chip, ColorChipTag color)
        {
            return chip != null &&
                   chip.Tags.Has(color) &&
                   !chip.IsConsumed &&
                   chip.Cell != null &&
                   CheckCellStability(chip.Cell) &&
                   (chip == _swappedChip || !chip.IsMoving);
        }

        private bool CheckCellStability([NotNull] CellComponent cell)
        {
            if (cell.IsStable || _swappedChip?.Cell == cell)
            {
                return true;
            }
            return FindPendingMatches(cell.GridPosition).Count > 0;
        }

        private List<PendingMatch> FindPendingMatches(Vector2Int gridPos)
        {
            var foundMatches = new List<PendingMatch>();

            foreach (PendingMatch pendingMatch in _pendingMatches)
            {
                if (pendingMatch.Contains(gridPos))
                {
                    foundMatches.Add(pendingMatch);
                }
            }

            return foundMatches;
        }

        private List<PendingMatch> FindPendingMatches(Match match)
        {
            var returningFoundMatches = new List<PendingMatch>();

            foreach (Vector2Int p in match)
            {
                List<PendingMatch> foundPendingMatches = FindPendingMatches(p);
                foreach (PendingMatch foundPendingMatch in foundPendingMatches)
                {
                    if (!returningFoundMatches.Contains(foundPendingMatch))
                    {
                        returningFoundMatches.Add(foundPendingMatch);
                    }
                }
            }

            return returningFoundMatches;
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }

        #region ISwapHandler
        bool SwapSystem.ISwapHandler.ShouldSwapSucceed(CellComponent cellFrom, CellComponent cellTo)
        {
            bool shouldSwapSucceed = false;

            _swappedChip = cellFrom.Chip;
            shouldSwapSucceed |= FindMatch(cellFrom.GridPosition) != null;

            _swappedChip = cellTo.Chip;
            shouldSwapSucceed |= FindMatch(cellTo.GridPosition) != null;

            _swappedChip = null;

            return shouldSwapSucceed;
        }

        void SwapSystem.ISwapHandler.HandleSwap(CellComponent cellFrom, CellComponent cellTo)
        {
            _swappedChip = cellFrom.Chip;
            TryConsumeMatch(cellFrom.GridPosition, false);

            _swappedChip = cellTo.Chip;
            TryConsumeMatch(cellTo.GridPosition, false);

            _swappedChip = null;
        }
        #endregion

        #region IFieldDestabilizer
        event Action IFieldDestabilizer.Stabilized
        {
            add { _stabilized += value; }
            remove { _stabilized -= value; }
        }

        event Action IFieldDestabilizer.Destabilized
        {
            add { _destabilized += value; }
            remove { _destabilized -= value; }
        }

        bool IFieldDestabilizer.IsStable => _isStable;
        #endregion

        #region Inner classes
        /// <param name="match">Special match that will occur.</param>
        /// <param name="specialChip">Special chip created.</param>
        /// <param name="chipToReplace">Chip in a container that will be replaced with a special chip. May be null.</param>
        public delegate void SpecialMatchOccurringDelegate(
                [NotNull] Match match,
                [NotNull] ChipComponent specialChip,
                [CanBeNull] ChipComponent chipToReplace
            );

        [Serializable]
        private struct SpecialMatch
        {
            public int ChipCount;
            public ChipComponent SpecialChip;
        }

        private class SpecialMatchComparer : IComparer<SpecialMatch>
        {
            public int Compare(SpecialMatch x, SpecialMatch y)
            {
                return x.ChipCount.CompareTo(y.ChipCount);
            }
        }
        #endregion
    }
}