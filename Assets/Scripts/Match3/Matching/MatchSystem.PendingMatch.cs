﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DreamTeam.Match3.Matching
{
    public partial class MatchSystem
    {
        private class PendingMatch
        {
            private MatchSystem _matchSystem;

            private float _timeout;
            private List<Impact> _impacts;

            public Match Match { get; private set; }

            public PendingMatch(MatchSystem matchSystem)
            {
                _matchSystem = matchSystem;
            }

            public void SetMatch(Match match)
            {
                IEnumerable<Vector2Int> newChips;

                if (Match == null)
                {
                    Match = match;
                    newChips = Match;
                }
                else
                {
                    newChips = match.Except(Match);
                    if (!newChips.Any())
                    {
                        return;
                    }

                    if (match.ContainsPosition(Match.Pivot))
                    {
                        match.Pivot = Match.Pivot;
                    }

                    Match = match;

                    CancelRedundantImpacts();
                }

                List<Impact> impacts = _matchSystem.ScheduleImpact(newChips);
                if (_impacts == null)
                {
                    _impacts = impacts;
                }
                else
                {
                    _impacts.AddRange(impacts);
                }

                impacts.ForEach(
                        impact =>
                        {
                            impact.Cancelling += OnImpactCancelling;
                            impact.Begin();
                        }
                    );

                _timeout = _matchSystem._matchTimeout;
            }

            private void OnImpactCancelling(Impact impact)
            {
                _impacts.Remove(impact);
            }

            public bool Update()
            {
                _timeout -= Time.deltaTime;
                if (_timeout > 0f)
                {
                    return false;
                }

                Consume();
                return true;
            }

            public bool Contains(Vector2Int position)
            {
                return Match.ContainsPosition(position);
            }

            public void Consume()
            {
                _matchSystem.TryPrepareSpecialMatch(Match);
                _impacts.ForEach(impact => impact.Perform());
            }

            public void Cancel()
            {
                Match = null;
                _impacts.ToList().ForEach(impact => impact.Cancel());
                _impacts.Clear();
            }

            private void CancelRedundantImpacts()
            {
                var impactsToCancel = new List<Impact>();

                for (int i = 0; i < _impacts.Count;)
                {
                    Impact impact = _impacts[i];
                    if (!Match.Contains(impact.Cell.GridPosition))
                    {
                        _impacts.RemoveAt(i);

                        // Cancelling in a such way cause when you invoke straight Cancel() here:
                        // 1. Cell listens to Provider.Cancel() and might become stable;
                        // 2. On cell stabilized MatchSystem tries check match in that cell again;
                        // 3. It invokes TryConsumeMatch and overwrite pending match with those, who has less cells;
                        // 4. If here we had to cancel more than 1 impact other ones will be lost and become uncancelled forever
                        impactsToCancel.Add(impact);
                    }
                    else
                    {
                        i++;
                    }
                }

                impactsToCancel.ForEach(impact => impact.Cancel());
            }
        }
    }
}