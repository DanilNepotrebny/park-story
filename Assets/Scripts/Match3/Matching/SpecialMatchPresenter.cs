﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Tweening;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.Matching
{
    /// <summary>
    /// Presenter that starts animation for a special match.
    /// </summary>
    public class SpecialMatchPresenter : MonoBehaviour, IImpactController
    {
        [SerializeField] private float _movingTime = 0.3f;
        [SerializeField] private float _cellReleaseDelay;
        [SerializeField] private Material _chipMaterial;
        [SerializeField] private AnimationCurve _flashingEasing;
        [SerializeField] private Easing _movingEasing;
        [SerializeField] private GameObject _matchEffect;

        [SerializeField, SortingLayer] private int _specialChipSortingLayer;
        [SerializeField] private string _specialChipTrigger;
        [SerializeField] private string _specialChipExitState;

        [SerializeField] private float _chipReplaceTime;

        private SpecialMatchImpactController.Factory _controllerFactory;

        public float MovingTime => _movingTime;
        public Material ChipMaterial => _chipMaterial;
        public AnimationCurve FlashingEasing => _flashingEasing;
        public Easing MovingEasing => _movingEasing;
        public GameObject MatchEffect => _matchEffect;
        public float CellReleaseDelay => _cellReleaseDelay;

        public int SpecialChipSortingLayer => _specialChipSortingLayer;
        public string SpecialChipTrigger => _specialChipTrigger;
        public string SpecialChipExitState => _specialChipExitState;

        public float ChipReplaceTime => _chipReplaceTime;

        [Inject]
        private void Construct(SpecialMatchImpactController.Factory controllerFactory, MatchSystem matchSystem)
        {
            _controllerFactory = controllerFactory;

            matchSystem.SpecialMatchOccurring += OnSpecialMatchOccurring;
        }

        private void OnSpecialMatchOccurring(Match match, ChipComponent specialChip, ChipComponent chipToReplace)
        {
            chipToReplace?.AddImpactController(this);
            _controllerFactory.Create(match, specialChip, chipToReplace, this);
        }

        private void ReleaseChip(
            [NotNull] ChipComponent chip,
            ImpactFlag impactType,
            [NotNull] IDeferredInvocationHandle impactEndHandle)
        {
            chip.ExecuteNextImpactControllers(this, impactType, impactEndHandle);
            impactEndHandle.Unlock();
        }

        #region IImpactController

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.SpecialMatch;
        }

        void IImpactController.OnImpact(
            ChipComponent chip,
            ImpactFlag impactType,
            IDeferredInvocationHandle impactEndHandle,
            ref bool isModal)
        {
            isModal = true;
            var impactHandle = impactEndHandle.Lock();

            LeanTween.delayedCall(
                    gameObject,
                    ChipReplaceTime,
                    () => ReleaseChip(chip, impactType, impactHandle)
                );
        }

        void IImpactController.Cancel()
        {
            // nothing to do
        }

        #endregion IImpactController
    }
}
