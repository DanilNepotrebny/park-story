﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Linq;
using DreamTeam.Audio;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.Matching
{
    [RequireComponent(typeof(MatchSystem), typeof(AudioSystemPlayer))]
    public class MatchSystemSoundsPresenter : MonoBehaviour
    {
        [SerializeField, RequiredField] private AudioSourceProvider _audioSourceProvider;

        private MatchSystem _matchSystem => GetComponent<MatchSystem>();
        private AudioSystemPlayer _audioPlayer => GetComponent<AudioSystemPlayer>();

        protected void Awake()
        {
            _matchSystem.MatchPerformed += OnMatchConsuming;
        }

        protected void OnDestroy()
        {
            _matchSystem.MatchPerformed -= OnMatchConsuming;
        }

        private void OnMatchConsuming(int seriesCount)
        {
            int index = Mathf.Clamp(seriesCount, 1, _audioSourceProvider.Count) - 1;
            AudioSource source = _audioSourceProvider.ElementAt(index);
            _audioPlayer.Play(source);
        }
    }
}