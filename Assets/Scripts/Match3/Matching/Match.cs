﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3.Matching
{
    public class Match : IEnumerable<Vector2Int>
    {
        private readonly HashSet<Vector2Int> _positions = new HashSet<Vector2Int>();
        private Vector2Int _pivot;

        public Vector2Int Pivot
        {
            get { return _pivot; }
            set
            {
                _pivot = value;
                Assert.IsTrue(ContainsPosition(_pivot), "Match doesn't contain pivot position.");
            }
        }

        public bool IsSquareMatch { get; }
        public int Count => _positions.Count;

        public Match(Vector2Int pivot, bool isSquareMatch = false)
        {
            IsSquareMatch = isSquareMatch;
            AddPosition(pivot);
            Pivot = pivot;
        }

        public bool AddPosition(Vector2Int position)
        {
            return _positions.Add(position);
        }

        public bool ContainsPosition(Vector2Int position)
        {
            return _positions.Contains(position);
        }

        public IEnumerator<Vector2Int> GetEnumerator()
        {
            return _positions.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            string str = "Match ";
            foreach (Vector2Int gridPos in _positions)
            {
                str = $"{str} [{gridPos}]";
            }

            return str;
        }
    }
}