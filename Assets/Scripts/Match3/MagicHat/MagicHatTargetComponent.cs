﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Movement;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.MagicHat
{
    /// <summary>
    /// Incapsulates animation sequence of the <see cref="MagicHatComponent"/> target.
    /// </summary>
    [RequireComponent(typeof(MovementSequence), typeof(Animator))]
    public class MagicHatTargetComponent : MonoBehaviour, IImpactController, IAnimatorStateExitHandler
    {
        [SerializeField] private MovementSequence _rayPrefab;
        [SerializeField] private string _impactTriggerName = "StartImpact";
        [SerializeField] private string _impactStateName = "Impact";
        [SerializeField, SortingLayer] private int _chipSortingLayer;
        [SerializeField] private float _finalScale = 0.2f;
        [SerializeField] private AnimationCurve _scaleEasing;
        [SerializeField, Range(0f, 1f)] private float _finalFlashAmount = 1f;
        [SerializeField] private AnimationCurve _flashEasing;
        [SerializeField] private float _rayContactDistance = 0.5f;
        [SerializeField] private GameObject _rayContactEffect;
        [SerializeField] private GameObject _flyingChipEffect;

        [Inject] private Instantiator _instantiator;

        private MagicHatPresenter _presenter;
        private CellComponent _cell;
        private ChipPresenter _chipView;
        private MovementSequence _flightSequence;
        private bool _rayContactEffectSpawned;
        private Animator _animator;

        private readonly int _flashingAmountPropertyId = Shader.PropertyToID("_FlashAmount");
        private IDeferredInvocationHandle _chipImpactEndHandle;
        private IDeferredInvocationHandle _chipImpactInvocationHandle;
        private bool _hasImpacted;

        public Impact InvocingImpact { get; private set; }

        public event Action<MagicHatTargetComponent, float> ChipMovementUpdated;

        public void Bind(
            [NotNull] ChipComponent chip,
            [NotNull] Impact impact,
            [NotNull] IDeferredInvocationHandle chipImpactInvocationHandle,
            [NotNull] MagicHatPresenter presenter)
        {
            _cell = chip.Cell;
            _presenter = presenter;
            InvocingImpact = impact;

            Assert.IsNull(_chipImpactInvocationHandle);
            _chipImpactInvocationHandle = chipImpactInvocationHandle.Lock();
            _flyingChipEffect?.SetActive(false);

            Assert.IsNull(_flightSequence);
            _flightSequence = _instantiator.Instantiate(_rayPrefab, _presenter.HatTop);
            _flightSequence.gameObject.SetActive(false);
            _flightSequence.Updated += (sequence, ratio) =>
            {
                if (!_rayContactEffectSpawned &&
                    _rayContactEffect != null &&
                    Vector3.Distance(_flightSequence.transform.position, transform.position) <= _rayContactDistance)
                {
                    _rayContactEffectSpawned = true;
                    _instantiator.Instantiate(_rayContactEffect, transform.position);
                }
            };
            _flightSequence.Finished += OnFlightSequenceFinished;
        }

        /// <summary> Starts this target movement sequence. </summary>
        public void StartMovement()
        {
            _flightSequence.gameObject.SetActive(true);
            _flightSequence.Move(_presenter.HatTop, transform.position);
        }

        public void CancelMovement()
        {
            // Send event to presenter that we're done.
            FireChipMovementUpdated(1f);
            OnChipMovementCompleted();
            ReleaseTargetChipImpactEndHandle();
        }

        protected void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        protected void OnDestroy()
        {
            ReleaseTargetChipImpactEndHandle();

            ChipComponent chip = _cell.Chip;
            if (chip != null)
            {
                chip.RemoveImpactController(this);
                chip.Impacted -= OnChipImpacted;
            }
        }

        private static void DetachAndStopEmitters(GameObject obj, bool worldPositionStays)
        {
            var particleEffects = obj.GetComponentsInChildren<ParticleSystem>(false);
            foreach (var particleEffect in particleEffects)
            {
                if (particleEffect.isPlaying)
                {
                    particleEffect.Stop();
                    particleEffect.transform.SetParent(null, worldPositionStays);
                }
            }
        }

        private void OnFlightSequenceFinished(MovementSequence mover)
        {
            GameObject rayObject = _flightSequence.gameObject;
            DetachAndStopEmitters(rayObject, true);
            rayObject.SetActive(false);

            ChipComponent chip = _cell.Chip;
            if (chip != null)
            {
                if (!chip.WillBeTotallyConsumed(_presenter.HatImpact))
                {
                    CancelMovement();
                }
                else
                {
                    chip.Impacted += OnChipImpacted;
                    chip.AddImpactController(this);
                }
            }

            _chipImpactInvocationHandle.Unlock();
            _chipImpactInvocationHandle = null;
        }

        private void OnChipImpacted(ChipComponent chipComponent)
        {
            if (!_hasImpacted)
            {
                CancelMovement();
            }
        }

        private void OnChipMovementUpdated(MovementSequence sequence, float ratio)
        {
            FireChipMovementUpdated(ratio);

            float scale = _scaleEasing.Lerp(1f, _finalScale, ratio);
            transform.localScale = new Vector3(scale, scale, scale);

            float flashAmount = _flashEasing.Lerp(0f, _finalFlashAmount, ratio);
            _chipView.SetMaterialFloat(_flashingAmountPropertyId, flashAmount);
        }

        private void FireChipMovementUpdated(float ratio)
        {
            ChipMovementUpdated?.Invoke(this, ratio);
        }

        private void OnChipMovementCompleted()
        {
            if (_flightSequence != null)
            {
                _flightSequence.transform.SetParent(transform);
            }

            if (_flyingChipEffect != null)
            {
                DetachAndStopEmitters(_flyingChipEffect, false);
            }

            gameObject.Dispose();
        }

        private void ReleaseTargetChipImpactEndHandle()
        {
            _chipImpactEndHandle?.Unlock();
            _chipImpactEndHandle = null;
        }

        void IImpactController.OnImpact(
            ChipComponent chip,
            ImpactFlag impactType,
            IDeferredInvocationHandle impactEndHandle,
            ref bool isModal)
        {
            if (!chip.IsConsumed)
            {
                return;
            }

            _chipView = chip.DetachView();
            Assert.IsNotNull(_chipView, "Chip has no view. Somebody has already detached it");
            _chipView.transform.SetParent(transform);
            _chipView.transform.localPosition = Vector3.zero;

            _hasImpacted = true;

            Assert.IsNull(_chipImpactEndHandle, $"Impact end handle is not null");
            _chipImpactEndHandle = impactEndHandle.Lock();
            isModal = true;

            _animator.SetTrigger(_impactTriggerName);
        }

        void IImpactController.Cancel()
        {
            ReleaseTargetChipImpactEndHandle();
            _hasImpacted = false;
        }

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.MagicHatTarget;
        }

        void IAnimatorStateExitHandler.OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (stateInfo.shortNameHash == Animator.StringToHash(_impactStateName))
            {
                _flightSequence.transform.SetParent(null);

                _chipView.SortingOptions.PushSortingLayer(_chipSortingLayer);
                _chipView.SetAnimatorEnabled(false); // Disable animator to get control over flashing parameter
                _animator.enabled = false; // Disable animator to get control over scale

                if (_flyingChipEffect != null)
                {
                    _flyingChipEffect.SetActive(true);
                }

                // Flying to hat
                var mover = GetComponent<MovementSequence>();
                mover.Finished += m => OnChipMovementCompleted();
                mover.Updated += OnChipMovementUpdated;
                mover.Move(_presenter.HatTop);
            }
        }
    }
}