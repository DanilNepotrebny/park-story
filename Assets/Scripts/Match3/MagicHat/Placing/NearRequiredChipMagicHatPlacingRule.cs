﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.Requirements.ChipConsumption;
using JetBrains.Annotations;
using Zenject;

namespace DreamTeam.Match3.MagicHat.Placing
{
    public class NearRequiredChipMagicHatPlacingRule : BaseMagicHatPlacingRule
    {
        [Inject] private SwapSystem _swapSystem;

        [Inject(Id = LevelSettings.GoalInstancesId)]
        private IReadOnlyList<Requirement.Instance> _goals;

        public override MagicHatPlacingRulePriority Priority => MagicHatPlacingRulePriority.NearRequiredChip;

        public override bool IsCellValid(CellComponent cell)
        {
            bool isValid = _goals
                .OfType<ChipConsumptionRequirement.Instance>()
                .Any(goal => FindValidRequirementCellAround(cell, goal, _swapSystem) != null);

            return isValid;
        }

        public static CellComponent FindValidRequirementCellAround(
                [NotNull] CellComponent cell,
                [NotNull] ChipConsumptionRequirement.Instance requirement,
                [NotNull] SwapSystem swapSystem
            )
        {
            return cell
                .IterateCellsAround()
                .FirstOrDefault(
                        cellAround => 
                            !cellAround.IsEmpty &&
                            cellAround.Chip.Tags.Has(requirement.RequiredChipTag, false) &&
                            swapSystem.CanBeSwapped(cell, cellAround)
                    );
        }

        public static CellComponent FindValidCellAround([NotNull] CellComponent cell, [NotNull] SwapSystem swapSystem)
        {
            return cell
                .IterateCellsAround()
                .FirstOrDefault(
                        cellAround =>
                            !cellAround.IsEmpty &&
                            swapSystem.CanBeSwapped(cell, cellAround)
                    );
        }
    }
}