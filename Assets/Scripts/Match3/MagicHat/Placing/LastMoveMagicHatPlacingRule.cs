﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using Zenject;

namespace DreamTeam.Match3.MagicHat.Placing
{
    public class LastMoveMagicHatPlacingRule : NearRequiredChipMagicHatPlacingRule
    {
        [Inject(Id = LevelSettings.LimitationInstancesId)]
        private IReadOnlyList<Requirement.Instance> _limitations;

        private const int RemainingMovesToCheck = 1;

        public override MagicHatPlacingRulePriority Priority => MagicHatPlacingRulePriority.LastMove;

        public override bool IsCellValid(CellComponent cell)
        {
            return !IsRequiredAmountOfMovesLeft() || base.IsCellValid(cell);
        }

        private bool IsRequiredAmountOfMovesLeft()
        {
            var movesLimitation = _limitations.OfType<MovesRequirement.Instance>().FirstOrDefault();
            return movesLimitation != null && movesLimitation.RemainingMoves == RemainingMovesToCheck;
        }
    }
}