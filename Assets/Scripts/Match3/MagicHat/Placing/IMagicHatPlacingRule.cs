﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using JetBrains.Annotations;

namespace DreamTeam.Match3.MagicHat.Placing
{
    public interface IMagicHatPlacingRule
    {
        MagicHatPlacingRulePriority Priority { get; }

        void OnCheckBegin();

        void OnCheckEnd();

        bool IsCellValid([NotNull] CellComponent cell);
    }

    // lower integer value means higher priority
    public enum MagicHatPlacingRulePriority
    {
        UnderPopcornChip,
        NearRequiredChip,
        LastMove,
        BreakNoBonusCombination
    }
}