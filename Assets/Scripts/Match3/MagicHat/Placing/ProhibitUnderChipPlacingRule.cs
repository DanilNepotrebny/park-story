﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.ChipTags;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.MagicHat.Placing
{
    public class ProhibitUnderChipPlacingRule : BaseMagicHatPlacingRule
    {
        [SerializeField] private ChipTag _chipTag;

        [Inject] private GridSystem _gridSystem;
        [Inject] private GravitySystem _gravitySystem;

        private List<CellComponent> _targetChipCells = new List<CellComponent>();
        private Dictionary<CellComponent, bool> _cellValidationStates = new Dictionary<CellComponent, bool>();

        public override MagicHatPlacingRulePriority Priority => MagicHatPlacingRulePriority.UnderPopcornChip;

        public override void OnCheckBegin()
        {
            base.OnCheckBegin();

            _gridSystem.ForEachCell(cell => _cellValidationStates.Add(cell, true));

            _targetChipCells = GetCells(_chipTag);
            foreach (CellComponent chipCell in _targetChipCells)
            {
                foreach (CellComponent downCell in _gravitySystem.EnumerateDownstream(chipCell, true, true, false))
                {
                    _cellValidationStates[downCell] = false;
                }
            }
        }

        public override void OnCheckEnd()
        {
            base.OnCheckEnd();

            _targetChipCells.Clear();
            _cellValidationStates.Clear();
        }

        public override bool IsCellValid(CellComponent cell)
        {
            return _cellValidationStates[cell];
        }

        private List<CellComponent> GetCells([NotNull] ChipTag chipTag)
        {
            return _gridSystem
                .Cells
                .Where(cell => !cell.IsEmpty && cell.Chip.Tags.Has(chipTag, false))
                .ToList();
        }
    }
}