﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.MagicHat.Placing
{
    public abstract class BaseMagicHatPlacingRule : MonoBehaviour, IMagicHatPlacingRule
    {
        [Inject] private MagicHatSystem _magicHatSystem;

        public abstract MagicHatPlacingRulePriority Priority { get; }

        public abstract bool IsCellValid(CellComponent cell);

        public virtual void OnCheckBegin()
        {
            // override in childs
        }

        public virtual void OnCheckEnd()
        {
            // override in childs
        }

        protected void Start()
        {
            _magicHatSystem.RegisterPlacingRule(this);
        }

        protected void OnDestroy()
        {
            _magicHatSystem.UnregisterPlacingRule(this);
        }
    }
}
