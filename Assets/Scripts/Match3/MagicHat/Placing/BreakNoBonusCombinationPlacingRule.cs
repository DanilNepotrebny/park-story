﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.Matching;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.MagicHat.Placing
{
    public class BreakNoBonusCombinationPlacingRule : BaseMagicHatPlacingRule
    {
        [Inject] private MatchSystem _matchSystem;
        [Inject] private MovesSystem _movesSystem;
        [Inject] private GridSystem _gridSystem;

        private List<MovesSystem.Move> _moves = new List<MovesSystem.Move>();
        private Dictionary<CellComponent, bool> _cellValidationStates = new Dictionary<CellComponent, bool>();

        public override MagicHatPlacingRulePriority Priority => MagicHatPlacingRulePriority.BreakNoBonusCombination;

        public override bool IsCellValid(CellComponent cell)
        {
            return _cellValidationStates[cell];
        }

        public override void OnCheckBegin()
        {
            base.OnCheckBegin();

            _gridSystem.ForEachCell(cell => _cellValidationStates.Add(cell, true));

            _movesSystem.FindMoves(_moves, false);
            _moves = _moves.Where(move => move.Type == MovesSystem.MoveType.Swap).ToList();

            foreach (MovesSystem.Move move in _moves)
            {
                UpdateValidationStates(move.SourceMatch, move.Target);
                UpdateValidationStates(move.TargetMatch, move.Source);
            }
        }

        public override void OnCheckEnd()
        {
            base.OnCheckEnd();

            _moves.Clear();
            _cellValidationStates.Clear();
        }

        private void UpdateValidationStates([NotNull] List<ChipComponent> matchChips, CellComponent cell)
        {
            if (matchChips.Count == 0)
            {
                return;
            }

            var match = new Match(matchChips.First().Cell.GridPosition);
            for (int i = 1; i < matchChips.Count; ++i) // 0 - is a pivot cell
            {
                match.AddPosition(matchChips[i].Cell.GridPosition);
            }
            // don't care about square match cause it is special too and requires 4 chips

            if (_matchSystem.IsSpecialMatch(match))
            {
                foreach (Vector2Int cellCoordinate in match)
                {
                    CellComponent notValidCell = _gridSystem.GetCell(cellCoordinate);
                    _cellValidationStates[notValidCell] = false;
                }

                _cellValidationStates[cell] = false;
            }
        }
    }
}