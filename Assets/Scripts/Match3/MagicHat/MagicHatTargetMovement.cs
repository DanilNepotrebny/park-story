﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Screen;
using DreamTeam.Utils.Movement;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.MagicHat
{
    /// <summary> Movement controller for chip flight into magic hat. </summary>
    public class MagicHatTargetMovement : PathMovementController
    {
        [Space] [SerializeField] private float _hatControlPointAmplitude = 15f;
        [SerializeField] private float _chipControlPointAmplitude = 5f;
        [SerializeField] private float _chipControlPointRoundingAngle = 90f;
        [SerializeField] private bool _switchControlPoints;

        [Inject] private MovementArea _movementArea;
        [Inject] private ScreenController _screen;

        protected override void CalculatePath(
            Vector3 start,
            out Vector3 controlPoint1,
            out Vector3 controlPoint2,
            Vector3 end)
        {
            float angle = Vector3.Angle(start - end, Vector3.right);
            angle = Mathf.Round(angle / _chipControlPointRoundingAngle) * _chipControlPointRoundingAngle;

            controlPoint1 = start + Quaternion.AngleAxis(angle, Vector3.up) * Vector3.up * _chipControlPointAmplitude;
            controlPoint2 = end;
            if (start.y >= end.y || _switchControlPoints)
            {
                controlPoint2 += Vector3.up * _hatControlPointAmplitude;
            }

            controlPoint2.y = Mathf.Min(
                controlPoint2.y,
                Camera.main.ScreenToWorldPoint(new Vector3(0f, _screen.Height, 0f)).y);

            _movementArea.FitToMovementArea(ref controlPoint1);
            _movementArea.FitToMovementArea(ref controlPoint2);
        }
    }
}