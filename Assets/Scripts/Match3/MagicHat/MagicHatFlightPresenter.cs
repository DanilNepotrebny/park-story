﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using DreamTeam.Utils.Movement;
using Spine;
using Spine.Unity;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.MagicHat
{
    /// <summary>
    /// Service object that is responsible for magic hat appearance on the field. When hat is charged this object
    /// starts flying from hat panel to the specified position.
    /// </summary>
    [RequireComponent(typeof(MovementSequence), typeof(SkeletonAnimation), typeof(MagicHatFlightMovementController))]
    public class MagicHatFlightPresenter : MonoBehaviour
    {
        [SerializeField, SpineAnimation] private string _startAnimation;
        [SerializeField, SpineAnimation] private string _flightAnimation;
        [SerializeField, SpineAnimation] private string _endAnimation;
        [SerializeField] private Vector3 _flightEndOffset;
        [SerializeField] private GameObject[] _flightEndEffectsToSpawn;

        [Header("Scale")] [SerializeField] private Vector3 _maxScale;
        [SerializeField] private AnimationCurve _headingToExtremumScaleEasing;
        [SerializeField] private AnimationCurve _headingFromExtremumScaleEasing;

        [Header("Animation speed")] [SerializeField]
        private float _maxAnimationSpeed;

        [SerializeField] private AnimationCurve _headingToExtremumAnimationSpeedEasing;
        [SerializeField] private AnimationCurve _headingFromExtremumAnimationSpeedEasing;

        [Inject] private Instantiator _instantiator;

        private Vector3 _targetPosition;
        private Vector3 _startScale;
        private float _startAnimationSpeed;
        private SkeletonAnimation _animation;
        private MovementSequence _movementSequence;
        private MagicHatFlightMovementController _flightController;

        /// <summary> Fired when whole flight sequence (moving + animation) is completed. </summary>
        public event Action FlightCompleted;

        public void Activate(Vector3 targetPosition)
        {
            var track = _animation.AnimationState.SetAnimation(0, _startAnimation, false);
            track.Complete += OnStartAnimationCompleted;

            _targetPosition = targetPosition;
            _startScale = transform.localScale;
            _startAnimationSpeed = _animation.timeScale;
        }

        protected void Awake()
        {
            _animation = GetComponent<SkeletonAnimation>();
            _movementSequence = GetComponent<MovementSequence>();
            _flightController = GetComponent<MagicHatFlightMovementController>();
            _flightController.Moved += OnMoved;
        }

        protected void OnValidate()
        {
            foreach (GameObject effectPrefab in _flightEndEffectsToSpawn)
            {
                if (effectPrefab == null)
                {
                    UnityEngine.Debug.LogAssertion(
                        $"Missing element in {nameof(_flightEndEffectsToSpawn)} list in {name}",
                        this);
                }
            }
        }

        private void OnMoved(float ratio, bool isExtremumHeading)
        {
            if (isExtremumHeading)
            {
                transform.localScale = _headingToExtremumScaleEasing.Lerp(_startScale, _maxScale, ratio);
                _animation.timeScale = _headingToExtremumAnimationSpeedEasing.Lerp(
                    _startAnimationSpeed,
                    _maxAnimationSpeed,
                    ratio);
            }
            else
            {
                transform.localScale = _headingFromExtremumScaleEasing.Lerp(_startScale, _maxScale, ratio);
                _animation.timeScale = _headingFromExtremumAnimationSpeedEasing.Lerp(
                    _startAnimationSpeed,
                    _maxAnimationSpeed,
                    ratio);
            }
        }

        private void OnStartAnimationCompleted(TrackEntry trackEntry)
        {
            _movementSequence.Finished += OnMovementFinished;
            _movementSequence.Move(transform.position, _targetPosition + _flightEndOffset);

            _animation.AnimationState.SetAnimation(0, _flightAnimation, true);
        }

        private void OnMovementFinished(MovementSequence mover)
        {
            _animation.timeScale = _startAnimationSpeed;
            var track = _animation.AnimationState.SetAnimation(0, _endAnimation, false);
            track.Complete += OnEndAnimationCompleted;
            foreach (GameObject effectPrefab in _flightEndEffectsToSpawn)
            {
                _instantiator.Instantiate(effectPrefab, _targetPosition);
            }
        }

        private void OnEndAnimationCompleted(TrackEntry trackEntry)
        {
            FlightCompleted?.Invoke();
            gameObject.Dispose();
        }
    }
}