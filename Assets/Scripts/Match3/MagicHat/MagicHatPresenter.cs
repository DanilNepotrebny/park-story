﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using Event = Spine.Event;

namespace DreamTeam.Match3.MagicHat
{
    /// <summary>
    /// Magic hat presenter. Controls animations of active hat. Creates target controllers for each chip which is consumed by hat.
    /// </summary>
    [RequireComponent(typeof(ChipPresenter))]
    public class MagicHatPresenter : MonoBehaviour
    {
        [SerializeField] private MagicHatTargetComponent _targetPrefab;
        [SerializeField, SpineEvent] private string _activationEvent;
        [SerializeField, SpineBone] private string _hatTopBone;
        [SerializeField, SpineAnimation] private string _activationBeginAnimation;
        [SerializeField, SpineAnimation] private string _activationLoopAnimation;
        [SerializeField, SpineAnimation] private string _activationEndAnimation;
        [SerializeField] private ParticleSystem _activationParticleSystem;
        [SerializeField] private float _targetPathRatioToEndActivation;
        [SerializeField] private GameObject _beginActivationEffect;

        [Header("Double hat settings:"), SerializeField, SpineAnimation] private string _doubleHatBeginAnimation;
        [SerializeField, SpineAnimation] private string _doubleHatEndAnimation;
        [SerializeField, SpineEvent] private string _doubleHatImpactEvent;

        [Inject] private Instantiator _instantiator;
        [Inject] private MagicHatComponent _magicHat;

        private readonly List<MagicHatTargetComponent> _targets = new List<MagicHatTargetComponent>();
        private int _activationEndCount;
        private IDeferredInvocationHandle _hatImpactEndHandle;
        private SkeletonAnimation _animation;
        private bool _endActivationImmediately;
        private ChipPresenter _presenter;

        public int HatImpact => _magicHat.Impact;

        /// <summary> Position of the hat hole at the moment (depends on the current animation frame) </summary>
        public Vector3 HatTop => _animation.Skeleton.FindBone(_hatTopBone).GetWorldPosition(transform);

        public int ActivationEndCounter
        {
            get { return _activationEndCount; }
            private set
            {
                int newValue = Mathf.Max(0, value);
                if (_activationEndCount == newValue)
                {
                    return;
                }

                _activationEndCount = value;
                if (_activationEndCount == 0)
                {
                    EndActivation();
                }
            }
        }

        public void PlayDoubleHatBeginAnimation()
        {
            _presenter.ChipAnimatorController.SetAnimatorEnabled(false);
            _animation.AnimationState.SetAnimation(0, _doubleHatBeginAnimation, false);
        }

        public void PlayDoubleHatEndAnimation(Action impactHandler)
        {
            var entry = _animation.AnimationState.SetAnimation(0, _doubleHatEndAnimation, false);
            if (impactHandler != null)
            {
                entry.Event += (trackEntry, @event) =>
                {
                    if (@event.Data.Name == _doubleHatImpactEvent)
                    {
                        impactHandler.Invoke();
                    }
                };
            }
        }

        protected void Awake()
        {
            _presenter = GetComponent<ChipPresenter>();
            _animation = GetComponentInChildren<SkeletonAnimation>();
            _magicHat.Activating += OnActivating;
            _magicHat.ChipImpacting += OnChipImpacting;
            _magicHat.ChipImpactCancelling += OnChipImpactCancelling;
            _magicHat.ChipsImpacting += OnChipsImpacting;

            _activationParticleSystem?.gameObject.SetActive(false);
        }

        private void OnChipsImpacting(int chipsAmount)
        {
            Assert.IsTrue(ActivationEndCounter == 0);
            ActivationEndCounter = chipsAmount;
            if (ActivationEndCounter == 0)
            {
                _endActivationImmediately = true;
            }
        }

        private void OnChipImpacting(
            ChipComponent chip,
            Impact impact,
            IDeferredInvocationHandle chipImpactInvocationHandle)
        {
            MagicHatTargetComponent target = _instantiator.Instantiate(_targetPrefab, chip.transform.position);
            target.ChipMovementUpdated += OnTargetChipMovementUpdated;
            target.Bind(chip, impact, chipImpactInvocationHandle, this);
            target.StartMovement();

            _targets.Add(target);
        }

        private void OnChipImpactCancelling([NotNull] Impact impact)
        {
            MagicHatTargetComponent target = _targets.Find(t => t.InvocingImpact == impact);
            if (target != null)
            {
                // Call before target cancel
                // ling cause on cancel will be called movement update event that isn't needed.
                // In movement end we will unsubscribe from this event.
                OnTargetChipMovementEnd(target);

                target.CancelMovement();
            }
            else
            {
                // we haven't even launched projectile yet. So just decrease counter
                ActivationEndCounter -= 1;
            }
        }

        private void OnActivating(IDeferredInvocationHandle hatImpactEndHandle)
        {
            _presenter.ChipAnimatorController.SetAnimatorEnabled(false);
            _hatImpactEndHandle = hatImpactEndHandle.Lock();

            TrackEntry track = _animation.AnimationState.SetAnimation(0, _activationBeginAnimation, false);
            track.Event += OnActivationEventOccurred;
        }

        private void OnActivationEventOccurred(TrackEntry trackEntry, Event @event)
        {
            if (@event.Data.Name == _activationEvent)
            {
                _activationParticleSystem?.gameObject.SetActive(true);
                _animation.AnimationState.SetAnimation(0, _activationLoopAnimation, true);

                if (_beginActivationEffect != null)
                {
                    _instantiator.Instantiate(_beginActivationEffect, gameObject);
                }

                if (_endActivationImmediately)
                {
                    _endActivationImmediately = false;
                    EndActivation();
                }
            }
        }

        private void OnTargetChipMovementUpdated([NotNull] MagicHatTargetComponent target, float ratio)
        {
            if (ratio >= _targetPathRatioToEndActivation)
            {
                OnTargetChipMovementEnd(target);
            }
        }

        private void OnTargetChipMovementEnd([NotNull] MagicHatTargetComponent target)
        {
            if (_targets.Remove(target))
            {
                target.ChipMovementUpdated -= OnTargetChipMovementUpdated;
                ActivationEndCounter -= 1;
            }
        }

        private void EndActivation()
        {
            _activationParticleSystem?.Stop();

            var track = _animation.AnimationState.SetAnimation(0, _activationEndAnimation, false);
            track.Complete += t => ReleaseHatImpactEndHandle();
        }

        private void ReleaseHatImpactEndHandle()
        {
            _hatImpactEndHandle.Unlock();
            _hatImpactEndHandle = null;
        }
    }
}