﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Movement;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.MagicHat
{
    [RequireComponent(typeof(MovementSequence))]
    public class DoubleHatController : MonoBehaviour, IImpactController
    {
        [SerializeField] private int _impact = -1;
        [SerializeField] private float _radius = 1000f;
        [SerializeField] private float _delay = 0.2f;
        [SerializeField] private float _immediateRadius = 0f;
        [SerializeField] private MovementSequence _leftChipFlightPrefab;
        [SerializeField] private MovementSequence _rightChipFlightPrefab;
        [SerializeField] private MovementSequence _topChipFlightPrefab;
        [SerializeField] private MovementSequence _bottomChipFlightPrefab;
        [SerializeField] private Vector3 _horizontalIncomingPointOffset;
        [SerializeField] private Vector3 _verticalIncomingPointOffset;
        [SerializeField] private GameObject _explosionEffect;
        [SerializeField, SortingLayer] private int _impactSortingLayer;

        [Inject] private GridSystem _grid;
        [Inject] private ImpactSystem _impactSystem;
        [Inject] private Instantiator _instantiator;

        private readonly List<IDeferredInvocationHandle> _impactEndHandles = new List<IDeferredInvocationHandle>();

        private readonly Dictionary<ChipComponent, MovementSequence> _flightPrefabs =
            new Dictionary<ChipComponent, MovementSequence>();

        private List<ChipComponent> _hatChips = new List<ChipComponent>();

        private Vector2Int _epicenter;
        private List<Impact> _scheduledImpacts = new List<Impact>();
        private DeferredInvocation _explodeInv;

        public List<Impact> ScheduleActivation([NotNull] ChipComponent hat1, [NotNull] ChipComponent hat2)
        {
            transform.position = (hat1.transform.position + hat2.transform.position) / 2;

            Vector2Int position1 = hat1.Cell.GridPosition;
            Vector2Int position2 = hat2.Cell.GridPosition;
            _epicenter = position1;

            int dx = position1.x - position2.x;
            if (dx != 0)
            {
                _flightPrefabs.Add(hat1, dx < 0 ? _leftChipFlightPrefab : _rightChipFlightPrefab);
                _flightPrefabs.Add(hat2, dx > 0 ? _leftChipFlightPrefab : _rightChipFlightPrefab);

                transform.position += _horizontalIncomingPointOffset;
            }
            else
            {
                int dy = position1.y - position2.y;
                _flightPrefabs.Add(hat1, dy > 0 ? _topChipFlightPrefab : _bottomChipFlightPrefab);
                _flightPrefabs.Add(hat2, dy < 0 ? _topChipFlightPrefab : _bottomChipFlightPrefab);

                transform.position += _verticalIncomingPointOffset;
            }

            Impact impact2 = SetupHatChip(hat2);
            Impact impact1 = SetupHatChip(hat1);

            var impacts = new List<Impact>
            {
                impact2,
                impact1
            };

            CancelScheduledExplosion();
            _scheduledImpacts = Explosion.ScheduleExplode(
                _grid,
                _impactSystem,
                hat1.Cell.GridPosition,
                _radius,
                _impact);

            return impacts;
        }

        protected void Awake()
        {
            _explodeInv = new DeferredInvocation(DoExplosion);
        }

        protected void OnDestroy()
        {
            foreach (ChipComponent chip in _hatChips)
            {
                chip.RemoveImpactController(this);
            }
        }

        private Impact SetupHatChip([NotNull] ChipComponent chip)
        {
            _hatChips.Add(chip);

            chip.AddImpactController(this);

            Impact impact = chip.ScheduleConsumption(ImpactFlag.DoubleMagicHatActivation);
            impact.Begging += CleanUpOnImpact;
            impact.Cancelling += CleanUpOnImpact;

            return impact;
        }

        private void CleanUpOnImpact(Impact impact)
        {
            impact.Begging -= CleanUpOnImpact;
            impact.Cancelling -= CleanUpOnImpact;

            CancelScheduledExplosion();
        }

        private void CancelScheduledExplosion()
        {
            _scheduledImpacts.ForEach(i => i.Cancel());
            _scheduledImpacts.Clear();
        }

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.DoubleHat;
        }

        void IImpactController.OnImpact(
            ChipComponent chip,
            ImpactFlag impactType,
            IDeferredInvocationHandle impactEndHandle,
            ref bool isModal)
        {
            isModal = true;
            _impactEndHandles.Add(impactEndHandle.Lock());

            if (_explodeInv.IsLocked)
            {
                _explodeInv.Handle.Lock();
            }
            else
            {
                _explodeInv.Start();
            }

            // TODO: Extract animation and movement logics into presenter class
            MovementSequence mover = _instantiator.Instantiate(_flightPrefabs[chip], chip.transform.position);
            mover.transform.SetParent(transform);
            mover.Finished += OnHatsMovementFinished;

            var presenter = chip.DetachView();
            presenter.transform.SetParent(mover.transform);
            presenter.SortingOptions.SortingLayerId = _impactSortingLayer;

            MagicHatPresenter hatPresenter = presenter.GetComponent<MagicHatPresenter>();
            hatPresenter.PlayDoubleHatBeginAnimation();
            mover.Move(transform.position);
        }

        private void OnHatsMovementFinished(MovementSequence sequence)
        {
            var magicHatPresenter = sequence.GetComponentInChildren<MagicHatPresenter>();
            magicHatPresenter.PlayDoubleHatEndAnimation(
                () =>
                {
                    _explodeInv.Handle.Unlock();
                    sequence.gameObject.Dispose();
                });
        }

        private void DoExplosion()
        {
            PlayMagicWandAnimation();

            Explosion.Explode(_grid, _impactSystem, _epicenter, _radius, _impact, _delay, _immediateRadius);
            _instantiator.Instantiate(_explosionEffect, _grid.GridToWorld(_epicenter));

            _impactEndHandles.ForEach(h => h.Unlock());
            _impactEndHandles.Clear();
        }

        private void PlayMagicWandAnimation()
        {
            // Magic wand should touch the field at local (0;0) in the animation, so move the object
            // to the center of the cell in which explosion will occur.
            var mover = GetComponent<MovementSequence>();
            mover.Move(_grid.GridToWorld(_epicenter), true);
        }

        void IImpactController.Cancel()
        {
            // nothing to do
        }
    }
}