// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.GameControls;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Match3.MagicHat
{
    public class MagicHatPanel : MonoBehaviour
    {
        [SerializeField] private GameControlsMode _flyingHatMode;
        [SerializeField, RequiredField] private SimpleAnimation _showHideAnimation;
        [SerializeField] private RectTransform _particlesTarget;
        [SerializeField] private RectTransform _hatStartPoint;
        [SerializeField] private float _chargeSpeed;
        [SerializeField] private Image _progress;
        [SerializeField] private UnityEvent _chargeTweenStarted;
        [SerializeField] private SkeletonGraphic _hatAnimation;
        [SerializeField] private GameObject _increaseChargeEffect;
        [SerializeField, SpineAnimation] private string _increaseChargeAnimation;
        [SerializeField, SpineAnimation] private string _appearAnimation;
        [SerializeField] private MagicHatFlightPresenter _flightPresenterPrefab;
        [SerializeField] private ParticleSystem _chargedStateEmitter;

        [Inject] private MagicHatSystem _magicHatSystem;
        [Inject] private GameControlsSystem _controlsSystem;
        [Inject] private Instantiator _instantiator;

        private float _chargeView;
        private int _tweenId;
        private IDeferredInvocationHandle _hatsCreatingHandle;
        private TrackEntry _animTrack;
        private bool _isPlayingChargedAnimation;
        private Stack<ActionDisposable> _modeDisableHandles = new Stack<ActionDisposable>();

        public Vector3 ParticlesTarget => _particlesTarget.transform.position;
        private bool IsViewCharged => Mathf.Approximately(_chargeView, 1f);

        public void OnParticleMovingCompleted()
        {
            PlayAnimation(_increaseChargeAnimation);

            if (_increaseChargeEffect != null)
            {
                _instantiator.Instantiate(_increaseChargeEffect, transform.position);
            }
        }

        protected void Awake()
        {
            UpdateView(0f);
        }

        protected void OnEnable()
        {
            _magicHatSystem.ChargeChanged += OnChargeChanged;
            _magicHatSystem.MagicHatCreating += OnMagicHatCreating;
            _magicHatSystem.HatsCreating += OnHatsCreating;
            _magicHatSystem.Disabled += OnMagicHatSystemDisabled;
        }

        protected void OnDisable()
        {
            _magicHatSystem.ChargeChanged -= OnChargeChanged;
            _magicHatSystem.MagicHatCreating -= OnMagicHatCreating;
            _magicHatSystem.HatsCreating -= OnHatsCreating;
            _magicHatSystem.Disabled -= OnMagicHatSystemDisabled;
        }

        protected void OnDestroy()
        {
            ReleaseAllModeDisableHandles();
        }

        private void OnMagicHatSystemDisabled()
        {
            if (!_showHideAnimation.Play(SimpleAnimationType.Hide))
            {
                gameObject.SetActive(false);
            }
            else
            {
                _showHideAnimation.AnimationFinished += OnIngameMagicHatPanelAnimationFinished;
            }
        }

        private void OnIngameMagicHatPanelAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            _showHideAnimation.AnimationFinished -= OnIngameMagicHatPanelAnimationFinished;
            if (animationType == SimpleAnimationType.Hide)
            {
                gameObject.SetActive(false);
            }
        }

        private void OnHatsCreating([NotNull] IDeferredInvocationHandle handle)
        {
            if (!IsViewCharged)
            {
                Assert.IsNull(_hatsCreatingHandle, $"Hats creating handle is not freed");
                _hatsCreatingHandle = handle.Lock();
            }

            if (_isPlayingChargedAnimation)
            {
                OnAnimationCompleted();
            }
        }

        private void OnMagicHatCreating([NotNull] IDeferredInvocationHandle handle, [NotNull] CellComponent target)
        {
            if (!_magicHatSystem.IsCharged)
            {
                _chargedStateEmitter?.Stop();
                UpdateView(0f);
                OnChargeChanged(_magicHatSystem.ChargePercent);
            }

            IDeferredInvocationHandle creationHandle = handle.Lock();

            _modeDisableHandles.Push(_controlsSystem.EnableMode(_flyingHatMode));

            MagicHatFlightPresenter flightPresenter = _instantiator.Instantiate(
                _flightPresenterPrefab,
                _hatStartPoint.transform.position);

            flightPresenter.FlightCompleted += () =>
            {
                ReleaseModeDisableHandle();
                creationHandle.Unlock();
            };
            flightPresenter.Activate(target.transform.position);
        }

        private void OnChargeChanged(float chargePercent)
        {
            if (_magicHatSystem.IsCharged)
            {
                _chargedStateEmitter?.Play();
                _isPlayingChargedAnimation = true;
                PlayAnimation(_appearAnimation);
            }

            float clampedCharge = Mathf.Clamp(chargePercent, 0f, 1f);
            if (_chargeView > clampedCharge || Mathf.Approximately(_chargeView, clampedCharge))
            {
                return;
            }

            if (LeanTween.isTweening(_tweenId))
            {
                LeanTween.cancel(gameObject, _tweenId);
            }
            else if (clampedCharge > 0)
            {
                _chargeTweenStarted.Invoke();
            }

            _tweenId = LeanTween.value(gameObject, UpdateView, _chargeView, clampedCharge, 1f)
                .setSpeed(_chargeSpeed)
                .setEaseOutSine()
                .setOnComplete(OnTweenCompleted)
                .id;
        }

        private void PlayAnimation(string animationName)
        {
            if (_animTrack != null)
            {
                _animTrack.Complete -= OnAnimationCompleted;
            }

            _hatAnimation.freeze = false;
            _animTrack = _hatAnimation.AnimationState.SetAnimation(0, animationName, false);
            _animTrack.Complete += OnAnimationCompleted;
        }

        private void OnAnimationCompleted(TrackEntry entry = null)
        {
            _hatAnimation.freeze = true;
            _isPlayingChargedAnimation = false;
        }

        private void OnTweenCompleted()
        {
            float clampedCharge = Mathf.Clamp(_magicHatSystem.ChargePercent, 0f, 1f);
            UpdateView(clampedCharge);

            _hatsCreatingHandle?.Unlock();
            _hatsCreatingHandle = null;
        }

        private void UpdateView(float value)
        {
            _chargeView = value;
            if (_progress != null)
            {
                _progress.fillAmount = _chargeView;
            }
        }

        private void ReleaseAllModeDisableHandles()
        {
            while (_modeDisableHandles.Count > 0)
            {
                ReleaseModeDisableHandle();
            }
        }

        private void ReleaseModeDisableHandle()
        {
            if (_modeDisableHandles.Count > 0)
            {
                ActionDisposable handle = _modeDisableHandles.Pop();
                handle.Dispose();
            }
        }
    }
}
