﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.GameControls;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.MagicHat.Placing;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.MagicHat
{
    public class MagicHatSystem : MonoBehaviour, IFieldDestabilizer
    {
        [SerializeField] private ChipComponent _magicHatPrefab;

        [Header("Charging")] [SerializeField] private float _chargingMultiplier = 1.5f;
        [SerializeField] private AnimationCurve _particlesWidthMultiplier;
        [SerializeField] private AnimationCurve _particlesStartSize;
        [SerializeField] private AnimationCurve _particlesGravityModifier;
        [SerializeField] private AnimationCurve _particlesEmissionRateOverTime;
        [SerializeField, RequiredField] private GameControlsMode _disablingMode;
        [SerializeField, RequiredField] private int _activationLevelNumber;

        [Inject] private GridSystem _grid;
        [Inject] private CombinationSystem _combinationSystem;
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;
        [Inject] private LevelRewardSequence _rewardSequence;
        [Inject] private GenerationSystem _generationSystem;
        [Inject] private ICurrentLevelInfo _currentLevelInfo;
        [Inject] private GameControlsSystem _controlsSystem;
        [Inject] private SwapSystem _swapSystem;

        [Inject(Id = GlobalInstaller.MainLevelPackControllerId)]
        private SingleLevelPackController _mainLevelPackController;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        private float _charge;
        private DeferredInvocation _hatsCreatingInv;
        private int _activatedChargersAmount;
        private const ImpactFlag _impactType = ImpactFlag.Replace;
        private readonly List<CellComponent> _pendingCells = new List<CellComponent>();
        private GameControlsContext _controlsContext;
        private ActionDisposable _disablingHandle;
        private List<IMagicHatPlacingRule> _placingRules = new List<IMagicHatPlacingRule>();

        public float ChargingMultiplier => _activatedChargersAmount > 1 ? _chargingMultiplier : 1f;
        public float ParticleWidth => _particlesWidthMultiplier.Evaluate(_activatedChargersAmount);
        public float ParticleStartSize => _particlesStartSize.Evaluate(_activatedChargersAmount);
        public float ParticleGravityModifier => _particlesGravityModifier.Evaluate(_activatedChargersAmount);
        public float ParticleEmissionRateOverTime => _particlesEmissionRateOverTime.Evaluate(_activatedChargersAmount);
        public bool IsCharged => _charge >= _maxCharge;
        public float ChargePercent => _charge / _maxCharge;
        public bool IsEnabled => enabled;
        public float RechargeAmount => _maxCharge - _charge;

        public bool IsStable => _pendingCells.Count == 0;

        private int _maxCharge => _currentLevelInfo.CurrentLevel.MagicHatMaxCharge;

        public event Action Stabilized;
        public event Action Destabilized;

        public event Action<IDeferredInvocationHandle> SelectingTargetCell;
        public event Action<float> ChargeChanged;
        public event Action<IDeferredInvocationHandle> HatsCreating;
        public event Action<IDeferredInvocationHandle, CellComponent> MagicHatCreating;
        public event Action Disabled;

        public void AddCharge(float value)
        {
            if (AddChargeInternal(value))
            {
                TryCreateHats();
            }
        }

        public void RegisterPlacingRule([NotNull] IMagicHatPlacingRule rule)
        {
            Assert.IsFalse(_placingRules.Contains(rule), "_placingRules.Contains(rule)");
            _placingRules.Add(rule);
        }

        public void UnregisterPlacingRule([NotNull] IMagicHatPlacingRule rule)
        {
            _placingRules.Remove(rule);
        }

        protected void Awake()
        {
            _controlsContext = _controlsSystem.ActiveContext;

            if (_mainLevelPackController.CurrentLevelPack == _currentLevelInfo.CurrentLevelPack &&
                _currentLevelInfo.CurrentLevelIndex + 1 < _activationLevelNumber)
            {
                enabled = false;
                _disablingHandle = _controlsContext.EnableMode(_disablingMode);
                return;
            }

            _fieldStabilizationSystem.AddDestabilizer(this);
            _fieldStabilizationSystem.AddStabilizationHandler(
                    TryCreateHats,
                    FieldStabilizationHandlerOrder.MagicHatSystem
                );
            _rewardSequence.Performing += OnLevelRewardSequencePerforming;
        }

        protected void Start()
        {
            _grid.ForEachCell(cell => SubscribeToCharger(cell.Chip));
            _generationSystem.ChipGenerated += SubscribeToCharger;
        }

        protected void OnDestroy()
        {
            if (_disablingHandle != null)
            {
                _disablingHandle?.Dispose();
                _disablingHandle = null;
            }
            else
            {
                _rewardSequence.Performing -= OnLevelRewardSequencePerforming;
                _fieldStabilizationSystem.RemoveStabilizationHandler(TryCreateHats);
                _fieldStabilizationSystem.RemoveDestabilizer(this);
            }
        }

        private void SubscribeToCharger([CanBeNull] ChipComponent chip)
        {
            var charger = chip?.GetComponent<MagicHatChargerComponent>();
            if (charger == null)
            {
                return;
            }

            chip.CellChanged += OnCellChanged;
            SubscribeToImpactEvents(chip.Cell);
        }

        private void OnCellChanged(ChipComponent chip, CellComponent prevCell)
        {
            UnsubscribeFromImpactEvents(prevCell);
            SubscribeToImpactEvents(chip.Cell);
        }

        private void SubscribeToImpactEvents([CanBeNull] CellComponent cell)
        {
            if (cell != null)
            {
                cell.ImpactScheduled += OnImpactScheduled;
            }
        }

        private void UnsubscribeFromImpactEvents([CanBeNull] CellComponent cell)
        {
            if (cell != null)
            {
                cell.ImpactScheduled -= OnImpactScheduled;
            }
        }

        private void OnImpactScheduled(CellComponent cell, Impact impact)
        {
            impact.Cancelling += OnScheduledImpactCancelling;

            _activatedChargersAmount = 0;
            foreach (CellComponent gridCell in _grid.Cells)
            {
                ChipComponent chip = gridCell.Chip;
                if (chip == null || !chip.WillBeConsumed() || !chip.GetComponent<MagicHatChargerComponent>())
                {
                    continue;
                }

                _activatedChargersAmount += 1;
            }
        }

        private void OnScheduledImpactCancelling(Impact impact)
        {
            impact.Cancelling -= OnScheduledImpactCancelling;
            _activatedChargersAmount = 0;
        }

        private void OnLevelRewardSequencePerforming()
        {
            _rewardSequence.Performing -= OnLevelRewardSequencePerforming;

            enabled = false;
            Disabled?.Invoke();
        }

        private bool AddChargeInternal(float value)
        {
            float newCharge = Mathf.Max(_charge + value, 0);
            if (Math.Abs(_charge - newCharge) < Mathf.Epsilon)
            {
                return false;
            }

            _charge = newCharge;
            ChargeChanged?.Invoke(ChargePercent);
            return true;
        }

        private void TryCreateHats()
        {
            if (!_fieldStabilizationSystem.IsFieldStable ||
                (_hatsCreatingInv != null && _hatsCreatingInv.IsLocked) ||
                !IsCharged)
            {
                return;
            }

            using (IDeferredInvocationHandle handle = new DeferredInvocation(BeginHatsCreation).Start())
            {
                SelectingTargetCell?.Invoke(handle);
            }
        }

        private void BeginHatsCreation()
        {
            var targetCells = new List<CellComponent>();
            while (IsCharged)
            {
                CellComponent found = FindCell();
                if (found == null)
                {
                    break;
                }

                AddPendingCell(found);
                targetCells.Add(found);
                AddChargeInternal(-_maxCharge);
            }

            if (targetCells.Count == 0)
            {
                UnityEngine.Debug.Log("Didn't find any valid cell to place MagicHat", this);
                return;
            }

            _hatsCreatingInv = new DeferredInvocation(() => CreateHats(targetCells));
            using (var handle = _hatsCreatingInv.Start())
            {
                HatsCreating?.Invoke(handle);
            }
        }

        private void CreateHats(List<CellComponent> cellsToCreateInto)
        {
            foreach (CellComponent cell in cellsToCreateInto)
            {
                var di = new DeferredInvocation(() => GenerateMagicHatChip(cell));
                using (var handle = di.Start())
                {
                    MagicHatCreating?.Invoke(handle, cell);
                }
            }
        }

        private void GenerateMagicHatChip([NotNull] CellComponent target)
        {
            ChipComponent hat = _generationSystem.InstantiateChip(_magicHatPrefab, target.Presenter.transform.position);
            target.ReplaceChip(hat, _impactType);
            RemovePendingCell(target);
        }
        
        private void AddPendingCell(CellComponent found)
        {
            _pendingCells.Add(found);

            if (_pendingCells.Count == 1)
            {
                Destabilized?.Invoke();
            }
        }

        private void RemovePendingCell(CellComponent target)
        {
            _pendingCells.Remove(target);

            if (_pendingCells.Count == 0)
            {
                Stabilized?.Invoke();
            }
        }

        private CellComponent FindCell()
        {
            _placingRules.ForEach(rule => rule.OnCheckBegin());

            // TODO: Cache candidates collection in class to prevent garbage collection
            var candidates = new List<CellComponent>();

            int rulePriorityCutoff = Enum.GetValues(typeof(MagicHatPlacingRulePriority)).Length;
            for (; rulePriorityCutoff > 0; rulePriorityCutoff -= 1)
            {
                List<IMagicHatPlacingRule> filteredPlacingRules =
                    _placingRules.Where(rule => (int)rule.Priority < rulePriorityCutoff).ToList();

                foreach (CellComponent cell in _grid.Cells)
                {
                    ChipComponent chip = cell.Chip;
                    if (chip != null && !chip.CanBeImpacted(_impactType) ||
                        _pendingCells.Contains(cell) ||
                        filteredPlacingRules.Any(rule => !rule.IsCellValid(cell)))
                    {
                        continue;
                    }

                    foreach (CellComponent neighbourCell in cell.IterateCellsAround())
                    {
                        if (_swapSystem.CanBeSwapped(cell, neighbourCell) &&
                            _combinationSystem.HasCombination(neighbourCell.Chip, _magicHatPrefab))
                        {
                            candidates.Add(cell);
                            break;
                        }
                    }
                }

                if (candidates.Count > 0)
                {
                    break;
                }
            }

            _placingRules.ForEach(rule => rule.OnCheckEnd());

            return candidates.Count > 0 ?
                candidates[_random.Next(candidates.Count)] :
                null;
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }
    }
}