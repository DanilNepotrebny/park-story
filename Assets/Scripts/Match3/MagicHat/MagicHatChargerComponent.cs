﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.MagicHat
{
    [RequireComponent(typeof(ChipComponent))]
    public class MagicHatChargerComponent : MonoBehaviour
    {
        [SerializeField] private float _charge;
        [SerializeField, Min(1)] private int _projectilesAmount;
        [SerializeField] private MagicHatParticle _particle;

        [Inject] private Instantiator _instantiator;
        [Inject] private MagicHatSystem _magicHatSystem;

        private ChipComponent _chip;

        public float Charge => _charge * _magicHatSystem.ChargingMultiplier;

        protected void Awake()
        {
            _chip = GetComponent<ChipComponent>();
        }

        private void Start()
        {
            _chip.Impacting += OnChipImpacting;
        }

        private void OnChipImpacting(ChipComponent chip, int impactValue)
        {
            if (!chip.IsConsumed || !_magicHatSystem.IsEnabled)
            {
                return;
            }

            float charge = Charge;
            float chargePortion = charge / _projectilesAmount;
            for (int i = 0; i < _projectilesAmount; ++i)
            {
                if (i == _projectilesAmount - 1)
                {
                    chargePortion = charge;
                }

                MagicHatParticle particle = _instantiator.Instantiate(_particle, transform.position);
                particle.Activate(chargePortion);
                charge -= chargePortion;
            }
        }
    }
}