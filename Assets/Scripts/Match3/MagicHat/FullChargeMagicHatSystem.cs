﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.MagicHat
{
    /// <summary>
    /// Once fully charges magic hat bar to produce magic hat when moves left is reached if it had not charged before
    /// </summary>
    public class FullChargeMagicHatSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private int _movesLeft = 1;

        [Inject] private MagicHatSystem _magicHatSystem;
        [Inject] private SaveGroup _saveGroup;

        [Inject(Id = LevelSettings.LimitationInstancesId)]
        private IReadOnlyList<Requirement.Instance> _limitations;

        private bool _hasCharged;

        private MovesRequirement.Instance MovesRequirement => _limitations.OfType<MovesRequirement.Instance>().First();

        public void Init()
        {
            _saveGroup.RegisterSynchronizable(name, this);
        }

        protected void Start()
        {
            if (_hasCharged)
            {
                return;
            }

            _magicHatSystem.ChargeChanged += OnMagicHatSystemChargeChanged;
            MovesRequirement.Changed += OnMovesRequirementChanged;
        }

        protected void OnDestroy()
        {
            UnsubscribeFromAllEvents();
        }

        private void OnMagicHatSystemChargeChanged(float fillRatio)
        {
            if (_magicHatSystem.IsCharged)
            {
                _hasCharged = true;
                UnsubscribeFromAllEvents();
            }
        }

        private void OnMovesRequirementChanged()
        {
            if (MovesRequirement.RemainingMoves == _movesLeft)
            {
                _magicHatSystem.AddCharge(_magicHatSystem.RechargeAmount);
            }
        }

        private void UnsubscribeFromAllEvents()
        {
            _magicHatSystem.ChargeChanged -= OnMagicHatSystemChargeChanged;
            MovesRequirement.Changed -= OnMovesRequirementChanged;
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncBool(nameof(_hasCharged), ref _hasCharged, false);
        }
    }
}