﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Match3.ChipTags;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3.MagicHat
{
    /// <summary>
    /// Combination controller for detecting combination of magic hat with compatible regular chip.
    /// </summary>
    [CreateAssetMenu(
        fileName = "MagicHatCombinationController",
        menuName = "Match 3/Chip controllers/Create magic hat combination controller")]
    public class MagicHatCombinationController : ScriptableObject, ICombinationController
    {
        [SerializeField] private ChipTagSet _compatibleTags = new ChipTagSet();

        bool ICombinationController.HasCombination(ChipComponent mainChip, ChipComponent additionalChip)
        {
            MagicHatComponent hat = mainChip.GetComponent<MagicHatComponent>();
            if (hat == null || additionalChip == null)
            {
                return false;
            }

            return additionalChip.CanBeImpacted(ImpactFlag.MagicHat) &&
                additionalChip.Tags.FindFirstOf(_compatibleTags) != null;
        }

        List<Impact> ICombinationController.ScheduleCombinationImpact(
                ChipComponent mainChip,
                ChipComponent additionalChip
            )
        {
            MagicHatComponent hatChip = mainChip.GetComponent<MagicHatComponent>();

            Assert.IsNotNull(hatChip);
            Assert.IsNotNull(additionalChip);

            ChipTag tagToImpact = additionalChip.Tags.FindFirstOf(_compatibleTags);
            Impact impact = hatChip.ScheduleActivation(tagToImpact);
            return new List<Impact> { impact };
        }

        void ICombinationController.AcceptCombination(ChipComponent mainChip, ChipComponent additionalChip)
        {
            List<Impact> impacts = ((ICombinationController)this).ScheduleCombinationImpact(mainChip, additionalChip);
            foreach (Impact impact in impacts)
            {
                impact.Perform();
            }
        }
    }
}