﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3.MagicHat
{
    [CreateAssetMenu(
        fileName = "DoubleHatCombinationController",
        menuName = "Match 3/Chip controllers/Create double hat combination controller")]
    public class DoubleHatCombinationController : ScriptableObject, ICombinationController
    {
        bool ICombinationController.HasCombination(ChipComponent mainChip, ChipComponent additionalChip)
        {
            return mainChip.GetComponent<MagicHatComponent>() != null &&
                additionalChip?.GetComponent<MagicHatComponent>() != null;
        }

        List<Impact> ICombinationController.ScheduleCombinationImpact(
                ChipComponent mainChip,
                ChipComponent additionalChip
            )
        {
            MagicHatComponent hatChip = mainChip.GetComponent<MagicHatComponent>();

            Assert.IsNotNull(hatChip);
            Assert.IsNotNull(additionalChip);

            return hatChip.ScheduleActivation(mainChip, additionalChip);
        }

        void ICombinationController.AcceptCombination(ChipComponent mainChip, ChipComponent additionalChip)
        {
            List<Impact> impacts = ((ICombinationController)this).ScheduleCombinationImpact(mainChip, additionalChip);
            impacts.ForEach(i => i.Perform());
        }
    }
}