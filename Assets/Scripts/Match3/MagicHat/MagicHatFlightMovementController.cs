﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using DreamTeam.Utils.Movement;
using DreamTeam.Utils.Tweening;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using Helper = DreamTeam.Utils.Movement.BezierMovementHelper;

namespace DreamTeam.Match3.MagicHat
{
    /// <summary>
    /// Service object that is responsible for magic hat appearance on the field. When hat is charged this object
    /// starts flying from hat panel to the specified position.
    /// </summary>
    public class MagicHatFlightMovementController : MovementController
    {
        [Header("Extremum")]
        [SerializeField, Tooltip("Coefficient of covered distance by a hat. Bigger value leads to farther distance to cover")]
        private float _extremumOffsetCoeff = 1f;

        [SerializeField, Min(2), Tooltip("Amount of samples along the path to determine zero point. Bigger amount - higher precision")]
        private int _extremumCalculationPrecision = 50;

        [Header("Additional time")]
        [SerializeField, Min(0.1f), Tooltip("More distance to cover, bigger movement time")]
        private float _timeByCoveringDistanceFactor = 2.3f;

        [SerializeField, Range(0, 1), Tooltip("Proportion between time by covering distance and time by move height (simulate gravity)")]
        private float _timeDistanceToHeightProportion = 0.8f;

        [Header("Easing")]
        [SerializeField] private Easing _firstPartEasing;
        [SerializeField] private Easing _lastPartEasing;

        [Inject] private MovementArea _movementArea;

        private readonly Vector3[] _ltFirstPath = new Vector3[Helper.BezierPartPiecesAmount];
        private readonly Vector3[] _ltLastPath = new Vector3[Helper.BezierPartPiecesAmount];
        private float _zeroPointRatio;
        private bool _isLastPartMovement;

        public event MovementUpdated Moved;

        protected override LTDescr StartMovement(Vector3 end, float time)
        {
            Helper.BezierPathPart pathPart = CreateMovementPath(transform.position, end);

            _zeroPointRatio = Helper.CalculateZeroPointRatio(
                new[] { pathPart },
                _extremumCalculationPrecision,
                (left, right) => left.y > right.y
            );
            IList<Helper.BezierPathPart> splittedPath = Helper.SplitPart(pathPart, _zeroPointRatio);

            var ltPath = Helper.BuildLeanTweenPath(splittedPath);
            Array.ConstrainedCopy(ltPath, 0, _ltFirstPath, 0, Helper.BezierPartPiecesAmount);
            Array.ConstrainedCopy(ltPath, Helper.BezierPartPiecesAmount, _ltLastPath, 0, Helper.BezierPartPiecesAmount);

            float firstPartTime, lastPartTime;
            CalculateMovementTime(time, end, out firstPartTime, out lastPartTime);

            _isLastPartMovement = false;
            MoveAlongPath(_ltFirstPath, firstPartTime, _firstPartEasing,
                () =>
                {
                    _isLastPartMovement = true;
                    LTDescr descr = MoveAlongPath(_ltLastPath, lastPartTime, _lastPartEasing, OnFinished);
                    descr.updateNow();
                }
            );

            return null;
        }

        private void CalculateMovementTime(float totalTime, Vector3 end, out float firstPartTime, out float lastPartTime)
        {
            // calculate base time to cover the distance
            float dist = (end - transform.position).magnitude;
            totalTime += Mathf.Sqrt(dist) / _timeByCoveringDistanceFactor;

            // bigger fall by Y component means less time to cover the distance (fake gravity)
            float firstYDiff = Mathf.Abs(_ltFirstPath[0].y - _ltFirstPath[3].y);
            float lastYDiff = Mathf.Abs(_ltLastPath[0].y - _ltLastPath[3].y);
            float diffRatio = lastYDiff / firstYDiff;
            float timeRatio = _zeroPointRatio * _timeDistanceToHeightProportion +
                diffRatio * (1f - _timeDistanceToHeightProportion);

            Assert.IsTrue(timeRatio >= 0 && timeRatio <= 1);
            firstPartTime = totalTime * timeRatio;
            lastPartTime = totalTime - firstPartTime;
        }

        protected override void SetupMovement(LTDescr descriptor)
        {
            // nothing to do here. All needed setup is doing in MoveAlongPath() method
        }

        protected override void OnUpdated(float value, float progress)
        {
            Moved?.Invoke(progress, !_isLastPartMovement);
            if (_isLastPartMovement)
            {
                progress = _zeroPointRatio + progress * (1f - _zeroPointRatio);
            }
            else
            {
                progress *= _zeroPointRatio;
            }

            base.OnUpdated(value, progress);
        }

        protected override void OnDrawPath()
        {
            base.OnDrawPath();

            Helper.DrawPath(_ltFirstPath.Concat(_ltLastPath).ToArray());
        }

        private LTDescr MoveAlongPath(Vector3[] path, float time, Easing easing, Action onComplete)
        {
            var descriptor = LeanTween.move(gameObject, path, time);
            descriptor.setOrientToPath2d(IsOrientedByPath);
            descriptor.setOnComplete(onComplete);

            if (!Mathf.Approximately(Speed, 0f))
            {
                descriptor.setSpeed(Speed);
            }
            if (HasOnUpdatedListeners)
            {
                descriptor.setOnUpdateRatio(OnUpdated);
            }

            easing.SetupDescriptor(descriptor);
            return descriptor;
        }

        private Helper.BezierPathPart CreateMovementPath(Vector3 start, Vector3 end)
        {
            Vector3 midPoint = (start + end) * 0.5f;
            Vector3 movement = end - start;
            float dist = movement.magnitude;
            Vector3 movDir = movement.normalized;
            Vector3 plane = end.x > start.x ? Vector3.back : Vector3.forward;
            Vector3 perp = Vector3.Cross(movDir, plane).normalized;
            Vector3 controlPointOffset = midPoint + perp * dist * _extremumOffsetCoeff;

            Vector3 controlPoint1 = (start + controlPointOffset) * 0.5f;
            Vector3 controlPoint2 = (end + controlPointOffset) * 0.5f;

            _movementArea.FitToMovementArea(ref controlPoint1);
            _movementArea.FitToMovementArea(ref controlPoint2);

            return new Helper.BezierPathPart(
                new []
                {
                    start, controlPoint1, end, controlPoint2
                }
            );
        }

        #region Inner types

        public delegate void MovementUpdated(float ratio, bool isExtremumHeading);

        #endregion Inner types
    }
}
