﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Movement;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.MagicHat
{
    [RequireComponent(typeof(MovementSequence), typeof(ParticleSystem))]
    public class MagicHatParticle : PathMovementController
    {
        [SerializeField] private TrailRenderer _trail;

        [Inject] private MagicHatSystem _system;
        [Inject] private MagicHatPanel _panel;

        private float _charge;
        private ParticleSystem _ps;
        private MovementSequence _sequence;

        public virtual void Activate(float charge)
        {
            _charge = charge;
            _trail.widthCurve.keys = new[] { new Keyframe(0, _system.ParticleWidth) };

            ParticleSystem.MainModule main = _ps.main;
            main.startSize = new ParticleSystem.MinMaxCurve(_system.ParticleStartSize);
            main.gravityModifier = new ParticleSystem.MinMaxCurve(_system.ParticleGravityModifier);

            ParticleSystem.EmissionModule emission = _ps.emission;
            emission.rateOverTime = new ParticleSystem.MinMaxCurve(_system.ParticleEmissionRateOverTime);

            _sequence.Finished += OnMovingFinished;
            _sequence.Move(_panel.ParticlesTarget, true);
        }

        protected void Awake()
        {
            _ps = GetComponent<ParticleSystem>();
            _sequence = GetComponent<MovementSequence>();
        }

        protected override void CalculatePath(
            Vector3 start,
            out Vector3 controlPoint1,
            out Vector3 controlPoint2,
            Vector3 end)
        {
            controlPoint1 = start - Sequence.StartPosition + start;
            controlPoint2 = controlPoint1;
        }

        private void OnMovingFinished(MovementSequence mover)
        {
            _system.AddCharge(_charge);
            _panel.OnParticleMovingCompleted();
            _trail.gameObject.transform.SetParent(null);
        }
    }
}