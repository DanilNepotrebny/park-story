﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Match3.MagicHat
{
    /// <summary>
    /// Incapsulates magic hat functionality. Magic hat is a bonus chip which is able to impact all chips that share
    /// color with the chip magic hat is swapped with.
    /// </summary>
    [RequireComponent(typeof(ChipComponent))]
    public class MagicHatComponent : MonoBehaviour, IImpactController
    {
        [SerializeField] private int _impact = -1;
        [SerializeField] private ChipTagSet _consumptionTags = new ChipTagSet();
        [FormerlySerializedAs("_colorChipTagsAdapter"), SerializeField]
        private ColorChipTagsAdapter _impactingChipTagsAdapter = new ColorChipTagsAdapter();

        [Space, SerializeField] private DoubleHatController _doubleHatPrefab;
        [SerializeField, Min(0f)] private float _impactFiringDelay = 0.05f;

        [Inject] private GridSystem _grid;
        [Inject] private ImpactSystem _impactSystem;
        [Inject] private Instantiator _instantiator;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        private ChipTag _tagToImpact;
        private List<Impact> _impacts;
        private ChipComponent _chip;
        private bool _processingImpactScheduling;
        private List<Impact> _scheduledImpacts = new List<Impact>();
        private Coroutine _impactCoroutine;
        private DoubleHatController _doubleHatController;

        public int Impact => _impact;

        /// <summary> Raised when magic hat is activated and found specified chips to consume. </summary>
        public event ActivatingDelegate Activating;

        public event ChipImpactingDelegate ChipImpacting;
        public event ChipsImpactingDelegate ChipsImpacting;
        public event ChipImpactingCancelledDelegate ChipImpactCancelling;

        /// <summary> Schedules magic hat chip consume and makes it to impact all chips with specified chip tag </summary>
        public Impact ScheduleActivation([NotNull] ChipTag tagToImpact)
        {
            _tagToImpact = tagToImpact;
            return _chip.ScheduleConsumption(ImpactFlag.MagicHatActivation);
        }

        public List<Impact> ScheduleActivation([NotNull] ChipComponent hat1, [NotNull] ChipComponent hat2)
        {
            _doubleHatController = _instantiator.Instantiate(_doubleHatPrefab);
            return _doubleHatController.ScheduleActivation(hat1, hat2);
        }

        protected void Awake()
        {
            _chip = GetComponent<ChipComponent>();
            _chip.AddImpactController(this);
            _chip.CellChanged += OnCellChanged;
            SubscribeToImpactEvents(_chip.Cell);
        }

        private void OnCellChanged(ChipComponent chip, CellComponent prevCell)
        {
            UnsubscribeFromImpactEvents(prevCell);
            SubscribeToImpactEvents(_chip.Cell);
        }

        private void SubscribeToImpactEvents([CanBeNull] CellComponent cell)
        {
            if (cell != null)
            {
                cell.ImpactScheduled += OnCellImpactScheduled;
            }
        }

        private void UnsubscribeFromImpactEvents([CanBeNull] CellComponent cell)
        {
            if (cell != null)
            {
                cell.ImpactScheduled -= OnCellImpactScheduled;
            }
        }

        private void OnCellImpactScheduled(CellComponent cell, Impact impact)
        {
            if (_scheduledImpacts.Count > 0 || _processingImpactScheduling ||
                !_chip.CanBeImpacted(impact.Flag) ||
                _chip.IsConsumed || !_chip.WillBeConsumed())
            {
                return;
            }

            Assert.AreEqual(_chip.Cell, cell);
            impact.Cancelling += OnScheduledImpactCancelling;

            _processingImpactScheduling = true;
            {
                if (impact.Flag == ImpactFlag.DoubleMagicHatActivation)
                {
                    // nothing to call here
                }
                else
                {
                    OnMagicHatActivationScheduled();
                }
            }
            _processingImpactScheduling = false;
        }

        private void OnMagicHatActivationScheduled()
        {
            List<CellComponent> cells = FindCells();

            if (_tagToImpact != null)
            {
                _chip.Tags.Push(_impactingChipTagsAdapter[_tagToImpact]);
            }

            _scheduledImpacts = ScheduleImpact(cells);
        }

        private void OnScheduledImpactCancelling(Impact impact)
        {
            impact.Cancelling -= OnScheduledImpactCancelling;

            if (_processingImpactScheduling)
            {
                UnityEngine.Debug.LogError($"{nameof(MagicHatComponent)}: Cancelling while processing impact scheduling", this);
            }

            _processingImpactScheduling = true;
            {
                if (impact.Flag == ImpactFlag.DoubleMagicHatActivation)
                {
                    OnCancelDoubleMagicHatActivation();
                }
                else
                {
                    OnCancelMagicHatActivation();
                }
            }
            _processingImpactScheduling = false;
        }

        private void OnCancelMagicHatActivation()
        {
            if (_tagToImpact != null)
            {
                _chip.Tags.Pop(_impactingChipTagsAdapter[_tagToImpact]);
                _tagToImpact = null;
            }

            CancelScheduledImpacts();
        }

        private void OnCancelDoubleMagicHatActivation()
        {
            if (_doubleHatController != null)
            {
                _doubleHatController.Dispose();
                _doubleHatController = null;
            }
        }

        private void CancelScheduledImpacts()
        {
            _scheduledImpacts.ForEach(impact => impact.Cancel());
            _scheduledImpacts.Clear();
        }

        private bool IsChipSuitable(ChipComponent chip)
        {
            return chip != null &&
                   chip.CanBeImpacted(ImpactFlag.MagicHat) &&
                   (_tagToImpact == null || chip.Tags.Has(_tagToImpact));
        }

        private bool IsChipSuitableAndIsntConsuming(ChipComponent chip)
        {
            return IsChipSuitable(chip) &&
                   !chip.WillBeConsumed();
        }

        private ChipTag FindSuitableTag()
        {
            var counters = new Dictionary<ChipTag, int>();

            foreach (CellComponent cell in _grid.Cells)
            {
                ChipComponent chip = cell.Chip;
                if (!IsChipSuitableAndIsntConsuming(chip))
                {
                    continue;
                }

                ChipTag chipTag = chip.Tags.FindFirstOf(_consumptionTags);
                if (chipTag != null)
                {
                    if (!counters.ContainsKey(chipTag))
                    {
                        counters.Add(chipTag, 1);
                    }
                    else
                    {
                        counters[chipTag]++;
                    }
                }
            }

            int maxCounter = int.MinValue;
            ChipTag result = null;

            foreach (KeyValuePair<ChipTag, int> pair in counters)
            {
                if (pair.Value > maxCounter)
                {
                    maxCounter = pair.Value;
                    result = pair.Key;
                }
            }
            return result;
        }

        private void OnChipImpacting(ChipComponent chip, int impactValue)
        {
            if (IsChipSuitableAndIsntConsuming(chip))
            {
                return;
            }

            chip.Impacting -= OnChipImpacting;

            ChipComponent candidate = chip;

            // Trying to find another suitable chip in this cell
            while (candidate != null)
            {
                candidate = candidate.Child;

                if (IsChipSuitableAndIsntConsuming(candidate))
                {
                    candidate.Impacting += OnChipImpacting;
                    return;
                }
            }

            Impact impact = _impacts.Find(i => i.Cell == chip.Cell);
            impact.Cancel();
        }

        private void OnImpactInvocationEnd([NotNull] Impact impact)
        {
            if (!IsChipSuitable(impact.Cell.Chip))
            {
                impact.Cancel();
                return;
            }

            _impacts.Remove(impact);
            impact.Cell.Chip.Impacting -= OnChipImpacting;
            impact.Perform();
        }

        private List<CellComponent> FindCells()
        {
            if (_tagToImpact == null)
            {
                _tagToImpact = FindSuitableTag();
            }

            return _grid.Cells.
                Where(cell => IsChipSuitableAndIsntConsuming(cell.Chip)).
                ToList();
        }

        private List<Impact> ScheduleImpact(List<CellComponent> cells)
        {
            return _impactSystem.ScheduleImpact(cells, _impact, ImpactFlag.MagicHat);
        }

        private void OnImpactCancelling([NotNull] Impact impact)
        {
            ChipImpactCancelling?.Invoke(impact);

            _impacts.Remove(impact);
        }

        private IEnumerator LaunchProjectiles(
            [NotNull] IDeferredInvocationHandle impactEndHandle,
            IList<Impact> impacts,
            float delay)
        {
            using (impactEndHandle.Lock())
            {
                ChipsImpacting?.Invoke(impacts.Count);

                foreach (Impact impact in impacts)
                {
                    ChipComponent chip = impact.Cell.Chip;
                    if (!IsChipSuitable(chip))
                    {
                        impact.Cancel();
                        continue;
                    }

                    chip.Impacting += OnChipImpacting;

                    var impactInvocation = new DeferredInvocation<Impact>(OnImpactInvocationEnd, impact);
                    using (var impactHandle = impactInvocation.Start())
                    {
                        ChipImpacting?.Invoke(chip, impact, impactHandle);
                    }

                    yield return new WaitForSeconds(delay);
                }
            }
        }

        #region IImpactController

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.MagicHat;
        }

        void IImpactController.OnImpact(
            ChipComponent chip,
            ImpactFlag impactType,
            IDeferredInvocationHandle impactEndHandle,
            ref bool isModal)
        {
            UnityEngine.Debug.Assert(chip.IsConsumed);

            CancelScheduledImpacts(); // reset recently scheduled because actual impact performs with animation

            if (impactType == ImpactFlag.DoubleMagicHatActivation)
            {
                return;
            }

            isModal = true;
            using (var endHandle = impactEndHandle.Lock())
            {
                Activating?.Invoke(endHandle);

                List<CellComponent> cells = FindCells();
                _impacts = ScheduleImpact(cells);
                _impacts.ForEach(impact => impact.Cancelling += OnImpactCancelling);
                _impacts.RandomShuffle(_random);

                Assert.IsNull(_impactCoroutine);
                _impacts.ForEach(i => i.Begin());
                _impactCoroutine = StartCoroutine(LaunchProjectiles(endHandle, _impacts.ToList(), _impactFiringDelay));
            }
        }

        void IImpactController.Cancel()
        {
            // nothing to do
        }

        #endregion IImpactController

        #region Inner types

        public delegate void ActivatingDelegate(IDeferredInvocationHandle hatImpactEndHandle);
        public delegate void ChipsImpactingDelegate(int chipsAmount);
        public delegate void ChipImpactingCancelledDelegate(Impact impact);
        public delegate void ChipImpactingDelegate(
            ChipComponent chip,
            Impact impact,
            IDeferredInvocationHandle chipImpactInvocationHandle);

        [Serializable] public class ColorChipTagsAdapter : KeyValueList<ChipTag, ChipTag> {}

        #endregion Inner types
    }
}
