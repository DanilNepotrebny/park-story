﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.GameControls;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    public class TapAndTapSwapInput : MonoBehaviour
    {
        [SerializeField, RequiredField] private TapAndTapSelectionPresenter _selectionPresenter;

        [Inject] private GridSystem _grid;
        [Inject] private SwapSystem _swapSystem;
        [Inject] private GameControlsSystem _gameControlsSystem;

        private Queue<CellComponent> _subscribedCells = new Queue<CellComponent>();

        public CellComponent SelectedCell { get; private set; }

        protected void OnEnable()
        {
            foreach (CellComponent cell in _grid.Cells)
            {
                cell.Presenter.CellTapped += OnCellTapped;
                cell.Presenter.CellSwiped += OnCellSwiped;
                _subscribedCells.Enqueue(cell);
            }

            _gameControlsSystem.ActiveContext.GameControlsModeActivityChanged += OnGameControlsModeActivityChanged;
        }

        protected void OnDisable()
        {
            while (_subscribedCells.Count > 0)
            {
                CellComponent cell = _subscribedCells.Dequeue();
                cell.Presenter.CellTapped -= OnCellTapped;
                cell.Presenter.CellSwiped -= OnCellSwiped;
            }

            _gameControlsSystem.ActiveContext.GameControlsModeActivityChanged -= OnGameControlsModeActivityChanged;
        }

        private void OnCellTapped(CellComponent cell)
        {
            if (SelectedCell == null)
            {
                SelectCell(cell);
            }
            else if (SelectedCell != cell)
            {
                if (_swapSystem.CanBeSwapped(SelectedCell, cell))
                {
                    SwapChips(cell);
                }
                else
                {
                    DeselectCell();
                    SelectCell(cell);
                }
            }
        }

        private void SwapChips(CellComponent tappedCell)
        {
            Vector2 dirVector = tappedCell.transform.position - SelectedCell.transform.position;
            Direction dir = _swapSystem.CalculateSwapDirection(dirVector);
            if (dir != Direction.None)
            {
                _swapSystem.SwapChips(SelectedCell, dir);
            }

            DeselectCell();
        }

        private void OnCellSwiped(CellPresenter cell, Vector2 swipeOffset)
        {
            DeselectCell();
        }

        private void OnGameControlsModeActivityChanged([NotNull] GameControlsMode mode, bool isEnabled)
        {
            DeselectCell(); // TODO: Check logic. Maybe we don't need to deselect cell on every game controls mode change
        }

        private void OnSelectedChipImpacted(ChipComponent chip)
        {
            chip.Impacted -= OnSelectedChipImpacted;
            DeselectCell();
        }

        private void SelectCell(CellComponent cell)
        {
            if (!cell.IsEmpty && cell.Chip.CanBeSwapped)
            {
                SelectedCell = cell;
                SelectedCell.Chip.Impacted += OnSelectedChipImpacted;
                _selectionPresenter.SetEnabled(cell);
            }
        }

        private void DeselectCell()
        {
            if (SelectedCell != null)
            {
                SelectedCell = null;
                _selectionPresenter.SetDisabled();
            }
        }
    }
}