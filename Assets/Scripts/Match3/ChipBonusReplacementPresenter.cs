﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(ChipPresenter))]
    public class ChipBonusReplacementPresenter : MonoBehaviour, IImpactController, IFieldDestabilizer
    {
        [SerializeField] private string _replacingByBonusParamName = "OnReplacingByBonus";
        [SerializeField] private string _replacingByBonusState = "ReplacingByBonus";

        [SerializeField] private string _bonusReplaceParamName = "OnBonusReplace";
        [SerializeField] private string _bonusReplaceState = "BonusReplace";

        [SerializeField] private string _vfxLaunchEvent = "BonusReplacementVFXLaunch";
        [SerializeField, RequiredField] private GameObject _vfxPrefab;

        [Inject] private Instantiator _instantiator;
        [Inject] private FieldStabilizationSystem _stabilizationSystem;

        private ChipPresenter _chipPresenter;
        private IDeferredInvocationHandle _impactEndHandle;
        private bool _isAnimationPlaying;
        private const ImpactFlag _impactType = ImpactFlag.BonusReplace;

        public bool IsAnimationPlaying
        {
            get { return _isAnimationPlaying; }
            private set
            {
                if (_isAnimationPlaying == value)
                {
                    return;
                }

                _isAnimationPlaying = value;
                if (_isAnimationPlaying)
                {
                    Destabilized?.Invoke();
                }
                else
                {
                    Stabilized?.Invoke();
                }
            }
        }

        public bool IsStable => !IsAnimationPlaying;

        public event Action Stabilized;
        public event Action Destabilized;

        protected void Awake()
        {
            _chipPresenter = GetComponent<ChipPresenter>();
            _chipPresenter.AddAnimationStateEnterHandler(_replacingByBonusState, OnReplacingByBonusStateEntered);
            _chipPresenter.AddAnimationStateExitHandler(_replacingByBonusState, OnReplacingByBonusStateExited);
            _chipPresenter.AddAnimationStateEnterHandler(_bonusReplaceState, OnBonusReplaceStateEntered);
            _chipPresenter.AddAnimationStateExitHandler(_bonusReplaceState, OnBonusReplaceStateExited);
            _chipPresenter.ChipComponent.ChipReplacing += OnChipReplacing;
            _chipPresenter.ChipComponent.AddImpactController(this);
            _chipPresenter.ChipAnimatorController.AddAnimationEventHandler(_vfxLaunchEvent, OnVFXLaunchEventOccurred);

            _stabilizationSystem.AddDestabilizer(this);
        }

        protected void OnDestroy()
        {
            _stabilizationSystem.RemoveDestabilizer(this);
            _chipPresenter.ChipComponent.RemoveImpactController(this);
            _chipPresenter.ChipComponent.ChipReplacing -= OnChipReplacing;
        }

        private void OnReplacingByBonusStateEntered()
        {
            IsAnimationPlaying = true;
        }

        private void OnReplacingByBonusStateExited()
        {
            IsAnimationPlaying = false;
            _impactEndHandle?.Unlock();
            _impactEndHandle = null;
        }

        private void OnBonusReplaceStateEntered()
        {
            IsAnimationPlaying = true;
        }

        private void OnBonusReplaceStateExited()
        {
            IsAnimationPlaying = false;
        }

        private void OnChipReplacing(ImpactFlag impact)
        {
            if (impact == ImpactFlag.BonusReplace)
            {
                _chipPresenter.SetTrigger(_bonusReplaceParamName);
            }
        }

        private void OnVFXLaunchEventOccurred()
        {
            _instantiator.Instantiate(_vfxPrefab, gameObject.transform.position);
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }

        #region IImpactController

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.Replacement;
        }

        void IImpactController.OnImpact(
            ChipComponent chip,
            ImpactFlag impactType,
            IDeferredInvocationHandle impactEndHandle,
            ref bool isModal)
        {
            if (impactType != _impactType)
            {
                return;
            }

            isModal = true;
            _impactEndHandle = impactEndHandle.Lock();

            _chipPresenter.SetTrigger(_replacingByBonusParamName);
        }

        void IImpactController.Cancel()
        {
            // nothing to do
        }

        #endregion IImpactController
    }
}
