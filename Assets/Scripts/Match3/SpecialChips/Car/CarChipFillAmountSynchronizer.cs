﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace DreamTeam.Match3.SpecialChips.Car
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(CarChipComponent))]
    public class CarChipFillAmountSynchronizer : MonoBehaviour
    {
        #if UNITY_EDITOR

        private int _chipsToFill;
        private CarChipComponent _carChip;
        private GridSystem.EditorApi _grid;

        private GridSystem.EditorApi Grid => _grid ?? (_grid = FindObjectOfType<GridSystem>()?.GetEditorApi());

        protected void Awake()
        {
            _carChip = GetComponent<CarChipComponent>();
        }

        protected void Start()
        {
            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                enabled = false;
                return;
            }

            SynchronizeThisWithOuter();
            _chipsToFill = _carChip.ChipsToFill;
        }

        protected void Update()
        {
            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                enabled = false;
                return;
            }

            if (_carChip.ChipsToFill != _chipsToFill)
            {
                _chipsToFill = _carChip.ChipsToFill;
                SynchronizeOuterWithThis();
            }
        }

        private void SynchronizeThisWithOuter()
        {
            foreach (CellComponent cell in Grid.Cells)
            {
                if (cell == null)
                {
                    continue;
                }

                ChipContainerComponent container = cell.GetComponent<ChipContainerComponent>();
                if (container == null || container.Chip == null)
                {
                    continue;
                }

                CarChipComponent car = container.Chip.GetComponent<CarChipComponent>();
                if (car == null)
                {
                    continue;
                }

                SerializedObject serialized = new SerializedObject(car);
                _carChip.ChipsToFill = car.ChipsToFill;
                serialized.ApplyModifiedPropertiesWithDirtyFlag();
                break;
            }
        }

        private void SynchronizeOuterWithThis()
        {
            foreach (CellComponent cell in Grid.Cells)
            {
                if (cell == null)
                {
                    continue;
                }

                ChipContainerComponent container = cell.GetComponent<ChipContainerComponent>();
                if (container == null || container.Chip == null)
                {
                    continue;
                }

                CarChipComponent car = container.Chip.GetComponent<CarChipComponent>();
                if (car != null)
                {
                    SerializedObject serialized = new SerializedObject(car);
                    car.ChipsToFill = _carChip.ChipsToFill;
                    serialized.ApplyModifiedPropertiesWithDirtyFlag();
                }
            }
        }

        #endif // UNITY_EDITOR
    }
}