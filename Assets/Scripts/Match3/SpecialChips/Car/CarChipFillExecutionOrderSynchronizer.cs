﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;

#endif // UNITY_EDITOR

namespace DreamTeam.Match3.SpecialChips.Car
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(CarChipComponent))]
    public class CarChipFillExecutionOrderSynchronizer : MonoBehaviour
    {
        [Inject] private GridSystem _gridSystem;

        private CarChipComponent _carChip;
        private bool _wasInCell;

        #if UNITY_EDITOR
        private GridSystem.EditorApi Grid
        {
            get
            {
                var grid = FindObjectOfType<GridSystem>();
                if (grid != null)
                {
                    return grid.GetEditorApi();
                }

                return null;
            }
        }
        #endif // UNITY_EDITOR

        protected void Awake()
        {
            _carChip = GetComponent<CarChipComponent>();
            _carChip.FullFillPredicted += OnFullFillPredicted;
        }

        protected void Start()
        {
            if (!IsInCell())
            {
                return;
            }

            if (IsEditorOnly() && _carChip.FillExecutionOrder == CarChipComponent.FillExecutionOrderUnset)
            {
                SerializedObjectAction(_carChip, () => _carChip.FillExecutionOrder = GetHighestExecutionOrder() + 1);
            }
        }

        protected void OnDestroy()
        {
            if (!_wasInCell)
            {
                return;
            }

            var cars = new List<CarChipComponent>();
            ForEachSimilarCarChipOnGrid(
                car =>
                {
                    if (car != _carChip && car.FillExecutionOrder > _carChip.FillExecutionOrder)
                    {
                        cars.Add(car);
                    }
                });

            if (cars.All(car => car.FillExecutionOrder > 0))
            {
                cars.ForEach(car => SerializedObjectAction(car, () => car.FillExecutionOrder -= 1));
            }
        }

        private void OnFullFillPredicted()
        {
            _carChip.FillExecutionOrder = CarChipComponent.FillExecutionOrderUnset;

            var cars = new List<CarChipComponent>();
            ForEachSimilarCarChipOnGrid(
                car =>
                {
                    if (car != _carChip && car.FillExecutionOrder > _carChip.FillExecutionOrder)
                    {
                        cars.Add(car);
                    }
                });

            cars.ForEach(car => SerializedObjectAction(car, () => car.FillExecutionOrder -= 1));
        }

        private int GetHighestExecutionOrder()
        {
            int[] highestOrder = { -1 };

            ForEachSimilarCarChipOnGrid(
                car =>
                {
                    if (car.FillExecutionOrder > highestOrder[0])
                    {
                        highestOrder[0] = car.FillExecutionOrder;
                    }
                });

            return highestOrder[0];
        }

        private bool IsInCell()
        {
            bool[] found = { false };

            ForEachSimilarCarChipOnGrid(
                car =>
                {
                    if (car == _carChip)
                    {
                        found[0] = true;
                    }
                });

            _wasInCell = found[0];
            return found[0];
        }

        private bool IsSimilar([CanBeNull] CarChipComponent car)
        {
            return car != null && car.FillChipTags.SequenceEqual(_carChip.FillChipTags);
        }

        private static void SerializedObjectAction([NotNull] Object obj, [NotNull] Action action)
        {
            #if UNITY_EDITOR
            if (IsEditorOnly())
            {
                SerializedObject serialized = new SerializedObject(obj);
                action.Invoke();
                serialized.ApplyModifiedPropertiesWithDirtyFlag();
                return;
            }
            #endif // UNITY_EDITOR

            action.Invoke();
        }

        private void ForEachSimilarCarChipOnGrid([NotNull] Action<CarChipComponent> action)
        {
            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                if (_gridSystem == null)
                {
                    return;
                }

                foreach (CellComponent cell in _gridSystem.Cells)
                {
                    CarChipComponent car = cell.Chip?.GetComponent<CarChipComponent>();
                    if (!IsSimilar(car))
                    {
                        continue;
                    }

                    action.Invoke(car);
                }
            }
            #if UNITY_EDITOR
            else
            {
                if (Grid == null)
                {
                    return;
                }

                foreach (CellComponent cell in Grid.Cells)
                {
                    if (cell == null)
                    {
                        continue;
                    }

                    ChipContainerComponent container = cell.GetComponent<ChipContainerComponent>();
                    if (container == null || container.Chip == null)
                    {
                        continue;
                    }

                    CarChipComponent car = container.Chip.GetComponent<CarChipComponent>();
                    if (!IsSimilar(car))
                    {
                        continue;
                    }

                    action.Invoke(car);
                }
            }
            #endif // UNITY_EDITOR
        }

        private static bool IsEditorOnly()
        {
            return !EditorUtils.IsPlayingOrWillChangePlaymode();
        }
    }
}