﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif // UNITY_EDITOR

namespace DreamTeam.Match3.SpecialChips.Car
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(CarChipPresenter))]
    public class CarChipFillExecutionOrderEditorDrawer : MonoBehaviour
    {
        [SerializeField] private CarChipComponent _carComponent;

        #if UNITY_EDITOR
        private static GUIStyle _style;
        private static readonly Vector2 _offset = new Vector2(-14, -32);
        private const int _fontSize = 50;

        protected void OnEnable()
        {
            if (!Application.isPlaying)
            {
                SceneView.onSceneGUIDelegate += DrawSceneGUI;
            }
        }

        protected void OnDisable()
        {
            if (!Application.isPlaying)
            {
                SceneView.onSceneGUIDelegate -= DrawSceneGUI;
            }
        }

        protected void OnValidate()
        {
            if (_carComponent == null)
            {
                UnityEngine.Debug.LogAssertion($"Missing {nameof(_carComponent)}", this);
            }
        }

        private void DrawSceneGUI(SceneView sceneView)
        {
            if (this == null)
            {
                SceneView.onSceneGUIDelegate -= DrawSceneGUI;
                return;
            }

            if (_carComponent == null || _carComponent.FillExecutionOrder == CarChipComponent.FillExecutionOrderUnset)
            {
                return;
            }

            if (_style == null)
            {
                _style = new GUIStyle
                {
                    normal = { textColor = Color.black },
                    fontSize = _fontSize
                };
            }

            Handles.BeginGUI();
            {
                Vector2 pos2D = HandleUtility.WorldToGUIPoint(transform.position) + _offset;
                GUI.Label(new Rect(pos2D.x, pos2D.y, 0, 0), (_carComponent.FillExecutionOrder + 1).ToString(), _style);
            }
            Handles.EndGUI();
        }
        #endif // UNITY_EDITOR
    }
}