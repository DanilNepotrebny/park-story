﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.MoveWeight;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
#if UNITY_EDITOR
using UnityEditor;

#endif // UNITY_EDITOR

namespace DreamTeam.Match3.SpecialChips.Car
{
    [RequireComponent(typeof(ChipComponent), typeof(SourceChipsMoveWeightModifier))]
    public class CarChipComponent : MonoBehaviour, IImpactController, IChipConsumptionHandler
    {
        [SerializeField] private ChipTagSet _chipTags;
        [SerializeField] private int _chipsToFill;
        [SerializeField] private Direction _direction;
        [SerializeField, HideInInspector] private int _fillExecutionOrder = FillExecutionOrderUnset;

        [Inject] private GridSystem _gridSystem;
        [Inject] private ImpactSystem _impactSystem;
        [Inject] private ChipConsumptionDispatcher _consumptionDispatcher;

        private ChipComponent _chip;
        private int _fillValue;
        private int _predictedFillValue;
        private readonly List<CellComponent> _cellsToHarvest = new List<CellComponent>();
        private Impact _scheduledSelfImpact;
        private int _scheduledConsumptionTargetsAmount;
        private bool _processingImpactScheduling;
        private readonly List<Impact> _scheduledImpacts = new List<Impact>();
        private SourceChipsMoveWeightModifier _sourceChipsWeightModifier;
        private HashSet<ChipComponent> _scheduledConsumingChips = new HashSet<ChipComponent>();

        private RequiredChipConsumingDelegate _requiredChipConsumingDelegate;

        public const int FillExecutionOrderUnset = -1;

        public ChipTagSet FillChipTags => _chipTags;
        public Direction HeadingDirection => _direction;
        public float FillPercent => _fillValue / (float)ChipsToFill;

        public int ChipsToFill
        {
            get { return _chipsToFill; }
            set { _chipsToFill = value; }
        }

        public int FillExecutionOrder
        {
            get { return _fillExecutionOrder; }
            set
            {
                int clampedValue = Mathf.Clamp(value, FillExecutionOrderUnset, int.MaxValue);
                if (_fillExecutionOrder == clampedValue)
                {
                    return;
                }

                _fillExecutionOrder = clampedValue;
                SubscribeToChipConsumption();
            }
        }

        public int PredictedFillValue
        {
            get { return _predictedFillValue; }
            set
            {
                int clampedValue = Mathf.Clamp(value, 0, ChipsToFill);
                if (_predictedFillValue == clampedValue || IsFilled(_predictedFillValue))
                {
                    return;
                }

                _predictedFillValue = clampedValue;

                if (IsFilled(_predictedFillValue))
                {
                    UnsubscribeFromChipConsumption();
                    FullFillPredicted?.Invoke();
                }
            }
        }

        public int FillValue
        {
            get { return _fillValue; }
            set
            {
                int clampedValue = Mathf.Clamp(value, 0, ChipsToFill);
                if (_fillValue == clampedValue || IsFilled(_fillValue))
                {
                    return;
                }

                _fillValue = clampedValue;
                FillChanged?.Invoke(FillPercent);

                if (IsFilled(_fillValue))
                {
                    UnsubscribeFromChipConsumption();
                    ScheduleConsume(_chip.Cell).Perform();
                }
            }
        }

        private int ScheduledConsumptionTargetsAmount
        {
            get { return _scheduledConsumptionTargetsAmount; }
            set
            {
                if (_scheduledConsumptionTargetsAmount == value)
                {
                    return;
                }

                bool wasFilled = IsFilled(FillValue + _scheduledConsumptionTargetsAmount);
                _scheduledConsumptionTargetsAmount = value;
                bool isFilled = IsFilled(FillValue + _scheduledConsumptionTargetsAmount);

                if (!wasFilled && isFilled)
                {
                    Assert.IsNull(_scheduledSelfImpact);
                    _scheduledSelfImpact = ScheduleConsume(_chip.Cell);
                }
                else if (wasFilled && !isFilled)
                {
                    _sourceChipsWeightModifier.RevertAll();
                    _scheduledSelfImpact?.Cancel();
                    _scheduledSelfImpact = null;
                }
            }
        }

        /// arg_1 - fill value (%) [0; 1]
        public event Action<float> FillChanged;

        /// arg_1 - list of cells that will be harvested
        /// arg_2 - car chip consume handle
        public event Action<List<HarvestingCell>, IDeferredInvocationHandle> CellsHarvesting;

        public event Action FullFillPredicted;

        public void SetRequiredChipConsumingDelegate(
                RequiredChipConsumingDelegate requiredChipConsumingDelegate
            )
        {
            _requiredChipConsumingDelegate = requiredChipConsumingDelegate;
        }

        #region IChipConsumptionHandler

        ChipTagSet IChipConsumptionHandler.GetValidChipTags()
        {
            return FillChipTags;
        }

        int IChipConsumptionHandler.GetPriorityOrder()
        {
            return int.MaxValue - FillExecutionOrder;
        }

        void IChipConsumptionHandler.OnConsuming(
                ChipComponent chip,
                IDeferredInvocationHandle consumptionHandle,
                ref bool isModal
            )
        {
            isModal = true;

            ResetScheduledConsumingChips();

            PredictedFillValue += 1;
            var inv = new DeferredInvocation(() => FillValue += 1);
            using (IDeferredInvocationHandle fillChangeHandle = inv.Start())
            {
                if (_requiredChipConsumingDelegate != null)
                {
                    isModal = _requiredChipConsumingDelegate.Invoke(chip, consumptionHandle, fillChangeHandle);
                }
            }
        }

        void IChipConsumptionHandler.OnConsumptionScheduled(ChipComponent chip)
        {
            _scheduledConsumingChips.Add(chip);
            ScheduledConsumptionTargetsAmount = _scheduledConsumingChips.Count;
            _sourceChipsWeightModifier.ApplyAdditionalWeight(chip);
        }

        void IChipConsumptionHandler.OnScheduledConsumptionCancelled(ChipComponent chip, int impactGroupId)
        {
            _scheduledConsumingChips.Remove(chip);
            ScheduledConsumptionTargetsAmount = _scheduledConsumingChips.Count;
        }

        private void ResetScheduledConsumingChips()
        {
            _scheduledConsumingChips.Clear();
            ScheduledConsumptionTargetsAmount = 0;
        }

        #endregion IChipConsumptionHandler

        #region IImpactController

        void IImpactController.OnImpact(
            ChipComponent chip,
            ImpactFlag impactType,
            IDeferredInvocationHandle impactEndHandle,
            ref bool isModal)
        {
            if (!chip.IsConsumed)
            {
                return;
            }

            isModal = true;
            CancelScheduledImpacts(); // reset previously scheduled
            FillValue = PredictedFillValue = ChipsToFill;

            using (var handle = impactEndHandle.Lock())
            {
                HarvestHeadingDirection(handle);
            }
        }

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.CarHarvesting;
        }

        void IImpactController.Cancel()
        {
            // nothing to do
        }

        #endregion IImpactController

        protected void OnValidate()
        {
            if (_chipTags.IsEmpty())
            {
                UnityEngine.Debug.LogAssertion(
                    $"{nameof(_chipTags)} field missing at least one {nameof(ChipTag)}",
                    this);
            }

            if (_chipsToFill < 1)
            {
                _chipsToFill = 1;
                UnityEngine.Debug.LogWarning("Amount of chips to fill a car can't be negative or zero. Set to 1", this);
            }

            #if UNITY_EDITOR
            if (PrefabUtility.GetPrefabType(this) == PrefabType.Prefab)
            {
                _fillExecutionOrder = FillExecutionOrderUnset;
            }
            #endif // UNITY_EDITOR
        }

        protected void Awake()
        {
            _sourceChipsWeightModifier = GetComponent<SourceChipsMoveWeightModifier>();
            _chip = GetComponent<ChipComponent>();
            _chip.CellChanged += OnCellChanged;
            SubscribeToImpactEvents(_chip.Cell);
        }

        protected void OnEnable()
        {
            SubscribeToChipConsumption();
            _chip.AddImpactController(this);
            InitializeHarvestingCells();
        }

        protected void OnDisable()
        {
            _chip.RemoveImpactController(this);
            UnsubscribeFromChipConsumption();
        }

        private void SubscribeToChipConsumption()
        {
            if (FillExecutionOrder == 0)
            {
                _consumptionDispatcher?.Register(this);
            }
        }

        private void UnsubscribeFromChipConsumption()
        {
            ResetScheduledConsumingChips();
            _consumptionDispatcher?.Unregister(this);
        }

        private void InitializeHarvestingCells()
        {
            _cellsToHarvest.Clear();
            Vector2Int cellPos = (_chip.Cell != null) ? _chip.Cell.GridPosition : Vector2Int.zero;
            Vector2Int dirOffset = GetDirectionOffset(_direction);

            do
            {
                cellPos += dirOffset;
                CellComponent cell = _gridSystem.GetCell(cellPos);

                if (cell != null)
                {
                    _cellsToHarvest.Add(cell);
                }
            }
            while (_gridSystem.IsInBounds(cellPos));
        }

        private void OnCellChanged(ChipComponent chip, CellComponent prevCell)
        {
            UnsubscribeFromImpactEvents(prevCell);
            SubscribeToImpactEvents(_chip.Cell);
        }

        private void SubscribeToImpactEvents([CanBeNull] CellComponent cell)
        {
            if (cell != null)
            {
                cell.ImpactScheduled += OnImpactScheduled;
            }
        }

        private void UnsubscribeFromImpactEvents([CanBeNull] CellComponent cell)
        {
            if (cell != null)
            {
                cell.ImpactScheduled -= OnImpactScheduled;
            }
        }

        private void OnImpactScheduled(CellComponent cell, Impact impact)
        {
            Assert.AreEqual(_chip.Cell, cell);
            if (_scheduledImpacts.Count > 0 ||
                _processingImpactScheduling ||
                _chip.IsConsumed ||
                !_chip.WillBeConsumed())
            {
                return;
            }

            impact.Cancelling += OnScheduledImpactCancelling;

            _processingImpactScheduling = true;
            _cellsToHarvest.ForEach(c => _scheduledImpacts.Add(ScheduleConsume(c)));
            _processingImpactScheduling = false;
        }

        private void OnScheduledImpactCancelling(Impact impact)
        {
            impact.Cancelling -= OnScheduledImpactCancelling;

            if (_processingImpactScheduling)
            {
                UnityEngine.Debug.LogError($"{nameof(CarChipComponent)}: Cancelling while processing impact scheduling", this);
            }

            _processingImpactScheduling = true;
            CancelScheduledImpacts();
            _processingImpactScheduling = false;
        }

        private void CancelScheduledImpacts()
        {
            _scheduledImpacts.ForEach(impact => impact.Cancel());
            _scheduledImpacts.Clear();
        }

        private Vector2Int GetDirectionOffset(Direction dir)
        {
            switch (dir)
            {
                case Direction.Up:    return Vector2Int.up;
                case Direction.Down:  return Vector2Int.down;
                case Direction.Left:  return Vector2Int.left;
                case Direction.Right: return Vector2Int.right;
                default:
                    throw new InvalidEnumArgumentException(nameof(dir), (int)dir, typeof(Direction));
            }
        }

        private Impact ScheduleConsume([NotNull] CellComponent cell)
        {
            int impactValue = cell.CalculateClearingImpactValue();
            return _impactSystem.ScheduleImpact(cell, -impactValue, ImpactFlag.Car);
        }

        private bool IsFilled(int currentValue)
        {
            return currentValue >= ChipsToFill;
        }

        private void HarvestHeadingDirection(IDeferredInvocationHandle impactEndHandle)
        {
            var handles = new List<IDeferredInvocationHandle>(_cellsToHarvest.Count);
            var cells = new List<HarvestingCell>(_cellsToHarvest.Count);
            foreach (CellComponent cell in _cellsToHarvest)
            {
                var di = new DeferredInvocation(() => ScheduleConsume(cell).Perform());
                IDeferredInvocationHandle handle = di.Start();
                handles.Add(handle);
                cells.Add(new HarvestingCell(cell, handle));
            }

            CellsHarvesting?.Invoke(cells, impactEndHandle);

            handles.ForEach(h => h.Unlock());
        }

        #region Inner types

        public enum Direction
        {
            Up,
            Down,
            Left,
            Right
        }

        public delegate bool RequiredChipConsumingDelegate(
                [NotNull] ChipComponent chip,
                [NotNull] IDeferredInvocationHandle consumptionHandle,
                [NotNull] IDeferredInvocationHandle commitConsumeHandle
            );

        public struct HarvestingCell
        {
            public CellComponent Cell { get; }
            public IDeferredInvocationHandle ConsumeHandle { get; }

            public HarvestingCell([NotNull] CellComponent cell, [NotNull] IDeferredInvocationHandle handle)
            {
                Cell = cell;
                ConsumeHandle = handle;
            }
        }

        #endregion Inner types
    }
}