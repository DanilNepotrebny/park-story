﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Movement;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.SpecialChips.Car
{
    public class ChipFlightConsumptionAnimator : MonoBehaviour
    {
        [SerializeField, SortingLayer] private int _sortingLayer;
        [SerializeField] private string _chipStateTrigger = "OnConsumeFlight";
        [SerializeField] private string _consumeFlightState = "ConsumeFlight";

        [Inject] private ChipConsumptionFactory _chipConsumptionFactory;
        [Inject] private Instantiator _instantiator;

        public bool OnChipConsuming(
                [NotNull] ChipComponent chip,
                [NotNull] IDeferredInvocationHandle consumptionHandle,
                [NotNull] IDeferredInvocationHandle fillChangeHandle
            )
        {
            ChipTag chipTag = chip.Tags.GetFirst(); // TODO: Does it really needed
            Assert.IsNotNull(chipTag);
            GameObject projectile = _chipConsumptionFactory.Create(chipTag, _instantiator);

            if (projectile == null)
            {
                return false;
            }

            ChipPresenter chipView = chip.DetachView();
            Vector3 startPos = chipView.transform.position;
            chipView.transform.SetParent(projectile.transform);
            chipView.SortingOptions.PushSortingLayer(_sortingLayer);

            var sequence = projectile.GetComponent<MovementSequence>();
            IDeferredInvocationHandle[] fillChangeHandlePacked = { fillChangeHandle.Lock() };
            IDeferredInvocationHandle[] consumptionHandlePacked = { consumptionHandle.Lock() };

            Action beginFlightAction = () =>
            {
                consumptionHandlePacked[0].Unlock();
                consumptionHandlePacked[0] = null;

                chipView.transform.localPosition = Vector3.zero;
                sequence.Move(startPos, transform.position, true);
                sequence.Updated += (mov, ratio) => chipView.SetAnimationNormalizedTime(ratio);
                sequence.Finished += seq => fillChangeHandlePacked[0].Unlock();
            };

            if (_chipStateTrigger != string.Empty)
            {
                chipView.AddAnimationStateEnterHandler(_consumeFlightState, beginFlightAction);
                chipView.SetTrigger(_chipStateTrigger);
            }
            else
            {
                beginFlightAction.Invoke();
            }

            return true;
        }
    }
}