﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Spine;
using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using AnimationState = Spine.AnimationState;

namespace DreamTeam.Match3.SpecialChips.Car
{
    [RequireComponent(typeof(ChipPresenter), typeof(ChipFlightConsumptionAnimator))]
    public class CarChipPresenter : MonoBehaviour
    {
        [SerializeField] private float _fillDuration;

        [Header("Animation")] [SerializeField] private SkeletonAnimation _animation;
        [SerializeField] private SkeletonIdleAnimationHandler _idleAnimationHandler;
        [SerializeField, SpineAnimation] private string _harvestAnimation;
        [SerializeField, SpineBone] private string _harvestBoneName;
        [SerializeField, SortingLayer] private int _harvestAnimationSortingLayer;

        [Header("Fill bar")] [SerializeField, SpineAnimation]
        private string _barFilledAnimation;

        [SerializeField, SpineAnimation] private string _barFullyFilledAnimation;
        [SerializeField, SpineBone] private string _barValueBoneName;
        [SerializeField, SpineBone] private string _barTopBoneName;
        [SerializeField, SpineBone] private string _barBottomBoneName;

        [Header("Particles")] [SerializeField] private ParticleSystem _unsuccessfulFillParticles;
        [SerializeField] private ParticleSystem _harvestingStartParticles;

        [Header("Sounds")] [SerializeField, RequiredField] private ChipAudioController _audioController;
        [SerializeField, RequiredField] private string _harvestAudioEvent;

        private ChipPresenter _presenter;
        private Bone _harvestBone;
        private Bone _barValueBone;
        private Bone _barTopBone;
        private Bone _barBottomBone;
        private LTDescr _fillAnimation;
        private List<CarChipComponent.HarvestingCell> _harvestingCells = new List<CarChipComponent.HarvestingCell>();
        private IDeferredInvocationHandle _endHandle;

        public CarChipComponent CarComponent { get; private set; }

        protected void Awake()
        {
            _presenter = GetComponent<ChipPresenter>();
            CarComponent = _presenter.ChipComponent.GetComponent<CarChipComponent>();

            var flightAnimator = GetComponent<ChipFlightConsumptionAnimator>();
            CarComponent.SetRequiredChipConsumingDelegate(flightAnimator.OnChipConsuming);
        }

        protected void Start()
        {
            _harvestBone = _animation.Skeleton.FindBone(_harvestBoneName);
            _barValueBone = _animation.Skeleton.FindBone(_barValueBoneName);
            _barTopBone = _animation.Skeleton.FindBone(_barTopBoneName);
            _barBottomBone = _animation.Skeleton.FindBone(_barBottomBoneName);
            CarComponent.FillChanged += OnFillChanged;
            CarComponent.CellsHarvesting += OnCarBeginToHarvestCells;
        }

        protected void Update()
        {
            if (_fillAnimation == null)
            {
                SetBarValue(CarComponent.FillPercent);
            }
        }

        #if UNITY_EDITOR
        protected void OnDrawGizmos()
        {
            if (!Application.isPlaying)
            {
                return;
            }

            Gizmos.color = Color.red;
            _barTopBone.UpdateWorldTransform();
            Vector2 topWorldPos = _barTopBone.GetWorldPosition(transform);
            Gizmos.DrawSphere(new Vector3(topWorldPos.x, topWorldPos.y, 0), 0.1f);

            _barBottomBone.UpdateWorldTransform();
            Vector2 bottomWorldPos = _barBottomBone.GetWorldPosition(transform);
            Gizmos.DrawSphere(new Vector3(bottomWorldPos.x, bottomWorldPos.y, 0), 0.1f);

            Gizmos.color = Color.yellow;
            _barValueBone.UpdateWorldTransform();
            Vector2 valueWorldPos = _barValueBone.GetWorldPosition(transform);
            Gizmos.DrawSphere(new Vector3(valueWorldPos.x, valueWorldPos.y, 0), 0.05f);
        }
        #endif // UNITY_EDITOR

        protected void OnDestroy()
        {
            TryCancelFillAnimation();
        }

        private void OnCarBeginToHarvestCells(
            List<CarChipComponent.HarvestingCell> harvestingCells,
            IDeferredInvocationHandle carChipConsumeHandle)
        {
            Assert.IsNull(_endHandle);
            _endHandle = carChipConsumeHandle.Lock();

            _harvestingCells.Clear();
            foreach (CarChipComponent.HarvestingCell hc in harvestingCells)
            {
                _harvestingCells.Add(new CarChipComponent.HarvestingCell(hc.Cell, hc.ConsumeHandle.Lock()));
            }

            StartCoroutine(PlayHarvestAnimation());
        }

        private IEnumerator PlayHarvestAnimation()
        {
            yield return new WaitUntil(() => _fillAnimation == null);

            _idleAnimationHandler.StopAnimation();
            _presenter.SortingOptions.SortingLayerId = _harvestAnimationSortingLayer;

            _harvestingStartParticles.Play(true);

            _audioController.PlaySound(_harvestAudioEvent);
            AnimationState state = _animation.AnimationState;
            TrackEntry trackEntry = state.SetAnimation(0, _harvestAnimation, false);
            trackEntry.Complete += entry =>
            {
                Assert.IsNotNull(_endHandle);
                _endHandle.Unlock();
                _endHandle = null;
            };

            foreach (CarChipComponent.HarvestingCell hc in _harvestingCells)
            {
                yield return new WaitUntil(() => IsRanOver(hc.Cell.transform.position));

                hc.ConsumeHandle.Unlock();
            }
        }

        private void OnFillChanged(float fillPercent)
        {
            _idleAnimationHandler.StopAnimation();

            string fillAnimName;
            if (Mathf.Approximately(fillPercent, 1))
            {
                fillAnimName = _barFullyFilledAnimation;
            }
            else
            {
                fillAnimName = _barFilledAnimation;
                _unsuccessfulFillParticles.Play(true);
            }

            _animation.AnimationState.SetAnimation(0, fillAnimName, true);

            TryCancelFillAnimation();

            float currentFillPercent = CalculateBarFillPercent();
            _fillAnimation = LeanTween.value(currentFillPercent, fillPercent, _fillDuration);
            _fillAnimation.setOnUpdate(SetBarValue);
            _fillAnimation.setOnComplete(OnFillAnimationComplete);
        }

        private void TryCancelFillAnimation()
        {
            if (_fillAnimation != null)
            {
                LeanTween.cancel(_fillAnimation.id);
                _fillAnimation = null;
            }
        }

        private void OnFillAnimationComplete()
        {
            _fillAnimation = null;
            _idleAnimationHandler.PlayAnimation(false);
        }

        private void SetBarValue(float value)
        {
            Vector2 topWorldPos = _barTopBone.GetSkeletonSpacePosition();
            Vector2 bottomWorldPos = _barBottomBone.GetSkeletonSpacePosition();

            Vector2 barOffset = topWorldPos - bottomWorldPos;
            float barSize = barOffset.magnitude;
            Vector2 dir = barOffset.normalized;
            Vector2 valueWorldPos = bottomWorldPos + dir * barSize * value;
            _barValueBone.SetPositionSkeletonSpace(valueWorldPos);
            _animation.Skeleton.UpdateWorldTransform(); // applies bones transformation in right order
        }

        private float CalculateBarFillPercent()
        {
            _barTopBone.UpdateWorldTransform();
            Vector2 topWorldPos = _barTopBone.GetWorldPosition(transform);

            _barBottomBone.UpdateWorldTransform();
            Vector2 bottomWorldPos = _barBottomBone.GetWorldPosition(transform);

            _barValueBone.UpdateWorldTransform();
            Vector2 valueWorldPos = _barValueBone.GetWorldPosition(transform);

            float barSize = Vector2.Distance(bottomWorldPos, topWorldPos);
            float valueSize = Vector2.Distance(bottomWorldPos, valueWorldPos);
            float fillPercent = valueSize / barSize;
            return fillPercent;
        }

        private bool IsRanOver(Vector3 point)
        {
            _harvestBone.UpdateWorldTransform();
            Vector3 bonePos = _harvestBone.GetWorldPosition(transform);

            switch (CarComponent.HeadingDirection)
            {
                case CarChipComponent.Direction.Up: return bonePos.y > point.y;
                case CarChipComponent.Direction.Down: return bonePos.y < point.y;
                case CarChipComponent.Direction.Left: return bonePos.x < point.x;
                case CarChipComponent.Direction.Right: return bonePos.x > point.x;
                default:
                    throw new InvalidEnumArgumentException(
                        nameof(CarComponent.HeadingDirection),
                        (int)CarComponent.HeadingDirection,
                        typeof(Direction));
            }
        }
    }
}