﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System.Linq;
using DreamTeam.Match3.ChipTags;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3.SpecialChips.Mantle
{
    public partial class MantleComponent : MantleComponent.IEditorApi
    {
        #region IEditorApi

        public bool Add(MantleStubChipComponent.IEditorApi stub)
        {
            if ((this as IEditorApi).Contains(stub))
            {
                return false;
            }

            _stubChips.Add((MantleStubChipComponent)stub);
            return true;
        }

        public bool Remove(MantleStubChipComponent.IEditorApi stub)
        {
            return _stubChips.Remove((MantleStubChipComponent)stub);
        }

        public bool Contains(MantleStubChipComponent.IEditorApi stub)
        {
            return _stubChips.Contains((MantleStubChipComponent)stub);
        }

        public bool IsEmpty()
        {
            return _stubChips.Count == 0;
        }

        public int CalculateStubsCountToBeValidShape([NotNull] GridSystem grid)
        {
            int missingChips = 0;
            var rect = CalculateGridArea(grid);
            foreach (Vector2Int rectInnPos in rect.allPositionsWithin)
            {
                bool isContains = _stubChips.Any(
                        stub =>
                        {
                            CellComponent cell = grid.GetCell(grid.WorldToGrid(stub.gameObject.transform.position));
                            Vector2Int pos = grid.GetGridPosition(cell);
                            return rectInnPos == pos;
                        }
                    );
                missingChips += isContains ? 0 : 1;
            }

            if (missingChips == 0 && _stubChips.Count < 4)
            {
                missingChips = 4 - _stubChips.Count;
            }

            return missingChips;
        }

        public void SetChipsConsumeAmount(int amount)
        {
            Assert.IsTrue(amount > 0);
            _amountToConsume = amount;
        }

        public void SetChipToConsume([NotNull] ChipTag chipTag)
        {
            _chipToConsume = chipTag;
        }

        public void SetConsumeDirection(Direction dir)
        {
            _consumeDirection = dir;
        }

        public void OnSelected()
        {
            Presenter.BezelTilemap.color = Color.black;
        }

        public void OnDeselected()
        {
            if (Presenter.BezelTilemap != null)
            {
                Presenter.BezelTilemap.color = Color.white;
            }
        }

        #endregion IEditorApi

        #region Inner types

        public interface IEditorApi
        {
            GameObject gameObject { get; }
            MantlePresenter Presenter { get; }

            bool Add([NotNull] MantleStubChipComponent.IEditorApi stub);
            bool Remove([NotNull] MantleStubChipComponent.IEditorApi stub);
            bool Contains([NotNull] MantleStubChipComponent.IEditorApi stub);
            bool IsEmpty();
            int CalculateStubsCountToBeValidShape([NotNull] GridSystem gridSystem);
            void SetChipsConsumeAmount(int amount);
            void SetChipToConsume([NotNull] ChipTag chipTag);
            void SetConsumeDirection(Direction dir);

            void OnSelected();
            void OnDeselected();
        }

        #endregion Inner types
    }
}

#endif // UNITY_EDITOR
