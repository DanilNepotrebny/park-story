﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DreamTeam.Audio;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.Hammer;
using DreamTeam.Match3.SpecialChips.Car;
using DreamTeam.Match3.UI;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.RendererSorting;
using DreamTeam.Utils.Spine;
using ModestTree;
using Spine;
using Spine.Unity;
using Spine.Unity.Modules.AttachmentTools;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Tilemaps;
using Zenject;

namespace DreamTeam.Match3.SpecialChips.Mantle
{
    [RequireComponent(typeof(ChipFlightConsumptionAnimator))]
    public partial class MantlePresenter : MonoBehaviour,
        ISortingOptionsProvider, IHammerModeShadingReceiver, IFieldDestabilizer
    {
        [SerializeField] private MantleComponent _mantle;
        [SerializeField] private GameObject _view;
        [FormerlySerializedAs("_animation"),SerializeField] private SkeletonAnimation _skeleton;
        [SerializeField] private SkeletonIdleAnimationHandler _idleAnimationHandler;
        [SerializeField] private SimpleAnimation _viewAppearanceAnimation;
        [SerializeField, HideInInspector] private Tilemap _bezelTilemap;

        [Header("Consuming chip icon")]
        [SerializeField, SpineAnimation] private string _chipConsumedAnimationName;
        [SerializeField, SpineAnimation] private string _mantleConsumeAnimation;
        [SerializeField, SpineSlot] private string _chipIconSlotName;
        [SerializeField] private Material _chipIconMaterial;

        [Header("Counter")]
        [SerializeField] private TextMeshPro _counterTextField;
        [SerializeField] private string _textFormat;

        [Header("Roll")]
        [SerializeField] private GameObject _rollMask;
        [SerializeField] private GameObject _rollObject;
        [SerializeField] private float _rollAnimationDuration = 1f;
        [SerializeField] private float _rollTwistAnimationDuration = 5f;
        [SerializeField] private AudioSystemPlayer _rollTwistingSound;

        [Inject] private GridSystem _gridSystem;
        [Inject] private ChipTagIconSettings _chipIconSettings;
        [Inject] private GridAnimator _gridAnimator;
        [Inject] private Instantiator _instantiator;
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;

        private readonly List<ShadingReceiverData> _shadingReceiverData = new List<ShadingReceiverData>();

        public SortingOptions SortingOptions { get; private set; }

        private Vector2 _rollSize
        {
            get { return _rollObject.GetComponent<SpriteRenderer>().size; }
            set { _rollObject.GetComponent<SpriteRenderer>().size = value; }
        }

        private bool IsAnimationOngoing
        {
            get { return _isAnimationOngoingBF; }
            set
            {
                if (_isAnimationOngoingBF == value)
                {
                    return;
                }

                _isAnimationOngoingBF = value;

                if (_isAnimationOngoingBF)
                {
                    Destabilized?.Invoke();
                }
                else
                {
                    Stabilized?.Invoke();
                }
            }
        }
        private bool _isAnimationOngoingBF;

        private ChipFlightConsumptionAnimator _chipFlightAnimator => GetComponent<ChipFlightConsumptionAnimator>();

        private RectInt _mantleArea => _mantle.CalculateGridArea(_gridSystem);

        public bool IsStable => !IsAnimationOngoing;

        public event Action Stabilized;

        public event Action Destabilized;

        protected void Start()
        {
            _fieldStabilizationSystem.AddDestabilizer(this);
            _mantle.RequiredChipsCountChanged += OnRequiredChipsCountChanged;
            _mantle.SetRequiredChipConsumingHandler(_chipFlightAnimator.OnChipConsuming);
            _mantle.Consuming +=
                (pairs, destroyHandle) =>
                {
                    StartCoroutine(PlayConsumeAnimation(pairs, destroyHandle));
                };

            var provider = _bezelTilemap.GetComponent<ISortingOptionsProvider>();
            Assert.IsNotNull(provider, $"Mantle's tilemap has no component realizing {nameof(ISortingOptionsProvider)}");

            SortingOptions = _instantiator.Instantiate<SortingOptions>(
                    provider.SortingOptions.SortingLayerId,
                    provider.SortingOptions.SortingOrder
                );
            SortingOptions.SetParent(provider.SortingOptions, 0);

            _gridAnimator.AnimationInCellStarting += OnGridAnimationInCellStarting;
            _viewAppearanceAnimation.AnimationFinished += OnViewAnimationFinished;

            SetupShadingData();
            SetupChipIcon();
            UpdatePosition();
            UpdateCounterText();

            SetupRoll();
        }

        private void SetupShadingData()
        {
            _shadingReceiverData.Clear();

            Renderer[] renderers = GetComponentsInChildren<Renderer>(true);
            renderers.ForEach(r => _shadingReceiverData.Add(new ShadingReceiverData(r, new MaterialPropertyBlock())));
        }

        protected void OnDestroy()
        {
            _fieldStabilizationSystem.RemoveDestabilizer(this);
            _gridAnimator.AnimationInCellStarting -= OnGridAnimationInCellStarting;
            _viewAppearanceAnimation.AnimationFinished -= OnViewAnimationFinished;

            SortingOptions.Dispose();
            SortingOptions = null;
        }

        private void OnGridAnimationInCellStarting(SimpleAnimationType animationType, Vector2Int cellGridPosition)
        {
            float distance = Vector3.Distance(_gridSystem.GridToWorld(cellGridPosition), _view.transform.position);
            float distanceThreshold =
                Mathf.Sqrt(Mathf.Pow(_gridSystem.CellWidth, 2) + Mathf.Pow(_gridSystem.CellHeight, 2));

            if (distance < distanceThreshold && !_viewAppearanceAnimation.IsPlaying)
            {
                if (animationType == SimpleAnimationType.Show)
                {
                    _view.SetActive(true);
                }

                _viewAppearanceAnimation.Play(animationType);
            }
        }

        private void OnViewAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            if (animationType == SimpleAnimationType.Hide)
            {
                _view.SetActive(false);
            }
        }

        private IEnumerator PlayConsumeAnimation(
            List<KeyValuePair<Vector3, IDeferredInvocationHandle>> stubGroupHandles,
            IDeferredInvocationHandle mantleDestroyHandle)
        {
            IsAnimationOngoing = true;
            IDeferredInvocationHandle destroyHandle = mantleDestroyHandle.Lock();
            List<KeyValuePair<Vector3, IDeferredInvocationHandle>> unlockHandles = stubGroupHandles
                .Select(pair => new KeyValuePair<Vector3, IDeferredInvocationHandle>(pair.Key, pair.Value.Lock()))
                .ToList();

            bool isMantleConsumeAnimationComplete = false;
            PlayAnimation(_mantleConsumeAnimation, () => isMantleConsumeAnimationComplete = true);

            yield return new WaitUntil(() => isMantleConsumeAnimationComplete);

            yield return PlayRollAppearAnimation(_rollAnimationDuration);

            Coroutine rollTwistingCoroutine = StartCoroutine(PlayRollTwistingAnimation(_rollTwistAnimationDuration));

            var dirOffset = new Vector3(_mantle.ConsumeDirection.GetOffsetX(), _mantle.ConsumeDirection.GetOffsetY());
            foreach (var pair in unlockHandles)
            {
                Vector3 groupPos = pair.Key;
                IDeferredInvocationHandle handle = pair.Value;

                handle.Unlock();

                yield return new WaitUntil(
                        () =>
                        {
                            Vector3 rollPos = Vector3.Scale(_rollObject.transform.position, dirOffset);
                            return groupPos.x < rollPos.x || groupPos.y < rollPos.y;
                        }
                    );
            }

            yield return rollTwistingCoroutine;

            yield return PlayRollDisappearAnimation(_rollAnimationDuration);

            IsAnimationOngoing = false;
            destroyHandle.Unlock();

            _bezelTilemap.gameObject.Dispose();
        }

        private void SetupRoll()
        {
            Vector3 posOffset = CalculateRollPositionOffset();

            _rollObject.SetActive(false);
            _rollObject.transform.position = transform.position + posOffset;
            _rollObject.transform.rotation = CalculateRollRotation();
            SetupRollSize();

            // setup mask
            Vector3 posOffsetNoSize = CalculateRollPositionOffset(false);

            _rollMask.SetActive(false);
            _rollMask.transform.position = transform.position + posOffsetNoSize;
            _rollMask.transform.localScale = new Vector3(
                    _mantle.IsConsumeDirectionVertical ? _mantleArea.size.x : 0,
                    _mantle.IsConsumeDirectionVertical ? 0 : _mantleArea.size.y
                );
        }

        private IEnumerator PlayRollAppearAnimation(float timeInSec)
        {
            _rollObject.SetActive(true);
            _rollObject.transform.localScale = Vector3.up;

            LTDescr ltd = LeanTween.scale(_rollObject, Vector3.one, timeInSec);

            yield return new WaitUntil(() => !LeanTween.isTweening(ltd.id));
        }

        private IEnumerator PlayRollDisappearAnimation(float timeInSec)
        {
            LTDescr ltd = LeanTween.scale(_rollObject, Vector3.up, timeInSec);

            yield return new WaitUntil(() => !LeanTween.isTweening(ltd.id));

            _rollObject.SetActive(false);
        }

        private IEnumerator PlayRollTwistingAnimation(float rollTwistAnimationDuration)
        {
            _rollTwistingSound?.Play();

            Vector3 offset = CalculateRollPositionOffset();
            Vector3 rollTargetPos = transform.position - offset;

            LTDescr rollDescriptor = LeanTween.move(_rollObject, rollTargetPos, rollTwistAnimationDuration);

            _rollMask.SetActive(true);
            var targetScale = new Vector3(_mantleArea.size.x, _mantleArea.size.y);

            LTDescr maskPosDescriptor = LeanTween.move(_rollMask, transform.position, rollTwistAnimationDuration);
            LTDescr maskScaleDescriptor = LeanTween.scale(_rollMask, targetScale, rollTwistAnimationDuration);

            yield return new WaitUntil(
                    () =>
                        !LeanTween.isTweening(rollDescriptor.id) &&
                        !LeanTween.isTweening(maskPosDescriptor.id) &&
                        !LeanTween.isTweening(maskScaleDescriptor.id)
                );
        }

        private Vector3 CalculateRollPositionOffset(bool applyRollSize = true)
        {
            Vector2Int dirOffsetInt = _mantle.ConsumeDirection.GetOffset();
            var dirOffset = new Vector2(dirOffsetInt.x, dirOffsetInt.y);

            var offset = new Vector3(
                    _mantleArea.size.x * -dirOffset.x * _gridSystem.CellWidth * 0.5f,
                    _mantleArea.size.y * -dirOffset.y * _gridSystem.CellHeight * 0.5f,
                    0f
                );

            Vector3 sizeOffset = Vector3.zero;
            if (applyRollSize)
            {
                sizeOffset = dirOffset;
                sizeOffset.x *= _mantle.IsConsumeDirectionVertical ? _rollSize.y : _rollSize.x;
                sizeOffset.y *= _mantle.IsConsumeDirectionVertical ? _rollSize.x : _rollSize.y;
            }

            return offset + 0.5f * sizeOffset;
        }

        private Quaternion CalculateRollRotation()
        {
            switch (_mantle.ConsumeDirection)
            {
                case Direction.Down:
                    return Quaternion.Euler(0, 0, 90);

                case Direction.Left:
                    return Quaternion.identity; // default state for curtain asset

                case Direction.Right:
                    return Quaternion.Euler(0, 0, 180);

                case Direction.Up:
                    return Quaternion.Euler(0, 0, -90);

                default:
                    throw new InvalidEnumArgumentException();
            }
        }

        private void SetupRollSize()
        {
            _rollSize = new Vector2(
                    _rollSize.x,
                    _mantle.IsConsumeDirectionVertical ?
                        _mantleArea.size.x * _gridSystem.CellWidth :
                        _mantleArea.size.y * _gridSystem.CellHeight
                );
        }

        private void OnRequiredChipsCountChanged()
        {
            UpdateCounterText();

            PlayAnimation(
                _chipConsumedAnimationName,
                () =>
                {
                    if (!_mantle.IsUnlocked)
                    {
                        _idleAnimationHandler.PlayAnimation(false);
                    }
                });
        }

        private void PlayAnimation(string animationName, Action onCompleteCallback = null)
        {
            _idleAnimationHandler.StopAnimation();
            TrackEntry entry = _skeleton.state.SetAnimation(0, animationName, false);
            entry.Complete += trackEntry => onCompleteCallback?.Invoke();
        }

        private void UpdateCounterText()
        {
            if (_mantle.IsUnlocked)
            {
                _counterTextField.gameObject.SetActive(false);
                return;
            }

            _counterTextField.text = string.Format(_textFormat, _mantle.ChipsToUnlock);
        }

        private void UpdatePosition()
        {
            var center = new Vector2(
                    _mantleArea.position.x + (_mantleArea.width - 1) * 0.5f,
                    _mantleArea.position.y + (_mantleArea.height - 1) * 0.5f
                );
            _mantle.transform.position = _gridSystem.GridToWorldF(center);
        }

        private void SetupChipIcon()
        {
            Sprite icon = _chipIconSettings.GetIcon(_mantle.ChipToConsume);
            RegionAttachment regionAttachment = icon.ToRegionAttachmentPMAClone(_chipIconMaterial);

            float scale = _skeleton.skeletonDataAsset.scale * 100f;
            regionAttachment.SetScale(scale, scale);
            regionAttachment.UpdateOffset(); // applying SetScale(..)

            Slot slot = _skeleton.Skeleton.FindSlot(_chipIconSlotName);
            slot.Attachment = regionAttachment;
        }

        List<ShadingReceiverData> IHammerModeShadingReceiver.GetShadingData()
        {
            return _shadingReceiverData;
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }
    }
}
