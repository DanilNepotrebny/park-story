﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using JetBrains.Annotations;

#if UNITY_EDITOR

namespace DreamTeam.Match3.SpecialChips.Mantle
{
    public partial class MantleStubChipComponent : MantleStubChipComponent.IEditorApi
    {
        public void SetMantle([NotNull] MantleComponent mantle)
        {
            _mantle = mantle;
        }

        #region Inner types

        public interface IEditorApi
        {
            ChipComponent Chip { get; }
            void SetMantle([NotNull] MantleComponent mantle);
        }

        #endregion Inner types
    }
}

#endif // UNITY_EDITOR
