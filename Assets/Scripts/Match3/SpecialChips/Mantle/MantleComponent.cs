﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.MoveWeight;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.SpecialChips.Mantle
{
    [RequireComponent(typeof(SourceChipsMoveWeightModifier))]
    public partial class MantleComponent : MonoBehaviour, IChipConsumptionHandler
    {
        [SerializeField] private MantlePresenter _presenter;
        [SerializeField] private ChipTag _chipToConsume;
        [SerializeField, Min(1)] private int _amountToConsume;
        [SerializeField] private Direction _consumeDirection = Direction.Down;

        [SerializeField, HideInInspector]
        private List<MantleStubChipComponent> _stubChips = new List<MantleStubChipComponent>();

        [Inject] private ImpactSystem _impactSystem;
        [Inject] private ChipConsumptionDispatcher _consumptionDispatcher;

        private readonly ChipTagSet _validTagsSet = new ChipTagSet(false);
        private int _scheduledConsumingChipsCount;
        private int _chipsToUnlock = int.MaxValue;
        private int _predictedChipsToUnlock = int.MaxValue;
        private readonly List<Impact> _scheduledSelfImpacts = new List<Impact>();

        private SourceChipsMoveWeightModifier _sourceChipsWeightModifier =>
            GetComponent<SourceChipsMoveWeightModifier>();

        private RequiredChipConsumingDelegate _requiredChipConsumingDelegate;

        public MantlePresenter Presenter => _presenter;

        public ChipTag ChipToConsume => _chipToConsume;

        public Direction ConsumeDirection => _consumeDirection;

        public bool IsConsumeDirectionVertical =>
            ConsumeDirection == Direction.Down ||
            ConsumeDirection == Direction.Up;

        public int ChipsConsumeAmount => _amountToConsume;

        public bool IsUnlocked => IsUnlockedInternal(ChipsToUnlock);

        public int ChipsToUnlock
        {
            get { return _chipsToUnlock; }
            private set
            {
                int clampedValue = Mathf.Clamp(value, 0, _amountToConsume);
                if (_chipsToUnlock == clampedValue || IsUnlockedInternal(_chipsToUnlock))
                {
                    return;
                }

                _chipsToUnlock = clampedValue;
                RequiredChipsCountChanged?.Invoke();

                if (IsUnlockedInternal(_chipsToUnlock))
                {
                    Consume();
                }
            }
        }

        public int PredictedChipsToUnlock
        {
            get { return _predictedChipsToUnlock; }
            set
            {
                int clampedValue = Mathf.Clamp(value, 0, _amountToConsume);
                if (_predictedChipsToUnlock == clampedValue || IsUnlockedInternal(_predictedChipsToUnlock))
                {
                    return;
                }

                _predictedChipsToUnlock = clampedValue;

                if (IsUnlockedInternal(_predictedChipsToUnlock))
                {
                    _consumptionDispatcher?.Unregister(this);
                }
            }
        }

        private int ScheduledConsumingChipsCount
        {
            get { return _scheduledConsumingChipsCount; }
            set
            {
                if (_scheduledConsumingChipsCount == value)
                {
                    return;
                }

                bool wasUnlocked = IsUnlockedInternal(_chipsToUnlock - _scheduledConsumingChipsCount);
                _scheduledConsumingChipsCount = Mathf.Clamp(value, 0, _amountToConsume);
                bool isUnlocked = IsUnlockedInternal(_chipsToUnlock - _scheduledConsumingChipsCount);

                if (!wasUnlocked && isUnlocked)
                {
                    Assert.IsTrue(_scheduledSelfImpacts.Count == 0);
                    foreach (MantleStubChipComponent stub in _stubChips)
                    {
                        _scheduledSelfImpacts.Add(ScheduleConsume(stub.Chip.Cell));
                    }
                }
                else if (wasUnlocked && !isUnlocked)
                {
                    _sourceChipsWeightModifier.RevertAll();
                    Assert.IsTrue(_scheduledSelfImpacts.Count > 0);
                    _scheduledSelfImpacts.ForEach(impact => impact.Cancel());
                    _scheduledSelfImpacts.Clear();
                }
            }
        }

        public event Action RequiredChipsCountChanged;
        public event ConsumingDelegate Consuming;

        public RectInt CalculateGridArea([NotNull] GridSystem grid)
        {
            int minX = int.MaxValue,
                minY = int.MaxValue,
                maxX = -1,
                maxY = -1;

            foreach (MantleStubChipComponent stub in _stubChips)
            {
                CellComponent cell = grid.GetCell(grid.WorldToGrid(stub.gameObject.transform.position));
                Vector2Int pos = grid.GetGridPosition(cell);
                minX = Mathf.Min(pos.x, minX);
                minY = Mathf.Min(pos.y, minY);
                maxX = Mathf.Max(pos.x, maxX);
                maxY = Mathf.Max(pos.y, maxY);
            }

            return new RectInt(minX, minY, maxX - minX + 1, maxY - minY + 1);
        }

        public void SetRequiredChipConsumingHandler(
                RequiredChipConsumingDelegate requiredChipConsumingHandler
            )
        {
            _requiredChipConsumingDelegate = requiredChipConsumingHandler;
        }

        protected void Awake()
        {
            _validTagsSet.Add(_chipToConsume);
            _chipsToUnlock = _amountToConsume;
        }

        protected void OnEnable()
        {
            _consumptionDispatcher?.Register(this);
        }

        protected void OnDisable()
        {
            _consumptionDispatcher?.Unregister(this);
        }

        protected void OnValidate()
        {
            switch (_consumeDirection)
            {
                case Direction.Up:
                case Direction.Down:
                case Direction.Left:
                case Direction.Right:
                    break; // proper one

                default:
                    UnityEngine.Debug.LogError(
                            $"{nameof(_consumeDirection)} property has wrong ('{_consumeDirection}') value"
                        );
                    break;
            }
        }

        private void Consume()
        {
            List<List<MantleStubChipComponent>> stubGroups = BuildConsumingList();
            var stubHandles = new List<KeyValuePair<Vector3, IDeferredInvocationHandle>>(stubGroups.Count);
            foreach (List<MantleStubChipComponent> stubGroup in stubGroups)
            {
                var stubImpactList = new List<Impact>(stubGroup.Count);
                stubGroup.ForEach(stub => stubImpactList.Add(ScheduleConsume(stub.Chip.Cell)));

                var stubGroupDI = new DeferredInvocation(
                        () => stubImpactList.ForEach(impact => impact.Perform())
                    );

                Vector3 aStubPos = stubGroup.First().transform.position;
                var dirOffset = new Vector3(ConsumeDirection.GetOffsetX(), ConsumeDirection.GetOffsetY());
                var pair = new KeyValuePair<Vector3, IDeferredInvocationHandle>(
                        Vector3.Scale(aStubPos, dirOffset),
                        stubGroupDI.Start()
                    );

                stubHandles.Add(pair);
            }

            var destroyDI = new DeferredInvocation(() => gameObject.Dispose());
            using (IDeferredInvocationHandle destroyHandle = destroyDI.Start())
            {
                Consuming?.Invoke(stubHandles, destroyHandle);
                stubHandles.ForEach(pair => pair.Value.Unlock());
            }
        }

        private List<List<MantleStubChipComponent>> BuildConsumingList()
        {
            Vector2Int dirOffset = _consumeDirection.GetOffset();
            _stubChips.Sort(
                    (first, second) =>
                    {
                        Vector2Int firstModified = first.Chip.Cell.GridPosition * dirOffset;
                        Vector2Int secondModified = second.Chip.Cell.GridPosition * dirOffset;

                        int sign = IsConsumeDirectionVertical ? 1 : -1;

                        return sign * (int)(secondModified.magnitude - firstModified.magnitude);
                    }
                );

            if (dirOffset.x + dirOffset.y > 0 && IsConsumeDirectionVertical)
            {
                _stubChips.Reverse();
            }
            else if (dirOffset.x + dirOffset.y < 0 && !IsConsumeDirectionVertical)
            {
                _stubChips.Reverse();
            }

            List<List<MantleStubChipComponent>> groupedList = _stubChips
                .GroupBy(s => s.Chip.Cell.GridPosition * dirOffset)
                .Select(grp => grp.ToList())
                .ToList();

            return groupedList;
        }

        private bool IsUnlockedInternal(int value)
        {
            return Mathf.Max(value, 0) == 0;
        }

        private Impact ScheduleConsume([NotNull] CellComponent cell)
        {
            int impactValue = cell.CalculateClearingImpactValue();
            return _impactSystem.ScheduleImpact(cell, -impactValue, ImpactFlag.MantleStubConsume);
        }

        #region IChipConsumputionHandler

        ChipTagSet IChipConsumptionHandler.GetValidChipTags()
        {
            return _validTagsSet;
        }

        int IChipConsumptionHandler.GetPriorityOrder()
        {
            return 0;
        }

        void IChipConsumptionHandler.OnConsuming(
                ChipComponent chip,
                IDeferredInvocationHandle consumptionHandle,
                ref bool isModal
            )
        {
            ScheduledConsumingChipsCount = 0; // reset previously scheduled

            PredictedChipsToUnlock -= 1;
            var di = new DeferredInvocation(() => ChipsToUnlock -= 1);
            using (IDeferredInvocationHandle handle = di.Start())
            {
                if (_requiredChipConsumingDelegate != null)
                {
                    isModal = _requiredChipConsumingDelegate.Invoke(chip, consumptionHandle, handle);
                }
            }
        }

        void IChipConsumptionHandler.OnConsumptionScheduled(ChipComponent chip)
        {
            ScheduledConsumingChipsCount += 1;
            _sourceChipsWeightModifier.ApplyAdditionalWeight(chip);
        }

        void IChipConsumptionHandler.OnScheduledConsumptionCancelled(ChipComponent chip, int impactGroupId)
        {
            ScheduledConsumingChipsCount -= 1;
        }

        #endregion IChipConsumputionHandler

        #region Inner types

        public delegate bool RequiredChipConsumingDelegate(
                [NotNull] ChipComponent chip,
                [NotNull] IDeferredInvocationHandle consumptionHandle,
                [NotNull] IDeferredInvocationHandle commitConsumeHandle
            );

        public delegate void ConsumingDelegate(
                [NotNull] List<KeyValuePair<Vector3, IDeferredInvocationHandle>> stubGroupHandles,
                [NotNull] IDeferredInvocationHandle mantleConsumeHandle
            );

        #endregion Inner types
    }
}