﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using UnityEngine.Tilemaps;

namespace DreamTeam.Match3.SpecialChips.Mantle
{
    public partial class MantlePresenter : MantlePresenter.IEditorApi
    {
        public Tilemap BezelTilemap
        {
            get { return _bezelTilemap; }
            set { _bezelTilemap = value; }
        }

        #region Inner types

        public interface IEditorApi
        {
            Tilemap BezelTilemap { get; set; }
        }

        #endregion Inner types
    }
}

#endif // UNITY_EDITOR
