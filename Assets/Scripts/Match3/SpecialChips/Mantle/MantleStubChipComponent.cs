﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.SpecialChips.Mantle
{
    [RequireComponent(typeof(ChipComponent))]
    public partial class MantleStubChipComponent : MonoBehaviour
    {
        [SerializeField, ReadOnly] private MantleComponent _mantle;

        private ChipComponent _chip => GetComponent<ChipComponent>();

        public MantleComponent Mantle => _mantle;

        public ChipComponent Chip => _chip;
    }
}
