﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.SpecialChips.Firework
{
    [RequireComponent(typeof(ChipComponent))]
    public class FireworkChipComponent : MonoBehaviour, IImpactController
    {
        [SerializeField, Range(0, ProbabilityRightBound)]
        private float[] _missilesProbabilityToHitRequirement = new float[MissilesNumber];

        [Inject(Id = LevelSettings.GoalInstancesId)]
        private IReadOnlyList<Requirement.Instance> _requirements;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        [Inject] private ImpactSystem _impactSystem;
        [Inject] private GridSystem _gridSystem;

        private ChipComponent _chip;
        private const int _impactValue = 1;
        private const ImpactFlag _impactType = ImpactFlag.FireworkMissile;
        private bool _processingImpactScheduling;
        private readonly List<Impact> _scheduledImpacts = new List<Impact>();

        public const int MissilesNumber = 3;
        public const float ProbabilityRightBound = 1;

        public event FiringDelegate Firing;

        public event MissileLaunchedDelegate MissileLaunched;

        protected void Awake()
        {
            _chip = GetComponent<ChipComponent>();
            _chip.CellChanged += OnCellChanged;
            SubscribeToImpactEvents(_chip.Cell);
        }

        protected void OnEnable()
        {
            _chip.AddImpactController(this);
        }

        protected void OnDisable()
        {
            _chip.RemoveImpactController(this);
        }

        private void OnCellChanged(ChipComponent chip, CellComponent prevCell)
        {
            UnsubscribeFromImpactEvents(prevCell);
            SubscribeToImpactEvents(_chip.Cell);
        }

        private void SubscribeToImpactEvents([CanBeNull] CellComponent cell)
        {
            if (cell != null)
            {
                cell.ImpactScheduled += OnImpactScheduled;
            }
        }

        private void UnsubscribeFromImpactEvents([CanBeNull] CellComponent cell)
        {
            if (cell != null)
            {
                cell.ImpactScheduled -= OnImpactScheduled;
            }
        }

        private void OnImpactScheduled(CellComponent cell, Impact impact)
        {
            Assert.AreEqual(_chip.Cell, cell);
            if (_scheduledImpacts.Count > 0 ||
                _processingImpactScheduling ||
                _chip.IsConsumed ||
                !_chip.WillBeConsumed())
            {
                return;
            }

            impact.Cancelling += OnScheduledImpactCancelling;

            _processingImpactScheduling = true;
            List<CellComponent> targets = DetermineTargetCells();
            targets.ForEach(target => _scheduledImpacts.Add(ScheduleImpact(target)));
            _processingImpactScheduling = false;
        }

        private void OnScheduledImpactCancelling(Impact impact)
        {
            impact.Cancelling -= OnScheduledImpactCancelling;

            if (_processingImpactScheduling)
            {
                UnityEngine.Debug.LogError($"{nameof(FireworkChipComponent)}: Cancelling while processing impact scheduling", this);
            }

            _processingImpactScheduling = true;
            CancelScheduledImpacts();
            _processingImpactScheduling = false;
        }

        private void CancelScheduledImpacts()
        {
            _scheduledImpacts.ForEach(impact => impact.Cancel());
            _scheduledImpacts.Clear();
        }

        private void LaunchMissiles(IDeferredInvocationHandle impactEndHandle)
        {
            Firing?.Invoke(impactEndHandle);

            List<CellComponent> targets = DetermineTargetCells();
            for (int i = 0; i < targets.Count; ++i)
            {
                CellComponent target = targets[i];
                LaunchMissile(i, target);
            }
        }

        private List<CellComponent> DetermineTargetCells()
        {
            HashSet<CellComponent> requirementCells = CalculateRequirementTargets();

            var targets = new List<CellComponent>(MissilesNumber);
            var requirementCellsArray = requirementCells.ToArray();

            for (int i = 0; i < MissilesNumber; ++i)
            {
                CellComponent reqCell = requirementCellsArray.Length > i ? requirementCellsArray[i] : null;
                if (reqCell != null)
                {
                    float probability = _random.Next((int)(ProbabilityRightBound * 100)) / 100.0f;
                    float missileProbability = _missilesProbabilityToHitRequirement[i];
                    if (probability < missileProbability || Mathf.Approximately(probability, missileProbability))
                    {
                        targets.Add(reqCell);
                        continue;
                    }
                }

                // fallback. Add random target
                CellComponent cell = FindRandomTarget(targets.Union(requirementCellsArray).ToArray());
                if (cell == null)
                {
                    break; // there is no more targets at all
                }

                targets.Add(cell);
            }

            return targets;
        }

        private void LaunchMissile(int idx, [NotNull] CellComponent target)
        {
            Impact impact = ScheduleImpact(target);
            impact.Begin();

            var di = new DeferredInvocation(() => impact.Perform());
            IDeferredInvocationHandle handle = di.Start();
            MissileLaunched?.Invoke(idx, target.transform.position, handle);
            handle.Unlock();
        }

        private HashSet<CellComponent> CalculateRequirementTargets()
        {
            var cells = new HashSet<CellComponent>();
            var possibleCells = new HashSet<CellComponent>();

            foreach (Requirement.Instance requirement in _requirements)
            {
                if (requirement.IsReached)
                {
                    continue;
                }

                RequirementHelper helper = requirement.FindHelper();
                Assert.IsNotNull(helper, $"{nameof(RequirementHelper)} is null while calculating firework targets");

                var helperCells = new HashSet<CellComponent>();
                helper.FindCellsToImpact(_impactType, int.MaxValue, ref helperCells);
                helperCells.RemoveWhere(cell => cell.Chip.WillBeConsumed());
                possibleCells.UnionWith(helperCells);
            }

            List<CellComponent> possibleCellsShuffled = possibleCells.ToList();
            possibleCellsShuffled.RandomShuffle(_random);
            for (int i = 0; i < MissilesNumber && i < possibleCellsShuffled.Count; ++i)
            {
                cells.Add(possibleCellsShuffled[i]);
            }

            return cells;
        }

        private CellComponent FindRandomTarget(IList<CellComponent> cellsToSkip)
        {
            var randCellPos = new Vector2Int();
            var checkedCells = new HashSet<CellComponent>();
            do
            {
                if (checkedCells.Count == _gridSystem.CellsCount)
                {
                    break;
                }

                randCellPos.x = _random.Next(_gridSystem.Width);
                randCellPos.y = _random.Next(_gridSystem.Height);

                CellComponent cell = _gridSystem.GetCell(randCellPos);
                if (cell == null || !checkedCells.Add(cell))
                {
                    continue;
                }

                if (cell.Chip == null ||
                    cellsToSkip.Contains(cell) ||
                    !cell.Chip.CanBeImpacted(_impactType))
                {
                    continue;
                }

                return cell;
            }
            while (true);

            return null;
        }

        private Impact ScheduleImpact([NotNull] CellComponent targetCell)
        {
            return _impactSystem.ScheduleImpact(targetCell, -_impactValue, _impactType);
        }

        #region IImpactController

        void IImpactController.OnImpact(
            ChipComponent chip,
            ImpactFlag impactType,
            IDeferredInvocationHandle impactEndHandle,
            ref bool isModal)
        {
            if (!chip.IsConsumed)
            {
                return;
            }

            CancelScheduledImpacts(); // reset previously scheduled impact. Actual performs with animation
            isModal = true;

            using (IDeferredInvocationHandle handle = impactEndHandle.Lock())
            {
                LaunchMissiles(handle);
            }
        }

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.FireworkMissiles;
        }

        void IImpactController.Cancel()
        {
            // nothing to do
        }

        #endregion IImpactController

        #region Inner types

        public delegate void FiringDelegate([NotNull] IDeferredInvocationHandle impactEndHandle);

        public delegate void MissileLaunchedDelegate(
            int missileIndex,
            Vector3 flightTargetPosition,
            IDeferredInvocationHandle impactHandle);

        #endregion
    }
}
