﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Utils;
using DreamTeam.Utils.Coroutines;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Movement;
using DreamTeam.Utils.Spine;
using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Rendering;
using Zenject;
using AnimationState = Spine.AnimationState;

namespace DreamTeam.Match3.SpecialChips.Firework
{
    [RequireComponent(typeof(ChipPresenter), typeof(SortingGroup))]
    public class FireworkChipPresenter : MonoBehaviour, IFieldDestabilizer
    {
        [Header("Type")] [SerializeField] private SkeletonAnimation _typeAnimation;
        [SerializeField, SpineAnimation] private string _typeIdleAnimation;
        [SerializeField, SpineAnimation] private string _typeDisposeAnimation;

        [Header("Missiles")] [SerializeField, SpineAnimation]
        private string _missileLaunchAnimation;

        [SerializeField, SpineAnimation] private string _missileFlightAnimation;
        [SerializeField] private float _missileFlightAdditionalAngle;
        [SerializeField, SortingLayer] private int _missileFlightSortingLayer;

        [SerializeField]
        private FireworkMissile[] _missiles = new FireworkMissile[FireworkChipComponent.MissilesNumber];

        [Header("Sounds"), SerializeField, RequiredField] private ChipAudioController _audioController;
        [SerializeField, RequiredField] private string _launchAudioEvent = "LaunchMissiles";

        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;

        private ChipPresenter _chipPresenter;
        private SortingGroup _sortingGroup;
        private FireworkChipComponent _firework;
        private int _animatingMissilesNumber;
        private IDeferredInvocationHandle _impactEndHandle;

        public bool IsStable => AnimatingMissilesNumber == 0;

        private int AnimatingMissilesNumber
        {
            get { return _animatingMissilesNumber; }
            set
            {
                if (_animatingMissilesNumber == value)
                {
                    return;
                }

                _animatingMissilesNumber = Mathf.Clamp(value, 0, value);
                if (_animatingMissilesNumber == 0)
                {
                    Stabilized?.Invoke();
                }
                else
                {
                    Destabilized?.Invoke();
                }
            }
        }

        public event Action Stabilized;
        public event Action Destabilized;

        protected void Awake()
        {
            _chipPresenter = GetComponent<ChipPresenter>();
            _sortingGroup = GetComponent<SortingGroup>();
            _firework = _chipPresenter.ChipComponent.GetComponent<FireworkChipComponent>();
        }

        protected void OnEnable()
        {
            _fieldStabilizationSystem.AddDestabilizer(this);
            _firework.Firing += OnFireworkFiring;
            _firework.MissileLaunched += OnFireworkMissileLaunched;
        }

        protected void OnDisable()
        {
            _firework.Firing -= OnFireworkFiring;
            _firework.MissileLaunched -= OnFireworkMissileLaunched;
            _fieldStabilizationSystem.RemoveDestabilizer(this);
        }

        protected void OnValidate()
        {
            if (_typeAnimation == null)
            {
                UnityEngine.Debug.LogAssertion($"Missing {nameof(_typeAnimation)} reference", this);
            }

            if (string.IsNullOrEmpty(_typeIdleAnimation))
            {
                UnityEngine.Debug.LogAssertion($"Missing {nameof(_typeIdleAnimation)}", this);
            }

            if (string.IsNullOrEmpty(_typeDisposeAnimation))
            {
                UnityEngine.Debug.LogAssertion($"Missing {nameof(_typeDisposeAnimation)}", this);
            }

            if (string.IsNullOrEmpty(_missileLaunchAnimation))
            {
                UnityEngine.Debug.LogAssertion($"Missing {nameof(_missileLaunchAnimation)}", this);
            }

            if (string.IsNullOrEmpty(_missileFlightAnimation))
            {
                UnityEngine.Debug.LogAssertion($"Missing {nameof(_missileFlightAnimation)}", this);
            }

            for (int i = 0; i < _missiles.Length; ++i)
            {
                FireworkMissile missile = _missiles[i];
                if (missile.Animation == null)
                {
                    UnityEngine.Debug.LogAssertion($"Missing Animation in missile_{i}");
                }

                if (missile.Sequence == null)
                {
                    UnityEngine.Debug.LogAssertion($"Missing Sequence in missile_{i}");
                }
            }
        }

        protected void Start()
        {
            _typeAnimation.state.SetAnimation(0, _typeIdleAnimation, true);
        }

        private void OnFireworkFiring(IDeferredInvocationHandle impactEndHandle)
        {
            Assert.IsNull(_impactEndHandle);
            _impactEndHandle = impactEndHandle.Lock();
            _chipPresenter.ChipComponent.DetachView();
            _typeAnimation.state.SetAnimation(0, _typeDisposeAnimation, false);
        }

        private IEnumerator LaunchMissile(int missileIdx, bool isLastMissile)
        {
            _animatingMissilesNumber += 1;
            FireworkMissile missile = _missiles[missileIdx];

            yield return new WaitForSeconds(missile.LaunchDelay);

            if (missile.IdleAnimationHandler != null)
            {
                missile.IdleAnimationHandler.enabled = false;
            }

            missile.Animation.state.SetAnimation(0, _missileLaunchAnimation, false);

            yield return new WaitForEvent<TrackEntry, AnimationState>(
                missile.Animation.state,
                nameof(missile.Animation.state.Complete));

            if (isLastMissile)
            {
                ReleaseImpactEndHandle();
            }

            if (missile.FlightParticleSystem != null)
            {
                missile.FlightParticleSystem.Play(true);
            }

            TrackEntry te = missile.Animation.state.SetAnimation(0, _missileFlightAnimation, true);
            te.MixDuration = 0; // fix of animation mixing (because of existed firework type animation)

            missile.Sequence.Move(missile.StartPosition, missile.TargetPosition, true);
            missile.Sequence.Finished +=
                seq =>
                {
                    missile.SmokeParticleSystem.Stop();
                    missile.SmokeParticleSystem.transform.SetParent(null);
                    LeanTween.value(0, 1, missile.SmokeParticleSystemFreeingDelay)
                        .setOnComplete(() => missile.SmokeParticleSystem.Dispose());

                    missile.Handle?.Unlock();
                    missile.Handle = null;
                };

            bool applyAdditionalRotation = true;
            missile.Sequence.Updated +=
                (sequence, f) =>
                {
                    if (applyAdditionalRotation) // fix of rotation glitch for one update
                    {
                        applyAdditionalRotation = false;
                        missile.Animation.gameObject.transform.Rotate(
                            Vector3.back,
                            _missileFlightAdditionalAngle,
                            Space.Self);
                    }
                };

            _animatingMissilesNumber -= 1;
        }

        private void OnFireworkMissileLaunched(int idx, Vector3 targetPosition, IDeferredInvocationHandle handle)
        {
            _audioController.PlaySound(_launchAudioEvent);

            _sortingGroup.sortingLayerID = _missileFlightSortingLayer;

            _missiles[idx].TargetPosition = targetPosition;
            _missiles[idx].Handle = handle.Lock();

            bool isLastMissile = idx == _missiles.Length - 1;
            StartCoroutine(LaunchMissile(idx, isLastMissile));
        }

        private void ReleaseImpactEndHandle()
        {
            _impactEndHandle.Unlock();
            _impactEndHandle = null;
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }

        #region Internal types

        [Serializable]
        private struct FireworkMissile
        {
            [SerializeField, Range(0, 5)] private float _launchDelay;
            [SerializeField] private MovementSequence _sequence;
            [SerializeField] private SkeletonAnimation _animation;
            [SerializeField] private SkeletonIdleAnimationHandler _idleAnimationHandler;
            [SerializeField] private ParticleSystem _flightParticleSystem;
            [SerializeField] private ParticleSystem _smokeParticleSystem;
            [SerializeField] private float smokeParticleSystemFreeingDelay;

            public float LaunchDelay => _launchDelay;
            public MovementSequence Sequence => _sequence;
            public SkeletonAnimation Animation => _animation;
            public SkeletonIdleAnimationHandler IdleAnimationHandler => _idleAnimationHandler;
            public ParticleSystem FlightParticleSystem => _flightParticleSystem;
            public ParticleSystem SmokeParticleSystem => _smokeParticleSystem;
            public float SmokeParticleSystemFreeingDelay => smokeParticleSystemFreeingDelay;

            public Vector3 StartPosition => _animation.transform.position;
            public Vector3 TargetPosition { get; set; }
            public IDeferredInvocationHandle Handle { get; set; }
        }

        #endregion Internal types
    }
}