﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Movement;
using UnityEngine;

namespace DreamTeam.Match3.SpecialChips.ChipSpawner
{
    public class ChipSpawnerFlightPresenter : MonoBehaviour
    {
        [SerializeField, SortingLayer] private int _sortingLayer;
        [SerializeField] private string _flightTriggerName = "OnSpawnFlight";

        private ChipPresenter _viewClone;
        private ChipPresenter _view;

        private IDeferredInvocationHandle _movingHandle;

        public MovementSequence MovementSequence { get; private set; }

        public void Move(
                ChipComponent chip,
                Vector3 positionFrom,
                Vector3 positionTo,
                int sortingOrder,
                IDeferredInvocationHandle movingHandle
            )
        {
            _viewClone = chip.CloneView();
            if (_viewClone == null)
            {
                UnityEngine.Debug.LogWarning(
                        $"Missing presenter of {chip.name} in {chip.Cell.name}."
                    );
                return;
            }

            _movingHandle = movingHandle.Lock();

            _viewClone.SortingOptions.PushSortingLayer(_sortingLayer);
            _viewClone.SortingOptions.PushSortingOrder(sortingOrder);
            _viewClone.transform.SetParent(transform, false);
            _viewClone.SetTrigger(_flightTriggerName);

            _view = chip.Presenter;
            _view.gameObject.SetActive(false);

            MovementSequence.Move(positionFrom, positionTo, true);
        }

        protected void Awake()
        {
            MovementSequence = GetComponent<MovementSequence>();
        }

        protected void OnDestroy()
        {
            if (_viewClone != null)
            {
                _viewClone.Dispose();
            }
            
            if (_view != null)
            {
                _view.gameObject.SetActive(true);
            }

            _movingHandle.Unlock();
            _movingHandle = null;
        }
    }
}