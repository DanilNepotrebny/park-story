﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.GameControls;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Utils;
using DreamTeam.Utils.Coroutines;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.SpecialChips.ChipSpawner
{
    public class ChipSpawnSystem : MonoBehaviour, IFieldDestabilizer
    {
        [SerializeField] private GameControlsMode _gameControlsMode;
        
        [Inject] private MovesSystem _movesSystem;
        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;
        [Inject] private GridSystem _gridSystem;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        private ActionDisposable _modeDisableHandle;

        private const ImpactFlag _replaceImpactType = ImpactFlag.Replace;

        private LinkedList<ChipSpawner> _scheduledChipSpawners = new LinkedList<ChipSpawner>();
        private List<CellComponent> _candidatesCache = new List<CellComponent>();

        private bool _isStable;

        public event Action Stabilized;
        public event Action Destabilized;
        
        public bool IsStable
        {
            get { return _isStable; }

            set
            {
                if (_isStable != value)
                {
                    _isStable = value;
                    if (_isStable)
                    {
                        Stabilized?.Invoke();
                    }
                    else
                    {
                        Destabilized?.Invoke();
                    }
                }
            }
        }

        public void ScheduleChipsSpawn(ChipSpawner chipSpawner)
        {
            _scheduledChipSpawners.AddLast(chipSpawner);

            if (_scheduledChipSpawners.Count == 1)
            {
                StartCoroutine(SpawnChipsRoutine());
            }
        }

        protected void OnDestroy()
        {
            ReleaseModeDisableHandle();
        }

        private IEnumerator SpawnChipsRoutine()
        {
            Assert.IsNull(_modeDisableHandle, "_modeDisableHandle != null");
            _modeDisableHandle = _gameControlsSystem.EnableMode(_gameControlsMode);

            IsStable = true;
            _fieldStabilizationSystem.AddDestabilizer(this);

            while (_scheduledChipSpawners.Count > 0)
            {
                _fieldStabilizationSystem.AddStabilizationHandler(OnFieldStabilized, FieldStabilizationHandlerOrder.ChipSpawnSystem);

                yield return new WaitUntil(() => !IsStable);

                _fieldStabilizationSystem.RemoveStabilizationHandler(OnFieldStabilized);

                LinkedList<CellComponent> allCandidates = new LinkedList<CellComponent>(_gridSystem.Cells.Where(CanBeReplaced));
                
                allCandidates.RandomShuffle(_random);
                LinkedListNode<ChipSpawner> chipSpawnerNode = _scheduledChipSpawners.First;
                while (chipSpawnerNode != null)
                {
                    if (chipSpawnerNode.Value.TrySelectCandidates(allCandidates, _candidatesCache))
                    {
                        foreach (CellComponent cell in _candidatesCache)
                        {
                            allCandidates.Remove(cell);
                        }

                        chipSpawnerNode.Value.SpawnChips(_candidatesCache, _replaceImpactType);

                        LinkedListNode<ChipSpawner> nextNode = chipSpawnerNode.Next;
                        _scheduledChipSpawners.Remove(chipSpawnerNode);
                        chipSpawnerNode = nextNode;
                    }

                    _candidatesCache.Clear();
                }

                if (_scheduledChipSpawners.Count > 0)
                {
                    IsStable = true;
                    ReleaseModeDisableHandle();

                    yield return new WaitForEvent<MovesSystem>(_movesSystem, "MoveSucceeding");

                    Assert.IsNull(_modeDisableHandle, "_modeDisableHandle != null");
                    _modeDisableHandle = _gameControlsSystem.EnableMode(_gameControlsMode);
                    break;
                }
            }

            ReleaseModeDisableHandle();

            IsStable = true;
            _fieldStabilizationSystem.RemoveDestabilizer(this);
        }

        private void OnFieldStabilized()
        {
            IsStable = false;
        }

        private bool CanBeReplaced(CellComponent cell)
        {
            return cell?.Chip?.CanBeImpacted(_replaceImpactType) ?? false;
        }

        private void ReleaseModeDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }
    }
}