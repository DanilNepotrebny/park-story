﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using Spine;
using Spine.Unity;
using UnityEngine;
using Zenject;
using Event = Spine.Event;

namespace DreamTeam.Match3.SpecialChips.ChipSpawner
{
    [RequireComponent(typeof(ChipPresenter))]
    public class ChipSpawnerPresenter : ChipStateSpinePresenter
    {
        [SerializeField] private ChipSpawnerFlightPresenter _flightPresenterPrefab;
        [SerializeField] private GameObject _spawnEffectPrefab;
        [SerializeField, SpineAnimation] private string _spawnAnimationName;
        [SerializeField, SpineEvent] private string _spawnEventName;
        [SerializeField] private Transform _spawnOrigin;

        [Inject] private Instantiator _instantiator;

        private ChipSpawner _chipSpawner;

        private IDeferredInvocationHandle _chipSpawningInvocationHandle;

        private List<ChipComponent> _pendingChips;
        private List<IDeferredInvocationHandle> _pendingChipHandles = new List<IDeferredInvocationHandle>();

        protected override int State => _chipSpawner.ImpactCounter;

        protected override void Awake()
        {
            base.Awake();

            ChipPresenter chipPresenter = GetComponent<ChipPresenter>();
            _chipSpawner = chipPresenter.ChipComponent.GetComponent<ChipSpawner>();
            _chipSpawner.ImpactCounterChanged += OnStateChanged;
            _chipSpawner.ChipSpawning += OnChipSpawning;
        }

        private void OnChipSpawning(List<ChipComponent> chips, List<IDeferredInvocationHandle> replaceHandles)
        {
            _pendingChips = chips;

            foreach (IDeferredInvocationHandle h in replaceHandles)
            {
                _pendingChipHandles.Add(h.Lock());
            }

            TrackEntry e = AnimationState.SetAnimation(0, _spawnAnimationName, false);
            e.Event += OnSpawnEvent;
            e.Complete += OnSpawn;
            e.Interrupt += OnSpawn;
        }

        private void OnSpawnEvent(TrackEntry trackEntry, Event e)
        {
            if (e.ToString() == _spawnEventName)
            {
                OnSpawn(trackEntry);
            }
        }

        private void OnSpawn(TrackEntry trackEntry)
        {
            trackEntry.Event -= OnSpawnEvent;
            trackEntry.Complete -= OnSpawn;
            trackEntry.Interrupt -= OnSpawn;

            SpawnFlightPresenters(_pendingChips, _pendingChipHandles);

            foreach (IDeferredInvocationHandle h in _pendingChipHandles)
            {
                h.Dispose();
            }

            _pendingChipHandles.Clear();
            _pendingChips = null;
        }

        private void SpawnFlightPresenters(List<ChipComponent> chips, List<IDeferredInvocationHandle> replaceHandles)
        {
            _instantiator.Instantiate(_spawnEffectPrefab, transform.position);

            for (int i = 0; i < chips.Count; i++)
            {
                ChipSpawnerFlightPresenter flightPresenter = _instantiator.Instantiate(_flightPresenterPrefab);
                flightPresenter.Move(chips[i], _spawnOrigin.transform.position, chips[i].transform.position, i, replaceHandles[i]);
            }
        }
    }
}