﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.GameControls;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.SpecialChips.ChipSpawner
{
    [RequireComponent(typeof(ChipComponent))]
    public class ChipSpawner : MonoBehaviour, IFieldDestabilizer
    {
        [SerializeField] private ChipComponent _chipPrefab;
        [SerializeField] private int _spawnedChipsCount = 4;
        [SerializeField] private int _impactToSpawn;
        [SerializeField] private GameControlsMode _gameControlsMode;
        [SerializeField] private ChipTagSet _ignoredBaseChipTags;

        [Inject] private GenerationSystem _generationSystem;
        [Inject] private ChipSpawnSystem _chipSpawnSystem;
        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;
        [Inject] private LevelRewardSequence _rewardSequence;

        private ChipComponent _chip;
        private int _impactCount;
        private int _spawningChipsCount;
        private ChipTag _ignoredChipTag;
        private bool _isActive = true;
        private ActionDisposable _modeDisableHandle;

        private int _spawningChipsCounter
        {
            get { return _spawningChipsCount; }
            set
            {
                if (_spawningChipsCount != value)
                {
                    _spawningChipsCount = value;

                    if (IsStable)
                    {
                        Stabilized?.Invoke();
                    }
                    else
                    {
                        Destabilized?.Invoke();
                    }
                }
            }
        }

        public bool IsStable => _spawningChipsCounter == 0;

        public int ImpactCounter
        {
            get { return _impactCount; }
            set
            {
                if (_impactCount != value)
                {
                    _impactCount = value;
                    ImpactCounterChanged?.Invoke();
                }
            }
        }

        public event Action ImpactCounterChanged;
        public event Action Stabilized;
        public event Action Destabilized;
        public event Action<List<ChipComponent>, List<IDeferredInvocationHandle>> ChipSpawning;

        public bool TrySelectCandidates(IEnumerable<CellComponent> allCandidates, List<CellComponent> candidatesCache)
        {
            foreach (CellComponent cell in allCandidates)
            {
                if (cell.Chip.Tags.Has(_ignoredChipTag))
                {
                    continue;
                }

                candidatesCache.Add(cell);

                if (candidatesCache.Count == _spawnedChipsCount)
                {
                    return true;
                }
            }

            return false;
        }

        public void SpawnChips(IEnumerable<CellComponent> targetCells, ImpactFlag replaceImpactType)
        {
            _spawningChipsCounter = targetCells.Count();

            if (_spawningChipsCounter > 0)
            {
                Assert.IsNull(_modeDisableHandle, "_modeDisableHandle != null");
                _modeDisableHandle = _gameControlsSystem.EnableMode(_gameControlsMode);
            }

            List<ChipComponent> chips = new List<ChipComponent>();
            List<IDeferredInvocationHandle> replaceInvocationHandles = new List<IDeferredInvocationHandle>();
            
            foreach (CellComponent targetCell in targetCells)
            {
                ChipComponent chip = _generationSystem.InstantiateChip(_chipPrefab);
                chip.transform.position = targetCell.transform.position;

                DeferredInvocation replaceInvocation = new DeferredInvocation(
                        () =>
                        {
                            targetCell.ReplaceChip(chip, replaceImpactType);

                            _spawningChipsCounter--;
                            if (_spawningChipsCounter == 0)
                            {
                                ReleaseModeDisableHandle();
                                ImpactCounter = 0;
                            }
                        }
                    );

                chips.Add(chip);
                replaceInvocationHandles.Add(replaceInvocation.Start());
            }

            ChipSpawning?.Invoke(chips, replaceInvocationHandles);

            foreach (IDeferredInvocationHandle handle in replaceInvocationHandles)
            {
                handle.Dispose();
            }
        }

        protected void Awake()
        {
            _chip = GetComponent<ChipComponent>();
            _chip.Impacting += OnChipImpacting;

            _ignoredChipTag = _chipPrefab.Tags.FindFirstOf(_ignoredBaseChipTags);

            _fieldStabilizationSystem.AddDestabilizer(this);
            _rewardSequence.Performing += OnLevelCompletionStarting;
        }

        protected void OnDestroy()
        {
            _fieldStabilizationSystem.RemoveDestabilizer(this);
            ReleaseModeDisableHandle();
        }

        private void OnLevelCompletionStarting()
        {
            _rewardSequence.Performing -= OnLevelCompletionStarting;

            _isActive = false;
        }

        private void OnChipImpacting(ChipComponent chip, int impactValue)
        {
            if (_isActive && ImpactCounter < _impactToSpawn)
            {
                ImpactCounter += impactValue;

                if (ImpactCounter == _impactToSpawn)
                {
                    _chipSpawnSystem.ScheduleChipsSpawn(this);
                }
            }
        }

        private void ReleaseModeDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }
    }
}