﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Movement;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3.SpecialChips.GumballMachine
{
    [RequireComponent(typeof(Animation))]
    public class GumballComponent : MonoBehaviour
    {
        [SerializeField] private MovementSequence _movementSequence;

        private IDeferredInvocationHandle _handle;
        private Animation _animation;
        private Vector3 _targetPos;
        private Coroutine _animationCoroutine;

        public void StartSequence(Vector3 targetPos, IDeferredInvocationHandle handle)
        {
            Assert.IsNull(_handle);
            _handle = handle.Lock();

            _targetPos = targetPos;

            if (_animationCoroutine != null)
            {
                UnityEngine.Debug.LogWarning(
                    "Attempt to start sequence when there is ongoing one. Sequence will be restarted.",
                    this);
                StopCoroutine(_animationCoroutine);
            }

            _animationCoroutine = StartCoroutine(StartSequence_Internal());
        }

        protected void Awake()
        {
            _animation = GetComponent<Animation>();
            _movementSequence.Finished += OnFlightFinished;
        }

        private IEnumerator StartSequence_Internal()
        {
            _animation.Play();

            yield return new WaitUntil(() => !_animation.isPlaying);

            _movementSequence.Move(_targetPos, true);
        }

        private void OnFlightFinished(MovementSequence movementSequence)
        {
            _handle.Unlock();
            _handle = null;

            _animationCoroutine = null;
        }
    }
}