﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3.SpecialChips.GumballMachine
{
    [RequireComponent(typeof(ChipPresenter))]
    public class GumballMachinePresenter : ChipStateSpinePresenter
    {
        private ChipComponent _chip;

        protected override int State => _chip.Health - 1;

        protected override void Awake()
        {
            base.Awake();

            _chip = GetComponent<ChipPresenter>().ChipComponent;
            Assert.IsNotNull(_chip, $"There is no {nameof(ChipComponent)} in parent");
            _chip.Impacting += (chip, impactValue) => OnStateChanged();
        }
    }
}