﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using Random = UnityEngine.Random;

namespace DreamTeam.Match3.SpecialChips.GumballMachine
{
    [RequireComponent(typeof(ChipComponent))]
    public class GumballMachineComponent : MonoBehaviour, IImpactController
    {
        [SerializeField] private float _delayBetweenProjectilesSpawning;
        [SerializeField] private Vector3 _spawnOffset;
        [SerializeField] private bool _randomizeProjectiles;
        [SerializeField] private GumballComponent[] _projectilePrefabs;

        [Inject] private Instantiator _instantiator;

        private ChipComponent Chip => _chip ?? (_chip = GetComponent<ChipComponent>());
        private ChipComponent _chip;
        private int _projectilesToSpawn;
        private Coroutine _animCoroutine;
        private IDeferredInvocationHandle _consumeHandle;
        private readonly Queue<GumballComponent> _projectilePrefabsToSpawn = new Queue<GumballComponent>(5);

        public int InitialGumballsCount => GetComponent<HealthController>().MaxHealth;

        public event Action<GumballComponent> ProjectileSpawned;

        protected void OnValidate()
        {
            if (_projectilePrefabs.Length == 0)
            {
                UnityEngine.Debug.LogError("Must be at least one projectile prefab to spawn!", this);
            }
            else
            {
                for (int i = 0; i < _projectilePrefabs.Length; ++i)
                {
                    if (_projectilePrefabs[i] == null)
                    {
                        UnityEngine.Debug.LogError($"Have null-projectile in prefabs! Element {i}", this);
                    }
                }
            }

            if (_delayBetweenProjectilesSpawning < 0)
            {
                UnityEngine.Debug.LogWarning(
                    $"Delay between projectiles must be positive number. Will be clamped to 0. Was {_delayBetweenProjectilesSpawning}",
                    this);
                _delayBetweenProjectilesSpawning = 0;
            }
        }

        protected void Start()
        {
            Chip.Impacting += OnChipImpacting;
            Chip.AddImpactController(this);
        }

        protected void OnDestroy()
        {
            Assert.IsNull(_consumeHandle, $"Consume handle is not released");
            Chip.RemoveImpactController(this);
        }

        private void OnChipImpacting(ChipComponent chip, int impactValue)
        {
            _projectilesToSpawn += impactValue;
            FillProjectilesQueue(chip.Health, impactValue);
            Assert.IsTrue(_projectilesToSpawn == _projectilePrefabsToSpawn.Count, "Check logic!");

            if (_animCoroutine == null)
            {
                _animCoroutine = StartCoroutine(SpawnProjectiles());
            }
        }

        private IEnumerator SpawnProjectiles()
        {
            while (_projectilesToSpawn > 0)
            {
                GumballComponent projectilePrefab = _projectilePrefabsToSpawn.Dequeue();
                _projectilesToSpawn -= 1;

                if (ProjectileSpawned != null)
                {
                    GumballComponent projectile = _instantiator.Instantiate(
                        projectilePrefab,
                        transform.position + _spawnOffset);
                    ProjectileSpawned.Invoke(projectile);
                }

                yield return new WaitForSeconds(_delayBetweenProjectilesSpawning);
            }

            ReleaseConsumeHandle();
            _animCoroutine = null;
        }

        private void FillProjectilesQueue(int currentHealth, int impactValue)
        {
            do
            {
                int previousHealth = currentHealth + impactValue;
                previousHealth = Mathf.Clamp(previousHealth, 1, previousHealth);
                bool randomize = (previousHealth - 1) >= _projectilePrefabs.Length || _randomizeProjectiles;
                int index = randomize ? Random.Range(0, _projectilePrefabs.Length) : previousHealth - 1;
                GumballComponent projectileToEnqueue = _projectilePrefabs[index];
                _projectilePrefabsToSpawn.Enqueue(projectileToEnqueue);
                impactValue -= 1;
            }
            while (impactValue > 0);
        }

        private void ReleaseConsumeHandle()
        {
            _consumeHandle?.Unlock();
            _consumeHandle = null;
        }

        #region IImpactController

        public void OnImpact(
            ChipComponent chip,
            ImpactFlag impactType,
            IDeferredInvocationHandle impactEndHandle,
            ref bool isModal)
        {
            if (!chip.WillBeConsumed())
            {
                return;
            }

            Assert.IsNull(_consumeHandle, $"Consume handle is already locked");
            _consumeHandle = impactEndHandle.Lock();
        }

        public void Cancel()
        {
            ReleaseConsumeHandle();
        }

        public ImpactPriority GetPriority()
        {
            return ImpactPriority.GumballMachineProjectilesSpawn;
        }

        #endregion IImpactController
    }
}