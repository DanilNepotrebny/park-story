﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.UI.HUD;
using DreamTeam.Match3.UI.RequirementPresenters;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Movement;
using DreamTeam.Utils.Spine;
using JetBrains.Annotations;
using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.SpecialChips.Frisbee
{
    [RequireComponent(typeof(ChipPresenter))]
    public class FrisbeePresenter : MonoBehaviour, IFieldDestabilizer
    {
        [Inject] private LimitationsPanel _limitationsPanel;
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;

        [SerializeField] private FrisbeeComponent _frisbeeComponent;
        [SerializeField] private MovementSequence _consumeSequence;
        [SerializeField, Min(0f), Max(1f)] private float _cellFreeingDelay = 0.5f;
        [SerializeField] private SkeletonIdleAnimationHandler _idleAnimationHandler;
        [SerializeField] private ParticleSystem _flightParticleSystem;
        [SerializeField, SortingLayer] private int _flightSortingLayer;
        [SerializeField, SpineAnimation] private string _flightAnimationBegin;
        [SerializeField, SpineAnimation] private string _flightAnimationIdle;
        [SerializeField, RequiredField] private ChipAudioController _audioController;
        [SerializeField, RequiredField] private string _flightAudioEvent;

        #region IFieldDestabilizer

        public event Action Stabilized;
        public event Action Destabilized;
        public bool IsStable => !IsAnimationOngoing;

        #endregion IFieldDestabilizer

        private bool IsAnimationOngoing
        {
            get { return _isAnimationOngoing; }
            set
            {
                if (_isAnimationOngoing == value)
                {
                    return;
                }

                _isAnimationOngoing = value;

                if (_isAnimationOngoing)
                {
                    Destabilized?.Invoke();
                }
                else
                {
                    Stabilized?.Invoke();
                }
            }
        }

        private bool _isAnimationOngoing;

        private IDeferredInvocationHandle _addMovesHandle;
        private SkeletonAnimation _skeletonAnimation;
        private ChipPresenter _chipPresenter;
        private Vector3 _flightBeginPos;

        protected void Awake()
        {
            _chipPresenter = GetComponent<ChipPresenter>();
            _skeletonAnimation = GetComponentInChildren<SkeletonAnimation>();
            _frisbeeComponent.MovesAdding += MovesAdding;
        }

        protected void Start()
        {
            _fieldStabilizationSystem.AddDestabilizer(this);
        }

        protected void OnDestroy()
        {
            _fieldStabilizationSystem.RemoveDestabilizer(this);
        }

        private void MovesAdding(
            [NotNull] IDeferredInvocationHandle impactEndHandle,
            [NotNull] IDeferredInvocationHandle addMovesHandle)
        {
            _frisbeeComponent.MovesAdding -= MovesAdding;

            Assert.IsNull(_addMovesHandle);
            _addMovesHandle = addMovesHandle.Lock();

            StartCoroutine(UnlockImpactEnd(impactEndHandle.Lock()));

            _flightBeginPos = transform.position;
            IsAnimationOngoing = true;
            ChipPresenter chipPresenter = _frisbeeComponent.Chip.DetachView();
            Assert.AreEqual(_chipPresenter, chipPresenter, "Detached view must be this one. Check implementation!");

            if (_idleAnimationHandler != null)
            {
                _idleAnimationHandler.enabled = false;
            }

            _skeletonAnimation.state.SetAnimation(0, _flightAnimationBegin, false);
            _skeletonAnimation.state.Complete += OnFlightBeginAnimationComplete;
        }

        private void OnFlightBeginAnimationComplete(TrackEntry trackEntry)
        {
            _skeletonAnimation.state.Complete -= OnFlightBeginAnimationComplete;

            _audioController.PlaySound(_flightAudioEvent);

            _skeletonAnimation.state.SetAnimation(0, _flightAnimationIdle, true);

            _consumeSequence.Finished += MovementFinished;
            _chipPresenter.SortingOptions.PushSortingLayer(_flightSortingLayer);

            if (_flightParticleSystem != null)
            {
                _flightParticleSystem.Play();
            }

            var limitationsPresenter = _limitationsPanel.GetComponentInChildren<MovesLimitationInstancePresenter>();
            _consumeSequence.Move(_flightBeginPos, limitationsPresenter.transform.position, true);
        }

        private void MovementFinished(MovementSequence movementSequence)
        {
            _consumeSequence.Finished -= MovementFinished;
            IsAnimationOngoing = false;

            _addMovesHandle.Unlock();
            _addMovesHandle = null;
        }

        private IEnumerator UnlockImpactEnd([NotNull] IDeferredInvocationHandle impactEndHandle)
        {
            yield return new WaitForSeconds(_cellFreeingDelay);

            impactEndHandle.Unlock();
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }
    }
}