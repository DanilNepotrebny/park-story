﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.SpecialChips.Frisbee
{
    [RequireComponent(typeof(ChipComponent))]
    public class FrisbeeComponent : MonoBehaviour, IImpactController
    {
        [SerializeField] private ProductPack _pack;

        [Inject] private ProductPackInstanceContainer _productPackInstanceContainer;

        private DeferredInvocation _addMovesInvocation;
        private IDeferredInvocationHandle _impactEndHandle;

        public ChipComponent Chip { get; private set; }

        public event MovesAddingDelegate MovesAdding;

        protected void Awake()
        {
            _addMovesInvocation = new DeferredInvocation(
                    () => _productPackInstanceContainer.GetInstance(_pack).GiveForFree(GamePlace.InGame)
                );
            Chip = GetComponent<ChipComponent>();
        }

        protected void Start()
        {
            Chip.AddImpactController(this);
        }

        protected void OnDestroy()
        {
            Chip.RemoveImpactController(this);
        }

        #region IImpactController

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.AcquireAdditionalMoves;
        }

        void IImpactController.OnImpact(
            ChipComponent chip,
            ImpactFlag impactType,
            IDeferredInvocationHandle impactEndHandle,
            ref bool isModal)
        {
            if (!chip.IsConsumed)
            {
                return;
            }

            isModal = true;

            var addMovesHandle = _addMovesInvocation.Start();
            MovesAdding?.Invoke(impactEndHandle, addMovesHandle);
            addMovesHandle.Unlock();
        }

        void IImpactController.Cancel()
        {
            // TODO: Nothing to do here?
        }

        #endregion IImpactController

        #region Inner types

        public delegate void MovesAddingDelegate(
                [NotNull] IDeferredInvocationHandle impactEndHandle,
                [NotNull] IDeferredInvocationHandle addMovesHandle
            );

        #endregion Inner types
    }
}