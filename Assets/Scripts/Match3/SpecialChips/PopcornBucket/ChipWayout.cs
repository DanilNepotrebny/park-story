﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.ChipTags;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Match3.SpecialChips.PopcornBucket
{
    public class ChipWayout : MonoBehaviour
    {
        [SerializeField] private ChipTagSet _acceptableChips;

        private CellComponent _cell;
        private Impact _silentlyScheduledImpact;

        protected void Awake()
        {
            _cell = GetComponentInParent<CellComponent>();
            _cell.AddMovementStopHandler(CellComponent.MovementHandlerType.ChipWayout, OnMovementStopped);
            _cell.AddMovementFinishHandler(CellComponent.MovementHandlerType.ChipWayout, OnMovementFinished);
            _cell.ChipChangedSilently += OnChipChangedSilently;
        }

        private void OnChipChangedSilently(CellComponent cell, ChipComponent oldChip)
        {
            _silentlyScheduledImpact?.Cancel();
            _silentlyScheduledImpact = null;

            ChipComponent chip = cell.Chip;
            if (chip == null || !IsAcceptableChip(chip))
            {
                return;
            }

            _silentlyScheduledImpact = chip.ScheduleConsumption(ImpactFlag.WayoutConsume);
        }

        private void OnMovementStopped([NotNull] CellComponent cell, ChipComponent.MovingType type)
        {
            if (type != ChipComponent.MovingType.Swapping)
            {
                TryConsume();
            }
        }

        private void OnMovementFinished([NotNull] CellComponent cell, ChipComponent.MovingType type)
        {
            TryConsume();
        }

        private void TryConsume()
        {
            ChipComponent chip = _cell.Chip;
            if (chip == null || !IsAcceptableChip(chip))
            {
                return;
            }

            chip
                .ScheduleConsumption(ImpactFlag.WayoutConsume)
                .Perform();
        }

        private bool IsAcceptableChip([NotNull] ChipComponent chip)
        {
            ChipTag chipTag = chip.Tags.FindFirstOf(_acceptableChips, false);
            return chipTag != null;
        }
    }
}