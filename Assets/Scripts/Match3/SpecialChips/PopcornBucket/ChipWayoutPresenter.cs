﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using DreamTeam.Utils.RendererSorting;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.SpecialChips.PopcornBucket
{
    public class ChipWayoutPresenter : MonoBehaviour, ISortingOptionsProvider
    {
        [Inject] private Instantiator _instantiator;

        private Renderer _renderer;

        public SortingOptions SortingOptions { get; private set; }

        protected void Awake()
        {
            _renderer = GetComponentInChildren<Renderer>();

            SortingOptions = _instantiator.Instantiate<SortingOptions>(
                _renderer.sortingLayerID,
                _renderer.sortingOrder);
            SortingOptions.SortingLayerChanged += OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged += OnSortingOrderChanged;
        }

        protected void OnDestroy()
        {
            SortingOptions.SortingLayerChanged -= OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged -= OnSortingOrderChanged;
            SortingOptions.Dispose();
            SortingOptions = null;
        }

        private void OnSortingOrderChanged(SortingOptions options, int prevOrder)
        {
            _renderer.sortingOrder = options.SortingOrder;
        }

        private void OnSortingLayerChanged(SortingOptions options)
        {
            _renderer.sortingLayerID = options.SortingLayerId;
        }
    }
}