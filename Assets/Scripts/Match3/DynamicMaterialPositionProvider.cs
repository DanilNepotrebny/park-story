﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(RenderersController), typeof(PositionChangeNotifier))]
    public class DynamicMaterialPositionProvider : MonoBehaviour
    {
        [SerializeField] private string _shaderPropertyName;

        private RenderersController _renderersController;
        private PositionChangeNotifier _positionChangeNotifier;

        /// <summary>
        /// Unity callback
        /// </summary>
        protected void Start()
        {
            _renderersController = GetComponent<RenderersController>();
            GetComponent<PositionChangeNotifier>().PositionChanged += UpdateRenderers;

            UpdateRenderers();
        }

        private void UpdateRenderers(Transform tr = null)
        {
            foreach (Renderer r in _renderersController.Renderers)
            {
                MaterialPropertyBlock b = new MaterialPropertyBlock();
                r.GetPropertyBlock(b);

                Vector4 position = transform.position;
                position.w = 1;
                b.SetVector(_shaderPropertyName, position);

                r.SetPropertyBlock(b);
            }
        }
    }
}