﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(SpriteRenderer), typeof(Animation))]
    public class TapAndTapSelectionPresenter : MonoBehaviour
    {
        public void SetEnabled(CellComponent cell)
        {
            transform.position = cell.transform.position;
            GetComponent<SpriteRenderer>().enabled = true;
            GetComponent<Animation>().Play();
        }

        public void SetDisabled()
        {
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<Animation>().Stop();
        }
    }
}