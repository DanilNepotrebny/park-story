﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(Tilemap), typeof(TilemapRenderer))]
    public class GridBezelAnimator : MonoBehaviour
    {
        /// <summary>
        /// Event fires when an animation has been finished
        /// </summary>
        public event Action<Vector3, AnimationType> AnimationFinished;

        public enum AnimationType
        {
            Show,
            Hide
        };

        [SerializeField] private GridLayout _grid;

        private Tilemap _tilemap;
        private readonly HashSet<AnimatingTile> _animatingTiles = new HashSet<AnimatingTile>();
        private const float AnimationTickTimeMS = 0.0166f; // 60 fps

        public TilemapRenderer TilemapRenderer { get; private set; }

        /// <summary>
        /// Is tile animating right now
        /// </summary>
        public bool IsPlaying(Vector3 tilePos)
        {
            return _animatingTiles.Contains(new AnimatingTile { Position = tilePos });
        }

        /// <summary>
        /// Set tile alpha
        /// </summary>
        public void SetTileAlpha(Vector3 tileWorldPos, float alpha)
        {
            Vector3Int cellPos = _grid.WorldToCell(tileWorldPos);
            SetTileAlpha(cellPos, alpha);
        }

        /// <summary>
        /// Stop all ongoing animations
        /// </summary>
        public void StopAllAnimations()
        {
            StopAllCoroutines();
        }

        /// <summary>
        /// Play an animation for specified tile
        /// </summary>
        /// <param name="tilePos">Tile world position</param>
        /// <param name="type">Type of animation to play</param>
        /// <param name="duration">Animation duration in seconds</param>
        public void PlayAnimation(Vector3 tilePos, AnimationType type, float duration)
        {
            if (IsPlaying(tilePos))
            {
                AnimatingTile animTile = _animatingTiles.First(e => EqualsApproximately(e.Position, tilePos));
                StopCoroutine(animTile.AnimationCoroutine);
                _animatingTiles.Remove(animTile);
            }

            Coroutine c = StartCoroutine(PlayAnimationInternal(tilePos, type, duration));

            AnimatingTile tile = new AnimatingTile
            {
                Position = tilePos,
                AnimationCoroutine = c
            };
            _animatingTiles.Add(tile);
        }

        /// <summary>
        /// Equal two Vector3 with approximation
        /// </summary>
        public static bool EqualsApproximately(Vector3 first, Vector3 second)
        {
            // TODO: Extension method for Vector3
            return Mathf.Approximately(first.x, second.x) &&
                Mathf.Approximately(first.y, second.y) &&
                Mathf.Approximately(first.z, second.z);
        }

        private IEnumerator PlayAnimationInternal(Vector3 tileWorldPos, AnimationType type, float duration)
        {
            Vector3Int tilePos = _grid.WorldToCell(tileWorldPos);

            Coroutine c = null;
            switch (type)
            {
                case AnimationType.Show:
                    c = StartCoroutine(PlayBezelCellAppearAnimation(tilePos, duration));
                    break;
                case AnimationType.Hide:
                    c = StartCoroutine(PlayBezelCellDisappearAnimation(tilePos, duration));
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            yield return c;

            AnimationFinished?.Invoke(tileWorldPos, type);
        }

        private IEnumerator PlayBezelCellAppearAnimation(Vector3Int tilePos, float duration)
        {
            const float targetAlphaValue = 1f;
            if (Mathf.Approximately(duration, 0) || duration < 0)
            {
                SetTileAlpha(tilePos, targetAlphaValue);
                yield break;
            }

            float valueTickDelta = AnimationTickTimeMS / duration;

            float alpha = 0;
            while (alpha < targetAlphaValue || Mathf.Approximately(alpha, targetAlphaValue))
            {
                SetTileAlpha(tilePos, alpha);

                yield return new WaitForSeconds(AnimationTickTimeMS);

                alpha += valueTickDelta;
            }
        }

        private IEnumerator PlayBezelCellDisappearAnimation(Vector3Int tilePos, float duration)
        {
            const float targetAlphaValue = 0;
            if (Mathf.Approximately(duration, 0) || duration < 0)
            {
                SetTileAlpha(tilePos, targetAlphaValue);
                yield break;
            }

            float valueTickDelta = AnimationTickTimeMS / duration;

            float alpha = 1f;
            while (alpha > targetAlphaValue || Mathf.Approximately(alpha, targetAlphaValue))
            {
                SetTileAlpha(tilePos, alpha);

                yield return new WaitForSeconds(AnimationTickTimeMS);

                alpha -= valueTickDelta;
            }
        }

        private void OnAnimationFinished(Vector3 tileWorldPos, AnimationType animationType)
        {
            _animatingTiles.RemoveWhere(d => EqualsApproximately(d.Position, tileWorldPos));
        }

        private void SetTileAlpha(Vector3Int tilePos, float alpha)
        {
            Color color = _tilemap.GetColor(tilePos);
            color.a = alpha;
            _tilemap.SetColor(tilePos, color);
        }

        /// <summary>
        /// Unity callback
        /// </summary>
        protected void Awake()
        {
            _tilemap = GetComponent<Tilemap>();
            TilemapRenderer = GetComponent<TilemapRenderer>();

            AnimationFinished += OnAnimationFinished;
        }

        private struct AnimatingTile : IEqualityComparer<AnimatingTile>
        {
            public Vector3 Position;
            public Coroutine AnimationCoroutine;

            bool IEqualityComparer<AnimatingTile>.Equals(AnimatingTile first, AnimatingTile second)
            {
                return EqualsApproximately(first.Position, second.Position);
            }

            int IEqualityComparer<AnimatingTile>.GetHashCode(AnimatingTile tile)
            {
                return tile.Position.GetHashCode();
            }
        }
    }
}