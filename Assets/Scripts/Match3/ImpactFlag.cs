﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Flags that determine impact type when doing impact to the chip. Chip can be immune to some impact types.
    /// </summary>
    [Flags]
    public enum ImpactFlag
    {
        /// <summary> Simple match of 3 or more chips </summary>
        Match = 1 << 0,

        /// <summary> Activation of complex chip via combination with other chip or double tapping. </summary>
        Activation = 1 << 1,

        /// <summary> Impact as a result of explosion </summary>
        Explosion = 1 << 2,

        /// <summary> Impacted with a hammer booster </summary>
        Hammer = 1 << 3,

        /// <summary> Impacted by magic hat </summary>
        MagicHat = 1 << 4,

        /// <summary> Replacement of chip with another chip (e.g., when magic hat is flying to the field and replacing one of the chips) </summary>
        Replace = 1 << 5,

        /// <summary> Chip wayout consumes chip </summary>
        WayoutConsume = 1 << 6,

        /// <summary> Impacted because of neighbour chip consumption </summary>
        Neighbour = 1 << 7,

        /// <summary> The only way to consume infinite gumball machine </summary>
        GumballMachineConsume = 1 << 8,

        FireworkMissile = 1 << 9,
        Car = 1 << 10,
        HiddenObjectRelease = 1 << 11,

        /// <summary> Impact type dealt by the bonus to the target cell (that might has chip into itself) </summary>
        BonusReplace = 1 << 12,

        /// <summary> Impact type dealt to the chip to activate during bonus sequence </summary>
        BonusActivation = 1 << 13,

        /// <summary> Impact type dealt to the mantle stub chip during mantle consuming </summary>
        MantleStubConsume = 1 << 14,

        /// <summary> Impact type dealt to magic hat to activate it on swap with match </summary>
        MagicHatActivation = 1 << 15,

        /// <summary> Impact type dealt to magic hat to activate it by double magic hat </summary>
        DoubleMagicHatActivation = 1 << 16,

        All = int.MaxValue
    }
}