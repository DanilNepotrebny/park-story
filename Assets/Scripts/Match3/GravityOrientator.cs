﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Editor only component for rotating view based on the cell gravity.
    /// </summary>
    public class GravityOrientator : BaseGravityEditorHelper
    {
        #if UNITY_EDITOR

        protected override void UpdateView(Direction gravity)
        {
            transform.rotation = Quaternion.FromToRotation(Vector3.down, (Vector2)gravity.GetOffset());
        }

        #endif
    }
}