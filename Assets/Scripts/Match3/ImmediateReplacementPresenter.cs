﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(ChipPresenter))]
    public class ImmediateReplacementPresenter : MonoBehaviour, IImpactController
    {
        [SerializeField] private string _replacingParamName = "OnImmediateReplace";

        private const ImpactFlag _impactType = ImpactFlag.Replace;
        private ChipPresenter _chipPresenter;

        protected void Awake()
        {
            _chipPresenter = GetComponent<ChipPresenter>();
            _chipPresenter.ChipComponent.ChipReplacing += OnChipReplacing;
            _chipPresenter.ChipComponent.AddImpactController(this);
        }

        protected void OnDestroy()
        {
            _chipPresenter.ChipComponent.RemoveImpactController(this);
            _chipPresenter.ChipComponent.ChipReplacing -= OnChipReplacing;
        }

        private void OnChipReplacing(ImpactFlag replacementimpact)
        {
            if (replacementimpact == _impactType)
            {
                _chipPresenter.SetTrigger(_replacingParamName);
            }
        }

        #region IImpactController

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.Replacement;
        }

        void IImpactController.OnImpact(
            ChipComponent chip,
            ImpactFlag impactType,
            IDeferredInvocationHandle impactEndHandle,
            ref bool isModal)
        {
            if (impactType != _impactType)
            {
                return;
            }

            isModal = true;
            chip.Presenter.gameObject.SetActive(false);
        }

        void IImpactController.Cancel()
        {
            // nothing to do
        }

        #endregion IImpactController
    }
}
