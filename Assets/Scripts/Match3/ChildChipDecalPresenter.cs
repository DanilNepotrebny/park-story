﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.ChipTags;
using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Decal that can be activated when chip is placed into some container (into jam, for example).
    /// See <see cref="ChildChipDecalController"/> for container part.
    /// </summary>
    public class ChildChipDecalPresenter : MonoBehaviour
    {
        [SerializeField] private ChipTag _containerTag;

        public bool IsCompatible(ChipComponent container)
        {
            return container.Tags.Has(_containerTag);
        }

        public void SetActive(bool isActive)
        {
            gameObject.SetActive(isActive);
        }
    }
}