﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Controls impact process of the chip. Can be used both for logics and animations.
    /// </summary>
    public interface IImpactController
    {
        /// <summary>
        /// Gets priority of this impact controller.
        /// </summary>
        ImpactPriority GetPriority();

        void Cancel();

        /// <summary>
        /// Called when the impact process is started.
        /// </summary>
        /// <param name="chip">Chip which is impacted</param>
        /// <param name="impactEndHandle">Handle of impact end invocation, that can be deferred</param>
        /// <param name="isModal">Set this to true if next impact controllers should not be called </param>
        void OnImpact(
                [NotNull] ChipComponent chip,
                ImpactFlag impactType,
                [NotNull] IDeferredInvocationHandle impactEndHandle,
                ref bool isModal
            );
    }
}