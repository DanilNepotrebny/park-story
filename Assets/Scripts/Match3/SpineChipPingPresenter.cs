﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Spine.Unity;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Plays random animation from list on cell tapped
    /// </summary>
    [RequireComponent(typeof(SkeletonAnimation))]
    public class SpineChipPingPresenter : ChipPingPresenter
    {
        [SerializeField] private List<AnimationDelayPair> _animations = new List<AnimationDelayPair>();

        private string _idleAnimation;
        private SkeletonAnimation _skeleton;

        private SpineChipPartsPresenter _partsPresenter => GetComponentInParent<SpineChipPartsPresenter>();

        public void ClearPingAnimations()
        {
            _animations.Clear();
        }

        public void AddPingAnimation(string animationName)
        {
            _animations.Add(new AnimationDelayPair(animationName, 0));
        }

        public void SetIdleAnimation(string animationName)
        {
            _idleAnimation = animationName;
        }

        protected void Awake()
        {
            _skeleton = GetComponent<SkeletonAnimation>();
            _idleAnimation = _skeleton.AnimationName;
        }

        protected override void PlayPingAnim(CellComponent cell)
        {
            int randomIndex = Random.Range(0, _animations.Count);
            AnimationDelayPair _animationData = _animations[randomIndex];
            StartCoroutine(PlayAnimation(_animationData.Animation, _animationData.Delay));
        }

        private IEnumerator PlayAnimation(string animationName, float delay)
        {
            yield return new WaitForSeconds(delay);

            string current = _skeleton.state.GetCurrent(0).Animation.Name;
            if (_idleAnimation != current && _animations.All(pair => pair.Animation != current))
            {
                yield break;
            }

            _partsPresenter?.LockIdleAnimation();
            _skeleton.state.SetAnimation(0, animationName, false).
                Complete += (track) =>
                {
                    _skeleton.state.SetAnimation(0, _idleAnimation, false);
                    _partsPresenter?.UnlockIdleAnimation();
                };
        }

        [Serializable]
        private struct AnimationDelayPair
        {
            [SerializeField, SpineAnimation] private string _animation;
            [SerializeField] private float _delay;

            public string Animation => _animation;
            public float Delay => _delay;

            public AnimationDelayPair(string animation, float delay)
            {
                _animation = animation;
                _delay = delay;
            }
        }
    }
}