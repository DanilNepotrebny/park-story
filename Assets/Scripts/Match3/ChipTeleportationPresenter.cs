﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(ChipPresenter))]
    public class ChipTeleportationPresenter : BaseChipMovementPresenter, IFieldDestabilizer
    {
        [SerializeField] private string _teleportationTriggerName = "OnTeleportation";
        [SerializeField] private string _teleportingEndTriggerName = "OnTeleportingEnd";
        [SerializeField] private string _teleportExitStateName = "TeleportExit";
        [SerializeField] private string _teleportationTypeParamName = "TeleportationType";

        private Action _teleportationFinishCallback;
        private DeferredInvocation _teleportationEndDI;

        public override ChipComponent.MovingType MovingType => ChipComponent.MovingType.Teleportation;

        #region IFieldDestabilizer

        public event Action Stabilized;
        public event Action Destabilized;
        public bool IsStable { get; private set; }

        #endregion IFieldDestabilizer

        public event TeleportationInAction TeleportingIn;
        public event TeleportationOutAction TeleportingOut;

        public void SetTeleportationAnimationType(TeleportationAnimationType type)
        {
            ChipPresenter.SetFloat(_teleportationTypeParamName, (float)type);
        }

        public void SetAnimationDirection(Direction animationDirection)
        {
            ChipPresenter.SetAnimationDirection(animationDirection);
        }

        protected override void Awake()
        {
            base.Awake();

            ChipPresenter.AddAnimationStateEnterHandler(_teleportExitStateName, OnTeleportFromStateEntered);
            ChipPresenter.AddAnimationStateExitHandler(_teleportExitStateName, OnTeleportFromStateExited);

            _teleportationEndDI = new DeferredInvocation(() => ChipPresenter.SetTrigger(_teleportingEndTriggerName));
        }

        protected override void PrepareMovement()
        {
            IsStable = false;
            Destabilized?.Invoke();
        }

        protected override void StartMovement(Action finishCallback)
        {
            IDeferredInvocationHandle teleportationEndHandle = _teleportationEndDI.Start();
            TeleportingIn?.Invoke(this, teleportationEndHandle);
            teleportationEndHandle.Unlock();

            ChipPresenter.SetTrigger(_teleportationTriggerName);

            _teleportationFinishCallback = finishCallback;
        }

        protected override void FinishMovement()
        {
            IsStable = true;
            Stabilized?.Invoke();
        }

        private void OnTeleportFromStateEntered()
        {
            AssertTeleportationStarted();

            ChipPresenter.transform.position = Chip.transform.position;

            TeleportingOut?.Invoke(this);
        }

        private void OnTeleportFromStateExited()
        {
            AssertTeleportationStarted();

            _teleportationFinishCallback?.Invoke();
        }

        private void AssertTeleportationStarted()
        {
            Assert.IsNotNull(_teleportationFinishCallback, "Teleportation animation is in progress but teleptortation moving.");
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }

        #region Inner types

        public delegate void TeleportationInAction(
            ChipTeleportationPresenter presenter,
            IDeferredInvocationHandle teleportationEndHandle);

        public delegate void TeleportationOutAction(ChipTeleportationPresenter presenter);

        public enum TeleportationAnimationType
        {
            Teleport = 0,
            Pipe = 1,
            Conveyor = 2
        }

        #endregion
    }
}