﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.MoveWeight;
using DreamTeam.Match3.Requirements;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using ModestTree;
using UnityEngine;
using Zenject;
using Assert = UnityEngine.Assertions.Assert;

namespace DreamTeam.Match3
{
    /// <summary> Encapsulates common functionality of a match-3 chip. </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(HealthController), typeof(ChipTagsComponent), typeof(ChipMoveWeightCriterion))]
    public class ChipComponent : MonoBehaviour, IFieldDestabilizer
    {
        [SerializeField] private ChipPresenter _view;
        [SerializeField] private ChipTagsComponent _chipTags;
        [SerializeField] private ChipComponentSettings _settings;

        [Inject] private Instantiator _instantiator;
        [Inject] private ImpactSystem _impactSystem;
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;
        [InjectOptional] private ChipContainerComponent _childContainer;

        private bool _isViewDetached;
        private bool _isTagsComponentInitialized;

        private HealthController _healthController;
        private CellComponent _cell;
        private CellComponent _containerCell;
        private ImpactFlag _impactFlags;

        private DeferredInvocation _impactEnded;

        private readonly SortedSet<IImpactController> _impactControllers =
            new SortedSet<IImpactController>(new ImpactControllerComparer());

        private MovingType _movingType;
        private int _movingCounter;
        private DeferredInvocation _movingStopped;

        /// <summary> 
        /// Cell that contains this chip. If chip is contained inside another chip in this cell this property will
        /// no be null.
        /// </summary>
        public CellComponent Cell
        {
            get { return _cell; }
            private set
            {
                if (_cell == value)
                {
                    return;
                }

                var oldCell = _cell;
                _cell = value;

                OnCellChanged(oldCell);
            }
        }

        /// <summary>
        /// Cell that this chip is attached to. If chip is contained inside another chip in this cell this property 
        /// will be null.
        /// </summary>
        public CellComponent ContainerCell
        {
            get { return _containerCell; }
            private set
            {
                if (_containerCell == value)
                {
                    return;
                }

                CellComponent oldCell = _containerCell;
                _containerCell = value;

                ContainerCellChanged?.Invoke(_containerCell, oldCell);
            }
        }

        public ChipContainerComponent ParentContainer { get; private set; }

        public ChipPresenter Presenter => _view;

        public ChipMoveWeightCriterion MoveWeightCriterion { get; private set; }

        public HealthController HealthController =>
            _healthController ?? (_healthController = GetComponent<HealthController>());

        public int Health => HealthController.Value;

        public float BaseMoveWeight => _settings.BaseMoveWeight;

        public float RequiredMoveWeight => _settings.RequiredMoveWeight;

        public RequirementHelper Helper => _settings.Helper;

        /// <summary> Returns chip contained inside this chip container. If this chip doesn't have a container returns null. </summary>
        public virtual ChipComponent Child => _childContainer?.Chip;

        public bool IsChildVisible => _childContainer?.IsChipVisible ?? false;

        /// <summary> Is this chip consumed and will be removed from field. </summary>
        public virtual bool IsConsumed => Health == 0;

        /// <summary> Is this chip consumed and has no alive inner chips. </summary>
        public bool IsTotallyConsumed => CalculateTotalHealth() == 0;

        /// <summary> Is this chip is moving at the moment. </summary>
        public virtual bool IsMoving => _movingCounter > 0;

        public bool IsInMovingState => _movingStopped.IsLocked;

        /// <summary> Can this chip be swapped by user </summary>
        public virtual bool CanBeSwapped => _settings.CanBeSwapped;

        public bool CanFall => _settings.CanFall;

        /// <summary> Can this chip be shuffled </summary>
        public bool CanBeShuffled => _settings.CanBeShuffled;

        public bool IsStable => !IsMoving;

        public ChipTagsComponent Tags
        {
            get
            {
                // Initialize lazily because can be accessed from prefab.
                if (!_isTagsComponentInitialized)
                {
                    _chipTags.AddRange(_settings.ChipTagSet);
                    _isTagsComponentInitialized = true;
                }

                return _chipTags;
            }
        }

        private ChipComponent _parentChip => ParentContainer?.GetComponent<ChipComponent>();

        public event Action Stabilized;
        public event Action Destabilized;

        public event Action<MovingType, IDeferredInvocationHandle> Moving;
        public event Action<MovingType> Moved;

        public virtual event Action<ChipComponent> Impacted;

        public event Action<ChipComponent, int> Impacting;

        public event CellChangedDelegate CellChanged;
        public event ContainerCellChangedDelegate ContainerCellChanged;
        public event ChildChangedDelegate ChildChanged;
        public event ViewDetachedDelegate ViewDetached;
        public event ViewAttachedDelegate ViewAttached;
        public event ChipReplacingDelegate ChipReplacing;

        public void AddImpactFlags(ImpactFlag flags)
        {
            _impactFlags |= flags;
        }

        public void RemoveImpactFlags(ImpactFlag flags)
        {
            _impactFlags &= ~flags;
        }

        /// <summary>
        /// Returns true if chip can recieve impact of specified type.
        /// </summary>
        public virtual bool CanBeImpacted(ImpactFlag flag)
        {
            return (_impactFlags & flag) > 0;
        }

        /// <summary>
        /// Does impact on this chip's health. If health becomes zero chip enters consuming state.
        /// Pass zero as <paramref name="delta"/> to just play impact animation.
        /// If chip is immune to type of impact specified with impact flag this method will do nothing.
        /// <b>Should be called only from <see cref="CellComponent" />.</b>
        /// </summary>
        /// <returns>True if chip has absorbed the impact.</returns>
        public bool OnCellImpacting(int delta, ImpactFlag flag)
        {
            bool canBeImpacted = CanBeImpacted(flag);
            if (!IsConsumed && !canBeImpacted)
            {
                return false;
            }

            ChipComponent childBeforeImpact = Child;

            if (!IsConsumed && canBeImpacted)
            {
                if (_impactEnded.IsLocked)
                {
                    _impactControllers.ForEach(c => c.Cancel());
                    Assert.IsFalse(_impactEnded.IsLocked, "Some of impact controllers didn't cancel impact invocation lock.");
                }

                int prevHealth = Health;
                delta = HealthController.OnHealthImpacting(delta, flag);

                Cell?.BeginProcess(CellComponent.ProcessType.Impacting);

                int impactValue = prevHealth - Health + Math.Abs(delta);
                Impacting?.Invoke(this, impactValue);

                using (var impactEndHandle = _impactEnded.Start())
                {
                    foreach (IImpactController controller in _impactControllers)
                    {
                        if (InvokeImpactController(controller, flag, impactEndHandle))
                        {
                            break;
                        }
                    }
                }
            }

            if (delta != 0 && childBeforeImpact != null)
            {
                return childBeforeImpact.OnCellImpacting(delta, flag);
            }

            return IsImpactAbsorptionPossible(flag);
        }

        /// <summary>
        /// Is any received impact with impact 'flag' will be absorbed by this chip
        /// </summary>
        public bool IsImpactAbsorptionPossible(ImpactFlag flag)
        {
            bool canBeImpacted = CanBeImpacted(flag);
            return !IsConsumed && canBeImpacted;
        }

        /// <summary>
        /// Schedule special impact like activation of special mechanics.
        /// </summary>
        public Impact ScheduleActivation()
        {
            Assert.IsTrue(CanBeImpacted(ImpactFlag.Activation));
            return ScheduleConsumption(ImpactFlag.Activation);
        }

        /// <summary>
        /// Schedules such impact to this chip that it will be consumed.
        /// </summary>
        public Impact ScheduleConsumption(ImpactFlag flag)
        {
            return _impactSystem.ScheduleImpact(Cell, -Health, flag);
        }

        public void ConsumeSeparately(ImpactFlag flag)
        {
            ParentContainer.Chip = null;

            OnCellImpacting(-Health, flag);
        }

        public void HandleChipReplacing(ImpactFlag impactType)
        {
            ChipReplacing?.Invoke(impactType);
        }

        /// <summary>
        /// Adds impact controller.
        /// </summary>
        public void AddImpactController(IImpactController controller)
        {
            _impactControllers.Add(controller);
        }

        /// <summary>
        /// Removes impact controller.
        /// </summary>
        public void RemoveImpactController(IImpactController controller)
        {
            _impactControllers.Remove(controller);
        }

        /// <summary>
        /// Continues execution of impact controllers from the next controller after specified one.
        /// </summary>
        public void ExecuteNextImpactControllers(
            [NotNull] IImpactController controller,
            ImpactFlag impactType,
            [NotNull] IDeferredInvocationHandle impactEndHandle)
        {
            bool isFound = false;
            using (var enumerator = _impactControllers.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    IImpactController current = enumerator.Current;

                    if (current == controller)
                    {
                        isFound = true;
                        continue;
                    }

                    if (isFound)
                    {
                        if (InvokeImpactController(current, impactType, impactEndHandle))
                        {
                            break;
                        }
                    }
                }
            }

            if (!isFound)
            {
                throw new InvalidOperationException("Can't execute next impact controller: input impact controller is not assigned on chip.");
            }
        }

        /// <summary>
        /// Detaches chip view from chip game object.
        /// </summary>
        public ChipPresenter DetachView()
        {
            if (_isViewDetached)
            {
                UnityEngine.Debug.LogWarning(
                    $"Detaching view that was already detached from {name} in {Cell?.name}", this);
                return null;
            }

            ChipPresenter view = _view;
            view.transform.SetParent(null);

            // detached view might be re-parented somewhere next, so would be better to disable ViewParentController
            var viewParentController = GetComponent<ViewParentController>();
            if (viewParentController != null)
            {
                viewParentController.enabled = false;
            }

            _isViewDetached = true;
            _view = null;

            ViewDetached?.Invoke(this, view);

            return view;
        }

        public bool AttachView([NotNull] ChipPresenter presenter)
        {
            if (!_isViewDetached)
            {
                UnityEngine.Debug.LogError(
                    $"Attempt to attach view to the {name} with already attached view in {Cell?.name}", this);
                return false;
            }

            Assert.IsTrue(presenter.ChipComponent == this, $"Attaching wrong {nameof(ChipPresenter)}");
            presenter.transform.SetParent(transform);
            presenter.ResetPosition();
            _view = presenter;

            var viewParentController = GetComponent<ViewParentController>();
            if (viewParentController != null)
            {
                viewParentController.enabled = true;
            }

            _isViewDetached = false;

            ViewAttached?.Invoke(this);

            return true;
        }

        /// <summary>
        /// Clones view GameObject
        /// </summary>
        public ChipPresenter CloneView()
        {
            if (_view == null)
            {
                return null;
            }

            ChipPresenter view = _instantiator.Instantiate(_view, true);

            // We better return enabled object so re-enable cloned view
            view.gameObject.SetActive(true);

            return view;
        }

        /// <summary>
        /// Calculates total chip health including all inner chips.
        /// </summary>
        public int CalculateTotalHealth()
        {
            int totalHealth = Health;

            ChipComponent innerChip = GetComponent<ChipContainerComponent>()?.Chip;
            if (innerChip != null)
            {
                totalHealth += innerChip.CalculateTotalHealth();
            }

            return totalHealth;
        }

        /// <summary>
        /// Calculates will be chip totally consumed after impact of specified power.
        /// </summary>
        public bool WillBeTotallyConsumed(int impact)
        {
            return CalculateTotalHealth() + impact <= 0;
        }

        /// <summary>
        /// Returns true if this chip will soon be consumed based on prediction by impacts scheduled on the cell.
        /// </summary>
        public bool WillBeConsumed()
        {
            return CalculateScheduledImpact() + Health <= 0;
        }

        /// <summary>
        /// Returns true if this chip will soon be impacted based on prediction by impacts scheduled on the cell.
        /// </summary>
        public bool WillBeImpacted()
        {
            return CalculateScheduledImpact() < 0;
        }

        public void KillSilently()
        {
            ParentContainer.DeleteChipSilently();
            gameObject.SetActive(false);
            gameObject.Dispose();
        }

        public void MoveTo(ChipContainerComponent container, MovingType type)
        {
            StartMoving(type);
            transform.position = container.transform.position;

            var handle = _movingStopped.Start();
            Moving?.Invoke(type, handle);
            handle.Unlock();
        }

        public void MoveToSilently(ChipContainerComponent container)
        {
            transform.position = container.transform.position;
        }

        public void StartMoving(MovingType type)
        {
            Assert.IsFalse(IsInMovingState, "Start moving of already moving chip is prohibited");

            MovingType oldType = _movingType;
            _movingType = type;

            if (IsMoving && _movingType != oldType)
            {
                Moved?.Invoke(oldType);
            }

            _movingCounter++;

            if (_movingCounter == 1)
            {
                Destabilized?.Invoke();
            }
        }

        public void EndMoving()
        {
            _movingCounter--;

            UnityEngine.Debug.Assert(_movingCounter >= 0, "Moving counter is negative.", this);

            if (_movingCounter == 0)
            {
                Cell?.OnChipMovingFinished(_movingType);
                Moved?.Invoke(_movingType);
                
                Stabilized?.Invoke();
            }
        }

        public void DestroyChip()
        {
            if (ParentContainer != null)
            {
                ParentContainer.Chip = null;
            }

            gameObject.Dispose();
        }

        protected void Awake()
        {
            Assert.IsNotNull(_settings);
            _impactFlags = _settings.ImpactFlags;

            MoveWeightCriterion = GetComponent<ChipMoveWeightCriterion>();
            _impactEnded = new DeferredInvocation(OnImpactEnded);
            _movingStopped = new DeferredInvocation(OnMovingStopped);

            if (_childContainer != null)
            {
                _childContainer.ChipChanged += OnChildChanged;
            }

            OnTransformParentChanged();
            
            _fieldStabilizationSystem.AddDestabilizer(this);
        }

        protected void Start()
        {
            if (Presenter != null)
            {
                Presenter.DestroyNotifier.Destroying += OnViewDestroying;
            }
        }

        protected void OnDestroy()
        {
            _fieldStabilizationSystem.RemoveDestabilizer(this);

            if (_view != null && !_isViewDetached)
            {
                _view.gameObject.Dispose();
                _view = null;
            }
        }

        protected void OnTransformParentChanged()
        {
            // TODO: Remove dependencies from transform

            Cell = this.GetComponentInParent<CellComponent>(true);

            var newContainer = transform.parent?.GetComponent<ChipContainerComponent>();
            if (ParentContainer != newContainer)
            {
                ParentContainer = newContainer;

                ContainerCell = Cell?.Chip == this ? Cell : null;
            }
        }

        private void OnViewDestroying(GameObject viewGO)
        {
            _view = null;
        }

        private bool InvokeImpactController(
            [NotNull] IImpactController controller,
            ImpactFlag impactType,
            [NotNull] IDeferredInvocationHandle impactEndHandle)
        {
            bool isModal = false;
            controller.OnImpact(this, impactType, impactEndHandle, ref isModal);
            return isModal;
        }

        private void OnCellChanged(CellComponent oldCell)
        {
            if (_impactEnded.IsLocked)
            {
                oldCell?.EndProcess(CellComponent.ProcessType.Impacting);
                Cell?.BeginProcess(CellComponent.ProcessType.Impacting);
            }

            CellChanged?.Invoke(this, oldCell);
        }

        private void OnImpactEnded()
        {
            var previousCell = Cell;

            Impacted?.Invoke(this);

            if (IsConsumed)
            {
                if (ParentContainer != null)
                {
                    ParentContainer.Chip = Child;
                    ParentContainer = null;
                }

                DestroyChip();
            }

            previousCell?.EndProcess(CellComponent.ProcessType.Impacting);
        }

        private void OnMovingStopped()
        {
            Cell?.OnChipMovingStopped(_movingType);

            EndMoving();
        }

        private void OnChildChanged(ChipComponent oldChip, ChipComponent chip)
        {
            ChildChanged?.Invoke(oldChip, chip);
        }

        private ChipComponent FindRootChip()
        {
            return _parentChip != null ? _parentChip.FindRootChip() : this;
        }

        private int CalculateScheduledImpact()
        {
            if (Cell == null)
            {
                return 0;
            }

            ChipComponent chip = FindRootChip();
            int impactOnThis = 0;

            // Iterate all scheduled impacts and apply them in order. This is not accurate
            // because impacts are in order of addition but not in order of actual performance.
            using (IEnumerator<Impact.IData> impacts = Cell.ScheduledImpacts.GetEnumerator())
            {
                while (impacts.MoveNext())
                {
                    Impact.IData impact = impacts.Current;
                    Assert.IsNotNull(impact);
                    int impactValue = impact.Value;

                    do
                    {
                        Assert.IsNotNull(chip);
                        if (!chip.CanBeImpacted(impact.Flag))
                        {
                            break;
                        }

                        if (chip == this)
                        {
                            impactOnThis += impactValue;
                            break;
                        }

                        impactValue += chip.Health;
                        if (impactValue <= 0)
                        {
                            chip = chip.Child;
                        }
                        else
                        {
                            break;
                        }
                    }
                    while (chip != null);
                }
            }

            return impactOnThis;
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return Cell != null ? Cell.name : "Out of cell";
        }

        #region Inner types

        public enum MovingType
        {
            Immediate,
            Falling,
            Swapping,
            Teleportation,
            ConveyorTransportation
        }

        public delegate void ChipReplacingDelegate(ImpactFlag replacementImpact);

        public delegate void CellChangedDelegate([NotNull] ChipComponent chip, [CanBeNull] CellComponent prevCell);

        public delegate void ChildChangedDelegate(ChipComponent prevChild, ChipComponent child);

        public delegate void ViewDetachedDelegate([NotNull] ChipComponent chip, [NotNull] ChipPresenter detachedView);

        public delegate void ViewAttachedDelegate([NotNull] ChipComponent chip);

        public delegate void ContainerCellChangedDelegate(
                [CanBeNull] CellComponent newCell,
                [CanBeNull] CellComponent oldCell
            );

        private class ImpactControllerComparer : IComparer<IImpactController>
        {
            public int Compare(IImpactController x, IImpactController y)
            {
                int priorityX = (int)x.GetPriority();
                int priorityY = (int)y.GetPriority();

                return priorityY.CompareTo(priorityX);
            }
        }

        #endregion Inner types
    }
}