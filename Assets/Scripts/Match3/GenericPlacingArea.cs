﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using ReadOnly = DreamTeam.Utils.ReadOnlyAttribute;

namespace DreamTeam.Match3
{
    public partial class GenericPlacingArea<T> : BasePlacingArea
        where T : IPlaceable
    {
        [SerializeField, HideInInspector]
        private List<Object> _elements = new List<Object>();

        [SerializeField, HideInInspector]
        private List<Vector2Int> _cellPositions = new List<Vector2Int>();

        [Inject] private GridSystem _grid;
        [Inject(Id = Match3Installer.RandomId)] private CustomRandom _random;

        public List<IPlaceable> Elements => _elements.ConvertAll(obj => obj as IPlaceable);

        public int Square => _cellPositions.Count;

        public bool Expand(Vector2Int cellPos)
        {
            if (Contains(cellPos))
            {
                return false;
            }

            _cellPositions.Add(cellPos);
            return true;
        }

        public bool Shrink(Vector2Int cellPos)
        {
            if (!Contains(cellPos))
            {
                return false;
            }

            return _cellPositions.Remove(cellPos);
        }

        public bool Contains(Vector2Int cellPos)
        {
            return _cellPositions.Contains(cellPos);
        }

        protected void Start()
        {
            var placeables = new List<IPlaceable>(Elements);
            placeables.Sort((left, right) => right.Square - left.Square); // bigger first

            var shuffledCellPos = new List<Vector2Int>(_cellPositions);
            shuffledCellPos.RandomShuffle(_random);

            bool isPlaced = false;
            foreach (Vector2Int cell in shuffledCellPos)
            {
                isPlaced = TryPlaceElement(cell, shuffledCellPos, placeables);
                if (isPlaced)
                {
                    break;
                }
            }

            if (!isPlaced)
            {
                UnityEngine.Debug.Log($"Placing area didn't find any variation to place. Using editor setup.");
            }
        }

        private bool TryPlaceElement(Vector2Int pos, List<Vector2Int> cells, List<IPlaceable> placeables)
        {
            Assert.IsTrue(cells.Contains(pos));

            IPlaceable placeable = placeables.First();

            // randomize rotation
            bool performRotation = _random.Next(2) == 1;
            if (performRotation)
            {
                placeable.Rotate();
            }

            if (!CanBePlaced(placeable, pos, cells))
            {
                if (performRotation)
                {
                    performRotation = false;
                    placeable.Rotate(); // rotate back

                    if (!CanBePlaced(placeable, pos, cells))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            var newBound = new RectInt(pos, placeable.Bounds.size);

            var newPlaceables = new List<IPlaceable>(placeables);
            newPlaceables.Remove(placeable);

            bool isPlacing = false;
            if (newPlaceables.Count == 0)
            {
                isPlacing = true;
            }
            else
            {
                var newCells = new List<Vector2Int>(cells);
                foreach (Vector2Int point in newBound.allPositionsWithin)
                {
                    newCells.Remove(point);
                }
                newCells.RandomShuffle(_random);

                foreach (Vector2Int newCell in newCells)
                {
                    if (TryPlaceElement(newCell, newCells, newPlaceables))
                    {
                        isPlacing = true;
                        break;
                    }
                }
            }

            if (isPlacing)
            {
                placeable.Place(pos);
                return true;
            }

            if (performRotation)
            {
                placeable.Rotate(); // rotate back
            }

            return false;
        }

        private bool CanBePlaced([NotNull] IPlaceable placeable, Vector2Int pos, [NotNull] List<Vector2Int> cells)
        {
            var newBounds = new RectInt(pos, placeable.Bounds.size);
            foreach (Vector2Int point in newBounds.allPositionsWithin)
            {
                if (!cells.Contains(point))
                {
                    return false;
                }
            }

            return true;
        }
    }

    public abstract class BasePlacingArea : MonoBehaviour
    {
        protected static readonly List<Color> PickedGizmoColors = new List<Color>();
    }
}
