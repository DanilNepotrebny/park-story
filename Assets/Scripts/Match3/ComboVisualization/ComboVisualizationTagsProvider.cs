﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.ComboVisualization
{
    [RequireComponent(typeof(ChipTagsComponent))]
    public class ComboVisualizationTagsProvider : MonoBehaviour
    {
        [SerializeField, RequiredField] private List<ComboVisualizationTag> _tags = new List<ComboVisualizationTag>();

        public IReadOnlyCollection<ComboVisualizationTag> Tags => _tags.AsReadOnly();
    }
}