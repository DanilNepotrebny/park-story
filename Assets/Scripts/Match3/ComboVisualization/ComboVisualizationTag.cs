﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Match3.ComboVisualization
{
    [CreateAssetMenu(menuName = "Match 3/Combo visualization tag")]
    public class ComboVisualizationTag : ScriptableObject
    {
        [SerializeField] private int _minComboPower = 1;

        public int MinComboPower => _minComboPower;

        protected void OnValidate()
        {
            _minComboPower = Mathf.Max(_minComboPower, 1);
        }
    }
}