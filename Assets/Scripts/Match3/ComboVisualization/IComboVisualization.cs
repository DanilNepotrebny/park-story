﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Match3.ComboVisualization
{
    public interface IComboVisualization
    {
        int ComboPower { get; }
        Bounds CurrentBounds { get; }
        Bounds GetBoundsWithCenter(Vector3 center);
        void Play(Vector3 wshowPosition, int comboPower, Action onPlayedCallback);
    }
}