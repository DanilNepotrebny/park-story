﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace DreamTeam.Match3.ComboVisualization
{
    public partial class ComboVisualizationSystem : MonoBehaviour
    {
        [SerializeField] private PresentersMap _presentersMap;

        [Inject] private GridSystem _grid;
        [Inject] private MovesSystem _movesSystem;
        [Inject] private Instantiator _instantiator;

        private Dictionary<ComboVisualizationTag, PresenterLauncher> _launchersMap =
            new Dictionary<ComboVisualizationTag, PresenterLauncher>();

        protected void Awake()
        {
            _movesSystem.MoveSucceed += OnMovesSucceed;
        }

        protected void OnDestroy()
        {
            _movesSystem.MoveSucceed -= OnMovesSucceed;
        }

        protected void OnValidate()
        {
            _presentersMap.OnValidate();
        }

        protected void OnDrawGizmosSelected()
        {
            GridSystem grid = FindObjectOfType<GridSystem>();
            if (grid != null)
            {
                Bounds safeZone = CalculateSafeZone(grid);
                Gizmos.color = Color.blue;
                Gizmos.DrawWireCube(safeZone.center, safeZone.size);
            }
        }

        private Bounds CalculateSafeZone(GridSystem grid)
        {
            Bounds result = grid.GetWorldBounds();

            result.min -= new Vector3(grid.CellWidth, grid.CellHeight, 0) * 0.5f;
            result.max += new Vector3(grid.CellWidth, grid.CellHeight, 0) * 0.5f;

            return result;
        }

        private void OnMovesSucceed(List<Vector2Int> cellPositions)
        {
            _launchersMap.Clear();

            foreach (CellComponent cell in _grid.Cells)
            {
                ComboVisualizationTagsProvider provider = GetVisualizationProvider(cell);
                if (provider != null)
                {
                    AddOrCreatePresenterLauncher(provider);
                    cell.Chip.Impacted += OnChipImpacted;
                }
            }
        }

        private void AddOrCreatePresenterLauncher(ComboVisualizationTagsProvider provider)
        {
            foreach (ComboVisualizationTag comboTag in provider.Tags)
            {
                PresenterLauncher launcher;
                if (!_launchersMap.TryGetValue(comboTag, out launcher))
                {
                    launcher = new PresenterLauncher(
                            _presentersMap[comboTag],
                            _grid,
                            CalculateSafeZone(_grid),
                            _instantiator,
                            comboTag.MinComboPower
                        );
                    _launchersMap.Add(comboTag, launcher);
                }

                launcher.ProvidersCount++;
                launcher.IncrementPower();
            }
        }

        private ComboVisualizationTagsProvider GetVisualizationProvider(CellComponent cell)
        {
            if (!cell.IsEmpty && cell.Chip.WillBeConsumed())
            {
                return cell.Chip.GetComponent<ComboVisualizationTagsProvider>();
            }
            return null;
        }

        private void OnChipImpacted(ChipComponent chip)
        {
            chip.Impacted -= OnChipImpacted;
            ComboVisualizationTagsProvider provider = chip.GetComponent<ComboVisualizationTagsProvider>();

            foreach (ComboVisualizationTag comboTag in provider.Tags)
            {
                PresenterLauncher launcher;
                if (_launchersMap.TryGetValue(comboTag, out launcher))
                {
                    launcher.ProvidersCount--;
                    launcher.SetShowPosition(chip.transform.position);

                    if (launcher.ProvidersCount <= 1)
                    {
                        _launchersMap.Remove(comboTag);
                        launcher.Launch();
                    }
                }
            }
        }

        [Serializable]
        private struct PresentersMap
        {
            [SerializeField] private List<PresentersCollection> _presentersCollection;

            public IComboVisualization this[ComboVisualizationTag tag] =>
                _presentersCollection.Find(item => item.Tag == tag).RandomPresenter;

            public void OnValidate()
            {
                _presentersCollection.ForEach(item => item.OnValidate());
            }

            [Serializable]
            private struct PresentersCollection
            {
                [SerializeField, RequiredField] private ComboVisualizationTag _tag;
                [SerializeField, RequiredField] private List<GameObject> _presenters;

                public ComboVisualizationTag Tag => _tag;

                public IComboVisualization RandomPresenter =>
                    _presenters[Random.Range(0, _presenters.Count - 1)].GetComponent<IComboVisualization>();

                public void OnValidate()
                {
                    for (int index = 0; index < _presenters.Count; index++)
                    {
                        if (_presenters[index] != null &&
                            _presenters[index].GetComponent<IComboVisualization>() == null)
                        {
                            UnityEngine.Debug.LogAssertion(
                                    $"{_presenters[index].name} doesn't have any" +
                                    $"component implementing {nameof(IComboVisualization)}"
                                );
                            _presenters[index] = null;
                        }
                    }
                }
            }
        }
    }
}