﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.ComboVisualization
{
    public partial class ComboVisualizationSystem
    {
        private class PresenterLauncher
        {
            private static List<IComboVisualization> _playingPresenters = new List<IComboVisualization>();

            private Vector3? _showPosition;
            private Instantiator _instantiator;
            private GridSystem _grid;
            private IComboVisualization _presenter;
            private int _comboPower;
            private int _minComboPower;
            private Bounds _safeZone;
            public int ProvidersCount { get; set; }

            public PresenterLauncher(
                    IComboVisualization presenter,
                    GridSystem grid,
                    Bounds safeZone,
                    Instantiator instantiator,
                    int minComboPower
                )
            {
                _presenter = presenter;
                _grid = grid;
                _safeZone = safeZone;
                _instantiator = instantiator;
                _minComboPower = minComboPower;
            }

            public void SetShowPosition(Vector3 value)
            {
                if (_showPosition == null)
                {
                    _showPosition = value;
                }
            }

            public void Launch()
            {
                if (_comboPower >= _minComboPower)
                {
                    var presenterGO = _instantiator.Instantiate((_presenter as Component).gameObject);
                    var presenter = presenterGO.GetComponent<IComboVisualization>();

                    CalculateShowPosition(presenter);

                    presenter.Play(
                            (Vector3)_showPosition,
                            _comboPower,
                            () =>
                            {
                                _playingPresenters.Remove(presenter);
                                presenterGO.Dispose();
                            }
                        );
                    _playingPresenters.Add(presenter);
                }
            }

            public void IncrementPower()
            {
                _comboPower += _presenter.ComboPower;
            }

            private void CalculateShowPosition(IComboVisualization presenter)
            {
                _showPosition = presenter.GetBoundsWithCenter((Vector3)_showPosition).Incapsulate(_safeZone).center;

                _grid.ProcessCellsWhileConditionByPositions(
                        position =>
                        {
                            Bounds bounds = presenter
                                .GetBoundsWithCenter(_grid.GridToWorld(position))
                                .Incapsulate(_safeZone);

                            _showPosition = bounds.center;
                            return !IntersectsOtherPresenter(bounds);
                        },
                        _grid.WorldToGrid((Vector3)_showPosition)
                    );
            }

            private bool IntersectsOtherPresenter(Bounds bounds)
            {
                foreach (var presenter in _playingPresenters)
                {
                    if (bounds.Intersects(presenter.CurrentBounds))
                    {
                        return true;
                    }
                }
                return false;
            }
        }
    }
}