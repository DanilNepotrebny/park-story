﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.TransformLookup;
using DreamTeam.Utils;
using TMPro;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.ComboVisualization
{
    public class TextMeshProComboPresenter : MonoBehaviour, IComboVisualization
    {
        [SerializeField, ReadOnly, RequiredField] private TMP_Text _targetText;
        [SerializeField, ReadOnly, RequiredField] private Animation _animation;

        [SerializeField, Tooltip("{0} - combo power")] private string _textFormat = "Combox{0}";
        [SerializeField, RequiredField] private TransformLookupId _parentRectId;

        [Space]
        [SerializeField] private Vector2 _boundsSize;
        [SerializeField] private Vector2 _boundsShift;

        public int ComboPower => 1;

        public Bounds CurrentBounds => GetBoundsWithCenter(transform.position);

        [Inject]
        private void Construct(TransformLookupSystem _transformLookupSystem)
        {
            transform.SetParent(_transformLookupSystem.FindTransform(_parentRectId), false);
        }

        public Bounds GetBoundsWithCenter(Vector3 center)
        {
            return new Bounds(center + (Vector3)_boundsShift, (Vector3)_boundsSize);
        }

        public void Play(Vector3 showPosition, int comboPower, Action onPlayedCallback)
        {
            transform.position = showPosition;
            _targetText.text = string.Format(_textFormat, comboPower);
            StartCoroutine(PlayRoutine(onPlayedCallback));
        }

        protected void OnValidate()
        {
            if (_targetText == null)
            {
                _targetText = GetComponentInChildren<TextMeshProUGUI>();
            }

            if (_animation == null)
            {
                _animation = GetComponentInChildren<Animation>();
            }

            if (_targetText != null)
            {
                _targetText.text = string.Format(_textFormat, 0);
            }
        }

        protected void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(CurrentBounds.center, CurrentBounds.size);
        }

        private IEnumerator PlayRoutine(Action onEnd)
        {
            _animation.Play();
            yield return new WaitWhile(() => _animation.isPlaying);
            onEnd?.Invoke();
        }
    }
}