﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;

namespace DreamTeam.Match3
{
    public abstract class BaseGravityEditorHelper : EditorBehaviour
    {
        #if UNITY_EDITOR

        protected CellComponent.IEditorAPI CellEditorApi { get; private set; }

        protected abstract void UpdateView(Direction gravity);

        protected void OnEnable()
        {
            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                return;
            }

            InitCellEditorApi();
        }

        protected void OnDisable()
        {
            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                return;
            }

            UnsubscribeFromCellEvents();
        }

        protected void OnTransformParentChanged()
        {
            InitCellEditorApi();
            UpdateView();
        }

        private void SubscribeToCellEvents()
        {
            if (CellEditorApi != null)
            {
                CellEditorApi.GravityChanged += OnGravityChanged;
            }
        }

        private void UnsubscribeFromCellEvents()
        {
            if (CellEditorApi != null)
            {
                CellEditorApi.GravityChanged -= OnGravityChanged;
                CellEditorApi = null;
            }
        }

        private void OnGravityChanged()
        {
            UpdateView();
        }

        private void InitCellEditorApi()
        {
            UnsubscribeFromCellEvents();
            CellEditorApi = GetComponentInParent<CellComponent.IEditorAPI>();
            SubscribeToCellEvents();
        }

        private void UpdateView()
        {
            UpdateView(CellEditorApi?.Gravity ?? Direction.Down);
        }

        #endif
    }
}