﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Listener of the group impact. See <see cref="ImpactSystem"/> for additinal information.
    /// </summary>
    public interface IGroupImpactListener
    {
        /// <summary>
        /// Called when the cell is impacted only if this group hasn't been processed yet.
        /// </summary>
        /// <param name="cell">Impacted cell</param>
        /// <param name="chip">Impacted chip (or null)</param>
        /// <param name="flag">Impact flag</param>
        /// <returns>True if this group should be marked as processed for this listener</returns>
        bool OnGroupImpacting(CellComponent cell, ChipComponent chip, ImpactFlag flag, int groupId);

        /// <summary>
        /// Called when the cell is scheduled for impact only if this group hasn't been processed yet.
        /// </summary>
        /// <returns>True if this group should be marked as processed for this listener</returns>
        bool OnGroupImpactScheduled(CellComponent cell, ImpactFlag flag, int groupId);
    }

    /// <summary>
    /// Incapsulates cell impacting and makes possible of listening to group impact (impacting several cells at a time). Some chips or other entities
    /// can react to group impact in a special way (via implementation of <see cref="IGroupImpactListener"/>).
    /// </summary>
    public class ImpactSystem : MonoBehaviour
    {
        private readonly Dictionary<CellComponent, List<IGroupImpactListener>> _listeners =
            new Dictionary<CellComponent, List<IGroupImpactListener>>();

        private readonly List<ImpactGroup> _groups = new List<ImpactGroup>();
        private int _lastImpactId;

        /// <summary>
        /// Schedules group impact to the specified list of cells.
        /// Returns list of <see cref="Impact"/> (one for each cell) to control impact lifecycle.
        /// See <see cref="CellComponent.ScheduleSingleImpact"/> for additional information.
        /// </summary>
        /// <returns> List of scheduled impacts. Returns empty list when 'cells' argument is empty </returns>
        public List<Impact> ScheduleImpact(List<CellComponent> cells, int impact, ImpactFlag flag)
        {
            var result = new List<Impact>();
            if (cells.Count > 0)
            {
                int impactId = ++_lastImpactId;

                foreach (CellComponent cell in cells)
                {
                    result.Add(cell.ScheduleSingleImpact(impact, flag, impactId));
                }

                _groups.Add(new ImpactGroup(this, cells, impactId, flag));
            }

            return result;
        }

        /// <summary>
        /// Schedules group impact to one specified cell.
        /// Returns <see cref="Impact"/> to control impact lifecycle.
        /// See <see cref="CellComponent.ScheduleSingleImpact"/> for additional information.
        /// </summary>
        public Impact ScheduleImpact([NotNull] CellComponent cell, int impact, ImpactFlag flag)
        {
            List<Impact> impacts = ScheduleImpact(new List<CellComponent> { cell }, impact, flag);
            return impacts[0];
        }

        public void AddGroupImpactListener(IGroupImpactListener listener, CellComponent cell)
        {
            List<IGroupImpactListener> listeners;
            if (!_listeners.TryGetValue(cell, out listeners))
            {
                listeners = new List<IGroupImpactListener>();
                _listeners.Add(cell, listeners);
            }

            listeners.Add(listener);
        }

        public void RemoveGroupImpactListener(IGroupImpactListener listener, CellComponent cell)
        {
            List<IGroupImpactListener> listeners;
            if (_listeners.TryGetValue(cell, out listeners))
            {
                listeners.Remove(listener);
            }
        }

        private void RemoveGroup(ImpactGroup group)
        {
            _groups.Remove(group);
        }

        #region Inner types

        private class ImpactGroup
        {
            private List<IGroupImpactListener> _processedListeners = new List<IGroupImpactListener>();
            private int _lockCounter;
            private int _impactId;
            private ImpactSystem _system;

            public ImpactGroup(ImpactSystem system, List<CellComponent> cells, int impactId, ImpactFlag flag)
            {
                _lockCounter = cells.Count;
                _impactId = impactId;
                _system = system;

                var processed = new List<IGroupImpactListener>();

                foreach (CellComponent cell in cells)
                {
                    cell.Impacted += OnCellImpacted;
                    cell.ImpactCanceled += OnCellImpactCanceled;

                    ForEachListener(
                            cell,
                            l => l.OnGroupImpactScheduled(cell, flag, impactId),
                            processed
                        );
                }
            }

            private void OnCellImpacted(CellComponent cell, ChipComponent oldChip, ImpactFlag flag, int impactId)
            {
                if (impactId != _impactId)
                {
                    return;
                }

                ForEachListener(
                        cell,
                        l => l.OnGroupImpacting(cell, oldChip, flag, impactId),
                        _processedListeners
                    );

                ReleaseLock(cell);
            }

            private void OnCellImpactCanceled(CellComponent cell, ImpactFlag flag, int impactId)
            {
                if (impactId == _impactId)
                {
                    ReleaseLock(cell);
                }
            }

            private void ForEachListener(
                    CellComponent cell,
                    Func<IGroupImpactListener, bool> action,
                    List<IGroupImpactListener> processed
                )
            {
                List<IGroupImpactListener> listeners;
                if (!_system._listeners.TryGetValue(cell, out listeners))
                {
                    return;
                }

                foreach (IGroupImpactListener listener in listeners.ToArray())
                {
                    if (!processed.Contains(listener) && action.Invoke(listener))
                    {
                        processed.Add(listener);
                    }
                }
            }

            private void ReleaseLock(CellComponent cell)
            {
                cell.Impacted -= OnCellImpacted;
                cell.ImpactCanceled -= OnCellImpactCanceled;

                _lockCounter--;
                if (_lockCounter == 0)
                {
                    _system.RemoveGroup(this);
                }
            }
        }

        #endregion Inner types
    }
}