﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Makes game object stay still on the same position.
    /// </summary>
    public class PositionHolder : MonoBehaviour
    {
        private Vector3 _position;

        protected void Start()
        {
            _position = transform.position;
        }

        protected void Update()
        {
            transform.position = _position;
        }
    }
}