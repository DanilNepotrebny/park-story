﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Component that encapsulates two-dimensional match-3 grid.
    /// Most of the settings can be configured through Match-3 Grid Editor window rather than through standard inspector window.
    /// </summary>
    public partial class GridSystem : MonoBehaviour
    {
        [SerializeField, HideInInspector] private CellComponent[] _cells;
        [SerializeField, HideInInspector] private int _width;
        [SerializeField, HideInInspector] private int _height;
        [SerializeField, HideInInspector] private float _cellWidth;
        [SerializeField, HideInInspector] private float _cellHeight;

        /// <summary>
        /// Gets the height of the grid.
        /// </summary>
        public virtual int Height => _height;

        /// <summary>
        /// Gets the width of the grid.
        /// </summary>
        public virtual int Width => _width;

        /// <summary>
        /// Gets the width of the cell in world coordinates.
        /// </summary>
        public float CellWidth => _cellWidth;

        /// <summary>
        /// Gets the height of the cell in world coordinates.
        /// </summary>
        public float CellHeight => _cellHeight;

        public Vector2 CellSize => new Vector2(CellWidth, CellHeight);

        /// <summary>
        /// Gets cells count in the grid.
        /// </summary>
        public int CellsCount => _cells.Count(cell => cell != null);

        /// <summary>
        /// Two-dimensional indexer of cells in the grid. Each cell can be null. This is the fastest way to access the cells
        /// (there are no inner out of bounds checks) so prefer using this indexer if you iterate over x and y coordinates separately.
        /// </summary>
        public virtual CellComponent this[int x, int y]
        {
            get
            {
                int index = CalculateIndex(x, y);
                return index.InRange(0, _cells.Length - 1) ? _cells[index] : null;
            }
            private set { _cells[CalculateIndex(x, y)] = value; }
        }

        public virtual IEnumerable<CellComponent> Cells => _cells.Where(cell => cell != null);

        public void ForEachCell([NotNull] Action<CellComponent> action)
        {
            foreach (CellComponent cell in Cells)
            {
                action.Invoke(cell);
            }
        }

        /// <summary>
        /// Gets the grid position of the specified cell.
        /// </summary>
        public Vector2Int GetGridPosition(CellComponent cell)
        {
            int index = Array.IndexOf(_cells, cell);
            if (index < 0)
            {
                throw new ArgumentException("Cell is not found in the grid");
            }

            return new Vector2Int(index % _width, index / _width);
        }

        /// <summary>
        /// Gets cell by specified position. Returns null if position is out of grid bounds.
        /// </summary>
        public virtual CellComponent GetCell(Vector2Int pos)
        {
            if (IsInBounds(pos))
            {
                return this[pos.x, pos.y];
            }

            return null;
        }

        /// <summary>
        /// Converts world coordinates to grid coordinates
        /// </summary>
        public Vector2Int WorldToGrid(Vector3 _worldPosition)
        {
            Vector2 gridPosF = WorldToGridF(_worldPosition);
            return new Vector2Int(
                    Mathf.RoundToInt(gridPosF.x),
                    Mathf.RoundToInt(gridPosF.y)
                );
        }

        /// <summary>
        /// Converts grid coordinates to world coordinates
        /// </summary>
        public Vector3 GridToWorld(Vector2Int _gridPosition)
        {
            return GridToWorldF(new Vector2(_gridPosition.x, _gridPosition.y));
        }

        /// <summary>
        /// Converts world coordinates to grid coordinates
        /// </summary>
        public Vector2 WorldToGridF(Vector3 _position)
        {
            Vector3 localWorldPos = _position - transform.position;
            Vector2 gridPos = new Vector2(
                    localWorldPos.x / CellWidth,
                    localWorldPos.y / CellHeight
                );
            return gridPos;
        }

        /// <summary>
        /// Converts grid coordinates to world coordinates
        /// </summary>
        public Vector3 GridToWorldF(Vector2 _gridPosition)
        {
            Vector3 localWorldPos = new Vector3(
                    _gridPosition.x * CellWidth,
                    _gridPosition.y * CellHeight,
                    0
                );
            return localWorldPos + transform.position;
        }

        public RectInt WorldToGridBounds(Rect worldBounds)
        {
            Vector2Int gridSize = new Vector2Int(
                    Mathf.RoundToInt(worldBounds.width / CellWidth),
                    Mathf.RoundToInt(worldBounds.height / CellHeight)
                );

            Vector2Int gridPosition = WorldToGrid(worldBounds.position + CellSize * 0.5f);
            return new RectInt(gridPosition, gridSize);
        }

        public Rect GridToWorldBounds(RectInt gridBounds)
        {
            Vector2 worldPosition = (Vector2)GridToWorldF(gridBounds.position) - CellSize * 0.5f;
            Vector2 worldSize = new Vector2(gridBounds.width * CellWidth, gridBounds.height * CellHeight);
            return new Rect(worldPosition, worldSize);
        }

        public Bounds GetWorldBounds()
        {
            Vector3 min = GridToWorldF(new Vector2(-0.5f, -0.5f));
            Vector3 max = GridToWorldF(new Vector2(Width - 0.5f, Height - 0.5f));

            var bounds = new Bounds();
            bounds.SetMinMax(min, max);
            return bounds;
        }

        public void ProcessCellsInRadius(Action<CellComponent, float> processor, Vector2Int center, float radius)
        {
            int maxOffset = Mathf.FloorToInt(radius);

            int xMin = Mathf.Max(center.x - maxOffset, 0);
            int xMax = Mathf.Min(center.x + maxOffset, Width - 1);
            int yMin = Mathf.Max(center.y - maxOffset, 0);
            int yMax = Mathf.Min(center.y + maxOffset, Height - 1);

            for (int y = yMin; y <= yMax; y++)
            {
                for (int x = xMin; x <= xMax; x++)
                {
                    Vector2Int position = new Vector2Int(x, y);
                    float distance = (position - center).magnitude;

                    if (distance <= radius)
                    {
                        CellComponent cell = this[position.x, position.y];
                        if (cell != null)
                        {
                            processor(cell, distance);
                        }
                    }
                }
            }
        }

        public void ProcessCellsWhileConditionByPositions(
                Func<Vector2Int, bool> processor,
                Vector2Int center
            )
        {
            var positionsToProcess = new Queue<Vector2Int>();
            positionsToProcess.Enqueue(center);

            while (positionsToProcess.Count > 0)
            {
                Vector2Int currentPosition = positionsToProcess.Dequeue();

                if (processor.Invoke(currentPosition))
                {
                    break;
                }

                var direction = Direction.Down;
                do
                {
                    Vector2Int neighbourPosition = currentPosition + direction.GetOffset();
                    if (IsInBounds(neighbourPosition) && !positionsToProcess.Contains(neighbourPosition))
                    {
                        positionsToProcess.Enqueue(neighbourPosition);
                    }

                    direction = direction.Rotate90CW();
                }
                while (direction != Direction.Down);
            }
        }

        public bool IsInBounds(Vector2Int pos)
        {
            return pos.x >= 0 &&
                pos.x < _width &&
                pos.y >= 0 &&
                pos.y < _height;
        }

        protected void Awake()
        {
            RectInt realBounds = CalculateRealBounds();
            if (realBounds.width != Width ||
                realBounds.height != Height)
            {
                UnityEngine.Debug.LogError("Grid bounds contain excess areas. Please resave the level.");
            }
        }

        private int CalculateIndex(int x, int y)
        {
            return CalculateIndex(_width, x, y);
        }

        private static int CalculateIndex(int width, int x, int y)
        {
            return y * width + x;
        }

        private RectInt CalculateRealBounds()
        {
            var min = new Vector2Int(int.MaxValue, int.MaxValue);
            var max = new Vector2Int(int.MinValue, int.MinValue);

            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    if (this[x, y] != null)
                    {
                        var position = new Vector2Int(x, y);

                        min = Vector2Int.Min(min, position);
                        max = Vector2Int.Max(max, position);
                    }
                }
            }

            return new RectInt(min, max - min + Vector2Int.one);
        }
    }
}