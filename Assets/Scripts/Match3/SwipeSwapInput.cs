﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    public class SwipeSwapInput : MonoBehaviour
    {
        [Inject] private GridSystem _grid;
        [Inject] private SwapSystem _swapSystem;

        private readonly List<CellPresenter> _cells = new List<CellPresenter>();

        protected void OnEnable()
        {
            _cells.Clear();
            foreach (CellComponent cell in _grid.Cells)
            {
                cell.Presenter.CellSwiped += OnCellSwiped;
                _cells.Add(cell.Presenter);
            }
        }

        protected void OnDisable()
        {
            foreach (CellPresenter cell in _cells)
            {
                cell.CellSwiped -= OnCellSwiped;
            }
        }

        private void OnCellSwiped(CellPresenter cell, Vector2 swipeOffset)
        {
            if (cell.CellComponent.IsEmpty)
            {
                return;
            }

            Direction dir = _swapSystem.CalculateSwapDirection(swipeOffset);
            if (dir != Direction.None)
            {
                _swapSystem.SwapChips(cell.CellComponent, dir);
            }
        }
    }
}