﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.Utils;
using DreamTeam.Utils.Coroutines;
using DreamTeam.Utils.Spine;
using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using AnimationState = Spine.AnimationState;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(ChipPresenter))]
    public abstract class ChipStateSpinePresenter : MonoBehaviour
    {
        [SerializeField] private AnimationsGroup[] _animations;

        [Inject] private Instantiator _instantiator;

        private SkeletonAnimation _animation;
        private AnimationState _animState => _animation.AnimationState;
        private SkeletonIdleAnimationHandler _idleAnimationHandler;
        private SpineChipPingPresenter[] _pingPresenters;

        private Coroutine _stateChangeAnimCoroutine;

        protected abstract int State { get; }
        protected AnimationState AnimationState => _animState;

        protected virtual void Awake()
        {
            _animation = GetComponentInChildren<SkeletonAnimation>();
            Assert.IsNotNull(_animation, "There is no SkeletonAnimation component in child. Is there any view?");

            _idleAnimationHandler = GetComponentInChildren<SkeletonIdleAnimationHandler>();
            Assert.IsNotNull(
                _idleAnimationHandler,
                "There is no SkeletonIdleAnimationHandle component in child. Is there any view?");

            _pingPresenters = GetComponentsInChildren<SpineChipPingPresenter>();
            Assert.IsTrue(
                    _pingPresenters.Length > 0,
                    $"There is no {nameof(SpineChipPingPresenter)} component in child. Is there any view?"
                );
        }

        protected virtual void Start()
        {
            AnimationsGroup animationsGroup = GetAnimationsGroup(State);
            SetIdleAnimation(animationsGroup);
            SetPingAnimation(animationsGroup);
        }

        protected void OnStateChanged()
        {
            if (_stateChangeAnimCoroutine != null)
            {
                StopCoroutine(_stateChangeAnimCoroutine);
            }

            AnimationsGroup animationsGroup = GetAnimationsGroup(State);
            if (animationsGroup != null)
            {
                _stateChangeAnimCoroutine = StartCoroutine(PlayStateChangeAnimation(animationsGroup));
            }
        }

        private IEnumerator PlayStateChangeAnimation(AnimationsGroup animationsGroup)
        {
            _idleAnimationHandler.StopAnimation();

            if (animationsGroup.StateChangeEffectPrefab != null)
            {
                _instantiator.Instantiate(animationsGroup.StateChangeEffectPrefab, transform.position);
            }

            if (animationsGroup.StateChangeAnimation != string.Empty)
            {
                _animState.SetAnimation(0, animationsGroup.StateChangeAnimation, false);

                SetPingAnimation(animationsGroup);

                yield return new WaitForEvent<TrackEntry, AnimationState>(_animState, nameof(_animState.Complete));
            }

            SetIdleAnimation(animationsGroup);
            _stateChangeAnimCoroutine = null;
        }

        private AnimationsGroup GetAnimationsGroup(int state)
        {
            int index = state < _animations.Length ? state : _animations.Length - 1;

            if (index < 0)
            {
                return null;
            }

            return _animations[index];
        }

        private void SetIdleAnimation(AnimationsGroup animationsGroup)
        {
            _idleAnimationHandler.IdleAnimation = animationsGroup.IdleAnimation;
            _idleAnimationHandler.AdditionalAnimations = animationsGroup.PeriodicAnimations;
            _idleAnimationHandler.PlayAnimation(false);
        }

        private void SetPingAnimation(AnimationsGroup animationsGroup)
        {
            foreach (SpineChipPingPresenter presenter in _pingPresenters)
            {
                presenter.SetIdleAnimation(animationsGroup.IdleAnimation);

                presenter.ClearPingAnimations();
                foreach (string animationName in animationsGroup.PingAnimations)
                {
                    presenter.AddPingAnimation(animationName);
                }
            }
        }

        #region Internal classes

        [Serializable]
        private class AnimationsGroup
        {
            [SerializeField, SpineAnimation] private string _idleAnimation;
            [SerializeField, SpineAnimation] private string _stateChangeAnimation;
            [SerializeField] private GameObject _stateChangeEffectPrefab;
            [SerializeField, SpineAnimation] private string[] _periodicAnimations;
            [SerializeField, SpineAnimation] private string[] _pingAnimations;

            public string IdleAnimation => _idleAnimation;
            public string StateChangeAnimation => _stateChangeAnimation;
            public GameObject StateChangeEffectPrefab => _stateChangeEffectPrefab;
            public string[] PeriodicAnimations => _periodicAnimations;
            public string[] PingAnimations => _pingAnimations;
        }

        #endregion Internal classes
    }
}