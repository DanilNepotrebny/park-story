﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Inventory;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3
{
    [CreateAssetMenu(menuName = "Match 3/Level failure offers collection")]
    public class LevelFailureOffersCollection : ScriptableObject
    {
        [SerializeField] private List<ProductPack> _offers = new List<ProductPack>();

        public ProductPack GetOffer(int attemptCount)
        {
            List<ProductPack> validOffers = _offers.Where(item => item.CanBeReceived).ToList();
            Assert.IsTrue(validOffers.Count > 0, $"Didn't find any {nameof(ProductPack)} to return as offer. " +
                $"Every existed offer contains at least one locked reward. Total offers count - {_offers.Count}");

            int offerIndex = Mathf.Clamp(attemptCount, 0, validOffers.Count - 1);
            return validOffers[offerIndex];
        }
    }
}