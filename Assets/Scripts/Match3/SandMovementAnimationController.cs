﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.RendererSorting;
using UnityEngine;
using UnityEngine.Rendering;
using Zenject;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(ChipFallingPresenter))]
    public class SandMovementAnimationController : MonoBehaviour
    {
        [SerializeField] private GameObject _bezelTileContainer;
        [SerializeField] private GameObject _tileAssemblingEffectPrefab;
        [SerializeField] private GameObject _tileBreakingEffectPrefab;
        [SerializeField] private SimpleAnimation _movementOverlay;
        [SerializeField] private float _movementDelay;
        [SerializeField] private int _tileEffectsRelativeSortingOrder;

        [Inject] private Instantiator _instantiator;

        private ChipPresenter _chipPresenter;
        private IDeferredInvocationHandle _fallingStartHandle;

        private bool _isFallingStarted;
        
        protected void Awake()
        {
            _chipPresenter = GetComponent<ChipPresenter>();

            BaseChipMovementPresenter[] movementPresenters = GetComponents<BaseChipMovementPresenter>();
            foreach (BaseChipMovementPresenter movementPresenter in movementPresenters)
            {
                movementPresenter.Moving += OnMoving;
                movementPresenter.Moved += OnMoved;
            }
        }

        private void OnMoving(IDeferredInvocationHandle fallingStartHandle)
        {
            if (_isFallingStarted)
            {
                return;
            }
            
            _isFallingStarted = true;
            _fallingStartHandle = fallingStartHandle.Lock();

            _bezelTileContainer.SetActive(false);
            _movementOverlay.gameObject.SetActive(true);
            _movementOverlay.Play(SimpleAnimationType.Show);

            CreateEffect(_tileBreakingEffectPrefab);

            Invoke(nameof(StartMovement), _movementDelay);
        }

        private void StartMovement()
        {
            _fallingStartHandle.Dispose();
            _fallingStartHandle = null;
        }

        private void OnMoved()
        {
            _movementOverlay.Play(SimpleAnimationType.Hide);
            _movementOverlay.AnimationFinished += OnMovementOverlayHidden;

            _bezelTileContainer.SetActive(true);

            _isFallingStarted = false;

            CreateEffect(_tileAssemblingEffectPrefab);
        }

        private void OnMovementOverlayHidden(ISimpleAnimationPlayer player, SimpleAnimationType obj)
        {
            _movementOverlay.AnimationFinished -= OnMovementOverlayHidden;
            _movementOverlay.gameObject.SetActive(false);
        }

        private void CreateEffect(GameObject effectPrefab)
        {
            GameObject effect = _instantiator.Instantiate(
                    effectPrefab,
                    _bezelTileContainer.transform.position,
                    _chipPresenter.gameObject
                );
            effect.GetComponent<ChipPartsPresenter>()?.SetChipPresenter(_chipPresenter);

            SortingGroup sortingGroup = effect.GetComponent<SortingGroup>();
            if (sortingGroup != null)
            {
                SortingOptions sortingOptions = _chipPresenter.SortingOptions;
                sortingGroup.sortingLayerID = sortingOptions.SortingLayerId;
                sortingGroup.sortingOrder = sortingOptions.SortingOrder + _tileEffectsRelativeSortingOrder;
            }
        }
    }
}