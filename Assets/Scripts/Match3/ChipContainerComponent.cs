﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Contains chip component.
    /// </summary>
    [DisallowMultipleComponent]
    public partial class ChipContainerComponent : MonoBehaviour
    {
        [SerializeField] private ChipComponent _chip;
        [SerializeField] private ChipTagSet _chipVisibilityRestriction;

        /// <summary>
        /// Gets or sets the chip from/into this container.
        /// </summary>
        public ChipComponent Chip
        {
            get { return _chip; }
            set
            {
                if (value != null && EditorUtils.IsPlayingOrWillChangePlaymode())
                {
                    ChangeChip(value, ChipComponent.MovingType.Immediate);
                }
                else
                {
                    SetChip(value);
                }
            }
        }

        public bool IsChipVisible => _chip?.Tags.FindFirstOf(_chipVisibilityRestriction) != null;

        /// <summary>
        /// Triggered when the chip inside this container has changed.
        /// </summary>
        public event Action<ChipComponent, ChipComponent> ChipChanged;

        public event Action<ChipComponent, ChipComponent> ChipChangedSilently;

        /// <summary>
        /// Changes chip in this container to the specified one via animated movement process.
        /// </summary>
        public void ChangeChip(
                [NotNull] ChipComponent chip,
                ChipComponent.MovingType type
            )
        {
            ChipComponent oldChip = _chip;
            ChipContainerComponent oldContainer = chip.Cell?.GetComponent<ChipContainerComponent>();

            ChangeChipSilently(chip, type);

            ChipChanged?.Invoke(oldChip, chip);
            if (oldContainer != null)
            {
                oldContainer.ChipChanged?.Invoke(chip, null);
            }
        }

        public void ChangeChipSilently([NotNull] ChipComponent chip, ChipComponent.MovingType type)
        {
            SetChipSilently(chip);
            chip.MoveTo(this, type);
        }

        /// <summary>
        /// Swaps chips between two containers.
        /// </summary>
        public void SwapChips(
                ChipContainerComponent other,
                ChipComponent.MovingType type = ChipComponent.MovingType.Immediate
            )
        {
            ChipComponent thisChip = Chip;
            ChipComponent otherChip = other.Chip;

            SwapChipsInternal(other);

            otherChip?.MoveTo(this, type);
            thisChip?.MoveTo(other, type);

            ChipChanged?.Invoke(thisChip, otherChip);
            other.ChipChanged?.Invoke(otherChip, thisChip);
        }

        /// <summary>
        /// Swaps chips cyclically between multiple containers.
        /// </summary>
        public static void RotateChips(
                List<ChipContainerComponent> containers,
                List<ChipComponent.MovingType> movingTypes = null
            )
        {
            for (int i = containers.Count - 1; i > 0; i--)
            {
                ChipContainerComponent container1 = containers[i];
                ChipContainerComponent container2 = containers[i - 1];
                container1.SwapChipsInternal(container2);
            }

            for (int i = 0; i < containers.Count; i++)
            {
                ChipContainerComponent container = containers[i];

                int prevIndex = (i - 1 + containers.Count) % containers.Count;
                ChipComponent.MovingType movingType = movingTypes?[prevIndex] ?? ChipComponent.MovingType.Immediate;
                container.Chip?.MoveTo(container, movingType);
            }

            for (int i = 0; i < containers.Count; i++)
            {
                ChipContainerComponent container = containers[i];
                ChipContainerComponent nextContainer = containers[(i + 1) % containers.Count];
                containers[i].ChipChanged?.Invoke(nextContainer.Chip, container.Chip);
            }
        }

        /// <summary>
        /// Swaps chips between two containers without calling any callbacks. Should be used for temporary calculations only.
        /// </summary>
        public void SwapChipsSilently(ChipContainerComponent other)
        {
            SwapChipsInternal(other);

            Chip?.MoveToSilently(this);
            other.Chip?.MoveToSilently(other);

            ChipChangedSilently?.Invoke(Chip, other.Chip);
            other.ChipChangedSilently?.Invoke(other.Chip, Chip);
        }

        public bool CanInsertChip()
        {
            if (Chip != null)
            {
                ChipContainerComponent container = Chip.GetComponent<ChipContainerComponent>();
                if (container != null)
                {
                    return container.CanInsertChip();
                }

                return false;
            }

            return true;
        }

        public void PushChip(ChipComponent chip)
        {
            if (Chip != null)
            {
                ChipContainerComponent container = Chip.GetComponent<ChipContainerComponent>();
                if (container != null)
                {
                    container.PushChip(chip);
                }
            }
            else
            {
                Chip = chip;
            }
        }

        public ChipComponent PopChip()
        {
            ChipComponent poppedChip = null;

            if (Chip != null)
            {
                ChipContainerComponent container = Chip.GetComponent<ChipContainerComponent>();
                if (container != null)
                {
                    poppedChip = container.PopChip();
                }

                if (poppedChip == null)
                {
                    poppedChip = Chip;
                    Chip = null;
                }
            }

            return poppedChip;
        }

        public ChipComponent GetTopChip()
        {
            if (Chip != null)
            {
                ChipContainerComponent container = Chip.GetComponent<ChipContainerComponent>();
                if (container != null)
                {
                    return container.GetTopChip();
                }

                return Chip;
            }

            return GetComponent<ChipComponent>();
        }

        /// <summary>
        /// Recursively iterates through all inner containers starting from this one.
        /// </summary>
        public IEnumerator<ChipContainerComponent> GetEnumerator()
        {
            ChipContainerComponent container = this;
            while (container != null)
            {
                yield return container;

                container = container.Chip != null ?
                    container.Chip.GetComponent<ChipContainerComponent>() :
                    null;
            }
        }

        /// <summary>
        /// Detaches chip from the container without invoking events.
        /// </summary>
        public void DeleteChipSilently()
        {
            SetChipSilently(null);
        }

        private void SetChip(ChipComponent chip)
        {
            ChipComponent oldChip = _chip;
            ChipContainerComponent oldContainer = chip?.ParentContainer;

            SetChipSilently(chip);

            ChipChanged?.Invoke(oldChip, chip);
            if (oldContainer != null)
            {
                oldContainer.ChipChanged?.Invoke(chip, null);
            }
        }

        private void SetChipSilently(ChipComponent chip)
        {
            ChipComponent oldChip = _chip;
            if (oldChip != null)
            {
                oldChip.transform.SetParent(null);
            }

            _chip = chip;

            if (chip != null)
            {
                chip.ParentContainer?.SetChipSilently(null);
                chip.transform.SetParent(transform);
            }
        }

        private void SwapChipsInternal(ChipContainerComponent other)
        {
            ChipComponent tmp = _chip;

            _chip = other._chip;
            if (_chip != null)
            {
                _chip.transform.SetParent(transform);
            }

            other._chip = tmp;
            if (tmp != null)
            {
                tmp.transform.SetParent(other.transform);
            }
        }
    }
}