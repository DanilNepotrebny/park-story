﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.GameControls;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    public abstract class ChipPingPresenter : MonoBehaviour
    {
        [SerializeField] private ChipAudioController _audioController;
        [SerializeField] private string _audioEventName;

        [Space, SerializeField, RequiredField] private ChipComponent _chip;

        [Inject] private GameControlsSystem _gameControlsSystem;

        [Inject(Id = Match3Installer.ChipPingActivityControlId)]
        private GameControl _activityControl;

        private CellComponent _subscribedCell;

        protected void OnEnable()
        {
            _chip.CellChanged += OnCellChanged;
            SubscribeCell();
        }

        protected void OnDisable()
        {
            _chip.CellChanged -= OnCellChanged;
            UnsubscribeCell();
        }

        protected void Reset()
        {
            _chip = GetComponentInParent<ChipComponent>();
        }

        private void OnCellChanged(ChipComponent chip, CellComponent previousCell)
        {
            UnsubscribeCell();
            SubscribeCell();
        }

        private void SubscribeCell()
        {
            if (_chip.Cell != null)
            {
                _chip.Cell.Presenter.CellTapped += OnCellTapped;
                _subscribedCell = _chip.Cell;
            }
        }

        private void UnsubscribeCell()
        {
            if (_subscribedCell != null)
            {
                _subscribedCell.Presenter.CellTapped -= OnCellTapped;
                _subscribedCell = null;
            }
        }

        private void OnCellTapped(CellComponent cell)
        {
            if (_gameControlsSystem.IsControlInState(_activityControl, GameControl.State.Unlocked))
            {
                _audioController?.PlaySound(_audioEventName);
                PlayPingAnim(cell);
            }
        }

        protected abstract void PlayPingAnim(CellComponent cell);
    }
}