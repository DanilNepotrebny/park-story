﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Cutscenes;
using DreamTeam.GameControls;
using DreamTeam.Location.ActionByLevel;
using DreamTeam.Match3.Bezel;
using DreamTeam.Match3.Conveyor;
using DreamTeam.Match3.ExplosionEffect;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.Hammer;
using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.MagicHat;
using DreamTeam.Match3.Matching;
using DreamTeam.Match3.MoveWeight;
using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.Requirements.ChipConsumption;
using DreamTeam.Match3.SpecialChips.ChipSpawner;
using DreamTeam.Match3.Spreading;
using DreamTeam.Match3.UI.HUD;
using DreamTeam.Match3.Wall;
using DreamTeam.Mirroring;
using DreamTeam.Playables.Tutorial.Match3.Presenters;
using DreamTeam.SceneLoading;
using DreamTeam.Tutorial;
using DreamTeam.Utils;
using DreamTeam.Utils.Movement;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    /// <summary> Dependency injection installer for match-3 scene. </summary>
    public class Match3Installer : MonoInstaller<Match3Installer>, ICurrentLevelInfo
    {
        public const string ChipPingActivityControlId = "ChipPingActivityControl";
        public const string RequirementHelpersContainerId = "RequirementHelpersContainer";
        public const string RandomId = "Match3RandomContainerId";
        public const string TutorialCellCutoffPrefabId = "TutorialCellCutoffPrefabId";
        public const string TutorialMovePointerPresenterId = "TutorialMovePointerPresenterId";

        [Inject] private LevelSystem _levelSystem;
        [Inject] private Instantiator _instantiator;

        [SerializeField] private GoalsAndCoinsContainer _goalsAndCoinsContainer;
        [SerializeField] private ChipConsumptionGoalFactory _chipConsumptionGoalFactory;
        [SerializeField] private ChipConsumptionFactory _chipConsumptionFactory;
        [SerializeField] private GoalInstancesPanel _goalInstancesPanel;
        [SerializeField] private LimitationsPanel _limitationsPanel;
        [SerializeField] private MoveWeightCalculator _moveWeightCalculator;
        [SerializeField] private MovementArea _movementArea;
        [SerializeField] private GameObject _requirementHelpersContainer;
        [SerializeField] private GameObject _tintCellCutoffPrefab;
        [SerializeField] private TutorialMovePointerPresenter _tutorialMovePointerPresenter;
        [SerializeField] private GameControl _chipPingActivityControl;
        
        private LevelPack _bindedLevelPack;
        private BaseLevelPackController _bindedLevelPackController;

        public int CurrentLevelIndex => _bindedLevelPack.FindLevelIndex(CurrentLevel);
        public LevelPack CurrentLevelPack => _bindedLevelPack;
        public string CurrentLevelId => _bindedLevelPackController.GetLevelId(CurrentLevel);
        public LevelSettings CurrentLevel { get; private set; }

        public override void InstallBindings()
        {
            Container.Bind<MirrorObjectsRegistrator>().AsSingle().NonLazy();
            Container.Bind<CutsceneSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<TutorialTintController>().FromComponentInHierarchy(null, true).AsSingle().NonLazy();
            Container.Bind<TutorialExclusiveTapController>().FromComponentInHierarchy(null, true).AsSingle().NonLazy();

            Container.Bind<GridPresentersSorter>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<WallSystem>().FromComponentInHierarchy().AsSingle();
            Container.Bind<SwapSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<TapAndTapSwapInput>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<MatchSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<GravitySystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<CombinationSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<ChipConsumptionDispatcher>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<HammerSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<FieldStabilizationSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<LevelCompletionSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<LevelRewardSequence>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<MagicHatSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<ShuffleSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<AutoHintSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<ChipBoosterGenerationSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<MagicHatPanel>().FromComponentInHierarchy(null, true).AsSingle().NonLazy();
            Container.Bind<LimitationsPanel>().FromInstance(_limitationsPanel).AsSingle().NonLazy();
            Container.Bind<GoalInstancesPanel>().FromInstance(_goalInstancesPanel).AsSingle().NonLazy();
            Container.Bind<MovesSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<ExplosionEffectSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<ImpactSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<SpreadingSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<BezelSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<ConveyorSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<ChipSpawnSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<BoosterTipSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<LevelFailureHandler>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<MovementArea>().FromInstance(_movementArea).AsSingle().NonLazy();
            Container.Bind<MoveWeightCalculator>().FromInstance(_moveWeightCalculator).AsSingle().NonLazy();
            Container.Bind<GameObject>()
                .WithId(RequirementHelpersContainerId)
                .FromInstance(_requirementHelpersContainer)
                .AsSingle()
                .NonLazy();

            Container.Bind<ChipConsumptionGoalFactory>().FromInstance(_chipConsumptionGoalFactory);
            Container.Bind<ChipConsumptionFactory>().FromInstance(_chipConsumptionFactory);

            Container.Bind<GameControl>().WithId(ChipPingActivityControlId).FromInstance(_chipPingActivityControl);
            Container.BindInternals<ActorsByLevelManager>();

            Container.BindComponent<SaveGroup>(this);

            Container.
                BindFactory<Match, ChipComponent, ChipComponent, SpecialMatchPresenter, SpecialMatchImpactController,
                    SpecialMatchImpactController.Factory>();

            Container.BindFactory<ChipConsumptionRequirement.Instance, ConsumptionRequirementImpactController,
                    ConsumptionRequirementImpactController.Factory>();

            Container.BindFactory<ChipCleanupRequirement.Instance, ChipCleanupRequirementImpactController,
                ChipCleanupRequirementImpactController.Factory>();

            Container.Bind<GoalsAndCoinsContainer>().FromInstance(_goalsAndCoinsContainer).AsSingle().NonLazy();
            _goalsAndCoinsContainer.InstallBindings(Container);

            Container.Bind<ICurrentLevelInfo>().FromInstance(this);

            Container.Bind<LoadSceneSettings>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<ReloadSceneSettings>().FromComponentInHierarchy().AsSingle().NonLazy();

            CurrentLevel = _levelSystem.GetLoadedLevel();
            CurrentLevel.InstallBindings(Container);

            Container.Bind<CustomRandom>()
                .WithId(RandomId)
                .FromInstance(new CustomRandom(CurrentLevel.RandomSeeds))
                .AsSingle()
                .NonLazy();

            _levelSystem.FindLevelInfo(CurrentLevel, out _bindedLevelPackController, out _bindedLevelPack);

            Container.BindInterfacesAndSelfTo<Match3InfoDebugWriter>().AsSingle();

            Container.Bind<LevelStartNotifier>().FromNewComponentOn(gameObject).AsSingle().NonLazy();

            Container.BindInstance(_tintCellCutoffPrefab).WithId(TutorialCellCutoffPrefabId).AsCached().NonLazy();
            Container.BindInstance(_tutorialMovePointerPresenter)
                .WithId(TutorialMovePointerPresenterId)
                .AsCached()
                .NonLazy();
        }

        protected void OnDestroy()
        {
            if (CurrentLevel != null)
            {
                CurrentLevel.DisposeBindings(Container);
            }
        }
    }
}