﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

// #define DRAW_GRID_GRAVITY_GIZMOS

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.UI.SimpleAnimating;
using ModestTree;
using UnityEngine;
using Zenject;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DreamTeam.Match3
{
    public partial class GridAnimator
    {
        [Header("Gravity animation")]

        [SerializeField] private float _gravityAnimationDelay = 10f;
        [SerializeField] private float _gravityAnimationDelta = 0.05f;

        [Inject] private GravitySystem _gravitySystem;
        [Inject] private FieldStabilizationSystem _stabilizationSystem;

        private bool _isGravityAnimating;
        private LTDescr _timerDescr;
        private Coroutine _cancellationCoroutine;
        private bool _playedInitialAnimation;
        private readonly Dictionary<CellComponent, int> _animatingCells = new Dictionary<CellComponent, int>();

        #if UNITY_EDITOR && DRAW_GRID_GRAVITY_GIZMOS
        private static GUIStyle _style;
        private static readonly Vector2 _offset = new Vector2(-10, -32);
        private const int _fontSize = 30;

        protected void OnDrawGizmos()
        {
            if (_style == null)
            {
                _style = new GUIStyle
                {
                    normal = { textColor = Color.black },
                    fontSize = _fontSize
                };
            }

            Handles.BeginGUI();
            {
                foreach (var pair in _animatingCells)
                {
                    CellComponent cell = pair.Key;
                    int order = pair.Value;

                    Vector2 pos2D = HandleUtility.WorldToGUIPoint(cell.transform.position) + _offset;
                    GUI.Label(new Rect(pos2D.x, pos2D.y, 0, 0), order.ToString(), _style);
                }
            }
            Handles.EndGUI();
        }
        #endif // UNITY_EDITOR && DRAW_GRID_GRAVITY_GIZMOS

        protected void AwakeOnPartGravity()
        {
            _stabilizationSystem.AddStabilizationHandler(RequestAnimationPlay, FieldStabilizationHandlerOrder.GravityAnimating);
        }

        protected void OnDestroyForGravity()
        {
            _stabilizationSystem.RemoveStabilizationHandler(RequestAnimationPlay);
        }

        private void SetupGravityAnimation()
        {
            AnimationFinished += (animType) =>
            {
                if (animType == SimpleAnimationType.Show)
                {
                    StartCoroutine(PlayGravityAnimation());
                }
            };

            // #1: Find all gravity streams
            var gravityStreams = new List<List<CellComponent>>();
            foreach (CellComponent cell in _gridSystem.Cells)
            {
                if (gravityStreams.Any(set => set.Contains(cell)))
                {
                    continue;
                }

                var streamCells = new HashSet<CellComponent>();
                streamCells.UnionWith(
                        _gravitySystem.EnumerateUpstream(
                                cell,
                                checkSides: false,
                                checkBarriers: false,
                                includingStart: true
                            )
                    );
                streamCells = streamCells.Reverse().ToHashSet();
                streamCells.UnionWith(
                        _gravitySystem.EnumerateDownstream(
                                cell,
                                checkSides: false,
                                acceptEmpty: true,
                                checkBarriers: false,
                                checkWalls: false
                            )
                    );

                gravityStreams.Add(streamCells.ToList());
            }

            gravityStreams.Sort((left, right) => (int)(1 * Mathf.Sign(right.Count - left.Count)));

            // #2: Distribute streams across areas
            var gravityStreamsCopy = new List<List<CellComponent>>(gravityStreams);
            var areas = new List<List<CellComponent>>();
            for (int i = 0; i < gravityStreamsCopy.Count; ++i)
            {
                List<CellComponent> gravityStream = gravityStreamsCopy[i];
                areas.Add(gravityStream);

                HashSet<CellComponent> grownArea = GrowArea(gravityStream);

                bool isUpdatedCurrentArea;
                do
                {
                    isUpdatedCurrentArea = false;
                    for (int j = i + 1; j < gravityStreamsCopy.Count;)
                    {
                        List<CellComponent> unprocessedStream = gravityStreamsCopy[j];
                        bool sameArea = unprocessedStream.Any(cell => grownArea.Contains(cell));
                        if (sameArea)
                        {
                            gravityStreamsCopy.RemoveAt(j);
                            List<CellComponent> currentArea = areas.Last();
                            currentArea.AddRange(unprocessedStream);
                            grownArea = GrowArea(currentArea);
                            isUpdatedCurrentArea = true;
                        }
                        else
                        {
                            j += 1;
                        }
                    }
                }
                while (isUpdatedCurrentArea); // reevaluate previously skipped streams (now they might be valid)
            }

            areas.Sort((left, right) => (int)(1 * Mathf.Sign(right.Count - left.Count)));

            // #3: Iterate over areas (from biggest to smallest)
            int areaStartOrder = 0;
            do
            {
                List<CellComponent> area = areas.First();
                areas.Remove(area);

                // #4: Find longest stream in the area. Starting from it distribute time deltas to whole area
                int prevStreamSize = 0;
                foreach (List<CellComponent> stream in gravityStreams) // they were sorted by size
                {
                    CellComponent streamStart = stream.First(); // the very first element is the stream start
                    if (!area.Contains(streamStart))
                    {
                        continue;
                    }

                    if (prevStreamSize > 0 && stream.Count != prevStreamSize)
                    {
                        break;
                    }

                    prevStreamSize = stream.Count;

                    var processingCells = new Queue<KeyValuePair<CellComponent, int>>();
                    processingCells.Enqueue(new KeyValuePair<CellComponent, int>(streamStart, areaStartOrder));
                    while (processingCells.Count > 0)
                    {
                        var pair = processingCells.Dequeue();
                        CellComponent cell = pair.Key;
                        int order = pair.Value;

                        if (!_animatingCells.ContainsKey(cell))
                        {
                            _animatingCells.Add(cell, order);
                        }
                        else
                        {
                            int newOrder = Mathf.Min(order, _animatingCells[cell]);
                            if (newOrder >= _animatingCells[cell])
                            {
                                continue;
                            }

                            _animatingCells[cell] = newOrder;
                        }

                        var nextCells = _gravitySystem.EnumerateFallingDown(cell, true, false, false).ToList();
                        CellComponent forward = _gridSystem.GetCell(cell.GridPosition + cell.Gravity.GetOffset());
                        if (forward != null)
                        {
                            CellComponent target = _gridSystem.GetCell(forward.GridPosition + forward.Gravity.GetOffset());
                            nextCells.Remove(target);
                        }

                        nextCells.ForEach(
                                nextCell =>
                                {
                                    var next = new KeyValuePair<CellComponent, int>(nextCell, order + 1);
                                    processingCells.Enqueue(next);
                                }
                            );
                    }
                }

                // #5: Same areas in size have same start delta times
                if (areas.Count > 0 && area.Count == areas.First().Count)
                {
                    continue;
                }

                // #6: Find next biggest area. It's delta starts from biggest value of the previos one
                areaStartOrder = area.Max(cell => _animatingCells.ContainsKey(cell) ? _animatingCells[cell] : 0);
            }
            while (areas.Count > 0);

            // #6: Complete unprocessed cells with backward (aka updstream) algorithm
            var unprocessedCells = new List<CellComponent>();
            foreach (CellComponent cell in _gridSystem.Cells)
            {
                if (!_animatingCells.ContainsKey(cell))
                {
                    unprocessedCells.Add(cell);
                }
            }

            while (unprocessedCells.Count > 0)
            {
                int unprocessedBefore = unprocessedCells.Count;
                foreach (CellComponent cell in _gridSystem.Cells)
                {
                    foreach (CellComponent nextCell in _gravitySystem.EnumerateFallingDown(cell, true, false, false))
                    {
                        if (!_animatingCells.ContainsKey(cell))
                        {
                            if (_animatingCells.ContainsKey(nextCell))
                            {
                                int order = _animatingCells[nextCell] - 1;
                                _animatingCells.Add(cell, order);
                            }
                        }
                        else if (!_animatingCells.ContainsKey(nextCell))
                        {
                            int order = _animatingCells[cell] + 1;
                            _animatingCells.Add(nextCell, order);
                        }
                    }

                    // check twin one (on the same line and with the same direction)
                    if (!_animatingCells.ContainsKey(cell))
                    {
                        int order = int.MaxValue;

                        Vector2Int offset = cell.Gravity.Rotate90CW().GetOffset();
                        CellComponent neighbour = _gridSystem.GetCell(cell.GridPosition + offset);

                        if (neighbour != null &&
                            cell.Gravity == neighbour.Gravity &&
                            _animatingCells.ContainsKey(neighbour))
                        {
                            order = _animatingCells[neighbour];
                        }

                        offset = cell.Gravity.Rotate90CCW().GetOffset();
                        neighbour = _gridSystem.GetCell(cell.GridPosition + offset);

                        if (neighbour != null &&
                            cell.Gravity == neighbour.Gravity &&
                            _animatingCells.ContainsKey(neighbour))
                        {
                            order = Mathf.Min(order, _animatingCells[neighbour]);
                        }

                        if (order < int.MaxValue)
                        {
                            _animatingCells.Add(cell, order);
                        }
                    }
                }

                // cleanup
                unprocessedCells.RemoveAll(cell => _animatingCells.ContainsKey(cell));
                if (unprocessedBefore == unprocessedCells.Count)
                {
                    UnityEngine.Debug.LogAssertion($"Was not able to calculate gravity animation for all cells. " +
                        $"Check level layout and update algorithm");
                    break;
                }
            }
        }

        private void RequestAnimationPlay()
        {
            if (!_playedInitialAnimation)
            {
                return;
            }

            _timerDescr = LeanTween.delayedCall(_gravityAnimationDelay, () => StartCoroutine(PlayGravityAnimation()));

            StopAnimationCancellation();
            _cancellationCoroutine = StartCoroutine(CheckAnimationCancel());
        }

        // generate area that contains all related cells to each of passed (grown in radius by 1)
        private HashSet<CellComponent> GrowArea(List<CellComponent> cells)
        {
            var area = new HashSet<CellComponent>();

            var dir = Direction.Up;
            do
            {
                Vector2Int offset = dir.GetOffset();

                foreach (CellComponent cell in cells)
                {
                    CellComponent found = _gridSystem.GetCell(cell.GridPosition + offset);
                    if (found != null)
                    {
                        area.Add(found);
                    }
                }

                dir = dir.RotateCW();
            }
            while (dir != Direction.Up);

            return area;
        }

        private IEnumerator PlayGravityAnimation()
        {
            if (!IsAnimatingAllowed())
            {
                yield break;
            }

            StopAnimationCancellation();

            int order = 0;
            while (_animatingCells.ContainsValue(order))
            {
                var animCollection = _animatingCells.Where(pair => order == pair.Value);
                foreach (var pair in animCollection)
                {
                    CellComponent cell = pair.Key;
                    cell.Presenter.GravityPresenter.PlayAnimation();
                }

                yield return new WaitForSeconds(_gravityAnimationDelta);

                order += 1;
            }

            _playedInitialAnimation = true;
            if (_stabilizationSystem.IsFieldStable)
            {
                RequestAnimationPlay();
            }
        }

        private IEnumerator CheckAnimationCancel()
        {
            yield return new WaitUntil(() => !_stabilizationSystem.IsFieldStable);

            LeanTween.cancel(_timerDescr.id);
            _timerDescr = null;
            _cancellationCoroutine = null;
        }

        private bool IsAnimatingAllowed()
        {
            foreach (CellComponent cell in _gridSystem.Cells)
            {
                if (cell.Gravity != CellComponent.DefaultGravity)
                {
                    return true;
                }
            }

            return false;
        }

        private void StopAnimationCancellation()
        {
            if (_cancellationCoroutine != null)
            {
                StopCoroutine(_cancellationCoroutine);
                _cancellationCoroutine = null;
            }
        }
    }
}