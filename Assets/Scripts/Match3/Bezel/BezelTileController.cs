﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.Bezel
{
    public class BezelTileController : MonoBehaviour
    {
        [SerializeField] private BezelBrush _brush;

        [Inject] private ChipComponent _chip;
        [Inject] private BezelSystem _bezelSystem;

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<ChipComponent>().FromComponentInParents(includeInactive: true, useFirst: true);
        }

        protected void Awake()
        {
            _chip.CellChanged += OnChipChanged;
        }

        private void OnChipChanged(ChipComponent chip, CellComponent oldCell)
        {
            if (isActiveAndEnabled)
            {
                Erase(oldCell);
                Paint(_chip.Cell);
            }
        }

        protected void OnEnable()
        {
            Paint(_chip.Cell);
        }

        protected void OnDisable()
        {
            Erase(_chip.Cell);
        }

        private void Paint(CellComponent cell)
        {
            if (cell != null)
            {
                _bezelSystem.Paint(_brush, cell.GridPosition);
            }
        }

        private void Erase(CellComponent cell)
        {
            if (cell != null)
            {
                _bezelSystem.Erase(_brush, cell.GridPosition);
            }
        }
    }
}