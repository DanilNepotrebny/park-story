﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.Tilemaps;

namespace DreamTeam.Match3.Bezel
{
    public partial class TilemapFader
    {
        private class TileFader
        {
            private const int _debrisSortingOrderMax = 10000;

            private int _debrisSortingOrder = _debrisSortingOrderMax;

            private Vector3Int _position;
            
            private Tilemap _tilemap;
            private float _fadingTime;
            
            private float _tileFadingCurrentTime = float.MaxValue;
            private float _tileFadingStartingAlpha;

            private bool _isDirty;
            private Sprite _oldTileSprite;
            private BezelTile.CellCornerType _oldTileMask;
            private float _oldTileAlpha;

            public TileFader(
                    Tilemap tilemap,
                    Vector3Int position,
                    float fadingTime
                )
            {
                _tilemap = tilemap;
                _position = position;
                _fadingTime = fadingTime;
            }
            
            public void OnTileChanging(BezelTile oldTile, BezelTile newTile)
            {
                if (_isDirty)
                {
                    return;
                }

                _isDirty = true;

                _oldTileSprite = oldTile?.Sprite;
                _oldTileMask = oldTile?.CornerType.ResetUnusedBits() ?? 0;
                _oldTileAlpha = oldTile != null ? _tilemap.GetColor(_position).a : 0.0f;
            }

            public void Update()
            {
                if (_isDirty)
                {
                    _isDirty = false;

                    BezelTile newTile = _tilemap.GetTile<BezelTile>(_position);
                    BezelTile.CellCornerType newMask = newTile?.CornerType.ResetUnusedBits() ?? 0;

                    TryCreateOldTileDebris(newMask);

                    if (newTile != null)
                    {
                        bool isFadingIn = (~_oldTileMask & newMask) != 0;

                        if (isFadingIn)
                        {
                            _tileFadingCurrentTime = 0;
                            _tileFadingStartingAlpha = 0;
                        }
                        else
                        {
                            _tileFadingCurrentTime = float.MaxValue;
                            SetTileAlpha(1.0f);
                        }
                    }
                }

                if (_tileFadingCurrentTime <= _fadingTime)
                {
                    _tileFadingCurrentTime += Time.deltaTime;

                    float alpha = Mathf.Lerp(
                            _tileFadingStartingAlpha,
                            1.0f,
                            Mathf.Clamp(_tileFadingCurrentTime / _fadingTime, 0.0f, 1.0f)
                        );

                    SetTileAlpha(alpha);
                }
            }

            private void TryCreateOldTileDebris(BezelTile.CellCornerType newMask)
            {
                if (_oldTileSprite != null)
                {
                    float fadingTime = _tileFadingCurrentTime < _fadingTime ? _tileFadingCurrentTime : _fadingTime;

                    float startingAlpha;
                    float targetAlpha;
                    int sortingOrder;

                    bool isFadingOut = (_oldTileMask & ~newMask) != 0;

                    if (isFadingOut)
                    {
                        startingAlpha = _oldTileAlpha;
                        targetAlpha = 0.0f;
                        sortingOrder = _debrisSortingOrder;
                    }
                    else
                    {
                        startingAlpha = 1.0f;
                        targetAlpha = 1.0f;
                        sortingOrder = -_debrisSortingOrder;
                    }

                    GameObject go = new GameObject(
                            $"{_tilemap.name}Debris[{_position.x}; {_position.y}]",
                            typeof(SpriteRenderer)
                        );

                    go.transform.position = _tilemap.GetCellCenterWorld(_position);

                    SpriteRenderer r = go.GetComponent<SpriteRenderer>();
                    r.sprite = _oldTileSprite;

                    r.sortingLayerID = _tilemap.GetComponent<TilemapRenderer>().sortingLayerID;
                    r.sortingOrder = sortingOrder;

                    Color color = r.color;
                    color.a = startingAlpha;
                    r.color = color;

                    color.a = targetAlpha;
                    LeanTween.color(r.gameObject, color, fadingTime).setDestroyOnComplete(true);

                    _debrisSortingOrder--;
                }
            }

            private void SetTileAlpha(float alpha)
            {
                Color color = _tilemap.GetColor(_position);
                color.a = alpha;
                _tilemap.SetColor(_position, color);
            }
        }
    }
}