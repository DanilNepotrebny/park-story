﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace DreamTeam.Match3.Bezel
{
    [RequireComponent(typeof(Tilemap))]
    public partial class TilemapFader : MonoBehaviour, ITilemapChangeListener
    {
        [SerializeField] private float _debrisFadingTime;
        
        private Dictionary<Vector3Int, TileFader> _tileFaders = new Dictionary<Vector3Int, TileFader>();
        private Tilemap _tilemap;
        
        protected void Awake()
        {
            _tilemap = GetComponent<Tilemap>();
        }

        protected void LateUpdate()
        {
            foreach (KeyValuePair<Vector3Int, TileFader> pair in _tileFaders)
            {
                pair.Value.Update();
            }
        }

        #region ITilemapChangeListener

        void ITilemapChangeListener.OnTileChanging(Vector3Int position, BezelTile oldTile, BezelTile newTile)
        {
            if (!isActiveAndEnabled)
            {
                return;
            }

            TileFader tileFader;
            if (!_tileFaders.ContainsKey(position))
            {
                tileFader = new TileFader(_tilemap, position, _debrisFadingTime);
                _tileFaders.Add(position, tileFader);
            }
            else
            {
                tileFader = _tileFaders[position];
            }

            tileFader.OnTileChanging(oldTile, newTile);
        }

        #endregion
    }
}