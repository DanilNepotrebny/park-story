﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Match3.Bezel
{
    public interface ITilemapChangeListener
    {
        void OnTileChanging(Vector3Int position, BezelTile oldTile, BezelTile newTile);
    }
}