﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Tilemaps;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DreamTeam.Match3.Bezel
{
    [CreateAssetMenu(fileName = "BezelBrush", menuName = "Bezel/Brush")]
    [CustomGridBrush(false, true, false, "BezelBrush")]
    public class BezelBrush : GridBrushBase
    {
        [SerializeField] private BezelTile[] _tiles;

        public BezelTile.CellCornerType CurrentCornerType { get; set; }

        public override void Paint(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
        {
            Paint(FindTilemap(brushTarget), position, CurrentCornerType);
        }

        public void Paint([NotNull] Tilemap tilemap, Vector3Int position, BezelTile.CellCornerType targetCornerType)
        {
            BezelTile existedTile = tilemap.GetTile(position) as BezelTile;
            if (existedTile != null)
            {
                targetCornerType |= existedTile.CornerType.ResetUnusedBits();
            }

            BezelTile tile = _tiles.SingleOrDefault(t => targetCornerType == t.CornerType.ResetUnusedBits());
            if (tile != existedTile)
            {
                #if UNITY_EDITOR
                {
                    Undo.RecordObject(tilemap, "Adding field bezel tile from tilemap");
                }
                #endif

                SetTile(tilemap, position, tile);
            }
        }

        public override void Erase(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
        {
            Erase(FindTilemap(brushTarget), position, CurrentCornerType);
        }

        public void Erase([NotNull] Tilemap tilemap, Vector3Int position, BezelTile.CellCornerType targetCornerType)
        {
            BezelTile existedTile = tilemap.GetTile(position) as BezelTile;
            if (existedTile != null)
            {
                targetCornerType = existedTile.CornerType.ResetUnusedBits() & ~targetCornerType;

                #if UNITY_EDITOR
                {
                    Undo.RegisterCompleteObjectUndo(tilemap, "Erasing field bezel tile from tilemap");
                }
                #endif

                BezelTile tile = _tiles.SingleOrDefault(t => targetCornerType == t.CornerType.ResetUnusedBits());
                SetTile(tilemap, position, tile);
            }
        }

        private static void SetTile(Tilemap tilemap, Vector3Int position, BezelTile tile)
        {
            ITilemapChangeListener listener = tilemap.GetComponent<ITilemapChangeListener>();
            BezelTile oldTile = tilemap.GetTile<BezelTile>(position);
            if (oldTile != tile)
            {
                listener?.OnTileChanging(position, oldTile, tile);
            }

            tilemap.SetTile(position, tile);
        }

        private Tilemap FindTilemap(GameObject brushTarget)
        {
            var tilemap = brushTarget.GetComponent<Tilemap>();
            if (tilemap == null)
            {
                throw new ArgumentException("Brush target contains no tilemap.");
            }

            return tilemap;
        }
    }

    #if UNITY_EDITOR

    [CustomEditor(typeof(BezelBrush))]
    public class FieldBezelBrushEditor : GridBrushEditorBase
    {
        public override void OnPaintInspectorGUI()
        {
            var brush = target as BezelBrush;
            brush.CurrentCornerType = (BezelTile.CellCornerType) EditorGUILayout.EnumFlagsField("Corner type", brush.CurrentCornerType);

            EditorGUILayout.Separator();

            base.OnPaintInspectorGUI();
        }
    }

    #endif
}