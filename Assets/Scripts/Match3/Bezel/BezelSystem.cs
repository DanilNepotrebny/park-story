﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Tilemaps;
using Zenject;

namespace DreamTeam.Match3.Bezel
{
    public class BezelSystem : MonoBehaviour
    {
        [SerializeField] private TilemapDictionary _tilemaps;

        [Inject] private GridSystem _grid;

        public void Paint(BezelBrush brush, Vector2Int cellPosition)
        {
            Tilemap tilemap = _tilemaps[brush];
            Paint(brush, cellPosition, tilemap);
        }

        public void Paint(BezelBrush brush, Vector2Int cellPosition, Tilemap tilemap)
        {
            ForEachCorner(tilemap, cellPosition, brush.Paint);
        }

        public void Erase(BezelBrush brush, Vector2Int cellPosition)
        {
            Tilemap tilemap = _tilemaps[brush];
            Erase(brush, cellPosition, tilemap);
        }

        public void Erase(BezelBrush brush, Vector2Int cellPosition, Tilemap tilemap)
        {
            ForEachCorner(tilemap, cellPosition, brush.Erase);
        }

        private void ForEachCorner(
                Tilemap tilemap,
                Vector2Int cellPosition,
                Action<Tilemap, Vector3Int, BezelTile.CellCornerType> action
            )
        {
            foreach (BezelTile.CellCornerType corner in GetCorners())
            {
                Vector3Int position = GridToTilemap(tilemap, cellPosition + GetOffset(corner));
                action(tilemap, position, corner);
            }
        }

        private IEnumerable<BezelTile.CellCornerType> GetCorners()
        {
            yield return BezelTile.CellCornerType.TopLeft;
            yield return BezelTile.CellCornerType.TopRight;
            yield return BezelTile.CellCornerType.BottomRight;
            yield return BezelTile.CellCornerType.BottomLeft;
        }

        private Vector2Int GetOffset(BezelTile.CellCornerType corner)
        {
            switch (corner)
            {
                case BezelTile.CellCornerType.TopLeft:
                    return Vector2Int.up;

                case BezelTile.CellCornerType.TopRight:
                    return Vector2Int.one;

                case BezelTile.CellCornerType.BottomRight:
                    return Vector2Int.right;

                case BezelTile.CellCornerType.BottomLeft:
                    return Vector2Int.zero;
            }

            throw new InvalidEnumArgumentException();
        }

        private Vector3Int GridToTilemap(Tilemap tilemap, Vector2Int cellPosition)
        {
            if (!Application.isPlaying)
            {
                _grid = FindObjectOfType<GridSystem>();
            }

            return tilemap.WorldToCell(_grid.GridToWorld(cellPosition));
        }

        #region Inner types

        [Serializable]
        private class TilemapDictionary : KeyValueList<BezelBrush, Tilemap>
        {
        }

        #endregion
    }
}