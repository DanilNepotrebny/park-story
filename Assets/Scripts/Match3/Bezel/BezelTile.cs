﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

namespace DreamTeam.Match3.Bezel
{
    [CreateAssetMenu(fileName = "BezelTile", menuName = "Bezel/Tile")]
    public class BezelTile : TileBase
    {
        public CellCornerType CornerType => _cornerType;
        [SerializeField, EnumFlag] private CellCornerType _cornerType;

        [SerializeField, FormerlySerializedAs("_sprite")]
        private Sprite _defaultSprite;

        [SerializeField, Tooltip("Any additional sprite can be chosen randomly instead of default sprite")]
        private Sprite[] _additionalSprites;

        public Sprite Sprite => _defaultSprite;

        public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
        {
            tileData.sprite = GetSprite();
        }

        private Sprite GetSprite()
        {
            if (_additionalSprites == null || _additionalSprites.Length == 0)
            {
                return _defaultSprite;
            }

            int idx = Random.Range(0, _additionalSprites.Length + 1);
            return idx == _additionalSprites.Length ?
                _defaultSprite :
                _additionalSprites[idx];
        }

        [Flags]
        public enum CellCornerType
        {
            TopLeft = 1,
            TopRight = 1 << 1,
            BottomRight = 1 << 2,
            BottomLeft = 1 << 3
        }
    }

    public static class ExtensionMethods
    {
        private static int mask = 0;

        /// <summary>
        /// Resets bits outside of enum values to 0
        /// </summary>
        /// <param name="type">this</param>
        /// <returns>this</returns>
        public static BezelTile.CellCornerType ResetUnusedBits(this BezelTile.CellCornerType type)
        {
            if (mask == 0)
            {
                Array values = Enum.GetValues(typeof(BezelTile.CellCornerType));
                foreach (object value in values)
                {
                    mask |= (int)value;
                }
            }

            type &= (BezelTile.CellCornerType)mask;
            return type;
        }
    }
}