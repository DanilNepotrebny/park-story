﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    public interface IChipConsumptionHandler
    {
        /// <summary>
        /// Get chips tags that supported by the handler and should be checked
        /// </summary>
        ChipTagSet GetValidChipTags();

        /// <summary>
        /// Order in the priority to obtain OnConsuming() method call
        /// </summary>
        int GetPriorityOrder();

        /// <summary>
        /// Method is called when a chip began to consume
        /// </summary>
        /// <param name="chip">Consuming chip</param>
        /// <param name="consumptionHandle">Consuming handle</param>
        /// <param name="isModal">Is consumption handling modal</param>
        void OnConsuming([NotNull] ChipComponent chip, IDeferredInvocationHandle consumptionHandle, ref bool isModal);

        /// <summary>
        /// Called when the chip is going be consumed due to recently scheduled impact
        /// </summary>
        /// <param name="chip"> Chip that is going to be consumed </param>
        void OnConsumptionScheduled([NotNull] ChipComponent chip);

        /// <summary>
        /// Called when scheduled impact to the chip that led to consume was cancelled
        /// </summary>
        /// <param name="chip"> Chip that might be consumed </param>
        /// <param name="impactGroupId"> Cancelled impact group id </param>
        void OnScheduledConsumptionCancelled([NotNull] ChipComponent chip, int impactGroupId);
    }

    public class ChipConsumptionDispatcher : MonoBehaviour, IImpactController
    {
        [Inject] private GenerationSystem _generationSystem;
        [Inject] private GridSystem _gridSystem;

        private readonly HashSet<IChipConsumptionHandler> _handlers = new HashSet<IChipConsumptionHandler>();
        private readonly Dictionary<Impact, ChipComponent> _scheduledImpacts = new Dictionary<Impact, ChipComponent>();

        public bool Register([NotNull] IChipConsumptionHandler handler)
        {
            return _handlers.Add(handler);
        }

        public bool Unregister([NotNull] IChipConsumptionHandler handler)
        {
            return _handlers.Remove(handler);
        }

        #region IImpactController

        void IImpactController.OnImpact(
                ChipComponent chip,
                ImpactFlag impactType,
                IDeferredInvocationHandle consumeHandle,
                ref bool isModal
            )
        {
            if (!chip.IsConsumed)
            {
                return;
            }

            isModal = false;
            IChipConsumptionHandler watcher = GetFirstChipWatcher(chip);
            watcher?.OnConsuming(chip, consumeHandle, ref isModal);
        }

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.ChipConsumptionDispatcher;
        }

        void IImpactController.Cancel()
        {
            // nothing to do
        }

        #endregion IImpactController

        protected void Awake()
        {
            _generationSystem.ChipGenerated += chip => chip.AddImpactController(this);
            foreach (CellComponent cell in _gridSystem.Cells)
            {
                cell.ImpactScheduled += OnImpactScheduled;
            }
        }

        private void OnImpactScheduled([NotNull] CellComponent cell, Impact impact)
        {
            ChipComponent chip = cell.Chip;
            if (chip == null || !chip.WillBeConsumed())
            {
                return;
            }

            _scheduledImpacts[impact] = chip;
            impact.Cancelling += OnImpactCancelling;
            IChipConsumptionHandler watcher = GetFirstChipWatcher(chip);
            watcher?.OnConsumptionScheduled(chip);
        }

        private void OnImpactCancelling(Impact impact)
        {
            impact.Cancelling -= OnImpactCancelling;

            ChipComponent chip = _scheduledImpacts[impact];
            IChipConsumptionHandler watcher = GetFirstChipWatcher(chip);
            watcher?.OnScheduledConsumptionCancelled(chip, impact.GroupId);

            _scheduledImpacts.Remove(impact);
        }

        private IChipConsumptionHandler GetFirstChipWatcher([NotNull] ChipComponent chip)
        {
            var handlers = new List<IChipConsumptionHandler>();

            foreach (IChipConsumptionHandler handler in _handlers)
            {
                ChipTagSet validChipTags = handler.GetValidChipTags();
                ChipTag chipTag = chip.Tags.FindFirstOf(validChipTags, false);
                if (chipTag != null)
                {
                    handlers.Add(handler);
                }
            }

            return handlers.OrderByDescending(h => h.GetPriorityOrder()).FirstOrDefault();
        }
    }
}