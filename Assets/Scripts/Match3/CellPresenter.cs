﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using DreamTeam.Utils.RendererSorting;
using TouchScript.Gestures;
using TouchScript.Gestures.TransformGestures;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(TapGesture), typeof(TransformGesture), typeof(CellComponent))]
    public class CellPresenter : MonoBehaviour, ISortingOptionsProvider
    {
        [SerializeField] private CellComponent _cell;
        [SerializeField] private CellGravityPresenter _gravityPresenter;
        [SerializeField] private TapGesture _tapGesture;
        [SerializeField] private TapGesture _doubleTapGesture;
        [SerializeField] private TransformGesture _transformGesture;
        [SerializeField] private SortingGroupOptionsProvider _chipSortingGroupOptionsProvider;
        [Inject] private Instantiator _instantiator;

        private Renderer _renderer;

        public CellComponent CellComponent => _cell;

        public CellGravityPresenter GravityPresenter => _gravityPresenter;

        public TapGesture SingleTapGesture => _tapGesture;
        public TapGesture DoubleTapGesture => _doubleTapGesture;
        public TransformGesture SwapGesture => _transformGesture;

        public SortingOptions SortingOptions { get; private set; }

        public event Action<CellPresenter, Vector2> CellSwiped;
        public event Action<CellComponent> CellTapped;

        /// <summary> Finds all sortable views in this cell (cell, chip, underlay, etc.) and returns them in a list. </summary>
        public List<ISortingOptionsProvider> FindSortingOptionsProviders()
        {
            var providers = new List<ISortingOptionsProvider>();

            var underlay = CellComponent.Underlay;

            if (_chipSortingGroupOptionsProvider != null)
            {
                providers.Add(_chipSortingGroupOptionsProvider);
            }

            if (underlay != null)
            {
                providers.Add(underlay.Presenter);
            }

            return providers;
        }

        protected void Awake()
        {
            _renderer = GetComponent<SpriteRenderer>();
            SortingOptions = _instantiator.Instantiate<SortingOptions>(
                    _renderer.sortingLayerID,
                    _renderer.sortingOrder
                );
            SortingOptions.SortingLayerChanged += OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged += OnSortingOrderChanged;

            _transformGesture.Transformed += OnSwiped;
            _doubleTapGesture.Tapped += OnDoubleTapped;
            _tapGesture.Tapped += OnTapGuestureTapped;
        }

        protected void OnDestroy()
        {
            _doubleTapGesture.Tapped -= OnDoubleTapped;
            _tapGesture.Tapped -= OnTapGuestureTapped;
            _transformGesture.Transformed -= OnSwiped;

            SortingOptions.SortingLayerChanged -= OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged -= OnSortingOrderChanged;
            SortingOptions.Dispose();
            SortingOptions = null;
        }

        private void OnSwiped(object sender, EventArgs eventArgs)
        {
            CellSwiped?.Invoke(this, _transformGesture.DeltaPosition);
            _transformGesture.Cancel();
        }

        private void OnDoubleTapped(object sender, EventArgs eventArgs)
        {
            _cell.Chip?.Presenter?.OnDoubleTapped();
        }

        private void OnSortingOrderChanged(SortingOptions options, int prevorder)
        {
            _renderer.sortingOrder = options.SortingOrder;
        }

        private void OnSortingLayerChanged(SortingOptions options)
        {
            _renderer.sortingLayerID = options.SortingLayerId;
        }

        private void OnTapGuestureTapped(object sender, EventArgs e)
        {
            CellTapped?.Invoke(_cell);
        }
    }
}