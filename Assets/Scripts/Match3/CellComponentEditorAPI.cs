﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Match3
{
    public partial class CellComponent : CellComponent.IEditorAPI
    {
        private Action _gravityChanged;

        ChipComponent IEditorAPI.Chip => ((IEditorAPI)this).ChipContainer?.Chip;
        ChipContainerComponent IEditorAPI.ChipContainer => gameObject.GetComponent<ChipContainerComponent>();

        Direction IEditorAPI.Gravity
        {
            get { return _gravity; }
            set
            {
                Undo.RecordObject(this, "Change gravity");
                _gravity = value;

                _gravityChanged?.Invoke();
            }
        }

        event Action<ChipComponent, ChipComponent> IEditorAPI.ChipChanged
        {
            add { ((IEditorAPI)this).ChipContainer.ChipChanged += value; }
            remove { ((IEditorAPI)this).ChipContainer.ChipChanged -= value; }
        }

        event Action IEditorAPI.GravityChanged
        {
            add { _gravityChanged += value; }
            remove { _gravityChanged -= value; }
        }

        protected void OnDrawGizmos()
        {
            if (!_allowBoosterChipGeneration)
            {
                Gizmos.DrawIcon(transform.position + new Vector3(-0.4f, 0.4f), "BoosterDisallowIcon.png");
            }

            if (!IsMoveGenerationEnabled)
            {
                Gizmos.DrawIcon(transform.position + new Vector3(0.4f, -0.4f), "CellDisallowMoveGeneration.png");
            }

            if (!_hasGeneratedChip)
            {
                Gizmos.DrawIcon(transform.position, "HasNoGeneratedChip.png");
            }

            var drawnIcons = new HashSet<ProcessType>();

            foreach (ProcessType processType in _activeProcesses)
            {
                if (!drawnIcons.Contains(processType))
                {
                    Gizmos.DrawIcon(transform.position, $"Cell{processType}.png");
                    drawnIcons.Add(processType);
                }
            }
        }

        void IEditorAPI.SetAllowBoosterChipGeneration(bool value)
        {
            _allowBoosterChipGeneration = value;
        }

        void IEditorAPI.SetMoveGenerationEnabled(bool isEnabled)
        {
            _isMoveGenerationEnabled = isEnabled;
        }

        void IEditorAPI.SetHasGeneratedChip(bool value)
        {
            _hasGeneratedChip = value;
        }

        public interface IEditorAPI
        {
            ChipComponent Chip { get; }
            ChipContainerComponent ChipContainer { get; }
            Direction Gravity { get; set; }

            void SetHasGeneratedChip(bool value);
            void SetAllowBoosterChipGeneration(bool value);
            void SetMoveGenerationEnabled(bool isEnabled);

            event Action<ChipComponent, ChipComponent> ChipChanged;
            event Action GravityChanged;
        }
    }
}

#endif