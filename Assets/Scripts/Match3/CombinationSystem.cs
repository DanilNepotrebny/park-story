﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3
{
    public class CombinationSystem : MonoBehaviour, SwapSystem.ISwapHandler
    {
        [SerializeField] private Object[] _controllers;

        [Inject]
        private void Construct(SwapSystem swapSystem)
        {
            swapSystem.RegisterSwapHandler(this);
        }

        public bool HasCombination(
                [CanBeNull] ChipComponent mainChip,
                [CanBeNull] ChipComponent additionalChip
            )
        {
            return FindSuitableController(ref mainChip, ref additionalChip) != null;
        }

        public List<Impact> ScheduleCombinationImpact(
                [CanBeNull] ChipComponent mainChip,
                [CanBeNull] ChipComponent additionalChip
            )
        {
            ICombinationController controller = FindSuitableController(ref mainChip, ref additionalChip);
            if (controller != null)
            {
                Assert.IsNotNull(mainChip);

                return controller.ScheduleCombinationImpact(mainChip, additionalChip);
            }

            return new List<Impact>();
        }

        private ICombinationController FindSuitableController(
                [CanBeNull] ref ChipComponent mainChip,
                [CanBeNull] ref ChipComponent additionalChip
            )
        {
            if (mainChip == null && additionalChip == null)
            {
                return null;
            }

            if (mainChip == null)
            {
                // Main chip is guranteed to not be null, so need to swap references.
                mainChip = additionalChip;
                additionalChip = null;
            }

            foreach (Object obj in _controllers)
            {
                var controller = obj as ICombinationController;
                if (controller == null)
                {
                    continue;
                }

                if (controller.HasCombination(mainChip, additionalChip))
                {
                    return controller;
                }

                if (additionalChip != null && controller.HasCombination(additionalChip, mainChip))
                {
                    ChipComponent tmp = mainChip;
                    mainChip = additionalChip;
                    additionalChip = tmp;

                    return controller;
                }
            }

            return null;
        }

        bool SwapSystem.ISwapHandler.ShouldSwapSucceed(CellComponent cellFrom, CellComponent cellTo)
        {
            return HasCombination(cellFrom.Chip, cellTo.Chip);
        }

        void SwapSystem.ISwapHandler.HandleSwap(CellComponent cellFrom, CellComponent cellTo)
        {
            ChipComponent mainChip = cellFrom.Chip;
            ChipComponent additionalChip = cellTo.Chip;

            ICombinationController controller = FindSuitableController(ref mainChip, ref additionalChip);
            if (controller != null)
            {
                Assert.IsNotNull(mainChip);

                controller.AcceptCombination(mainChip, additionalChip);
            }
        }
    }
}