﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Tweening;
using UnityEngine;

namespace DreamTeam.Match3
{
    [CreateAssetMenu(menuName = "Match 3/ChipTweenMovementPresenterSettings")]
    public class ChipTweenMovementPresenterSettings : ScriptableObject
    {
        [SerializeField] private float _movingTime = 0.2f;
        [SerializeField] private Easing _easing;
        [SerializeField] private ChipComponent.MovingType _movingType;

        public float MovingTime => _movingTime;
        public Easing Easing => _easing;
        public ChipComponent.MovingType MovingType => _movingType;
    }
}