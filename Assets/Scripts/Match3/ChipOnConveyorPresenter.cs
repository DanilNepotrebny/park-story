﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Conveyor;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(ChipPresenter))]
    public class ChipOnConveyorPresenter : MonoBehaviour
    {
        [Inject] private ConveyorSystem _conveyorSystem;

        private bool _isStarted;

        private ChipPresenter _presenter => GetComponent<ChipPresenter>();

        protected void Start()
        {
            _isStarted = true;
            OnCellChanged(_presenter.ChipComponent, null);
        }

        protected void OnEnable()
        {
            _presenter.ChipComponent.CellChanged += OnCellChanged;

            if (_isStarted)
            {
                OnCellChanged(_presenter.ChipComponent, null);
            }
        }

        protected void OnDisable()
        {
            _presenter.ChipComponent.CellChanged -= OnCellChanged;
        }

        private void OnCellChanged([NotNull] ChipComponent chip, [CanBeNull] CellComponent prevCell)
        {
            Assert.IsTrue(chip == _presenter.ChipComponent, "Passing chip must be equal to presenter one");
            if (_conveyorSystem.HasAnyBlock(_presenter.ChipComponent.Cell))
            {
                _presenter.SetIdleAnimationType(ChipPresenter.IdleAnimationType.Conveyor);
            }
        }
    }
}
