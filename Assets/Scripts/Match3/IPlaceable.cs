﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Match3
{
    public interface IPlaceable
    {
        Type GetElementType();
        void Place(Vector2Int gridPos);
        void Rotate();

        RectInt Bounds { get; }
        int Square { get; }
    }
}
