﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Inventory;
using DreamTeam.Match3.ChipBoosterPlacingRules;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Utils;
using JetBrains.Annotations;
using ModestTree;
using UnityEngine;
using Zenject;
using Assert = UnityEngine.Assertions.Assert;

namespace DreamTeam.Match3
{
    /// <summary>
    /// This system generates equipped boosters and replaces existed chips (except predefined ones)
    /// </summary>
    public class ChipBoosterGenerationSystem : MonoBehaviour
    {
        [Inject] private EquippedChipBoosters _equippedChipBoosters;
        [Inject] private GridSystem _gridSystem;
        [Inject] private LevelStartNotifier _levelStartNotifier;
        [Inject] private GenerationSystem _generationSystem;
        [Inject] private Instantiator _instantiator;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        private readonly List<PlacingChip> _chipsToGenerate = new List<PlacingChip>(5);
        private List<CellComponent> _replaceableCells;

        private const int _spendingAmount = 1;
        private const ImpactFlag _replaceImpactType = ImpactFlag.BonusReplace;

        /// <summary>
        /// Generates chips from equipped boosters. Generated chips will replace existed ones in destionation cells
        /// </summary>
        public void GenerateEquippedBoosters()
        {
            FillChipsToGenerate(_equippedChipBoosters);

            if (_chipsToGenerate.Count != 0)
            {
                FindReplaceableCells(EquippedBoosterGenerationRule);
                GenerateChips();

                _levelStartNotifier.LevelStarted += OnLevelStarted;
            }
        }

        public void GenerateBoosters(ICollection<ChipBooster> boosters)
        {
            FillChipsToGenerate(boosters);

            if (_chipsToGenerate.Count != 0)
            {
                FindReplaceableCells(DefaultBoosterGenerationRule);
                GenerateChips();
            }
        }

        protected void Awake()
        {
            _replaceableCells = new List<CellComponent>(_gridSystem.Height * _gridSystem.Width);
        }

        private void FillChipsToGenerate(IEnumerable<ChipBooster> boosters)
        {
            _chipsToGenerate.Clear();
            boosters.ForEach(
                    booster =>
                    {
                        foreach (ChipComponent chipPrefab in booster.ChipPrefabs)
                        {
                            BaseChipBoosterPlacingRule placingRule = booster.PlacingRulePrefab != null ?
                                _instantiator.Instantiate(booster.PlacingRulePrefab) :
                                null;

                            _chipsToGenerate.Add(
                                    new PlacingChip
                                    {
                                        ChipPrefab = chipPrefab,
                                        PlacingRule = placingRule
                                    }
                                );
                        }
                    }
                );
        }

        private void GenerateChips()
        {
            PutChipsOnCells(_chipsToGenerate, _replaceableCells);

            Assert.IsTrue(
                    _chipsToGenerate.Count == 0,
                    $"Not enough valid cells where boosters ({_chipsToGenerate.Count}) can be generated. Check level design"
                );
        }

        private void PutChipsOnCells(List<PlacingChip> chips, List<CellComponent> cells)
        {
            chips.Sort(
                    (left, right) =>
                    {
                        if (left.PlacingRule == null && right.PlacingRule == null ||
                            left.PlacingRule != null && right.PlacingRule != null)
                        {
                            return 0;
                        }

                        return left.PlacingRule == null ? 1 : -1;
                    }
                );

            foreach (PlacingChip placingChip in chips)
            {
                ChipComponent chip = _generationSystem.InstantiateChip(placingChip.ChipPrefab);

                bool isPlaced = false;
                for (int i = 0; i < cells.Count; ++i)
                {
                    CellComponent cell = cells[i];
                    bool canBePlaced = placingChip.PlacingRule == null ||
                        placingChip.PlacingRule.CanBePlaced(chip, cell);

                    if (canBePlaced)
                    {
                        chip.transform.position = cell.transform.position;
                        cell.ForceReplaceChip(chip, _replaceImpactType);
                        cells.RemoveAt(i);
                        isPlaced = true;
                        break;
                    }
                }

                Assert.IsTrue(isPlaced, $"Didn't find a place to put booster chip '{chip.name}' into");
            }

            chips.Clear();
            cells.Clear();
        }

        private void FindReplaceableCells(Predicate<CellComponent> isValidCondition)
        {
            _replaceableCells.Clear();
            foreach (CellComponent cell in _gridSystem.Cells)
            {
                if (!HasAtLeastOneBorderCell(cell) ||
                    !isValidCondition(cell))
                {
                    continue;
                }

                if (cell.IsEmpty || cell.Chip.CanBeImpacted(ImpactFlag.BonusReplace))
                {
                    _replaceableCells.Add(cell);
                }
            }

            _replaceableCells.RandomShuffle(_random);
            _replaceableCells.Sort(ReplaceableCellsComparison);
        }

        private int ReplaceableCellsComparison([NotNull] CellComponent left, [NotNull] CellComponent right)
        {
            bool isLeft = IsCellTheMostSuitableForReplace(left);
            bool isRight = IsCellTheMostSuitableForReplace(right);

            if (isLeft && !isRight)
            {
                return -1;
            }

            if (isRight && !isLeft)
            {
                return 1;
            }

            return 0;
        }

        private bool IsCellTheMostSuitableForReplace([NotNull] CellComponent cell)
        {
            // We check for Replace cause common chip might be required one and in that case it would have no Replace.
            // But as fall back, when we don't have free spot any more, we will use required too
            return cell.IsEmpty || cell.Chip.CanBeImpacted(ImpactFlag.Replace);
        }

        private bool EquippedBoosterGenerationRule([NotNull] CellComponent cell)
        {
            if (!cell.AllowBoosterChipGeneration || !cell.HasGeneratedChip)
            {
                return false;
            }

            return DefaultBoosterGenerationRule(cell);
        }

        private bool DefaultBoosterGenerationRule([NotNull] CellComponent cell)
        {
            return cell.IsEmpty ||
                cell.Chip.CanBeSwapped && cell.Chip.CanBeShuffled;
        }

        private void OnLevelStarted()
        {
            _levelStartNotifier.LevelStarted -= OnLevelStarted;

            _equippedChipBoosters.ForEach(booster => booster.TrySpend(_spendingAmount, GamePlace.InGame));
            _equippedChipBoosters.UnequipAll();
        }

        private bool HasAtLeastOneBorderCell([NotNull] CellComponent cell)
        {
            float radius = 1f;
            bool hasBorderCell = false;

            _gridSystem.ProcessCellsInRadius(
                    (c, dist) => hasBorderCell |= (c != null),
                    cell.GridPosition,
                    radius
                );

            return hasBorderCell;
        }

        #region Inner types

        private struct PlacingChip
        {
            public ChipComponent ChipPrefab { get; set; }
            public BaseChipBoosterPlacingRule PlacingRule { get; set; }
        }

        #endregion
    }
}