﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Match3.Levels;
using DreamTeam.Messaging;
using DreamTeam.Messaging.MessageAssets;
using DreamTeam.SocialNetworking;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    public class LevelDrivenLetterSender : MonoBehaviour
    {
        [SerializeField] private LevelLetterList _levelLetterPairs;

        [Inject(Id = GlobalInstaller.MainLevelPackControllerId)]
        private SingleLevelPackController _mainLevelPackController;

        [Inject] private MessageSystem _messageSystem;
        [Inject] private SocialProfilesSystem _socialProfilesSystem;
        [Inject] private Instantiator _instantiator;

        protected void Awake()
        {
            _mainLevelPackController.CurrentLevelAdvanced += OnCurrentLevelAdvanced;
        }

        protected void OnDestroy()
        {
            _mainLevelPackController.CurrentLevelAdvanced -= OnCurrentLevelAdvanced;
        }

        private void OnCurrentLevelAdvanced(int previousIndex, int currentIndex)
        {
            int previousLevelNumber = currentIndex;

            foreach (KeyValuePair<int, CharacterRewardsMessageAsset> pair in _levelLetterPairs)
            {
                if (pair.Key == previousLevelNumber)
                {
                    foreach (Reward reward in pair.Value.Rewards)
                    {
                        _instantiator.Inject(reward);
                    }

                    CharacterRewardsMessage message = new CharacterRewardsMessage(_instantiator, pair.Value.Rewards, GamePlace.Letter, pair.Value.PopupScene.Name);
                    message.Sender = _socialProfilesSystem.FindProfile(pair.Value.SenderID);
                    _messageSystem.Add(message);
                }
            }
        }

        [Serializable]
        private class LevelLetterList : KeyValueList<int, CharacterRewardsMessageAsset>
        {
        }
    }
}