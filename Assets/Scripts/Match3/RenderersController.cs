﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    [ExecuteInEditMode]
    public class RenderersController : MonoBehaviour
    {
        public List<Renderer> Renderers { get; private set; }

        /// <summary>
        /// Unity callback
        /// </summary>
        protected void Awake()
        {
            Renderers = new List<Renderer>();
            GetComponentsInChildren<Renderer>(true, Renderers);

            int i = 0;
            while (i < Renderers.Count)
            {
                Renderer r = Renderers[i];

                DestroyNotifier destroyNotifier = r.GetComponent<DestroyNotifier>();
                if (destroyNotifier == null)
                {
                    if (r.gameObject != gameObject)
                    {
                        UnityEngine.Debug.LogError($"{r.name} contains no {nameof(DestroyNotifier)} component.", r);
                        Renderers.RemoveAt(i);
                        continue;
                    }
                }
                else
                {
                    destroyNotifier.Destroying += OnRendererDestroying;
                }

                i++;
            }
        }

        private void OnRendererDestroying(GameObject go)
        {
            Renderer r = go.GetComponent<Renderer>();
            if (r != null)
            {
                Renderers.Remove(r);
            }
        }
    }
}