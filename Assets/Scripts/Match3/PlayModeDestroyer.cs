﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    public class PlayModeDestroyer : MonoBehaviour
    {
        protected void Awake()
        {
            gameObject.DisposeImmediate();
        }
    }
}