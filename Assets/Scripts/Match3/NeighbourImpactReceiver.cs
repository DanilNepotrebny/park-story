﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Adds a such property to the chip that it is automatically impacted when one of the neighbour chips is consumed.
    /// </summary>
    [RequireComponent(typeof(ChipComponent))]
    public class NeighbourImpactReceiver : MonoBehaviour, IGroupImpactListener
    {
        [SerializeField] private ChipTagSet _restrictionTags = new ChipTagSet();
        [SerializeField, EnumFlag] private ImpactFlag _restrictionFlags;
        [SerializeField] private ChipTagSet _tagsIgnoringFlags = new ChipTagSet();
        [SerializeField] private float _radius = 1f;
        [SerializeField] private int _impact = -1;

        [Inject] private ChipComponent _chip;
        [Inject] private ImpactSystem _impactSystem;
        [Inject] private GridSystem _grid;

        private readonly Dictionary<int, Impact> _scheduledImpacts = new Dictionary<int, Impact>();

        public void FillAffectingCells(List<CellComponent> cells, ImpactFlag impactFlag)
        {
            _grid.ProcessCellsInRadius(
                    (c, distance) =>
                    {
                        if (ShouldBeImpacted(c.Chip, impactFlag) && !cells.Contains(c))
                        {
                            cells.Add(c);
                        }
                    },
                    _chip.Cell.GridPosition,
                    _radius
                );
        }

        protected void Awake()
        {
            _chip.ContainerCellChanged += OnContainerCellChanged;
            SetSubscriptionToEvents(_chip.ContainerCell, true);
        }

        protected void OnValidate()
        {
            if ((_restrictionFlags & ImpactFlag.Neighbour) > 0)
            {
                UnityEngine.Debug.LogAssertion($"{nameof(NeighbourImpactReceiver)} can't receive impact from " +
                    $"{nameof(ImpactFlag.Neighbour)}. Otherwise update implementation and be aware of stack overflow", this);
                _restrictionFlags ^= ImpactFlag.Neighbour;
            }
        }

        private void OnContainerCellChanged(CellComponent newCell, CellComponent oldCell)
        {
            SetSubscriptionToEvents(oldCell, false);
            SetSubscriptionToEvents(newCell, true);

            CellComponent cell = oldCell != null ? oldCell : newCell;
            List<int> keysList = _scheduledImpacts.Keys.ToList(); // TryCancelImpact may modify _scheduledImpacts collection
            foreach (int groupId in keysList)
            {
                TryCancelImpact(cell, groupId);
            }
        }

        private void SetSubscriptionToEvents([CanBeNull] CellComponent cell, bool isSubscribed)
        {
            if (cell == null)
            {
                return;
            }

            _grid.ProcessCellsInRadius(
                    (c, distance) =>
                    {
                        if (c == cell)
                        {
                            return;
                        }

                        if (isSubscribed)
                        {
                            _impactSystem.AddGroupImpactListener(this, c);
                            c.ImpactCanceled += OnImpactCancelled;
                        }
                        else
                        {
                            _impactSystem.RemoveGroupImpactListener(this, c);
                            c.ImpactCanceled -= OnImpactCancelled;
                        }
                    },
                    cell.GridPosition,
                    _radius
                );
        }

        private void TryCancelImpact([NotNull] CellComponent cell, int groupId)
        {
            bool hasImpact = false;

            _grid.ProcessCellsInRadius(
                    (c, distance) => { hasImpact |= c.ScheduledImpacts.Any(i => i.GroupId == groupId); },
                    cell.GridPosition,
                    _radius
                );

            if (!hasImpact)
            {
                _scheduledImpacts[groupId].Cancel();
                _scheduledImpacts.Remove(groupId);
            }
        }

        private void OnImpactCancelled([NotNull] CellComponent cell, ImpactFlag flag, int groupId)
        {
            if (_scheduledImpacts.ContainsKey(groupId))
            {
                TryCancelImpact(_chip.Cell, groupId);
            }
        }

        private bool ShouldBeImpacted(ChipComponent chip, ImpactFlag flag)
        {
            return chip != null &&
                   chip.IsConsumed &&
                   IsImpactAllowed(chip, flag);
        }

        private bool IsImpactAllowed([NotNull] ChipComponent chip, ImpactFlag flag)
        {
            bool isAllowedByFlag =
                (flag & _restrictionFlags) == flag ||
                chip.Tags.FindFirstOf(_tagsIgnoringFlags) != null;

            if (!isAllowedByFlag)
            {
                return false;
            }

            return _restrictionTags.IsEmpty() || chip.Tags.FindFirstOf(_restrictionTags, false) != null;
        }

        bool IGroupImpactListener.OnGroupImpacting(CellComponent cell, ChipComponent chip, ImpactFlag flag, int groupId)
        {
            if (!_scheduledImpacts.ContainsKey(groupId))
            {
                return false;
            }

            Impact impact = _scheduledImpacts[groupId];
            _scheduledImpacts.Remove(groupId);

            if (ShouldBeImpacted(chip, flag))
            {
                impact.Perform();
            }
            else
            {
                impact.Cancel();
            }

            return true;
        }

        bool IGroupImpactListener.OnGroupImpactScheduled(CellComponent cell, ImpactFlag flag, int groupId)
        {
            if (cell.Chip == null || !IsImpactAllowed(cell.Chip, flag))
            {
                return false;
            }

            Impact impact = _impactSystem.ScheduleImpact(_chip.Cell, _impact, ImpactFlag.Neighbour);
            _scheduledImpacts.Add(groupId, impact);

            return true;
        }
    }
}
