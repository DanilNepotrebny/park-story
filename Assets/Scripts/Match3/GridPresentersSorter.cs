﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using DreamTeam.Utils.RendererSorting;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Setups sorting orders of all suitable views within a <see cref="GridSystem"/> according to sorting layer and other
    /// specified options.
    /// </summary>
    public class GridPresentersSorter : MonoBehaviour
    {
        [SerializeField] private GridSortingOptions[] _options;

        [Inject] private GridSystem _grid;

        protected void Start()
        {
            foreach (var cell in _grid.Cells)
            {
                List<ISortingOptionsProvider> providers = cell.Presenter.FindSortingOptionsProviders();
                if (providers != null && providers.Count > 0)
                {
                    foreach (ISortingOptionsProvider provider in providers)
                    {
                        provider.SortingOptions.SortingLayerChanged += options => UpdateSortingOptions(options, cell.GridPosition);
                        UpdateSortingOptions(provider.SortingOptions, cell.GridPosition);
                    }
                }
            }
        }

        private void UpdateSortingOptions(SortingOptions sortingOptions, Vector2Int gridPosition)
        {
            foreach (GridSortingOptions options in _options)
            {
                if (options.IsSupported(sortingOptions))
                {
                    options.UpdateSortingOptions(sortingOptions, gridPosition);
                }
            }
        }

        #region Inner classes

        [Serializable]
        private class GridSortingOptions
        {
            [SerializeField, SortingLayer] private int _sortingLayer;

            [SerializeField] private bool _isLeftToRight = true;
            [SerializeField] private bool _isTopToBottom = true;

            [SerializeField] private int _orderFactor = 100;

            public bool IsSupported(SortingOptions options)
            {
                return options.SortingLayerId == _sortingLayer;
            }

            public void UpdateSortingOptions(SortingOptions options, Vector2Int gridPos)
            {
                int xSign = _isLeftToRight ? 1 : -1;
                int ySign = _isTopToBottom ? -1 : 1;

                options.SortingOrder = xSign * gridPos.x * _orderFactor + ySign * gridPos.y * _orderFactor;
            }
        }

        #endregion
    }
}