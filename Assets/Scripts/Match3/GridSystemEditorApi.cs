﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Match3
{
    public partial class GridSystem
    {
        private static Func<GridSystem, EditorApi> _editorApiFactory;

        /// <summary>
        /// Editor API for the GridSystem component. Should NOT be used in game logics code.
        /// </summary>
        [InitializeOnLoad]
        public class EditorApi
        {
            private WeakReference<GridSystem> _gridRef;

            /// <summary>
            /// GridSystem object
            /// </summary>
            public GridSystem Grid
            {
                get
                {
                    GridSystem grid;
                    if (_gridRef.TryGetTarget(out grid))
                    {
                        return grid;
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Width of the grid
            /// </summary>
            public int Width
            {
                get { return Grid._width; }
                set { Grid._width = value; }
            }

            /// <summary>
            /// Height of the grid
            /// </summary>
            public int Height
            {
                get { return Grid._height; }
                set { Grid._height = value; }
            }

            /// <summary>
            /// Width of the cell
            /// </summary>
            public float CellWidth
            {
                get { return Grid._cellWidth; }
                set { Grid._cellWidth = value; }
            }

            /// <summary>
            /// Height of the cell
            /// </summary>
            public float CellHeight
            {
                get { return Grid._cellHeight; }
                set { Grid._cellHeight = value; }
            }

            /// <summary>
            /// Raw access to grid cells array
            /// </summary>
            public CellComponent[] Cells
            {
                get { return Grid._cells; }
                set { Grid._cells = value; }
            }

            /// <summary>
            /// Indexer for getting or setting cells.
            /// </summary>
            public CellComponent this[int x, int y]
            {
                get { return Grid[x, y]; }
                set { Grid[x, y] = value; }
            }

            public void InsertCell(Vector2Int cellPos, CellComponent cell)
            {
                if (cell == null)
                {
                    this[cellPos.x, cellPos.y] = null;
                    return;
                }

                cell.gameObject.transform.parent = Grid.transform;
                SetCellPosition(cell, cellPos);
            }

            public void OptimizeBounds()
            {
                RectInt bounds = Grid.CalculateRealBounds();

                if (bounds.min != Vector2Int.zero)
                {
                    for (int y = bounds.yMin; y < bounds.yMax; y++)
                    {
                        for (int x = bounds.xMin; x < bounds.xMax; x++)
                        {
                            CellComponent cell = this[x, y];
                            if (cell != null)
                            {
                                this[x, y] = null;
                                SetCellPosition(cell, new Vector2Int(x, y) - bounds.min);
                            }
                        }
                    }
                }

                if (bounds.width != Width || bounds.height != Height)
                {
                    if (bounds.width != Width)
                    {
                        for (int y = 0; y < bounds.height; y++)
                        {
                            for (int x = 0; x < bounds.width; x++)
                            {
                                int newIndex = CalculateIndex(bounds.width, x, y);
                                Cells[newIndex] = this[x, y];
                            }
                        }

                        Width = bounds.width;
                    }

                    Height = bounds.height;

                    Array.Resize(ref Grid._cells, Width * Height);
                }
            }

            static EditorApi()
            {
                _editorApiFactory = grid => new EditorApi(grid);
            }

            private EditorApi(GridSystem grid)
            {
                _gridRef = new WeakReference<GridSystem>(grid, false);
            }

            private void SetCellPosition([NotNull] CellComponent cell, Vector2Int cellPos)
            {
                cell.name = $"Cell_{cellPos.x}x{cellPos.y}";
                cell.gameObject.transform.position = Grid.GridToWorld(cellPos);
                this[cellPos.x, cellPos.y] = cell;
            }
        }

        /// <summary>
        /// Returns the grid editor API object. Should NOT be used in game logics code.
        /// </summary>
        public EditorApi GetEditorApi()
        {
            return _editorApiFactory(this);
        }
    }
}

#endif