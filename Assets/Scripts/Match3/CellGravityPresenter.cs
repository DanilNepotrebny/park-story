﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using DreamTeam.Utils.RendererSorting;
using UnityEngine;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(SpriteRenderer), typeof(Animation))]
    public class CellGravityPresenter : MonoBehaviour
    {
        [SerializeField, RequiredField] private CellPresenter _cellPresenter;
        [SerializeField] private string _chipGravityDownTriggerName = "OnGravityDown";
        [SerializeField] private string _chipGravityRightTriggerName = "OnGravityRight";
        [SerializeField] private string _chipGravityUpTriggerName = "OnGravityUp";
        [SerializeField] private string _chipGravityLeftTriggerName = "OnGravityLeft";
        [SerializeField] private int _sortingOrderOffset = 1;

        private SpriteRenderer _spriteRenderer;
        private Animation _animation;

        private ChipPresenter _chipPresenter => _cellPresenter.CellComponent.Chip?.Presenter;
        private Direction _gravityDirection => _cellPresenter.CellComponent.Gravity;

        public void PlayAnimation()
        {
            _animation.Play(PlayMode.StopAll);

            if (_chipPresenter != null && _cellPresenter.CellComponent.Chip.CanFall)
            {
                string triggerName = GetTriggerName(_gravityDirection);
                _chipPresenter.SetTrigger(triggerName);
            }
        }

        protected void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _animation = GetComponent<Animation>();
        }

        private void OnEnable()
        {
            _cellPresenter.SortingOptions.SortingLayerChanged += OnCellSortingLayerChanged;
            _cellPresenter.SortingOptions.SortingOrderChanged += OnCellSortingOrderChanged;

            OnCellSortingLayerChanged(_cellPresenter.SortingOptions);
            OnCellSortingOrderChanged(_cellPresenter.SortingOptions, 0);
        }

        protected void OnDisable()
        {
            _cellPresenter.SortingOptions.SortingOrderChanged -= OnCellSortingOrderChanged;
            _cellPresenter.SortingOptions.SortingLayerChanged -= OnCellSortingLayerChanged;
        }

        private string GetTriggerName(Direction gravityDir)
        {
            switch (gravityDir)
            {
                case Direction.Up:
                    return _chipGravityUpTriggerName;

                case Direction.Down:
                    return _chipGravityDownTriggerName;

                case Direction.Left:
                    return _chipGravityLeftTriggerName;

                case Direction.Right:
                    return _chipGravityRightTriggerName;

                default:
                    throw new ArgumentOutOfRangeException(nameof(gravityDir), gravityDir, null);
            }
        }

        private void OnCellSortingLayerChanged(SortingOptions options)
        {
            _spriteRenderer.sortingLayerID = options.SortingLayerId;
        }

        private void OnCellSortingOrderChanged(SortingOptions options, int prevOrder)
        {
            _spriteRenderer.sortingOrder = options.SortingOrder + _sortingOrderOffset;
        }
    }
}
