﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Reflection;
using DreamTeam.Match3.Matching;
using DreamTeam.Match3.MoveWeight;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    public class MovesSystem : MonoBehaviour
    {
        [Inject] private GridSystem _gridSystem;
        [Inject] private MatchSystem _matchSystem;
        [Inject] private CombinationSystem _combinationSystem;
        [Inject] private MoveWeightCalculator _moveWeightCalculator;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        private SwapSystem _swapSystem;
        private readonly List<ChipActivator> _chipActivators = new List<ChipActivator>();
        
        [Obfuscation(Exclude = false, Feature = "-rename")]
        public event Action<List<Vector2Int>> MoveSucceeding;

        public event Action<List<Vector2Int>> MoveSucceed;

        [Inject]
        private void Construct(SwapSystem swapSystem)
        {
            _swapSystem = swapSystem;
            _swapSystem.SwapSucceed += OnSwapSucceed;
            _swapSystem.SwapSucceeding += OnSwapSucceeding;
        }

        /// <param name="cacheList"> List where counting moves are storing during calculation </param>
        /// <returns> Amount of moves </returns>
        public int CalculateExistingMovesCount(List<Move> cacheList = null)
        {
            if (cacheList == null)
            {
                cacheList = new List<Move>();
            }
            else
            {
                cacheList.Clear();
            }

            FindMoves(cacheList);

            return cacheList.Count;
        }

        /// <summary> Checks that the field has potential matches or bonuses to activate </summary>
        /// <returns> Has any move </returns>
        public bool FindMoves([CanBeNull] ICollection<Move> moves, bool calculateWeight = true)
        {
            bool result = FindSwapMoves(moves, calculateWeight);
            if (result && moves == null)
            {
                return true;
            }

            result |= FindActivationMoves(moves, calculateWeight);
            if (result && moves == null)
            {
                return true;
            }

            return result;
        }

        /// <summary> Checks that the field has potential matches or bonuses to activate in the position </summary>
        /// <returns> Has any move </returns>
        public bool FindMoves([CanBeNull] ICollection<Move> moves, Vector2Int position, bool calculateWeight = true)
        {
            bool result = FindSwapMoves(moves, position, calculateWeight);
            if (result && moves == null)
            {
                return true;
            }

            result |= FindActivationMoves(moves, position, calculateWeight);
            if (result && moves == null)
            {
                return true;
            }

            return result;
        }

        /// <summary> Registers ChipActivator component to be counted as move </summary>
        public void RegisterChipActivator(ChipActivator chipActivator)
        {
            if (_chipActivators.Exists(a => a == chipActivator))
            {
                return;
            }

            _chipActivators.Add(chipActivator);
            chipActivator.ChipActivating += OnChipActivating;
            chipActivator.ChipActivated += OnChipActivated;
        }

        /// <summary> Unregisters ChipActivator component to be counted as move </summary>
        public void UnregisterChipActivator(ChipActivator chipActivator)
        {
            chipActivator.ChipActivating -= OnChipActivating;
            chipActivator.ChipActivated -= OnChipActivated;
            _chipActivators.Remove(chipActivator);
        }

        /// <summary> Find the best move based on move's weight. Bigger weight means better move </summary>
        /// <returns> The best move </returns>
        public Move FindTheBestMove([NotNull] IEnumerable<Move> moves)
        {
            float weight = -1;
            var foundMoves = new List<Move>();
            foreach (Move move in moves)
            {
                if (move.Weight > weight)
                {
                    weight = move.Weight;
                    foundMoves.Clear();
                    foundMoves.Add(move);
                }
                else if (Mathf.Approximately(move.Weight, weight))
                {
                    foundMoves.Add(move);
                }
            }

            if (foundMoves.Count == 0)
            {
                return null;
            }

            int index = _random.Next(foundMoves.Count);
            Move result = foundMoves[index];
            return result;
        }

        private bool FindActivationMoves([CanBeNull] ICollection<Move> moves, bool calculateWeight = true)
        {
            if (moves == null)
            {
                return _chipActivators.Count > 0;
            }

            foreach (ChipActivator activator in _chipActivators)
            {
                ChipComponent chip = activator.ChipPresenter.ChipComponent;
                if (chip == null)
                {
                    continue;
                }

                CellComponent cell = chip.Cell;
                if (cell == null)
                {
                    continue;
                }

                var move = new Move
                {
                    Source = cell,
                    Weight = calculateWeight ? _moveWeightCalculator.CalculateActivationMove(activator) : 0,
                    Type = MoveType.Activation
                };

                moves.Add(move);
            }

            return _chipActivators.Count > 0;
        }

        private bool FindActivationMoves(
            [CanBeNull] ICollection<Move> moves,
            Vector2Int position,
            bool calculateWeight = true)
        {
            if (moves == null)
            {
                foreach (ChipActivator activator in _chipActivators)
                {
                    ChipComponent chip = activator.ChipPresenter.ChipComponent;
                    if (chip == null || chip.Cell == null)
                    {
                        continue;
                    }

                    if (chip.Cell.GridPosition == position)
                    {
                        return true;
                    }
                }

                return false;
            }

            foreach (ChipActivator activator in _chipActivators)
            {
                ChipComponent chip = activator.ChipPresenter.ChipComponent;
                if (chip == null)
                {
                    continue;
                }

                CellComponent cell = chip.Cell;
                if (cell == null)
                {
                    continue;
                }

                if (cell.GridPosition == position)
                {
                    moves.Add(
                        new Move
                        {
                            Source = cell,
                            Weight = calculateWeight ? _moveWeightCalculator.CalculateActivationMove(activator) : 0,
                            Type = MoveType.Activation
                        });
                }
            }

            return moves.Count > 0;
        }

        private bool FindSwapMoves([CanBeNull] ICollection<Move> moves, bool calculateWeight = true)
        {
            bool hasMove = false;
            for (int x = 0; x < _gridSystem.Width; ++x)
            {
                for (int y = 0; y < _gridSystem.Height; ++y)
                {
                    Vector2Int position = new Vector2Int(x, y);
                    hasMove |= FindSwapMove(moves, position, Direction.Right, calculateWeight);
                    hasMove |= FindSwapMove(moves, position, Direction.Up, calculateWeight);

                    if (moves == null && hasMove)
                    {
                        return true;
                    }
                }
            }

            return moves != null && moves.Count > 0;
        }

        private bool FindSwapMoves(
            [CanBeNull] ICollection<Move> moves,
            Vector2Int position,
            bool calculateWeight = true)
        {
            bool hasAnyMove = false;
            hasAnyMove |= FindSwapMove(moves, position, Direction.Right, calculateWeight);
            hasAnyMove |= FindSwapMove(moves, position, Direction.Up, calculateWeight);
            return hasAnyMove;
        }

        private bool FindSwapMove(
            [CanBeNull] ICollection<Move> outMoves,
            Vector2Int position,
            Direction direction,
            bool calculateWeight = true)
        {
            CellComponent targetCell = _gridSystem.GetCell(position);
            Vector2Int neighbourPos = position + direction.GetOffset();
            CellComponent sourceCell = _gridSystem.GetCell(neighbourPos);
            if (targetCell == null || sourceCell == null || !_swapSystem.CanBeSwapped(targetCell, sourceCell))
            {
                return false;
            }

            targetCell.SwapChipsSilently(sourceCell);

            Match match1 = _matchSystem.FindMatch(position);
            Match match2 = _matchSystem.FindMatch(neighbourPos);
            bool hasMatch = match1 != null || match2 != null;

            List<ChipComponent> match1Chips = GetMatchChips(match1);
            List<ChipComponent> match2Chips = GetMatchChips(match2);

            targetCell.SwapChipsSilently(sourceCell);

            bool hasCombination = _combinationSystem.HasCombination(targetCell.Chip, sourceCell.Chip);

            if (outMoves == null || (!hasMatch && !hasCombination))
            {
                return hasMatch || hasCombination;
            }

            sourceCell.SwapChipsSilently(targetCell); // swap to calculate real field state after swap-match

            float weight = 0;
            if (calculateWeight)
            {
                weight = _moveWeightCalculator.CalculateMatchingMove(
                    new List<Match>
                    {
                        match1,
                        match2
                    },
                    sourceCell.Chip,
                    targetCell.Chip);
            }

            sourceCell.SwapChipsSilently(targetCell); // rollback swapping

            var move = new Move
            {
                Source = sourceCell,
                Target = targetCell,
                SourceMatch = match1Chips,
                TargetMatch = match2Chips,
                Weight = weight,
                Type = MoveType.Swap
            };
            outMoves.Add(move);

            return true;
        }

        private List<ChipComponent> GetMatchChips([CanBeNull] Match match)
        {
            if (match == null)
            {
                return new List<ChipComponent>();
            }

            List<ChipComponent> chips = new List<ChipComponent>(match.Count);
            foreach (Vector2Int position in match)
            {
                CellComponent cell = _gridSystem.GetCell(position);
                chips.Add(cell.Chip);
            }

            return chips;
        }

        private void OnChipActivated(Vector2Int cellPosition)
        {
            MoveSucceed?.Invoke(new List<Vector2Int> { cellPosition });
        }

        private void OnChipActivating(Vector2Int cellPosition)
        {
            MoveSucceeding?.Invoke(new List<Vector2Int> { cellPosition });
        }

        private void OnSwapSucceed(List<Vector2Int> cellPositions)
        {
            MoveSucceed?.Invoke(cellPositions);
        }

        private void OnSwapSucceeding(List<Vector2Int> cellPositions)
        {
            MoveSucceeding?.Invoke(cellPositions);
        }

        #region InnerClasses

        public class Move
        {
            public CellComponent Source { get; set; }
            public List<ChipComponent> SourceMatch { get; set; } = new List<ChipComponent>();

            public CellComponent Target { get; set; }
            public List<ChipComponent> TargetMatch { get; set; } = new List<ChipComponent>();

            public float Weight { get; set; }
            public MoveType Type { get; set; }
        }

        public enum MoveType
        {
            Swap,
            Activation
        }

        #endregion InnerClasses
    }
}