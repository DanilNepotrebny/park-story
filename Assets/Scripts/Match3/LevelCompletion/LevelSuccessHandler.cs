﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.SceneLoading;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.LevelCompletion
{
    public class LevelSuccessHandler : MonoBehaviour
    {
        [Inject] private LevelCompletionSystem _levelCompletionSystem;
        [Inject] private LoadingSystem _loadingSystem;
        [Inject] private LoadSceneSettings _loadSceneSettings;
 
        protected void Start()
        {
            _levelCompletionSystem.LevelEnded += OnLevelEnded;
        }

        private void OnLevelEnded(bool isSuccessful)
        {
            if (!isSuccessful)
            {
                return;
            }

            _loadingSystem.LoadScene(_loadSceneSettings.ScenePath, true, _loadSceneSettings.LoadingScreenType);
        }
    }
}