﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils.Invocation;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.LevelCompletion
{
    [RequireComponent(typeof(Animator))]
    public class LevelRewardPresenter : MonoBehaviour
    {
        [SerializeField] private string _showTriggerName;
        [SerializeField] private string _hideTriggerName;
        [SerializeField] private TapGesture _tapGesture;

        [Inject] private LevelRewardSequence _levelRewardSequence;

        private Animator _animator;
        private IDeferredInvocationHandle _endSequenceHandle;

        protected void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        protected void Start()
        {
            _tapGesture.Tapped += OnGestureTapped;
            _levelRewardSequence.BonusConversionSequenceStarted += OnBonusConversionSequenceStarted;
            _levelRewardSequence.BonusConversionSequenceFinished += OnBonusConversionSequenceFinished;
        }

        protected void OnDestroy()
        {
            _levelRewardSequence.BonusConversionSequenceStarted -= OnBonusConversionSequenceStarted;
            _levelRewardSequence.BonusConversionSequenceFinished -= OnBonusConversionSequenceFinished;
            _tapGesture.Tapped -= OnGestureTapped;

            FinishBonusSequence();
        }

        private void OnBonusConversionSequenceStarted(IDeferredInvocationHandle endSequenceHandle)
        {
            Assert.IsNull(_endSequenceHandle, $"Bonus sequence end handle is not freed");
            _endSequenceHandle = endSequenceHandle.Lock();
            _animator.SetTrigger(_showTriggerName);
        }

        private void OnBonusConversionSequenceFinished()
        {
            FinishBonusSequence();
        }

        private void FinishBonusSequence()
        {
            ReleaseBonusSequenceEndHandle();
            _animator.SetTrigger(_hideTriggerName);
        }

        private void OnGestureTapped(object sender, EventArgs e)
        {
            FinishBonusSequence();
        }

        private void ReleaseBonusSequenceEndHandle()
        {
            _endSequenceHandle?.Dispose();
            _endSequenceHandle = null;
        }
    }
}
