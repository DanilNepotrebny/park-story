﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Match3;
using DreamTeam.Audio;
using DreamTeam.GameControls;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Popups;
using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.Requirements.BonusCounters;
using DreamTeam.Match3.UI.HUD;
using DreamTeam.Match3.UI.RequirementPresenters;
using DreamTeam.Popups;
using DreamTeam.UI.HUD;
using DreamTeam.UI.RewardPresenters;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Movement;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.LevelCompletion
{
    public class LevelRewardSequence : MonoBehaviour
    {
        [SerializeField] private ChipTag _bonusTag;
        [SerializeField] private float _activationDelay;
        [SerializeField] private float _limitationConversionDelay;
        [SerializeField] private float _bonusConsumptionDelay;
        [SerializeField] private float _completionDelay;
        [SerializeField] private MovementSequence _bonusCoinProjectilePrefab;
        [SerializeField] private MovementSequence _limitationProjectilePrefab;
        [SerializeField] private WeightedBonusPrefabs _weightedBonusPrefabs;
        [SerializeField] private WeightedSoftCurrencyReward _weightedBonusReward;

        [SerializeField] private SceneReference _initialPopup;
        [SerializeField, Range(0, 10)] private float _initialPopupShowingDuration;
        [SerializeField, RequiredField] private GameControlsMode _mode;
        [SerializeField] private SceneReference _finalPopup;
        [SerializeField, RequiredField] private AudioSystemPlayer _audio;

        [Inject] private Instantiator _instantiator;
        [Inject] private ICurrentLevelInfo _currentLevelInfo;
        [Inject] private GameControlsSystem _controlsSystem;
        [Inject] private GridSystem _gridSystem;
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;
        [Inject] private LimitationsPanel _limitationsPanel;
        [Inject] private GenerationSystem _generationSystem;
        [Inject] private MissionCompletedCoinsPresenter _coinsPresenter;
        [Inject] private ImpactSystem _impactSystem;
        [Inject] private PopupSystem _popupSystem;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private IResourcesPanel _resourcesPanel;

        [Inject(Id = LevelSettings.LimitationInstancesId)]
        public IReadOnlyList<Requirement.Instance> _limitations;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        [Inject(Id = GlobalInstaller.MoneyId)]
        private CountedItem _money;

        private const ImpactFlag _bonusReplaceImpact = ImpactFlag.BonusReplace;
        private const ImpactFlag _bonusActivationImpact = ImpactFlag.BonusActivation;

        private List<BonusRewardData> _bonusRewards;
        private Dictionary<CountedItem, int> _displayedResourcesCount = new Dictionary<CountedItem, int>();
        private int _currentBonusRewardIndex;
        private ActionDisposable _modeDisableHandle;
        private int _receivedMoney;
        private Dictionary<MovementSequence, ChipReplaceData> _movingChips = new Dictionary<MovementSequence, ChipReplaceData>();
        private bool _forceEndBonusSequence;
        private int _receivingMoneyFromLimitationsAmount;

        private bool _gridHasBonus => FindBonus() != null;

        /// <summary> Holder for temporal coins when level has been finished to play coins rewardable animation </summary>
        public int ReceivedMoney
        {
            get { return _receivedMoney; }
            set
            {
                if (value == _receivedMoney)
                {
                    return;
                }

                int previousValue = _receivedMoney;
                _receivedMoney = value;
                ReceivedMoneyChanged?.Invoke(_receivedMoney, previousValue);
            }
        }

        public bool IsSequencePlaying => _modeDisableHandle != null;

        public event ReceivedMoneyChangedDelegate ReceivedMoneyChanged;

        public event Action Performing;
        public event Action Performed;
        public event BonusConversionSequenceStartedDelegate BonusConversionSequenceStarted;
        public event BonusConversionSequenceFinishedDelegate BonusConversionSequenceFinished;

        public IEnumerator Perform()
        {
            _modeDisableHandle = _controlsSystem.EnableMode(_mode);

            Performing?.Invoke();

            _generationSystem.ChipGenerated += OnChipGenerated;

            foreach (CellComponent cell in _gridSystem.Cells)
            {
                ChipComponent chip = cell.Chip;
                if (chip != null &&
                    !chip.IsMoving &&
                    !chip.IsConsumed &&
                    cell.IsStable &&
                    chip.Tags.Has(_bonusTag))
                {
                    chip.Impacting += OnChipImpacting;
                }
            }

            bool allPopupsHidden = false;
            _popupSystem.HideAll(() => allPopupsHidden = true);

            yield return new WaitUntil(() => allPopupsHidden);

            yield return ShowPopup(_initialPopup, null, _initialPopupShowingDuration);
            _audio.Play();

            ReceivedMoney += _currentLevelInfo.CurrentLevel.MoneyRewardCount;

            yield return new WaitForSeconds(_activationDelay);

            yield return StartConsumingSequence();

            yield return new WaitForSeconds(_completionDelay);

            ReceivedMoney += _receivingMoneyFromLimitationsAmount;
            _receivingMoneyFromLimitationsAmount = 0;

            CountedItem _mainReward = _currentLevelInfo.CurrentLevel.MainRewardType;
            var popupArgs = new MissionCompletePopup.Args
            {
                ClosingCallback = () => UnlockResourcePresenters(_mainReward, _money),
                GiveMainRewardCallback = count => UpdateResourcePresenter(_mainReward, count),
                GiveCoinsRewardCallback = count => UpdateResourcePresenter(_money, count),
                MainRewardType = _mainReward,
                MainRewardsAmountReceived = _currentLevelInfo.CurrentLevel.MainRewardAmount,
                CoinsAmountReceived = ReceivedMoney
            };

            _audio.Stop();
            yield return ShowPopup(_finalPopup, popupArgs);

            _modeDisableHandle.Dispose();
            _modeDisableHandle = null;

            Performed?.Invoke();
        }

        public void OnLevelSucceeded()
        {
            _bonusRewards = new List<BonusRewardData>();

            _receivingMoneyFromLimitationsAmount = 0;

            foreach (Requirement.Instance limitation in _limitations)
            {
                IBonusCounterProvider bonusProvider = limitation as IBonusCounterProvider;
                if (bonusProvider != null)
                {
                    LimitationInstancePresenter limitationPresenter = _limitationsPanel.FindRootPresenter(limitation);
                    Assert.IsNotNull(
                            limitationPresenter,
                            $"Presenter for limitation {limitation.GetType().Name} is not found."
                        );

                    for (int i = 0; i < bonusProvider.BonusCounter.RewardCount; i++)
                    {
                        BonusRewardData rewardData = AddBonusReward(bonusProvider.BonusCounter, limitationPresenter);
                        _receivingMoneyFromLimitationsAmount += rewardData.RewardCount;
                    }
                }
            }

            LevelSettings currentLevel = _currentLevelInfo.CurrentLevel;

            LockResourcePresenters(currentLevel.MainRewardType, _money);
            _money.TryReceive(
                currentLevel.MoneyRewardCount + _receivingMoneyFromLimitationsAmount,
                ReceivingType.Earned,
                GamePlace.InGame);
            currentLevel.MainRewardType.TryReceive(
                currentLevel.MainRewardAmount,
                ReceivingType.Earned,
                GamePlace.InGame);

            var levelSucceededEvent = _instantiator.Instantiate<LevelSucceededEvent>(
                    currentLevel.MoneyRewardCount,
                    _receivingMoneyFromLimitationsAmount
                );
            _analyticsSystem.Send(levelSucceededEvent);
        }

        private void LockResourcePresenters(params CountedItem[] countedItems)
        {
            _displayedResourcesCount.Clear();
            foreach (CountedItem countedItem in countedItems)
            {
                _resourcesPanel.GetLockablePresenter(countedItem)?.Lock();
                UpdateResourcePresenter(countedItem, 0);
            }
        }

        private void UpdateResourcePresenter(CountedItem countedItem, int count)
        {
            int currentValue;
            int previousValue;

            if (_displayedResourcesCount.TryGetValue(countedItem, out previousValue))
            {
                currentValue = previousValue + count;
                _displayedResourcesCount[countedItem] = currentValue;
            }
            else
            {
                currentValue = previousValue = countedItem.Count;
                _displayedResourcesCount.Add(countedItem, currentValue);
            }

            var updateArgs = new CountedItemMotionFinishData(currentValue, previousValue);
            _resourcesPanel.GetLockablePresenter(countedItem)?.UpdateManualy(updateArgs);
        }

        private void UnlockResourcePresenters(params CountedItem[] countedItems)
        {
            foreach (CountedItem countedItem in countedItems)
            {
                _resourcesPanel.GetLockablePresenter(countedItem)?.Unlock();
            }
        }

        private BonusRewardData AddBonusReward(
                IBonusCounter bonusCounter,
                LimitationInstancePresenter limitationPresenter
            )
        {
            var rewardData = new BonusRewardData
            {
                BonusCounter = bonusCounter,
                OriginProvider = limitationPresenter,
                RewardCount = GenerateBonusRewardCount(),
            };

            _bonusRewards.Add(rewardData);
            return rewardData;
        }

        private int GenerateBonusRewardCount()
        {
            return RandomUtils.RandomElement(_weightedBonusReward, _random);
        }

        private void OnChipGenerated(ChipComponent chipComponent)
        {
            if (chipComponent.Tags.Has(_bonusTag))
            {
                chipComponent.Impacting += OnChipImpacting;
            }
        }

        private void OnChipImpacting(ChipComponent chipComponent, int impactValue)
        {
            int coinsCount = 0;
            BonusRewardData rewardData = _bonusRewards.FirstOrDefault(data => data.Chip == chipComponent);
            if (rewardData != null)
            {
                coinsCount = rewardData.RewardCount;
            }
            else
            {
                coinsCount = GenerateBonusRewardCount();
                _money.TryReceive(coinsCount, ReceivingType.Earned, GamePlace.InGame);
            }

            for (int i = 0; i < coinsCount; ++i)
            {
                MovementSequence projectile = _instantiator.Instantiate(
                    _bonusCoinProjectilePrefab,
                    chipComponent.transform.position);
                projectile.Finished += mover =>
                {
                    ReceivedMoney += 1;
                    if (_receivingMoneyFromLimitationsAmount > 0)
                    {
                        _receivingMoneyFromLimitationsAmount -= 1;
                    }
                };
                projectile.Move(
                        chipComponent.transform.position,
                        _coinsPresenter.CoinMovementFinishPoint.position,
                        true
                    );
            }
        }

        private IEnumerator StartConsumingSequence()
        {
            Coroutine consumingSequenceCoroutine = null;
            var forceEndSequenceDI = new DeferredInvocation(
                () =>
                {
                    if (consumingSequenceCoroutine != null)
                    {
                        _generationSystem.enabled = false;
                        CancelAllMovingChips();
                        StopCoroutine(consumingSequenceCoroutine);
                        consumingSequenceCoroutine = null;
                    }
                });
            var finishSequenceDI = new DeferredInvocation(
                () =>
                {
                    if (consumingSequenceCoroutine != null)
                    {
                        _generationSystem.enabled = false;
                        CancelAllMovingChips();
                        StopCoroutine(consumingSequenceCoroutine);
                        consumingSequenceCoroutine = null;
                    }
                });

            using (IDeferredInvocationHandle forceEndHandle = forceEndSequenceDI.Start())
            {
                using (IDeferredInvocationHandle finishHandle = finishSequenceDI.Start())
                {
                    consumingSequenceCoroutine = StartCoroutine(ConsumingSequence(finishHandle));
                }

                BonusConversionSequenceStarted?.Invoke(forceEndHandle);
            }

            yield return new WaitUntil(() => consumingSequenceCoroutine == null);
            BonusConversionSequenceFinished?.Invoke();
        }

        private IEnumerator ConsumingSequence(IDeferredInvocationHandle endSequenceHandle)
        {
            using (IDeferredInvocationHandle endHandle = endSequenceHandle.Lock())
            {
                do
                {
                    while (_gridHasBonus)
                    {
                        yield return TryConsumeBonuses();
                    }

                    yield return TryConvertLimitations();
                }
                while (_gridHasBonus);

                yield return new WaitUntil(() => _fieldStabilizationSystem.IsFieldStable);

                if (_gridHasBonus)
                {
                    yield return ConsumingSequence(endHandle);
                }
            }
        }

        private ChipComponent FindBonus()
        {
            foreach (CellComponent cell in _gridSystem.Cells)
            {
                ChipComponent chip = cell.Chip;
                if (chip != null &&
                    !chip.IsMoving &&
                    !chip.IsConsumed &&
                    cell.IsStable &&
                    chip.Tags.Has(_bonusTag) &&
                    chip.CanBeImpacted(_bonusActivationImpact))
                {
                    return chip;
                }
            }

            return null;
        }

        private IEnumerator TryConsumeBonuses()
        {
            ChipComponent bonus = null;
            do
            {
                bonus = FindBonus();
                if (bonus != null)
                {
                    Impact impact = _impactSystem.ScheduleImpact(bonus.Cell, -bonus.Health, _bonusActivationImpact);
                    impact.Perform();

                    yield return new WaitForSeconds(_bonusConsumptionDelay);
                }
            }
            while (bonus != null);
        }

        private IEnumerator TryConvertLimitations()
        {
            CellComponent targetCell = null;

            var reservedCells = new HashSet<CellComponent>();

            while (_currentBonusRewardIndex < _bonusRewards.Count &&
                TryGetCellForBonus(reservedCells, out targetCell))
            {
                BonusRewardData rewardData = _bonusRewards[_currentBonusRewardIndex];
                _currentBonusRewardIndex++;
                ChipComponent bonusChipPrefab = GetBonusChipPrefab(rewardData);

                reservedCells.Add(targetCell);

                Transform projectileOrigin = rewardData.OriginProvider.ProjectileOrigin;
                MovementSequence projectile = _instantiator.Instantiate(
                    _limitationProjectilePrefab,
                    projectileOrigin.position);
                _movingChips.Add(projectile, new ChipReplaceData(targetCell, bonusChipPrefab));

                CellComponent cell = targetCell;
                projectile.Finished += mover =>
                {
                    ChipReplaceData replaceData = _movingChips[mover];
                    ChipComponent chip = _generationSystem.InstantiateChip(replaceData.Chip);
                    chip.transform.position = cell.transform.position;

                    rewardData.Chip = chip;

                    replaceData.Cell.ForceReplaceChip(chip, _bonusReplaceImpact);
                    _movingChips.Remove(mover);
                };
                projectile.Move(projectileOrigin.position, targetCell.transform.position);

                yield return new WaitForSeconds(_limitationConversionDelay);
            }

            yield return new WaitWhile(() => _movingChips.Count > 0);
        }

        private void CancelAllMovingChips()
        {
            foreach (KeyValuePair<MovementSequence, ChipReplaceData> pair in _movingChips)
            {
                pair.Key.Stop();
            }
        }

        private bool TryGetCellForBonus(ICollection<CellComponent> exceptCollection, out CellComponent result)
        {
            List<CellComponent> candidates = new List<CellComponent>();
            foreach (CellComponent cell in _gridSystem.Cells)
            {
                ChipComponent chip = cell.Chip;
                if (chip != null && chip.CanBeImpacted(_bonusReplaceImpact) && !exceptCollection.Contains(cell))
                {
                    candidates.Add(cell);
                }
            }

            result = candidates.Count > 0 ? candidates[_random.Next(candidates.Count)] : null;
            return result != null;
        }

        private ChipComponent GetBonusChipPrefab(BonusRewardData rewardData)
        {
            Assert.IsTrue(rewardData.BonusCounter.RewardCount > 0);

            rewardData.BonusCounter.Consume();
            return RandomUtils.RandomElement(_weightedBonusPrefabs, _random);
        }

        private IEnumerator ShowPopup(
                SceneReference popupScene,
                Popup.Args popupArgs,
                float showDuration = 0
            )
        {
            _popupSystem.Show(popupScene.Name, popupArgs);

            Popup popup = null;
            yield return new WaitUntil(() => (popup = _popupSystem.GetPopup(popupScene.Name)) != null);

            bool isPopupClosed = false;
            popup.Hidden += p => isPopupClosed = true;

            if (showDuration > 0)
            {
                yield return new WaitForSeconds(showDuration);

                _popupSystem.Hide(popupScene.Name);
            }

            yield return new WaitUntil(() => isPopupClosed);
        }

        #region Inner types

        public delegate void ReceivedMoneyChangedDelegate(int value, int previousValue);
        public delegate void BonusConversionSequenceStartedDelegate(IDeferredInvocationHandle endSequenceHandle);
        public delegate void BonusConversionSequenceFinishedDelegate();

        [Serializable]
        private class WeightedBonusPrefabs : KeyValueList<ChipComponent, int>
        {
        }

        [Serializable]
        private class WeightedSoftCurrencyReward : KeyValueList<int, int>
        {
        }

        private struct ChipReplaceData
        {
            public CellComponent Cell { get; }
            public ChipComponent Chip { get; }

            public ChipReplaceData(CellComponent cell, ChipComponent chip)
            {
                Cell = cell;
                Chip = chip;
            }
        }

        private class BonusRewardData
        {
            public IBonusCounter BonusCounter { get; set; }
            public LimitationInstancePresenter OriginProvider { get; set; }
            public int RewardCount { get; set; }
            public ChipComponent Chip { get; set; }
        }

        #endregion
    }
}