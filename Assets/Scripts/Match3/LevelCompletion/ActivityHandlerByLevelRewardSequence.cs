﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.LevelCompletion
{
    /// <summary>
    /// Activity handler that activates objects during level reward sequence
    /// </summary>
    public class ActivityHandlerByLevelRewardSequence : MonoBehaviour
    {
        [SerializeField] private bool _invertMode;
        [SerializeField, RequiredField] private Behaviour[] _components;
        [SerializeField, RequiredField] private GameObject[] _gameObjects;

        [Inject] private LevelRewardSequence _levelRewardSequence;

        protected void OnEnable()
        {
            _levelRewardSequence.Performing += OnSequencePerforming;
            _levelRewardSequence.Performed += OnSequencePerformed;

            ValidateObjects(_components);
            ValidateObjects(_gameObjects);

            UpdateActivity();
        }

        protected void OnDisable()
        {
            _levelRewardSequence.Performing -= OnSequencePerforming;
            _levelRewardSequence.Performed -= OnSequencePerformed;
        }

        private void OnSequencePerforming()
        {
            UpdateActivity();
        }

        private void OnSequencePerformed()
        {
            UpdateActivity();
        }

        private void UpdateActivity()
        {
            SetActive(_levelRewardSequence.IsSequencePlaying);
        }

        private void ValidateObjects(IEnumerable<Object> objects)
        {
            foreach (Object obj in objects)
            {
                Assert.IsNotNull(obj, $"Found null object in {nameof(ActivityHandlerByLevelRewardSequence)} " +
                    $"in {gameObject.name}");
            }
        }

        private void SetActive(bool isActive)
        {
            isActive ^= _invertMode;
            foreach (GameObject go in _gameObjects)
            {
                go?.SetActive(isActive);
            }

            foreach (Behaviour behaviour in _components)
            {
                if (behaviour != null)
                {
                    behaviour.enabled = isActive;
                }
            }
        }
    }
}
