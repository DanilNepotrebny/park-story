﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Match3;
using DreamTeam.Match3.Hammer;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.LevelCompletion
{
    public class LevelStartNotifier : MonoBehaviour
    {
        private MovesSystem _movesSystem;
        private HammerSystem _hammerSystem;
        private AnalyticsSystem _analyticsSystem;
        private Instantiator _instantiator;

        public event Action LevelStarted;

        public bool IsLevelStarted { get; private set; }

        [Inject]
        private void Construct(
                MovesSystem movesSystem,
                HammerSystem hammerSystem,
                AnalyticsSystem analyticsSystem,
                Instantiator instantiator
            )
        {
            _analyticsSystem = analyticsSystem;
            _instantiator = instantiator;

            _movesSystem = movesSystem;
            _movesSystem.MoveSucceed += OnMoveSucceed;

            _hammerSystem = hammerSystem;
            _hammerSystem.Striking += OnHammerStriking;
        }

        private void OnHammerStriking(CellComponent target, IDeferredInvocationHandle commitstrikehandle)
        {
            OnLevelStarted();
        }

        private void OnMoveSucceed(List<Vector2Int> cellPositions)
        {
            OnLevelStarted();
        }

        private void OnLevelStarted()
        {
            IsLevelStarted = true;

            _movesSystem.MoveSucceed -= OnMoveSucceed;
            _hammerSystem.Striking -= OnHammerStriking;

            _analyticsSystem.Send(_instantiator.Instantiate<LevelStartedEvent>());
            LevelStarted?.Invoke();
        }
    }
}