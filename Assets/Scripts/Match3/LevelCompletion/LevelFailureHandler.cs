﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Match3;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Prices;
using DreamTeam.Match3.Popups;
using DreamTeam.Popups;
using DreamTeam.SceneLoading;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.LevelCompletion
{
    /// <summary> System that controls failure sequence. </summary>
    public class LevelFailureHandler : MonoBehaviour
    {
        [SerializeField] private SceneReference _outOfMovesPopupScene;
        [SerializeField, RequiredField] private LevelFailureOffersCollection _outOfMovesOffers;
        [SerializeField] private SceneReference _debriefingPopup;
        [SerializeField] private SceneReference _boosterTipPopup;
        [SerializeField] private CountedItemPrice _failurePrice;

        [Inject] private LevelCompletionSystem _levelCompletionSystem;
        [Inject] private PopupSystem _popupSystem;
        [Inject] private BoosterTipSystem _boosterTipSystem;
        [Inject] private LoadingSystem _loadingSystem;
        [Inject] private LoadSceneSettings _loadSceneSettings;
        [Inject] private ReloadSceneSettings _reloadSceneSettings;
        [Inject] private LevelStartNotifier _levelStartNotifier;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private Instantiator _instantiator;
        [Inject] private PriceInstanceContainer _priceInstanceContainer;

        private IDeferredInvocationHandle _levelFailingHandle;
        private int _tryCount;
        private ICancelable _failurePayment;
        private CountedItemPrice.Instance _failurePriceInstance;

        public ProductPack LastPurchasedOutOfMovesOffer =>
            _tryCount > 0 ? _outOfMovesOffers.GetOffer(_tryCount - 1) : null;

        public event Action<IDeferredInvocationHandle> FailureHandleBeginning;

        protected void Awake()
        {
            _failurePriceInstance = (CountedItemPrice.Instance)_priceInstanceContainer.GetInstance(_failurePrice);
        }

        protected void Start()
        {
            _levelCompletionSystem.LevelFailing += OnLevelFailing;
            _levelCompletionSystem.LevelEnded += OnLevelEnded;
            _levelCompletionSystem.LevelSucceeded += OnLevelSucceeded;
            _levelStartNotifier.LevelStarted += OnLevelStarted;
        }

        private void OnLevelStarted()
        {
            if (_failurePayment == null)
            {
                _failurePayment = _failurePriceInstance.TrySchedulePayment(GamePlace.InGame);
            }
        }

        private void OnLevelSucceeded()
        {
            _failurePayment?.Cancel();
        }

        private void OnLevelFailing(IDeferredInvocationHandle commitLevelFailureHandle)
        {
            _levelFailingHandle = commitLevelFailureHandle.Lock();

            var di = new DeferredInvocation(ShowOutOfMovesPopup);
            using (IDeferredInvocationHandle handle = di.Start())
            {
                FailureHandleBeginning?.Invoke(handle);
            }
        }

        private void ShowOutOfMovesPopup()
        {
            _popupSystem.Show(
                    _outOfMovesPopupScene.Name,
                    new OutOfMovesPopup.Args
                    {
                        PurchaseResultCallback = OnOutOfMovesPopupPurchaseResult,
                        ProductPack = _outOfMovesOffers.GetOffer(_tryCount),
                        GamePlace = GamePlace.OutOfMoves
                    }
                );
        }

        private void OnOutOfMovesPopupPurchaseResult(bool isPurchased)
        {
            if (isPurchased)
            {
                _tryCount++;
            }

            _levelFailingHandle.Unlock();
            _levelFailingHandle = null;
        }

        private void OnLevelEnded(bool isSuccessful)
        {
            if (isSuccessful)
            {
                return;
            }

            _failurePayment?.Cancel();
            _failurePriceInstance.TryPay(GamePlace.InGame);

            if (_failurePriceInstance.CanBePayed)
            {
                Booster tipBooster = _boosterTipSystem.GetBooster();
                if (tipBooster != null)
                {
                    bool isBoosterPicked = false;
                    _popupSystem.Show(
                            _boosterTipPopup.Name,
                            new BoosterTipPopup.Args
                            {
                                ProductPack = tipBooster.DefaultProductPack,
                                GamePlace = GamePlace.BoosterTip,
                                OnBoosterPicked = () => isBoosterPicked = true,
                                ClosingCallback = () => ShowDebriefingPopup(!isBoosterPicked)
                            }
                        );
                }
                else
                {
                    ShowDebriefingPopup();
                }
            }
            else
            {
                _loadingSystem.LoadScene(_loadSceneSettings.ScenePath, true, _loadSceneSettings.LoadingScreenType);
            }
        }

        private void ShowDebriefingPopup(bool unequipBoosters = true)
        {
            _popupSystem.Show(
                    _debriefingPopup.Name,
                    new MissionDebriefingPopup.Args
                    {
                        ReplayButtonCallback = () =>
                        {
                            OnDebriefingClosed(
                                    DebriefingDecisionType.Replay,
                                    _reloadSceneSettings.ScenePath,
                                    _reloadSceneSettings.LoadingScreenType
                                );
                        },
                        CancelButtonCallBack = () =>
                        {
                            OnDebriefingClosed(
                                    DebriefingDecisionType.Close,
                                    _loadSceneSettings.ScenePath,
                                    _loadSceneSettings.LoadingScreenType
                                );
                        },
                        UnequipAllBoosters = unequipBoosters,
                    }
                );
        }

        private void OnDebriefingClosed(
                DebriefingDecisionType decision,
                string nextScenePath,
                LoadingSystem.LoadingScreenType loadingScreenType
            )
        {
            var debriefingEvent = _instantiator.Instantiate<LevelFailedDebriefingEvent>(
                    decision
                );
            _analyticsSystem.Send(debriefingEvent);
            _loadingSystem.LoadScene(
                    nextScenePath,
                    true,
                    loadingScreenType
                );
        }
    }
}