﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3.LevelCompletion
{
    public enum DebriefingDecisionType
    {
        Replay,
        Close
    }

    public static class PurchasePlaceExtensions
    {
        public static string ToAnalyticsId(this DebriefingDecisionType self)
        {
            switch (self)
            {
                case DebriefingDecisionType.Replay:
                    return "replay";
                case DebriefingDecisionType.Close:
                    return "close";
            }

            UnityEngine.Debug.LogError($"Ivalid value for {self.GetType().Name} enum.");
            return "";
        }
    }
}