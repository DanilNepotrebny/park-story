﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Match3;
using DreamTeam.Debug;
using DreamTeam.GameControls;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Match3.LevelCompletion
{
    public class LevelCompletionSystem : MonoBehaviour
    {
        [SerializeField, FormerlySerializedAs("_selectionMode")] private GameControlsMode _controlsMode;

        [Inject(Id = LevelSettings.GoalInstancesId)]
        private IReadOnlyList<Requirement.Instance> _goalInstances;

        [Inject(Id = LevelSettings.LimitationInstancesId)]
        private IReadOnlyList<Requirement.Instance> _limitationInstances;

        [Inject] private CheatPanel _cheatPanel;
        [Inject] private LevelSystem _levelSystem;
        [Inject] private ICurrentLevelInfo _currentLevelInfo;
        [Inject] private Instantiator _instantiator;
        [Inject] private ShuffleSystem _shuffleSystem;
        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private LevelRewardSequence _levelRewardSequence;
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;
        [Inject] private AnalyticsSystem _analyticsSystem;

        private int _receivedCoins;
        private CheckMethod _limitationsCheckMethod;
        private LimitationChecker _limitationChecker;
        private ActionDisposable _controlsModeDisableHandle;

        private bool _areGoalsReached => _goalInstances.All(r => r.IsReached);

        private bool _areGoalsStable => _goalInstances.All(r => r.IsStable);

        private bool _isLimitationReached => _limitationsCheckMethod?.Invoke() ?? true;

        public event Action LevelSucceeded;

        /// <summary>
        /// Event is called when limitation has been reached before completing goals.
        /// arg_1 - level failing end invocation
        /// </summary>
        public event Action<IDeferredInvocationHandle> LevelFailing;

        /// <summary>
        /// Event is called when level was ended.
        /// arg_1 - is level ended successfully
        /// </summary>
        public event Action<bool> LevelEnded;

        public void FailLevel()
        {
            UnsubscribeForLevelEndingChecks();
            LevelEnded?.Invoke(false);
        }

        /// <summary> Set method to check whether a limitation is reached. </summary>
        public void SetLimitationsCheck(CheckMethod method)
        {
            _limitationsCheckMethod = method;
        }

        protected void Awake()
        {
            if (_goalInstances == null || _goalInstances.Count == 0)
            {
                throw new InvalidOperationException("Can't find any goals.");
            }

            _limitationChecker = _instantiator.Instantiate<LimitationChecker>();
            _limitationsCheckMethod = _limitationChecker.IsLimitationReached;

            _shuffleSystem.ShuffleFailed += FailLevel;
        }

        protected void Start()
        {
            SubscribeForLevelEndingChecks();

            _cheatPanel.AddDrawer(CheatPanel.Category.Match3, DrawCheats);
        }

        protected void OnDestroy()
        {
            ReleaseModeDisableHandle();

            _cheatPanel.RemoveDrawer(DrawCheats);
            _shuffleSystem.ShuffleFailed -= FailLevel;
        }

        private void SubscribeForLevelEndingChecks()
        {
            foreach (Requirement.Instance goal in _goalInstances)
            {
                goal.Reached += CheckLevelEnding;
            }

            foreach (Requirement.Instance limitation in _limitationInstances)
            {
                limitation.Reached += CheckLevelEnding;
            }
        }

        private void UnsubscribeForLevelEndingChecks()
        {
            foreach (Requirement.Instance goal in _goalInstances)
            {
                goal.Reached -= CheckLevelEnding;
            }

            foreach (Requirement.Instance limitation in _limitationInstances)
            {
                limitation.Reached -= CheckLevelEnding;
            }
        }

        private void CheckLevelEnding()
        {
            if (_areGoalsReached || _isLimitationReached)
            {
                UnsubscribeForLevelEndingChecks();
                StartCoroutine(PerformLevelEnding());
            }
        }

        private IEnumerator PerformLevelEnding()
        {
            Assert.IsNull(_controlsModeDisableHandle, $"Previously enabled mode is not disabled!");
            _controlsModeDisableHandle = _gameControlsSystem.EnableMode(_controlsMode);

            bool wasLevelCompleted = false;

            if (_areGoalsReached)
            {
                wasLevelCompleted = true;
                OnLevelSucceeded();
            }

            yield return new WaitUntil(() => _fieldStabilizationSystem.IsFieldStable && _areGoalsStable);

            if (_areGoalsReached)
            {
                if (!wasLevelCompleted)
                {
                    OnLevelSucceeded();
                }

                yield return PerformLevelRewardSequence();
            }
            else
            {
                if (_isLimitationReached)
                {
                    yield return PerformLevelFailing();
                }
                else
                {
                    SubscribeForLevelEndingChecks(); // limitation was changed (for example by frisbee in last move)
                    ReleaseModeDisableHandle();
                    yield break;
                }
            }

            ReleaseModeDisableHandle();
        }

        private void ReleaseModeDisableHandle()
        {
            _controlsModeDisableHandle?.Dispose();
            _controlsModeDisableHandle = null;
        }

        private void OnLevelSucceeded()
        {
            _levelRewardSequence.OnLevelSucceeded();
            LevelSucceeded?.Invoke();
            _levelSystem.OnLevelSucceeded(_currentLevelInfo.CurrentLevel);
        }

        private IEnumerator PerformLevelRewardSequence()
        {
            yield return _levelRewardSequence.Perform();

            LevelEnded?.Invoke(true);
        }

        private IEnumerator PerformLevelFailing()
        {
            bool isInvocationLocked = false;
            DeferredInvocation invocation = new DeferredInvocation(() => isInvocationLocked = true);

            IDeferredInvocationHandle handle = invocation.Start();
            LevelFailing?.Invoke(invocation);
            handle.Unlock();

            yield return new WaitUntil(() => isInvocationLocked);

            if (_isLimitationReached)
            {
                _analyticsSystem.Send(_instantiator.Instantiate<LevelFailedEvent>());
                LevelEnded?.Invoke(false);
            }
            else // in case of purchased additional moves booster, for example
            {
                SubscribeForLevelEndingChecks();
            }
        }

        private void DrawCheats(Rect areaRect)
        {
            if (GUILayout.Button("Lose level"))
            {
                foreach (Requirement.Instance limitation in _limitationInstances)
                {
                    ((Requirement.Instance.ICheatApi)limitation).SetReached();
                }
                _cheatPanel.Hide();
            }
            else if (GUILayout.Button("Complete level"))
            {
                foreach (Requirement.Instance goal in _goalInstances)
                {
                    ((Requirement.Instance.ICheatApi)goal).SetReached();
                }
                _cheatPanel.Hide();
            }
        }

        #region Inner types

        public delegate bool CheckMethod();

        private class LimitationChecker
        {
            [Inject(Id = LevelSettings.LimitationInstancesId)]
            private IReadOnlyList<Requirement.Instance> _limitationInstances;

            public bool IsLimitationReached()
            {
                return _limitationInstances.Any(l => l.IsReached);
            }
        }

        #endregion Inner types
    }
}