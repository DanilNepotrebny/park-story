﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Match3.Levels;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3
{
    public class BoosterTipSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private Booster[] _boosters;

        [Inject] private EquippedChipBoosters _equippedChipBoosters;
        [Inject] private LevelCompletionSystem _completionSystem;
        [Inject] private ICurrentLevelInfo _currentLevelInfo;
        [Inject] private Lives _livesItem;
        [Inject] private SaveGroup _saveGroup;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        private string _lastLevelId;
        private int _failsCount;
        private bool _isTipEnabled = true;
        private bool _popupWasShown; // don't synchronize

        public const int FailsAmountToShow = 3;
        public const int LivesAmountToShow = 1;

        private bool _isTipNeeded
        {
            get
            {
                if (_failsCount < FailsAmountToShow)
                {
                    return false;
                }

                if (_livesItem.Count != LivesAmountToShow)
                {
                    return false;
                }

                if (!_isTipEnabled)
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary> Get booster to purchase in BoosterTip popup </summary>
        /// <returns> Booster to purchase or null </returns>
        public Booster GetBooster()
        {
            if (!_isTipNeeded)
            {
                return null;
            }

            Booster booster = _currentLevelInfo.CurrentLevel.TipBooster;
            if (booster != null)
            {
                Assert.IsFalse(booster.IsLocked);
                return booster;
            }

            var boosters = new List<Booster>(_boosters.Where(b => !b.IsLocked));
            boosters.RandomShuffle(_random);
            return boosters.FirstOrDefault();
        }

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("BoosterTipSystem", this);
        }

        protected void Start()
        {
            _completionSystem.LevelEnded += isSuccessful =>
            {
                if (!isSuccessful)
                {
                    OnLevelFailed();
                }
            };

            SetLastLevelId(_currentLevelInfo.CurrentLevelId);

            if (_equippedChipBoosters.Any())
            {
                _isTipEnabled = false;
            }
        }

        private void SetLastLevelId(string levelId)
        {
            if (_lastLevelId != levelId)
            {
                Reset();
                _lastLevelId = levelId;
            }
        }

        private void Reset()
        {
            _failsCount = 0;
            _popupWasShown = false;
            _isTipEnabled = true;
        }

        private void OnLevelFailed()
        {
            if (_popupWasShown)
            {
                _isTipEnabled = false;
                return;
            }

            _failsCount += 1;
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncString("LevelId", ref _lastLevelId, "");
            state.SyncInt("FailsCount", ref _failsCount, 0);
            state.SyncBool("CurrentLevelIsValid", ref _isTipEnabled, true);
        }
    }
}
