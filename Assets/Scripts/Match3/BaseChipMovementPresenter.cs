﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3
{
    public abstract class BaseChipMovementPresenter : MonoBehaviour
    {
        private DeferredInvocation _moveToChipInvocation;

        private IDeferredInvocationHandle _movingEndHandle;

        public abstract ChipComponent.MovingType MovingType { get; }

        protected ChipComponent Chip => ChipPresenter.ChipComponent;
        protected ChipPresenter ChipPresenter { get; private set; }

        public event Action<IDeferredInvocationHandle> Moving;
        public event Action Moved;

        protected virtual void Awake()
        {
            _moveToChipInvocation = new DeferredInvocation(StartMovement);

            ChipPresenter = GetComponent<ChipPresenter>();
            SubscribeToEvents();
        }
        
        protected void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        protected abstract void PrepareMovement();
        protected abstract void StartMovement(Action finishCallback);
        protected abstract void FinishMovement();

        protected virtual void SubscribeToEvents()
        {
            Chip.Moving += OnChipMoving;
            Chip.Moved += OnChipMoved;
            Chip.ViewDetached += OnViewDetached;
        }

        protected virtual void UnsubscribeFromEvents()
        {
            Chip.Moving -= OnChipMoving;
            Chip.Moved -= OnChipMoved;
            Chip.ViewDetached -= OnViewDetached;
        }

        private void OnChipMoving(ChipComponent.MovingType type, IDeferredInvocationHandle movingEndHandle)
        {
            if (type != MovingType)
            {
                return;
            }

            _movingEndHandle = movingEndHandle.Lock();

            PrepareMovement();

            using (IDeferredInvocationHandle handle = _moveToChipInvocation.Start())
            {
                Moving?.Invoke(handle);
            }
        }

        private void OnChipMoved(ChipComponent.MovingType type)
        {
            if (type != MovingType)
            {
                return;
            }

            FinishMovement();

            Moved?.Invoke();
        }

        private void StartMovement()
        {
            StartMovement(OnMovementFinished);
        }

        private void OnMovementFinished()
        {
            _movingEndHandle.Unlock();
            _movingEndHandle = null;
        }

        private void OnViewDetached(ChipComponent chip, ChipPresenter presenter)
        {
            Assert.AreEqual(presenter, ChipPresenter);
            UnsubscribeFromEvents();
        }
    }
}