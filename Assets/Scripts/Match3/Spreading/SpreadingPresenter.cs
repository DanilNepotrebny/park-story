﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using Spine;
using Spine.Unity;
using UnityEngine;
using Zenject;
using Event = Spine.Event;

namespace DreamTeam.Match3.Spreading
{
    [RequireComponent(typeof(ChipContainerPresenter))]
    public class SpreadingPresenter : MonoBehaviour
    {
        [SerializeField] private GameObject _view;
        [SerializeField] private SkeletonAnimation _spredingAnimation;

        [SerializeField, SpineEvent(dataField = nameof(_spredingAnimation))]
        private string _spreadEvent;

        private ChipComponent _chip;

        private IDeferredInvocationHandle _childAttachingHandle;
        private bool _isSpreading;

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<ChipComponent>().FromComponentInParents(includeInactive: true, useFirst: true);
            yield return container.Bind<ChipContainerPresenter>().FromComponentSibling();
        }

        [Inject]
        private void Construct(
                SpreadingSystem spreadingSystem,
                ChipComponent chip,
                ChipContainerPresenter containerPresenter
            )
        {
            _chip = chip;

            containerPresenter.ChildAttaching += OnChildAttaching;

            spreadingSystem.OnSpreading += OnSpreading;
        }

        private void OnChildAttaching(IDeferredInvocationHandle handle)
        {
            if (_isSpreading)
            {
                _childAttachingHandle = handle.Lock();
            }
        }

        private void OnSpreading(ChipComponent spreadingChip, ChipComponent spreaderChip, ChipComponent chipToReplace)
        {
            if (_chip == spreadingChip)
            {
                _isSpreading = true;

                _spredingAnimation.transform.up = spreaderChip.transform.position - chipToReplace.transform.position;
                _spredingAnimation.gameObject.SetActive(true);

                _spredingAnimation.AnimationState.Event += OnAnimationEventOcurred;
                _spredingAnimation.AnimationState.Complete += OnAnimationCompleted;

                _view.SetActive(false);
            }
        }

        private void OnAnimationEventOcurred(TrackEntry trackEntry, Event @event)
        {
            if (@event.Data.Name == _spreadEvent)
            {
                _view.SetActive(true);

                _childAttachingHandle.Unlock();
                _childAttachingHandle = null;
            }
        }

        private void OnAnimationCompleted(TrackEntry trackEntry)
        {
            _spredingAnimation.gameObject.SetActive(false);
            _isSpreading = false;
        }
    }
}