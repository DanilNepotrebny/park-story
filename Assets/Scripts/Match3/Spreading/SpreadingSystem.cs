﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.Spreading
{
    /// <summary>
    /// Controls spreading mechanics on the field. Creates new chip every turn if none of registered
    /// <see cref="Spreader"/> canceled the spreading.
    /// </summary>
    public class SpreadingSystem : MonoBehaviour
    {
        [SerializeField] private ChipComponent _prefabToSpread;
        [SerializeField] private ChipTagSet _targetTags;
        [SerializeField] private ChipTagSet _prioritizedTargetTags;
        [SerializeField] private float _spreadingRadius = 1f;

        [Inject] private GridSystem _grid;
        [Inject] private GenerationSystem _generationSystem;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        private readonly List<Spreader> _spreaders = new List<Spreader>();
        private int _spreadingLockCounter;
        private int _movesCounter;

        private bool _isSpreadingAllowed =>
            _spreaders.Count > 0 &&
            _movesCounter == 0 &&
            _spreadingLockCounter == 0;

        public event SpreadingCallback OnSpreading;

        [Inject]
        private void Construct(MovesSystem movesSystem)
        {
            movesSystem.MoveSucceeding += OnMoveSucceeding;
            movesSystem.MoveSucceed += OnMoveSucceed;
        }

        public void RegisterSpreader(Spreader spreader)
        {
            if (_spreaders.Contains(spreader))
            {
                return;
            }

            _spreaders.Add(spreader);
            spreader.SpreadingLocked += OnSpreadingLocked;
            spreader.SpreadingUnlocked += OnSpreadingUnlocked;

            UpdateMovesCounter();
        }

        private void OnSpreadingLocked([NotNull] Spreader spreader)
        {
            _spreadingLockCounter += 1;
        }

        private void OnSpreadingUnlocked([NotNull] Spreader spreader)
        {
            _spreadingLockCounter -= 1;
            Assert.IsTrue(_spreadingLockCounter >= 0, $"");
        }

        public void UnregisterSpreader(Spreader spreader)
        {
            if (!_spreaders.Contains(spreader))
            {
                return;
            }

            spreader.SpreadingLocked -= OnSpreadingLocked;
            spreader.SpreadingUnlocked -= OnSpreadingUnlocked;
            _spreaders.Remove(spreader);

            UpdateMovesCounter();
        }

        protected void Start()
        {
            UpdateMovesCounter();
        }

        private void UpdateMovesCounter()
        {
            if (_movesCounter == 0)
            {
                _movesCounter = int.MaxValue;
            }

            foreach (Spreader spreader in _spreaders)
            {
                _movesCounter = Mathf.Min(_movesCounter, spreader.MovesToStartSpreading);
            }
        }

        private void OnMoveSucceeding(List<Vector2Int> cellPositions)
        {
            _spreadingLockCounter = 0;
            if (_movesCounter > 0)
            {
                _movesCounter--;
            }
        }

        private void OnMoveSucceed(List<Vector2Int> cellPositions)
        {
            if (!_isSpreadingAllowed)
            {
                return;
            }

            SpreadingInfo info = SelectCellToSpread();
            CellComponent cell = info.Cell;
            if (cell != null)
            {
                ChipComponent spreadingChip = _generationSystem.InstantiateChip(_prefabToSpread);
                ChipComponent previousChip = cell.Chip;

                OnSpreading?.Invoke(spreadingChip, info.Spreader.Chip, previousChip);

                cell.Chip = spreadingChip;
                cell.InsertChip(previousChip);
            }
            else
            {
                UnityEngine.Debug.Log("Cell to spread has not been found.");
            }
        }

        private SpreadingInfo SelectCellToSpread()
        {
            var candidates = new List<SpreadingInfo>();

            foreach (Spreader spreader in _spreaders)
            {
                _grid.ProcessCellsInRadius(
                        (cell, distance) =>
                        {
                            ChipComponent chip = cell.Chip;
                            if (chip != null &&
                                !chip.IsConsumed &&
                                !chip.WillBeConsumed() &&
                                chip.Tags.FindFirstOf(_targetTags, false) != null &&
                                spreader.IsValidTarget(cell))
                            {
                                candidates.Add(
                                        new SpreadingInfo
                                        {
                                            Cell = cell,
                                            Spreader = spreader
                                        }
                                    );
                            }
                        },
                        spreader.Chip.Cell.GridPosition,
                        _spreadingRadius
                    );
            }

            if (candidates.Count == 0)
            {
                return new SpreadingInfo();
            }

            candidates.RandomShuffle(_random);

            foreach (SpreadingInfo info in candidates)
            {
                if (info.Cell.Chip.Tags.FindFirstOf(_prioritizedTargetTags) != null)
                {
                    return info;
                }
            }

            return candidates[0];
        }

        #region Inner types

        public delegate void SpreadingCallback(
                ChipComponent spreadingChip,
                ChipComponent spreaderChip,
                ChipComponent chipToReplace
            );

        private struct SpreadingInfo
        {
            public CellComponent Cell;
            public Spreader Spreader;
        }

        #endregion
    }
}