﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Wall;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.Spreading
{
    public class WallSpreadingConstraint : MonoBehaviour, ISpreadingConstraint
    {
        [InjectOptional] private WallSystem _wallSystem;

        public bool IsSpreadingPossible(Spreader spreader, CellComponent target)
        {
            if (_wallSystem == null)
            {
                return true;
            }

            var wallPos = new WallSystem.Position(spreader.Chip.Cell.GridPosition, target.GridPosition);
            bool hasWall = _wallSystem.HasWall(wallPos);
            return !hasWall;
        }
    }
}
