﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.Spreading
{
    /// <summary>
    /// Chip with ability to spread. Spreading is controlled by <see cref="SpreadingSystem"/>, spreader itself
    /// just provides its position and can cancel the spreading if impact was predicted.
    /// </summary>
    [RequireComponent(typeof(ChipComponent))]
    public class Spreader : MonoBehaviour
    {
        [SerializeField] private int _movesToStartSpreading;
        [SerializeField] private List<GameObject> _constraints; // TODO: Interface field

        [Inject] private SpreadingSystem _spreadingSystem;
        [Inject] private Instantiator _instantiator;

        [Inject]
        public ChipComponent Chip { get; }

        public int MovesToStartSpreading => _movesToStartSpreading;

        public event Action<Spreader> SpreadingLocked;
        public event Action<Spreader> SpreadingUnlocked;

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<ChipComponent>().FromComponentSibling();
        }

        public bool IsValidTarget([NotNull] CellComponent target)
        {
            foreach (GameObject go in _constraints)
            {
                if (go.GetComponent<ISpreadingConstraint>() == null)
                {
                    continue;
                }

                var constraint = _instantiator.Instantiate(go).GetComponent<ISpreadingConstraint>();
                if (!constraint.IsSpreadingPossible(this, target))
                {
                    return false;
                }
            }

            return true;
        }

        protected void OnValidate()
        {
            foreach (GameObject go in _constraints)
            {
                var constraint = go.GetComponent<ISpreadingConstraint>();
                if (constraint == null)
                {
                    UnityEngine.Debug.LogAssertion($"{go.name} has no component realizing " +
                        $"{nameof(ISpreadingConstraint)} interface", this);
                }
            }
        }

        protected void Awake()
        {
            Chip.CellChanged += UpdateCellSubscriptions;
            Chip.ContainerCellChanged += OnContainerCellChanged;
            UpdateCellSubscriptions(null, null);
        }

        protected void OnEnable()
        {
            TryToRegister();
        }

        protected void OnDisable()
        {
            _spreadingSystem.UnregisterSpreader(this);
        }

        private void OnImpactScheduled(CellComponent cell, Impact impact)
        {
            Assert.IsTrue(cell.Contains(Chip), $"Chip isn't the same as in cell impact scheduling we are listening to");
            if (Chip.WillBeImpacted())
            {
                impact.Cancelling += OnImpactCancelling;
                SpreadingLocked?.Invoke(this);
            }
        }

        private void OnImpactCancelling(Impact impact)
        {
            impact.Cancelling -= OnImpactCancelling;
            SpreadingUnlocked?.Invoke(this);
        }

        private void UpdateCellSubscriptions([CanBeNull] ChipComponent chip, [CanBeNull] CellComponent oldCell)
        {
            if (oldCell != null)
            {
                oldCell.ImpactScheduled -= OnImpactScheduled;
            }

            CellComponent newCell = Chip.Cell;
            if (newCell != null)
            {
                newCell.ImpactScheduled += OnImpactScheduled;
            }
        }

        private void TryToRegister()
        {
            if (Chip.ContainerCell != null)
            {
                _spreadingSystem.RegisterSpreader(this);
            }
        }

        private void OnContainerCellChanged(CellComponent newcell, CellComponent oldcell)
        {
            TryToRegister();
        }
    }
}
