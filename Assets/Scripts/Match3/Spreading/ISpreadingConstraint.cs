﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using JetBrains.Annotations;

namespace DreamTeam.Match3.Spreading
{
    public interface ISpreadingConstraint
    {
        bool IsSpreadingPossible([NotNull] Spreader spreader, [NotNull] CellComponent target);
    }
}