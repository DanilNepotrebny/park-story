﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DreamTeam.GameControls;
using DreamTeam.Match3.FieldStabilization;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Match3
{
    public class AutoHintSystem : MonoBehaviour
    {
        [SerializeField] private GameControl _activityControl;
        [SerializeField] private ChipAnimationInfo _chipAnimInfo;
        [SerializeField, FormerlySerializedAs("StartDelay")] private float _startDelay;

        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;
        [Inject] private MovesSystem _movesSystem;
        [Inject] private GameControlsSystem _controlsSystem;
        [Inject] private SwapSystem _swapSystem;

        private Coroutine _animCoroutine;
        private HintMove _hintMove;
        private bool _isAnimationOngoing;

        public void OverrideAndPlayMove([NotNull] MovesSystem.Move move)
        {
            StopAnimation();

            _hintMove = new HintMove(move);
            PlayHintAnimation(_hintMove);
        }

        public void ResetMoveOverride()
        {
            StopAnimation();
        }

        protected void OnEnable()
        {
            _swapSystem.SwapSucceeding += OnSwapSucceeding;
            _fieldStabilizationSystem.FieldDestabilized += OnFieldDestabilized;
            _fieldStabilizationSystem.AddStabilizationHandler(
                OnFieldStabilized,
                FieldStabilizationHandlerOrder.AutoHintSystem);
            _controlsSystem.AddGameControlStateChangeHandler(_activityControl, OnActivityControlStateChanged);
        }

        protected void OnDisable()
        {
            _controlsSystem.RemoveGameControlStateChangeHandler(_activityControl, OnActivityControlStateChanged);
            _fieldStabilizationSystem.RemoveStabilizationHandler(OnFieldStabilized);
            _fieldStabilizationSystem.FieldDestabilized -= OnFieldDestabilized;
            _swapSystem.SwapSucceeding -= OnSwapSucceeding;
        }

        private void OnActivityControlStateChanged(GameControl.State state)
        {
            switch (state)
            {
                case GameControl.State.Locked:
                    StopAnimation();
                    break;
                case GameControl.State.Unlocked:
                    TryStartAnimation();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        private void OnSwapSucceeding(List<Vector2Int> cellPositions)
        {
            StopAnimation();
        }

        private void OnFieldStabilized()
        {
            TryStartAnimation();
        }

        private void OnFieldDestabilized()
        {
            StopAnimation();
        }

        private bool TryStartAnimation()
        {
            if (!_fieldStabilizationSystem.IsFieldStable ||
                _controlsSystem.IsControlInState(_activityControl, GameControl.State.Locked) ||
                _isAnimationOngoing)
            {
                return false;
            }

            Assert.IsNull(_animCoroutine);
            _animCoroutine = StartCoroutine(StartHintAnimation());
            return true;
        }

        private IEnumerator StartHintAnimation()
        {
            yield return new WaitForSeconds(_startDelay);

            var moves = new List<MovesSystem.Move>();
            bool foundMoves = _movesSystem.FindMoves(moves);
            if (!foundMoves)
            {
                yield break;
            }

            MovesSystem.Move bestMove = _movesSystem.FindTheBestMove(moves);
            _hintMove = new HintMove(bestMove);
            PlayHintAnimation(_hintMove);
        }

        private void PlayHintAnimation(HintMove move)
        {
            Assert.IsFalse(_isAnimationOngoing);
            _isAnimationOngoing = true;

            string triggerName = _chipAnimInfo.GetTriggerName(move.Direction);
            SetChipAnimatorTrigger(move.Chip, triggerName);
            move.MatchChips.ForEach(chip => SetChipAnimatorTrigger(chip, _chipAnimInfo.idleTriggerName));
        }

        private void StopAnimation()
        {
            _isAnimationOngoing = false;
            if (_animCoroutine != null)
            {
                StopCoroutine(_animCoroutine);
                _animCoroutine = null;
            }

            if (_hintMove != null)
            {
                SetChipAnimatorTrigger(_hintMove.Chip, _chipAnimInfo.exitTriggerName);
                _hintMove.MatchChips.ForEach(chip => SetChipAnimatorTrigger(chip, _chipAnimInfo.exitTriggerName));
                _hintMove = null;
            }
        }

        private static void SetChipAnimatorTrigger([CanBeNull] ChipComponent chip, string triggerName)
        {
            if (chip == null)
            {
                return;
            }

            if (chip.gameObject.activeInHierarchy)
            {
                chip.Presenter.SetTrigger(triggerName);
            }
        }

        #region Inner types

        private class HintMove
        {
            public ChipComponent Chip { get; private set; }
            public List<ChipComponent> MatchChips { get; private set; }
            public Direction Direction { get; } = Direction.None;

            public HintMove(MovesSystem.Move move)
            {
                ChipComponent source = move.Source.Chip;

                if (move.Target == null)
                {
                    // Move is activation
                    Initialize(source, new List<ChipComponent>(), null);
                    return;
                }

                Direction = move.SourceMatch.Count >= move.TargetMatch.Count ?
                    CalculateMoveDirection(move.Source, move.Target) :
                    CalculateMoveDirection(move.Target, move.Source);

                ChipComponent target = move.Target.Chip;

                if (move.SourceMatch.Count == 0 && move.TargetMatch.Count == 0)
                {
                    // Move is combination

                    var matchChips = new List<ChipComponent>();
                    if (source == null)
                    {
                        source = target;
                        target = null;

                        Direction = Direction.GetMirrored();
                    }

                    if (target != null)
                    {
                        matchChips.Add(target);
                    }

                    Initialize(source, matchChips, null);
                }
                else if (move.SourceMatch.Count >= move.TargetMatch.Count)
                {
                    Initialize(source, move.SourceMatch, target);
                }
                else
                {
                    Initialize(target, move.TargetMatch, source);
                }
            }

            private void Initialize(
                ChipComponent chip,
                IEnumerable<ChipComponent> matchChips,
                [CanBeNull] ChipComponent nonMatchChip)
            {
                Chip = chip;
                MatchChips = new List<ChipComponent>(matchChips);
                MatchChips.RemoveAll(c => c == nonMatchChip);
                MatchChips.Remove(chip);
            }

            private static Direction CalculateMoveDirection([NotNull] CellComponent sourceCell, [NotNull] CellComponent targetCell)
            {
                Vector2Int sourceIndex = sourceCell.GridPosition;
                Vector2Int targetIndex = targetCell.GridPosition;
                if (sourceIndex.x == targetIndex.x)
                {
                    if (sourceIndex.y > targetIndex.y)
                    {
                        return Direction.Down;
                    }

                    if (sourceIndex.y < targetIndex.y)
                    {
                        return Direction.Up;
                    }
                }
                else if (sourceIndex.y == targetIndex.y)
                {
                    if (sourceIndex.x > targetIndex.x)
                    {
                        return Direction.Left;
                    }

                    if (sourceIndex.x < targetIndex.x)
                    {
                        return Direction.Right;
                    }
                }

                UnityEngine.Debug.LogError("Logic error. There are only horizontal or vertical swaps exist");
                return Direction.None;
            }
        }

        [Serializable]
        private struct ChipAnimationInfo
        {
            [FormerlySerializedAs("ExitTriggerName")]
            public string exitTriggerName;

            [FormerlySerializedAs("IdleTriggerName")]
            public string idleTriggerName;

            [FormerlySerializedAs("TendsToLeftTriggerName")]
            public string tendsToLeftTriggerName;

            [FormerlySerializedAs("TendsToTopTriggerName")]
            public string tendsToTopTriggerName;

            [FormerlySerializedAs("TendsToRightTriggerName")]
            public string tendsToRightTriggerName;

            [FormerlySerializedAs("TendsToBottomTriggerName")]
            public string tendsToBottomTriggerName;

            [FormerlySerializedAs("TapToActivateTriggerName")]
            public string tapToActivateTriggerName;

            public string GetTriggerName(Direction direction)
            {
                switch (direction)
                {
                    case Direction.None:
                        return tapToActivateTriggerName;
                    case Direction.Up:
                        return tendsToTopTriggerName;
                    case Direction.Down:
                        return tendsToBottomTriggerName;
                    case Direction.Left:
                        return tendsToLeftTriggerName;
                    case Direction.Right:
                        return tendsToRightTriggerName;
                    default:
                        throw new InvalidEnumArgumentException(
                            $"Got {direction} direction. But expected None|Up|Down|Left|Right..");
                }
            }
        }

        #endregion Inner types
    }
}