﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(ChipPresenter))]
    public class ChipSwappingDeniedPresenter : MonoBehaviour
    {
        [SerializeField] private DirectionAnimatorTriggerList _directionTriggerList;
        [Inject] private SwapSystem _swapSystem;

        private ChipPresenter _chipPresenter;

        protected void Awake()
        {
            _chipPresenter = GetComponent<ChipPresenter>();
            _swapSystem.SwapDenied += OnSwapDenied;
        }

        protected void OnDestroy()
        {
            _swapSystem.SwapDenied -= OnSwapDenied;
        }

        protected void Reset()
        {
            _directionTriggerList = new DirectionAnimatorTriggerList();
            _directionTriggerList.Add(new KeyValuePair<Direction, string>(Direction.Up, "SwapDeniedUp"));
            _directionTriggerList.Add(new KeyValuePair<Direction, string>(Direction.Down, "SwapDeniedDown"));
            _directionTriggerList.Add(new KeyValuePair<Direction, string>(Direction.Left, "SwapDeniedLeft"));
            _directionTriggerList.Add(new KeyValuePair<Direction, string>(Direction.Right, "SwapDeniedRight"));
        }

        private void OnSwapDenied(CellComponent cellFrom, Direction dir)
        {
            if (cellFrom == _chipPresenter.ChipComponent.Cell)
            {
                _chipPresenter.SetTrigger(_directionTriggerList[dir]);
            }
        }

        [Serializable]
        private class DirectionAnimatorTriggerList : KeyValueList<Direction, string>
        {
        }
    }
}