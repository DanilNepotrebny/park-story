﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Reflection;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Incapsulates working with animator for chip and adds additional animation events.
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class ChipAnimatorContoller : MonoBehaviour, IAnimatorStateExitHandler, IAnimatorStateEnterHandler
    {
        [SerializeField] private string _normalizedTimeParameter = "NormalizedTime";

        private Animator _animator;

        private EventDictionary<string> _animationEventHandlers = new EventDictionary<string>();
        private EventDictionary<int> _animationStateExitHandlers = new EventDictionary<int>();
        private EventDictionary<int> _animationStateEnterHandlers = new EventDictionary<int>();

        public event Action<string> AnimationEventHandlerOccurred;

        public void SetAnimatorEnabled(bool isEnabled)
        {
            _animator.enabled = isEnabled;
        }

        public bool IsAnimatorEnabled()
        {
            return _animator.isActiveAndEnabled;
        }

        public void SetTrigger(string paramName)
        {
            ValidateAnimatorOnParameterChange(paramName);

            _animator.SetTrigger(paramName);
        }

        public void SetBool(string paramName, bool value)
        {
            ValidateAnimatorOnParameterChange(paramName);

            _animator.SetBool(paramName, value);
        }

        public void SetInteger(string paramName, int value)
        {
            ValidateAnimatorOnParameterChange(paramName);

            _animator.SetInteger(paramName, value);
        }

        public void SetFloat(string paramName, float value)
        {
            ValidateAnimatorOnParameterChange(paramName);

            _animator.SetFloat(paramName, value);
        }

        /// <summary>
        /// Called when some event in animation has occurred.
        /// </summary>
        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void OnAnimationEventOccurred(string eventName)
        {
            _animationEventHandlers.Invoke(eventName);
            AnimationEventHandlerOccurred?.Invoke(eventName);
        }

        /// <summary>
        /// Adds handler for animation event.
        /// </summary>
        public void AddAnimationEventHandler(string eventName, Action handler)
        {
            _animationEventHandlers.Add(eventName, handler);
        }

        public void RemoveAnimationEventHandler(string eventName, Action handler)
        {
            _animationEventHandlers.Remove(eventName, handler);
        }

        public void AddAnimationStateExitHandler(string stateName, Action handler)
        {
            AddAnimatorStateHandler(_animationStateExitHandlers, stateName, handler);
        }

        public void RemoveAnimationStateExitHandler(string stateName, Action handler)
        {
            RemoveAnimatorStateHandler(_animationStateExitHandlers, stateName, handler);
        }

        public void AddAnimationStateEnterHandler(string stateName, Action handler)
        {
            AddAnimatorStateHandler(_animationStateEnterHandlers, stateName, handler);
        }

        public void RemoveAnimationStateEnterHandler(string stateName, Action handler)
        {
            RemoveAnimatorStateHandler(_animationStateEnterHandlers, stateName, handler);
        }

        public void SetAnimationNormalizedTime(float time)
        {
            SetFloat(_normalizedTimeParameter, time);
        }

        protected void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        private void AddAnimatorStateHandler(EventDictionary<int> events, string stateName, Action handler)
        {
            events.Add(Animator.StringToHash(stateName), handler);
        }

        private void RemoveAnimatorStateHandler(EventDictionary<int> events, string stateName, Action handler)
        {
            events.Remove(Animator.StringToHash(stateName), handler);
        }

        private void InvokeAnimatorStateHandler(EventDictionary<int> events, AnimatorStateInfo stateInfo)
        {
            events.Invoke(stateInfo.shortNameHash);
        }

        void IAnimatorStateExitHandler.OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            InvokeAnimatorStateHandler(_animationStateExitHandlers, stateInfo);
        }

        void IAnimatorStateEnterHandler.OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            InvokeAnimatorStateHandler(_animationStateEnterHandlers, stateInfo);
        }

        private void ValidateAnimatorOnParameterChange(string paramName)
        {
            if (!_animator.enabled)
            {
                UnityEngine.Debug.LogWarning($"Trying to set integer {paramName} while animator is disabled", this);
            }
        }
    }
}