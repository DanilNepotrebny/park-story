﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System.Collections.Generic;

namespace DreamTeam.Match3.ChipGeneration.PredefinedVariation
{
    public partial class PredefinedVariativeChipGenerationSystem : PredefinedVariativeChipGenerationSystem.IEditorAPI
    {
        public void SyncChipsVariations(HashSet<int> predefinedVariationIds)
        {
            _chipsVariations.RemoveAll(
                    collectionItem => { return !predefinedVariationIds.Contains(collectionItem.VariationId); }
                );

            foreach (int id in predefinedVariationIds)
            {
                if (_chipsVariations.Find(item => item.VariationId == id) == null)
                {
                    _chipsVariations.Add(new ChipsVariation(id));
                }
            }

            _chipsVariations.Sort((A, B) => A.VariationId.CompareTo(B.VariationId));
        }

        public interface IEditorAPI
        {
            void SyncChipsVariations(HashSet<int> predefinedVariationIds);
        }
    }
}

#endif