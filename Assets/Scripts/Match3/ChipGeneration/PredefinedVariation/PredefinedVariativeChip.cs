﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.ChipGeneration.PredefinedVariation
{
    [RequireComponent(typeof(ChipComponent))]
    public partial class PredefinedVariativeChip : MonoBehaviour
    {
        [SerializeField] private int _variationId;

        public int VariationId => _variationId;

        public void ReplaceByChip(ChipComponent chip)
        {
            var parentContainer = transform.parent.GetComponentInParent<ChipContainerComponent>();
            parentContainer.ChangeChipSilently(chip, ChipComponent.MovingType.Immediate);
            gameObject.Dispose();
        }
    }
}