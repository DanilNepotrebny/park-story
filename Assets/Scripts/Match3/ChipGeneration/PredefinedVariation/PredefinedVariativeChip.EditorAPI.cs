﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;
using System.IO;
using UnityEngine;

namespace DreamTeam.Match3.ChipGeneration.PredefinedVariation
{
    [ExecuteInEditMode]
    public partial class PredefinedVariativeChip
    {
        protected void Awake()
        {
            EditorStaticAPI.Created?.Invoke(this);
        }

        protected void OnDestroy()
        {
            EditorStaticAPI.Destroyed?.Invoke(this);
        }

        protected void OnDrawGizmos()
        {
            string iconPath = $"PredefinedVariativeChip/{_variationId}.png";
            if (!File.Exists("Assets/Gizmos/" + iconPath))
            {
                iconPath = $"PredefinedVariativeChip/default.png";
            }

            Gizmos.DrawIcon(transform.position, iconPath);
        }

        public static class EditorStaticAPI
        {
            public static Action<PredefinedVariativeChip> Created;
            public static Action<PredefinedVariativeChip> Destroyed;
        }
    }
}

#endif