﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.ChipGeneration.PredefinedVariation
{
    public partial class PredefinedVariativeChipGenerationSystem : MonoBehaviour
    {
        [SerializeField] private List<ChipsVariation> _chipsVariations = new List<ChipsVariation>();

        [Inject] private GenerationSystem _generationSystem;
        [Inject] private GridSystem _gridSystem;

        private HashSet<ChipComponent> _excludedChips = new HashSet<ChipComponent>();

        protected void Awake()
        {
            foreach (ChipsVariation variation in _chipsVariations)
            {
                foreach (CellComponent cell in _gridSystem.Cells)
                {
                    var predefinedColorChip = cell.GetComponentInChildren<PredefinedVariativeChip>();
                    if (predefinedColorChip != null && variation.VariationId == predefinedColorChip.VariationId)
                    {
                        Apply(predefinedColorChip);
                    }
                }
            }
            _excludedChips.Clear();
        }

        private void Apply(PredefinedVariativeChip target)
        {
            ChipsVariation variation = _chipsVariations.Find(item => item.VariationId == target.VariationId);
            Assert.IsNotNull(
                    variation,
                    $"[{nameof(PredefinedVariativeChipGenerationSystem)}] Can't find variation for id " +
                        $"{target.VariationId}."
                );

            ChipComponent targetChip = variation.GetRandomOrCachedChip(_excludedChips);
            Assert.IsNotNull(
                    targetChip,
                    $"[{nameof(PredefinedVariativeChipGenerationSystem)}] Can't get target chip for variation " +
                        $"with id {target.VariationId}. Please check appropriate chip weights."
                );

            if (targetChip != null)
            {
                _excludedChips.Add(targetChip);
                ChipComponent generatedChip = _generationSystem.InstantiateChip(targetChip);
                target.ReplaceByChip(generatedChip);
            }
        }

        [Serializable]
        private class ChipsVariation
        {
            [SerializeField] private int _variationId;
            [SerializeField] private ChipWeights _chipWeights;

            public int VariationId => _variationId;

            public ChipsVariation(int variationId)
            {
                _variationId = variationId;
                _chipWeights = new ChipWeights();
            }

            public ChipComponent GetRandomOrCachedChip(ICollection<ChipComponent> excludeList)
            {
                return _chipWeights.GetRandomOrCachedChip(excludeList);
            }

            [Serializable]
            private class ChipWeights : KeyValueList<ChipComponent, int>
            {
                private ChipComponent _cachedChip = null;

                public ChipComponent GetRandomOrCachedChip(ICollection<ChipComponent> excludeList)
                {
                    if (_cachedChip == null)
                    {
                        ChipWeights weights = MemberwiseClone() as ChipWeights;
                        weights.RemoveRange(excludeList);
                        _cachedChip = RandomUtils.RandomElement(weights);
                    }

                    return _cachedChip;
                }
            }
        }
    }
}