﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.LevelCompletion;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.ChipGeneration
{
    /// <summary>
    /// Generates random chip in the cell.
    /// </summary>
    public class GeneratorComponent : MonoBehaviour
    {
        [SerializeField] private string _generatedChipPresenterStateTrigger = "OnGeneratorProduced";
        [SerializeField] private ChipTagSet _excludedChipTags;

        [Inject] private GenerationSystem _generation;
        [Inject] private LevelRewardSequence _rewardSequence;

        private const float _offset = 1.72f;
        private CellComponent _cell;
        private bool _applyNotAllowedChipTags = true;
        private HashSet<ChipTag> _notAllowedChipTags;

        public event Action<CellComponent> ChipGenerated;

        protected void Awake()
        {
            _cell = GetComponentInParent<CellComponent>();
            _cell.ChipChanged += OnCellChipChanged;
            _cell.Stabilized += OnCellStabilized;
            _notAllowedChipTags = new HashSet<ChipTag>(_excludedChipTags);
            _rewardSequence.Performing += OnLevelRewardSequencePerforming;
        }

        private void OnLevelRewardSequencePerforming()
        {
            _applyNotAllowedChipTags = false;
        }

        protected void OnDrawGizmos()
        {
            Gizmos.DrawIcon(transform.position, "GeneratorComponent.png");
        }

        private void OnCellChipChanged(CellComponent cell, ChipComponent oldChip)
        {
            TryGenerateChip();
        }

        private void OnCellStabilized()
        {
            TryGenerateChip();
        }

        private void TryGenerateChip()
        {
            if (!_cell.IsEmpty || !_cell.IsStable)
            {
                return;
            }

            ChipComponent chip = _generation.TryGenerateChip(
                    _cell,
                    GenerationSystem.GenerationStage.Generators,
                    _applyNotAllowedChipTags ? _notAllowedChipTags : null
                );

            if (chip == null)
            {
                return;
            }

            chip.Presenter.transform.position -= (Vector3)(Vector2)_cell.Gravity.GetOffset() * _offset;
            chip.Presenter.SetTrigger(_generatedChipPresenterStateTrigger);

            ChipGenerated?.Invoke(_cell);
        }
    }
}
