﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.ChipGeneration
{
    public partial class ChipPrefabsCollector : ChipPrefabsCollector.IEditorApi
    {
        private static int PrefabComparison(GameObject x, GameObject y)
        {
            if (x == null && y == null)
            {
                return 0;
            }
            if (x == null)
            {
                return 1;
            }
            if (y == null)
            {
                return -1;
            }

            return string.Compare(x.name, y.name, StringComparison.Ordinal);
        }

        void IEditorApi.RegisterChipPrefab(GameObject prefab)
        {
            if (prefab != null && !_chipPrefabs.Contains(prefab))
            {
                EditorUtils.RecordObjectUndo(this, "RegisterChipPrefab");
                _chipPrefabs.Add(prefab);
                _chipPrefabs.Sort(PrefabComparison);
            }
        }

        void IEditorApi.CleanUp()
        {
            EditorUtils.RecordObjectUndo(this, "CleanUp");
            _chipPrefabs.RemoveAll(item => item == null);
        }

        #region IEditorApi

        public interface IEditorApi
        {
            void RegisterChipPrefab(GameObject prefab);
            void CleanUp();
        }

        #endregion
    }
}