﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Match3.ChipTags;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Match3.ChipGeneration
{
    public abstract class ChipGenerationRuleBase : MonoBehaviour
    {
        private bool _isStarted;

        public ChipComponent ApplyRule([NotNull] CellComponent cell, HashSet<ChipTag> notAllowedChips = null)
        {
            if (!_isStarted)
            {
                UnityEngine.Debug.LogError($"{GetType().Name} was not Started. Check script execution order", this);
                return null;
            }

            return ApplyRuleChild(cell, notAllowedChips);
        }

        public abstract void EnableReducedMatchesMode();

        protected abstract ChipComponent ApplyRuleChild([NotNull] CellComponent cell, HashSet<ChipTag> notAllowedChips);

        protected void Start()
        {
            _isStarted = true;
        }
    }
}