﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Match3.Matching;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DreamTeam.Match3.ChipGeneration
{
    /// <summary> Responsible for filling the match-3 grid with random chips when level starts. </summary>
    public partial class GenerationSystem : MonoBehaviour
    {
        [Inject] private GridSystem _grid;
        [Inject] private Instantiator _instantiator;
        [Inject] private ShuffleSystem _shuffleSystem;
        [Inject] private MatchSystem _matchSystem;
        [Inject] private LevelCompletionSystem _levelCompletionSystem;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        [SerializeField, HideInInspector,
         Tooltip(
             "Generation rules which will be applied to each empty cell to fill with chip. Rules applies one-by-one sorted by priority until one will be applied successfully")]
        private GenerationRule[] _generationRules;

        [SerializeField, Tooltip("Count of tries to generate valid field.")]
        private int _generationIterationsCount = 10;

        [SerializeField] private int _chipGenerationTriesNumber = 10;
        [SerializeField] private int _chipsCountBeforeMatchesReducing = 20;

        private Action _onFieldGenerated;
        private bool _isFieldGenerated;

        private bool _isMatchesReducingNeeded;

        public event Action<ChipComponent> ChipGenerated;

        [Flags]
        public enum GenerationStage
        {
            FieldInitialization = 1,
            Generators = 1 << 1
        }

        /// <summary> Fired after field is generated. In case of subscription after generation action will be called immediately. </summary>
        public event Action FieldGenerated
        {
            add
            {
                if (_isFieldGenerated)
                {
                    value();
                }
                else
                {
                    _onFieldGenerated += value;
                }
            }
            remove
            {
                if (!_isFieldGenerated)
                {
                    _onFieldGenerated -= value;
                }
            }
        }

        public ChipComponent TryGenerateChip(
            [NotNull] CellComponent cell,
            GenerationStage generationStage,
            HashSet<ChipTag> notAllowedChips = null)
        {
            if (!enabled)
            {
                return null;
            }
            
            ChipComponent generatedChip = GenerateChipSilently(cell, generationStage, notAllowedChips);
            if (generatedChip != null)
            {
                cell.InsertChip(generatedChip);
            }

            RaiseChipGenerated(generatedChip);

            if (_isMatchesReducingNeeded && _chipsCountBeforeMatchesReducing > 0)
            {
                _chipsCountBeforeMatchesReducing--;

                if (_chipsCountBeforeMatchesReducing == 0)
                {
                    foreach (GenerationRule rule in _generationRules)
                    {
                        rule.ChipRule.EnableReducedMatchesMode();
                    }
                }
            }

            return generatedChip;
        }

        /// <summary> Instantiates chip with proper events raised </summary>
        public ChipComponent InstantiateChip(ChipComponent chipPrefab)
        {
            ChipComponent chip = _instantiator.Instantiate(chipPrefab);
            RaiseChipGenerated(chip);
            return chip;
        }

        public ChipComponent InstantiateChip([NotNull] ChipComponent chipPrefab, Vector3 position)
        {
            ChipComponent chip = _instantiator.Instantiate(chipPrefab, position);
            RaiseChipGenerated(chip);
            return chip;
        }

        protected void Start()
        {
            foreach (CellComponent cell in _grid.Cells)
            {
                if (cell != null && _matchSystem.FindMatch(cell.GridPosition) != null)
                {
                    throw new InvalidOperationException(
                        $"There are predefined matches on the level in the cell '{cell.name}' at [{cell.GridPosition.x}, {cell.GridPosition.y}].");
                }
            }

            var generatedChips = new List<ChipComponent>(_grid.CellsCount);

            int generationTries = 0;

            bool isFieldGenerated = false;
            while (!isFieldGenerated && generationTries < _generationIterationsCount)
            {
                var cells = new List<CellComponent>(_grid.Cells);
                cells.RandomShuffle(_random);
                foreach (CellComponent cell in cells)
                {
                    if (!cell.HasGeneratedChip || !cell.CanInsertChip())
                    {
                        continue;
                    }

                    bool generateChip = true;
                    int chipGenerationTriesNumber = 0;
                    do
                    {
                        chipGenerationTriesNumber += 1;
                        if (chipGenerationTriesNumber > _chipGenerationTriesNumber)
                        {
                            throw new InvalidOperationException(
                                $"Couldn't generate chip for {_chipGenerationTriesNumber} tries without providing a match. Check generation rules or smth.");
                        }

                        ChipComponent generatedChip = GenerateChipSilently(cell, GenerationStage.FieldInitialization);
                        if (generatedChip == null)
                        {
                            continue;
                        }

                        cell.InsertChip(generatedChip);

                        if (_matchSystem.HasMatches)
                        {
                            generatedChip.KillSilently();
                            continue;
                        }

                        generateChip = false;
                        generatedChips.Add(generatedChip);
                    }
                    while (generateChip);
                }

                if (_shuffleSystem.TryShuffle(true))
                {
                    isFieldGenerated = true;
                }
                else
                {
                    foreach (ChipComponent generatedChip in generatedChips)
                    {
                        generatedChip.KillSilently();
                    }

                    generatedChips.Clear();
                }

                ++generationTries;
            }

            if (!isFieldGenerated)
            {
                throw new InvalidOperationException(
                    $"Couldn't generate field for {_generationIterationsCount} iterations. It is highly probable that the field is predefined as not valid.");
            }

            foreach (CellComponent cell in _grid.Cells)
            {
                if (cell.Chip != null)
                {
                    RaiseChipGenerated(cell.Chip);
                }
            }

            _isFieldGenerated = true;
            _onFieldGenerated?.Invoke();
            _onFieldGenerated = null;

            _levelCompletionSystem.LevelSucceeded += OnLevelSucceeded;
        }

        protected void OnValidate()
        {
            if (_generationRules.Length == 0)
            {
                UnityEngine.Debug.LogError($"Generation system has no generation rules!", this);
            }

            for (int i = 0; i < _generationRules.Length; ++i)
            {
                GenerationRule rule = _generationRules[i];
                if (rule.ChipRule == null)
                {
                    UnityEngine.Debug.LogError($"Missing assigned generation rule in Element {i}", this);
                }
                #if UNITY_EDITOR
                else if (PrefabUtility.GetPrefabType(rule.ChipRule) == PrefabType.Prefab)
                {
                    UnityEngine.Debug.LogError(
                        $"You assigned {rule.ChipRule.name} prefab into GenerationSystem. Assign instance instead!",
                        this);
                }
                #endif
            }
        }

        private ChipComponent GenerateChipSilently(
            [NotNull] CellComponent cell,
            GenerationStage generationStage,
            HashSet<ChipTag> notAllowedChips = null)
        {
            foreach (var rule in _generationRules)
            {
                if ((rule.Stage & generationStage) == 0)
                {
                    continue;
                }

                ChipComponent chipPrefab = rule.ChipRule.ApplyRule(cell, notAllowedChips);
                if (chipPrefab == null)
                {
                    continue;
                }

                ChipComponent chip = _instantiator.Instantiate(chipPrefab);
                return chip;
            }

            return null;
        }

        private void RaiseChipGenerated(ChipComponent chip)
        {
            while (chip != null)
            {
                ChipGenerated?.Invoke(chip);
                chip = chip.Child;
            }
        }

        private void OnLevelSucceeded()
        {
            _isMatchesReducingNeeded = true;
        }

        #region Internal classes

        [Serializable]
        private struct GenerationRule
        {
            public GenerationStage Stage;
            public ChipGenerationRuleBase ChipRule;
        }

        #endregion Internal classes
    }
}