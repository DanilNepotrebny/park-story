﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.ChipGeneration.GenerationRules
{
    public class RandomChipGenerationRule : ChipGenerationRuleBase
    {
        [Serializable]
        private class ChipWeights : KeyValueList<ChipComponent, int>
        {
        }

        [SerializeField, Tooltip("One of the prefabs choses randomly and generates into cell")]
        private ChipWeights _chipWeights;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        public override void EnableReducedMatchesMode()
        {
            int maxWeight = _chipWeights.Max(p => p.Value);

            foreach (KeyValuePair<ChipComponent, int> pair in _chipWeights.ToArray())
            {
                if (pair.Value == 0)
                {
                    _chipWeights[pair.Key] = maxWeight;
                }
            }
        }

        protected void Awake()
        {
            Assert.IsTrue(_chipWeights.Count > 0, "There is no chips in RandomChipGenerationRule.");
        }

        protected override ChipComponent ApplyRuleChild(CellComponent cell, HashSet<ChipTag> notAllowedChips)
        {
            if (notAllowedChips == null || notAllowedChips.Count == 0)
            {
                return RandomUtils.RandomElement(_chipWeights, _random);
            }

            var allowedChipWeights = new ChipWeights();
            foreach (KeyValuePair<ChipComponent, int> chipWeight in _chipWeights)
            {
                bool isAllowedChip = true;
                ChipComponent chip = chipWeight.Key;
                foreach (ChipTag chipTag in notAllowedChips)
                {
                    if (chip.Tags.Has(chipTag, false))
                    {
                        isAllowedChip = false;
                        break;
                    }
                }

                if (isAllowedChip)
                {
                    allowedChipWeights.Add(chipWeight);
                }
            }

            return allowedChipWeights.Count == 0 ? null : RandomUtils.RandomElement(allowedChipWeights, _random);
        }
    }
}
