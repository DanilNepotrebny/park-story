﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.Requirements.ChipConsumption;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.ChipGeneration.GenerationRules
{
    public class AdvancedChipGenerationRule : ChipGenerationRuleBase
    {
        [Inject(Id = LevelSettings.GoalInstancesId)]
        private IReadOnlyList<Requirement.Instance> _goalInstances;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        [Inject] private GridSystem _gridSystem;
        [Inject] private SwapSystem _swapSystem;

        private const float _probabilityLowerBound = 0;
        private const float _probabilityUpperBound = 1;
        private const float _maxDistance = 1000;

        [SerializeField, Range(1, 100), Tooltip("Maximum amount of chips existing on field at the same time")]
        private int _maxChipsToGenerate;

        [SerializeField, Tooltip(
             "Curve that indicates probability value according to current amount PopcornBucket chips on the field. " +
             "Where, 'time' axis - indicates chips amount, 'value' axis - probability value (0.5 - 50%)")]
        private AnimationCurve _baseProbabilityCurve =
            AnimationCurve.EaseInOut(0, _probabilityLowerBound, 10, _probabilityUpperBound);

        [SerializeField, Tooltip(
             "Curve that indicates additional probability value according to shortest distance to similar " +
             "PopcornBucket chip. Where, 'time' axis - distance value, 'value' axis - probability value (0.5 - 50%). " +
             "This check applies before base probability.")]
        private AnimationCurve _distanceProbabilityCurve =
            AnimationCurve.EaseInOut(0, _probabilityLowerBound, 15, _probabilityUpperBound);

        [SerializeField, Range(0, 10), Tooltip(
             "Amount of turns after which PopcornBucket chip will be generated " +
             "if there were no generation in other way")]
        private int _turnsToGenerateForSure;

        [SerializeField, Tooltip("Common chip tag for chip prefabs")]
        private ChipTag _validationChipTag;

        [SerializeField, Tooltip(
             "Chips that will be generated on field. If there is no appropriate requirement generation goes non-limited")]
        private ChipPrefabsDictionary _chipPrefabs = new ChipPrefabsDictionary();

        private int _chipsSwapped;
        private int _totalChipsToGenerate;
        private bool _isReducedMatchesModeEnabled;

        public override void EnableReducedMatchesMode()
        {
            _isReducedMatchesModeEnabled = true;
        }

        protected new void Start()
        {
            base.Start();

            _chipsSwapped = 0;
            _swapSystem.SwapsFinished += OnSwapsFinished;

            int requirementsAmount = _goalInstances.Select(goalInstance => goalInstance.BaseRequirement)
                .OfType<ChipConsumptionRequirement>()
                .Where(req => req.RequiredChipTag == _validationChipTag)
                .Sum(req => req.RequiredCount);

            if (requirementsAmount == 0)
            {
                _totalChipsToGenerate = int.MaxValue;
            }
            else
            {
                int predefinedAmount = _chipPrefabs
                    .Where(pair => pair.Value > 0)
                    .Sum(pair => GetCurrentAmount(_validationChipTag));

                if (predefinedAmount > requirementsAmount)
                {
                    UnityEngine.Debug.LogWarning("Predefined goal chips more than required by the goal");
                }

                _totalChipsToGenerate = Mathf.Max(requirementsAmount - predefinedAmount, 0);
                Assert.IsTrue(
                    _totalChipsToGenerate > 0,
                    "Calculated that there are no need to generate chips. " +
                    "Check layout and level settings or remove this generation rule");
            }
        }

        protected override ChipComponent ApplyRuleChild(CellComponent cellComponent, HashSet<ChipTag> notAllowedChips)
        {
            if (_isReducedMatchesModeEnabled || _totalChipsToGenerate <= 0)
            {
                return null;
            }

            if (notAllowedChips != null && notAllowedChips.Contains(_validationChipTag))
            {
                return null;
            }

            int existingAmount = GetCurrentAmount(_validationChipTag);
            if (existingAmount >= _maxChipsToGenerate)
            {
                return null;
            }

            ChipComponent chipPrefab = RandomUtils.RandomElement(_chipPrefabs, _random);
            float shortestDistance = CalculateShortestDistanceToSimilarChip(cellComponent);
            float currentProbability = _random.Next(
                (int)(_probabilityLowerBound * 100),
                (int)(_probabilityUpperBound * 100)) / 100.0f;
            float distanceProbability = CalculateDistanceProbability(shortestDistance);

            if (distanceProbability < currentProbability) // didn't pass by distance
            {
                return null;
            }

            if (_chipsSwapped >= _turnsToGenerateForSure)
            {
                _chipsSwapped = 0;
                _totalChipsToGenerate -= 1;
                return chipPrefab;
            }

            float evaluatedProbability = CalculateProbability(existingAmount);

            if (evaluatedProbability > currentProbability ||
                Mathf.Approximately(evaluatedProbability, currentProbability))
            {
                _chipsSwapped = 0;
                _totalChipsToGenerate -= 1;
                return chipPrefab;
            }

            return null;
        }

        private float CalculateDistanceProbability(float shortestDistance)
        {
            return Mathf.Clamp(
                _distanceProbabilityCurve.Evaluate(shortestDistance),
                _probabilityLowerBound,
                _probabilityUpperBound);
        }

        private float CalculateProbability(int existingChipsAmount)
        {
            return Mathf.Clamp(
                _baseProbabilityCurve.Evaluate(existingChipsAmount),
                _probabilityLowerBound,
                _probabilityUpperBound);
        }

        private int GetCurrentAmount([NotNull] ChipTag chipTag)
        {
            int amount = 0;

            foreach (CellComponent cell in _gridSystem.Cells)
            {
                ChipComponent chip = cell.Chip;
                while (chip != null)
                {
                    if (chip.Tags.Has(chipTag, false))
                    {
                        amount += 1;
                        break;
                    }

                    chip = chip.Child;
                }
            }

            return amount;
        }

        private float CalculateShortestDistanceToSimilarChip([NotNull] CellComponent checkingCell)
        {
            float distance = _maxDistance;
            Vector3 checkingCellPos = checkingCell.gameObject.transform.position;

            foreach (CellComponent cell in _gridSystem.Cells)
            {
                ChipComponent chip = cell.Chip;
                if (chip == null || !chip.Tags.Has(_validationChipTag, false))
                {
                    continue;
                }

                if (chip.Cell == null)
                {
                    // Workaround. Somehow chip doesn't know its cell at this stage.
                    // That is why it's possible to generate sequence of popcorn chips
                    return 0;
                }

                Vector3 posDiff = chip.Cell.gameObject.transform.position - checkingCellPos;
                if (posDiff.magnitude < distance)
                {
                    distance = posDiff.magnitude;
                }
            }

            return distance;
        }

        private void OnSwapsFinished()
        {
            ++_chipsSwapped;
        }

        [Serializable] public class ChipPrefabsDictionary : KeyValueList<ChipComponent, int>{}
    }
}