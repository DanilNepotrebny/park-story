﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Match3.ChipGeneration
{
    public partial class ChipPrefabsCollector : ScriptableObject
    {
        [SerializeField] private List<GameObject> _chipPrefabs;

        public List<GameObject> ChipPrefabs => _chipPrefabs;
    }
}
