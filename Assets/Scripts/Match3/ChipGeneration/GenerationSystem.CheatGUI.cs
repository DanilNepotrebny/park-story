﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Debug;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.ChipGeneration
{
    public partial class GenerationSystem
    {
        [SerializeField, RequiredField] private ChipPrefabsCollector _collector;

        [Inject] private TapAndTapSwapInput _input;
        [Inject] private CheatPanel _cheatPanel;

        protected void OnEnable()
        {
            _cheatPanel.AddDrawer(CheatPanel.Category.ChipGeneration, DrawCheatGUI);
        }

        protected void OnDisable()
        {
            _cheatPanel.RemoveDrawer(DrawCheatGUI);
        }

        private void DrawCheatGUI(Rect area)
        {
            if (_input.SelectedCell == null)
            {
                GUIStyle style = new GUIStyle(GUI.skin.label) { normal = { textColor = Color.red } };
                const string text = "Select a cell on the field";
                GUILayout.Label(text, style);
                return;
            }

            foreach (GameObject prefab in _collector.ChipPrefabs)
            {
                if (GUILayout.Button(prefab.name))
                {
                    GameObject go = _instantiator.Instantiate(prefab);
                    ChipComponent chip = go.GetComponent<ChipComponent>();
                    _input.SelectedCell.ForceReplaceChip(chip, ImpactFlag.Replace);
                }
            }
        }
    }
}
