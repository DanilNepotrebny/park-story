﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.GameControls;
using DreamTeam.Popups;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using ModestTree;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.StartSequence
{
    public class ShowPopupForSecondsOnStart : MonoBehaviour
    {
        [SerializeField] private SceneReference _popupScene;
        [SerializeField, Range(0, 10)] private float _showingDuration;

        [SerializeField] private GameControl _activationControl;
        [Header("On popup hidden")] [SerializeField, RequiredField]
        private GameControlsMode _modeToActivate;

        [SerializeField] private GameObject[] _objectsToActivate;

        [Inject] private PopupSystem _popupSystem;
        [Inject] private GameControlsSystem _controlsSystem;

        private ActionDisposable _modeDisableHandle;

        protected void Start()
        {
            if (_activationControl == null ||
                _controlsSystem.IsControlInState(_activationControl, GameControl.State.Locked))
            {
                ShowPopup();
            }
            else
            {
                _controlsSystem.AddGameControlStateChangeHandler(_activationControl, OnControlStateChanged);
            }
        }

        protected void OnDestroy()
        {
            _modeDisableHandle?.Dispose();
            _controlsSystem.RemoveGameControlStateChangeHandler(_activationControl, OnControlStateChanged);
        }

        private void ShowPopup()
        {
            Popup popup = _popupSystem.Show(_popupScene.Name);
            if (popup != null)
            {
                popup.Shown += OnPopupShown;
            }
            else
            {
                // Popup is loading. This is temporal case for now. Checking in coroutine until it will be loaded
                StartCoroutine(WaitForLoadingPopup());
            }
        }

        private void OnControlStateChanged(GameControl.State newState)
        {
            if (newState == GameControl.State.Locked)
            {
                ShowPopup();
                _controlsSystem.RemoveGameControlStateChangeHandler(_activationControl, OnControlStateChanged);
            }
        }

        private void OnPopupShown(Popup popup)
        {
            StartCoroutine(ShowPopupForSeconds(_showingDuration));
        }

        private IEnumerator ShowPopupForSeconds(float duration)
        {
            yield return new WaitForSeconds(duration);

            _popupSystem.Hide(_popupScene.Name, OnPopupHidden);
        }

        private void OnPopupHidden()
        {
            _objectsToActivate?.ForEach(o => o.SetActive(true));
            Assert.IsNull(_modeDisableHandle, $"{_modeDisableHandle} is not freed");
            _modeDisableHandle = _controlsSystem.EnableMode(_modeToActivate);
        }

        private IEnumerator WaitForLoadingPopup()
        {
            while (true)
            {
                Popup popup = _popupSystem.GetPopup(_popupScene.Name);
                if (popup != null)
                {
                    popup.Shown += OnPopupShown;
                    yield break;
                }

                yield return null;
            }
        }
    }
}