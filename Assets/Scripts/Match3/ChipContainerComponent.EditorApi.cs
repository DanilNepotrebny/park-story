﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using JetBrains.Annotations;
using UnityEditor;

namespace DreamTeam.Match3
{
    public partial class ChipContainerComponent : ChipContainerComponent.IEditorApi
    {
        public void SwapChipsWithUndo(ChipContainerComponent other)
        {
            ChipComponent tmp = _chip;

            _chip = other._chip;
            if (_chip != null)
            {
                Undo.SetTransformParent(_chip.transform, transform, "Set other container's chip to be our child");
            }

            other._chip = tmp;
            if (tmp != null)
            {
                Undo.SetTransformParent(tmp.transform, other.transform, "Set out previous chip into other container");
            }
        }

        #region Inner types

        public interface IEditorApi
        {
            void SwapChipsWithUndo([NotNull] ChipContainerComponent other);
        }

        #endregion Inner types
    }
}

#endif // UNITY_EDITOR
