﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.GameControls;
using DreamTeam.Match3.Conveyor;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.Wall;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Spine;
using JetBrains.Annotations;
using ModestTree;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Match3
{
    public partial class GridAnimator : MonoBehaviour, IFieldDestabilizer
    {
        [SerializeField, FormerlySerializedAs("_selectionMode")]
        private GameControlsMode _controlsMode;

        [SerializeField] private AnimationInfo _appearAnimation = new AnimationInfo
        {
            StartPoint = Vector2Int.zero,
            Delay = 0.01f,
            ChipPresenterStateName = "Appear",
            ChipPresenterTriggerParamName = "OnGridAppear"
        };

        [SerializeField] private AnimationInfo _disappearAnimation = new AnimationInfo
        {
            StartPoint = Vector2Int.zero,
            Delay = 0.01f,
            ChipPresenterStateName = "Disappear",
            ChipPresenterTriggerParamName = "OnGridDisappear"
        };

        [Inject] private GridSystem _gridSystem;
        [Inject] private List<GridBezelAnimator> _gridBezelAnimators;
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;
        [Inject] private List<HiddenObject> _hiddenObjects;
        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private ConveyorSystem _conveyorSystem;
        [InjectOptional] private WallSystem _wallSystem;

        private bool _isAnimationOngoing;
        private SimpleAnimationType _currentAnimation;
        private bool[] _cellsToAnimate;
        private bool _isGridHidden;
        private List<Vector3> _animatedBezelTiles;
        private Dictionary<CellComponent, CellAnimationData> _cellsAnimationData;
        private GameControlsContext _controlsContext;
        private Coroutine _gridAnimationCoroutine;
        private ActionDisposable _modeDisposeHandle;
        private const string ChipAnimationTypeParamName = "GridChipAnimationType";

        #region IFieldDestabilizer

        public bool IsStable => !_isAnimationOngoing && !_isGridHidden;

        public event Action Stabilized;
        public event Action Destabilized;

        #endregion IFieldDestabilizer

        public event AnimationTypeDelegate AnimationStarting;
        public event AnimationInCellDelegate AnimationInCellStarting;
        public event AnimationTypeDelegate AnimationFinished;

        private bool IsAnimationOngoing
        {
            set
            {
                if (_isAnimationOngoing != value)
                {
                    _isAnimationOngoing = value;
                    InvokeStabilizationEvent();
                }
            }
            get { return _isAnimationOngoing; }
        }

        private bool IsGridHidden
        {
            set
            {
                if (_isGridHidden != value)
                {
                    _isGridHidden = value;
                    InvokeStabilizationEvent();
                }
            }
            get { return _isGridHidden; }
        }

        public void PlayAnimation(SimpleAnimationType animationType)
        {
            if (_gridAnimationCoroutine != null)
            {
                StopCoroutine(_gridAnimationCoroutine);
            }
            foreach (GridBezelAnimator bezelAnimator in _gridBezelAnimators)
            {
                if (bezelAnimator != null)
                {
                    bezelAnimator.StopAllAnimations();
                }
            }

            _gridAnimationCoroutine = StartCoroutine(PlayAnimationInternal(animationType));
        }

        protected void Awake()
        {
            _fieldStabilizationSystem.AddDestabilizer(this);
            _controlsContext = _gameControlsSystem.ActiveContext; // TODO: Inject controls context
            IsGridHidden = true;

            AwakeOnPartGravity();
        }

        protected void Start()
        {
            _appearAnimation.StartPoint.x = Mathf.Clamp(_appearAnimation.StartPoint.x, 0, _gridSystem.Width - 1);
            _appearAnimation.StartPoint.y = Mathf.Clamp(_appearAnimation.StartPoint.y, 0, _gridSystem.Height - 1);
            _disappearAnimation.StartPoint.x = Mathf.Clamp(_disappearAnimation.StartPoint.x, 0, _gridSystem.Width - 1);
            _disappearAnimation.StartPoint.y = Mathf.Clamp(_disappearAnimation.StartPoint.y, 0, _gridSystem.Height - 1);

            _animatedBezelTiles = new List<Vector3>((_gridSystem.Width + 1) * (_gridSystem.Height + 1));
            int cellsAmount = _gridSystem.Width * _gridSystem.Height;
            _cellsAnimationData = new Dictionary<CellComponent, CellAnimationData>(cellsAmount);
            _cellsToAnimate = new bool[cellsAmount];

            float cellHalfWidth = _gridSystem.CellWidth * 0.5f;
            float cellHalfHeight = _gridSystem.CellHeight * 0.5f;

            foreach (CellComponent cell in _gridSystem.Cells)
            {
                _cellsAnimationData.Add(cell, new CellAnimationData());
                cell.gameObject.SetActive(false);

                List<Wall.Wall> walls = FindAssociatedWalls(cell);
                walls.ForEach(w => w.gameObject.SetActive(false));

                Vector3 cellPos = cell.transform.position;

                var cellTL = new Vector3(cellPos.x - cellHalfWidth, cellPos.y + cellHalfHeight, cellPos.z);
                var cellTR = new Vector3(cellPos.x + cellHalfWidth, cellPos.y + cellHalfHeight, cellPos.z);
                var cellBL = new Vector3(cellPos.x + cellHalfWidth, cellPos.y - cellHalfHeight, cellPos.z);
                var cellBR = new Vector3(cellPos.x - cellHalfWidth, cellPos.y - cellHalfHeight, cellPos.z);

                foreach (GridBezelAnimator bezelAnimator in _gridBezelAnimators)
                {
                    if (bezelAnimator != null)
                    {
                        bezelAnimator.SetTileAlpha(cellTL, 0);
                        bezelAnimator.SetTileAlpha(cellTR, 0);
                        bezelAnimator.SetTileAlpha(cellBL, 0);
                        bezelAnimator.SetTileAlpha(cellBR, 0);
                    }
                }
            }

            foreach (HiddenObject hiddenObject in _hiddenObjects)
            {
                hiddenObject.Released += (ho, i) => _hiddenObjects.Remove(ho);
                hiddenObject.gameObject.SetActive(false);
            }

            SetupGravityAnimation();
        }

        protected void OnDestroy()
        {
            ReleaseModeDisableHandle();
            OnDestroyForGravity();
            _fieldStabilizationSystem.RemoveDestabilizer(this);
        }

        private IEnumerator PlayAnimationInternal(SimpleAnimationType animationType)
        {
            Assert.IsNull(_modeDisposeHandle, $"{nameof(_modeDisposeHandle)} is not freed");
            if (_controlsMode != null)
            {
                _modeDisposeHandle = _controlsContext.EnableMode(_controlsMode);
            }

            IsAnimationOngoing = true;

            AnimationStarting?.Invoke(animationType);

            _currentAnimation = animationType;
            _animatedBezelTiles.Clear();
            _cellsToAnimate.Fill(true);

            foreach (CellComponent cell in _gridSystem.Cells)
            {
                CellAnimationData data = _cellsAnimationData[cell];
                data.Animations = cell.GetComponentsInChildren<SimpleAnimation>();
                data.Animators = cell.GetComponentsInChildren<SimpleAnimator>();
                data.SkeletonIdleAnimations = cell.GetComponentsInChildren<SkeletonIdleAnimationHandler>();

                data.ChipPresenters = new List<ChipPresenter>();
                ChipComponent chip = cell.Chip;
                while (chip != null)
                {
                    data.ChipPresenters.Add(chip.Presenter);

                    chip = chip.IsChildVisible ? chip.Child : null;
                }
            }

            Vector2Int startPoint = GetAnimStartPoint(animationType);

            yield return StartCoroutine(AnimateCell(startPoint.x, startPoint.y));

            _hiddenObjects.ForEach(hidden => hidden.gameObject.SetActive(true));

            IsAnimationOngoing = false;
            ReleaseModeDisableHandle();

            IsGridHidden = _currentAnimation == SimpleAnimationType.Hide;

            AnimationFinished?.Invoke(_currentAnimation);
            _gridAnimationCoroutine = null;
        }

        private void ReleaseModeDisableHandle()
        {
            _modeDisposeHandle?.Dispose();
            _modeDisposeHandle = null;
        }

        // simple wave algorithm
        // TODO: Get rid of recursive calls
        private IEnumerator AnimateCell(int x, int y)
        {
            int index = CalculateIndex(x, y);
            if (!_cellsToAnimate[index])
            {
                yield break;
            }

            _cellsToAnimate[index] = false;

            yield return new WaitForSeconds(GetAnimDelay(_currentAnimation));

            AnimationInCellStarting?.Invoke(_currentAnimation, new Vector2Int(x, y));

            var neighbourTilesAnimationCoroutines = new List<Coroutine>(4);
            if (x - 1 >= 0)
            {
                neighbourTilesAnimationCoroutines.Add(StartCoroutine(AnimateCell(x - 1, y)));
            }
            if (y - 1 >= 0)
            {
                neighbourTilesAnimationCoroutines.Add(StartCoroutine(AnimateCell(x, y - 1)));
            }
            if (x + 1 < _gridSystem.Width)
            {
                neighbourTilesAnimationCoroutines.Add(StartCoroutine(AnimateCell(x + 1, y)));
            }
            if (y + 1 < _gridSystem.Height)
            {
                neighbourTilesAnimationCoroutines.Add(StartCoroutine(AnimateCell(x, y + 1)));
            }

            CellComponent cell = _gridSystem[x, y];
            if (cell != null)
            {
                float cellHalfWidth = _gridSystem.CellWidth * 0.5f;
                float cellHalfHeight = _gridSystem.CellHeight * 0.5f;
                Vector3 cellPos = cell.transform.position;

                var cellTL = new Vector3(cellPos.x - cellHalfWidth, cellPos.y + cellHalfHeight, cellPos.z);
                var cellTR = new Vector3(cellPos.x + cellHalfWidth, cellPos.y + cellHalfHeight, cellPos.z);
                var cellBL = new Vector3(cellPos.x + cellHalfWidth, cellPos.y - cellHalfHeight, cellPos.z);
                var cellBR = new Vector3(cellPos.x - cellHalfWidth, cellPos.y - cellHalfHeight, cellPos.z);

                List<Wall.Wall> walls = FindAssociatedWalls(cell);

                if (_currentAnimation == SimpleAnimationType.Show)
                {
                    cell.gameObject.SetActive(true);
                    walls.ForEach(w => w.gameObject.SetActive(true));
                }

                float bezelAnimDuration = 0;
                CellAnimationData cellAnimData = _cellsAnimationData[cell];

                int animatingPresentersCount = 0;
                if (cellAnimData.ChipPresenters != null)
                {
                    foreach (ChipPresenter presenter in cellAnimData.ChipPresenters)
                    {
                        if (presenter != null &&
                            presenter.SetTrigger(GetChipPresenterTriggerParamName(_currentAnimation)))
                        {
                            if (_conveyorSystem.HasAnyBlock(presenter.ChipComponent.Cell))
                            {
                                presenter.SetFloat(ChipAnimationTypeParamName, (float)ChipAnimationType.Conveyor);
                            }

                            animatingPresentersCount += 1;
                            presenter.AddAnimationStateExitHandler(
                                    GetChipPresenterStateName(_currentAnimation),
                                    () => animatingPresentersCount -= 1
                                );
                        }
                    }
                }

                // cell animations
                foreach (SimpleAnimation simpleAnim in cellAnimData.Animations)
                {
                    simpleAnim.Play(_currentAnimation);

                    float clipLength = simpleAnim.GetAnimationDuration(_currentAnimation);
                    bezelAnimDuration = Mathf.Max(clipLength, bezelAnimDuration);
                }

                // cell content animations
                cellAnimData.Animators.ForEach(anim => anim.Play(_currentAnimation));

                // animate walls
                foreach (Wall.Wall wall in walls)
                {
                    if (!wall.Presenter.IsPlayingAnimation)
                    {
                        wall.Presenter.Play(_currentAnimation);
                    }
                }

                var bezelTilesAnimationCoroutines = new List<Coroutine>(4)
                {
                    StartCoroutine(AnimateBezelTile(cellTL, bezelAnimDuration)),
                    StartCoroutine(AnimateBezelTile(cellTR, bezelAnimDuration)),
                    StartCoroutine(AnimateBezelTile(cellBL, bezelAnimDuration)),
                    StartCoroutine(AnimateBezelTile(cellBR, bezelAnimDuration))
                };

                // wait for animations end
                yield return new WaitUntil(() => animatingPresentersCount == 0);

                foreach (SimpleAnimation simpleAnimation in cellAnimData.Animations)
                {
                    yield return new WaitUntil(() => !simpleAnimation.IsPlaying);
                }

                foreach (SimpleAnimator simpleAnimator in cellAnimData.Animators)
                {
                    yield return new WaitUntil(() => !simpleAnimator.IsPlaying);
                }

                foreach (Coroutine coroutine in bezelTilesAnimationCoroutines)
                {
                    yield return coroutine;
                }

                switch (_currentAnimation)
                {
                    case SimpleAnimationType.Show:
                        foreach (SkeletonIdleAnimationHandler anim in cellAnimData.SkeletonIdleAnimations)
                        {
                            if (anim.isActiveAndEnabled)
                            {
                                anim.PlayAnimation(true);
                            }
                        }

                        break;

                    case SimpleAnimationType.Hide:
                        cell.gameObject.SetActive(false);
                        walls.ForEach(w => w.gameObject.SetActive(false));
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(nameof(_currentAnimation), _currentAnimation, null);
                }
            }

            foreach (Coroutine coroutine in neighbourTilesAnimationCoroutines)
            {
                yield return coroutine;
            }
        }

        private IEnumerator AnimateBezelTile(Vector3 tilePos, float duration)
        {
            if (_animatedBezelTiles.Contains(tilePos))
            {
                yield break;
            }

            _animatedBezelTiles.Add(tilePos);
            GridBezelAnimator.AnimationType animationType = ConvertToGridBezelAnimationType(_currentAnimation);

            foreach (GridBezelAnimator bezelAnimator in _gridBezelAnimators)
            {
                if (bezelAnimator != null)
                {
                    bezelAnimator.PlayAnimation(tilePos, animationType, duration);
                }
            }

            yield return new WaitUntil(
                    () =>
                    {
                        foreach (GridBezelAnimator bezelAnimator in _gridBezelAnimators)
                        {
                            if (bezelAnimator == null)
                            {
                                continue;
                            }

                            if (bezelAnimator.IsPlaying(tilePos))
                            {
                                return false;
                            }
                        }

                        return true;
                    }
                );
        }

        private Vector2Int GetAnimStartPoint(SimpleAnimationType type)
        {
            switch (type)
            {
                case SimpleAnimationType.Show:
                    return _appearAnimation.StartPoint;

                case SimpleAnimationType.Hide:
                    return _disappearAnimation.StartPoint;

                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private float GetAnimDelay(SimpleAnimationType type)
        {
            switch (type)
            {
                case SimpleAnimationType.Show:
                    return _appearAnimation.Delay;

                case SimpleAnimationType.Hide:
                    return _disappearAnimation.Delay;

                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private string GetChipPresenterTriggerParamName(SimpleAnimationType type)
        {
            switch (type)
            {
                case SimpleAnimationType.Show:
                    return _appearAnimation.ChipPresenterTriggerParamName;

                case SimpleAnimationType.Hide:
                    return _disappearAnimation.ChipPresenterTriggerParamName;

                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private string GetChipPresenterStateName(SimpleAnimationType type)
        {
            switch (type)
            {
                case SimpleAnimationType.Show:
                    return _appearAnimation.ChipPresenterStateName;

                case SimpleAnimationType.Hide:
                    return _disappearAnimation.ChipPresenterStateName;

                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private int CalculateIndex(int x, int y)
        {
            return y * _gridSystem.Width + x;
        }

        private static GridBezelAnimator.AnimationType ConvertToGridBezelAnimationType(SimpleAnimationType type)
        {
            switch (type)
            {
                case SimpleAnimationType.Show:
                    return GridBezelAnimator.AnimationType.Show;

                case SimpleAnimationType.Hide:
                    return GridBezelAnimator.AnimationType.Hide;

                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private void InvokeStabilizationEvent()
        {
            if (IsStable)
            {
                Stabilized?.Invoke();
            }
            else
            {
                Destabilized?.Invoke();
            }
        }

        private List<Wall.Wall> FindAssociatedWalls([NotNull] CellComponent cell)
        {
            if (_wallSystem == null)
            {
                return new List<Wall.Wall>(0);
            }

            return _wallSystem.FindAssociated(cell.GridPosition);
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }

        #region Inner types

        [Serializable]
        private struct AnimationInfo
        {
            public Vector2Int StartPoint;
            public float Delay;
            public string ChipPresenterStateName;
            public string ChipPresenterTriggerParamName;
        }

        private class CellAnimationData
        {
            public List<ChipPresenter> ChipPresenters;
            public SimpleAnimation[] Animations;
            public SimpleAnimator[] Animators;
            public SkeletonIdleAnimationHandler[] SkeletonIdleAnimations;
        }

        public enum ChipAnimationType
        {
            Default = 0,
            Conveyor = 1,
        }

        public delegate void AnimationTypeDelegate(SimpleAnimationType animationType);
        public delegate void AnimationInCellDelegate(SimpleAnimationType animationType, Vector2Int cellGridPosition);

        #endregion Inner types
    }
}