﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Match3
{
    public interface IGridPositionProvider
    {
        Vector2Int GridPosition { get; }
    }
}