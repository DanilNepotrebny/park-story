﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.UI.SimpleAnimating;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.FieldStabilization
{
    [RequireComponent(typeof(SimpleAnimation))]
    public class SimpleAnimationFieldDestabilizer : MonoBehaviour, IFieldDestabilizer
    {
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;

        public event Action Stabilized;
        public event Action Destabilized;
        public bool IsStable => !_simpleAnimation.IsPlaying;

        private SimpleAnimation _simpleAnimation;

        protected void Awake()
        {
            _simpleAnimation = GetComponent<SimpleAnimation>();
            _fieldStabilizationSystem.AddDestabilizer(this);
        }

        protected void Start()
        {
            _simpleAnimation.AnimationStarted += OnAnimationStarted;
            _simpleAnimation.AnimationStopped += OnAnimationStopped;
        }

        protected void OnDestroy()
        {
            _fieldStabilizationSystem.RemoveDestabilizer(this);
        }

        private void OnAnimationStarted(SimpleAnimationType animationType)
        {
            Destabilized?.Invoke();
        }

        private void OnAnimationStopped(SimpleAnimationType animationType)
        {
            Stabilized?.Invoke();
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }
    }
}