﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Match3.FieldStabilization
{
    /// <summary>
    /// Behaviour for animator state machine to work in conjunction with <see cref="AnimatorFieldDestabilizer"/>. 
    /// Put this behaviour on a state that should destabilize the field.
    /// </summary>
    [SharedBetweenAnimators]
    public class FieldDestabilizingAnimatorState : StateMachineBehaviour
    {
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);

            var destabilizer = animator.GetComponent<AnimatorFieldDestabilizer>();
            destabilizer?.OnDestabilizingStateEntered();
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);

            var destabilizer = animator.GetComponent<AnimatorFieldDestabilizer>();
            destabilizer?.OnDestabilizingStateExited();
        }
    }
}