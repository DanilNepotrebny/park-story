﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;

namespace DreamTeam.Match3.FieldStabilization
{
    public interface IFieldDestabilizer
    {
        event Action Stabilized;
        event Action Destabilized;

        bool IsStable { get; }
        string GetDebugStateDescription();
    }
}