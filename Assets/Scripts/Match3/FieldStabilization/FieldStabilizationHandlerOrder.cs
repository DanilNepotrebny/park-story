﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3.FieldStabilization
{
    public enum FieldStabilizationHandlerOrder
    {
        ChipSpawnSystem,
        AutoHintSystem,
        Conveyor,
        MagicHatSystem,
        Shuffle,
        GravityAnimating,
        Bot,
        CutscenePlayerByFieldStabilization,
        WaitForFieldStabilizedBehaviour,
    }
}