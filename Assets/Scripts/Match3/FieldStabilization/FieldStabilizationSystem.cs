﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Text;
using DreamTeam.Debug;
using DreamTeam.GameControls;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.FieldStabilization
{
    public class FieldStabilizationSystem : MonoBehaviour
    {
        [SerializeField, Tooltip("Enables when field is stabilized")]
        private GameControlsMode _gameMode;

        [Inject] private DebugStatesSystem _debugStatesSystem;
        [Inject] private GameControlsSystem _gameControlsSystem;

        private readonly List<IFieldDestabilizer> _destabilizers = new List<IFieldDestabilizer>();

        private bool _isDirty;
        private bool _isFieldStable = true;
        private GameControlsContext _controlsContext;
        private ActionDisposable _modeDisableHandle;
        private OrderedEvent _fieldStabilized = new OrderedEvent();

        public event Action FieldDestabilized;

        /// <summary> Is field and all it's elements stable (there are no ongoing animations, moves, swaps and so on) </summary>
        public bool IsFieldStable
        {
            get { return _isFieldStable; }
            private set
            {
                if (_isFieldStable == value)
                {
                    return;
                }

                _isFieldStable = value;
                if (value)
                {
                    Assert.IsNull(_modeDisableHandle, $"Previous game mode disable handle has not been disposed");
                    _modeDisableHandle = _controlsContext.EnableMode(_gameMode);
                    _fieldStabilized?.Invoke();
                }
                else
                {
                    ReleaseModeDisableHandle();
                    _fieldStabilized.Cancel();
                    FieldDestabilized?.Invoke();
                }
            }
        }

        public void AddDestabilizer(IFieldDestabilizer destabilizer)
        {
            if (_destabilizers.Contains(destabilizer))
            {
                return;
            }

            _destabilizers.Add(destabilizer);
            destabilizer.Stabilized += OnStabilized;
            destabilizer.Destabilized += OnDestabilized;
            _isDirty = true;
        }

        public void RemoveDestabilizer(IFieldDestabilizer destabilizer)
        {
            destabilizer.Stabilized -= OnStabilized;
            destabilizer.Destabilized -= OnDestabilized;
            _destabilizers.Remove(destabilizer);
            _isDirty = true;
        }

        public void AddStabilizationHandler(Action handler, FieldStabilizationHandlerOrder order)
        {
            _fieldStabilized.Add(handler, (int)order);
        }

        public void RemoveStabilizationHandler(Action handler)
        {
            _fieldStabilized.Remove(handler);
        }

        protected void OnEnable()
        {
            _debugStatesSystem.RegisterWriter(nameof(FieldStabilizationSystem), DrawDebugState);
            _controlsContext = _gameControlsSystem.ActiveContext;
        }

        protected void OnDisable()
        {
            _debugStatesSystem.UnregisterWriter(DrawDebugState);
        }

        protected void OnDestroy()
        {
            ReleaseModeDisableHandle();
        }

        protected void LateUpdate()
        {
            if (!_isDirty)
            {
                return;
            }

            _isDirty = false;

            foreach (IFieldDestabilizer destabilizer in _destabilizers)
            {
                if (!destabilizer.IsStable)
                {
                    IsFieldStable = false;
                    return;
                }
            }

            IsFieldStable = true;
        }

        private void ReleaseModeDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }

        private void OnStabilized()
        {
            _isDirty = true;
        }

        private void OnDestabilized()
        {
            IsFieldStable = false;
            _isDirty = false;
        }

        private void DrawDebugState(StringBuilder builder)
        {
            builder.AppendParameter("Stable", IsFieldStable.ToString());

            if (IsFieldStable)
            {
                return;
            }

            builder.AppendLine("\nDestabilizers:");

            foreach (IFieldDestabilizer destabilizer in _destabilizers)
            {
                if (destabilizer.IsStable)
                {
                    continue;
                }

                var mb = destabilizer as MonoBehaviour;
                string gameObjectName = mb != null ? mb.name : "---";
                string componentType = destabilizer.GetType().Name;

                builder.AppendFormat($"{gameObjectName} ({componentType}): {destabilizer.GetDebugStateDescription()}\n");
            }
        }
    }
}