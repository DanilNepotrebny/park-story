﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.FieldStabilization
{
    public class AnimatorFieldDestabilizer : MonoBehaviour, IFieldDestabilizer
    {
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;

        private int _counter;

        private int _destabilizationCounter
        {
            get { return _counter; }
            set
            {
                bool wasStable = IsStable;
                _counter = value;

                if (!wasStable && IsStable)
                {
                    Stabilized?.Invoke();
                }
                else if (wasStable && !IsStable)
                {
                    Destabilized?.Invoke();
                }
            }
        }

        public bool IsStable => _destabilizationCounter == 0;

        public event Action Stabilized;
        public event Action Destabilized;

        public void OnDestabilizingStateExited()
        {
            _destabilizationCounter--;
        }

        public void OnDestabilizingStateEntered()
        {
            _destabilizationCounter++;
        }

        protected void Awake()
        {
            _fieldStabilizationSystem.AddDestabilizer(this);
        }

        protected void OnDestroy()
        {
            _fieldStabilizationSystem.RemoveDestabilizer(this);
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }
    }
}