﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using DreamTeam.Utils.Movement;
using DreamTeam.Utils.RendererSorting;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(Animator), typeof(MovementSequence))]
    public class HiddenObjectPresenter : MonoBehaviour, IAnimatorStateExitHandler, ISortingOptionsProvider
    {
        [SerializeField] private string _horizontalParameter = "IsHorizontal";
        [SerializeField] private string _consumptionTrigger = "OnConsumption";
        [SerializeField] private string _consumptionFinalState = "Consumption";
        [SerializeField, SortingLayer] private int _consumptionLayer;
        [SerializeField] private AnimationCurve _scaleEasing;
        [SerializeField] private ParticleSystem _flyingEffectPrefab;
        [SerializeField] private GameObject[] _effectSpawnPoints;

        [Inject] private Instantiator _instantiator;

        private Animator _animator;
        private SpriteRenderer _renderer;
        private ParticleSystem _flyingEffect;

        private Action _finishedHandler;
        private RectTransform _targetRect;

        public SortingOptions SortingOptions { get; private set; }

        public void StartSequence(Action finishedHandler, RectTransform targetRect)
        {
            _finishedHandler = finishedHandler;
            _targetRect = targetRect;

            _animator.SetBool(_horizontalParameter, transform.rotation.z != 0f);
            _animator.SetTrigger(_consumptionTrigger);

            SortingOptions.PushSortingLayer(_consumptionLayer);

            foreach (GameObject spawnPoint in _effectSpawnPoints)
            {
                spawnPoint.SetActive(true);
            }
        }

        protected void Awake()
        {
            _animator = GetComponent<Animator>();
            _renderer = GetComponentInChildren<SpriteRenderer>();
            Assert.IsNotNull(_renderer, "Can't find sprite renderer in hidden object");

            SortingOptions = _instantiator.Instantiate<SortingOptions>(
                _renderer.sortingLayerID,
                _renderer.sortingOrder);
            SortingOptions.SortingLayerChanged += OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged += OnSortingOrderChanged;
        }

        protected void OnDestroy()
        {
            SortingOptions.SortingLayerChanged -= OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged -= OnSortingOrderChanged;
            SortingOptions.Dispose();
            SortingOptions = null;
        }

        private void OnMovementFinished(MovementSequence mover)
        {
            if (_flyingEffect != null)
            {
                _flyingEffect.Stop();
                _flyingEffect.transform.SetParent(null, false);
            }

            _finishedHandler?.Invoke();
        }

        void IAnimatorStateExitHandler.OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (stateInfo.shortNameHash == Animator.StringToHash(_consumptionFinalState))
            {
                if (_flyingEffectPrefab != null)
                {
                    _flyingEffect = _instantiator.Instantiate(_flyingEffectPrefab, Vector3.zero, gameObject);
                }

                _animator.enabled = false;

                transform.rotation = Quaternion.identity;

                Vector3[] corners = new Vector3[(int)RectCorner.Max];
                _targetRect.GetWorldCorners(corners);
                float targetWidth = (corners[(int)RectCorner.TopRight] - corners[(int)RectCorner.TopLeft]).x;
                float targetHeight = (corners[(int)RectCorner.TopLeft] - corners[(int)RectCorner.BottomLeft]).y;

                Vector3 sourceSize = _renderer.sprite.bounds.size;
                sourceSize.Scale(_renderer.transform.localScale);

                float scale = Mathf.Min(targetWidth / sourceSize.x, targetHeight / sourceSize.y);

                Vector3 startScale = transform.localScale;
                Vector3 finalScale = new Vector3(scale, scale, scale);

                var mover = GetComponent<MovementSequence>();
                mover.Finished += OnMovementFinished;
                mover.Updated += (sequence, f) =>
                {
                    transform.localScale = _scaleEasing.Lerp(startScale, finalScale, f);
                };
                mover.Move(_targetRect.transform.position);
            }
        }

        private void OnSortingOrderChanged(SortingOptions options, int prevOrder)
        {
            _renderer.sortingOrder = options.SortingOrder;
        }

        private void OnSortingLayerChanged(SortingOptions options)
        {
            _renderer.sortingLayerID = options.SortingLayerId;
        }
    }
}