﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Text;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.MoveWeight;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Match3
{
    using MovementHandler = Action<CellComponent, ChipComponent.MovingType>;
    using MovementHandlers =
        EventDictionary<CellComponent.MovementHandlerType, CellComponent, ChipComponent.MovingType>;

    /// <summary>
    /// Encapsulates functionality of a match-3 cell.
    /// </summary>
    [DisallowMultipleComponent, RequireComponent(typeof(ChipContainerComponent))]
    public partial class CellComponent : MonoBehaviour, IFieldDestabilizer, Impact.IProvider
    {
        [FormerlySerializedAs("_cellPresenter"), SerializeField] private CellPresenter _presenter;
        [SerializeField] private Direction _gravity = DefaultGravity;
        [SerializeField] private bool _hasGeneratedChip = true;
        [SerializeField] private bool _allowBoosterChipGeneration = true;
        [SerializeField] private bool _isMoveGenerationEnabled = true;
        [SerializeField] private CellMoveWeightCriterion[] _cellMoveWeightCriteria;

        private ChipContainerComponent _chipContainer;
        private GridSystem _gridSystem;
        private Instantiator _instantiator;

        private List<ProcessType> _activeProcesses = new List<ProcessType>();
        private List<Impact> _scheduledImpacts = new List<Impact>();

        private MovementHandlers _movementStopHandlers = new MovementHandlers();
        private MovementHandlers _movementFinishHandlers = new MovementHandlers();

        public const Direction DefaultGravity = Direction.Down;

        /// <summary>
        /// Does cell have chip initially set in the editor.
        /// </summary>
        public bool HasPredefinedChip { get; private set; }

        public bool IsMoveGenerationEnabled => _isMoveGenerationEnabled;

        public Vector2Int GridPosition { get; private set; }

        public CellPresenter Presenter => _presenter;
        public bool HasGeneratedChip => _hasGeneratedChip;
        public IWeightCriterion[] WeightCalculator => _cellMoveWeightCriteria;
        public bool AllowBoosterChipGeneration => _allowBoosterChipGeneration;

        /// <summary>
        /// Gets or sets the chip from/into this cell.
        /// </summary>
        public virtual ChipComponent Chip
        {
            get { return _chipContainer.Chip; }
            set { _chipContainer.Chip = value; }
        }

        /// <summary>
        /// Gets or sets the chip from/into this cell.
        /// </summary>
        public virtual Underlay Underlay { get; private set; }

        /// <summary>
        /// Direction of gravity force in this cell.
        /// </summary>
        public Direction Gravity => _gravity;

        public IEnumerable<Impact.IData> ScheduledImpacts => _scheduledImpacts;

        public bool IsStable => _activeProcesses.Count == 0;

        public bool IsEmpty => _chipContainer.Chip == null;

        /// <summary>
        /// Triggered when the cell becomes empty.
        /// </summary>
        public event ChipChangedDelegate ChipChanged;
        public event ChipChangedSilentlyDelegate ChipChangedSilently;

        public event ImpactedDelegate Impacted;
        public event ImpactCanceledDelegate ImpactCanceled;
        public event ImpactScheduledDelegate ImpactScheduled;

        public event Action Stabilized;
        public event Action Destabilized;

        [Inject]
        private void Construct(
                ChipContainerComponent chipContainer,
                GridSystem gridSystem,
                Instantiator instantiator,
                FieldStabilizationSystem stabilizationSystem,
                [InjectOptional] Underlay underlay
            )
        {
            _chipContainer = chipContainer;
            _gridSystem = gridSystem;
            _instantiator = instantiator;
            Underlay = underlay;

            stabilizationSystem.AddDestabilizer(this);

            GridPosition = _gridSystem.GetGridPosition(this);
        }

        /// <summary>
        /// Swaps chips between two cells.
        /// </summary>
        public void SwapChips(
                CellComponent other,
                ChipComponent.MovingType type = ChipComponent.MovingType.Immediate
            )
        {
            _chipContainer.SwapChips(other._chipContainer, type);
        }

        /// <summary>
        /// Swaps chips between two cells without calling any callbacks. Should be used for temporary calculations only.
        /// </summary>
        public void SwapChipsSilently(CellComponent other)
        {
            _chipContainer.SwapChipsSilently(other._chipContainer);
        }

        public virtual bool CanInsertChip()
        {
            return _chipContainer.CanInsertChip();
        }

        public virtual void InsertChip(ChipComponent chip)
        {
            _chipContainer.PushChip(chip);
        }

        public ChipComponent GetTopChip()
        {
            return _chipContainer.GetTopChip();
        }

        public bool Contains([NotNull] ChipComponent chip)
        {
            ChipComponent innerChip = Chip;
            while (innerChip != null)
            {
                if (innerChip == chip)
                {
                    return true;
                }

                innerChip = innerChip.Child;
            }

            return false;
        }

        public void ForceReplaceChip([NotNull] ChipComponent chip, ImpactFlag impactType)
        {
            if (Chip != null)
            {
                Chip.AddImpactFlags(impactType);
            }

            ReplaceChip(chip, impactType);
        }

        public virtual void ReplaceChip([NotNull] ChipComponent chip, ImpactFlag impactType)
        {
            chip.HandleChipReplacing(impactType);

            if (Chip != null)
            {
                Chip.Impacted += c => FinishReplacement(chip);
                Chip
                    .ScheduleConsumption(impactType)
                    .Perform();
            }
            else
            {
                FinishReplacement(chip);
            }
        }

        public void ChangeChip([NotNull] ChipComponent chip, ChipComponent.MovingType type)
        {
            _chipContainer.ChangeChip(chip, type);
        }

        /// <summary>
        /// Schedules single impact on the chip and underlay health of this cell. Within the game logic this method
        /// should be only used by the <see cref="ImpactSystem"/>.
        /// Underlay takes 1 point of impact only when chip has not absorbed the impact.
        /// </summary>
        public Impact ScheduleSingleImpact(int value, ImpactFlag flag, int id)
        {
            var impact = _instantiator.Instantiate<Impact>(this, value, flag, id);
            _scheduledImpacts.Add(impact);

            ImpactScheduled?.Invoke(this, impact);

            return impact;
        }

        /// <summary>
        /// Calculate impact value which striking with will clear entire cell
        /// </summary>
        public int CalculateClearingImpactValue()
        {
            int impactValue = 0;
            ChipComponent chip = Chip;
            while (chip != null)
            {
                impactValue += chip.Health;
                chip = chip.Child;
            }

            if (Underlay != null)
            {
                impactValue += Underlay.Health;
            }

            return impactValue;
        }

        /// <summary>
        /// Adds delegate that will be invoked when any chip stops its movement in this cell.
        /// Note that chip is not finally stopped and may resume movement later (<see cref="ChipComponent.IsMoving"/> is true,
        /// <see cref="ChipComponent.IsInMovingState"/> is false).
        /// </summary>
        public void AddMovementStopHandler(MovementHandlerType type, MovementHandler handler)
        {
            _movementStopHandlers.Add(type, handler);
        }

        /// <summary>
        /// Adds delegate that will be invoked when any chip finally stops its movement in this cell.
        /// (both <see cref="ChipComponent.IsMoving"/> and <see cref="ChipComponent.IsInMovingState"/> is false).
        /// </summary>
        public void AddMovementFinishHandler(MovementHandlerType type, MovementHandler handler)
        {
            _movementFinishHandlers.Add(type, handler);
        }

        public void OnChipMovingStopped(ChipComponent.MovingType type)
        {
            _movementStopHandlers.InvokeAll(this, type);
        }

        public void OnChipMovingFinished(ChipComponent.MovingType type)
        {
            _movementFinishHandlers.InvokeAll(this, type);
        }

        /// <summary>
        /// Begins process of the specified type for this cell. Cell is considered
        /// unstable (see <see cref="IsStable"/>) until you call <see cref="EndProcess"/> with the same process type.
        /// </summary>
        public void BeginProcess(ProcessType processType)
        {
            _activeProcesses.Add(processType);

            if (_activeProcesses.Count == 1)
            {
                Destabilized?.Invoke();
            }
        }

        /// <summary>
        /// Ends process of the specified type for this cell. If this was the last process for this cell, cell
        /// becomes stable (see <see cref="IsStable"/>).
        /// </summary>
        public void EndProcess(ProcessType processType)
        {
            if (!_activeProcesses.Remove(processType))
            {
                UnityEngine.Debug.LogError($"Trying to end process {processType} that has not been started.", this);
                return;
            }

            if (_activeProcesses.Count == 0)
            {
                Stabilized?.Invoke();
            }
        }

        /// <summary>
        /// Iterate over cells around this one.
        /// </summary>
        public IEnumerable<CellComponent> IterateCellsAround()
        {
            var dir = Direction.Up;
            do
            {
                Vector2Int offset = dir.GetOffset();
                CellComponent cellAround = _gridSystem.GetCell(GridPosition + offset);
                if (cellAround != null)
                {
                    yield return cellAround;
                }

                dir = dir.Rotate90CW();
            }
            while (dir != Direction.Up);
        }

        protected void Awake()
        {
            HasPredefinedChip = Chip != null;

            _chipContainer.ChipChanged += OnChipChanged;
            _chipContainer.ChipChangedSilently += OnChipChangedSilently;
        }

        private void OnChipChanged(ChipComponent oldChip, ChipComponent newChip)
        {
            HasPredefinedChip = false;

            ChipChanged?.Invoke(this, oldChip);
        }

        private void OnChipChangedSilently(ChipComponent oldChip, ChipComponent newChip)
        {
            ChipChangedSilently?.Invoke(this, oldChip);
        }

        private void FinishReplacement(ChipComponent chip)
        {
            Chip = chip;
        }

        void Impact.IProvider.BeginImpact(Impact impact)
        {
            Assert.IsTrue(_scheduledImpacts.Contains(impact));

            BeginProcess(ProcessType.ImpactBegin);
        }

        void Impact.IProvider.PerformImpact(Impact impact)
        {
            Assert.IsTrue(_scheduledImpacts.Contains(impact));

            _scheduledImpacts.Remove(impact);

            int delta = impact.Value;
            ImpactFlag flag = impact.Flag;

            ChipComponent chipBeforeImpact = Chip;

            bool isUnderlayReceivesImpact = chipBeforeImpact == null;
            if (chipBeforeImpact != null)
            {
                bool chipCanAbsorbImpact = chipBeforeImpact.OnCellImpacting(delta, flag);

                if (chipBeforeImpact != null && chipBeforeImpact.Child != null)
                {
                    isUnderlayReceivesImpact ^= !chipBeforeImpact.Child.IsImpactAbsorptionPossible(flag);
                }
                else
                {
                    isUnderlayReceivesImpact ^= !chipCanAbsorbImpact;
                }
            }

            if (Underlay != null && isUnderlayReceivesImpact)
            {
                Underlay.OnCellImpacting(delta, flag, chipBeforeImpact);
            }

            Impacted?.Invoke(this, chipBeforeImpact, flag, impact.GroupId);

            EndProcess(ProcessType.ImpactBegin);
        }

        void Impact.IProvider.CancelImpact(Impact impact, bool hasImpactBegan)
        {
            Assert.IsTrue(_scheduledImpacts.Contains(impact));

            _scheduledImpacts.Remove(impact);
            ImpactCanceled?.Invoke(this, impact.Flag, impact.GroupId);

            if (hasImpactBegan)
            {
                EndProcess(ProcessType.ImpactBegin);
            }
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            const string SingleIndent = "\t";
            const string DoubleIndent = SingleIndent + SingleIndent;

            if (_scheduledImpacts.Count > 0)
            {
                var builder = new StringBuilder($"({_scheduledImpacts.Count} scheduled impacts):");
                for (int i = 0; i < _scheduledImpacts.Count; ++i)
                {
                    Impact impact = _scheduledImpacts[i];
                    builder.AppendLine($"\n{SingleIndent}Impact_{i + 1} creation stack trace:");
                    builder.AppendLine($"{DoubleIndent}{impact.StackTraceStringOnConstruct.Replace("\n", $"\n{DoubleIndent}")}");
                }

                return builder.ToString();
            }

            return string.Empty;
        }

        #region Inner types

        public enum MovementHandlerType
        {
            ConveyorBlock,
            Matching,
            ChipWayout,
            Falling,
            Swapping
        }

        public enum ProcessType
        {
            Swapping,
            ImpactBegin,
            Impacting,
            HiddenObjectRelease
        }

        public delegate void ChipChangedDelegate([NotNull] CellComponent cell, ChipComponent prevChip);

        public delegate void ChipChangedSilentlyDelegate([NotNull] CellComponent cell, ChipComponent prevChip);

        public delegate void ImpactedDelegate(
            [NotNull] CellComponent cell,
            ChipComponent cellChipBeforeImpact,
            ImpactFlag impactType,
            int impactGroupId);

        public delegate void ImpactCanceledDelegate(
            [NotNull] CellComponent cell,
            ImpactFlag impactType,
            int impactGroupId);

        public delegate void ImpactScheduledDelegate([NotNull] CellComponent cell, [NotNull] Impact impact);

        #endregion
    }
}