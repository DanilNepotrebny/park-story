﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Match3
{
    public partial class HealthController
    {
        [SerializeField] private int _maxEditorHealth = 1;

        private static Func<HealthController, EditorApi> _editorApiFactory;

        /// <summary>
        /// Editor API for the HealthController component. Should NOT be used in game logics code.
        /// </summary>
        [InitializeOnLoad]
        public class EditorApi
        {
            private readonly WeakReference<HealthController> _hostRef;

            public HealthController Host
            {
                get
                {
                    HealthController target;
                    return _hostRef.TryGetTarget(out target) ? target : null;
                }
            }

            /// <summary>
            /// Max Health of the chip
            /// </summary>
            public int MaxHealth
            {
                get { return Host._maxHealth; }
                set
                {
                    if (Host._maxHealth == value)
                    {
                        return;
                    }

                    EditorUtility.SetDirty(Host);
                    Host._maxHealth = value > Host._maxEditorHealth ? 1 : Mathf.Clamp(value, 1, Host._maxEditorHealth);
                }
            }

            static EditorApi()
            {
                _editorApiFactory = host => new EditorApi(host);
            }

            private EditorApi(HealthController host)
            {
                _hostRef = new WeakReference<HealthController>(host, false);
            }
        }

        /// <summary>
        /// Returns the grid editor API object. Should NOT be used in game logics code.
        /// </summary>
        public EditorApi GetEditorApi()
        {
            return _editorApiFactory(this);
        }
    }
}

#endif // UNITY_EDITOR