﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Consumes chip on double tap.
    /// </summary>
    [RequireComponent(typeof(ChipPresenter))]
    public class ChipActivator : MonoBehaviour
    {
        [Inject] private MovesSystem _movesSystem;

        public ChipPresenter ChipPresenter { get; private set; }

        public event Action<Vector2Int> ChipActivating;
        public event Action<Vector2Int> ChipActivated;

        public void HandleChipActivation()
        {
            ChipComponent chip = ChipPresenter.ChipComponent;
            CellComponent cell = chip.Cell;

            if (cell.IsStable && !chip.IsMoving)
            {
                ChipActivating?.Invoke(cell.GridPosition);
                ScheduleActivation().Perform();
                ChipActivated?.Invoke(cell.GridPosition);
            }
        }

        public Impact ScheduleActivation()
        {
            ChipComponent chip = ChipPresenter.ChipComponent;
            return chip.ScheduleActivation();
        }

        protected void Awake()
        {
            ChipPresenter = GetComponent<ChipPresenter>();
            ChipPresenter.DoubleTapped += OnDoubleTapped;
            ChipPresenter.ChipComponent.ContainerCellChanged += (cell, oldCell) => UpdateMovesRegistration();
            UpdateMovesRegistration();
        }

        protected void OnDestroy()
        {
            _movesSystem.UnregisterChipActivator(this);
        }

        private void UpdateMovesRegistration()
        {
            if (ChipPresenter.ChipComponent.ContainerCell != null)
            {
                _movesSystem.RegisterChipActivator(this);
            }
            else
            {
                _movesSystem.UnregisterChipActivator(this);
            }
        }

        private void OnDoubleTapped(ChipComponent chip)
        {
            Assert.IsTrue(chip == ChipPresenter.ChipComponent);

            HandleChipActivation();
        }
    }
}