﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3
{
    public static class Explosion
    {
        public static void Explode(
                GridSystem grid,
                ImpactSystem impactSystem,
                Vector2Int epicenter,
                float radius,
                int value,
                float delay,
                float immediateRadius = 0f
            )
        {
            ScheduleExplodeInternal(
                    grid,
                    impactSystem,
                    epicenter,
                    radius,
                    value,
                    datas =>
                    {
                        foreach (ExplosionData data in datas)
                        {
                            float time = CalculateExplodeTime(delay, radius, data.DistanceToEpicenter, immediateRadius);
                            data.Cell.StartCoroutine(ExplodeInTime(time, data.Impact));
                        }
                    }
                );
        }

        public static List<Impact> ScheduleExplode(
                GridSystem grid,
                ImpactSystem impactSystem,
                Vector2Int epicenter,
                float radius,
                int value
            )
        {
            var scheduledImpact = new List<Impact>();
            ScheduleExplodeInternal(
                grid,
                impactSystem,
                epicenter,
                radius,
                value,
                datas => datas.ForEach(d => scheduledImpact.Add(d.Impact)));
            return scheduledImpact;
        }

        private static void ScheduleExplodeInternal(
                GridSystem grid,
                ImpactSystem impactSystem,
                Vector2Int epicenter,
                float radius,
                int value,
                OnExplosionImpactScheduled onScheduled = null
            )
        {
            var impactingCellsInRange = new List<ExplosionData>();

            grid.ProcessCellsInRadius(
                    (cell, distance) => impactingCellsInRange.Add(
                        new ExplosionData
                        {
                            Cell = cell,
                            DistanceToEpicenter = distance
                        }),
                    epicenter,
                    radius
                );

            if (impactingCellsInRange.Count == 0)
            {
                return;
            }

            var impactingCells = new List<CellComponent>(impactingCellsInRange.Count);
            foreach (ExplosionData explosionData in impactingCellsInRange)
            {
                impactingCells.Add(explosionData.Cell);
            }

            List<Impact> impacts = impactSystem.ScheduleImpact(impactingCells, value, ImpactFlag.Explosion);
            Assert.IsTrue(impactingCellsInRange.Count == impacts.Count);
            for (int i = 0; i < impacts.Count; ++i)
            {
                impactingCellsInRange[i].Impact = impacts[i];
            }

            onScheduled?.Invoke(impactingCellsInRange);
        }

        private static IEnumerator ExplodeInTime(float time, Impact impact)
        {
            impact.Begin();
            yield return new WaitForSeconds(time);
            impact.Perform();
        }

        private static float CalculateExplodeTime(float delay, float radius, float distance, float immediateRadius)
        {
            float time = Mathf.Lerp(0f, delay * Mathf.FloorToInt(radius), (distance - immediateRadius) / radius);
            return time;
        }

        #region Inner types

        private delegate void OnExplosionImpactScheduled(List<ExplosionData> explosionDatas);

        private class ExplosionData
        {
            public CellComponent Cell { get; set; }
            public Impact Impact { get; set; }
            public float DistanceToEpicenter { get; set; }
        }

        #endregion Inner types
    }
}