﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Stores data of scheduled impact and provides management of its lifecycle.
    /// </summary>
    public class Impact : Impact.IData
    {
        private readonly IProvider _provider;

        public int Value { get; }
        public ImpactFlag Flag { get; }
        public int GroupId { get; }
        public CellComponent Cell { get; }
        public ExecutionState State { get; private set; } = ExecutionState.Undefined;
        public string StackTraceStringOnConstruct { get; } // might be useful to check unfinished impact

        public event Action<Impact> Begging;
        public event Action<Impact> Performing;
        public event Action<Impact> Cancelling;

        public Impact(CellComponent cell, int value, ImpactFlag flag, int id)
        {
            StackTraceStringOnConstruct = StackTraceUtility.ExtractStackTrace();

            Cell = cell;
            Value = value;
            Flag = flag;
            GroupId = id;

            _provider = cell;
        }

        /// <summary>
        /// Begins this impact. <see cref="CellComponent.IsImpactActive"/> will be true right after this call,
        /// but the actual impact happens after <see cref="Perform"/> is called.
        /// </summary>
        public void Begin()
        {
            Assert.IsTrue(State != ExecutionState.Began);

            State = ExecutionState.Began;
            Begging?.Invoke(this);

            _provider.BeginImpact(this);
        }

        /// <summary>
        /// Performs this impact. All impact effect will be applied after this call
        /// (i.e. <see cref="ChipComponent.Health"/> will be lowered). If <see cref="Begin"/> has not been
        /// called it will be called within this method.
        /// </summary>
        public void Perform()
        {
            if (State == ExecutionState.Cancelled)
            {
                throw new InvalidOperationException("Called Perform() right after Cancel()");
            }

            if (State != ExecutionState.Began)
            {
                Begin();
            }

            State = ExecutionState.Performed;
            Performing?.Invoke(this);

            _provider.PerformImpact(this);
        }

        /// <summary>
        /// Cancels this impact. You can call <see cref="Cancel"/> any time before <see cref="Perform"/> is called.
        /// </summary>
        public void Cancel()
        {
            if (State == ExecutionState.Cancelled)
            {
                UnityEngine.Debug.LogWarning($"Cancelling already cancelled impact. Skipping..");
                return;
            }

            bool hasImpactBegan = State != ExecutionState.Undefined;

            State = ExecutionState.Cancelled;
            Cancelling?.Invoke(this);

            _provider.CancelImpact(this, hasImpactBegan);
        }

        #region Inner types

        /// <summary>
        /// Provider for impact operations. <see cref="CellComponent"/> should implement this.
        /// </summary>
        public interface IProvider
        {
            void BeginImpact(Impact impact);
            void PerformImpact(Impact impact);
            void CancelImpact(Impact impact, bool hasImpactBegan);
        }

        /// <summary>
        /// Interface for accessing common data of scheduled impact.
        /// </summary>
        public interface IData
        {
            int Value { get; }
            ImpactFlag Flag { get; }
            int GroupId { get; }
        }

        public enum ExecutionState
        {
            Undefined,
            Began,
            Performed,
            Cancelled
        }

        #endregion Inner types
    }
}