﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Match3.Conveyor
{
    public partial class ConveyorBlock : ConveyorBlock.IEditorApi
    {
        bool IEditorApi.IsSelected { get; set; }

        ConveyorBlock IEditorApi.PreviousBlock
        {
            get
            {
                ConveyorBlock previousBlock = this;
                while (previousBlock.NextBlock != this)
                {
                    previousBlock = previousBlock.NextBlock;
                }

                return previousBlock;
            }
        }

        ConveyorBlock IEditorApi.NextBlock
        {
            get { return NextBlock; }
            set
            {
                Undo.RecordObject(this, "Set NextBlock");
                NextBlock = value;
            }
        }

        Direction IEditorApi.TransportationDirection
        {
            get { return TransportationDirection; }
            set
            {
                Undo.RecordObject(this, "Set NextBlock");
                TransportationDirection = value;
            }
        }

        bool IEditorApi.HasChipSlot
        {
            get { return _hasChipSlot; }

            set
            {
                if (_hasChipSlot != value)
                {
                    Undo.RecordObject(this, "Set hasChipSlot");
                    _hasChipSlot = value;
                    _hasChipSlotChanged?.Invoke();
                }
            }
        }

        private event Action _hasChipSlotChanged;

        CellComponent IEditorApi.Cell => GetComponentInParent<CellComponent>();

        event Action IEditorApi.HasChipSlotChanged
        {
            add { _hasChipSlotChanged += value; }
            remove { _hasChipSlotChanged -= value; }
        }

        protected void OnDrawGizmos()
        {
            GridSystem gridSystem = GetComponentInParent<GridSystem>();

            Gizmos.color = Color.red;

            if (NextBlock != null)
            {
                float distance = (NextBlock.transform.position - transform.position).magnitude;
                float maxDistance = 1.5f * Mathf.Max(gridSystem.CellWidth, gridSystem.CellHeight);

                if (distance <= maxDistance)
                {
                    EditorUtils.DrawArrowGizmo(
                            transform.position,
                            NextBlock.transform.position - transform.position
                        );
                }
                else
                {
                    Vector3 arrowDirection = NextBlock.transform.position - transform.position;
                    Vector3 arrowNormal = new Vector3(-arrowDirection.y, arrowDirection.x).normalized;

                    Vector3 offset = arrowNormal * 0.5f;

                    Vector3 arrowStart = transform.position + offset;
                    Vector3 arrowFinish = NextBlock.transform.position + offset;

                    Gizmos.DrawLine(transform.position, transform.position + offset);
                    EditorUtils.DrawArrowGizmo(arrowStart, arrowDirection);
                    Gizmos.DrawLine(arrowFinish, NextBlock.transform.position);
                }
            }

            Rect cellRect = new Rect(
                    transform.position.x - gridSystem.CellWidth / 2,
                    transform.position.y - gridSystem.CellHeight / 2,
                    gridSystem.CellWidth,
                    gridSystem.CellHeight
                );

            if (((IEditorApi)this).IsSelected)
            {
                Handles.DrawSolidRectangleWithOutline(cellRect, new Color(0, 1, 0, 0.1f), Color.green);
            }

            if (IsTransportationActive)
            {
                Gizmos.DrawIcon(transform.position, "ConveyorTransportation.png");
            }
        }

        #region Inner types

        public interface IEditorApi
        {
            ConveyorBlock PreviousBlock { get; }
            ConveyorBlock NextBlock { get; set; }
            Direction TransportationDirection { get; set; }

            CellComponent Cell { get; }

            bool IsSelected { get; set; }
            bool HasChipSlot { get; set; }

            event Action HasChipSlotChanged;
        }

        #endregion
    }
}

#endif