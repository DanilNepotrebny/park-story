﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.Teleportation;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.Conveyor
{
    public partial class ConveyorBlock : BaseTeleport
    {
        [SerializeField] private ConveyorBlock _nextBlock;
        [SerializeField] private ChipComponent _dummyChip;
        [SerializeField] private bool _hasChipSlot = true;
        [SerializeField] private Direction _transportationDirection;

        [Inject] private ConveyorSystem _conveyorSystem;
        [Inject] private ChipContainerComponent _chipContainer;
        [Inject] private GenerationSystem _generationSystem;
        [Inject] private GridSystem _gridSystem;

        private ChipComponent _temporaryDummyChip;

        [Inject]
        public CellComponent Cell { get; }

        public ConveyorBlock NextBlock
        {
            get { return _nextBlock; }
            private set { _nextBlock = value; }
        }

        public Direction TransportationDirection
        {
            get { return _transportationDirection; }
            private set { _transportationDirection = value; }
        }

        public override CellComponent TargetCell => NextBlock.Cell;
        public override CellComponent SourceCell => null;
        public override Direction DirectionOut => NextBlock.TransportationDirection;
        public override Direction DirectionIn => TransportationDirection;

        public ChipContainerComponent ChipContainer => _chipContainer;
        public bool IsTransportationActive { get; private set; }

        public ChipComponent.MovingType MovingType => _isTeleport ?
            ChipComponent.MovingType.Teleportation :
            ChipComponent.MovingType.ConveyorTransportation;

        private bool _isTeleport =>
            !HasDirectTransition(this, NextBlock, _gridSystem) ||
            GetDirectionTo(this, NextBlock, _gridSystem) != TransportationDirection;

        public event Action TransportationBegun;
        public event Action TransportationEnded;

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<CellComponent>().FromComponentInParents(true, true);
            yield return container.Bind<ChipContainerComponent>().FromComponentInParents(true, true);
        }

        public void BeginTransportation()
        {
            IsTransportationActive = true;

            // Create dummy chip to prevent empty cells being occupied during conveyor movement
            if (Cell.IsEmpty)
            {
                _temporaryDummyChip = CreateDummyChip();
                _temporaryDummyChip.Moved += OnTemporaryChipMoved;
            }

            TransportationBegun?.Invoke();
        }

        public static bool HasDirectTransition(ConveyorBlock blockFrom, ConveyorBlock blockTo, GridSystem gridSystem)
        {
            return Mathf.Approximately(GetVectorTo(blockFrom, blockTo, gridSystem).magnitude, 1);
        }

        public static Vector2 GetVectorTo(ConveyorBlock blockFrom, ConveyorBlock blockTo, GridSystem gridSystem)
        {
            Vector2 fromPos = gridSystem.WorldToGridF(blockFrom.transform.position);
            Vector2 toPos = gridSystem.WorldToGridF(blockTo.transform.position);
            return toPos - fromPos;
        }

        public static Direction GetDirectionTo(ConveyorBlock blockFrom, ConveyorBlock blockTo, GridSystem gridSystem)
        {
            Vector2Int dir = Vector2Int.CeilToInt(GetVectorTo(blockFrom, blockTo, gridSystem).normalized);
            return dir.GetDirection();
        }

        protected void Awake()
        {
            if (!_hasChipSlot)
            {
                CreateDummyChip();
            }

            _conveyorSystem.RegisterConveyorBlock(this);
            Cell.AddMovementFinishHandler(CellComponent.MovementHandlerType.ConveyorBlock, OnCellMovementFinish);
        }

        private ChipComponent CreateDummyChip()
        {
            ChipComponent chip = _generationSystem.InstantiateChip(_dummyChip);
            Cell.InsertChip(chip);
            return chip;
        }

        private void OnTemporaryChipMoved(ChipComponent.MovingType movingType)
        {
            if (movingType == MovingType &&
                _temporaryDummyChip != null)
            {
                _temporaryDummyChip.Moved -= OnTemporaryChipMoved;
                _temporaryDummyChip.DestroyChip();
                _temporaryDummyChip = null;
            }
        }

        private void OnCellMovementFinish(CellComponent cell, ChipComponent.MovingType movingType)
        {
            if (movingType == ChipComponent.MovingType.ConveyorTransportation ||
                movingType == ChipComponent.MovingType.Teleportation)
            {
                Assert.IsTrue(IsTransportationActive);

                IsTransportationActive = false;

                TransportationEnded?.Invoke();
            }
        }
    }
}