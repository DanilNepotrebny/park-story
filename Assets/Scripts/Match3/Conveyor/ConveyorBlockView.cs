﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3.Teleportation;
using DreamTeam.Utils;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace DreamTeam.Match3.Conveyor
{
    public class ConveyorBlockView : EditorBehaviour
    {
        [SerializeField] private DirectionViews _directionViews;
        [SerializeField] private TeleportViews _teleportViewsIn;
        [SerializeField] private TeleportViews _teleportViewsOut;
        [SerializeField] private GameObject _chipSlotPrefab;

        #if UNITY_EDITOR
        private ConveyorBlock.IEditorApi _conveyorBlockApi;

        protected void OnEnable()
        {
            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                return;
            }

            _conveyorBlockApi = GetComponentInParent<ConveyorBlock.IEditorApi>();
            _conveyorBlockApi.HasChipSlotChanged += UpdateChipSlotView;
        }

        protected void OnDisable()
        {
            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                return;
            }

            if (_conveyorBlockApi != null)
            {
                _conveyorBlockApi.HasChipSlotChanged -= UpdateChipSlotView;
            }
        }

        public void RecreateView(GridSystem grid)
        {
            while (transform.childCount > 0)
            {
                transform.GetChild(0).gameObject.DisposeImmediate();
            }

            DirectionType type = GetDirectionType(grid);
            CreateView(_directionViews[type]);

            ConveyorBlock prev = _conveyorBlockApi.PreviousBlock;
            ConveyorBlock current = (ConveyorBlock)_conveyorBlockApi;
            ConveyorBlock next = _conveyorBlockApi.NextBlock;

            TryCreateTeleportView(prev, current, grid, true);
            TryCreateTeleportView(current, next, grid, false);

            UpdateChipSlotView();
        }

        private void UpdateChipSlotView()
        {
            GameObject currentChipSlot = null;
            foreach (Transform t in transform)
            {
                if (PrefabUtility.GetCorrespondingObjectFromSource(t.gameObject) == _chipSlotPrefab)
                {
                    currentChipSlot = t.gameObject;
                    break;
                }
            }

            if (_conveyorBlockApi.HasChipSlot && currentChipSlot == null)
            {
                CreateView(_chipSlotPrefab);
                return;
            }

            if (!_conveyorBlockApi.HasChipSlot && currentChipSlot != null)
            {
                Undo.DestroyObjectImmediate(currentChipSlot);
            }
        }

        private void TryCreateTeleportView(ConveyorBlock from, ConveyorBlock to, GridSystem grid, bool isTeleportOut)
        {
            if (!ConveyorBlock.HasDirectTransition(from, to, grid) ||
                from.TransportationDirection != ConveyorBlock.GetDirectionTo(from, to, grid))
            {
                TeleportViews teleportViews = isTeleportOut ? _teleportViewsOut : _teleportViewsIn;
                GameObject teleportObject = CreateView(teleportViews[_conveyorBlockApi.TransportationDirection]);
                TeleportPresenter.IEditorApi presenter = teleportObject.GetComponent<TeleportPresenter.IEditorApi>();
                presenter.Teleport = from;
            }
        }

        private GameObject CreateView(GameObject prefab)
        {
            GameObject view = (GameObject)PrefabUtility.InstantiatePrefab(prefab);
            Undo.SetTransformParent(view.transform, transform, "Set view parent");

            Undo.RecordObject(view.transform, "Created view");
            view.transform.localPosition = Vector3.zero;

            Undo.RegisterCreatedObjectUndo(view, "Created view object");
            return view;
        }

        private DirectionType GetDirectionType(GridSystem grid)
        {
            Direction prev = _conveyorBlockApi.PreviousBlock.TransportationDirection;
            Direction current = _conveyorBlockApi.TransportationDirection;

            if (!ConveyorBlock.HasDirectTransition(
                    _conveyorBlockApi.PreviousBlock,
                    (ConveyorBlock)_conveyorBlockApi,
                    grid) ||
                prev == current)
            {
                switch (current)
                {
                    case Direction.Up:
                        return DirectionType.Up;

                    case Direction.Down:
                        return DirectionType.Down;

                    case Direction.Left:
                        return DirectionType.Left;

                    case Direction.Right:
                        return DirectionType.Right;
                }
            }

            switch (prev)
            {
                case Direction.Up:
                    switch (current)
                    {
                        case Direction.Left:
                            return DirectionType.UpLeft;

                        case Direction.Right:
                            return DirectionType.UpRight;
                    }

                    break;

                case Direction.Down:
                    switch (current)
                    {
                        case Direction.Left:
                            return DirectionType.DownLeft;

                        case Direction.Right:
                            return DirectionType.DownRight;
                    }

                    break;

                case Direction.Left:
                    switch (current)
                    {
                        case Direction.Up:
                            return DirectionType.LeftUp;

                        case Direction.Down:
                            return DirectionType.LeftDown;
                    }

                    break;

                case Direction.Right:
                    switch (current)
                    {
                        case Direction.Up:
                            return DirectionType.RightUp;

                        case Direction.Down:
                            return DirectionType.RightDown;
                    }

                    break;
            }

            return DirectionType.Right;
        }
        #endif

        #region Inner Types

        private enum DirectionType
        {
            Left,
            Right,
            Up,
            Down,
            LeftDown,
            DownLeft,
            RightDown,
            DownRight,
            UpRight,
            RightUp,
            LeftUp,
            UpLeft
        }

        [Serializable, ContainsAllEnumKeys]
        private class DirectionViews : KeyValueList<DirectionType, GameObject>
        {
        }

        [Serializable]
        private class TeleportViews : KeyValueList<Direction, GameObject>
        {
        }

        #endregion
    }
}