﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;

namespace DreamTeam.Match3.Conveyor
{
    public class ConveyorBlockChipSlotController : EditorBehaviour
    {
        #if UNITY_EDITOR
        private ConveyorBlock.IEditorApi _conveyorBlockApi;
        private CellComponent.IEditorAPI _cellApi;

        protected void OnEnable()
        {
            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                return;
            }

            _conveyorBlockApi = GetComponent<ConveyorBlock.IEditorApi>();
        }

        protected void OnTransformParentChanged()
        {
            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                return;
            }

            TryPickUpCell();
        }

        private void TryPickUpCell()
        {
            if (_cellApi == null)
            {
                _cellApi = _conveyorBlockApi.Cell;
                if (_cellApi != null)
                {
                    _cellApi.ChipChanged += OnChipChanged;

                    CheckConveyorBlock();
                }
            }
        }

        protected void OnDestroy()
        {
            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                return;
            }

            if (_cellApi != null)
            {
                _cellApi.ChipChanged -= OnChipChanged;
            }
        }

        private void OnChipChanged(ChipComponent oldChip, ChipComponent newChip)
        {
            CheckConveyorBlock();
        }

        private void CheckConveyorBlock()
        {
            if (_cellApi.Chip != null)
            {
                _conveyorBlockApi.HasChipSlot = true;
            }
        }
        #endif
    }
}