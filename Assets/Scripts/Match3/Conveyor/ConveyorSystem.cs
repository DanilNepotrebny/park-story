﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.GameControls;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Match3.Conveyor
{
    public class ConveyorSystem : MonoBehaviour
    {
        [SerializeField, FormerlySerializedAs("_mainSelectionMode")]
        private GameControlsMode _mainControlsMode;

        [SerializeField, FormerlySerializedAs("_transportationSelectionMode")]
        private GameControlsMode _transportationControlsMode;

        [Inject] private MovesSystem _movesSystem;
        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;

        private ActionDisposable _mainModeDisableHandle;
        private ActionDisposable _transportationModeDisableHandle;

        private List<ConveyorLine> _conveyorLines = new List<ConveyorLine>();

        private bool _isTransportationActive
        {
            get
            {
                foreach (ConveyorLine line in _conveyorLines)
                {
                    foreach (ConveyorBlock block in line.Blocks)
                    {
                        if (block.IsTransportationActive)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        public void RegisterConveyorBlock(ConveyorBlock conveyorBlock)
        {
            foreach (ConveyorLine l in _conveyorLines)
            {
                if (l.Blocks.Contains(conveyorBlock))
                {
                    return;
                }
            }

            ConveyorLine line = new ConveyorLine();
            _conveyorLines.Add(line);

            AddBlockToLine(conveyorBlock, line);

            ConveyorBlock currentBlock = conveyorBlock.NextBlock;
            while (currentBlock != conveyorBlock)
            {
                if (line.Blocks.Contains(currentBlock))
                {
                    throw new InvalidOperationException(
                        $"Invalid conveyor configuration at cell {currentBlock.Cell.GridPosition}");
                }

                AddBlockToLine(currentBlock, line);
                currentBlock = currentBlock.NextBlock;
            }
        }

        public bool HasAnyBlock([NotNull] CellComponent cell)
        {
            return _conveyorLines.Any(line => line.Blocks.Any(block => block.Cell == cell));
        }

        protected void Start()
        {
            if (_conveyorLines.Count != 0)
            {
                _movesSystem.MoveSucceeding += OnMoveSucceeding;
            }
        }

        protected void OnDestroy()
        {
            ReleaseMainModeDisposeHandle();
            ReleaseTransportationModeDisposeHandle();
        }

        private void AddBlockToLine(ConveyorBlock conveyorBlock, ConveyorLine line)
        {
            line.Add(conveyorBlock);
            conveyorBlock.TransportationEnded += OnBlockTransportationEnded;
        }

        private void OnMoveSucceeding(List<Vector2Int> cellPositions)
        {
            Assert.IsNull(_mainModeDisableHandle, "_mainModeDisableHandle != null");
            _mainModeDisableHandle = _gameControlsSystem.EnableMode(_mainControlsMode);

            _fieldStabilizationSystem.AddStabilizationHandler(
                    OnFieldStabilized,
                    FieldStabilizationHandlerOrder.Conveyor
                );
        }

        private void OnFieldStabilized()
        {
            Assert.IsNull(_transportationModeDisableHandle, "_transportationModeDisableHandle != null");
            _transportationModeDisableHandle = _gameControlsSystem.EnableMode(_transportationControlsMode);
            _fieldStabilizationSystem.RemoveStabilizationHandler(OnFieldStabilized);

            foreach (ConveyorLine line in _conveyorLines)
            {
                foreach (ConveyorBlock block in line.Blocks)
                {
                    block.BeginTransportation();
                }

                ChipContainerComponent.RotateChips(line.ChipContainers, line.MovingTypes);
            }

            if (!_isTransportationActive)
            {
                EndTransportation();
            }
        }

        private void EndTransportation()
        {
            ReleaseMainModeDisposeHandle();
            ReleaseTransportationModeDisposeHandle();
        }

        private void OnBlockTransportationEnded()
        {
            if (!_isTransportationActive)
            {
                EndTransportation();
            }
        }

        private void ReleaseMainModeDisposeHandle()
        {
            _mainModeDisableHandle?.Dispose();
            _mainModeDisableHandle = null;
        }

        private void ReleaseTransportationModeDisposeHandle()
        {
            _transportationModeDisableHandle?.Dispose();
            _transportationModeDisableHandle = null;
        }

        #region Inner types

        private class ConveyorLine
        {
            public List<ConveyorBlock> Blocks = new List<ConveyorBlock>();
            public List<ChipComponent.MovingType> MovingTypes = new List<ChipComponent.MovingType>();
            public List<ChipContainerComponent> ChipContainers = new List<ChipContainerComponent>();

            public void Add(ConveyorBlock conveyorBlock)
            {
                Blocks.Add(conveyorBlock);
                ChipContainers.Add(conveyorBlock.ChipContainer);
                MovingTypes.Add(conveyorBlock.MovingType);
            }
        }

        #endregion
    }
}