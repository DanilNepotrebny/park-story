﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.Conveyor
{
    public class ConveyorBlockChipSlot : MonoBehaviour
    {
        private ConveyorBlock _block;

        private ConveyorBlock _conveyorBlock
        {
            get { return _block; }
            set
            {
                if (_block != null)
                {
                    _block.TransportationBegun -= OnTransportationBegun;
                }

                _block = value;

                if (_block != null)
                {
                    _block.TransportationBegun += OnTransportationBegun;
                }
            }
        }

        protected void Start()
        {
            _conveyorBlock = GetComponentInParent<ConveyorBlock>();
        }

        private void OnTransportationBegun()
        {
            transform.SetParentPreserveWorldScale(_conveyorBlock.Cell.Chip.transform);

            _conveyorBlock.NextBlock.TransportationEnded += OnTransportationEnded;
        }

        private void OnTransportationEnded()
        {
            _conveyorBlock = _conveyorBlock.NextBlock;
            _conveyorBlock.TransportationEnded -= OnTransportationEnded;

            transform.SetParent(_conveyorBlock.Cell.transform, true);
        }
    }
}