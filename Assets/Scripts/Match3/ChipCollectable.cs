﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Consumes chip on double tap.
    /// </summary>
    [RequireComponent(typeof(ChipComponent))]
    public class ChipCollectable : MonoBehaviour
    {
        private ChipComponent _chip;

        protected void Awake()
        {
            _chip = GetComponent<ChipComponent>();
            _chip.ContainerCellChanged += OnContainerCellChanged;
        }

        protected void OnDestroy()
        {
            _chip.ContainerCellChanged -= OnContainerCellChanged;
        }

        private void OnContainerCellChanged(CellComponent newCell, CellComponent oldCell)
        {
            if (newCell != null)
            {
                _chip.ScheduleActivation().Perform();
            }
        }
    }
}