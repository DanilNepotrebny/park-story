﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Linq;
using DreamTeam.Utils;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace DreamTeam.Match3.Teleportation
{
    /// <summary>
    /// Editor only component for selecting teleport view based on the cell gravity.
    /// </summary>
    public class TeleportViewSelector : BaseGravityEditorHelper
    {
        [SerializeField] private TeleportViews _teleportViews;

        #if UNITY_EDITOR

        protected override void UpdateView(Direction gravity)
        {
            while (transform.childCount > 0)
            {
                transform.GetChild(0).gameObject.DisposeImmediate();
            }

            GameObject view = CreateView(_teleportViews[gravity]);
            (view.GetComponentInChildren<TeleportPresenter>() as TeleportPresenter.IEditorApi).Teleport =
                FindTeleport();
        }

        private GameObject CreateView(GameObject prefab)
        {
            var view = (GameObject)PrefabUtility.InstantiatePrefab(prefab);
            Undo.SetTransformParent(view.transform, transform, "Set view parent");

            Undo.RecordObject(view.transform, "Created view");
            view.transform.localPosition = Vector3.zero;

            Undo.RegisterCreatedObjectUndo(view, "Created view object");
            return view;
        }

        private Teleport FindTeleport()
        {
            var teleport = GetComponentInParent<Teleport>();
            if (teleport != null)
            {
                return teleport;
            }

            Teleport[] teleports = FindObjectsOfType<Teleport>();
            teleport = teleports.FirstOrDefault(t => t.TargetCell == CellEditorApi as CellComponent);
            return teleport;
        }

        #endif

        #region Inner Types

        [Serializable]
        private class TeleportViews : KeyValueList<Direction, GameObject>
        {
        }

        #endregion
    }
}