﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.Teleportation
{
    public partial class Teleport : BaseTeleport
    {
        [SerializeField] private CellComponent _targetCell;

        [Inject] private GravitySystem _gravitySystem;

        [Inject]
        public override CellComponent SourceCell { get; }
        public override CellComponent TargetCell => _targetCell;
        public override Direction DirectionOut => TargetCell.Gravity;
        public override Direction DirectionIn => SourceCell.Gravity;

        public ChipComponent.MovingType MovingType => ChipComponent.MovingType.Teleportation;

        protected void Awake()
        {
            _gravitySystem.RegisterTeleport(this);
        }
    }
}