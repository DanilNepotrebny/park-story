﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using DreamTeam.Utils;
using UnityEditor;

namespace DreamTeam.Match3.Teleportation
{
    public partial class TeleportPresenter : TeleportPresenter.IEditorApi
    {
        BaseTeleport IEditorApi.Teleport
        {
            get { return Teleport; }
            set
            {
                Teleport = value;
                new SerializedObject(this).ApplyModifiedPropertiesWithDirtyFlag();
            }
        }

        #region Inner types

        public interface IEditorApi
        {
            BaseTeleport Teleport { get; set; }
        }

        #endregion
    }
}

#endif