﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Match3.Teleportation
{
    public partial class Teleport : Teleport.IEditorApi
    {
        // TODO: Should they be moved to other Teleport implementation or it's okay to leave them here?
        [SerializeField] private bool _separateOut = true;
        [SerializeField, Min(1)] private int _length;

        bool IEditorApi.IsSeparateOut => _separateOut;
        int IEditorApi.Length => _length;

        CellComponent IEditorApi.TargetCell
        {
            get { return TargetCell; }
            set
            {
                _targetCell = value;
                new SerializedObject(this).ApplyModifiedPropertiesWithDirtyFlag();
            }
        }

        protected void OnDrawGizmos()
        {
            if (TargetCell != null)
            {
                Vector3 startPos = transform.position;
                Vector3 endPos = TargetCell.transform.position;

                Gizmos.color = Selection.Contains(gameObject) ?
                    new Color(0.2f, 0.2f, 0.9f) :
                    new Color(0.2f, 0.2f, 0.9f, 0.5f);

                EditorUtils.DrawArrowGizmo(startPos, endPos - startPos);
            }
        }

        #region Inner types

        public interface IEditorApi
        {
            CellComponent TargetCell { get; set; }
            bool IsSeparateOut { get; }
            int Length { get; }
        }

        #endregion
    }
}

#endif