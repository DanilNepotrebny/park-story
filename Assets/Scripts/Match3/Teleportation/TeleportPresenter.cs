﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.Utils;
using DreamTeam.Utils.Coroutines;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.RendererSorting;
using JetBrains.Annotations;
using ModestTree;
using Spine;
using Spine.Unity;
using UnityEngine;
using Zenject;
using AnimationState = Spine.AnimationState;
using Assert = UnityEngine.Assertions.Assert;
using Event = Spine.Event;

namespace DreamTeam.Match3.Teleportation
{
    /// <summary>
    /// Animates teleportation process in the cell. For chip teleportation animation see <see cref="ChipTeleportationPresenter"/>.
    /// </summary>
    public partial class TeleportPresenter : MonoBehaviour, ISortingOptionsProvider
    {
        [SerializeField, RequiredField] private BaseTeleport _teleport;
        [SerializeField] private ChipTeleportationPresenter.TeleportationAnimationType _teleportationType;
        [SerializeField, SpineAnimation] private string _idleAnimationName;
        [SerializeField, SpineAnimation] private string _teleportAnimationName;
        [SerializeField, SpineEvent] private string _teleportationEndEventName;
        [SerializeField] private bool _isReactingOnTeleportationOut;
        [SerializeField] private Transform _effectsContainer;

        [Inject] private Instantiator _instantiator;

        private Renderer _renderer;
        private ParticleSystem[] _effects;
        private SkeletonAnimation _animation;
        private IDeferredInvocationHandle _teleportingEndHadle;

        public SortingOptions SortingOptions { get; private set; }

        protected BaseTeleport Teleport
        {
            get { return _teleport; }
            set { _teleport = value; }
        }

        protected void Awake()
        {
            _renderer = GetComponentInChildren<Renderer>();
            _animation = GetComponentInChildren<SkeletonAnimation>();
            _teleport.TargetCell.ChipChanged += OnChipChanged;
            if (_teleport.SourceCell != null)
            {
                _teleport.SourceCell.ChipChanged += OnChipChanged;
            }

            SortingOptions = _instantiator.Instantiate<SortingOptions>(
                _renderer.sortingLayerID,
                _renderer.sortingOrder);
            SortingOptions.SortingLayerChanged += OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged += OnSortingOrderChanged;
        }

        protected void Start()
        {
            _effects = _effectsContainer != null ?
                _effectsContainer.GetComponentsInChildren<ParticleSystem>() :
                Array.Empty<ParticleSystem>();
        }

        protected void OnDestroy()
        {
            SortingOptions.SortingLayerChanged -= OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged -= OnSortingOrderChanged;
            SortingOptions.Dispose();
            SortingOptions = null;
        }

        private void OnChipChanged(CellComponent cell, ChipComponent prevChip)
        {
            if (prevChip != null && prevChip.Presenter != null)
            {
                ChipTeleportationPresenter oldPresenter = prevChip.Presenter.TeleportationPresenter;
                if (oldPresenter != null)
                {
                    UnsubscribeFromEvent(oldPresenter);
                }
            }

            if (cell.Chip != null && cell.Chip.Presenter != null)
            {
                ChipTeleportationPresenter newPresenter = cell.Chip.Presenter.TeleportationPresenter;
                if (newPresenter != null)
                {
                    SubscribeToEvent(newPresenter);
                }
            }
        }

        private void OnTeleportingIn(
            ChipTeleportationPresenter presenter,
            [CanBeNull] IDeferredInvocationHandle teleportationEndHandle)
        {
            if (_teleportationEndEventName == string.Empty)
            {
                teleportationEndHandle = null;
            }

            StartCoroutine(PlayTeleportation(presenter, teleportationEndHandle));
        }

        private void OnTeleportingOut(ChipTeleportationPresenter presenter)
        {
            StartCoroutine(PlayTeleportation(presenter));
        }

        private IEnumerator PlayTeleportation(
            ChipTeleportationPresenter presenter,
            [CanBeNull] IDeferredInvocationHandle teleportationEndHandle = null)
        {
            Assert.IsNull(_teleportingEndHadle);
            _teleportingEndHadle = teleportationEndHandle?.Lock();

            presenter.SetAnimationDirection(
                _isReactingOnTeleportationOut ?
                _teleport.DirectionOut :
                _teleport.DirectionIn);

            _effects.ForEach(e => e.Play(false));
            TrackEntry teleportingTrack = _animation.state.SetAnimation(0, _teleportAnimationName, false);

            if (_teleportingEndHadle != null)
            {
                yield return new WaitForEvent<TrackEntry, Event, AnimationState>(
                    _animation.state,
                    nameof(_animation.state.Event),
                    (track, @event) => @event.Data.Name == _teleportationEndEventName);

                _teleportingEndHadle.Unlock();
                _teleportingEndHadle = null;
            }

            yield return new WaitUntil(() => teleportingTrack.IsComplete);

            _animation.state.SetAnimation(0, _idleAnimationName, true);
        }

        private void SubscribeToEvent([NotNull] ChipTeleportationPresenter presenter)
        {
            presenter.SetTeleportationAnimationType(_teleportationType);

            if (_isReactingOnTeleportationOut)
            {
                presenter.TeleportingOut += OnTeleportingOut;
            }
            else
            {
                presenter.TeleportingIn += OnTeleportingIn;
            }
        }

        private void UnsubscribeFromEvent([NotNull] ChipTeleportationPresenter presenter)
        {
            if (_isReactingOnTeleportationOut)
            {
                presenter.TeleportingOut -= OnTeleportingOut;
            }
            else
            {
                presenter.TeleportingIn -= OnTeleportingIn;
            }
        }

        private void OnSortingOrderChanged(SortingOptions options, int prevorder)
        {
            _renderer.sortingOrder = options.SortingOrder;
        }

        private void OnSortingLayerChanged(SortingOptions options)
        {
            _renderer.sortingLayerID = options.SortingLayerId;
        }
    }
}