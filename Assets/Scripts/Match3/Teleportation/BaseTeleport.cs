﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Match3.Teleportation
{
    public abstract class BaseTeleport : MonoBehaviour
    {
        public abstract CellComponent TargetCell { get; }
        public abstract CellComponent SourceCell { get; }
        public abstract Direction DirectionOut { get; }
        public abstract Direction DirectionIn { get; }
    }
}