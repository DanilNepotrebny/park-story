﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.MoveWeight
{
    [RequireComponent(typeof(CellComponent))]
    public abstract class CellMoveWeightCriterion : MonoBehaviour, IWeightCriterion
    {
        public CellComponent Cell { get; private set; }

        public abstract float CalculateWeight();

        protected void Awake()
        {
            Cell = GetComponent<CellComponent>();
        }
    }
}