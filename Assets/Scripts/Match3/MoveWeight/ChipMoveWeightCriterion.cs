﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.MoveWeight
{
    [RequireComponent(typeof(ChipComponent))]
    public class ChipMoveWeightCriterion : MonoBehaviour, IWeightCriterion
    {
        [Inject(Id = LevelSettings.GoalInstancesId)]
        private IReadOnlyList<Requirement.Instance> _goals;

        private ChipComponent _chip;

        public float AdditionalWeight { get; set; }

        float IWeightCriterion.CalculateWeight()
        {
            float weight = _chip.BaseMoveWeight + AdditionalWeight;
            var cells = new HashSet<CellComponent>();

            foreach (Requirement.Instance goal in _goals)
            {
                if (goal.IsReached)
                {
                    continue;
                }

                RequirementHelper helper = goal.FindHelper();
                if (helper == null)
                {
                    continue; // might be null, for example, when there are no required color chips to consume
                }

                cells.Clear();
                helper.FindCellsToImpact(ImpactFlag.All, int.MaxValue, ref cells);

                if (cells.Contains(_chip.Cell))
                {
                    weight += _chip.RequiredMoveWeight;
                }
            }

            return weight;
        }

        protected void Awake()
        {
            _chip = GetComponent<ChipComponent>();
        }
    }
}