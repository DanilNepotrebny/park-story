﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Matching;
using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.Requirements.ChipConsumption;
using DreamTeam.Match3.Wall;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.MoveWeight
{
    public class MoveWeightCalculator : MonoBehaviour, IWeightCalculator
    {
        [SerializeField,
         Tooltip("Closer move to required chip, bigger weight added. Does not apply to moves with required chips")]
        private float _maxWeightAddedByDistanceToRequiredChip = 500;

        [Inject] private MatchSystem _matchSystem;
        [Inject] private GridSystem _gridSystem;
        [Inject] private CombinationSystem _combinationSystem;
        [InjectOptional] private WallSystem _wallSystem;

        [Inject(Id = LevelSettings.GoalInstancesId)]
        private IReadOnlyList<Requirement.Instance> _goals;

        float IWeightCalculator.Calculate(IList<IWeightCriterion> criteria)
        {
            return criteria.Sum(criterion => criterion.CalculateWeight());
        }

        public float CalculateMatchingMove(
                [NotNull] IEnumerable<Match> matches,
                [CanBeNull] ChipComponent matchSourceChip,
                [CanBeNull] ChipComponent matchTargetChip
            )
        {
            var matchChips = new HashSet<ChipComponent>();

            if (matchSourceChip != null)
            {
                matchChips.Add(matchSourceChip);
            }

            if (matchTargetChip != null)
            {
                matchChips.Add(matchTargetChip);
            }

            var scheduledMatchImpacts = new List<Impact>();
            foreach (Match match in matches)
            {
                if (match == null)
                {
                    continue;
                }

                foreach (Vector2Int pos in match)
                {
                    CellComponent cell = _gridSystem[pos.x, pos.y];
                    if (cell != null && cell.Chip != null)
                    {
                        matchChips.Add(cell.Chip);
                    }
                }

                List<Impact> impacts = _matchSystem.ScheduleImpact(match);
                scheduledMatchImpacts.AddRange(impacts);
            }

            List<Impact> combinationImpacts =
                _combinationSystem.ScheduleCombinationImpact(matchSourceChip, matchTargetChip);
            scheduledMatchImpacts.AddRange(combinationImpacts);

            HashSet<IWeightCriterion> criteria = CalculateCriteria();

            scheduledMatchImpacts.ForEach(i => i.Cancel());

            float weight = ((IWeightCalculator)this).Calculate(criteria.ToList());
            weight += CalculateDistanceToRequiredWeight(matchChips);
            return weight;
        }

        public float CalculateActivationMove([NotNull] ChipActivator activator)
        {
            Impact impact = activator.ScheduleActivation();

            HashSet<IWeightCriterion> criteria = CalculateCriteria();

            impact.Cancel();

            float weight = ((IWeightCalculator)this).Calculate(criteria.ToList());
            weight += CalculateDistanceToRequiredWeight(
                new HashSet<ChipComponent> { activator.ChipPresenter.ChipComponent });
            return weight;
        }

        private HashSet<IWeightCriterion> CalculateCriteria()
        {
            var criteria = new HashSet<IWeightCriterion>();
            foreach (CellComponent cell in _gridSystem.Cells)
            {
                bool cellWillBeImpacted = false;

                ChipComponent chip = cell.Chip;
                while (chip != null)
                {
                    if (chip.WillBeImpacted())
                    {
                        cellWillBeImpacted = true;
                        criteria.Add(chip.MoveWeightCriterion);
                    }

                    chip = chip.Child;
                }

                if (cellWillBeImpacted)
                {
                    criteria.UnionWith(cell.WeightCalculator);
                }
            }

            if (_wallSystem != null)
            {
                _wallSystem.ForEachWall(
                    wall =>
                    {
                        if (wall.WillBeDestroyed())
                        {
                            criteria.Add(wall);
                        }
                    });
            }

            return criteria;
        }

        private float CalculateDistanceToRequiredWeight(HashSet<ChipComponent> chips)
        {
            float minDistance = float.MaxValue;
            foreach (Requirement.Instance goal in _goals)
            {
                if (goal.IsReached)
                {
                    continue;
                }

                ChipConsumptionRequirement chipConsumptionRequirement =
                    goal.BaseRequirement as ChipConsumptionRequirement;
                if (chipConsumptionRequirement == null)
                {
                    continue;
                }

                // move with required chip has no additional distance weight
                if (chips.Any(chip => chip.Tags.Has(chipConsumptionRequirement.RequiredChipTag, false)))
                {
                    return 0;
                }

                foreach (CellComponent cell in _gridSystem.Cells)
                {
                    if (cell.Chip == null || !cell.Chip.Tags.Has(chipConsumptionRequirement.RequiredChipTag, false))
                    {
                        continue;
                    }

                    Vector2Int cellPos = cell.GridPosition;
                    minDistance = chips.Select(chip => Vector2Int.Distance(cellPos, chip.Cell.GridPosition))
                        .Concat(new[] { minDistance })
                        .Min();
                }
            }

            minDistance = Mathf.Min(1f, minDistance);

            return _maxWeightAddedByDistanceToRequiredChip / minDistance;
        }
    }
}