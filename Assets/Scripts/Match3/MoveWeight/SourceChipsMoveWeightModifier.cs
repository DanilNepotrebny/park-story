﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Match3.MoveWeight
{
    /// <summary> Modifies DefaultMoveWeightCriterion weight of chips </summary>
    public class SourceChipsMoveWeightModifier : MonoBehaviour
    {
        [SerializeField] private float _additionalWeight;

        private readonly HashSet<ChipMoveWeightCriterion> _touchedCriteria = new HashSet<ChipMoveWeightCriterion>();

        /// <summary> Apply move weight modifier to chip's DefaultMoveWeightCriterion if any </summary>
        /// <param name="chip"> Chip to be applied on </param>
        public void ApplyAdditionalWeight([NotNull] ChipComponent chip)
        {
            ChipMoveWeightCriterion criterion = chip.MoveWeightCriterion;
            if (criterion != null && !_touchedCriteria.Contains(criterion))
            {
                criterion.AdditionalWeight += _additionalWeight;
                _touchedCriteria.Add(criterion);
            }
        }

        /// <summary> Revert all applied weights </summary>
        public void RevertAll()
        {
            foreach (ChipMoveWeightCriterion criterion in _touchedCriteria)
            {
                criterion.AdditionalWeight -= _additionalWeight;
            }

            _touchedCriteria.Clear();
        }
    }
}