﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.MoveWeight
{
    public class HiddenObjectMoveWeightCriterion : CellMoveWeightCriterion
    {
        [SerializeField] private float _weight = 1;

        [Inject] private List<HiddenObject> _hiddenObjects;
        [Inject] private GridSystem _gridSystem;

        private bool _isWeightApplied;

        public override float CalculateWeight()
        {
            return _isWeightApplied ? _weight : 0;
        }

        protected void Start()
        {
            foreach (HiddenObject ho in _hiddenObjects)
            {
                for (int x = ho.Bounds.x; x < ho.Bounds.xMax; ++x)
                {
                    for (int y = ho.Bounds.y; y < ho.Bounds.yMax; ++y)
                    {
                        if (Cell != _gridSystem.GetCell(new Vector2Int(x, y)))
                        {
                            continue;
                        }

                        if (Cell.Underlay == null)
                        {
                            return;
                        }

                        Cell.Underlay.Destroyed += underlay => _isWeightApplied = false;
                        _isWeightApplied = true;
                        return;
                    }
                }
            }
        }
    }
}