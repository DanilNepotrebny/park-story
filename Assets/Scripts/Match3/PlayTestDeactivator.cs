﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Disables game object for playtests build.
    /// </summary>
    public class PlayTestDeactivator : MonoBehaviour
    {
        protected void Awake()
        {
            #if PLAYTESTS
            {
                gameObject.SetActive(false);
            }
            #else
            {
                this.Dispose();
            }
            #endif
        }
    }
}