﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace DreamTeam.Match3
{
    public partial class GenericPlacingArea<T> : IPlacingAreaEditorApi
    {
        private const float SelectedAnimSpeed = 0.2f;
        private const float ColorDistinctFactor = 0.2f; // lower value - less color distinction
        private const float ColorDistributionFactor = 0.3f; // lower value - less color distinction

        private bool _pickColor = true;
        private Color _gizmoColor = Color.black;
        private float _animColorDelta;
        private bool _isSelected;

        protected void OnDrawGizmos()
        {
            if (_pickColor)
            {
                _pickColor = false;
                _gizmoColor = CalculateUniqueGizmoColor(PickedGizmoColors);
            }

            _cellPositions.ForEach(DrawAreaPart);
        }

        private void UpdateSelectionColor()
        {
            _gizmoColor.a += _animColorDelta;
            if (_gizmoColor.a < 0.25f || _gizmoColor.a > 1)
            {
                _animColorDelta = -_animColorDelta;
            }

            if (_isSelected)
            {
                EditorApplication.delayCall += UpdateSelectionColor;
            }
        }

        private void DrawAreaPart(Vector2Int cellPos)
        {
            if (_grid == null)
            {
                _grid = FindObjectOfType<GridSystem>();
            }

            if (_grid == null)
            {
                return;
            }

            CellComponent cell = _grid.GetCell(cellPos);
            var size = new Vector2(_grid.CellWidth, _grid.CellHeight);
            Vector2 halfSize = size * 0.5f;
            const float borderSize = 0.1f;

            Gizmos.color = _gizmoColor;

            if (!Contains(new Vector2Int(cellPos.x, cellPos.y - 1)))
            {
                Gizmos.DrawCube(
                        cell.transform.position - new Vector3(0, halfSize.y - borderSize * 0.5f, 0),
                        new Vector3(size.x, borderSize, 0)
                    );
            }

            if (!Contains(new Vector2Int(cellPos.x, cellPos.y + 1)))
            {
                Gizmos.DrawCube(
                        cell.transform.position + new Vector3(0, halfSize.y - borderSize * 0.5f, 0),
                        new Vector3(size.x, borderSize, 0)
                    );
            }

            if (!Contains(new Vector2Int(cellPos.x - 1, cellPos.y)))
            {
                Gizmos.DrawCube(
                        cell.transform.position - new Vector3(halfSize.x - borderSize * 0.5f, 0, 0),
                        new Vector3(borderSize, size.y, 0)
                    );
            }

            if (!Contains(new Vector2Int(cellPos.x + 1, cellPos.y)))
            {
                Gizmos.DrawCube(
                        cell.transform.position + new Vector3(halfSize.x - borderSize * 0.5f, 0, 0),
                        new Vector3(borderSize, size.y, 0)
                    );
            }
        }

        private static Color CalculateUniqueGizmoColor(List<Color> colorsToSkip)
        {
            int i = 0;
            const int iterCount = 10000;
            do
            {
                i += 1;
                if (i > iterCount)
                {
                    UnityEngine.Debug.LogWarning($"Can't calculate unique color for placing area. " +
                        $"Decrease {nameof(ColorDistinctFactor)} or/and {nameof(ColorDistributionFactor)}. " +
                        $"Falling back to black color...");
                    return Color.black;
                }

                // Pick a random, saturated and not-too-dark color
                Color color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);

                float rgDelta = Mathf.Abs(color.r - color.g);
                float gbDelta = Mathf.Abs(color.g - color.b);
                float rbDelta = Mathf.Abs(color.r - color.b);

                if (Mathf.Min(rgDelta, gbDelta, rbDelta) < ColorDistributionFactor)
                {
                    continue;
                }

                bool foundSimilarColor = false;
                foreach (Color skipColor in colorsToSkip)
                {
                    float rDelta = Mathf.Abs(color.r - skipColor.r);
                    float gDelta = Mathf.Abs(color.g - skipColor.g);
                    float bDelta = Mathf.Abs(color.b - skipColor.b);

                    if (rDelta + gDelta + bDelta < ColorDistinctFactor)
                    {
                        foundSimilarColor = true;
                        break;
                    }
                }

                if (foundSimilarColor)
                {
                    continue;
                }

                PickedGizmoColors.Add(color);
                return color;
            }
            while (true);
        }

        bool IPlacingAreaEditorApi.Add(IPlaceable placeable)
        {
            if (_elements.Contains(placeable as Object))
            {
                return false;
            }

            int existingPlaceablesSquare = 0;
            Elements.ForEach(p => existingPlaceablesSquare += p.Square);

            if (existingPlaceablesSquare + placeable.Square > Square)
            {
                UnityEngine.Debug.LogWarning($"Can't add more '{nameof(IPlaceable)}' onto area");
                return false;
            }

            _elements.Add(placeable as Object);
            return true;
        }

        bool IPlacingAreaEditorApi.Remove(IPlaceable placeable)
        {
            return _elements.Remove(placeable as Object);
        }

        Type IPlacingAreaEditorApi.GetElementType()
        {
            return typeof(T);
        }

        void IPlacingAreaEditorApi.OnSelected()
        {
            _isSelected = true;
            _gizmoColor.a = 1f;
            _animColorDelta = -SelectedAnimSpeed;
            EditorApplication.delayCall += UpdateSelectionColor;
        }

        void IPlacingAreaEditorApi.OnDeselected()
        {
            _isSelected = false;
            _gizmoColor.a = 1f;
            _animColorDelta = 0;
        }
    }

    public interface IPlacingAreaEditorApi
    {
        bool Contains(Vector2Int cellPos);
        bool Expand(Vector2Int cellPos);
        bool Shrink(Vector2Int cellPos);

        bool Add([NotNull] IPlaceable placeable);
        bool Remove([NotNull] IPlaceable placeable);
        Type GetElementType();

        void OnSelected();
        void OnDeselected();

        GameObject gameObject { get; }
        int Square { get; }
    }
}

#endif // UNITY_EDITOR
