﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System.Linq;
using UnityEngine;

namespace DreamTeam.Match3
{
    public partial class HiddenObject
    {
        private const float cellEdgeOffset = 0.18f;
        private const float cubeSizeExtent = cellEdgeOffset * 2;
        private const float edgeThickness = 0.1f;

        private bool _laysOnGrid => Cells.Count() == Bounds.height * Bounds.width;

        protected void OnDrawGizmos()
        {
            if (_grid == null)
            {
                return;
            }

            Rect worldBounds = _grid.GridToWorldBounds(Bounds);
            Vector2 halfSize = worldBounds.size * 0.5f;

            Gizmos.color = _laysOnGrid ? Color.blue : Color.red;
            Gizmos.DrawCube(
                    (Vector3)worldBounds.center - new Vector3(0, halfSize.y - cellEdgeOffset, 0),
                    new Vector3(worldBounds.size.x - cubeSizeExtent, edgeThickness, edgeThickness)
                );
            Gizmos.DrawCube(
                    (Vector3)worldBounds.center + new Vector3(0, halfSize.y - cellEdgeOffset, 0),
                    new Vector3(worldBounds.size.x - cubeSizeExtent, edgeThickness, edgeThickness)
                );
            Gizmos.DrawCube(
                    (Vector3)worldBounds.center - new Vector3(halfSize.x - cellEdgeOffset, 0, 0),
                    new Vector3(edgeThickness, worldBounds.size.y - cubeSizeExtent, edgeThickness)
                );
            Gizmos.DrawCube(
                    (Vector3)worldBounds.center + new Vector3(halfSize.x - cellEdgeOffset, 0, 0),
                    new Vector3(edgeThickness, worldBounds.size.y - cubeSizeExtent, edgeThickness)
                );
        }
    }
}

#endif