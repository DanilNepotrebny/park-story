﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Moves view related game object to the parent view related game object in hierarchy.
    /// </summary>
    /// <remarks>
    /// For example, if assigned to the logics game objects with such hierarchy
    /// -+-- Cell
    ///  +--- Cell View
    ///  +-+- Chip
    ///    +-- Chip View
    /// 
    /// it will be rearranged to
    /// -+-- Cell
    ///  +-+- Cell View
    ///  | +-- Chip View
    ///  +--- Chip
    /// </remarks>
    [DisallowMultipleComponent]
    public class ViewParentController : MonoBehaviour
    {
        [SerializeField, Tooltip("View that will have child source attached")]
        private Transform _viewSource;

        [SerializeField, Tooltip("View that will be attached to parent target")]
        private Transform _viewTarget;

        public Transform ViewSource
        {
            get { return _viewSource; }
            set { _viewSource = value; }
        }

        public Transform ViewTarget
        {
            get { return _viewTarget; }
            set { _viewTarget = value; }
        }

        protected void OnEnable()
        {
            OnTransformParentChanged();
        }

        protected void OnTransformParentChanged()
        {
            if (_viewSource == null || _viewTarget == null || !enabled)
            {
                return;
            }

            var parent = transform.parent?.GetComponent<ViewParentController>();
            if (parent != null)
            {
                _viewSource.SetParentPreserveLocalScale(parent._viewTarget);
            }
        }
    }
}