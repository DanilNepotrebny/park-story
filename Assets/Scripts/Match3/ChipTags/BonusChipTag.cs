﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3.ChipTags
{
    /// <summary>
    /// Chip tag for all bonus chips (bombs, magic hat etc.).
    /// </summary>
    public class BonusChipTag : ChipTag
    {
        public override bool IsAbstract => false;
    }
}