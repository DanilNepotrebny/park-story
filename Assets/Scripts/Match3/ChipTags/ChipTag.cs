﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Match3.ChipTags
{
    /// <summary>
    /// Base abstract class for all chip tags. Can be used if you need "any chip tag" in settings.
    /// </summary>
    public class ChipTag : ScriptableObject
    {
        /// <summary>
        /// Determines wheter this tag can be assigned to chip object
        /// </summary>
        public virtual bool IsAbstract => true;
    }
}