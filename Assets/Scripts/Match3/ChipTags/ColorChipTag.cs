﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3.ChipTags
{
    /// <summary>
    /// Abstract tag for all chips which have color.
    /// </summary>
    public class ColorChipTag : ChipTag
    {
    }
}