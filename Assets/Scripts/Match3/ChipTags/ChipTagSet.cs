﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Match3.ChipTags
{
    /// <summary>
    /// Set of chip tags. Supports tags class-based hierarchy. Can't contain two tags of the same type or derived from the same parent.
    /// </summary>
    [Serializable]
    public class ChipTagSet : IEnumerable<ChipTag>
    {
        [SerializeField] private List<ChipTag> _chipTags = new List<ChipTag>();

        private bool _allowsAbstract;

        /// <summary>
        /// Constructs chip tag set. 
        /// </summary>
        /// <param name="allowsAbstract">Pass false if you want to prohibit adding of abstract chip tags to the set</param>
        public ChipTagSet(bool allowsAbstract = true)
        {
            _allowsAbstract = allowsAbstract;
        }

        /// <summary>
        /// Searches for tag of specified or derived type. Returns null if no such tag is assigned.
        /// </summary>
        public T Get<T>() where T : ChipTag
        {
            foreach (ChipTag tag in _chipTags)
            {
                T candidate = tag as T;
                if (candidate != null)
                {
                    return candidate;
                }
            }

            return null;
        }

        /// <summary>
        /// Tests whether specified or derived tag is assigned or not.
        /// </summary>
        public bool Has(ChipTag tag)
        {
            return Get(tag) != null;
        }

        /// <summary>
        /// Find first common tag between current and other sets if it exists.
        /// </summary>
        public ChipTag FindFirstOf(ChipTagSet set)
        {
            foreach (ChipTag tag in set._chipTags)
            {
                ChipTag candidate = Get(tag);
                if (candidate != null)
                {
                    return candidate;
                }
            }

            return null;
        }

        /// <summary>
        /// Adds specified tag if tag of the same or derived type is not already assigned. If tag of parent class exists it will be replaced.
        /// </summary>
        public void Add(ChipTag tag)
        {
            if (!_allowsAbstract && tag.IsAbstract)
            {
                throw new ArgumentException(
                    "Can't add abstract tag to set with non-abstract tags restriction",
                    nameof(tag));
            }

            Type incomingType = tag.GetType();

            for (int i = 0; i < _chipTags.Count; i++)
            {
                ChipTag t = _chipTags[i];
                if (t == tag)
                {
                    // The same tag has already been assigned, do nothing.
                    return;
                }

                Type type = t.GetType();
                if (incomingType == type || incomingType.IsAssignableFrom(type))
                {
                    // The same type or derived type, do nothing.
                    return;
                }

                if (type.IsInstanceOfType(tag))
                {
                    // Found tag of parent class, replace it.
                    _chipTags[i] = tag;
                    return;
                }

                type = type.BaseType;

                while (type != typeof(ChipTag))
                {
                    if (type.IsInstanceOfType(tag))
                    {
                        throw new AmbiguousTagException(
                            string.Format(
                                "Can't add tag of type {0} because it is ambiguous with {1}",
                                incomingType.Name,
                                type.Name));
                    }

                    type = type.BaseType;
                }
            }

            _chipTags.Add(tag);
        }

        /// <summary>
        /// Adds all tags from another chip tag collection.
        /// </summary>
        public void AddRange(ChipTagSet tags)
        {
            foreach (ChipTag tag in tags._chipTags)
            {
                Add(tag);
            }
        }

        public void Remove(ChipTag chipTag)
        {
            _chipTags.Remove(Get(chipTag));
        }

        /// <summary>
        /// Removes all tags.
        /// </summary>
        public void Clear()
        {
            _chipTags.Clear();
        }

        /// <summary>
        /// Get the very first tag
        /// </summary>
        /// <param name="mightBeAbstract">Does abstract tags are allowed</param>
        public ChipTag GetFirst(bool mightBeAbstract = false)
        {
            // TODO: Does it really make sense?
            foreach (ChipTag tag in _chipTags)
            {
                if (mightBeAbstract)
                {
                    return tag;
                }

                if (!tag.IsAbstract)
                {
                    return tag;
                }
            }

            return null;
        }

        public bool IsEmpty()
        {
            return _chipTags.Count == 0;
        }

        public IEnumerator<ChipTag> GetEnumerator()
        {
            return _chipTags.GetEnumerator();
        }

        private ChipTag Get(ChipTag tag)
        {
            Type type = tag.GetType();

            foreach (ChipTag t in _chipTags)
            {
                if (t == tag || type.IsInstanceOfType(t))
                {
                    return t;
                }
            }

            return null;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #region Inner classes

        public class AmbiguousTagException : InvalidOperationException
        {
            public AmbiguousTagException() : base()
            {
            }

            public AmbiguousTagException(string message) : base(message)
            {
            }

            public AmbiguousTagException(string message, Exception inner) : base(message, inner)
            {
            }
        }

        #endregion
    }
}