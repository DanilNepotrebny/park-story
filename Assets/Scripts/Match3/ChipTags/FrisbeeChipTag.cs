﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3.ChipTags
{
    public class FrisbeeChipTag : ChipTag
    {
        public override bool IsAbstract => false;
    }
}