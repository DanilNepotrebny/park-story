﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Text;
using DreamTeam.Debug;
using DreamTeam.Utils;
using Zenject;

namespace DreamTeam.Match3
{
    public class Match3InfoDebugWriter : IInitializable, IDisposable
    {
        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        [Inject] private DebugStatesSystem _debugStatesSystem;

        public void Initialize()
        {
            _debugStatesSystem.RegisterWriter("Match3 Info", WriteDebugState);
        }

        public void Dispose()
        {
            _debugStatesSystem.UnregisterWriter(WriteDebugState);
        }

        private void WriteDebugState(StringBuilder builder)
        {
            builder.AppendParameter("Custom Random Seed", _random.Seed.ToString());
        }
    }
}
