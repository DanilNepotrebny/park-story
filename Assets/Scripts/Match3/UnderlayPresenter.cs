﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Audio;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.RendererSorting;
using UnityEngine;
using UnityEngine.Rendering;
using Zenject;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(SortingGroup), typeof(Renderer))]
    public class UnderlayPresenter : MonoBehaviour, ISortingOptionsProvider, IGridPositionProvider
    {
        [SerializeField, RequiredField] private Underlay _underlay;
        [SerializeField, RequiredField] private AudioSystemPlayer _audioPlayer;
        [SerializeField, RequiredField] private AudioSourceProvider _healthChandedSoundProvider;
        [SerializeField] private SortingLayerInfo[] _sortingLayers;

        [Inject] private Instantiator _instantiator;

        private SortingGroup _sortingGroup;

        public Vector2Int GridPosition { get; private set; }

        public SortingOptions SortingOptions { get; private set; }

        protected void Awake()
        {
            _sortingGroup = GetComponent<SortingGroup>();
            SortingOptions = _instantiator.Instantiate<SortingOptions>(
                _sortingGroup.sortingLayerID,
                _sortingGroup.sortingOrder);
            SortingOptions.SortingLayerChanged += OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged += OnSortingOrderChanged;

            _underlay.HealthChanged += OnHealthChanged;
            _underlay.Destroying += OnDestroying;

            var cell = GetComponentInParent<CellComponent>();
            GridPosition = cell.GridPosition;

            UpdateSortingLayer();
        }

        protected void OnDestroy()
        {
            SortingOptions.SortingLayerChanged -= OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged -= OnSortingOrderChanged;

            _underlay.HealthChanged -= OnHealthChanged;
            _underlay.Destroying -= OnDestroying;

            SortingOptions.Dispose();
            SortingOptions = null;
        }

        private void OnHealthChanged(int obj)
        {
            _audioPlayer.Play(_healthChandedSoundProvider.Current);
        }

        private void OnDestroying(IDeferredInvocationHandle handle)
        {
            if (_audioPlayer.IsPlaying)
            {
                IDeferredInvocationHandle destroyingHandle = handle.Lock();
                _audioPlayer.Stoped += destroyingHandle.Unlock;
            }
        }

        private void UpdateSortingLayer()
        {
            foreach (SortingLayerInfo info in _sortingLayers)
            {
                if (info.health == _underlay.Health)
                {
                    SortingOptions.SortingLayerId = info.layer;
                    break;
                }
            }
        }

        private void OnSortingOrderChanged(SortingOptions options, int prevorder)
        {
            _sortingGroup.sortingOrder = options.SortingOrder;
        }

        private void OnSortingLayerChanged(SortingOptions options)
        {
            _sortingGroup.sortingLayerID = options.SortingLayerId;
        }

        #region Inner classes

        [Serializable]
        private class SortingLayerInfo
        {
            [SerializeField] public int health;
            [SerializeField, SortingLayer] public int layer;
        }

        #endregion
    }
}