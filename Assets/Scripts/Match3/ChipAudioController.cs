﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Audio;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(ChipAnimatorContoller))]
    public class ChipAudioController : MonoBehaviour
    {
        [SerializeField, RequiredField] private AudioSystemPlayer _player;
        [SerializeField, RequiredField] private ChipAudioSettings _settings;

        protected ChipAnimatorContoller ChipAnimator => GetComponent<ChipAnimatorContoller>();

        public void PlaySound(string eventName)
        {
            AudioSource source = _settings.GetAudioSource(eventName);
            if (source != null)
            {
                _player.Play(source);
            }
        }

        protected void Awake()
        {
            ChipAnimator.AnimationEventHandlerOccurred += PlaySound;
        }

        protected void OnDestroy()
        {
            ChipAnimator.AnimationEventHandlerOccurred -= PlaySound;
        }

        protected void OnEnable()
        {
            _player.Stoped -= OnPlayerStoped;
        }

        protected void OnDisable()
        {
            if (_player.IsPlaying && !_player.IsLooped)
            {
                _player.transform.SetParent(null);
                _player.Stoped += OnPlayerStoped;
            }
        }

        private void OnPlayerStoped()
        {
            _player.gameObject.Dispose();
        }
    }
}