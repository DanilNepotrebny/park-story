﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    public partial class HealthController : MonoBehaviour
    {
        [SerializeField] private int _maxHealth = 1;
        [SerializeField, EnumFlag] private ImpactFlag _impactFlags;

        public int MaxHealth => _maxHealth;

        public event Action<int> ValueChanged;

        /// <summary>
        /// Current health of this chip.
        /// </summary>
        public int Value
        {
            get { return _value; }
            set
            {
                if (_value == value)
                {
                    return;
                }

                _value = value;
                ValueChanged?.Invoke(_value);
            }
        }

        private int _value;

        /// <returns>impact value left after health controller</returns>
        public int OnHealthImpacting(int impactValue, ImpactFlag flag)
        {
            if (!CanBeImpacted(flag))
            {
                return impactValue;
            }

            int impactValueLeft = 0;
            int newHealth = Value + impactValue;
            if (newHealth < 0)
            {
                impactValueLeft = newHealth;
                newHealth = 0;
            }

            Value = newHealth;

            return impactValueLeft;
        }

        public bool CanBeImpacted(ImpactFlag flag)
        {
            return (_impactFlags & flag) == flag;
        }

        protected void Awake()
        {
            Value = _maxHealth;
        }
    }
}