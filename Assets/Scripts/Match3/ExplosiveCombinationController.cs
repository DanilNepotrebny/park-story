﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Combination controller for detecting combination of one or two explosive chips. In a game this looks like just
    /// an activation of explosive chips via swapping, but not a combination of items.
    /// </summary>
    [CreateAssetMenu(
        fileName = "ExplosiveCombinationController",
        menuName = "Match 3/Chip controllers/Create explosive combination controller")]
    public class ExplosiveCombinationController : ScriptableObject, ICombinationController
    {
        bool ICombinationController.HasCombination(ChipComponent mainChip, ChipComponent additionalChip)
        {
            return mainChip.GetComponent<ExplosiveComponent>() != null;
        }

        List<Impact> ICombinationController.ScheduleCombinationImpact(
                ChipComponent mainChip,
                ChipComponent addtionalChip
            )
        {
            var impacts = new List<Impact>(2);

            Impact impact1 = ScheduleExplosiveChipActivation(mainChip);
            if (impact1 != null)
            {
                impacts.Add(impact1);
            }

            Impact impact2 = ScheduleExplosiveChipActivation(addtionalChip);
            if (impact2 != null)
            {
                impacts.Add(impact2);
            }

            return impacts;
        }

        void ICombinationController.AcceptCombination(ChipComponent mainChip, ChipComponent additionalChip)
        {
            ActivateExplosiveChip(mainChip);
            ActivateExplosiveChip(additionalChip);
        }

        private static void ActivateExplosiveChip([CanBeNull] ChipComponent chip)
        {
            WhetherExplosiveComponentExists(chip, explosive => chip.ScheduleActivation().Perform());
        }

        private static Impact ScheduleExplosiveChipActivation([CanBeNull] ChipComponent chip)
        {
            Impact impact = null;
            WhetherExplosiveComponentExists(chip, explosive => impact = chip.ScheduleActivation());
            return impact;
        }

        private static bool WhetherExplosiveComponentExists(
            [CanBeNull] MonoBehaviour mb,
            Action<ExplosiveComponent> action)
        {
            ExplosiveComponent explosive = mb?.GetComponent<ExplosiveComponent>();
            if (explosive == null)
            {
                return false;
            }

            action?.Invoke(explosive);
            return true;
        }
    }
}