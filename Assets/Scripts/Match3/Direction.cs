﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Direction enum with 8 possible angles and special <see cref="None"/> value.
    /// </summary>
    public enum Direction
    {
        None,
        Up,
        Down,
        Left,
        Right,
        UpLeft,
        UpRight,
        DownLeft,
        DownRight
    }

    public static class DirectionExtensions
    {
        /// <summary>
        /// Mirrors direction (e.g. returns <see cref="Direction.Up"/> for <see cref="Direction.Down"/> and vice versa).
        /// </summary>
        public static Direction GetMirrored(this Direction direction)
        {
            Direction result;
            switch (direction)
            {
                case Direction.None:
                    result = Direction.None;
                    break;
                case Direction.Up:
                    result = Direction.Down;
                    break;
                case Direction.Down:
                    result = Direction.Up;
                    break;
                case Direction.Left:
                    result = Direction.Right;
                    break;
                case Direction.Right:
                    result = Direction.Left;
                    break;
                case Direction.UpLeft:
                    result = Direction.DownRight;
                    break;
                case Direction.UpRight:
                    result = Direction.DownLeft;
                    break;
                case Direction.DownLeft:
                    result = Direction.UpRight;
                    break;
                case Direction.DownRight:
                    result = Direction.UpLeft;
                    break;
                default:
                    throw new ArgumentException("Unknown direction value");
            }

            return result;
        }

        /// <summary>
        /// Rotates direction clockwise by 45 degrees.
        /// </summary>
        public static Direction RotateCW(this Direction direction)
        {
            Direction result;
            switch (direction)
            {
                case Direction.None:
                    result = Direction.None;
                    break;
                case Direction.Up:
                    result = Direction.UpRight;
                    break;
                case Direction.UpRight:
                    result = Direction.Right;
                    break;
                case Direction.Right:
                    result = Direction.DownRight;
                    break;
                case Direction.DownRight:
                    result = Direction.Down;
                    break;
                case Direction.Down:
                    result = Direction.DownLeft;
                    break;
                case Direction.DownLeft:
                    result = Direction.Left;
                    break;
                case Direction.Left:
                    result = Direction.UpLeft;
                    break;
                case Direction.UpLeft:
                    result = Direction.Up;
                    break;
                default:
                    throw new ArgumentException("Unknown direction value");
            }

            return result;
        }

        /// <summary>
        /// Rotates direction counterclockwise by 45 degrees.
        /// </summary>
        public static Direction RotateCCW(this Direction direction)
        {
            Direction result;
            switch (direction)
            {
                case Direction.None:
                    result = Direction.None;
                    break;
                case Direction.Up:
                    result = Direction.UpLeft;
                    break;
                case Direction.UpRight:
                    result = Direction.Up;
                    break;
                case Direction.Right:
                    result = Direction.UpRight;
                    break;
                case Direction.DownRight:
                    result = Direction.Right;
                    break;
                case Direction.Down:
                    result = Direction.DownRight;
                    break;
                case Direction.DownLeft:
                    result = Direction.Down;
                    break;
                case Direction.Left:
                    result = Direction.DownLeft;
                    break;
                case Direction.UpLeft:
                    result = Direction.Left;
                    break;
                default:
                    throw new ArgumentException("Unknown direction value");
            }

            return result;
        }

        /// <summary>
        /// Rotates direction clockwise by 90 degrees.
        /// </summary>
        public static Direction Rotate90CW(this Direction direction)
        {
            Direction result;
            switch (direction)
            {
                case Direction.None:
                    result = Direction.None;
                    break;
                case Direction.Up:
                    result = Direction.Right;
                    break;
                case Direction.UpRight:
                    result = Direction.DownRight;
                    break;
                case Direction.Right:
                    result = Direction.Down;
                    break;
                case Direction.DownRight:
                    result = Direction.DownLeft;
                    break;
                case Direction.Down:
                    result = Direction.Left;
                    break;
                case Direction.DownLeft:
                    result = Direction.UpLeft;
                    break;
                case Direction.Left:
                    result = Direction.Up;
                    break;
                case Direction.UpLeft:
                    result = Direction.UpRight;
                    break;
                default:
                    throw new ArgumentException("Unknown direction value");
            }

            return result;
        }

        /// <summary>
        /// Rotates direction counterclockwise by 90 degrees.
        /// </summary>
        public static Direction Rotate90CCW(this Direction direction)
        {
            Direction result;
            switch (direction)
            {
                case Direction.None:
                    result = Direction.None;
                    break;
                case Direction.Up:
                    result = Direction.Left;
                    break;
                case Direction.UpRight:
                    result = Direction.UpLeft;
                    break;
                case Direction.Right:
                    result = Direction.Up;
                    break;
                case Direction.DownRight:
                    result = Direction.UpRight;
                    break;
                case Direction.Down:
                    result = Direction.Right;
                    break;
                case Direction.DownLeft:
                    result = Direction.DownRight;
                    break;
                case Direction.Left:
                    result = Direction.Down;
                    break;
                case Direction.UpLeft:
                    result = Direction.DownLeft;
                    break;
                default:
                    throw new ArgumentException("Unknown direction value");
            }

            return result;
        }

        /// <summary>
        /// Returns enumerator that cyclically rotates direction clockwise by 90 degrees.
        /// </summary>
        public static IEnumerable<Direction> GetRotate90CWEnumerable(this Direction direction)
        {
            Direction current = direction;
            do
            {
                yield return current;
                current = current.Rotate90CW();
            }
            while (current != direction);
        }

        /// <summary>
        /// Returns enumerator that cyclically rotates direction counterclockwise by 90 degrees.
        /// </summary>
        public static IEnumerable<Direction> GetRotate90CCWEnumerable(this Direction direction)
        {
            Direction current = direction;
            do
            {
                yield return current;
                current = current.Rotate90CCW();
            }
            while (current != direction);
        }

        /// <summary>
        /// Converts direction to <see cref="Vector2Int"/> object. Each component of the returned vector can be -1, 0 or 1.
        /// </summary>
        public static Vector2Int GetOffset(this Direction direction)
        {
            return new Vector2Int(GetOffsetX(direction), GetOffsetY(direction));
        }

        /// <summary>
        /// Gets X offset of this direction. Can be -1, 0 or 1.
        /// </summary>
        public static int GetOffsetX(this Direction direction)
        {
            int result = 0;
            switch (direction)
            {
                case Direction.None:
                    break;
                case Direction.Left:
                case Direction.UpLeft:
                case Direction.DownLeft:
                    result = -1;
                    break;
                case Direction.Right:
                case Direction.UpRight:
                case Direction.DownRight:
                    result = 1;
                    break;
                case Direction.Up:
                case Direction.Down:
                    break;
                default:
                    throw new ArgumentException("Unknown direction value");
            }

            return result;
        }

        /// <summary>
        /// Gets Y offset of this direction. Can be -1, 0 or 1.
        /// </summary>
        public static int GetOffsetY(this Direction direction)
        {
            int result = 0;
            switch (direction)
            {
                case Direction.None:
                    break;
                case Direction.Down:
                case Direction.DownRight:
                case Direction.DownLeft:
                    result = -1;
                    break;
                case Direction.Up:
                case Direction.UpRight:
                case Direction.UpLeft:
                    result = 1;
                    break;
                case Direction.Left:
                case Direction.Right:
                    break;
                default:
                    throw new ArgumentException("Unknown direction value");
            }

            return result;
        }

        public static Direction GetDirection(this Vector2Int self)
        {
            var current = Direction.Up;
            do
            {
                if (self == current.GetOffset())
                {
                    return current;
                }

                current = current.RotateCW();
            }
            while (current != Direction.Up);

            throw new ArgumentException($"Specified vector can't be converted into direction: {self}");
        }

        public static Direction CalculateNearestAxis(this Vector2 direction, float detectionAngle)
        {
            Direction dir = Direction.Up;
            do
            {
                if (Vector2.Angle(direction, dir.GetOffsetX() * Vector2.right + dir.GetOffsetY() * Vector2.up) <=
                    detectionAngle)
                {
                    return dir;
                }

                dir = dir.RotateCW().RotateCW();
            }
            while (dir != Direction.Up);

            return Direction.None;
        }
    }
}