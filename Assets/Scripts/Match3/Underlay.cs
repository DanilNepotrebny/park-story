﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Linq;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Encapsulates common functionality of a match-3 chip.
    /// </summary>
    [DisallowMultipleComponent]
    public partial class Underlay : MonoBehaviour
    {
        [SerializeField] private int _health = 1;
        [SerializeField, EnumFlag] private ImpactFlag _impactFlags;

        [SerializeField, Tooltip("Chips, whose impact will allow to receive impact by underlay")]
        private ChipTagSet _allowedChipTags;

        [Inject] private UnderlayPresenter _presenter;

        public UnderlayPresenter Presenter => _presenter;

        /// <summary>
        /// Current health of this underlay.
        /// </summary>
        public int Health
        {
            get { return _health; }
            private set
            {
                if (_health != value)
                {
                    _health = value;
                    HealthChanged?.Invoke(value);

                    if (_health == 0)
                    {
                        using (var handle = new DeferredInvocation(OnDestroyInvokationUnlocked).Start())
                        {
                            Destroying?.Invoke(handle);
                        }
                    }
                }
            }
        }

        public event Action<int> HealthChanged;
        public event Action<IDeferredInvocationHandle> Destroying;
        public event Action<Underlay> Destroyed;

        public void OnCellImpacting(int delta, ImpactFlag flag, [CanBeNull] ChipComponent chip)
        {
            if ((_impactFlags & flag) != flag)
            {
                return;
            }

            if (chip != null)
            {
                bool acceptImpact = false;
                while (chip != null)
                {
                    acceptImpact = _allowedChipTags.Any(chipTag => chip.Tags.Has(chipTag, false));
                    if (acceptImpact)
                    {
                        break;
                    }

                    chip = chip.Child;
                }

                if (!acceptImpact)
                {
                    return;
                }
            }

            Health = Mathf.Max(Health + delta, 0);
        }

        private void OnDestroyInvokationUnlocked()
        {
            Destroyed?.Invoke(this);
            gameObject.Dispose();
        }
    }
}