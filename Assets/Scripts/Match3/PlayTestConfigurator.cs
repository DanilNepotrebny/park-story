﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.Match3.Levels;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Sets game options for playtests build.
    /// </summary>
    public class PlayTestConfigurator : MonoBehaviour
    {
        [SerializeField] private float _timeScale;

        [Header("Moves")]
        [SerializeField] private CountedItem _moves;
        [SerializeField] private int _additionalMovesCount;
        [SerializeField] private LevelSettings[] _levelsToIgnore;

        #if PLAYTESTS

        [Inject] private ICurrentLevelInfo _currentLevelInfo;

        protected void Start()
        {
            Time.timeScale = _timeScale;

            if (!_levelsToIgnore.Contains(_currentLevelInfo.CurrentLevel))
            {
                _moves.TryReceive(_additionalMovesCount);
            }
        }

        protected void OnDestroy()
        {
            Time.timeScale = 1f;
        }

        #else

        protected void Awake()
        {
            gameObject.Dispose();
        }

        #endif
    }
}