﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using DreamTeam.Utils.Movement;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3
{
    public class ShuffleFlightPresenter : MonoBehaviour
    {
        [SerializeField, SortingLayer] private int _viewFromSortingLayer;
        [SerializeField, SortingLayer] private int _viewToSortingLayer;
        [SerializeField] private string _chipPresenterTriggerName = "OnShuffleFlight";
        [SerializeField] private string _chipIdleStateName = "Idle";

        private ChipPresenter _originPresenter;
        private ChipPresenter _clonedPresenterFrom;
        private ChipPresenter _clonedPresenterTo;
        private MaterialPropertyBlock _propertyBlock;
        private int _colorPropertyId = Shader.PropertyToID("_Color");

        public MovementSequence MovementSequence { get; private set; }

        public void Move(
            Vector3 positionFrom,
            ChipComponent chipFrom,
            Vector3 positionTo,
            ChipComponent chipTo,
            ref int sortingOrder)
        {
            _clonedPresenterFrom = chipFrom.CloneView();
            Assert.IsNotNull(_clonedPresenterFrom, $"Missing presenter of {chipFrom.name} in {chipFrom.Cell.name}");

            _clonedPresenterTo = chipTo.CloneView();
            Assert.IsNotNull(_clonedPresenterTo, $"Missing presenter of {chipTo.name} in {chipTo.Cell.name}");

            _clonedPresenterFrom.SetTrigger(_chipPresenterTriggerName);
            _clonedPresenterFrom.AddAnimationStateEnterHandler(_chipIdleStateName, OnProjectileChipEnteredIdleState);
            _clonedPresenterFrom.SortingOptions.PushSortingLayer(_viewFromSortingLayer);
            _clonedPresenterFrom.SortingOptions.PushSortingOrder(sortingOrder++);
            _clonedPresenterFrom.ResetPosition();
            _clonedPresenterFrom.transform.SetParent(transform, false);

            _originPresenter = chipFrom.Presenter;

            _clonedPresenterTo.SortingOptions.PushSortingLayer(_viewToSortingLayer);
            _clonedPresenterTo.SortingOptions.PushSortingOrder(sortingOrder++);
            _clonedPresenterTo.ResetPosition();
            _clonedPresenterTo.transform.SetParent(transform, false);
            _clonedPresenterTo.ChipAnimatorController.SetAnimatorEnabled(false);

            MovementSequence.Updated += OnMovementUpdated;
            MovementSequence.Finished += OnMovementFinished;
            UpdateViewByProgress(0);

            MovementSequence.Move(positionFrom, positionTo);
        }

        private void OnProjectileChipEnteredIdleState()
        {
            _clonedPresenterFrom.ChipAnimatorController.SetAnimatorEnabled(false);
            _originPresenter.gameObject.SetActive(false);
        }

        protected void Awake()
        {
            MovementSequence = GetComponent<MovementSequence>();
            _propertyBlock = new MaterialPropertyBlock();
        }

        protected void OnDestroy()
        {
            _clonedPresenterFrom.gameObject.Dispose();
            _clonedPresenterTo.gameObject.Dispose();
        }

        private void OnMovementUpdated(MovementSequence movementSequence, float normalizedProgress)
        {
            UpdateViewByProgress(normalizedProgress);
        }

        private void OnMovementFinished(MovementSequence obj)
        {
            if (_originPresenter != null)
            {
                _originPresenter.gameObject.SetActive(true);
                _originPresenter.SetTrigger(_chipPresenterTriggerName);
                _originPresenter.AddAnimationStateEnterHandler(_chipIdleStateName, OnOriginChipEnteredIdleState);
            }
        }

        private void OnOriginChipEnteredIdleState()
        {
            _originPresenter.RemoveAnimationStateEnterHandler(_chipIdleStateName, OnOriginChipEnteredIdleState);
            gameObject.Dispose();
        }

        private void UpdateViewByProgress(float ratio)
        {
            SetViewAlpha(_clonedPresenterFrom.gameObject, 1.0f - ratio);
            SetViewAlpha(_clonedPresenterTo.gameObject, ratio);
        }

        private void SetViewAlpha(GameObject view, float alpha)
        {
            Renderer[] renderers = view.GetComponentsInChildren<Renderer>();
            foreach (Renderer r in renderers)
            {
                r.GetPropertyBlock(_propertyBlock);
                Color color = r.sharedMaterial.color;
                color.a = alpha;
                _propertyBlock.SetColor(_colorPropertyId, color);
                r.SetPropertyBlock(_propertyBlock);
            }
        }
    }
}
