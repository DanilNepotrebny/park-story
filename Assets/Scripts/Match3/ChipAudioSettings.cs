﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Audio;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    [CreateAssetMenu]
    public class ChipAudioSettings : ScriptableObject
    {
        [SerializeField] private AudioEventsList _audioEvents;
        [Space, SerializeField] private ChipAudioSettings _fallbackSettings;

        public ICollection<string> AudioEvents => _audioEvents.Keys;

        public AudioSource GetAudioSource(string eventName)
        {
            IEnumerator<AudioSource> provider = _audioEvents.FindFirstOrDefault(eventName);

            if (provider != null)
            {
                if (!provider.MoveNext())
                {
                    provider.Reset();
                }

                return provider.Current;
            }

            return _fallbackSettings?.GetAudioSource(eventName);
        }

        [Serializable]
        private class AudioEventsList : KeyValueList<string, AudioSourceProvider>
        {
        }
    }
}