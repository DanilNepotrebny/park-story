﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Match3.Levels;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.Requirements
{
    [RequireComponent(typeof(ChipComponent))]
    public class ConsumeOnGumballRequirementReached : MonoBehaviour
    {
        [Inject] private ImpactSystem _impactSystem;

        [Inject(Id = LevelSettings.GoalInstancesId)]
        private IReadOnlyList<Requirement.Instance> _goalInstances;

        [SerializeField, EnumFlag] private ImpactFlag _impactFlags;

        private ChipComponent _chip;

        protected void Start()
        {
            foreach (Requirement.Instance instance in _goalInstances)
            {
                if (instance.BaseRequirement.GetType() == typeof(GumballConsumptionRequirement))
                {
                    instance.Reached += OnRequirementReached;
                }
            }

            _chip = GetComponent<ChipComponent>();
        }

        private void OnRequirementReached()
        {
            if (_chip.Cell == null)
            {
                return;
            }

            _impactSystem
                .ScheduleImpact(_chip.Cell, -_chip.Health, _impactFlags)
                .Perform();
        }

        protected void OnDestroy()
        {
            foreach (Requirement.Instance instance in _goalInstances)
            {
                if (instance.BaseRequirement.GetType() == typeof(GumballConsumptionRequirement))
                {
                    instance.Reached -= OnRequirementReached;
                }
            }
        }
    }
}