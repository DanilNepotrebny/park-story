﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.ChipTags;
using Zenject;

namespace DreamTeam.Match3.Requirements
{
    public class ChipCleanupRequirementImpactController : BaseGoalChipConsumingImpactController
    {
        private readonly ChipCleanupRequirement.Instance _requirement;

        protected override Requirement.Instance Requirement => _requirement;
        protected override ChipTag RequiredChipTag => _requirement.RequiredChipTag;

        public ChipCleanupRequirementImpactController(ChipCleanupRequirement.Instance requirement)
        {
            _requirement = requirement;
        }

        protected override void OnChipConsuming()
        {
            _requirement.OnChipConsuming();
        }

        protected override void OnChipConsumed()
        {
            _requirement.OnChipConsumed();
        }

        #region Inner types

        public class Factory : Factory<ChipCleanupRequirement.Instance, ChipCleanupRequirementImpactController> {}

        #endregion Inner types
    }
}
