﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3.Requirements
{
    public abstract partial class Requirement
    {
        public abstract partial class Instance : Instance.ICheatApi
        {
            protected abstract void SetReached();

            #region Instance.ICheatApi

            void ICheatApi.SetReached()
            {
                SetReached();
            }

            #endregion

            #region Inner types

            public interface ICheatApi
            {
                void SetReached();
            }

            #endregion
        }
    }
}