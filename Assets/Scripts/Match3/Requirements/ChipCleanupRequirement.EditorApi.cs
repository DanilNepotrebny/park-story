﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.ChipTags;
using JetBrains.Annotations;

#if UNITY_EDITOR

namespace DreamTeam.Match3.Requirements
{
    public partial class ChipCleanupRequirement
    {
        public override void SetRequiredCountInEditor(int count)
        {
            _requiredCount = count;
        }

        public void SetRequiredChipInEditor([NotNull] ChipTag chip)
        {
            _requiredChip = chip;
        }
    }
}

#endif // UNITY_EDITOR
