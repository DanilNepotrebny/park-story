﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;

namespace DreamTeam.Match3.Requirements.ChipConsumption
{
    public class ChipConsumptionGoal : ChipConsumptionRequirement
    {
        protected override Type InstanceType => typeof(Instance);

        public new class Instance : ChipConsumptionRequirement.Instance
        {
            protected Instance(ChipConsumptionGoal requirement)
                : base(requirement)
            {
            }
        }
    }
}