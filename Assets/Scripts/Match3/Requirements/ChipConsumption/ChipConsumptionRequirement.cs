﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.Requirements.ChipConsumption
{
    /// <summary>
    /// Requirement that requires defined count of consumptions of specific chip type.
    /// </summary>
    public partial class ChipConsumptionRequirement : CountedRequirement
    {
        [SerializeField] private int _requiredCount;
        [SerializeField] private ChipTag _requiredChipTag;

        public override int RequiredCount => _requiredCount;
        public ChipTag RequiredChipTag => _requiredChipTag;

        protected override Type InstanceType => typeof(Instance);

        /// <summary>
        /// Instance for Consumption Requirement.
        /// </summary>
        public new class Instance : CountedRequirement.Instance
        {
            [Inject] private GridSystem _grid;
            [Inject] private Instantiator _instantiator;

            private readonly ChipConsumptionRequirement _requirement;
            private ConsumptionRequirementImpactController.Factory _impactControllerFactory;
            private List<ChipComponent> _reservedChips = new List<ChipComponent>(8);
            private Queue<IDeferredInvocationHandle> _counterChangeHandles = new Queue<IDeferredInvocationHandle>();

            public override int RequiredCount => _requirement.RequiredCount;
            public ChipTag RequiredChipTag => _requirement.RequiredChipTag;

            public void OnChipConsuming()
            {
                CounterChanging += OnCounterChangeOnChipConsume;
                Counter += 1;
                CounterChanging -= OnCounterChangeOnChipConsume;
            }

            public void OnChipConsumed()
            {
                _counterChangeHandles.Dequeue().Unlock();
            }

            protected Instance(ChipConsumptionRequirement requirement)
                : base(requirement)
            {
                _requirement = requirement;

                Assert.IsNotNull(_requirement?._requiredChipTag);
            }

            [Inject]
            private void Construct(
                GenerationSystem generationSystem,
                ConsumptionRequirementImpactController.Factory impactControllerFactory)
            {
                _impactControllerFactory = impactControllerFactory;

                generationSystem.ChipGenerated += OnChipGenerated;
            }

            protected override RequirementHelper FindHelperChild()
            {
                var helpers = new HashSet<RequirementHelper>
                {
                    FindHelperOnGrid(TryGetHelperPrefab),
                    FindHelperOnGrid(TryGetChipProducerHelperPrefab)
                };
                helpers.RemoveWhere(h => h == null);

                CompositeRequirementHelper resultHelper = null;
                if (helpers.Count > 0)
                {
                    resultHelper = _instantiator.InstantiateComponentOnNewGameObject<CompositeRequirementHelper>();
                    resultHelper.Construct(helpers);
                }

                return resultHelper;
            }

            private void OnCounterChangeOnChipConsume(int newValue, IDeferredInvocationHandle commitChangeHandle)
            {
                _counterChangeHandles.Enqueue(commitChangeHandle.Lock());
            }

            private RequirementHelper TryGetHelperPrefab([NotNull] ChipComponent chip)
            {
                if (IsRequirementChip(chip))
                {
                    if (chip.Helper != null)
                    {
                        return chip.Helper;
                    }

                    UnityEngine.Debug.LogAssertion($"Missing helper in required {chip.name}");
                }

                return null;
            }

            private RequirementHelper TryGetChipProducerHelperPrefab([NotNull] ChipComponent chip)
            {
                var producer = chip.GetComponent<ChipProducerByImpact>();
                if (producer != null && producer.ProducingChipTag == _requirement.RequiredChipTag)
                {
                    return producer.HelperPrefab;
                }

                return null;
            }

            private RequirementHelper FindHelperOnGrid([NotNull] HelperLookUpDelegate @delegate)
            {
                foreach (CellComponent cell in _grid.Cells)
                {
                    ChipComponent chip = cell.Chip;

                    while (chip != null)
                    {
                        RequirementHelper helper = @delegate.Invoke(chip);
                        if (helper != null)
                        {
                            return helper;
                        }

                        chip = chip.Child;
                    }
                }

                return null;
            }

            private void OnChipGenerated([NotNull] ChipComponent chip)
            {
                if (!IsRequirementChip(chip))
                {
                    return;
                }

                ConsumptionRequirementImpactController controller = _impactControllerFactory.Create(this);
                chip.AddImpactController(controller);
                if (!IsReached)
                {
                    ReserveChip(chip);
                }
            }

            private bool IsRequirementChip([NotNull] ChipComponent chip)
            {
                return chip.Tags.Has(_requirement._requiredChipTag, false);
            }

            private void ReserveChip(ChipComponent chip)
            {
                if (chip.CanBeImpacted(ImpactFlag.Replace))
                {
                    chip.RemoveImpactFlags(ImpactFlag.Replace);
                    _reservedChips.Add(chip);
                }
            }

            #region Inner types

            private delegate RequirementHelper HelperLookUpDelegate([NotNull] ChipComponent chip);

            #endregion
        }
    }
}