﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.ChipTags;
using Zenject;

namespace DreamTeam.Match3.Requirements.ChipConsumption
{
    public class ConsumptionRequirementImpactController : BaseGoalChipConsumingImpactController
    {
        private readonly ChipConsumptionRequirement.Instance _requirement;

        protected override Requirement.Instance Requirement => _requirement;
        protected override ChipTag RequiredChipTag => _requirement.RequiredChipTag;

        public ConsumptionRequirementImpactController(ChipConsumptionRequirement.Instance requirement)
        {
            _requirement = requirement;
        }

        protected override void OnChipConsuming()
        {
            _requirement.OnChipConsuming();
        }

        protected override void OnChipConsumed()
        {
            _requirement.OnChipConsumed();
        }

        #region Inner types

        public class Factory : Factory<ChipConsumptionRequirement.Instance, ConsumptionRequirementImpactController> {}

        #endregion Inner types
    }
}
