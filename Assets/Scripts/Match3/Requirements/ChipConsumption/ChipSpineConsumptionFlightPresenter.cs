﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Spine.Unity;
using UnityEngine;

namespace DreamTeam.Match3.Requirements.ChipConsumption
{
    [RequireComponent(typeof(SkeletonAnimation))]
    public class ChipSpineConsumptionFlightPresenter : MonoBehaviour
    {
        [SerializeField] private AnimationReferenceAsset _flightAnimation;
        [SerializeField] private string _flightState;

        private ChipPresenter _chipPresenter;

        protected void Awake()
        {
            _chipPresenter = GetComponentInParent<ChipPresenter>();
            _chipPresenter.AddAnimationStateEnterHandler(_flightState, OnFlightBegan);
        }

        private void OnFlightBegan()
        {
            GetComponent<SkeletonAnimation>().AnimationState.SetAnimation(
                    0,
                    _flightAnimation.Animation,
                    true
                );
        }
    }
}