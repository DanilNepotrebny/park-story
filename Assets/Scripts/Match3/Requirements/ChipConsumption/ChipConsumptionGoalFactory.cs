﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.Requirements.ChipConsumption
{
    [CreateAssetMenu(fileName = "ChipConsumptionGoalFactory", menuName = "Match 3/Chip Consumption Goal Factory")]
    public class ChipConsumptionGoalFactory : GameObjectFactory<ChipTag, ChipConsumptionGoalFactory.ModelRefType,
        ChipConsumptionGoalFactory.PresentersMap>
    {
        [Serializable]
        public class ModelRefType : TypeReference<ChipTag>
        {
        }

        // TODO: Make requirement attribute to ensure that gameobject has ConsumptionRequirementFlightPresenter component
        [Serializable]
        public class PresentersMap : KeyValueList<ModelRefType, GameObject>
        {
        }
    }
}