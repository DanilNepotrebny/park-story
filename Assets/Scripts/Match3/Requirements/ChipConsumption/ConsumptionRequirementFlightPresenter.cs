﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.UI.HUD;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Movement;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace DreamTeam.Match3.Requirements.ChipConsumption
{
    [RequireComponent(typeof(MovementSequence))]
    public class ConsumptionRequirementFlightPresenter : MonoBehaviour, IFieldDestabilizer
    {
        [SerializeField] private string _chipStateTrigger = "OnConsumeFlight";
        [SerializeField] private string _goalFlightState = "ConsumeFlight";
        [SerializeField, SortingLayer] private int _chipSortingLayer;

        [SerializeField] private ActivatingEvent _activating;

        [Inject] private GoalInstancesPanel _panel;

        private IDeferredInvocationHandle _impactEndHandle;
        private Requirement.Instance _requirement;
        private Action _sequenceFinishedHandler;
        private ChipPresenter _view;
        private Vector3 _movementStartPos;
        private Vector3 _viewInitialOffset;
        private MovementSequence _sequence;

        #region IFieldDestabilizer

        public bool IsStable { get; private set; }

        public event Action Stabilized;
        public event Action Destabilized;

        #endregion IFieldDestabilizer

        public void Activate(
                ChipComponent chip,
                IDeferredInvocationHandle impactEndHandle,
                Requirement.Instance requirement,
                Action sequenceFinishedHandler
            )
        {
            _impactEndHandle = impactEndHandle.Lock();
            _requirement = requirement;
            _sequenceFinishedHandler = sequenceFinishedHandler;

            _view = chip.DetachView();
            _movementStartPos = _view.transform.position;
            transform.position = _movementStartPos;

            _view.SortingOptions.PushSortingLayer(_chipSortingLayer);

            _activating?.Invoke(_view);

            IsStable = false;
            Destabilized?.Invoke();

            if (_chipStateTrigger != string.Empty)
            {
                _view.AddAnimationStateEnterHandler(_goalFlightState, OnFlightBegan);
                _view.SetTrigger(_chipStateTrigger);
            }
            else
            {
                OnFlightBegan();
            }
        }

        protected void Awake()
        {
            _sequence = GetComponent<MovementSequence>();
        }

        private void OnFlightBegan()
        {
            _impactEndHandle.Unlock();
            _impactEndHandle = null;

            _view.transform.SetParent(transform, true);
            _view.ChipAnimatorController.SetAnimatorEnabled(false);

            var presenter = _panel.FindRootPresenter(_requirement);
            if (presenter != null)
            {
                _view.transform.localPosition = Vector3.zero;
                _viewInitialOffset = _view.ChipAnimatorController.transform.localPosition;
                _sequence.Finished += OnFlightEnded;
                _sequence.Updated += OnFlightUpdated;
                _sequence.Move(_movementStartPos, presenter.ProjectileOrigin.position, true);
            }
            else
            {
                EndSequence();
            }
        }

        private void OnFlightUpdated(MovementSequence mover, float ratio)
        {
            _view.SetAnimationNormalizedTime(ratio);
            _view.ChipAnimatorController.transform.localPosition =
                Vector3.Lerp(_viewInitialOffset, Vector3.zero, ratio);
        }

        private void OnFlightEnded(MovementSequence movementSequence)
        {
            EndSequence();
        }

        private void EndSequence()
        {
            _sequenceFinishedHandler?.Invoke();
            IsStable = true;
            Stabilized?.Invoke();
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }

        #region Inner types

        [Serializable]
        public class ActivatingEvent : UnityEvent<ChipPresenter>
        {
        }

        #endregion Inner types
    }
}