﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3.Requirements
{
    /// <summary>
    /// Goal that requires defined count of consumptions of specific chip type.
    /// </summary>
    public partial class HiddenObjectRequirement : CountedRequirement
    {
        [SerializeField, ReadOnly, Tooltip("Driven by Match3 Editor. So edit appropriate scene to change it.")]
        private int _requiredCount;

        public override int RequiredCount => _requiredCount;

        protected override Type InstanceType => typeof(Instance);

        /// <summary>
        /// Instance for Consumption Goal.
        /// </summary>
        public new class Instance : CountedRequirement.Instance
        {
            private Queue<IDeferredInvocationHandle> _counterChangeHandles = new Queue<IDeferredInvocationHandle>();

            public override int RequiredCount { get; }

            public event ReleasingDelegate Releasing;

            public Instance(CountedRequirement requirement, List<HiddenObject> hiddenObjects)
                : base(requirement)
            {
                AssertRequiredHiddenObjectsCount(requirement, hiddenObjects.Count);

                foreach (HiddenObject hiddenObject in hiddenObjects)
                {
                    hiddenObject.Released += OnHiddenObjectReleased;
                }

                RequiredCount = hiddenObjects.Count;
            }

            private void OnHiddenObjectReleased(HiddenObject hiddenObject, IDeferredInvocationHandle releasedHandle)
            {
                Assert.IsFalse(IsReached);

                CounterChanging += OnCounterChanging;
                Counter += 1;
                CounterChanging -= OnCounterChanging;

                releasedHandle = releasedHandle.Lock();

                var releasedPresentationInvocation = new DeferredInvocation(
                        () =>
                        {
                            releasedHandle.Unlock();
                            _counterChangeHandles.Dequeue().Unlock();
                        }
                    );

                using (IDeferredInvocationHandle handle = releasedPresentationInvocation.Start())
                {
                    Releasing?.Invoke(hiddenObject, handle);
                }
            }

            private void OnCounterChanging(int valueChangeDiff, IDeferredInvocationHandle commitChangeHandle)
            {
                _counterChangeHandles.Enqueue(commitChangeHandle.Lock());
            }

            protected override RequirementHelper FindHelperChild()
            {
                return null; // hidden object helper is the default one
            }

            private void AssertRequiredHiddenObjectsCount(CountedRequirement requirement, int objectsCountOnScene)
            {
                Assert.IsTrue(objectsCountOnScene != 0, "There is no Hidden Objects on the Level.");

                if (requirement.RequiredCount != objectsCountOnScene)
                {
                    UnityEngine.Debug.LogError(
                            $"{requirement.name}.{nameof(RequiredCount)} is different from {nameof(HiddenObject)}s count on scene."
                        );

                    #if UNITY_EDITOR
                    {
                        UnityEngine.Debug.LogError(
                                $"Fixing {requirement.name}.{nameof(RequiredCount)} to be same as {nameof(HiddenObject)}s count on scene."
                            );

                        (requirement as IEditorApi).SetRequiredCountInEditor(objectsCountOnScene);
                    }
                    #endif // UNITY_EDITOR
                }
            }

            #region Inner types

            public delegate void ReleasingDelegate(HiddenObject hiddenObject, IDeferredInvocationHandle commitCounterChangeHandle);

            #endregion Inner types
        }
    }
}