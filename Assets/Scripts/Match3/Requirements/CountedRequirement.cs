﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Match3.Requirements
{
    /// <summary>
    /// Requirement that requires defined count of consumptions of specific chip type.
    /// </summary>
    public abstract partial class CountedRequirement : Requirement
    {
        public abstract int RequiredCount { get; }

        /// <summary>
        /// Instance for Consumption Requirement.
        /// </summary>
        public new abstract partial class Instance : Requirement.Instance
        {
            private int _counter;

            public override bool IsReached => (Counter >= RequiredCount);

            public override bool IsStable => DeferredChangesCounter == 0;

            public int DeferredChangesCounter { get; private set; }

            protected override float NormalizedProgress
            {
                get
                {
                    float progress = (float)(Counter + DeferredChangesCounter) / RequiredCount;
                    return Mathf.Min(progress, 1f);
                }
            }

            public event CounterChangeDelegate CounterChanging;

            /// <summary>
            /// Current progress of reaching the requirement.
            /// </summary>
            public int Counter
            {
                get { return _counter; }
                protected set
                {
                    if (_counter == value)
                    {
                        return;
                    }

                    int diff = value - _counter;
                    var di = new DeferredInvocation(() => InvokeDeferredCounterChange(diff));
                    using (var handle = di.Start())
                    {
                        DeferredChangesCounter += 1;
                        CounterChanging?.Invoke(value, handle);
                    }
                }
            }

            /// <summary>
            /// Required count to reach the requirement.
            /// </summary>
            public abstract int RequiredCount { get; }

            protected void CommitCounterChange(int valueChangeDiff)
            {
                _counter += valueChangeDiff;

                OnChanged();
            }

            private void InvokeDeferredCounterChange(int valueChangeDiff)
            {
                DeferredChangesCounter -= 1;
                CommitCounterChange(valueChangeDiff);
            }

            #region Inner types

            public delegate void CounterChangeDelegate(
                int valueChangeDiff,
                [NotNull] IDeferredInvocationHandle commitChangeHandle);

            protected Instance(CountedRequirement requirement)
                : base(requirement)
            {
            }

            #endregion Inner types
        }
    }
}