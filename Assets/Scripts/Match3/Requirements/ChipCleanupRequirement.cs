﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.Requirements
{
    public partial class ChipCleanupRequirement : CountedRequirement
    {
        [SerializeField, ReadOnly] private int _requiredCount;
        [SerializeField] private ChipTag _requiredChip;

        public ChipTag RequiredChip => _requiredChip;
        public override int RequiredCount => _requiredCount;

        protected override Type InstanceType => typeof(Instance);

        public new class Instance : CountedRequirement.Instance
        {
            [Inject] private LevelStartNotifier _levelStartNotifier;

            private GenerationSystem _generationSystem;
            private readonly ChipCleanupRequirement _requirement;
            private ChipCleanupRequirementImpactController.Factory _impactControllerFactory;
            private Queue<IDeferredInvocationHandle> _counterChangeHandles = new Queue<IDeferredInvocationHandle>();
            private int _requiredChipsOnStart;

            public override int RequiredCount => 0;
            public ChipTag RequiredChipTag => _requirement.RequiredChip;

            public override bool IsReached => Counter == RequiredCount;

            protected override float NormalizedProgress
            {
                get
                {
                    Assert.IsTrue(_requiredChipsOnStart > 0, $"There are no required chips to cleanup at level start");
                    float progress = (float)(Counter - DeferredChangesCounter) / _requiredChipsOnStart;
                    return progress; // might be bigger than 1
                }
            }

            public Instance([NotNull] ChipCleanupRequirement requirement) :
                base(requirement)
            {
                _requirement = requirement;
                Assert.IsNotNull(_requirement.RequiredChip);
            }

            [Inject]
            private void Construct(
                GenerationSystem generationSystem,
                ChipCleanupRequirementImpactController.Factory impactControllerFactory)
            {
                _generationSystem = generationSystem;
                _impactControllerFactory = impactControllerFactory;

                generationSystem.ChipGenerated += OnChipGenerated;
            }

            public void OnChipConsuming()
            {
                CounterChanging += OnCounterChangeOnChipConsume;
                Counter -= 1;
                CounterChanging -= OnCounterChangeOnChipConsume;
            }

            public void OnChipConsumed()
            {
                _counterChangeHandles.Dequeue().Unlock();
            }

            protected override RequirementHelper FindHelperChild()
            {
                return null; // it's okay to use default one
            }

            private void OnCounterChangeOnChipConsume(int newValue, IDeferredInvocationHandle commitChangeHandle)
            {
                _counterChangeHandles.Enqueue(commitChangeHandle.Lock());
            }

            private void OnChipGenerated([NotNull] ChipComponent chip)
            {
                if (!IsRequirementChip(chip))
                {
                    return;
                }

                ChipCleanupRequirementImpactController controller = _impactControllerFactory.Create(this);
                chip.AddImpactController(controller);

                SetRequiredCount(Counter + 1);

                if (!_levelStartNotifier.IsLevelStarted)
                {
                    _requiredChipsOnStart += 1;
                }
            }

            private void SetRequiredCount(int amount)
            {
                int value = Mathf.Max(0, amount);
                if (Counter == value)
                {
                    return;
                }

                Counter = value;
                OnChanged();

                if (IsReached)
                {
                    _generationSystem.ChipGenerated -= OnChipGenerated;
                }
            }

            private bool IsRequirementChip([NotNull] ChipComponent chip)
            {
                return chip.Tags.Has(_requirement.RequiredChip, false);
            }
        }
    }
}
