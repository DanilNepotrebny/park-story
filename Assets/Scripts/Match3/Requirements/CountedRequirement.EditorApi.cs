﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using JetBrains.Annotations;

#if UNITY_EDITOR

namespace DreamTeam.Match3.Requirements
{
    public abstract partial class CountedRequirement : CountedRequirement.IEditorApi
    {
        public abstract void SetRequiredCountInEditor(int count);

        public void SetDefaultHelperInEditor([NotNull] RequirementHelper _helperPrefab)
        {
            _defaultHelperPrefab = _helperPrefab;
        }

        #region Inner types

        public interface IEditorApi
        {
            void SetRequiredCountInEditor(int count);

            void SetDefaultHelperInEditor([NotNull] RequirementHelper helperPrefab);
        }

        #endregion Inner types
    }
}

#endif // UNITY_EDITOR
