﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Debug;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.Requirements.BonusCounters;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.Requirements
{
    /// <summary>
    /// Requirement that requires defined count of turns being made by a player.
    /// </summary>
    public partial class MovesRequirement : CountedRequirement
    {
        [SerializeField] private int _requiredTurns = 10;
        [SerializeField] private int _rewardBonusPrice = 1;

        public override int RequiredCount => _requiredTurns;
        public int RewardBonusPrice => _rewardBonusPrice;

        protected override Type InstanceType => typeof(Instance);

        /// <summary>
        /// Instance for Turns Requirement.
        /// </summary>
        public new class Instance : CountedRequirement.Instance, IBonusCounterProvider
        {
            [Inject] private GenerationSystem _generationSystem;

            private readonly MovesRequirement _requirement;
            private MovesSystem _movesSystem;
            private CheatPanel _cheats;
            private CountedItem _movesItem;
            private MovesRequirementBonusCounter _bonusCounter;

            public override int RequiredCount => _requirement.RequiredCount;
            public int RemainingMoves => RequiredCount - Counter;

            public IBonusCounter BonusCounter
            {
                get
                {
                    if (_bonusCounter == null)
                    {
                        _bonusCounter = new MovesRequirementBonusCounter(this, _requirement.RewardBonusPrice);
                    }

                    return _bonusCounter;
                }
            }

            public override void Dispose()
            {
                if (_movesItem != null)
                {
                    _movesItem.CountChanged -= OnMovesItemValueChanged;
                }

                if (_cheats != null)
                {
                    _cheats.RemoveDrawer(DrawCheatGUI);
                }

                _bonusCounter?.Dispose();
            }

            private Instance(MovesRequirement requirement)
                : base(requirement)
            {
                _requirement = requirement;

                Assert.IsNotNull(_requirement);
                Assert.IsTrue(_requirement.RequiredCount > 0);
            }

            [Inject]
            private void Construct(
                MovesSystem movesSystem,
                [Inject(Id = GlobalInstaller.MovesId)] CountedItem movesItem,
                CheatPanel cheats)
            {
                _movesSystem = movesSystem;
                _movesSystem.MoveSucceed += OnMoveSucceed;

                _movesItem = movesItem;
                _movesItem.CountChanged += OnMovesItemValueChanged;

                _cheats = cheats;
                _cheats.AddDrawer(CheatPanel.Category.Match3, DrawCheatGUI);
            }

            protected override RequirementHelper FindHelperChild()
            {
                throw new NotImplementedException(); // it was not intended to have a helper for moves requirement
            }

            private void DrawCheatGUI(Rect obj)
            {
                if (GUILayout.Button("Spend all moves"))
                {
                    Counter = RequiredCount;
                    _cheats.Hide();
                }
            }

            private void OnMovesItemValueChanged(int current, int previous)
            {
                if (current <= 0)
                {
                    return;
                }

                if (_movesItem.TrySpend(current, GamePlace.InGame))
                {
                    Counter -= current;
                }
            }

            private void OnMoveSucceed(List<Vector2Int> cellPositions)
            {
                Counter++;
            }
        }

        [Serializable]
        private class WeightedBonusPrefabs : KeyValueList<ChipComponent, int>
        {
        }
    }
}