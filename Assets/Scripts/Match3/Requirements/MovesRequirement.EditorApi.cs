﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

namespace DreamTeam.Match3.Requirements
{
    public partial class MovesRequirement
    {
        public override void SetRequiredCountInEditor(int count)
        {
            // nothing to do here
        }
    }
}

#endif // UNITY_EDITOR
