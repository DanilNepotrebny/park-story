﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3.Requirements
{
    public partial class CountedRequirement
    {
        public new partial class Instance
        {
            protected override void SetReached()
            {
                Counter = RequiredCount;
            }
        }
    }
}