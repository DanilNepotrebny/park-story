﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.Requirements.ChipConsumption;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.Requirements
{
    public abstract class BaseGoalChipConsumingImpactController : IImpactController
    {
        [Inject] private Instantiator _instantiator;
        [Inject] private ChipConsumptionGoalFactory _chipConsumptionFactory;

        protected abstract Requirement.Instance Requirement { get; }

        protected abstract ChipTag RequiredChipTag { get; }

        protected abstract void OnChipConsuming();

        protected abstract void OnChipConsumed();

        #region IImpactController

        void IImpactController.OnImpact(
            ChipComponent chip,
            ImpactFlag impactType,
            IDeferredInvocationHandle impactEndHandle,
            ref bool isModal)
        {
            if (chip.IsConsumed && !Requirement.IsReached)
            {
                OnChipConsuming();

                GameObject flightGo = _chipConsumptionFactory.Create(RequiredChipTag, _instantiator);
                if (flightGo != null)
                {
                    var flight = flightGo.GetComponent<ConsumptionRequirementFlightPresenter>();
                    flight.Activate(chip, impactEndHandle, Requirement, OnChipConsumed);
                    isModal = true;
                }
                else
                {
                    OnChipConsumed();
                }
            }
        }

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.GoalChipConsuming;
        }

        void IImpactController.Cancel()
        {
            // nothing to do
        }

        #endregion IImpactController
    }
}
