﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

namespace DreamTeam.Match3.Requirements
{
    public partial class HiddenObjectRequirement
    {
        public override void SetRequiredCountInEditor(int count)
        {
            _requiredCount = count;
        }
    }
}

#endif // UNITY_EDITOR
