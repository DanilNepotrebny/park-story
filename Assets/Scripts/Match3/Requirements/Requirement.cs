﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.Requirements
{
    /// <summary> Base class for all requirement scriptable objects. </summary>
    public abstract partial class Requirement : ScriptableObject
    {
        [SerializeField] protected RequirementHelper _defaultHelperPrefab;

        public RequirementHelper DefaultHelperPrefab => _defaultHelperPrefab;

        /// <summary> Returns type of the requirement instance that corresponds to the requirement. </summary>
        protected abstract Type InstanceType { get; }

        /// <summary> Creates requirement instance. </summary>
        public Instance CreateInstance(IInstantiator instantiator)
        {
            return (Instance)instantiator.Instantiate(InstanceType, new object[] { this });
        }
    }
}
