﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.Requirements.RequirementHelpers
{
    public class PopcornChipConsumptionRequirementHelper : ChipConsumptionRequirementHelper
    {
        [Inject] private GridSystem _grid;
        [Inject] private GravitySystem _gravitySystem;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        private ImpactFlag _impactType;
        private HashSet<CellComponent> _container;

        public override int FindCellsToImpact(
            ImpactFlag impactType,
            int cellsNumber,
            ref HashSet<CellComponent> container)
        {
            _impactType = impactType;
            _container = container;
            int foundCells = 0;

            var cellsWithRequirementChips = new List<CellComponent>();
            foreach (CellComponent cell in _grid.Cells)
            {
                if (cell.Chip == null)
                {
                    continue;
                }

                if (IsRequirementChip(cell.Chip))
                {
                    cellsWithRequirementChips.Add(cell);
                }
                else if (cell.Chip.Child != null &&
                    cell.Chip.IsChildVisible &&
                    IsRequirementChip(cell.Chip.Child) &&
                    cell.Chip.CanBeImpacted(_impactType)) // popcorn is in the container
                {
                    container.Add(cell);
                    foundCells += 1;

                    if (foundCells == cellsNumber)
                    {
                        return foundCells;
                    }
                }
            }

            cellsWithRequirementChips.RandomShuffle(_random);
            foreach (CellComponent cell in cellsWithRequirementChips)
            {
                if (_container.Contains(cell))
                {
                    continue;
                }

                foreach (CellComponent downCell in _gravitySystem.EnumerateDownstream(cell, true, true, false))
                {
                    if (downCell.Chip == null ||
                        IsRequirementChip(downCell.Chip))
                    {
                        continue;
                    }

                    CellComponent targetCell = CheckPossibleTarget(downCell.GridPosition);
                    if (targetCell != null)
                    {
                        container.Add(targetCell);
                        foundCells += 1;

                        if (foundCells == cellsNumber)
                        {
                            return foundCells;
                        }
                    }
                }
            }

            return foundCells;
        }

        private CellComponent CheckPossibleTarget(Vector2Int gridPos)
        {
            CellComponent cell = _grid.GetCell(gridPos);
            if (cell == null || _container.Contains(cell))
            {
                return null;
            }

            if (cell.Chip != null && cell.Chip.CanBeImpacted(_impactType))
            {
                return cell;
            }

            return null;
        }
    }
}