﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Match3.Requirements.RequirementHelpers
{
    public class ChipImpactRequirementHelper : RequirementHelper
    {
        [FormerlySerializedAs("_gumballMachineTag"), SerializeField]
        private ChipTag _chipTag;

        [Inject] private GridSystem _grid;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        public override void Initialize(Requirement requirement)
        {
        }

        public override int FindCellsToImpact(
            ImpactFlag impactType,
            int cellsNumber,
            ref HashSet<CellComponent> container)
        {
            var cells = new List<CellComponent>();
            foreach (CellComponent cell in _grid.Cells)
            {
                if (cell.Chip == null ||
                    container.Contains(cell) ||
                    !cell.Chip.CanBeImpacted(impactType))
                {
                    continue;
                }

                if (IsRequirementChip(cell.Chip))
                {
                    cells.Add(cell);
                }
                else if (cell.Chip.Child != null &&
                    IsRequirementChip(cell.Chip.Child) &&
                    cell.Chip.Child.CanBeImpacted(impactType))
                {
                    cells.Add(cell);
                }
            }

            int cellsAmount = cells.Count;
            for (int i = 0; i < cellsAmount; ++i)
            {
                CellComponent cell = cells[i];
                var neighbourImpactReceiver = cell.Chip.GetComponent<NeighbourImpactReceiver>();
                if (neighbourImpactReceiver != null)
                {
                    neighbourImpactReceiver.FillAffectingCells(cells, impactType);
                }
            }

            cells.RandomShuffle(_random);
            int foundCells = Math.Min(cells.Count, cellsNumber);
            for (int i = 0; i < foundCells; ++i)
            {
                container.Add(cells[i]);
            }

            return foundCells;
        }

        private bool IsRequirementChip([NotNull] ChipComponent chip)
        {
            return chip.Tags.Has(_chipTag);
        }
    }
}
