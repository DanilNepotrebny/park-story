﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.Requirements.RequirementHelpers
{
    public class ChipCleanupRequirementHelper : RequirementHelper
    {
        [Inject] private GridSystem _grid;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        private ChipCleanupRequirement _requirement;

        public override void Initialize(Requirement requirement)
        {
            _requirement = requirement as ChipCleanupRequirement;
            Assert.IsNotNull(
                    _requirement,
                    $"Assigned not proper helper for requirement " +
                    $"'{requirement.GetType()}' - {nameof(ChipCleanupRequirement)}"
                );
        }

        public override int FindCellsToImpact(
            ImpactFlag impactType,
            int cellsNumber,
            ref HashSet<CellComponent> container)
        {
            int foundCells = 0;

            List<CellComponent> cells = _grid
                .Cells
                .Where(HasRequiredChip)
                .Except(container)
                .ToList();

            cells.RandomShuffle(_random);
            foreach (CellComponent cell in cells)
            {
                if (foundCells == cellsNumber)
                {
                    return foundCells;
                }

                ChipComponent chip = cell.Chip;
                if (!chip.CanBeImpacted(impactType))
                {
                    break;
                }

                if (IsRequirementChip(chip) ||
                    chip.IsChildVisible)
                {
                    container.Add(cell);
                    foundCells += 1;
                    break;
                }
            }

            return foundCells;
        }

        private bool HasRequiredChip([NotNull] CellComponent cell)
        {
            ChipComponent chip = cell.Chip;
            while (chip != null)
            {
                if (IsRequirementChip(chip))
                {
                    return true;
                }

                chip = chip.Child;
            }

            return false;
        }

        private bool IsRequirementChip([NotNull] ChipComponent chip)
        {
            return chip.Tags.Has(_requirement.RequiredChip, false);
        }
    }
}
