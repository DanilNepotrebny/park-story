﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using Zenject;

namespace DreamTeam.Match3.Requirements.RequirementHelpers
{
    public class CommonChipConsumptionRequirementHelper : ChipConsumptionRequirementHelper
    {
        [Inject] private GridSystem _grid;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        public override int FindCellsToImpact(
            ImpactFlag impactType,
            int cellsNumber,
            ref HashSet<CellComponent> container)
        {
            int foundCells = 0;

            List<CellComponent> cells = _grid
                .Cells
                .Where(HasRequiredChip)
                .Except(container)
                .ToList();

            cells.RandomShuffle(_random);
            foreach (CellComponent cell in cells)
            {
                if (foundCells == cellsNumber)
                {
                    return foundCells;
                }

                ChipComponent chip = cell.Chip;
                if (!chip.CanBeImpacted(impactType))
                {
                    break;
                }

                if (IsRequirementChip(chip) ||
                    chip.IsChildVisible)
                {
                    container.Add(cell);
                    foundCells += 1;
                    break;
                }
            }

            return foundCells;
        }
    }
}