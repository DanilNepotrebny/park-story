﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.Requirements.RequirementHelpers
{
    public class HiddenObjectRequirementHelper : RequirementHelper
    {
        [Inject] private GridSystem _grid;
        [Inject] private List<HiddenObject> _hiddenObjects;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        private HiddenObjectRequirement _requirement;
        private readonly List<CellComponent> _cells = new List<CellComponent>();

        public override void Initialize(Requirement requirement)
        {
            _requirement = requirement as HiddenObjectRequirement;
            Assert.IsNotNull(
                _requirement,
                $"Assigned not proper helper {nameof(HiddenObjectRequirementHelper)} for requirement '{requirement.GetType()}'");
        }

        public override int FindCellsToImpact(
            ImpactFlag impactType,
            int cellsNumber,
            ref HashSet<CellComponent> container)
        {
            int foundCellsNumber = 0;
            _cells.RandomShuffle(_random);

            foreach (CellComponent cell in _cells)
            {
                if (foundCellsNumber == cellsNumber)
                {
                    break;
                }

                if (cell.IsEmpty || !cell.Chip.CanBeImpacted(impactType))
                {
                    continue;
                }

                if (container.Add(cell))
                {
                    foundCellsNumber += 1;
                }
            }

            return foundCellsNumber;
        }

        protected void Awake()
        {
            foreach (HiddenObject ho in _hiddenObjects)
            {
                foreach (CellComponent cell in ho.CellsToRelease)
                {
                    cell.Underlay.Destroyed += OnUnderlayDestroyed;
                    _cells.Add(cell);
                }
            }
        }

        protected void OnDestroy()
        {
            _cells.ForEach(cell => cell.Underlay.Destroyed -= OnUnderlayDestroyed);
        }

        private void OnUnderlayDestroyed(Underlay underlay)
        {
            underlay.Destroyed -= OnUnderlayDestroyed;
            _cells.RemoveAll(cell => cell.Underlay.gameObject == underlay.gameObject);
        }
    }
}