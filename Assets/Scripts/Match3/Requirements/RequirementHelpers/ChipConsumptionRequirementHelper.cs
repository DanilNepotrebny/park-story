﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Requirements.ChipConsumption;
using JetBrains.Annotations;
using UnityEngine.Assertions;

namespace DreamTeam.Match3.Requirements.RequirementHelpers
{
    public abstract class ChipConsumptionRequirementHelper : RequirementHelper
    {
        protected ChipConsumptionRequirement Requirement { get; private set; }

        public override void Initialize(Requirement requirement)
        {
            Requirement = requirement as ChipConsumptionRequirement;
            Assert.IsNotNull(
                    Requirement,
                    $"Assigned not proper helper for requirement " +
                    $"'{requirement.GetType()}' - {nameof(ChipConsumptionRequirementHelper)}"
                );
        }

        protected bool HasRequiredChip([NotNull] CellComponent cell)
        {
            ChipComponent chip = cell.Chip;
            while (chip != null)
            {
                if (IsRequirementChip(chip))
                {
                    return true;
                }

                chip = chip.Child;
            }

            return false;
        }

        protected bool IsRequirementChip([CanBeNull] ChipComponent chip)
        {
            return chip?.Tags.Has(Requirement.RequiredChipTag, false) ?? false;
        }
    }
}