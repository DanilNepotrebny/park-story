﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.SpecialChips.GumballMachine;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.Requirements
{
    public partial class GumballConsumptionRequirement : CountedRequirement
    {
        [SerializeField] private int _requiredCount;
        [SerializeField] private ChipTag _gumballMachineChipTag;
        [SerializeField] private bool _synchronizeGumballsCount = false;

        public override int RequiredCount => _requiredCount;

        public ChipTag RequiredChipTag => _gumballMachineChipTag;

        protected override Type InstanceType => typeof(Instance);

        protected void OnValidate()
        {
            if (_gumballMachineChipTag == null)
            {
                UnityEngine.Debug.LogAssertion($"Missing {nameof(_gumballMachineChipTag)}", this);
            }
        }

        public new class Instance : CountedRequirement.Instance
        {
            [Inject(Id = Match3Installer.RequirementHelpersContainerId)]
            private GameObject _helperContainer;

            [Inject] private GridSystem _grid;
            [Inject] private Instantiator _instantiator;

            public event Action<GumballComponent, IDeferredInvocationHandle> CounterIncrementing;

            public override int RequiredCount => _requirement.RequiredCount;

            private readonly GumballConsumptionRequirement _requirement;
            private Queue<IDeferredInvocationHandle> _counterChangeHandles = new Queue<IDeferredInvocationHandle>();

            public Instance(GumballConsumptionRequirement requirement, List<GumballMachineComponent> gumballMachines)
                : base(requirement)
            {
                _requirement = requirement;
                gumballMachines.ForEach(spawner => spawner.ProjectileSpawned += OnGumballProjectileSpawned);
            }

            protected override RequirementHelper FindHelperChild()
            {
                return (from cell in _grid.Cells select GetHelperPrefab(cell.Chip) ?? GetHelperPrefab(cell.Chip?.Child)
                    into helperPrefab
                    where helperPrefab != null
                    select _instantiator.Instantiate(helperPrefab, _helperContainer)).FirstOrDefault();
            }

            private RequirementHelper GetHelperPrefab(ChipComponent chip)
            {
                if (chip != null && IsRequirementChip(chip))
                {
                    if (chip.Helper != null)
                    {
                        return chip.Helper;
                    }

                    UnityEngine.Debug.LogAssertion($"Missing helper in required {chip.name}");
                }

                return null;
            }

            private void OnGumballProjectileSpawned(GumballComponent projectile)
            {
                if (RequiredCount - Counter <= _counterChangeHandles.Count)
                {
                    projectile.gameObject.SetActive(false);
                    projectile.Dispose();
                    return;
                }

                CounterChanging += OnCounterChanging;
                Counter += 1;
                CounterChanging -= OnCounterChanging;

                var commitChangeDI = new DeferredInvocation(CommitCounterChange);
                using (var handle = commitChangeDI.Start())
                {
                    CounterIncrementing?.Invoke(projectile, handle);
                }
            }

            private void OnCounterChanging(int valueChangeDiff, IDeferredInvocationHandle commitChangeHandle)
            {
                _counterChangeHandles.Enqueue(commitChangeHandle.Lock());
            }

            private void CommitCounterChange()
            {
                _counterChangeHandles.Dequeue().Unlock();
            }

            private bool IsRequirementChip([NotNull] ChipComponent chip)
            {
                return chip.Tags.Has(_requirement.RequiredChipTag, false);
            }
        }
    }
}