﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;

namespace DreamTeam.Match3.Requirements.BonusCounters
{
    public interface IBonusCounter
    {
        int RewardCount { get; }
        event Action Changed;
        void Consume();
    }
}