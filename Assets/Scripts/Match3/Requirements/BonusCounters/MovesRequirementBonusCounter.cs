﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Match3.Requirements.BonusCounters
{
    public class MovesRequirementBonusCounter : IBonusCounter, IDisposable
    {
        private readonly MovesRequirement.Instance _requirement;
        private readonly int _rewardPrice;
        private int _remainingMoves = 0;

        public int RewardCount => _remainingMoves / _rewardPrice;

        public event Action Changed;

        public MovesRequirementBonusCounter(MovesRequirement.Instance requirement, int rewardPrice)
        {
            _rewardPrice = Mathf.Clamp(rewardPrice, 1, int.MaxValue);
            _requirement = requirement;

            SetRemainingMoves(_requirement.RemainingMoves);
            _requirement.Changed += OnRequirementChanged;
        }

        ~MovesRequirementBonusCounter()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (_requirement != null)
            {
                _requirement.Changed -= OnRequirementChanged;
            }
        }

        public void Consume()
        {
            SetRemainingMoves(_remainingMoves - 1);
        }

        private void SetRemainingMoves(int value)
        {
            value = Mathf.Clamp(value, 0, int.MaxValue);
            if (_remainingMoves == value)
            {
                return;
            }

            _remainingMoves = value;
            Changed?.Invoke();
        }

        private void OnRequirementChanged()
        {
            SetRemainingMoves(_requirement.RemainingMoves);
        }
    }
}