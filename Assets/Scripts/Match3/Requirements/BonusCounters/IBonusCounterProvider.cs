﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3.Requirements.BonusCounters
{
    /// <summary>
    /// Interface for Requirement that specifies
    /// </summary>
    public interface IBonusCounterProvider
    {
        IBonusCounter BonusCounter { get; }
    }
}