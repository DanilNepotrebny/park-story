﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using ModestTree;
using Zenject;

namespace DreamTeam.Match3.Requirements
{
    public class CompositeRequirementHelper : RequirementHelper
    {
        private readonly HashSet<RequirementHelper> _helpers = new HashSet<RequirementHelper>();

        [Inject] private Instantiator _instantiator;

        public void Construct(HashSet<RequirementHelper> helperPrefabs)
        {
            _helpers.Clear();
            foreach (RequirementHelper helperPrefab in helperPrefabs)
            {
                _helpers.Add(_instantiator.Instantiate(helperPrefab, gameObject));
            }
        }

        public override void Initialize(Requirement requirement)
        {
            _helpers.ForEach(h => h.Initialize(requirement));
        }

        public override int FindCellsToImpact(ImpactFlag impactType, int cellsNumber, ref HashSet<CellComponent> container)
        {
            int cellsAmountToFind = cellsNumber;
            int totalFoundCells = 0;

            foreach (RequirementHelper helper in _helpers)
            {
                int foundCells = helper.FindCellsToImpact(impactType, cellsAmountToFind, ref container);
                totalFoundCells += foundCells;
                cellsAmountToFind -= foundCells;

                if (cellsAmountToFind <= 0)
                {
                    break;
                }
            }

            return totalFoundCells;
        }
    }
}
