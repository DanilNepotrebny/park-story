﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Match3.Requirements
{
    public abstract class RequirementHelper : MonoBehaviour
    {
        public abstract void Initialize(Requirement requirement);

        /// <summary> Find cells impacting of will advance the requirement </summary>
        /// <param name="impactType"> Caller's impact type </param>
        /// <param name="cellsNumber"> Number of cells to be found </param>
        /// <param name="container"> Container where found cells to be added </param>
        /// <returns> Number of found cells </returns>
        public abstract int FindCellsToImpact(
            ImpactFlag impactType,
            int cellsNumber,
            ref HashSet<CellComponent> container);
    }
}