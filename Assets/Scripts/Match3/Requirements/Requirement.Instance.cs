﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.Requirements
{
    /// <summary> Base class for all requirement scriptable objects. </summary>
    public abstract partial class Requirement
    {
        /// <summary> Instance of the requirement that is created in runtime and controls all it's logics. </summary>
        public abstract partial class Instance : IDisposable
        {
            [Inject(Id = Match3Installer.RequirementHelpersContainerId)]
            private GameObject _helperContainer;

            [Inject] private Instantiator _instantiator;

            /// <summary> Returns whether requirement is reached. </summary>
            public abstract bool IsReached { get; }

            /// <summary> Returns whether requirement is stable and it's values are final </summary>
            public abstract bool IsStable { get; }

            /// <summary> Base requirement of instance </summary>
            public Requirement BaseRequirement { get; }

            public float ProgressPercentage => (float)Math.Round(NormalizedProgress * 100, 1);

            protected abstract float NormalizedProgress { get; }

            /// <summary>
            /// Event that is called when the requirement changed it's state.
            /// Shouldn't be invoked from inherited classes, use HandleChanged method instead.
            /// </summary>
            public virtual event Action Changed;

            /// <summary>
            /// Event that is called when the requirement is reached.
            /// Shouldn't be invoked from inherited classes, use HandleChanged method instead.
            /// </summary>
            public virtual event Action Reached;

            /// <summary> Find helper to advance the requirement </summary>
            /// <returns> Found helper or null </returns>
            public RequirementHelper FindHelper()
            {
                RequirementHelper helper = FindHelperChild();
                if (helper == null && BaseRequirement._defaultHelperPrefab != null)
                {
                    helper = _instantiator.Instantiate(BaseRequirement._defaultHelperPrefab);
                }

                if (helper != null)
                {
                    helper.transform.parent = _helperContainer.transform;
                    helper.Initialize(BaseRequirement);
                }

                return helper;
            }

            public virtual void Dispose()
            {
                // nothing to do
            }

            protected Instance(Requirement requirement)
            {
                BaseRequirement = requirement;
            }

            /// <summary> Handler for state changes of the requirement. Invokes OnChanged and OnReached events for all subscribers. </summary>
            protected void OnChanged()
            {
                Changed?.Invoke();

                if (IsReached)
                {
                    Reached?.Invoke();
                }
            }

            protected abstract RequirementHelper FindHelperChild();
        }
    }
}