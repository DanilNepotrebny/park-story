﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Incapsulates functionality of a chip which can be exploded. Explosion impacts other chips within specified radius.
    /// </summary>
    [RequireComponent(typeof(ChipComponent))]
    public class ExplosiveComponent : MonoBehaviour, IImpactController
    {
        [SerializeField] private float _radius = 1.0f;
        [SerializeField] private int _impact = -1;
        [SerializeField] private float _delay = 0.12f;
        [SerializeField] private GameObject _explosionEffect;

        [Inject] private GridSystem _grid;
        [Inject] private ImpactSystem _impactSystem;
        [Inject] private Instantiator _instantiator;

        private ChipComponent _chip;
        private bool _processingScheduledImpact;
        private List<Impact> _scheduledImpacts = new List<Impact>();

        protected void Awake()
        {
            _chip = GetComponent<ChipComponent>();
            _chip.AddImpactController(this);
            _chip.CellChanged += OnCellChanged;
            SubscribeToImpactEvents(_chip.Cell);
        }

        private void OnCellChanged(ChipComponent chip, CellComponent prevCell)
        {
            UnsubscribeFromImpactEvents(prevCell);
            SubscribeToImpactEvents(_chip.Cell);
        }

        private void SubscribeToImpactEvents([CanBeNull] CellComponent cell)
        {
            if (cell != null)
            {
                cell.ImpactScheduled += OnImpactScheduled;
            }
        }

        private void UnsubscribeFromImpactEvents([CanBeNull] CellComponent cell)
        {
            if (cell != null)
            {
                cell.ImpactScheduled -= OnImpactScheduled;
            }
        }

        private void OnImpactScheduled(CellComponent cell, Impact impact)
        {
            if (_scheduledImpacts.Count > 0 ||
                _processingScheduledImpact ||
                _chip.IsConsumed ||
                !_chip.WillBeConsumed())
            {
                return;
            }

            _processingScheduledImpact = true;

            impact.Cancelling += OnScheduledImpactCancelling;

            Assert.AreEqual(_chip.Cell, cell);
            _scheduledImpacts = Explosion.ScheduleExplode(
                _grid,
                _impactSystem,
                _chip.Cell.GridPosition,
                _radius,
                _impact);

            _processingScheduledImpact = false;
        }

        private void OnScheduledImpactCancelling(Impact impact)
        {
            impact.Cancelling -= OnScheduledImpactCancelling;

            if (_processingScheduledImpact)
            {
                UnityEngine.Debug.LogError($"{nameof(ExplosiveComponent)}: Cancelling while processing impact scheduling", this);
            }

            _processingScheduledImpact = true;
            CancelScheduledImpacts();
            _processingScheduledImpact = false;
        }

        private void CancelScheduledImpacts()
        {
            _scheduledImpacts.ForEach(impact => impact.Cancel());
            _scheduledImpacts.Clear();
        }

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.Explosive;
        }

        void IImpactController.OnImpact(
            ChipComponent chip,
            ImpactFlag impactType,
            IDeferredInvocationHandle impactEndHandle,
            ref bool isModal)
        {
            if (!chip.IsConsumed)
            {
                return;
            }

            CancelScheduledImpacts(); // reset previously scheduled because actual impact performs with delay
            if (_explosionEffect != null)
            {
                _instantiator.Instantiate(_explosionEffect, transform.position);
            }

            Assert.AreEqual(_chip, chip);
            Explosion.Explode(_grid, _impactSystem, _chip.Cell.GridPosition, _radius, _impact, _delay);
        }

        void IImpactController.Cancel()
        {
            // nothing to do
        }
    }
}