﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    [CreateAssetMenu(fileName = "ChipConsumptionFactory", menuName = "Match 3/Chip Consumption Factory")]
    public class ChipConsumptionFactory : GameObjectFactory<ChipTag, ChipConsumptionFactory.ModelRefType,
        ChipConsumptionFactory.PresentersMap>
    {
        [Serializable]
        public class ModelRefType : TypeReference<ChipTag>
        {
        }

        // TODO: GameObject must have MovementSequence component
        [Serializable]
        public class PresentersMap : KeyValueList<ModelRefType, GameObject>
        {
        }
    }
}