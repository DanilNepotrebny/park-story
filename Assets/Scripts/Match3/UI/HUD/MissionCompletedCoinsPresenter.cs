﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.LevelCompletion;
using DreamTeam.UI.SimpleAnimating;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Match3.UI.HUD
{
    [RequireComponent(typeof(SimpleAnimation))]
    public class MissionCompletedCoinsPresenter : MonoBehaviour
    {
        [SerializeField] private Text _coinsText;
        [SerializeField] private Transform _coinMovementFinishPoint;

        [Inject] private LevelRewardSequence _rewardSequence;

        private ISimpleAnimationPlayer _animPlayer;

        public Transform CoinMovementFinishPoint => _coinMovementFinishPoint;

        /// <summary> Show presenter with animation </summary>
        public void Show()
        {
            gameObject.SetActive(true);
            _animPlayer.Play(SimpleAnimationType.Show);
        }

        /// <summary> Hide presenter with animation </summary>
        public void Hide()
        {
            if (!_animPlayer.Play(SimpleAnimationType.Hide))
            {
                gameObject.SetActive(false);
            }
        }

        protected void Awake()
        {
            _rewardSequence.ReceivedMoneyChanged += UpdateReceivedCoinsText;

            _animPlayer = GetComponent<SimpleAnimation>();
            _animPlayer.AnimationFinished += OnAnimPlayerFinished;
        }

        protected void OnDestroy()
        {
            _rewardSequence.ReceivedMoneyChanged -= UpdateReceivedCoinsText;
        }

        protected void OnEnable()
        {
            UpdateReceivedCoinsText(_rewardSequence.ReceivedMoney, _rewardSequence.ReceivedMoney);
        }

        private void UpdateReceivedCoinsText(int current, int previous)
        {
            _coinsText.text = current.ToString();
        }

        private void OnAnimPlayerFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            if (animationType == SimpleAnimationType.Hide)
            {
                gameObject.SetActive(false);
            }
        }
    }
}