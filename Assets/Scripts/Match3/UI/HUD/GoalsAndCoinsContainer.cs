﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI.SimpleAnimating;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.UI.HUD
{
    public class GoalsAndCoinsContainer : MonoBehaviour
    {
        [SerializeField] private SimpleAnimation _goalsAnimation;
        [SerializeField] private MissionCompletedCoinsPresenter _completedCoinsPresenter;

        public void ShowCoins()
        {
            if (!_goalsAnimation.Play(SimpleAnimationType.Hide))
            {
                OnGoalsAnimationFinished(_goalsAnimation, SimpleAnimationType.Hide);
            }
        }

        public void InstallBindings(DiContainer container)
        {
            container.Bind<MissionCompletedCoinsPresenter>().FromInstance(_completedCoinsPresenter);
        }

        protected void Start()
        {
            _goalsAnimation.AnimationFinished += OnGoalsAnimationFinished;
            _goalsAnimation.gameObject.SetActive(true);

            _completedCoinsPresenter.gameObject.SetActive(false);
        }

        private void OnGoalsAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            _goalsAnimation.gameObject.SetActive(false);
            if (animationType == SimpleAnimationType.Hide)
            {
                _completedCoinsPresenter.Show();
            }
        }
    }
}