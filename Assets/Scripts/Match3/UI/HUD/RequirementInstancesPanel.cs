﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.UI.RequirementPresenters;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.UI.HUD
{
    /// <summary>
    /// Base class of panel that contains presenters for requirement instances.
    /// </summary>
    /// <typeparam name="T">Type of root requirement instance presenter for this panel.</typeparam>
    public abstract class RequirementInstancesPanel<T> : MonoBehaviour
        where T : RequirementInstanceRootPresenter
    {
        [SerializeField] private RequirementInstanceViewFactory _viewFactory;
        [SerializeField] private SwitcherByScreenOrientation _presentersContainer;

        [Inject] private Instantiator _instantiator;

        protected abstract IReadOnlyList<Requirement.Instance> RequirementInstances { get; set; }

        /// <summary>
        /// Unity callback
        /// </summary>
        protected void Start()
        {
            foreach (var instance in RequirementInstances)
            {
                GameObject view = _viewFactory.Create(instance, _instantiator);
                Assert.IsNotNull(view);
                view.transform.SetParent(GetPresentersContainer().transform, false);
            }
        }

        /// <summary>
        /// Finds root presenter of specified requirement instance.
        /// </summary>
        public T FindRootPresenter(Requirement.Instance instance)
        {
            T[] presenters = GetPresentersContainer().GetComponentsInChildren<T>();
            foreach (var presenter in presenters)
            {
                if (presenter.RequirementInstance == instance)
                {
                    return presenter;
                }
            }

            return null;
        }

        private GameObject GetPresentersContainer()
        {
            return _presentersContainer != null && _presentersContainer.Current != null ?
                _presentersContainer.Current :
                gameObject;
        }
    }
}