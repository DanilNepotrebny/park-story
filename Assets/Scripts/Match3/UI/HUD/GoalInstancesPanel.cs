﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.UI.RequirementPresenters;
using Zenject;

namespace DreamTeam.Match3.UI.HUD
{
    /// <summary>
    /// UI panel that contains views for goal instances for the current level.
    /// </summary>
    public class GoalInstancesPanel : RequirementInstancesPanel<RequirementInstanceRootPresenter>
    {
        [Inject(Id = LevelSettings.GoalInstancesId)]
        protected override IReadOnlyList<Requirement.Instance> RequirementInstances { get; set; }
    }
}