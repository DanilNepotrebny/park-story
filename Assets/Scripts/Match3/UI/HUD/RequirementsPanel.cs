﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.UI.RequirementPresenters;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.UI.HUD
{
    public abstract class RequirementsPanel : MonoBehaviour
    {
        [Inject] private Instantiator _instantiator;

        [SerializeField] private RequirementViewFactory _viewFactory;

        private List<GameObject> _views = new List<GameObject>();

        protected abstract IEnumerable<Requirement> Requirements { get; }

        private void OnEnable()
        {
            foreach (var instance in Requirements)
            {
                GameObject view = _viewFactory.Create(instance, _instantiator);
                Assert.IsNotNull(view);
                view.transform.SetParent(transform, false);
                _views.Add(view);
            }
        }

        private void OnDisable()
        {
            foreach (GameObject view in _views)
            {
                view.Dispose();
            }
            _views.Clear();
        }
    }
}