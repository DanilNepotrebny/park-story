﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using Zenject;

namespace DreamTeam.Match3.UI.HUD
{
    public class GoalsPanel : RequirementsPanel
    {
        [Inject] private ICurrentLevelInfo _currentLevelInfo;

        protected override IEnumerable<Requirement> Requirements => _currentLevelInfo.CurrentLevel.Goals;
    }
}