﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Hammer;
using DreamTeam.UI;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.UI
{
    [RequireComponent(typeof(PresenterHub))]
    public class HammerSystemItemPresenterInitializer : MonoBehaviour
    {
        [Inject] private HammerSystem _hammerSystem;

        protected void OnEnable()
        {
            GetComponent<PresenterHub>().Initialize(_hammerSystem.InventoryItem);
        }
    }
}