﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Popups;
using DreamTeam.SceneLoading;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Match3.UI
{
    public class ExitButtonController : MonoBehaviour
    {
        [SerializeField] private Button _button;
        [SerializeField] private SceneReference _quitPopup;

        [Inject] private LevelStartNotifier _levelStartNotifier;
        [Inject] private PopupSystem _popupSystem;
        [Inject] private LevelCompletionSystem _levelCompletionSystem;
        [Inject] private LoadingSystem _loadingSystem;
        [Inject] private LoadSceneSettings _loadSceneSettings;

        protected void Start()
        {
            _button.onClick.AddListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            if (_levelStartNotifier.IsLevelStarted)
            {
                _popupSystem.Show(
                        _quitPopup.Name,
                        new QuitPopup.Args()
                        {
                            QuitButtonCallback = () => _levelCompletionSystem.FailLevel()
                        }
                    );
            }
            else
            {
                _loadingSystem.LoadScene(_loadSceneSettings.ScenePath, true, _loadSceneSettings.LoadingScreenType);
            }
        }
    }
}