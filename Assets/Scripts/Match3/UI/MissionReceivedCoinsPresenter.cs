﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.LevelCompletion;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Match3.UI
{
    public class MissionReceivedCoinsPresenter : MonoBehaviour
    {
        [Inject] private LevelRewardSequence _rewardSequence;

        [SerializeField] private Text _coinsText;

        private void Awake()
        {
            _coinsText.text = _rewardSequence.ReceivedMoney.ToString();
        }
    }
}