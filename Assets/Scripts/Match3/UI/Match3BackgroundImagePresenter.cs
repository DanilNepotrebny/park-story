﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Levels;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Match3.UI
{
    [RequireComponent(typeof(Image))]
    public class Match3BackgroundImagePresenter : MonoBehaviour
    {
        [Inject] private ICurrentLevelInfo _currentLevelInfo;

        protected void OnEnable()
        {
            LevelPack levelPack = _currentLevelInfo?.CurrentLevelPack;
            LevelBackgroundSettings settings = levelPack?.BackgroundSettings;
            Assert.IsNotNull(levelPack);
            Assert.IsNotNull(
                    settings,
                    $"LevelPack '{levelPack.name}' doesn't contain background settings."
                );
            GetComponent<Image>().sprite = settings.GetBackground(_currentLevelInfo.CurrentLevelIndex);
        }
    }
}