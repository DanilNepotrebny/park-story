﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.SpecialChips.GumballMachine;
using DreamTeam.UI;
using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Match3.UI.RequirementPresenters
{
    public class GumballGoalPresenter : MonoBehaviour, IPresenter<Requirement.Instance>
    {
        [SerializeField] private RectTransform _projectileTarget;

        private IDeferredInvocationHandle _counterHandle;

        public void Initialize(Requirement.Instance model)
        {
            GumballConsumptionRequirement.Instance requirement = model as GumballConsumptionRequirement.Instance;
            if (requirement == null)
            {
                return;
            }

            requirement.CounterIncrementing += OnRequirementCounterIncrementing;
        }

        private void OnRequirementCounterIncrementing(GumballComponent projectile, IDeferredInvocationHandle handle)
        {
            _counterHandle = handle.Lock();
            projectile.StartSequence(_projectileTarget.position, handle);
            _counterHandle.Unlock();
        }
    }
}