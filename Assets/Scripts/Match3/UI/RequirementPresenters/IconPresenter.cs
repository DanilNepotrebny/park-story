﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Requirements;
using DreamTeam.UI;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Match3.UI.RequirementPresenters
{
    public abstract class IconPresenter : MonoBehaviour, IPresenter<Requirement>, IPresenter<Requirement.Instance>
    {
        [SerializeField] private Image _icon;

        protected abstract Sprite Icon { get; }

        /// <summary>
        /// Initializes goal presenter.
        /// </summary>
        public virtual void Initialize(Requirement requirement)
        {
            if (_icon != null)
            {
                _icon.sprite = Icon;
            }
        }

        public void Initialize(Requirement.Instance requirementInstance)
        {
            Initialize(requirementInstance.BaseRequirement);
        }
    }
}