﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Requirements;
using DreamTeam.UI.SimpleAnimating;
using UnityEngine;

namespace DreamTeam.Match3.UI.RequirementPresenters
{
    public class ReachedStateSwitcher : RequirementInstancePresenter
    {
        [SerializeField] private GameObject _notReachedStateView;
        [SerializeField] private GameObject _reachedStateView;

        private Requirement.Instance _requirementInstance;
        private SimpleAnimation _notReachedStateViewAnimation;
        private SimpleAnimation _reachedStateViewAnimation;

        /// <summary>
        /// Initializes goal presenter.
        /// </summary>
        public override void Initialize(Requirement.Instance requirementInstance)
        {
            base.Initialize(requirementInstance);

            _requirementInstance = requirementInstance;
        }

        protected void Awake()
        {
            if (_notReachedStateView != null)
            {
                _notReachedStateViewAnimation = _notReachedStateView.GetComponent<SimpleAnimation>();
                if (_notReachedStateViewAnimation != null)
                {
                    _notReachedStateViewAnimation.AnimationFinished += OnNotReachedViewAnimationFinished;
                }
            }

            if (_reachedStateView != null)
            {
                _reachedStateViewAnimation = _reachedStateView.GetComponent<SimpleAnimation>();
                if (_reachedStateViewAnimation != null)
                {
                    _reachedStateViewAnimation.AnimationFinished += OnReachedStateViewAnimationFinished;
                }
            }
        }

        protected override void UpdateView()
        {
            UpdateView(_reachedStateView, _reachedStateViewAnimation, _requirementInstance.IsReached);
            UpdateView(_notReachedStateView, _notReachedStateViewAnimation, !_requirementInstance.IsReached);
        }

        private void UpdateView(GameObject view, SimpleAnimation anim, bool shouldBeActive)
        {
            if (view == null)
            {
                return;
            }

            if (anim != null)
            {
                if (shouldBeActive)
                {
                    anim.Play(SimpleAnimationType.Show);
                }
                else
                {
                    if (anim.Play(SimpleAnimationType.Hide))
                    {
                        return;
                    }
                }
            }

            view.SetActive(shouldBeActive);
        }

        private void OnReachedStateViewAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            if (animationType == SimpleAnimationType.Hide)
            {
                _reachedStateView.SetActive(false);
            }
        }

        private void OnNotReachedViewAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            if (animationType == SimpleAnimationType.Hide)
            {
                _notReachedStateView.SetActive(false);
            }
        }
    }
}