﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.Requirements.ChipConsumption;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.UI.RequirementPresenters
{
    public class ChipConsumptionGoalIconPresenter : IconPresenter
    {
        [Inject] private ChipTagIconSettings _chipTagIconSettings;

        protected override Sprite Icon => _chipTagIconSettings.GetIcon(_requirement.RequiredChipTag);

        private ChipConsumptionRequirement _requirement;

        /// <summary>
        /// Initializes goal presenter.
        /// </summary>
        public override void Initialize(Requirement requirement)
        {
            _requirement = (ChipConsumptionRequirement)requirement;

            base.Initialize(requirement);
        }
    }
}