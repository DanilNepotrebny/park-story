﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Requirements;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.UI.RequirementPresenters
{
    public class ChipCleanupGoalIconPresenter : IconPresenter
    {
        [Inject] private ChipTagIconSettings _chipTagIconSettings;

        protected override Sprite Icon => _chipTagIconSettings.GetIcon(_requirement.RequiredChip);

        private ChipCleanupRequirement _requirement;

        public override void Initialize(Requirement requirement)
        {
            _requirement = (ChipCleanupRequirement)requirement;

            base.Initialize(requirement);
        }
    }
}
