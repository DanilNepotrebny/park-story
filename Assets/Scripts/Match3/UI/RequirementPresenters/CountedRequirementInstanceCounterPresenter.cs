﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Requirements;
using DreamTeam.Utils.Tweening;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DreamTeam.Match3.UI.RequirementPresenters
{
    public class CountedRequirementInstanceCounterPresenter : RequirementInstancePresenter
    {
        [SerializeField, FormerlySerializedAs("_format")]
        private string _reachedStateFormat;

        [SerializeField] private string _notReachedStateFormat;
        [SerializeField] private Text _counterText;

        [Space, SerializeField] private CounterTweener _incrementTweener;
        [SerializeField] private CounterTweener _decrementTweener;

        [Space, SerializeField] private UnityEvent _incrementCompleted;

        private CountedRequirement.Instance _requirementInstance;
        private int _currentCount;

        /// <summary>
        /// Initializes goal presenter.
        /// </summary>
        public override void Initialize(Requirement.Instance requirementInstance)
        {
            base.Initialize(requirementInstance);

            _requirementInstance = requirementInstance as CountedRequirement.Instance;
            Assert.IsNotNull(
                    _requirementInstance,
                    $"{nameof(requirementInstance)} is not {nameof(CountedRequirement.Instance)}"
                );

            _currentCount = _requirementInstance.Counter;
            SetCounterText(_currentCount);
        }

        protected override void UpdateView()
        {
            if (_counterText != null && _requirementInstance != null)
            {
                int newCount = _requirementInstance.Counter;
                if (_currentCount < newCount)
                {
                    _incrementTweener.Stop();
                    _decrementTweener.Start(
                            _currentCount,
                            newCount,
                            SetCounterText,
                            () =>
                            {
                                _currentCount = newCount;
                                _incrementCompleted.Invoke();
                            }
                        );
                }
                else if (_currentCount > newCount)
                {
                    _decrementTweener.Stop();
                    _incrementTweener.Start(_currentCount, newCount, SetCounterText, () => _currentCount = newCount);
                }
            }
        }

        private void SetCounterText(float count)
        {
            string format = _requirementInstance.IsReached ? _reachedStateFormat : _notReachedStateFormat;

            _counterText.text = string.Format(
                    format,
                    (int)count,
                    _requirementInstance.RequiredCount,
                    _requirementInstance.RequiredCount - (int)count
                );
        }

        public void DebugCallBack()
        {
            UnityEngine.Debug.Log($"{name} event called");
        }
    }
}