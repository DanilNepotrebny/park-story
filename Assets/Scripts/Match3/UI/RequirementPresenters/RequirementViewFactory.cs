﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3.Requirements;
using DreamTeam.UI;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.UI.RequirementPresenters
{
    /// <summary>
    /// Base class for common settings of the goal.
    /// </summary>
    [CreateAssetMenu(fileName = "RequirementViewFactory", menuName = "Match 3/RequirementViewFactory")]
    public class RequirementViewFactory : ViewFactory<Requirement, RequirementViewFactory.ModelRefType,
        RequirementViewFactory.PresentersMap>
    {
        [Serializable]
        public class ModelRefType : TypeReference<Requirement>
        {
        }

        [Serializable]
        public class PresentersMap : KeyValueList<ModelRefType, GameObject>
        {
        }
    }
}