﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.UI.HUD;
using DreamTeam.UI;
using UnityEngine;

namespace DreamTeam.Match3.UI.RequirementPresenters
{
    /// <summary>
    /// Root presenter for requirement instances. Assumed that there is a single instance of such
    /// presenter for each requirement instance.
    /// You can find root presenter using <see cref="RequirementInstancesPanel{T}.FindRootPresenter"/>.
    /// </summary>
    public class RequirementInstanceRootPresenter : MonoBehaviour, IPresenter<Requirement.Instance>
    {
        [SerializeField] private Transform _projectileOrigin;

        public Requirement.Instance RequirementInstance { get; private set; }

        /// <summary>
        /// Origin for all coming/incoming projectiles.
        /// </summary>
        public Transform ProjectileOrigin => _projectileOrigin;

        public virtual void Initialize(Requirement.Instance requirementInstance)
        {
            RequirementInstance = requirementInstance;
        }
    }
}