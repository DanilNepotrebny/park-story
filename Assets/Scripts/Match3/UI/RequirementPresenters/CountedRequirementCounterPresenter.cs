﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Requirements;
using DreamTeam.UI;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Match3.UI.RequirementPresenters
{
    public class CountedRequirementCounterPresenter : MonoBehaviour, IPresenter<Requirement>
    {
        [SerializeField] private Text _counterText;

        private CountedRequirement _requirement;

        /// <summary>
        /// Initializes goal presenter.
        /// </summary>
        public void Initialize(Requirement requirement)
        {
            _requirement = (CountedRequirement)requirement;

            if (_counterText != null)
            {
                _counterText.text = string.Format(_counterText.text, _requirement.RequiredCount);
            }
        }
    }
}