﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Requirements;
using DreamTeam.UI;
using UnityEngine;

namespace DreamTeam.Match3.UI.RequirementPresenters
{
    public abstract class RequirementInstancePresenter : MonoBehaviour, IPresenter<Requirement.Instance>
    {
        /// <summary>
        /// Initializes goal presenter.
        /// </summary>
        public virtual void Initialize(Requirement.Instance requirementInstance)
        {
            requirementInstance.Changed += OnRequirementStateChanged;
        }

        /// <summary>
        /// Unity callback
        /// </summary>
        protected void Start()
        {
            UpdateView();
        }

        /// <summary>
        /// Event that is called when requirement state is changed.
        /// </summary>
        protected virtual void OnRequirementStateChanged()
        {
            UpdateView();
        }

        protected abstract void UpdateView();
    }
}