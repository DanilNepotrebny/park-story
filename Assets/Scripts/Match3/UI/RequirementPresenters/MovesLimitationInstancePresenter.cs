﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.Requirements.BonusCounters;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Match3.UI.RequirementPresenters
{
    [RequireComponent(typeof(CountedRequirementInstanceCounterPresenter))]
    public class MovesLimitationInstancePresenter : LimitationInstancePresenter
    {
        [SerializeField] private Text _counterText;
        [SerializeField] private string _textFormat = "{0}";

        [Inject] private LevelRewardSequence _rewardSequence;
        private CountedRequirementInstanceCounterPresenter _requirementPresenter;

        private MovesRequirementBonusCounter _bonusCounter;
        private MovesRequirement.Instance _requirement;

        public override void Initialize(Requirement.Instance requirementInstance)
        {
            base.Initialize(requirementInstance);

            _requirement = requirementInstance as MovesRequirement.Instance;
            _bonusCounter = _requirement.BonusCounter as MovesRequirementBonusCounter;
        }

        private void Awake()
        {
            _requirementPresenter = GetComponent<CountedRequirementInstanceCounterPresenter>();
            _rewardSequence.Performing += OnRewardSequencePerforming;

            enabled = false;
        }

        private void OnDestroy()
        {
            _rewardSequence.Performing -= OnRewardSequencePerforming;
        }

        private void OnEnable()
        {
            if (_bonusCounter != null)
            {
                _bonusCounter.Changed += UpdateInstancePresenter;
            }
        }

        private void OnDisable()
        {
            if (_bonusCounter != null)
            {
                _bonusCounter.Changed -= UpdateInstancePresenter;
            }
        }

        private void OnRewardSequencePerforming()
        {
            _requirementPresenter.enabled = false;
            enabled = true;
        }

        private void UpdateInstancePresenter()
        {
            _counterText.text = string.Format(_textFormat, _bonusCounter.RewardCount);
        }
    }
}