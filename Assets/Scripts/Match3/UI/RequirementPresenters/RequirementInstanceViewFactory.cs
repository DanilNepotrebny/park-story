﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3.Requirements;
using DreamTeam.UI;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.UI.RequirementPresenters
{
    /// <summary>
    /// Base class for common settings of the goal.
    /// </summary>
    [CreateAssetMenu(fileName = "RequirementInstanceViewFactory", menuName = "Match 3/RequirementInstanceViewFactory")]
    public class RequirementInstanceViewFactory : ViewFactory<Requirement.Instance,
        RequirementInstanceViewFactory.ModelRefType, RequirementInstanceViewFactory.PresentersMap>
    {
        [Serializable]
        public class ModelRefType : TypeReference<Requirement.Instance>
        {
        }

        [Serializable]
        public class PresentersMap : KeyValueList<ModelRefType, GameObject>
        {
        }
    }
}