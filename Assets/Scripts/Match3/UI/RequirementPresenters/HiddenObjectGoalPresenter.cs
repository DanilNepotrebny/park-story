﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Requirements;
using DreamTeam.UI;
using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Match3.UI.RequirementPresenters
{
    public class HiddenObjectGoalPresenter : MonoBehaviour, IPresenter<Requirement.Instance>
    {
        [SerializeField] private RectTransform _targetRect;

        public void Initialize(Requirement.Instance requirementInstance)
        {
            var requirement = requirementInstance as HiddenObjectRequirement.Instance;
            if (requirement != null)
            {
                requirement.Releasing += OnReleasing;
            }
        }

        private void OnReleasing(HiddenObject hiddenObject, IDeferredInvocationHandle commitCounterChangeHandle)
        {
            commitCounterChangeHandle = commitCounterChangeHandle.Lock();

            hiddenObject.Presenter.StartSequence(commitCounterChangeHandle.Unlock, _targetRect);
        }
    }
}