﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Reflection;
using DreamTeam.GameControls;
using DreamTeam.UI.SimpleAnimating;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Match3.UI
{
    [RequireComponent(typeof(Toggle))]
    public class OptionsButtonController : MonoBehaviour
    {
        [SerializeField] private List<SimpleAnimation> _childAnimations;
        [SerializeField] private List<GameControlsMode> _hideOnModes;

        [Inject] private GameControlsSystem _gameControlsSystem;

        private Toggle _toggle;

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void OnToggled(bool toggled)
        {
            var animationType = toggled ? SimpleAnimationType.Show : SimpleAnimationType.Hide;
            _childAnimations.ForEach(anim => anim.Play(animationType));
        }

        protected void Awake()
        {
            _toggle = GetComponent<Toggle>();
            _gameControlsSystem.ActiveContext.GameControlsModeActivityChanged += OnGameControlsChanged;
        }

        protected void OnDestroy()
        {
            _gameControlsSystem.ActiveContext.GameControlsModeActivityChanged -= OnGameControlsChanged;
        }

        private void OnGameControlsChanged([NotNull] GameControlsMode mode, bool isEnabled)
        {
            foreach (GameControlsMode hideOnMode in _hideOnModes)
            {
                if (_gameControlsSystem.IsModeEnabled(hideOnMode))
                {
                    _toggle.isOn = false;
                    return;
                }
            }
        }
    }
}