﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.UI
{
    [CreateAssetMenu(fileName = "ChipTagIconSettings.asset", menuName = "Match 3/ChipTagIconSettings")]
    public class ChipTagIconSettings : ScriptableObject
    {
        [Serializable, NonUniqueKeys]
        private class ChipTagIconList : KeyValueList<ChipTag, Sprite>
        {
        }

        [SerializeField] private ChipTagIconList _chipTagIcons;

        /// <summary>
        /// Returns icon by the chip tag.
        /// </summary>
        public Sprite GetIcon(ChipTag chipTag)
        {
            Sprite foundSprite = _chipTagIcons.FindFirstOrDefault(chipTag);
            if (foundSprite == null)
            {
                UnityEngine.Debug.LogError($"There is no icon for {chipTag}", this);
            }

            return foundSprite;
        }
    }
}