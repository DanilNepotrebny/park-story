﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.SceneLoading;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace DreamTeam.Match3.Levels
{
    public partial class LevelSystem : MonoBehaviour, ICurrentLevelInfo
    {
        [Inject] private LoadingSystem _loadingSystem;

        private List<BaseLevelPackController> _controllers = new List<BaseLevelPackController>();

        private LevelPackControllerType _activeControllerType = LevelPackControllerType.Main;

        public IEnumerable<BaseLevelPackController> Controllers => _controllers;

        public LevelSettings CurrentLevel => ActiveController.CurrentLevel;
        public int CurrentLevelIndex => ActiveController.CurrentLevelIndex;
        public LevelPack CurrentLevelPack => ActiveController.CurrentLevelPack;
        public string CurrentLevelId => ActiveController.CurrentLevelId;

        public BaseLevelPackController ActiveController =>
            _controllers.FirstOrDefault(controller => controller.Type == _activeControllerType);

        public BaseLevelPackController LastLoadedController { get; private set; } = null;

        public void RegisterLevelPackController(BaseLevelPackController controller)
        {
            if (!_controllers.Contains(controller))
            {
                _controllers.Add(controller);
            }
        }

        public void SetActiveControllerByType(LevelPackControllerType type)
        {
            if (!_controllers.Exists(controller => controller.Type == type))
            {
                throw new ArgumentException(
                    $"There is no {nameof(BaseLevelPackController)} with type {type} registered in {nameof(LevelSystem)}");
            }

            _activeControllerType = type;
        }

        public BaseLevelPackController GetActiveControllerByType(LevelPackControllerType type)
        {
            return _controllers.FirstOrDefault(controller => controller.Type == type);
        }

        public LevelSettings GetLoadedLevel()
        {
            return FindLevel(SceneManager.GetActiveScene());
        }

        public void LoadLevel(LevelSettings level)
        {
            if (level == null)
            {
                throw new ArgumentNullException(nameof(level));
            }

            LastLoadedController = ActiveController;
            _loadingSystem.LoadScene(level.ScenePath, true, LoadingSystem.LoadingScreenType.Match3);
        }

        public bool FindLevelInfo(
                LevelSettings level,
                out BaseLevelPackController levelPackController,
                out LevelPack levelPack
            )
        {
            LevelPack pack = ActiveController.FindLevelPack(level);
            if (pack != null)
            {
                levelPackController = ActiveController;
                levelPack = pack;
                return true;
            }

            foreach (BaseLevelPackController controller in _controllers)
            {
                pack = controller.FindLevelPack(level);
                if (pack != null)
                {
                    levelPackController = controller;
                    levelPack = pack;
                    return true;
                }
            }

            levelPackController = null;
            levelPack = null;
            return false;
        }

        public void OnLevelSucceeded(LevelSettings level)
        {
            BaseLevelPackController controller;
            LevelPack levelPack;
            FindLevelInfo(level, out controller, out levelPack);
            if (controller != null)
            {
                controller.OnLevelSucceeded(level);
            }
        }

        public bool FindCurrentLevelInfo(LevelPackControllerType controllerType, out ICurrentLevelInfo info)
        {
            info = _controllers.FirstOrDefault(item => item.Type == controllerType);
            return info != null;
        }

        private LevelSettings FindLevel(Scene scene)
        {
            foreach (BaseLevelPackController controller in _controllers)
            {
                LevelSettings result = controller.FindLevel(scene);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }
    }
}