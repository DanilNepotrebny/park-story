﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using Object = UnityEngine.Object;

namespace DreamTeam.Match3.Levels
{
    public class ActivityHandlerByLevel : MonoBehaviour
    {
        [SerializeField, RequiredField] private LevelSettings _level;
        [SerializeField] private CompareType _comparison = CompareType.Equal;
        [SerializeField] private bool _onlyCurrentLevelPack = true;
        [SerializeField] private bool _invertMode;
        [SerializeField, RequiredField] private Behaviour[] _components;
        [SerializeField, RequiredField] private GameObject[] _gameObjects;

        [Inject] private LevelSystem _levelSystem;
        [Inject] private ICurrentLevelInfo _currentLevelInfo;

        protected void Awake()
        {
            ValidateObjects(_components);
            ValidateObjects(_gameObjects);

            SetActive(IsCurrentLevelValid());
        }

        private bool IsCurrentLevelValid()
        {
            BaseLevelPackController controller = null;
            LevelPack pack = null;
            if (!_levelSystem.FindLevelInfo(_level, out controller, out pack))
            {
                return false;
            }

            if (pack != _currentLevelInfo.CurrentLevelPack)
            {
                return !_onlyCurrentLevelPack;
            }

            int levelIndex = pack.FindLevelIndex(_level);
            
            switch (_comparison)
            {
                case CompareType.Less:
                    return _currentLevelInfo.CurrentLevelIndex < levelIndex;
                case CompareType.LessEqual:
                    return _currentLevelInfo.CurrentLevelIndex <= levelIndex;
                case CompareType.Equal:
                    return _currentLevelInfo.CurrentLevelIndex == levelIndex;
                case CompareType.GreaterEqual:
                    return _currentLevelInfo.CurrentLevelIndex >= levelIndex;
                case CompareType.Greater:
                    return _currentLevelInfo.CurrentLevelIndex > levelIndex;
                case CompareType.NotEqual:
                    return _currentLevelInfo.CurrentLevelIndex != levelIndex;
                default:
                    throw new ArgumentOutOfRangeException(
                        nameof(_comparison), _comparison, $"Unhandled {nameof(CompareType)} value");
            }
        }

        private void ValidateObjects(IEnumerable<Object> objects)
        {
            foreach (Object obj in objects)
            {
                Assert.IsNotNull(obj, $"Found null object in {nameof(ActivityHandlerByLevel)} in {gameObject.name}");
            }
        }

        private void SetActive(bool isActive)
        {
            isActive ^= _invertMode;
            foreach (GameObject go in _gameObjects)
            {
                go?.SetActive(isActive);
            }

            foreach (Behaviour behaviour in _components)
            {
                if (behaviour != null)
                {
                    behaviour.enabled = isActive;
                }
            }
        }

        #region Inner types

        public enum CompareType
        {
            Less,
            LessEqual,
            Equal,
            GreaterEqual,
            Greater,
            NotEqual,
        }

        #endregion Inner types
    }
}
