﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Debug;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace DreamTeam.Match3.Levels
{
    public partial class LevelSystem : LevelSystem.ICheatApi
    {
        private CheatPanel _cheatPanel;
        private LevelPack _currentCheatLevelPack;
        private const float _spaceBetweenControllers = 20;
        private const float _spaceAfterHeader = 20;

        [Inject]
        private void Construct(CheatPanel cheatPanel)
        {
            _cheatPanel = cheatPanel;
            _cheatPanel.AddDrawer(CheatPanel.Category.Levels, DrawCheatGUI);
        }

        void ICheatApi.DrawLevelsCheats(ref LevelPack currentLevelPack, Action<LevelSettings> levelGuiCallback)
        {
            if (currentLevelPack != null)
            {
                if (GUILayout.Button("Back to Level Packs"))
                {
                    currentLevelPack = null;
                    return;
                }

                GUILayout.Space(_spaceAfterHeader);

                foreach (LevelSettings level in currentLevelPack.Levels)
                {
                    levelGuiCallback?.Invoke(level);
                }
            }
            else
            {
                foreach (BaseLevelPackController controller in _controllers)
                {
                    GUIStyle centeredLabelStyle = new GUIStyle(GUI.skin.GetStyle("Label"));
                    centeredLabelStyle.alignment = TextAnchor.UpperCenter;
                    centeredLabelStyle.fontStyle = FontStyle.Bold;

                    GUILayout.BeginVertical();
                    GUILayout.Label($"{controller.Type} mode", centeredLabelStyle);

                    GUILayout.Space(GUI.skin.label.lineHeight * 0.5f);
                    GUILayout.Label("Change current state:");
                    var controllerApi = controller as BaseLevelPackController.ICheatGUI;
                    controllerApi.DrawCheatGUILayout();

                    GUILayout.Space(GUI.skin.label.lineHeight * 0.5f);
                    GUILayout.Label("Play level pack:");
                    foreach (LevelPack levelPack in controller.AllLevelPacks)
                    {
                        if (GUILayout.Button(levelPack.name))
                        {
                            currentLevelPack = levelPack;
                            return;
                        }
                    }

                    GUILayout.Space(_spaceBetweenControllers);
                    GUILayout.EndVertical();
                }
            }
        }

        private void DrawCheatGUI(Rect areaRect)
        {
            LevelSettings loadedLevel = FindLevel(SceneManager.GetActiveScene());
            if (loadedLevel != null)
            {
                GUILayout.BeginHorizontal();

                BaseLevelPackController controller;
                LevelPack pack;
                FindLevelInfo(loadedLevel, out controller, out pack);
                int idx = pack.FindLevelIndex(loadedLevel);

                if (idx > 0 && GUILayout.Button("Previous"))
                {
                    idx--;
                }
                else if (GUILayout.Button("Replay"))
                {
                    // Do nothing
                }
                else if (idx < pack.Levels.Length - 1 && GUILayout.Button("Next"))
                {
                    idx++;
                }
                else
                {
                    idx = -1;
                }

                if (idx >= 0)
                {
                    _cheatPanel.Hide();
                    LoadLevel(pack.GetLevel(idx));
                    return;
                }

                GUILayout.EndHorizontal();
                GUILayout.Space(_spaceAfterHeader);
            }

            ((ICheatApi)this).DrawLevelsCheats(
                    ref _currentCheatLevelPack,
                    level =>
                    {
                        if (GUILayout.Button(level.SceneName))
                        {
                            _cheatPanel.Hide();
                            LoadLevel(level);
                        }
                    }
                );
        }

        #region ICheatApi

        public interface ICheatApi
        {
            void DrawLevelsCheats(ref LevelPack currentLevelPack, Action<LevelSettings> levelGuiCallback);
        }

        #endregion
    }
}