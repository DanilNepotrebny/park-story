﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DreamTeam.Match3.Levels
{
    public partial class LevelPack : ScriptableObject
    {
        [SerializeField] private LevelBackgroundSettings _backgroundSettings;
        [SerializeField] private LevelSettings[] _levels;

        public LevelBackgroundSettings BackgroundSettings => _backgroundSettings;
        public LevelSettings[] Levels => _levels;

        public LevelSettings GetLevel(int currentIndex)
        {
            if (currentIndex < _levels.Length)
            {
                return _levels[currentIndex];
            }

            return null;
        }

        public int FindLevelIndex(Scene scene)
        {
            return Array.FindIndex(_levels, settings => settings.ScenePath == scene.path);
        }

        public int FindLevelIndex(LevelSettings currentLevel)
        {
            return Array.FindIndex(_levels, settings => settings == currentLevel);
        }

        public LevelSettings FindLevel(Scene scene)
        {
            int idx = FindLevelIndex(scene);
            if (idx >= 0)
            {
                return GetLevel(idx);
            }

            return null;
        }

        public bool Contains(LevelSettings level)
        {
            return _levels.Contains(level);
        }

        public string GetLevelId(LevelSettings level)
        {
            return GetLevelId(FindLevelIndex(level));
        }

        public string GetLevelId(int index)
        {
            return $"level{index + 1}";
        }
    }
}