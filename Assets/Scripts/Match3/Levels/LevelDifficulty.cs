﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Match3.Levels
{
    [CreateAssetMenu(menuName = "Match 3/Levels/Difficulty settings")]
    public class LevelDifficulty : ScriptableObject
    {
    }
}