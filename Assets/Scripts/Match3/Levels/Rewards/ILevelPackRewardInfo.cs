﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory;

namespace DreamTeam.Match3.Levels.Rewards
{
    public interface ILevelPackRewardInfo
    {
        bool IsTaken { get; }
        int LevelNumber { get; }
        ProductPack ProductPack { get; }

        event Action<ILevelPackRewardInfo> Taken;
    }
}