﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Inventory;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.Levels.Rewards
{
    [RequireComponent(typeof(EndlessGrindLevelPackController))]
    public class EndlessGrindLevelPackRewardsController : MonoBehaviour, ILevelPackRewardsController, ISynchronizable, IInitable
    {
        [SerializeField] private List<LevelPackReward> _rewards = new List<LevelPackReward>();

        [Inject] private LevelSystem _levelSystem;
        [Inject] private SaveGroup _saveGroup;
        [Inject] private ProductPackInstanceContainer _productPackInstanceContainer;

        private int _lastDisplayedLevelIndex;

        public bool IsRewardsAwailable => _levelSystem.ActiveController is EndlessGrindLevelPackController;
        public int LastDisplayedLevelIndex => _lastDisplayedLevelIndex;

        public IReadOnlyCollection<ILevelPackRewardInfo> AllRewards =>
            _rewards.ConvertAll(item => item as ILevelPackRewardInfo);

        private EndlessGrindLevelPackController _levelPackController => GetComponent<EndlessGrindLevelPackController>();

        public void UpdateLastDisplayedLevelIndex()
        {
            _lastDisplayedLevelIndex = _levelPackController.CurrentLevelIndex;
        }

        public event Action<IDeferredInvocationHandle> GivingRewards;

        public bool TryGiveRewards()
        {
            LevelPackReward reward = _rewards.FirstOrDefault(item => item.ShouldBeGiven);
            if (reward != null)
            {
                using (var handle = new DeferredInvocation(() => GiveReward(reward)).Start())
                {
                    GivingRewards?.Invoke(handle);
                }

                return true;
            }

            return false;
        }

        public void Init()
        {
            _rewards.Sort((A, B) => A.LevelNumber.CompareTo(B.LevelNumber));
            _saveGroup.RegisterSynchronizable("EndlessGrindLevelPackRewardsController", this);
        }

        protected void Awake()
        {
            _levelPackController.CurrentLevelAdvanced += OnCurrentLevelAdvanced;
        }

        protected void OnDestroy()
        {
            _levelPackController.CurrentLevelAdvanced -= OnCurrentLevelAdvanced;
        }

        private void GiveReward(LevelPackReward reward)
        {
            reward.Taken += OnRewardTaken;
            reward.ShouldBeGiven = false;
            reward.IsTaken = true;
            reward.Give(_productPackInstanceContainer);
        }

        private void OnRewardTaken(ILevelPackRewardInfo reward)
        {
            reward.Taken -= OnRewardTaken;
            if (_rewards.All(item => item.IsTaken))
            {
                _rewards.ForEach(item => item.IsTaken = false);
            }
        }

        private void OnCurrentLevelAdvanced(int previousIndex, int currentIndex)
        {
            _rewards.ForEach(item => item.ShouldBeGiven = previousIndex + 1 > item.LevelNumber && !item.IsTaken);
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncInt("lastDisplayedLevelIndex", ref _lastDisplayedLevelIndex);
            state.SyncObjectList("rewards", _rewards);
        }

        [Serializable]
        private class LevelPackReward : ISynchronizable, ILevelPackRewardInfo
        {
            [SerializeField] private int _levelNumber;
            [SerializeField, RequiredField] private ProductPack _productPack;

            private bool _isTaken;
            private bool _shouldBeGiven;
            private ProductPack.Instance _productPackInstance;

            public int LevelNumber => _levelNumber;
            public ProductPack ProductPack => _productPack;

            public bool IsTaken
            {
                get { return _isTaken; }
                set { _isTaken = value; }
            }

            public bool ShouldBeGiven
            {
                get { return _shouldBeGiven; }
                set { _shouldBeGiven = value; }
            }

            public event Action<ILevelPackRewardInfo> Taken;

            public void Sync(State state)
            {
                state.SyncBool("isTaken", ref _isTaken);
                state.SyncBool("shouldBeGiven", ref _shouldBeGiven);
            }

            public void Give(ProductPackInstanceContainer productPackInstanceContainer)
            {
                _productPackInstance = productPackInstanceContainer.GetInstance(_productPack);
                _productPackInstance.Given += OnProductPackGiven;
                _productPackInstance.GiveForFree(GamePlace.EndlessGrind);
            }

            private void OnProductPackGiven(ProductPack.Instance pack, GamePlace gamePlace)
            {
                _productPackInstance.Given -= OnProductPackGiven;
                _productPackInstance = null;
                _isTaken = true;
                _shouldBeGiven = false;
                Taken?.Invoke(this);
            }
        }
    }
}