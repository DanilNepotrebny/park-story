﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;

namespace DreamTeam.Match3.Levels.Rewards
{
    public interface ILevelPackRewardsController
    {
        bool IsRewardsAwailable { get; }
        int LastDisplayedLevelIndex { get; }
        IReadOnlyCollection<ILevelPackRewardInfo> AllRewards { get; }
        void UpdateLastDisplayedLevelIndex();
    }
}