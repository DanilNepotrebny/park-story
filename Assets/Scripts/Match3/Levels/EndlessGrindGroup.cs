﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.Levels
{
    [Serializable]
    public partial class EndlessGrindGroup : MonoBehaviour, ISynchronizable
    {
        [SerializeField, RequiredField] private List<LevelPack> _chains = new List<LevelPack>(2);

        private List<string> _openedChains = new List<string>();
        private int _currentLevelIndex;

        public LevelSettings CurrentLevel => CurrentChain.GetLevel(_currentLevelIndex);
        public int CurrentLevelIndex => _currentLevelIndex;
        public LevelPack CurrentChain => GetCurrentChain(_openedChains);

        public IReadOnlyCollection<LevelPack> Chains => _chains;

        public event BaseLevelPackController.LevelAdvanceDelegate CurrentLevelAdvanced;

        public void Sync(State state)
        {
            SyncOpenedChains(state, ref _openedChains);
            SyncCurrentLevelIndex(state, CurrentChain, ref _currentLevelIndex);
        }

        public bool Advance()
        {
            if (!AdvanceCurrentLevel())
            {
                AdvanceCurrentChain();
                return false;
            }

            return true;
        }

        public LevelSettings LoadCurrentLevelFromState(State state)
        {
            List<string> loadedOpenedChains = new List<string>();
            SyncOpenedChains(state, ref loadedOpenedChains);
            LevelPack loadedCurrentChain = GetCurrentChain(loadedOpenedChains);

            int loadedCurrentLevelIndex = 0;
            SyncCurrentLevelIndex(state, loadedCurrentChain, ref loadedCurrentLevelIndex);

            return loadedCurrentChain?.GetLevel(loadedCurrentLevelIndex);
        }

        public bool TryGetLevelId(LevelSettings level, out string levelId)
        {
            for (int i = 0; i < _chains.Count; i++)
            {
                LevelPack pack = _chains[i];
                if (pack.Contains(level))
                {
                    levelId = $"ch{i + 1}_{pack.GetLevelId(level)}";
                    return true;
                }
            }

            levelId = "";
            return false;
        }

        protected void Awake()
        {
            FillOpenedChains(null);
        }

        protected void OnValidate()
        {
            if (_chains.Count < 2)
            {
                UnityEngine.Debug.LogAssertion(
                    $"[{nameof(EndlessGrindGroup)}] Pls, specify 2 or more chains for {name}");
                _chains.Capacity = 2;
            }
        }

        private void AdvanceCurrentChain()
        {
            string completedChain = CurrentChain?.name;
            _openedChains.Remove(completedChain);

            if (_openedChains.Count == 0)
            {
                FillOpenedChains(completedChain);
            }
        }

        private bool AdvanceCurrentLevel()
        {
            SetCurrentLevelIndex(++_currentLevelIndex);
            return _currentLevelIndex > 0;
        }

        private void SetCurrentLevelIndex(int index)
        {
            int previousIndex = _currentLevelIndex;
            _currentLevelIndex = Mathf.Max(0, index);
            if (CurrentChain == null || _currentLevelIndex >= CurrentChain.Levels.Length)
            {
                _currentLevelIndex = 0;
            }
            CurrentLevelAdvanced?.Invoke(previousIndex, _currentLevelIndex);
        }

        private void FillOpenedChains(string lastCompetedChain)
        {
            foreach (LevelPack chain in _chains)
            {
                if (chain.name != lastCompetedChain)
                {
                    _openedChains.Add(chain.name);
                }
            }

            _openedChains.RandomShuffle();
        }

        private void SyncOpenedChains(State state, ref List<string> openedChains)
        {
            state.SyncStringList("openedChains", ref openedChains, true);
        }

        private void SyncCurrentLevelIndex(State state, LevelPack chain, ref int currentLevelIndex)
        {
            state.SyncInt("currentLevelIndex", ref currentLevelIndex);
            currentLevelIndex %= chain.Levels.Length;
        }

        private LevelPack GetCurrentChain(List<string> openedChains)
        {
            return openedChains != null ? _chains.FirstOrDefault(item => item.name == openedChains.Last()) : null;
        }
    }
}