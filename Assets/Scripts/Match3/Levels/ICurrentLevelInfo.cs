﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3.Levels
{
    public interface ICurrentLevelInfo
    {
        LevelSettings CurrentLevel { get; }
        int CurrentLevelIndex { get; }
        LevelPack CurrentLevelPack { get; }
        string CurrentLevelId { get; }
    }
}