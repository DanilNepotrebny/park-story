﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Cloud;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils.FileFolders;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Match3.Levels
{
    public abstract partial class BaseLevelPackController : MonoBehaviour, ISynchronizable, ICurrentLevelInfo, IInitable
    {
        [FormerlySerializedAs("_order"), SerializeField]
        private LevelPackControllerType _type;

        [Inject] private DiContainer _container;

        [Inject] private SaveManager _saveManager;
        [Inject] private SaveGroup _saveGroup;
        [Inject] private CloudSaveManager _cloudSaveManager;

        public LevelPackControllerType Type => _type;

        public abstract LevelSettings CurrentLevel { get; }
        public abstract int CurrentLevelIndex { get; }
        public abstract LevelPack CurrentLevelPack { get; }
        public abstract string CurrentLevelId { get; }

        public abstract IEnumerable<LevelPack> AllLevelPacks { get; }

        protected abstract string SaveID { get; }

        [Inject]
        protected LevelSystem LevelSystem { get; }

        public void Init()
        {
            _saveGroup.RegisterSynchronizable(SaveID, this);
            _saveGroup.Loaded += OnSaveGroupLoaded;

            foreach (LevelPack pack in AllLevelPacks)
            {
                _container.Inject(pack);
            }

            LevelSystem.RegisterLevelPackController(this);
        }

        public void OnLevelSucceeded(LevelSettings level)
        {
            if (CurrentLevel == level)
            {
                Advance();

                _saveManager.Save();
                _cloudSaveManager.SendSave();
            }
        }

        public LevelSettings LoadCurrentLevelFromSave(IFileFolder save)
        {
            State state = _saveGroup.CreateDeserializationState(save, this);
            return LoadCurrentLevelFromState(state);
        }

        public abstract string GetLevelId(LevelSettings currentLevel);

        public LevelSettings FindLevel(Scene scene)
        {
            foreach (LevelPack pack in AllLevelPacks)
            {
                LevelSettings level = pack.FindLevel(scene);
                if (level != null)
                {
                    return level;
                }
            }
            return null;
        }

        public LevelPack FindLevelPack(LevelSettings level)
        {
            return AllLevelPacks.FirstOrDefault(pack => pack.Contains(level));
        }

        public int FindLevelIndex(LevelSettings currentLevel)
        {
            foreach (LevelPack pack in AllLevelPacks)
            {
                int idx = pack.FindLevelIndex(currentLevel);
                if (idx >= 0)
                {
                    return idx;
                }
            }
            return -1;
        }

        public abstract void Sync(State state);

        protected abstract void Advance();
        protected abstract LevelSettings LoadCurrentLevelFromState(State state);

        protected virtual void OnSaveLoaded()
        {
            // DO NOTHING
        }

        private void OnSaveGroupLoaded()
        {
            _saveGroup.Loaded -= OnSaveGroupLoaded;
            OnSaveLoaded();
        }

        public delegate void LevelAdvanceDelegate(int previousIndex, int currentIndex);
    }
}