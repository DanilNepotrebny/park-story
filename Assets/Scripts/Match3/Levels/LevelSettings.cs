﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory.Items;
using DreamTeam.Match3.Requirements;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.Levels
{
    /// <summary>
    /// Level settings implementation
    /// </summary>
    public partial class LevelSettings : ScriptableObject
    {
        public const string GoalInstancesId = "GoalInstances";
        public const string LimitationInstancesId = "LimitationInstances";

        [SerializeField, HideInInspector] private List<Requirement> _goals = new List<Requirement>();
        [SerializeField, HideInInspector] private List<Requirement> _limitations = new List<Requirement>();

        [SerializeField] private SceneReference _scene = new SceneReference();
        [SerializeField, RequiredField] private LevelDifficulty _difficulty;
        [SerializeField, Space] private int _magicHatMaxCharge = 100;
        [SerializeField] private int _moneyRewardCount = 1;
        [SerializeField, RequiredField] private CountedItem _mainRewardType;
        [SerializeField] private int _mainRewardAmount = 1;
        [SerializeField] private Booster _tipBooster;
        [SerializeField] private int[] _randomSeeds;

        public IEnumerable<Requirement> Goals => _goals;

        public string SceneName => _scene.Name;
        public string ScenePath => _scene.AssetPath;
        public LevelDifficulty Difficulty => _difficulty;
        public int MagicHatMaxCharge => _magicHatMaxCharge;
        public int MoneyRewardCount => _moneyRewardCount;
        public CountedItem MainRewardType => _mainRewardType;

        public int MainRewardAmount => _mainRewardAmount;
        public Booster TipBooster => _tipBooster;
        public int[] RandomSeeds => _randomSeeds;

        public void InstallBindings(DiContainer container)
        {
            container.Bind<IReadOnlyList<Requirement.Instance>>().WithId(GoalInstancesId).FromMethod(
                context =>
                {
                    var instances = new List<Requirement.Instance>();
                    foreach (Requirement goal in _goals)
                    {
                        instances.Add(goal.CreateInstance(container));
                    }
                    return instances;
                }
            ).AsCached();

            container.Bind<IReadOnlyList<Requirement.Instance>>().WithId(LimitationInstancesId).FromMethod(
                context =>
                {
                    var instances = new List<Requirement.Instance>();
                    foreach (Requirement limitation in _limitations)
                    {
                        instances.Add(limitation.CreateInstance(container));
                    }
                    return instances;
                }
            ).AsCached();
        }

        public void DisposeBindings(DiContainer container)
        {
            DisposeRequirementInstances(container.ResolveId<IReadOnlyList<Requirement.Instance>>(LimitationInstancesId));
            DisposeRequirementInstances(container.ResolveId<IReadOnlyList<Requirement.Instance>>(GoalInstancesId));
        }

        protected void OnValidate()
        {
            _moneyRewardCount = Mathf.Max(1, _moneyRewardCount);
            _mainRewardAmount = Mathf.Max(1, _mainRewardAmount);
        }

        private void DisposeRequirementInstances(IReadOnlyList<Requirement.Instance> requirementInstances)
        {
            foreach (Requirement.Instance instance in requirementInstances)
            {
                instance.Dispose();
            }
        }
    }
}