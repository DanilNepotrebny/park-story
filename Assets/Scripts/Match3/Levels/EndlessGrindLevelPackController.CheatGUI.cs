﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3.Levels
{
    public partial class EndlessGrindLevelPackController
    {
        protected override void DrawCheatGUILayout()
        {
            DrawCurrentGroupIndex();
            (_currentGroup as ICheatGUI).DrawCheatGUILayout();
        }

        private void DrawCurrentGroupIndex()
        {
            int currentGroupNumber = _currentGroupIndex + 1;
            if (DrawCheatCounter(ref currentGroupNumber, "Group number", _currentGroup.name))
            {
                SetCurrentGroupIndex(currentGroupNumber - 1);
            }
        }
    }
}