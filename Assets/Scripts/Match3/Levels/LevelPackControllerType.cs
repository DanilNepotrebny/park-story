﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3.Levels
{
    public enum LevelPackControllerType
    {
        Main,
        Test,
        EndlessGrind
    }

    public static class LevelPackControllerTypeExtensions
    {
        public static string ToAnalyticsId(this LevelPackControllerType self)
        {
            switch (self)
            {
                case LevelPackControllerType.Main:
                    return "main";
                case LevelPackControllerType.EndlessGrind:
                    return "endless_grind";
            }

            UnityEngine.Debug.LogError($"Ivalid value for {self.GetType().Name} enum.");
            return "";
        }
    }
}