﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DreamTeam.Utils;

namespace DreamTeam.Match3.Levels
{
    public partial class LevelPack : LevelPack.IEditorApi
    {
        void IEditorApi.AddLevel(LevelSettings level)
        {
            EditorUtils.RecordObjectUndo(this, "Level added");

            if (_levels != null)
            {
                if (_levels.Contains(level))
                {
                    return;
                }

                Array.Resize(ref _levels, _levels.Length + 1);
            }
            else
            {
                _levels = new LevelSettings[1];
            }

            _levels[_levels.Length - 1] = level;
            Array.Sort(_levels, new LevelsComparer());
        }

        void IEditorApi.RemoveLevel(string levelScenePath)
        {
            EditorUtils.RecordObjectUndo(this, "Level removed");
            _levels = _levels.Where(settings => settings != null && ((LevelSettings.IEditorAPI)settings).SafeScenePath != levelScenePath).ToArray();
            Array.Sort(_levels, new LevelsComparer());
        }

        #region IEditorApi

        public interface IEditorApi
        {
            void AddLevel(LevelSettings level);
            void RemoveLevel(string levelScenePath);
        }

        #endregion

        #region Inner types

        private class LevelsComparer : IComparer<LevelSettings>
        {
            private const string _levelNameRegexPattern = "(.*?)([0-9]+)";
            private const int _nameGroupIdx = 1;
            private const int _indexGroupIdx = 2;

            private Regex _regex = new Regex(_levelNameRegexPattern);

            public int Compare(LevelSettings x, LevelSettings y)
            {
                if (x == null && y == null)
                {
                    return 0;
                }

                if (x == null)
                {
                    return -1;
                }

                if (y == null)
                {
                    return 1;
                }

                string xName, yName;
                int xIndex, yIndex;

                ParseLevelName(x, out xName, out xIndex);
                ParseLevelName(y, out yName, out yIndex);

                int result = xName.CompareTo(yName);
                if (result != 0)
                {
                    return result;
                }

                return xIndex.CompareTo(yIndex);
            }

            private void ParseLevelName(LevelSettings level, out string name, out int index)
            {
                Match match = _regex.Match(level.name);
                Group nameGroup = match.Groups[_nameGroupIdx];
                Group indexGroup = match.Groups[_indexGroupIdx];

                name = nameGroup.Value;
                index = int.Parse(indexGroup.Value);
            }
        }

        #endregion
    }
}

#endif