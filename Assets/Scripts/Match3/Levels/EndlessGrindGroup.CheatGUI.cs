﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Match3.Levels
{
    public partial class EndlessGrindGroup : BaseLevelPackController.ICheatGUI
    {
        private void DrawCurrentChain()
        {
            int chainNumber = _chains.IndexOf(CurrentChain) + 1;
            if (BaseLevelPackController.DrawCheatCounter(ref chainNumber, "Chain number", CurrentChain.name))
            {
                SetCurrentChain(_chains[Mathf.Max(0, chainNumber - 1) % _chains.Count]);
            }
        }

        private void DrawCurrentLevelIndex()
        {
            int currentLevelNumber = _currentLevelIndex + 1;
            if (BaseLevelPackController.DrawCheatCounter(ref currentLevelNumber, "Level number", CurrentLevel.name))
            {
                SetCurrentLevelIndex(currentLevelNumber - 1);
            }
        }

        private void SetCurrentChain(LevelPack chain)
        {
            _openedChains.Remove(chain.name);
            _openedChains.Add(chain.name);
        }

        void BaseLevelPackController.ICheatGUI.DrawCheatGUILayout()
        {
            DrawCurrentChain();
            DrawCurrentLevelIndex();
        }
    }
}