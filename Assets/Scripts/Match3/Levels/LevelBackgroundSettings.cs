﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3.Levels
{
    [CreateAssetMenu(menuName = "Match 3/Levels/Background Settings")]
    public class LevelBackgroundSettings : ScriptableObject
    {
        [SerializeField] private BackgroundsMap _backgrounds;

        protected void Awake()
        {
            SortBackgroundsByLevel();
        }

        public Sprite GetBackground(int levelIndex)
        {
            int maxBackgroundKey = _backgrounds[_backgrounds.Count - 1].Key;
            int mappedLevelIndex = levelIndex % maxBackgroundKey;
            return _backgrounds.Find(item => mappedLevelIndex < item.Key).Value;
        }

        [ContextMenu(nameof(SortBackgroundsByLevel))]
        private void SortBackgroundsByLevel()
        {
            _backgrounds.Sort((A, B) => A.Key.CompareTo(B.Key));
        }

        [Serializable]
        private class BackgroundsMap : KeyValueList<int, Sprite>
        {
        }
    }
}