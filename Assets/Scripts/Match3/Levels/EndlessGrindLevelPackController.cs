﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Synchronization.States;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3.Levels
{
    public partial class EndlessGrindLevelPackController : BaseLevelPackController
    {
        private int _currentGroupIndex;

        public override LevelSettings CurrentLevel => _currentGroup.CurrentLevel;
        public override int CurrentLevelIndex => _currentGroup.CurrentLevelIndex;
        public override LevelPack CurrentLevelPack => _currentGroup.CurrentChain;
        public override string CurrentLevelId => GetLevelId(CurrentLevel);

        public override IEnumerable<LevelPack> AllLevelPacks
        {
            get
            {
                var result = new List<LevelPack>();
                foreach (EndlessGrindGroup group in _groups)
                {
                    result.AddRange(group.Chains);
                }

                return result;
            }
        }

        protected override string SaveID => Type.ToString();

        private EndlessGrindGroup _currentGroup => _groups[_currentGroupIndex];

        private EndlessGrindGroup[] _groups
        {
            get
            {
                EndlessGrindGroup[] result = GetComponentsInChildren<EndlessGrindGroup>();
                Assert.IsTrue(
                        result.Length > 0,
                        $"[{nameof(EndlessGrindLevelPackController)}] " +
                        "Pls, create any {nameof(EndlessGrindGroup)} as child."
                    );

                return result;
            }
        }

        public event LevelAdvanceDelegate CurrentLevelAdvanced;

        public override string GetLevelId(LevelSettings level)
        {
            string levelId;
            for (int i = 0; i < _groups.Length; i++)
            {
                EndlessGrindGroup group = _groups[i];
                if (group.TryGetLevelId(level, out levelId))
                {
                    return $"{Type.ToAnalyticsId()}_gr{i + 1}_{levelId}";
                }
            }

            return "";
        }

        public override void Sync(State state)
        {
            SyncCurrentGroupIndex(state, ref _currentGroupIndex);
            state.SyncObjectList("groups", _groups);
        }

        protected void OnDestroy()
        {
            UnsubscribeCurrentGroup();
        }

        protected override void OnSaveLoaded()
        {
            SubscribeCurrentGroup();
        }

        protected override void Advance()
        {
            if (!_currentGroup.Advance())
            {
                UnsubscribeCurrentGroup();
                SetCurrentGroupIndex(++_currentGroupIndex);
                SubscribeCurrentGroup();
            }
        }

        private void SetCurrentGroupIndex(int index)
        {
            _currentGroupIndex = Mathf.Max(0, index) % _groups.Length;
        }

        protected override LevelSettings LoadCurrentLevelFromState(State state)
        {
            int loadedIndex = 0;
            SyncCurrentGroupIndex(state, ref loadedIndex);
            return _groups[loadedIndex].LoadCurrentLevelFromState(state);
        }

        private void SubscribeCurrentGroup()
        {
            _currentGroup.CurrentLevelAdvanced += OnCurrentLevelAdvanced;
        }

        private void UnsubscribeCurrentGroup()
        {
            _currentGroup.CurrentLevelAdvanced -= OnCurrentLevelAdvanced;
        }

        private void OnCurrentLevelAdvanced(int previousIndex, int currentIndex)
        {
            CurrentLevelAdvanced?.Invoke(previousIndex, currentIndex);
        }

        private void SyncCurrentGroupIndex(State state, ref int currentGroupIndex)
        {
            state.SyncInt("currentGroupIndex", ref currentGroupIndex);
            currentGroupIndex %= _groups.Length;
        }
    }
}