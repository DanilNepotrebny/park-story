﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using DreamTeam.Match3.Requirements;
using DreamTeam.Utils;

namespace DreamTeam.Match3.Levels
{
    public partial class LevelSettings : LevelSettings.IEditorAPI
    {
        string IEditorAPI.SafeScenePath => ((SceneReference.IEditorApi)_scene).SafeAssetPath;

        void IEditorAPI.AddGoal(Requirement goal)
        {
            if (goal != null && !_goals.Contains(goal))
            {
                _goals.Add(goal);
            }
        }

        void IEditorAPI.RemoveGoal(Requirement goal)
        {
            _goals.Remove(goal);
        }

        void IEditorAPI.SetSceneAssetPath(string path)
        {
            EditorUtils.RecordObjectUndo(this, "SetSceneAssetPath");
            ((SceneReference.IEditorApi)_scene).SetAssetPath(path);
        }

        #region Inner types

        public interface IEditorAPI
        {
            string SafeScenePath { get; }

            void SetSceneAssetPath(string path);
            void AddGoal(Requirement goal);
            void RemoveGoal(Requirement goal);
        }

        #endregion
    }
}

#endif