﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3.Levels
{
    public partial class SingleLevelPackController
    {
        protected override void DrawCheatGUILayout()
        {
            int currentLevelNumber = _currentIndex + 1;
            if (DrawCheatCounter(ref currentLevelNumber, "Level number", CurrentLevel?.name))
            {
                SetCurrentLevelIndex(currentLevelNumber - 1);
            }
        }
    }
}