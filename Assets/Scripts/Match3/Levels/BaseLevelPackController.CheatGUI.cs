// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Match3.Levels
{
    public abstract partial class BaseLevelPackController : BaseLevelPackController.ICheatGUI
    {
        public static bool DrawCheatCounter(ref int counter, string label, string resultLabel)
        {
            bool result = false;

            GUILayout.BeginHorizontal();
            GUILayout.Label(label, GUILayout.ExpandWidth(true), GUILayout.MaxWidth(400));

            string countText = GUILayout.TextField(counter.ToString(), GUILayout.Width(400));
            if (int.TryParse(countText, out counter))
            {
                result = true;
            }

            if (GUILayout.Button("+", GUILayout.Width(GUI.skin.label.lineHeight * 1.5f)))
            {
                counter++;
                result = true;
            }

            if (GUILayout.Button("-", GUILayout.Width(GUI.skin.label.lineHeight * 1.5f)))
            {
                counter--;
                result = true;
            }

            GUILayout.Label(resultLabel, GUILayout.ExpandWidth(true));
            GUILayout.EndHorizontal();

            return result;
        }

        protected abstract void DrawCheatGUILayout();

        void ICheatGUI.DrawCheatGUILayout()
        {
            GUILayout.BeginVertical();
            DrawCheatGUILayout();
            GUILayout.EndVertical();
        }

        public interface ICheatGUI
        {
            void DrawCheatGUILayout();
        }
    }
}