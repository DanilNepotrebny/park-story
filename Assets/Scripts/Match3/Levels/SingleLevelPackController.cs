﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Synchronization.States;
using UnityEngine;

namespace DreamTeam.Match3.Levels
{
    public partial class SingleLevelPackController : BaseLevelPackController
    {
        [SerializeField] private LevelPack _levelPack;

        private LevelPack[] _levelPacks;

        private int _currentIndex;

        public bool IsCompleted => _currentIndex >= _levelPack.Levels.Length;

        public override LevelSettings CurrentLevel => _levelPack.GetLevel(CurrentLevelIndex);
        public override int CurrentLevelIndex => _currentIndex;
        public override LevelPack CurrentLevelPack => _levelPack;

        public override string CurrentLevelId => GetLevelId(CurrentLevelIndex);

        public override IEnumerable<LevelPack> AllLevelPacks => _levelPacks ?? (_levelPacks = new[] { _levelPack });
        protected override string SaveID => Type.ToString();

        public event LevelAdvanceDelegate CurrentLevelAdvanced;
        public event Action Completed;

        public override string GetLevelId(LevelSettings level)
        {
            return $"{Type.ToAnalyticsId()}_{_levelPack.GetLevelId(level)}";
        }

        public string GetLevelId(int levelIndex)
        {
            return $"{Type.ToAnalyticsId()}_{_levelPack.GetLevelId(levelIndex)}";
        }

        public override void Sync(State state)
        {
            SyncCurrentLevel(state, ref _currentIndex);
        }

        protected override void Advance()
        {
            SetCurrentLevelIndex(++_currentIndex);

            if (_currentIndex >= _levelPack.Levels.Length)
            {
                Completed?.Invoke();
            }
        }

        private void SetCurrentLevelIndex(int index)
        {
            int previousIndex = _currentIndex;
            _currentIndex = Mathf.Clamp(index, 0, _levelPack.Levels.Length);
            CurrentLevelAdvanced?.Invoke(previousIndex, _currentIndex);
        }

        protected override LevelSettings LoadCurrentLevelFromState(State state)
        {
            int currentLevelIndex = 0;
            SyncCurrentLevel(state, ref currentLevelIndex);
            return _levelPack.GetLevel(currentLevelIndex);
        }

        private void SyncCurrentLevel(State state, ref int currentIndex)
        {
            state.SyncInt("currentIndex", ref currentIndex);
        }
    }
}