﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3
{
    /// <summary>
    /// Priority of <see cref="IImpactController"/> invocation. Higher priority means earlier invocation.
    /// </summary>
    public enum ImpactPriority
    {
        Chip,
        ChipContainer,
        Explosive,
        MagicHat,
        Replacement,
        AcquireAdditionalMoves,
        MagicHatTarget,
        ChipConsumptionDispatcher,
        GoalChipConsuming,
        SpecialMatch,
        HammerTarget,
        DoubleHat,
        FireworkMissiles,
        CarHarvesting,
        GumballMachineProjectilesSpawn,
    }
}