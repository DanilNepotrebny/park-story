﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using JetBrains.Annotations;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Determines that two specified chips can be combined via swapping. Each controller is supposed to process
    /// own combination case.
    /// </summary>
    public interface ICombinationController
    {
        /// <summary>
        /// Combines specified chips. Will be called only if <see cref="HasCombination"/> returned true for
        /// this pair of chips.
        /// </summary>
        void AcceptCombination([NotNull] ChipComponent mainChip, [CanBeNull] ChipComponent additionalChip);

        /// <summary>
        /// Determines whether this combination controller is supposed to accept specified combination of chips. For
        /// two arbitrary chips <see cref="CombinationSystem"/> will invoke this method twice: with first chip
        /// as a main and second as an additional, and vice versa if this is possible. Main chip is guaranteed to not
        /// be null.
        /// </summary>
        bool HasCombination([NotNull] ChipComponent mainChip, [CanBeNull] ChipComponent additionalChip);

        List<Impact> ScheduleCombinationImpact(
            [NotNull] ChipComponent mainChip,
            [CanBeNull] ChipComponent additionalChip);
    }
}