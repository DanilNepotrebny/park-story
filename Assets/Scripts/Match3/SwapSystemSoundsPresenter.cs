﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Audio;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(SwapSystem), typeof(AudioSystemPlayer))]
    public class SwapSystemSoundsPresenter : MonoBehaviour
    {
        [SerializeField, RequiredField] private AudioSource _swapingInSound;
        [SerializeField, RequiredField] private AudioSource _swapingBackSound;

        private SwapSystem _swapSystem => GetComponent<SwapSystem>();
        private AudioSystemPlayer _audioPlayer => GetComponent<AudioSystemPlayer>();

        protected void Awake()
        {
            _swapSystem.SwapingIn += OnSwapingIn;
            _swapSystem.SwapingBack += OnSwapingBack;
        }

        protected void OnDestroy()
        {
            _swapSystem.SwapingIn -= OnSwapingIn;
            _swapSystem.SwapingBack -= OnSwapingBack;
        }

        private void OnSwapingBack()
        {
            _audioPlayer.Play(_swapingInSound);
        }

        private void OnSwapingIn()
        {
            _audioPlayer.Play(_swapingBackSound);
        }
    }
}