﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.RendererSorting;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Rendering;
using Zenject;

namespace DreamTeam.Match3
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(DestroyNotifier), typeof(SortingGroup))]
    public class ChipPresenter : MonoBehaviour, IImpactController, ISortingOptionsProvider, IGridPositionProvider
    {
        [SerializeField] private ChipComponent _chip;
        [SerializeField] private GameObject _consumptionEffect;
        [SerializeField] private GameObject _impactEffect;
        [SerializeField] private string _impactTrigger = "OnImpact";
        [SerializeField] private string _impactAnimatorState = "Impact";
        [SerializeField] private string _consumptionAnimatorState = "Consuming";
        [SerializeField] private string _lockedParameter = "IsLocked";
        [SerializeField] private string _healthParameter = "Health";
        [SerializeField] private string _consumptionEffectEvent = "ConsumptionEffect";
        [SerializeField] private string _consumptionReleaseCellEvent = "ConsumptionReleaseCell";
        [SerializeField] private string _animationDirectionXParameter = "AnimationDirectionX";
        [SerializeField] private string _animationDirectionYParameter = "AnimationDirectionY";
        [SerializeField] private string _idleTypeParamName = "IdleType";
        [SerializeField] private ChipAnimatorContoller _animatorContoller;

        [SerializeField] private string _appearingTrigger = "OnAppear";
        [SerializeField] private string _containerImpactTrigger;
        [SerializeField] private string _containerImpactAnimatorState;

        [Inject] private Instantiator _instantiator;

        private bool _wasStarted;

        private Renderer _renderer;
        private SortingGroup _sortingGroup;
        private MaterialPropertyBlock _propertyBlock;

        private IDeferredInvocationHandle _impactEndHandle;
        private IDeferredInvocationHandle _containerImpactEndHandle;

        public ChipComponent ChipComponent => _chip;

        public ChipAnimatorContoller ChipAnimatorController => _animatorContoller;

        public SortingOptions SortingOptions { get; private set; }

        public Vector2Int GridPosition => _chip.Cell != null ? _chip.Cell.GridPosition : new Vector2Int();

        public DestroyNotifier DestroyNotifier { get; private set; }

        public ChipTeleportationPresenter TeleportationPresenter { get; private set; }

        public float FallingSpeed
        {
            get
            {
                var fallingPresenter = GetComponent<ChipFallingPresenter>();
                return fallingPresenter != null ? fallingPresenter.CurrentSpeed : 0f;
            }
        }

        public event Action<ChipComponent> DoubleTapped;

        public event Action Appearing;

        public bool SetAnimatorEnabled(bool isEnabled)
        {
            if (_animatorContoller != null)
            {
                _animatorContoller.SetAnimatorEnabled(isEnabled);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Changes current renderer material. Note that animator will be disabled until you call <see cref="ResetMaterial"/> because it conflicts
        /// with material changing.
        /// </summary>
        public void SetMaterial(Material material)
        {
            SetAnimatorEnabled(false);
            _renderer.material = material;
        }

        /// <summary>
        /// Resets material to the default one and enables animator.
        /// </summary>
        public void ResetMaterial()
        {
            // Animator will reset material, so all we need to do is to set animator enabled.
            SetAnimatorEnabled(true);
        }

        /// <summary>
        /// Sets per renderer material parameter with specified name.
        /// </summary>
        public void SetMaterialFloat(string propertyName, float value)
        {
            _renderer.GetPropertyBlock(_propertyBlock);
            _propertyBlock.SetFloat(propertyName, value);
            _renderer.SetPropertyBlock(_propertyBlock);
        }

        public void SetMaterialFloat(int propertyID, float value)
        {
            _renderer.GetPropertyBlock(_propertyBlock);
            _propertyBlock.SetFloat(propertyID, value);
            _renderer.SetPropertyBlock(_propertyBlock);
        }

        /// <summary>
        /// Used for handling of double tap. Don't call for any other reasons.
        /// </summary>
        public void OnDoubleTapped()
        {
            DoubleTapped?.Invoke(_chip);
        }

        public bool AddAnimationStateExitHandler(string stateName, Action handler)
        {
            if (_animatorContoller != null)
            {
                _animatorContoller.AddAnimationStateExitHandler(stateName, handler);
                return true;
            }

            return false;
        }

        public bool RemoveAnimationStateExitHandler(string stateName, Action handler)
        {
            if (_animatorContoller != null)
            {
                _animatorContoller.RemoveAnimationStateExitHandler(stateName, handler);
                return true;
            }

            return false;
        }

        public bool AddAnimationStateEnterHandler(string stateName, Action handler)
        {
            if (_animatorContoller != null)
            {
                _animatorContoller.AddAnimationStateEnterHandler(stateName, handler);
                return true;
            }

            return false;
        }

        public bool RemoveAnimationStateEnterHandler(string stateName, Action handler)
        {
            if (_animatorContoller != null)
            {
                _animatorContoller.RemoveAnimationStateEnterHandler(stateName, handler);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Sets animator trigger with specified name.
        /// </summary>
        public bool SetTrigger(string triggerName)
        {
            if (_animatorContoller != null && _animatorContoller.IsAnimatorEnabled())
            {
                _animatorContoller.SetTrigger(triggerName);
                return true;
            }

            return false;
        }

        public bool SetBool(string paramName, bool value)
        {
            if (_animatorContoller != null && _animatorContoller.IsAnimatorEnabled())
            {
                _animatorContoller.SetBool(paramName, value);
                return true;
            }

            return false;
        }

        /// <summary> Set animator's float parameter value </summary>
        public bool SetFloat(string paramName, float value)
        {
            if (_animatorContoller != null && _animatorContoller.IsAnimatorEnabled())
            {
                _animatorContoller.SetFloat(paramName, value);
                return true;
            }

            return false;
        }

        public bool SetAnimationNormalizedTime(float time)
        {
            if (_animatorContoller != null && _animatorContoller.IsAnimatorEnabled())
            {
                _animatorContoller.SetAnimationNormalizedTime(time);
                return true;
            }

            return false;
        }

        public bool SetAnimationDirection(Direction animationDirection)
        {
            if (_animatorContoller != null && _animatorContoller.IsAnimatorEnabled())
            {
                Vector2 directionOffset = animationDirection.GetOffset();
                _animatorContoller.SetFloat(_animationDirectionXParameter, directionOffset.x);
                _animatorContoller.SetFloat(_animationDirectionYParameter, directionOffset.y);
                return true;
            }

            return false;
        }

        public void OnContainerImpacting(IDeferredInvocationHandle impactEndHandle)
        {
            if (_animatorContoller == null)
            {
                return;
            }

            FinishContainerImpact();
            _containerImpactEndHandle = impactEndHandle.Lock();

            _animatorContoller.SetTrigger(_containerImpactTrigger);
        }

        public void FinishContainerImpact()
        {
            // we have to cache field cause after unlock we might receive new OnContainerImpacting event
            // (for example, when match goes in a row - aka match in ice) that led to a new FinishContainerImpact() call
            // which tries to unlock the same handle second time
            IDeferredInvocationHandle endHandle = _containerImpactEndHandle;
            _containerImpactEndHandle = null;

            endHandle?.Unlock();
        }

        public void SetIdleAnimationType(IdleAnimationType type)
        {
            SetFloat(_idleTypeParamName, (float)type);
        }

        public void Appear()
        {
            Appearing?.Invoke();

            _animatorContoller?.SetTrigger(_appearingTrigger);
        }

        public void ResetPosition()
        {
            transform.localPosition = Vector3.zero;
        }

        protected void Awake()
        {
            _animatorContoller?.AddAnimationEventHandler(_consumptionEffectEvent, CreateConsumptionEffect);
            _animatorContoller?.AddAnimationEventHandler(_consumptionReleaseCellEvent, ReleaseCell);

            DestroyNotifier = GetComponent<DestroyNotifier>();
            _renderer = GetComponentInChildren<Renderer>();
            _sortingGroup = GetComponent<SortingGroup>();
            _propertyBlock = new MaterialPropertyBlock();

            SortingOptions = _instantiator.Instantiate<SortingOptions>(
                _sortingGroup.sortingLayerID,
                _sortingGroup.sortingOrder);
            SortingOptions.SortingLayerChanged += OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged += OnSortingOrderChanged;

            _chip.AddImpactController(this);
            _chip.Moving += OnChipMoving;

            TeleportationPresenter = GetComponent<ChipTeleportationPresenter>();

            AddAnimationStateExitHandler(_impactAnimatorState, FinishImpact);
            AddAnimationStateExitHandler(_consumptionAnimatorState, gameObject.Dispose);
            AddAnimationStateExitHandler(_containerImpactAnimatorState, FinishContainerImpact);
        }

        protected void Start()
        {
            _wasStarted = true;
            OnEnable();

            UpdateSortingOptionsParent();
        }

        protected void OnEnable()
        {
            if (!_wasStarted)
            {
                return;
            }

            _chip.CellChanged += OnCellChanged;
            _chip.ContainerCellChanged += OnContainerCellChanged;
            UpdateLockedState();

            _chip.HealthController.ValueChanged += UpdateHealth;
            UpdateHealth(_chip.Health);
        }

        protected void OnDisable()
        {
            _chip.ContainerCellChanged -= OnContainerCellChanged;
            _chip.CellChanged -= OnCellChanged;
            _chip.HealthController.ValueChanged -= UpdateHealth;
        }

        protected void OnDestroy()
        {
            if (_chip != null)
            {
                _chip.Moving -= OnChipMoving;
            }

            SortingOptions.SortingLayerChanged -= OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged -= OnSortingOrderChanged;
            SortingOptions.Dispose();
            SortingOptions = null;

            FinishImpact();
            FinishContainerImpact();
        }

        private void UpdateHealth(int health)
        {
            _animatorContoller?.SetInteger(_healthParameter, health);
        }

        private void OnChipMoving(ChipComponent.MovingType type, IDeferredInvocationHandle handle)
        {
            if (type == ChipComponent.MovingType.Immediate)
            {
                transform.position = _chip.transform.position;
            }
        }

        private void OnContainerCellChanged(CellComponent newCell, CellComponent prevCell)
        {
            UpdateLockedState();
            UpdateSortingOptionsParent();
        }

        private void OnCellChanged(ChipComponent chip, CellComponent prevCell)
        {
            UpdateLockedState();
        }

        private void UpdateLockedState()
        {
            bool isLocked = _chip.Cell != null && _chip.ContainerCell == null;
            _animatorContoller?.SetBool(_lockedParameter, isLocked);
        }

        private void CreateConsumptionEffect()
        {
            if (_consumptionEffect != null)
            {
                _instantiator.Instantiate(_consumptionEffect, transform.position);
            }
        }

        private void ReleaseCell()
        {
            if (_chip.IsConsumed)
            {
                _chip.DetachView();
            }

            FinishImpact();
            FinishContainerImpact();
        }

        private void FinishImpact()
        {
            _impactEndHandle?.Unlock();
            _impactEndHandle = null;
        }

        private void OnSortingOrderChanged(SortingOptions options, int prevOrder)
        {
            _sortingGroup.sortingOrder = options.SortingOrder;
        }

        private void OnSortingLayerChanged(SortingOptions options)
        {
            _sortingGroup.sortingLayerID = options.SortingLayerId;
        }

        private void UpdateSortingOptionsParent()
        {
            SortingOptions.ResetParent();

            var provider = transform.parent?.GetComponentInParent<ISortingOptionsProvider>();
            if (provider != null)
            {
                SortingOptions.SetParent(provider.SortingOptions, 0);
            }
        }

        void IImpactController.OnImpact(
                ChipComponent chip,
                ImpactFlag impactType,
                IDeferredInvocationHandle impactEndHandle,
                ref bool isModal
            )
        {
            if (!string.IsNullOrEmpty(_impactTrigger))
            {
                // we have to finish existing container impact when real impact comes, cause in this situation
                // in-animator ContainerImpact state will be omit and animator proceeds directly to Impact/Consuming
                // state that lead to not finished container impact in parent chip
                FinishContainerImpact();

                Assert.IsNull(_impactEndHandle);
                _impactEndHandle = impactEndHandle.Lock();
                _animatorContoller?.SetTrigger(_impactTrigger);
            }

            if (_impactEffect != null)
            {
                _instantiator.Instantiate(_impactEffect, transform.position);
            }
        }

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.Chip;
        }

        void IImpactController.Cancel()
        {
            FinishImpact();
        }

        #region Inner types

        public enum IdleAnimationType
        {
            Default = 0,
            Conveyor = 1,
        }

        #endregion Inner types
    }
}