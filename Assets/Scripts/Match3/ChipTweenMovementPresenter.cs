﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(ChipPresenter))]
    public class ChipTweenMovementPresenter : MonoBehaviour
    {
        [SerializeField] private ChipTweenMovementPresenterSettings _movementSettings;

        private ChipComponent _chip;
        private IDeferredInvocationHandle _movingEndHandle;

        protected void Awake()
        {
            _chip = GetComponent<ChipPresenter>().ChipComponent;
            _chip.Moving += OnChipMoving;
        }

        protected void OnDestroy()
        {
            _chip.Moving -= OnChipMoving;
        }

        private void OnChipMoving(ChipComponent.MovingType type, IDeferredInvocationHandle movingEndHandle)
        {
            if (type == _movementSettings.MovingType)
            {
                _movingEndHandle = movingEndHandle.Lock();

                LTDescr descriptor = LeanTween.move(gameObject, _chip.transform, _movementSettings.MovingTime);
                _movementSettings.Easing.SetupDescriptor(descriptor);
                descriptor.setOnComplete(OnMovingEnded);
            }
        }

        private void OnMovingEnded()
        {
            var handle = _movingEndHandle;
            _movingEndHandle = null;
            handle.Unlock();
        }
    }
}