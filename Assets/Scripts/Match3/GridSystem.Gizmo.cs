﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace DreamTeam.Match3
{
    public partial class GridSystem
    {
        private static GUIStyle _style;
        private static readonly Vector2 _offset = new Vector2(-14, -32);
        private const int _fontSize = 50;

        protected void OnDrawGizmos()
        {
            if (_style == null)
            {
                _style = new GUIStyle
                {
                    normal = { textColor = Color.black },
                    fontSize = _fontSize
                };
            }

            Handles.BeginGUI();
            {
                for (int x = 0; x < Width; ++x)
                {
                    Vector2 pos2D = HandleUtility.WorldToGUIPoint(GridToWorld(new Vector2Int(x, -1))) + _offset;
                    GUI.Label(new Rect(pos2D.x - CellHeight, pos2D.y, 0, 0), x.ToString(), _style);
                }
                for (int y = 0; y < Height; ++y)
                {
                    Vector2 pos2D = HandleUtility.WorldToGUIPoint(GridToWorld(new Vector2Int(-1, y))) + _offset;
                    GUI.Label(new Rect(pos2D.x, pos2D.y - CellWidth, 0, 0), y.ToString(), _style);
                }
            }
            Handles.EndGUI();
        }
    }
}

#endif // UNITY_EDITOR
