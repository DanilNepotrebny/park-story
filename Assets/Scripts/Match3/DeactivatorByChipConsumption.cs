﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Audio;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    [RequireComponent(typeof(ChipPresenter))]
    public class DeactivatorByChipConsumption : MonoBehaviour
    {
        [SerializeField] private GameObject _target;
        [SerializeField] private AudioSystemPlayer _deactivationSound;

        protected void Awake()
        {
            GetComponent<ChipPresenter>().ChipComponent.Impacting += OnImpacting;
        }

        private void OnImpacting(ChipComponent chip, int healthDelta)
        {
            if (chip.IsConsumed)
            {
                _target?.SetActive(false);

                if (_deactivationSound != null)
                {
                    _deactivationSound.transform.SetParent(null);
                    _deactivationSound.Play();
                    _deactivationSound.Stoped += OnSoundStoped;
                }
            }
        }

        private void OnSoundStoped()
        {
            _deactivationSound.Stoped -= OnSoundStoped;
            _deactivationSound.gameObject.Dispose();
        }
    }
}