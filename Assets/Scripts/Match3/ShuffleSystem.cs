﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Debug;
using DreamTeam.GameControls;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Match3.Matching;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using ModestTree;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Zenject;
using Assert = UnityEngine.Assertions.Assert;

namespace DreamTeam.Match3
{
    internal class ShuffleSystem : MonoBehaviour, IFieldDestabilizer
    {
        [SerializeField, MinMaxRange(1, 10), Tooltip("Up to number of moves to be existed after shuffling")]
        private MinMaxRange _movesToCreate;

        [SerializeField] private int _shuffleFailedAnimationsCount;

        [SerializeField] private ShuffleFlightPresenter _flightPrefab;
        [SerializeField] private GameObject _noMoreMovesMessagePrefab;
        [SerializeField] private GameObject _shuffleMessagePrefab;
        [SerializeField] private UnityEvent _startingShuffleEvent;
        [SerializeField] private float _delayBeforeShuffleMessage = 0.5f;
        [SerializeField] private float _delayBeforeNoMoreMovesMessage = 0.5f;
        [SerializeField] private float _delayBeforeShuffleAnimation = 0.5f;

        [SerializeField, FormerlySerializedAs("_selectionMode")]
        private GameControlsMode _controlsMode;

        [SerializeField, FormerlySerializedAs("_selectionLockShuffle"), FormerlySerializedAs("_selectionItemShuffle")]
        private GameControl _controlItem;

        [Inject] private MatchSystem _matchSystem;
        [Inject] private MovesSystem _movesSystem;
        [Inject] private GridSystem _grid;
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;
        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private Instantiator _instantiator;
        [Inject] private CheatPanel _cheatPanel;

        [Inject(Id = Match3Installer.RandomId)]
        private CustomRandom _random;

        private List<CellComponent> _shuffableCellsBackingField;
        private KeyValueList<CellComponent, ChipComponent> _initialCellsStateList =
            new KeyValueList<CellComponent, ChipComponent>();
        private KeyValueList<CellComponent, ChipComponent> _shuffledCellsStateList =
            new KeyValueList<CellComponent, ChipComponent>();
        private List<Tuple<CellComponent, CellComponent>> _cellsToSwap;
        private List<CellComponent> _noMoveCellsBackingField;
        private bool _isShufflingBackingField;
        private bool _checkField;
        private ActionDisposable _modeDisableHandle;

        public event Action ShuffleFailed;

        #region IFieldDestabilizer

        public event Action Stabilized;
        public event Action Destabilized;
        public bool IsStable => !IsShuffling;

        #endregion IFieldDestabilizer

        public bool IsShuffling
        {
            get { return _isShufflingBackingField; }
            private set
            {
                if (_isShufflingBackingField == value)
                {
                    return;
                }

                _isShufflingBackingField = value;

                if (_isShufflingBackingField)
                {
                    Destabilized?.Invoke();
                }
                else
                {
                    Stabilized?.Invoke();
                }
            }
        }

        private List<CellComponent> ShuffableCells
        {
            get
            {
                if (_shuffableCellsBackingField == null || _shuffableCellsBackingField.Count == 0)
                {
                    _shuffableCellsBackingField = _grid.Cells.Where(cell => IsShuffable(cell)).ToList();
                }

                return _shuffableCellsBackingField;
            }
        }

        private List<CellComponent> NoMoveCells
        {
            get
            {
                if (_noMoveCellsBackingField == null || _noMoveCellsBackingField.Count == 0)
                {
                    _noMoveCellsBackingField = ShuffableCells.Where(cell => !cell.IsMoveGenerationEnabled).ToList();
                }

                return _noMoveCellsBackingField;
            }
        }

        public bool TryShuffle(bool isSilently = false)
        {
            CleanUp();
            bool checkNoMoveCells = isSilently;

            if (!IsShuffleNeeded(checkNoMoveCells))
            {
                return true;
            }

            bool isSuccessful = TryToShuffle(isSilently, checkNoMoveCells);
            return isSuccessful;
        }

        protected void Awake()
        {
            _cellsToSwap = new List<Tuple<CellComponent, CellComponent>>();

            _fieldStabilizationSystem.AddStabilizationHandler(
                CheckFieldNextFrame,
                FieldStabilizationHandlerOrder.Shuffle);

            _gameControlsSystem.AddGameControlStateChangeHandler(_controlItem, OnControlStateChanged);

            _cheatPanel.AddDrawer(CheatPanel.Category.Match3, DrawCheats);
            _fieldStabilizationSystem.AddDestabilizer(this);
        }

        protected void OnDestroy()
        {
            ReleaseModeDisableHandle();
            _gameControlsSystem.RemoveGameControlStateChangeHandler(_controlItem, OnControlStateChanged);
            _fieldStabilizationSystem.RemoveDestabilizer(this);
            _cheatPanel.RemoveDrawer(DrawCheats);
        }

        protected void Update()
        {
            if (_checkField)
            {
                _checkField = false;

                if (_fieldStabilizationSystem.IsFieldStable &&
                    _gameControlsSystem.IsControlInState(_controlItem, GameControl.State.Unlocked))
                {
                    TryShuffle();
                }
            }
        }

        private bool IsShuffleNeeded(bool checkNoMoveCells)
        {
            bool hasMoves = _movesSystem.FindMoves(null);
            bool isNoMoveCellsValid = true;
            if (checkNoMoveCells)
            {
                isNoMoveCellsValid = IsNoMoveCellsValid();
            }

            return _matchSystem.HasMatches || !hasMoves || !isNoMoveCellsValid;
        }

        private bool TryToShuffle(bool silently, bool checkNoMoveCells = false)
        {
            IsShuffling = true;
            bool preShuffleRandomly = !silently;
            if (PrepareShuffle(preShuffleRandomly, checkNoMoveCells))
            {
                if (silently)
                {
                    ApplyShuffle();
                    IsShuffling = false;
                }
                else
                {
                    StartCoroutine(AnimateShuffle(ApplyShuffle));
                }

                return true;
            }
            else if (!silently)
            {
                StartCoroutine(AnimateShuffle(OnShuffleFailed, _shuffleFailedAnimationsCount));
            }
            else
            {
                IsShuffling = false;
            }

            return false;
        }

        private void OnControlStateChanged(GameControl.State newState)
        {
            CheckFieldNextFrame();
        }

        private void CheckFieldNextFrame()
        {
            _checkField = true;
        }

        private static bool IsShuffable([NotNull] CellComponent cell, bool checkPredefinedChips = false)
        {
            bool isShuffable =
                cell != null &&
                !cell.IsEmpty &&
                cell.Chip.CanBeShuffled &&
                (checkPredefinedChips ? cell.HasPredefinedChip : !cell.HasPredefinedChip);
            return isShuffable;
        }

        private void SwapChipsRandomly(List<CellComponent> cells)
        {
            Assert.IsTrue(cells.Count > 1, $"Not enough cells to {nameof(SwapChipsRandomly)}");

            cells.RandomShuffle(_random);
            for (int i = 0; i < cells.Count - 1; i += 1)
            {
                CellComponent cell1 = cells[i];
                CellComponent cell2 = cells[i + 1];

                cell1.SwapChipsSilently(cell2);
            }
        }

        private void CacheGridState(KeyValueList<CellComponent, ChipComponent> container)
        {
            container.Clear();
            foreach (CellComponent cell in ShuffableCells)
            {
                container.Add(cell, cell.Chip);
            }
        }

        private bool IsSwapValid([NotNull] CellComponent first, [NotNull] CellComponent second)
        {
            ChipComponent firstCellInitialChip = _initialCellsStateList[first];
            ChipComponent secondCellInitialChip = _initialCellsStateList[second];
            return firstCellInitialChip != second.Chip &&
                secondCellInitialChip != first.Chip;
        }

        private bool PrepareShuffle(bool preShuffleRandomly = false, bool checkNoMoveCells = false)
        {
            _cellsToSwap.Clear();

            if (ShuffableCells.Count <= 1)
            {
                UnityEngine.Debug.LogError($"{nameof(ShuffleSystem)}: Not enough cells to perform shuffling", this);
                return false;
            }

            CacheGridState(_initialCellsStateList);

            if (preShuffleRandomly)
            {
                SwapChipsRandomly(ShuffableCells);
            }

            if (!RemoveExistingMatches())
            {
                UnityEngine.Debug.LogError($"Was not able to remove existing matches");
                return false;
            }

            if (checkNoMoveCells && !RemoveMovesFromNoMoveCells())
            {
                UnityEngine.Debug.LogError($"Was not able to remove non valid moves");
                return false;
            }

            var moves = new List<MovesSystem.Move>();
            _movesSystem.FindMoves(moves);
            int desiredMovesNumber = (int)_movesToCreate.GetRandomValue(_random);
            int movesToCreate = desiredMovesNumber - moves.Count;
            if (movesToCreate > 0)
            {
                if (!CreateMoves(movesToCreate))
                {
                    UnityEngine.Debug.LogError($"Was not able to create additional move (requested {movesToCreate})");
                    return false;
                }
            }

            CacheGridState(_shuffledCellsStateList);

            // rollback silent swaps
            for (int i = _cellsToSwap.Count - 1; i >= 0; --i)
            {
                var cellsTuple = _cellsToSwap[i];
                cellsTuple.Item1.SwapChipsSilently(cellsTuple.Item2);
            }

            return true;
        }

        /// <summary> Swap chips until all existed matches will be removed. </summary>
        /// <returns> Is successful </returns>
        private bool RemoveExistingMatches()
        {
            int[] shuffableCellsIndexerCache = CreateListAndFillWithIndexes(ShuffableCells.Count);

            while (_matchSystem.HasMatches)
            {
                Match foundMatch = _matchSystem.FindMatch(IsShuffableMatch);
                if (foundMatch == null)
                {
                    throw new Exception($"Found a match that is not shuffable. It might be predefined one! " +
                        $"Check level layout.");
                }

                Vector2Int matchCenter = CalculateMatchCenter(foundMatch);
                CellComponent targetCell = _grid[matchCenter.x, matchCenter.y];
                Assert.IsTrue(IsShuffable(targetCell), "Match's target cell is not shuffable");
                ChipComponent targetChip = targetCell.Chip;
                bool targetCellHasMatchBeforeSwap = _matchSystem.FindMatch(targetCell.GridPosition) != null;

                CellComponent cellToExchange = null;
                IEnumerable<int> randomIndexer = ShuffableCells.GetRandomIndexEnumerable(
                        (list, idx) =>
                        {
                            CellComponent cell = list[idx];
                            return !cell.IsEmpty &&
                                !foundMatch.Contains(cell.GridPosition) &&
                                !_matchSystem.IsMatchable(targetChip, cell.Chip) &&
                                IsSwapValid(targetCell, cell);
                        },
                        shuffableCellsIndexerCache,
                        _random
                    );

                foreach (int index in randomIndexer)
                {
                    CellComponent possibleCell = ShuffableCells[index];

                    // check that we will decrease amount of matches by the swap
                    bool possibleCellHasMatchBeforeSwap = _matchSystem.FindMatch(possibleCell.GridPosition) != null;

                    targetCell.SwapChipsSilently(possibleCell); // commit

                    int matchesCounter = 0;
                    matchesCounter += targetCellHasMatchBeforeSwap ? 1 : 0;
                    matchesCounter += possibleCellHasMatchBeforeSwap ? 1 : 0;

                    bool rollbackSwap;
                    bool targetCellHasMatchAfterSwap = _matchSystem.FindMatch(targetCell.GridPosition) != null;
                    if (!targetCellHasMatchBeforeSwap && targetCellHasMatchAfterSwap)
                    {
                        rollbackSwap = true;
                    }
                    else
                    {
                        matchesCounter -= targetCellHasMatchAfterSwap ? 0 : 1;

                        bool possibleCellHasMatchAfterSwap = _matchSystem.FindMatch(possibleCell.GridPosition) != null;
                        matchesCounter -= possibleCellHasMatchAfterSwap ? 0 : 1;
                        rollbackSwap = matchesCounter >= 0;
                    }

                    if (rollbackSwap)
                    {
                        targetCell.SwapChipsSilently(possibleCell); // rollback
                        continue;
                    }

                    cellToExchange = possibleCell;
                    break;
                }

                if (cellToExchange == null)
                {
                    UnityEngine.Debug.LogError($"Didn't find the way to break the match {foundMatch}");
                    return false;
                }

                _cellsToSwap.Add(new Tuple<CellComponent, CellComponent>(targetCell, cellToExchange));
            }

            return true;
        }

        private bool RemoveMovesFromNoMoveCells()
        {
            const int failedIterationsCount = 10;
            var moves = new List<MovesSystem.Move>();
            var moveCells = new List<CellComponent>(5);
            int[] shuffableCellsIndexerCache = CreateListAndFillWithIndexes(ShuffableCells.Count);

            do
            {
                List<CellComponent> noMoveCellsWithMovesBefore = GetNoMoveCellsWithMove();
                if (noMoveCellsWithMovesBefore.Count == 0)
                {
                    return true;
                }

                bool swapped = false;
                for (int failItr = 0; failItr < failedIterationsCount && !swapped; failItr += 1)
                {
                    foreach (CellComponent noMoveCell in noMoveCellsWithMovesBefore)
                    {
                        moveCells.Clear();
                        moves.Clear();
                        if (_movesSystem.FindMoves(moves, noMoveCell.GridPosition))
                        {
                            MovesSystem.Move move = moves.First();

                            if (move.SourceMatch.Any())
                            {
                                AddMatchableSourceMatchMoveCells(moveCells, move);
                            }
                            if (move.TargetMatch.Any())
                            {
                                AddMatchableTargetMatchMoveCells(moveCells, move);
                            }
                        }
                        else // in this case our no_move_cell is not pivot cell and is a part of further match
                        {
                            moves.Clear();
                            _movesSystem.FindMoves(moves);

                            foreach (MovesSystem.Move move in moves)
                            {
                                if (move.SourceMatch != null &&
                                    move.SourceMatch.Any(chip => chip.Cell == noMoveCell))
                                {
                                    AddMatchableSourceMatchMoveCells(moveCells, move);
                                }

                                if (move.TargetMatch != null &&
                                    move.TargetMatch.Any(chip => chip.Cell == noMoveCell))
                                {
                                    AddMatchableTargetMatchMoveCells(moveCells, move);
                                }
                            }
                        }

                        CellComponent sourceCell = null, targetCell = null;
                        foreach (CellComponent possibleSourceCell in moveCells)
                        {
                            IEnumerable<int> randomIndexer = ShuffableCells.GetRandomIndexEnumerable(
                                    (list, idx) =>
                                    {
                                        CellComponent cell = list[idx];
                                        return !cell.IsEmpty &&
                                            !moveCells.Contains(cell) &&
                                            !_matchSystem.IsMatchable(possibleSourceCell.Chip, cell.Chip) &&
                                            IsSwapValid(possibleSourceCell, cell);
                                    },
                                    shuffableCellsIndexerCache,
                                    _random
                                );

                            foreach (int index in randomIndexer)
                            {
                                targetCell = ShuffableCells[index];

                                possibleSourceCell.SwapChipsSilently(targetCell); // commit

                                List<CellComponent> noMoveCellsWithMovesOnAfter = GetNoMoveCellsWithMove();
                                bool rollbackSwap =
                                    noMoveCellsWithMovesBefore.Count <= noMoveCellsWithMovesOnAfter.Count ||
                                    _matchSystem.HasMatches;

                                if (rollbackSwap)
                                {
                                    possibleSourceCell.SwapChipsSilently(targetCell); // rollback
                                }
                                else
                                {
                                    sourceCell = possibleSourceCell;
                                    break;
                                }
                            }

                            if (sourceCell != null)
                            {
                                break;
                            }
                        }

                        if (sourceCell != null && targetCell != null)
                        {
                            swapped = true;
                            _cellsToSwap.Add(new Tuple<CellComponent, CellComponent>(sourceCell, targetCell));
                            break;
                        }
                    } // foreach (CellComponent noMoveCell in noMoveCellsWithMovesOnStart)
                } // for (int failItr = 0; failItr < failedIterationsCount && !swapped; failItr += 1)

                if (!swapped)
                {
                    UnityEngine.Debug.LogError($"Didn't find any swap to decrease amount of no_move_cells with moves");
                    return false;
                }
            }
            while (true);
        }

        private void AddMatchableSourceMatchMoveCells(List<CellComponent> list, MovesSystem.Move move)
        {
            list.Add(move.Source);
            list.AddRange(move.SourceMatch.
                Where(c => _matchSystem.IsMatchable(c, move.Source.Chip)).
                Select(c => c.Cell));
        }

        private void AddMatchableTargetMatchMoveCells(List<CellComponent> list, MovesSystem.Move move)
        {
            list.Add(move.Target);
            list.AddRange(move.TargetMatch.
                Where(c => _matchSystem.IsMatchable(c, move.Target.Chip)).
                Select(c => c.Cell));
        }

        /// <summary> Creates moves without providing new matches. </summary>
        /// <param name="movesAmount"> Desired moves amount to be created </param>
        /// <returns> Is successful </returns>
        private bool CreateMoves(int movesAmount)
        {
            Assert.IsFalse(_matchSystem.HasMatches, "Field has matches! Moves can be created only when there is no matches");

            // TODO: Check cases when ShuffableCells less than possible moves amount to create. Clamp when possible

            bool createdAMove = false;
            int nestingNumber = 1, iterations = movesAmount;
            while (iterations > 0 && nestingNumber <= _matchSystem.MinMatchLength)
            {
                int movesCreated = CreateMove(nestingNumber);
                if (movesCreated > 0)
                {
                    iterations -= movesCreated;
                    createdAMove = true;
                }
                else
                {
                    nestingNumber += 1;
                }
            }

            return createdAMove;
        }

        /// <summary> Create a move (or several) without providing a match (keeping in mind no-move cells) </summary>
        /// <param name="nestingNumber"> Allowed nesting recursive calls number to create a move.
        /// For example, 2 means that we can swap up to 2 chips to provide a move. </param>
        /// <returns> Number of moves created </returns>
        private int CreateMove(int nestingNumber = 0)
        {
            nestingNumber -= 1;
            int[] shuffableCellsToIndexerCache = CreateListAndFillWithIndexes(ShuffableCells.Count);
            int[] shuffableCellsFromIndexerCache = CreateListAndFillWithIndexes(ShuffableCells.Count);

            var movesAfterCacheList = new List<MovesSystem.Move>();
            int movesBefore = _movesSystem.CalculateExistingMovesCount();

            IEnumerable<int> randomIndexerCellFrom = ShuffableCells.GetRandomIndexEnumerable(
                    (list, index) =>
                    {
                        CellComponent cell = list[index];
                        return !cell.IsEmpty &&
                            !NoMoveCells.Contains(cell);
                    },
                    shuffableCellsFromIndexerCache,
                    _random
                );

            foreach (int indexCellFrom in randomIndexerCellFrom)
            {
                CellComponent cellFrom = ShuffableCells[indexCellFrom];
                IEnumerable<int> randomIndexerCellTo = ShuffableCells.GetRandomIndexEnumerable(
                        (list, index) =>
                        {
                            CellComponent cell = list[index];
                            return !cell.IsEmpty &&
                                cell != cellFrom &&
                                !NoMoveCells.Contains(cell) &&
                                !_matchSystem.IsMatchable(cell.Chip, cellFrom.Chip) &&
                                IsSwapValid(cell, cellFrom);
                        },
                        shuffableCellsToIndexerCache,
                        _random
                    );

                foreach (int indexCellTo in randomIndexerCellTo)
                {
                    CellComponent cellTo = ShuffableCells[indexCellTo];

                    cellFrom.SwapChipsSilently(cellTo); // commit

                    // check that we produced additional valid move and there are no new matches
                    bool rollbackSwap = !IsNoMoveCellsValid();

                    if (!rollbackSwap)
                    {
                        if (_matchSystem.FindMatch(cellFrom.GridPosition) != null ||
                            _matchSystem.FindMatch(cellTo.GridPosition) != null)
                        {
                            rollbackSwap = true;
                        }
                    }

                    if (rollbackSwap)
                    {
                        cellFrom.SwapChipsSilently(cellTo); // unsuccessful rollback
                        continue;
                    }

                    // TODO: Would be efficient to calculate only in cellTo and cellFrom cells?
                    int movesAfter = _movesSystem.CalculateExistingMovesCount(movesAfterCacheList);
                    int movesCreated = movesAfter - movesBefore;
                    if (movesCreated > 0)
                    {
                        _cellsToSwap.Add(new Tuple<CellComponent, CellComponent>(cellFrom, cellTo));
                        return movesCreated;
                    }

                    if (nestingNumber > 0)
                    {
                        _cellsToSwap.Add(new Tuple<CellComponent, CellComponent>(cellFrom, cellTo));
                        movesCreated = CreateMove(nestingNumber - 1);

                        if (movesCreated > 0)
                        {
                            return movesCreated;
                        }

                        _cellsToSwap.RemoveAt(_cellsToSwap.Count - 1);
                    }
                }
            }

            return 0;
        }

        private int[] CreateListAndFillWithIndexes(int capacity)
        {
            var list = new int[capacity];
            for (int i = 0; i < capacity; ++i)
            {
                list[i] = i;
            }
            return list;
        }

        private bool IsShuffableMatch(Match match)
        {
            foreach (Vector2Int cellIdx in match)
            {
                CellComponent cell = _grid[cellIdx.x, cellIdx.y];
                if (IsShuffable(cell))
                {
                    return true;
                }
            }

            return false;
        }

        // calculates position which has the most nearby positions
        private Vector2Int CalculateMatchCenter([NotNull] Match match)
        {
            var matchPositions = new List<Vector2Int>(match);
            Assert.IsTrue(matchPositions.Count > 0, "Empty match!");

            if (matchPositions.Count == 1)
            {
                return matchPositions[0];
            }

            var matchCenter = new Vector2Int();
            float totalDistanceToNeighbors = float.MaxValue;

            for (int i = 0; i < matchPositions.Count; ++i)
            {
                Vector2Int center = matchPositions[i];
                CellComponent cell = _grid.GetCell(center);
                if (!IsShuffable(cell))
                {
                    continue;
                }

                float totalDistance = matchPositions.
                    Where((t, j) => i != j).
                    Sum(t => Vector2Int.Distance(center, t));

                if (totalDistance < totalDistanceToNeighbors)
                {
                    matchCenter = center;
                    totalDistanceToNeighbors = totalDistance;
                }
            }

            return matchCenter;
        }

        private void ApplyShuffle()
        {
            ResetChipPresentersPosition();
            foreach (var cellsTuple in _cellsToSwap)
            {
                cellsTuple.Item1.SwapChips(cellsTuple.Item2);
            }

            _cellsToSwap.Clear();
        }

        private void ResetChipPresentersPosition()
        {
            ShuffableCells.ForEach(cell => cell.Chip.Presenter.ResetPosition());
        }

        private bool IsNoMoveCellsValid()
        {
            return GetNoMoveCellsWithMove().Count == 0;
        }

        private List<CellComponent> GetNoMoveCellsWithMove()
        {
            var cells = new List<CellComponent>();
            var moves = new List<MovesSystem.Move>();
            if (!_movesSystem.FindMoves(moves))
            {
                return cells;
            }

            foreach (CellComponent noMoveCell in NoMoveCells)
            {
                foreach (MovesSystem.Move move in moves)
                {
                    if (noMoveCell == move.Source ||
                        noMoveCell == move.Target ||
                        move.SourceMatch.Any(chip => chip != null && chip.Cell == noMoveCell) ||
                        move.TargetMatch.Any(chip => chip != null && chip.Cell == noMoveCell))
                    {
                        cells.Add(noMoveCell);
                        break;
                    }
                }
            }

            return cells;
        }

        private IEnumerator AnimateShuffle(Action callback, int animationsCount = 1)
        {
            Assert.IsNull(_modeDisableHandle, "_modeDisableHandle != null");
            _modeDisableHandle = _gameControlsSystem.EnableMode(_controlsMode);

            for (int animationIndex = 0; animationIndex < animationsCount; ++animationIndex)
            {
                yield return new WaitForSeconds(_delayBeforeNoMoreMovesMessage);

                _instantiator.Instantiate(_noMoreMovesMessagePrefab, (Vector2)_grid.GetWorldBounds().center);

                yield return new WaitForSeconds(_delayBeforeShuffleMessage);

                _instantiator.Instantiate(_shuffleMessagePrefab, (Vector2)_grid.GetWorldBounds().center);

                yield return new WaitForSeconds(_delayBeforeShuffleAnimation);

                _startingShuffleEvent.Invoke();

                var cellsFrom = new KeyValueList<CellComponent, ChipComponent>();
                _initialCellsStateList.ForEach(pair => cellsFrom.Add(pair));
                _grid.Cells.Where(cell => IsShuffable(cell, true)).ForEach(cell => cellsFrom.Add(cell, cell.Chip));
                cellsFrom.RandomShuffle(_random);

                var cellsTo = new KeyValueList<CellComponent, ChipComponent>();
                _shuffledCellsStateList.ForEach(pair => cellsTo.Add(pair));
                _grid.Cells.Where(cell => IsShuffable(cell, true)).ForEach(cell => cellsTo.Add(cell, cell.Chip));
                cellsTo.RandomShuffle(_random);

                if (cellsFrom.Count > 1 && cellsTo.Count > 1)
                {
                    int sortingOrder = 0, flyingProjectilesCount = 0;
                    foreach (var pairFrom in cellsFrom)
                    {
                        CellComponent cellFrom = pairFrom.Key;
                        ChipComponent chipFrom = pairFrom.Value;

                        for (int i = 0; i < cellsTo.Count; ++i)
                        {
                            var pairTo = cellsTo[i];
                            CellComponent cellTo = pairTo.Key;
                            ChipComponent chipTo = pairTo.Value;

                            if (chipFrom == chipTo || cellFrom == cellTo)
                            {
                                continue;
                            }

                            Vector3 positionFrom = cellFrom.transform.position;
                            Vector3 positionTo = cellTo.transform.position;

                            flyingProjectilesCount += 1;
                            ShuffleFlightPresenter flightPresenter = _instantiator.Instantiate(_flightPrefab, positionFrom);
                            flightPresenter.MovementSequence.Finished += mover => flyingProjectilesCount -= 1;
                            flightPresenter.Move(
                                    positionFrom,
                                    chipFrom,
                                    positionTo,
                                    chipTo,
                                    ref sortingOrder
                                );

                            cellsTo.RemoveAt(i);
                            break;
                        }
                    }

                    yield return new WaitUntil(() => flyingProjectilesCount == 0);
                }
            }

            ReleaseModeDisableHandle();

            callback?.Invoke();

            IsShuffling = false;
        }

        private void DrawCheats(Rect areaRect)
        {
            if (GUILayout.Button("Shuffle chips"))
            {
                TryToShuffle(silently:false);
                _cheatPanel.Hide();
            }
        }

        private void CleanUp()
        {
            _shuffableCellsBackingField?.Clear();
            _noMoveCellsBackingField?.Clear();
        }

        private void OnShuffleFailed()
        {
            ShuffleFailed?.Invoke();
        }

        private void ReleaseModeDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }

        string IFieldDestabilizer.GetDebugStateDescription()
        {
            return string.Empty;
        }
    }
}
