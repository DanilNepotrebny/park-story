﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Bezel;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.Conveyor;
using DreamTeam.Match3.MagicHat;
using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.SpecialChips.GumballMachine;
using DreamTeam.Match3.Spreading;
using DreamTeam.Match3.Teleportation;
using DreamTeam.Utils;
using Zenject;

namespace DreamTeam.Match3
{
    public class Match3LevelInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<GridSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<GridAnimator>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<GridBezelAnimator>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<GenerationSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<HiddenObjectPresenter>().FromComponentInChildren().WhenInjectedInto<HiddenObject>();
            Container.Bind<ChipContainerComponent>().FromComponentSibling().WhenInjectedInto<ChipComponent>();
            Container.Bind<ChipContainerComponent>().FromComponentSibling().WhenInjectedInto<CellComponent>();
            Container.Bind<ChipComponent>().FromComponentSibling().WhenInjectedInto<MagicHatChargerComponent>();
            Container.Bind<Underlay>().FromComponentInChildren(true).WhenInjectedInto<CellComponent>();
            Container.Bind<HiddenObject>().FromComponentInHierarchy();
            Container.Bind<GumballMachineComponent>()
                .FromComponentInHierarchy()
                .WhenInjectedInto<GumballConsumptionRequirement.Instance>();
            Container.Bind<MagicHatComponent>()
                .FromComponentInParents(false, true)
                .WhenInjectedInto<MagicHatPresenter>();

            Container.Bind<ChipComponent>().FromComponentSibling().WhenInjectedInto<NeighbourImpactReceiver>();
            Container.Bind<UnderlayPresenter>().FromComponentInChildren().WhenInjectedInto<Underlay>();
            Container.Bind<CellComponent>().FromComponentInParents(false, true).WhenInjectedInto<Teleport>();
            Container.Bind<ChipPresenter>().FromComponentInParents(false, true).WhenInjectedInto<ChipPartsPresenter>();

            Container.BindInternals<Spreader>();
            Container.BindInternals<SpreadingPresenter>();
            Container.BindInternals<BezelTileController>();
            Container.BindInternals<ConveyorBlock>();
            Container.BindInternals<ChildChipDecalController>();
        }
    }
}