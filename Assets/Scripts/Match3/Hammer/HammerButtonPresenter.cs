﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Match3.Hammer
{
    [RequireComponent(typeof(PositionChangeNotifier))]
    public class HammerButtonPresenter : MonoBehaviour
    {
        [SerializeField] private Sprite _backgroundIdle;
        [SerializeField] private Sprite _backgroundPressed;
        [SerializeField, RequiredField] private Image _background;
        [SerializeField, RequiredField] private GameObject _inModeEffectPrefab;

        [Inject] private HammerSystem _hammerSystem;
        [Inject] private Instantiator _instantiator;

        private GameObject _inModeEffect;
        private PositionChangeNotifier _positionNotifier;

        protected void Awake()
        {
            _positionNotifier = GetComponent<PositionChangeNotifier>();
            _positionNotifier.PositionChanged += OnPositionChanged;

            _inModeEffect = _instantiator.Instantiate(_inModeEffectPrefab);
            _inModeEffect.SetActive(false);
        }

        protected void Start()
        {
            _hammerSystem.EnabledChanged += OnHammerSystemEnabledChanged;
            UpdateBackground();
        }

        private void OnHammerSystemEnabledChanged(bool isEnabled)
        {
            UpdateBackground();

            if (isEnabled)
            {
                _inModeEffect.transform.position = transform.position;
            }

            _inModeEffect.SetActive(isEnabled);
        }

        private void OnPositionChanged(Transform tr)
        {
            if (_hammerSystem.enabled)
            {
                _inModeEffect.transform.position = transform.position;
            }
        }

        private void UpdateBackground()
        {
            _background.sprite = _hammerSystem.enabled ? _backgroundPressed : _backgroundIdle;
        }
    }
}