﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace DreamTeam.Match3.Hammer
{
    [RequireComponent(typeof(TilemapRenderer))]
    public class TilemapHammerModeShadingReceiver : MonoBehaviour, IHammerModeShadingReceiver
    {
        private List<ShadingReceiverData> _shadingReceiverData;

        public List<ShadingReceiverData> GetShadingData()
        {
            return _shadingReceiverData;
        }

        protected void Awake()
        {
            var tilemapRenderer = GetComponent<TilemapRenderer>();
            var propertyBlock = new MaterialPropertyBlock();

            _shadingReceiverData = new List<ShadingReceiverData>
            {
                new ShadingReceiverData(tilemapRenderer, propertyBlock)
            };
        }
    }
}
