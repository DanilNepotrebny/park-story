﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Audio;
using DreamTeam.GameControls;
using DreamTeam.Inventory;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using Spine;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using Zenject;
using Event = Spine.Event;

namespace DreamTeam.Match3.Hammer
{
    [RequireComponent(typeof(AudioSystemPlayer))]
    public class HammerPresenter : MonoBehaviour
    {
        [SerializeField, RequiredField] private HammerAnimator _animator;
        [SerializeField] private float _strikeSoundDelay;
        [SerializeField, RequiredField] private GameObject _strikeEffectPrefab;
        [SerializeField, RequiredField] private TapGesture _hammerModeTapGesture;
        [SerializeField, RequiredField] private TapGesture _exitModeTapGesture;
        [SerializeField, RequiredField, FormerlySerializedAs("_selectionMode")] private GameControlsMode _controlsMode;

        [SerializeField, RequiredField, FormerlySerializedAs("_selectionLock"), FormerlySerializedAs("_selectionItem")]
        private GameControl _hammerControl;

        [Inject] private GridSystem _gridSystem;
        [Inject] private HammerSystem _hammerSystem;
        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private Instantiator _instantiator;

        private bool _isSubscribedToCellsOnTapped;
        private bool _wasInHammerMode;
        private CellComponent _strikingCell;
        private IDeferredInvocationHandle _commitStrikeHandle;
        private ActionDisposable _modeDisableHandle;

        protected void Awake()
        {
            _gameControlsSystem.AddGameControlStateChangeHandler(_hammerControl, OnControlStateChanged);
            _hammerModeTapGesture.Tapped += (obj, args) => ToggleHammerMode();

            _hammerSystem.Striking += OnStriking;
            _hammerSystem.enabled = true;
        }

        protected void Start()
        {
            _animator.enabled = false;
            _animator.Animation.state.Event += OnStrike;
            _animator.Animation.state.Complete += track => _animator.enabled = false;

            ToggleHammerMode();
        }

        private void OnDestroy()
        {
            ReleaseModeDisableHandle();
            _gameControlsSystem.RemoveGameControlStateChangeHandler(_hammerControl, OnControlStateChanged);
        }

        private void ToggleHammerMode()
        {
            if (_hammerSystem.enabled)
            {
                ExitHammerMode();
            }
            else if (!_hammerSystem.InventoryItem.IsSpendingPossible(HammerSystem.SpendAmount))
            {
                ExitHammerMode();
                _hammerSystem.InventoryItem.ShowNotEnoughPopup(GamePlace.InGame);
            }
            else
            {
                EnterHammerMode();
            }
        }

        private void EnterHammerMode()
        {
            if (_gameControlsSystem.IsControlInState(_hammerControl, GameControl.State.Locked))
            {
                return;
            }

            _wasInHammerMode = true;
            _exitModeTapGesture.Tapped += OnExitGesture;

            SubscribeToCellsOnTapped();

            _hammerSystem.enabled = true;

            Assert.IsNull(_modeDisableHandle, "_modeDisableHandle != null");
            _modeDisableHandle = _gameControlsSystem.EnableMode(_controlsMode);
        }

        private void ExitHammerMode()
        {
            _hammerSystem.enabled = false;

            if (_wasInHammerMode)
            {
                _wasInHammerMode = false;
                ReleaseModeDisableHandle();
            }

            UnsubscribeFromCellsOnTapped();

            _exitModeTapGesture.Tapped -= OnExitGesture;
        }

        private void OnControlStateChanged(GameControl.State newState)
        {
            if (newState == GameControl.State.Locked &&
                _hammerSystem.enabled)
            {
                ExitHammerMode();
            }
        }

        private void OnCellTapped(CellComponent cell)
        {
            if (!IsReadyToStrike())
            {
                return;
            }

            if (_hammerSystem.SetTarget(cell))
            {
                _animator.gameObject.transform.position = cell.gameObject.transform.position;
                _animator.Animation.state.SetAnimation(0, _animator.Animation.AnimationName, false);
                GetComponent<AudioSystemPlayer>().Play(_strikeSoundDelay);
                _animator.enabled = true;
            }
        }

        private void OnStrike(TrackEntry trackEntry, Event e)
        {
            if (e.Data.Name == _animator.StrikeAnimationEventName)
            {
                _hammerSystem.enabled = true;
                _commitStrikeHandle.Unlock();
                _commitStrikeHandle = null;
                _instantiator.Instantiate(_strikeEffectPrefab, _strikingCell.Presenter.transform.position);
                ToggleHammerMode();
            }
        }

        private void OnStriking([NotNull] CellComponent target, [NotNull] IDeferredInvocationHandle commitStrikeHandle)
        {
            Assert.IsNull(_commitStrikeHandle);
            _strikingCell = target;
            _commitStrikeHandle = commitStrikeHandle.Lock();
        }

        private void OnExitGesture(object sender, EventArgs eventArgs)
        {
            if (!_animator.enabled)
            {
                _hammerSystem.enabled = true;
                ToggleHammerMode();
            }
        }

        private bool IsReadyToStrike()
        {
            return _hammerSystem.enabled && !_animator.enabled;
        }

        private void SubscribeToCellsOnTapped()
        {
            if (!_isSubscribedToCellsOnTapped)
            {
                foreach (CellComponent cell in _gridSystem.Cells)
                {
                    cell.Presenter.CellTapped += OnCellTapped;
                }

                _isSubscribedToCellsOnTapped = true;
            }
        }

        private void UnsubscribeFromCellsOnTapped()
        {
            if (_isSubscribedToCellsOnTapped)
            {
                foreach (CellComponent cell in _gridSystem.Cells)
                {
                    cell.Presenter.CellTapped -= OnCellTapped;
                }

                _isSubscribedToCellsOnTapped = false;
            }
        }

        private void ReleaseModeDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }
    }
}