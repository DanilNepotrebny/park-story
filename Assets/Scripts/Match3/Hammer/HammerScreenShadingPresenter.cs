﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.Bezel;
using DreamTeam.Utils;
using DreamTeam.Utils.RendererSorting;
using JetBrains.Annotations;
using ModestTree;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Match3.Hammer
{
    public interface IHammerModeShadingReceiver
    {
        List<ShadingReceiverData> GetShadingData();
    }

    public struct ShadingReceiverData
    {
        public Renderer Renderer { get; }
        public MaterialPropertyBlock PropertyBlock { get; }

        public ShadingReceiverData(Renderer renderer, MaterialPropertyBlock propertyBlock)
        {
            Renderer = renderer;
            PropertyBlock = propertyBlock;
        }
    }

    [RequireComponent(typeof(Image))]
    public class HammerScreenShadingPresenter : MonoBehaviour
    {
        [SerializeField] private Color _initialColor;
        [SerializeField] private Color _inModeColor;
        [SerializeField, Min(0)] private float _mixDuration = 0.5f;
        [SerializeField, SortingLayer] private int _sortingLayer;

        [Inject] private HammerSystem _hammerSystem;
        [Inject] private GridSystem _gridSystem;
        [Inject] private BezelSystem _bezelSystem;
        [Inject] private GridPresentersSorter _gridPresentersSorter;

        private Image _background;
        private LTDescr _coloringLtDescr;
        private Color _bgStartColor, _targetColor;
        private float _lastRatio = 1f;
        private int _gridBezelSortingLayer, _gridBezelSortingOrder;
        private List<SortingOptions> _options = new List<SortingOptions>();

        private readonly HashSet<SortingOptions> _parentChangedOptions = new HashSet<SortingOptions>();
        private List<IHammerModeShadingReceiver> _shadingReceivers =
            new List<IHammerModeShadingReceiver>();

        private readonly int _tintColorPropertyId = Shader.PropertyToID("_Color");
        private readonly Color _shadingReceiverColor = new Color(0.5f, 0.5f, 0.5f, 1f);

        protected void Awake()
        {
            _background = GetComponent<Image>();
            _hammerSystem.EnabledChanged += OnHammerSystemEnabledChanged;
        }

        protected void Start()
        {
            gameObject.SetActive(false);
        }

        private void OnHammerSystemEnabledChanged(bool isEnabled)
        {
            if (isEnabled)
            {
                gameObject.SetActive(true);
            }

            if (_coloringLtDescr != null)
            {
                LeanTween.cancel(_coloringLtDescr.id);
            }

            _bgStartColor = _background.color;
            _targetColor = isEnabled ? _inModeColor : _initialColor;
            float duration = Mathf.Lerp(0, _mixDuration, _lastRatio);
            _coloringLtDescr = LeanTween.value(0f, 1f, duration)
                .setOnStart(OnShadingStarted)
                .setOnUpdate(OnShadingUpdated)
                .setOnComplete(() => OnShadingCompleted(!isEnabled));
        }

        private void OnShadingStarted()
        {
            if (_options.Count != 0)
            {
                return;
            }

            var bezelShadingReceivers = _bezelSystem.GetComponentsInChildren<IHammerModeShadingReceiver>();
            bezelShadingReceivers.ForEach(receiver => _shadingReceivers.Add(receiver));

            var gridShadingReceivers = _gridSystem.GetComponentsInChildren<IHammerModeShadingReceiver>();
            gridShadingReceivers.ForEach(receiver => _shadingReceivers.Add(receiver));

            foreach (CellComponent cell in _gridSystem.Cells)
            {
                cell.ChipChanged += OnCellChipChanged;
            }

            AddSortingOptions(_gridSystem.gameObject);
            AddSortingOptions(_bezelSystem.gameObject);

            _gridPresentersSorter.enabled = false;

            foreach (SortingOptions options in _options)
            {
                options.ParentChanged += OnSortingOptionsParentChanged;
            }

            PushSortingOptions();
        }

        private void AddSortingOptions(GameObject parent)
        {
            var gridSortingOptions = parent.GetComponentsInChildren<ISortingOptionsProvider>();
            foreach (ISortingOptionsProvider provider in gridSortingOptions)
            {
                if (!_options.Contains(provider.SortingOptions))
                {
                    _options.Add(provider.SortingOptions);
                    provider.SortingOptions.Disposed += OnSortingOptionsDisposed;
                }
            }
        }

        private void OnSortingOptionsDisposed(SortingOptions sortingOptions)
        {
            _options.Remove(sortingOptions);
        }

        private void PopSortingOptions()
        {
            SortingOptions.PopFromSortingLayer(GetValidOptionsList());
        }

        private void PushSortingOptions()
        {
            SortingOptions.PushToSortingLayer(
                    GetValidOptionsList(),
                    _sortingLayer,
                    1 // keeping in mind grid bezel
                );
        }

        private List<SortingOptions> GetValidOptionsList()
        {
            return _options.Where(IsValid).ToList();
        }

        private void OnShadingUpdated(float ratio)
        {
            _background.color = Color.Lerp(_bgStartColor, _targetColor, ratio);
            UpdateShadingReceivers(ratio);

            _lastRatio = ratio;
        }

        private void UpdateShadingReceivers(float ratio)
        {
            Color targetColor = _hammerSystem.enabled ? _shadingReceiverColor : Color.white;
            Color initialColor = _hammerSystem.enabled ? Color.white : _shadingReceiverColor;
            Color color = Color.Lerp(initialColor, targetColor, ratio);

            foreach (IHammerModeShadingReceiver receiver in _shadingReceivers)
            {
                foreach (ShadingReceiverData shadingData in receiver.GetShadingData())
                {
                    if (shadingData.Renderer != null)
                    {
                        shadingData.Renderer.GetPropertyBlock(shadingData.PropertyBlock);
                        shadingData.PropertyBlock.SetColor(_tintColorPropertyId, color);
                        shadingData.Renderer.SetPropertyBlock(shadingData.PropertyBlock);
                    }
                }
            }
        }

        private void OnShadingCompleted(bool deactivate)
        {
            _coloringLtDescr = null;

            if (deactivate)
            {
                _gridSystem.ForEachCell(cell => cell.ChipChanged -= OnCellChipChanged);

                foreach (SortingOptions option in _options)
                {
                    option.ParentChanged -= OnSortingOptionsParentChanged;
                }

                PopSortingOptions();

                gameObject.SetActive(false);
                _options.Clear();
                _shadingReceivers.Clear();
                _parentChangedOptions.Clear();
                _gridPresentersSorter.enabled = true;
            }
        }

        private void OnCellChipChanged([CanBeNull] CellComponent chip, ChipComponent oldChip)
        {
            if (chip == null)
            {
                return;
            }

            PopSortingOptions();
            AddSortingOptions(chip.gameObject);
            PushSortingOptions();
        }

        private void OnSortingOptionsParentChanged(SortingOptions options, SortingOptions prevOptions)
        {
            _parentChangedOptions.Add(options);
        }

        private bool IsValid([CanBeNull] SortingOptions options)
        {
            return options != null &&
                !options.HasParent &&
                !_parentChangedOptions.Contains(options);
        }
    }
}