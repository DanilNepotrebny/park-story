﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Spine.Unity;
using UnityEngine;

namespace DreamTeam.Match3.Hammer
{
    [RequireComponent(typeof(SkeletonAnimation), typeof(MeshRenderer))]
    public class HammerAnimator : MonoBehaviour
    {
        [SerializeField, SpineEvent] private string _strikeAnimationEventName;

        private MeshRenderer _meshRenderer;

        public string StrikeAnimationEventName => _strikeAnimationEventName;
        public SkeletonAnimation Animation { get; private set; }

        protected void Awake()
        {
            Animation = GetComponent<SkeletonAnimation>();
            _meshRenderer = GetComponent<MeshRenderer>();
        }

        protected void OnEnable()
        {
            Animation.enabled = true;
            _meshRenderer.enabled = true;
        }

        protected void OnDisable()
        {
            Animation.enabled = false;
            _meshRenderer.enabled = false;
        }
    }
}