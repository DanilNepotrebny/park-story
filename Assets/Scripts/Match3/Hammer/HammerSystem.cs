﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.Hammer
{
    public class HammerSystem : MonoBehaviour
    {
        [SerializeField, Range(1, 5)] private int _impactPower = 1;
        [SerializeField, RequiredField] private CountedItem _inventoryItem;

        [Inject] private ImpactSystem _impactSystem;

        public const int SpendAmount = 1;
        public const ImpactFlag HammerImpactFlag = ImpactFlag.Hammer;

        public CountedItem InventoryItem => _inventoryItem;

        public event HammerStrikingDelegate Striking;
        public event Action<bool> EnabledChanged;

        /// <summary> Prepare target to be impacted by the hammer </summary>
        /// <param name="target"> Target cell </param>
        public bool SetTarget([NotNull] CellComponent target)
        {
            if (!enabled ||
                !IsValidTarget(target))
            {
                return false;
            }

            Assert.IsTrue(InventoryItem.IsSpendingPossible(SpendAmount));
            Impact impact = _impactSystem.ScheduleImpact(target, -_impactPower, HammerImpactFlag);
            impact.Begin();

            var di = new DeferredInvocation(() => DoImpact(impact));
            IDeferredInvocationHandle strikeHandle = di.Start();
            Striking?.Invoke(target, strikeHandle);
            strikeHandle.Unlock();

            return true;
        }

        protected void OnEnable()
        {
            EnabledChanged?.Invoke(true);
        }

        protected void OnDisable()
        {
            EnabledChanged?.Invoke(false);
        }

        protected void Start()
        {
            // added to be able disable/enable script from editor
        }

        private static bool IsValidTarget([NotNull] CellComponent target)
        {
            // TODO: Check for golden/platinum cell
            return !target.IsEmpty &&
                target.IsStable &&
                target.Chip.CanBeImpacted(HammerImpactFlag);
        }

        private void DoImpact([NotNull] Impact impact)
        {
            if (InventoryItem.TrySpend(SpendAmount, GamePlace.InGame))
            {
                impact.Perform();
            }
            else
            {
                UnityEngine.Debug.LogError("Hammer system can't strike due to lack of hammer items");
                impact.Cancel();
            }
        }

        #region Inner types

        public delegate void HammerStrikingDelegate(
            [NotNull] CellComponent target,
            [NotNull] IDeferredInvocationHandle commitStrikeHandle);

        #endregion Inner types
    }
}