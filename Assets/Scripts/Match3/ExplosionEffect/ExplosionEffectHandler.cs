﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.ExplosionEffect
{
    [RequireComponent(typeof(RenderersController))]
    public class ExplosionEffectHandler : MonoBehaviour
    {
        [SerializeField] private string _effectMatrixName = "_ExplosionEffectTransformation";
        [SerializeField] private float _radius = 0.78f;
        [SerializeField] private float _restoreStateDuration = 0.5f;

        [Inject] private ExplosionEffectSystem _explosionEffectSystem;

        private RenderersController _renderersController;
        private MaterialPropertyBlock _propertyBlock;

        public Vector3 Origin => transform.position;
        public float Radius => _radius;
        public Matrix4x4 LastTransformation { get; private set; } = Matrix4x4.identity;

        public void SetTransformation(Matrix4x4 effectTransformation)
        {
            if (_propertyBlock == null)
            {
                _propertyBlock = new MaterialPropertyBlock();
            }

            foreach (Renderer r in _renderersController.Renderers)
            {
                r.GetPropertyBlock(_propertyBlock);
                _propertyBlock.SetMatrix(_effectMatrixName, effectTransformation);
                r.SetPropertyBlock(_propertyBlock);
            }

            LastTransformation = effectTransformation;
        }

        protected virtual void Awake()
        {
            _renderersController = GetComponent<RenderersController>();
        }

        protected void Start()
        {
            Activate();
        }

        protected void OnDestroy()
        {
            Deactivate();
        }

        protected void Activate()
        {
            _explosionEffectSystem.RegisterEffectHandler(this);
        }

        protected void Deactivate()
        {
            _explosionEffectSystem.UnregisterEffectHandler(this, true, _restoreStateDuration);
        }
    }
}