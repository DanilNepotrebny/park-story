﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Match3.ExplosionEffect
{
    public partial class ExplosionEffectSystem : MonoBehaviour
    {
        private List<Effect> _explosionEffects = new List<Effect>();
        private List<ExplosionEffectHandler> _effectHandlers = new List<ExplosionEffectHandler>();

        public void CreateExplosion(Vector3 center, EffectSettings effectSettings, Action expirationCallback = null)
        {
            var effect = new Effect(center, effectSettings, expirationCallback);
            _explosionEffects.Add(effect);
        }

        protected void Update()
        {
            if (_explosionEffects.Count == 0)
            {
                return;
            }

            CleanUp();

            foreach (Effect effect in _explosionEffects.ToArray())
            {
                effect.Update();
            }

            _explosionEffects.RemoveAll(e => e.IsExpired());

            foreach (ExplosionEffectHandler handler in _effectHandlers)
            {
                Matrix4x4 transformation = Matrix4x4.identity;
                foreach (Effect effect in _explosionEffects)
                {
                    transformation *= effect.CalculateTransformation(handler);
                }

                handler.SetTransformation(transformation);
            }
        }

        protected void OnDrawGizmos()
        {
            foreach (Effect effect in _explosionEffects)
            {
                effect.DrawGizmos();
            }
        }

        public void RegisterEffectHandler(ExplosionEffectHandler handler)
        {
            if (!_effectHandlers.Contains(handler))
            {
                _effectHandlers.Add(handler);
            }
        }

        public void UnregisterEffectHandler(
            ExplosionEffectHandler handler,
            bool revertToNormalState = false,
            float duration = 0)
        {
            if (!_effectHandlers.Remove(handler))
            {
                return;
            }

            if (revertToNormalState)
            {
                Matrix4x4 lastTransformation = handler.LastTransformation;
                if (!lastTransformation.isIdentity)
                {
                    LerpHandlerTransformation(handler, Matrix4x4.identity, duration);
                }
            }
        }

        private void LerpHandlerTransformation(ExplosionEffectHandler handler, Matrix4x4 to, float duration)
        {
            Matrix4x4 from = handler.LastTransformation;
            Vector3 fromPos = from.ExtractTranslationFromMatrix();
            Quaternion fromRot = from.rotation;
            Vector3 fromScale = from.lossyScale;

            Vector3 toPos = to.ExtractTranslationFromMatrix();
            Quaternion toRot = to.rotation;
            Vector3 toScale = to.lossyScale;

            LTDescr d = LeanTween.value(0, 1, duration);
            d.setOnUpdate(
                (ratio) =>
                {
                    if (handler == null)
                    {
                        LeanTween.cancel(d.id);
                        return;
                    }

                    Vector3 translation = Vector3.Lerp(fromPos, toPos, ratio);
                    Quaternion rotation = Quaternion.Lerp(fromRot, toRot, ratio);
                    Vector3 scale = Vector3.Lerp(fromScale, toScale, ratio);

                    handler.SetTransformation(Matrix4x4.TRS(translation, rotation, scale));
                });
        }

        private void CleanUp()
        {
            int removedAmount = _effectHandlers.RemoveAll(handler => handler == null);
            Assert.IsTrue(removedAmount == 0, $"{removedAmount} {nameof(ExplosionEffectHandler)}s were cleaned up");
        }
    }
}