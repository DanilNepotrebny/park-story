﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Match3.ExplosionEffect
{
    public class ExplosionEffectChipHandler : ExplosionEffectHandler
    {
        private ChipPresenter _presenter;

        protected override void Awake()
        {
            base.Awake();

            _presenter = GetComponentInParent<ChipPresenter>();
        }

        protected void OnEnable()
        {
            _presenter.ChipComponent.ContainerCellChanged += OnContainerCellChanged;

            OnContainerCellChanged(_presenter.ChipComponent.ContainerCell, null);
        }

        protected void OnDisable()
        {
            _presenter.ChipComponent.ContainerCellChanged -= OnContainerCellChanged;
        }

        private void OnContainerCellChanged(CellComponent newCell, CellComponent oldCell)
        {
            if (newCell == null)
            {
                Deactivate();
            }
            else
            {
                Activate();
            }
        }
    }
}