﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Match3.ExplosionEffect
{
    public partial class ExplosionEffectSystem
    {
        [Serializable]
        public class EffectSettings
        {
            [SerializeField] private float _displacementMin = 0.1f;
            [SerializeField] private float _displacementMax = 0.5f;
            [SerializeField] private AnimationCurve _displacementEasing = AnimationCurve.Linear(0, 0, 1, 1);

            [SerializeField] private float _squeezingMin = 0.2f;
            [SerializeField] private float _squeezingMax = 0.2f;
            [SerializeField] private AnimationCurve _squeezingEasing = AnimationCurve.Linear(0, 0, 1, 1);

            [SerializeField] private float _minRadius = 0.78f;
            [SerializeField] private float _maxRadius = 2;

            [SerializeField, Tooltip("Distance from handler at which effect on handler would be maximal.")]
            private float _transformationDistance = 1;

            [SerializeField] private float _baseSpeed = 6;
            [SerializeField] private AnimationCurve _easing = AnimationCurve.Linear(0, 0, 1, 1);

            public float DisplacementMin => _displacementMin;
            public float DisplacementMax => _displacementMax;
            public AnimationCurve DisplacementEasing => _displacementEasing;

            public float SqueezingMin => _squeezingMin;
            public float SqueezingMax => _squeezingMax;
            public AnimationCurve SqueezingEasing => _squeezingEasing;

            public float MinRadius => _minRadius;
            public float MaxRadius => _maxRadius;
            public float TransformationDistance => _transformationDistance;
            public float TransformationRadius => MaxRadius + TransformationDistance;

            public float Speed => _baseSpeed;
            public AnimationCurve Easing => _easing;
        }

        private class Effect
        {
            private Vector3 _origin;
            private readonly EffectSettings _settings;
            private readonly Action _expirationCallback;

            private float _time;
            private float _timeMax;
            private float _radius;

            public Effect(Vector3 origin, EffectSettings effectSettings, Action expirationCallback)
            {
                _origin = origin;
                _settings = effectSettings;
                _expirationCallback = expirationCallback;

                _timeMax = (_settings.TransformationRadius - _settings.MinRadius) / _settings.Speed;
            }

            public bool IsExpired()
            {
                return _time >= _timeMax;
            }

            public void Update()
            {
                if (IsExpired())
                {
                    return;
                }

                _time += Time.deltaTime;

                float progress = Mathf.Clamp(_time / _timeMax, 0, 1);
                progress = _settings.Easing.Evaluate(progress);
                _radius = _settings.MinRadius + (_settings.TransformationRadius - _settings.MinRadius) * progress;

                if (IsExpired())
                {
                    _expirationCallback?.Invoke();
                }
            }

            public void DrawGizmos()
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(_origin, _settings.MinRadius);

                Gizmos.color = Color.blue;
                Gizmos.DrawWireSphere(_origin, _settings.MaxRadius);

                Gizmos.color = Color.magenta;
                Gizmos.DrawWireSphere(_origin, _settings.TransformationRadius);

                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(_origin, _radius);
            }

            public Matrix4x4 CalculateTransformation(ExplosionEffectHandler handler)
            {
                Matrix4x4 transformation = Matrix4x4.identity;

                Vector3 distanceVector = handler.Origin - _origin;
                float distance = distanceVector.magnitude;
                if (distance > _settings.MinRadius &&
                    distance < _settings.MaxRadius &&
                    distance < _radius)
                {
                    float distanceFromMinRadius = distance - _settings.MinRadius;
                    float radiusRange = _settings.MaxRadius - _settings.MinRadius;
                    float effectValueFactor = 1 - Mathf.Clamp(distanceFromMinRadius / radiusRange, 0, 1);
                    float effectProgress = (_radius - distance) / _settings.TransformationDistance;

                    float scaleDecrease = CalculateValue(
                            _settings.SqueezingMin,
                            _settings.SqueezingMax,
                            _settings.SqueezingEasing,
                            effectValueFactor,
                            effectProgress
                        );

                    float scale = 1 - scaleDecrease;

                    Vector3 scaleOrigin = handler.Origin + distanceVector.normalized * handler.Radius;

                    float rotationAngle = Vector3.SignedAngle(Vector3.right, distanceVector, new Vector3(0, 0, 1));
                    transformation *= Matrix4x4.Translate(scaleOrigin);
                    transformation *= Matrix4x4.Rotate(Quaternion.Euler(new Vector3(0, 0, rotationAngle)));
                    transformation *= Matrix4x4.Scale(new Vector3(scale, 1 / scale, 1));
                    transformation *= Matrix4x4.Rotate(Quaternion.Euler(new Vector3(0, 0, -rotationAngle)));
                    transformation *= Matrix4x4.Translate(-scaleOrigin);

                    float displacement = CalculateValue(
                            _settings.DisplacementMin,
                            _settings.DisplacementMax,
                            _settings.DisplacementEasing,
                            effectValueFactor,
                            effectProgress
                        );
                    transformation *= Matrix4x4.Translate(distanceVector.normalized * displacement);
                }

                return transformation;
            }

            private float CalculateValue(
                float valueMin,
                float valueMax,
                AnimationCurve valueEasing,
                float valueFactor,
                float progress)
            {
                float easedProgress = valueEasing.Evaluate(progress);
                float valueRange = valueMax - valueMin;
                float value = easedProgress * (valueMin + valueRange * valueFactor);
                return value;
            }
        }
    }
}