﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.ExplosionEffect
{
    public class ExplosionEffectSpawner : MonoBehaviour
    {
        [SerializeField] private ExplosionEffectSystem.EffectSettings _explosionEffectSettings;
        [SerializeField] private bool _destroyOnExpiration;

        [Inject] private ExplosionEffectSystem _explosionEffectSystem;

        protected void OnEnable()
        {
            EditorUtils.LateInject(this);

            _explosionEffectSystem.CreateExplosion(transform.position, _explosionEffectSettings, OnExplosionExpired);
        }

        private void OnExplosionExpired()
        {
            if (_destroyOnExpiration)
            {
                gameObject.Dispose();
            }
        }
    }
}