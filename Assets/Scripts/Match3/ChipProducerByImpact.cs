﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.Requirements;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Match3
{
    public class ChipProducerByImpact : MonoBehaviour
    {
        [SerializeField, RequiredField] private ChipTag _producingChipTag;
        [SerializeField, RequiredField] private RequirementHelper _helperPrefab;

        public ChipTag ProducingChipTag => _producingChipTag;

        public RequirementHelper HelperPrefab => _helperPrefab;
    }
}
