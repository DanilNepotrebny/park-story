﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Match3.Levels;
using DreamTeam.Popups;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Match3.Popups
{
    public class MissionBriefingPopup : Popup
    {
        [Inject] private LevelSystem _levelSystem;
        [Inject] private ICurrentLevelInfo _currentLevelInfo;
        [Inject] private EquippedChipBoosters _equippedChipBoosters;

        [SerializeField] private Button _playButton;
        [SerializeField] private ClosePopupWithButtonPressed _closeButton;

        /// <summary>
        /// Unity callback
        /// </summary>
        protected new void Awake()
        {
            base.Awake();

            // TODO: Here might be a bug with multiple button event from single tap
            _equippedChipBoosters.UnequipAll();
            _playButton.onClick.AddListener(() => _levelSystem.LoadLevel(_currentLevelInfo.CurrentLevel));
            _closeButton.CloseButtonClicked += () => _equippedChipBoosters.UnequipAll();
        }
    }
}