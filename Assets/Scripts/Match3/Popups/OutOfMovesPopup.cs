﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Match3;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Popups;
using DreamTeam.Utils;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Match3.Popups
{
    public class OutOfMovesPopup : ProductPackPurchasePopup
    {
        [Inject] private Instantiator _instantiator;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private ChipBoosterGenerationSystem _boosterGenerationSystem;
        [Inject] private PriceInstanceContainer _priceInstanceContainer;

        private PurchaseDecision _decision;
        private bool _wasPurchasePossible;

        private Args _args;

        public override bool Initialize(Popup.Args popupArgs)
        {
            base.Initialize(popupArgs);

            _args = popupArgs as Args;
            Assert.IsNotNull(_args, $"Passed wrong popup args! Pass {nameof(ProductPackPurchasePopup.Args)} instead");

            _decision = PurchaseDecision.Close;
            _wasPurchasePossible = _priceInstanceContainer.GetInstance(_args.ProductPack.Price).CanBePayed;
            return true;
        }

        protected override void OnHiding()
        {
            base.OnHiding();

            var outOfMovesEvent = _instantiator.Instantiate<OutOfMovesClosedEvent>(
                    _args.ProductPack,
                    _decision,
                    _wasPurchasePossible
                );
            _analyticsSystem.Send(outOfMovesEvent);
        }
        
        protected override void OnProductPackGiven(ProductPack.Instance pack, GamePlace gamePlace)
        {
            _decision = PurchaseDecision.Purchase;
            GenerateBoosters(_args.ProductPack);

            base.OnProductPackGiven(pack, gamePlace);
        }

        private void GenerateBoosters(ProductPack pack)
        {
            List<ChipBooster> boosters = new List<ChipBooster>();
            foreach (Reward reward in pack.Rewards)
            {
                var boosterReward = reward as LevelFailureBoosterReward;
                if (boosterReward != null)
                {
                    if (boosterReward.Booster.TrySpend(boosterReward.Count, GamePlace.InGame))
                    {
                        boosters.Add(boosterReward.Booster);
                    }
                }
            }

            _boosterGenerationSystem.GenerateBoosters(boosters);
        }

        #region Inner types

        public new class Args : SingleCountedItemRewardPurchasePopup.Args
        {
        }

        #endregion Inner types
    }
}