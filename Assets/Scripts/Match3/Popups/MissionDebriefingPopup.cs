﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Reflection;
using DreamTeam.Inventory;
using DreamTeam.Popups;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Match3.Popups
{
    public class MissionDebriefingPopup : Popup
    {
        [SerializeField] private Button _replayButton;

        [Inject] private EquippedChipBoosters _equippedChipBoosters;

        private Args _args;

        public override bool Initialize(Popup.Args popupArgs)
        {
            base.Initialize(popupArgs);

            _args = popupArgs as Args;

            if (_args.UnequipAllBoosters)
            {
                _equippedChipBoosters.UnequipAll();
            }

            return true;
        }

        /// <summary> Called from UnityEngine.Events.UnityEvent set up from Unity inspector.</summary>
        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void RaiseCancelCallback()
        {
            _args?.CancelButtonCallBack?.Invoke();
        }

        protected new void Awake()
        {
            base.Awake();

            _replayButton.onClick.AddListener(OnReplayButtonClicked);
            Hidden += p => _equippedChipBoosters.UnequipAll(); //TODO: should be extracted from UI to game logic.
        }

        private void OnReplayButtonClicked()
        {
            _args?.ReplayButtonCallback?.Invoke();
        }

        #region Inner types

        public new class Args : Popup.Args
        {
            public bool UnequipAllBoosters { get; set; } = true;
            public Action ReplayButtonCallback { get; set; }
            public Action CancelButtonCallBack { get; set; }
        }

        #endregion Inner types
    }
}