﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.GameControls;
using DreamTeam.Inventory.Items;
using DreamTeam.Popups;
using DreamTeam.TransformLookup;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.UI.SymbolAnimation;
using DreamTeam.Utils;
using DreamTeam.Utils.Coroutines;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Spine;
using Spine;
using Spine.Unity.Modules;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Match3.Popups
{
    public class MissionCompletePopup : Popup
    {
        [SerializeField, Header("Mission complete popup data")]
        private GameObjectActivityList _popupObjectsInitialActivity;

        [SerializeField] private SkeletonIdleAnimationHandler _lindaIdleAnimationHandler;
        [SerializeField] private SimpleAnimation _coinsContainerAnimation;
        [SerializeField] private SimpleAnimation _tapToContinueAnimation;

        [SerializeField] private AnimatedSymbolsCreator _congratulationText;
        [SerializeField] private ParticleSystem _raysOfGodEffect;
        [SerializeField] private ParticleSystem _raysOfGodEffectAdditional;

        [SerializeField] private List<RewardAnimationInfo> _rewardsInfo;

        // appear delays
        [SerializeField] private float _congratulationTextAppearDelay;

        [SerializeField] private float _mainRewardAppearDelay;
        [SerializeField] private float _shiningAppearDelay;
        [SerializeField] private float _missionCoinsAppearDelay;
        [SerializeField] private float _tapToContinueAppearDelay;

        // rewards receiving delays
        [SerializeField] private float _missionCoinsConsumeDelay;

        [SerializeField] private float _congratulationTextDisappearDelay;

        [SerializeField] private TapGesture _tapGesture;

        [Inject] private PopupSystem _popupSystem;
        [Inject] private TransformLookupSystem _transformLookupSystem;
        [Inject] private GridAnimator _gridAnimator;
        [Inject] private GameControlsSystem _gameControlsSystem;

        [Inject(Id = GlobalInstaller.MoneyId)]
        private CountedItem _moneyItem;

        private Args _args;
        private CountedItem _mainRewardItem;
        private RewardAnimationInfo _mainRewardInfo;
        private RewardAnimationInfo _coinsRewardInfo;
        private ActionDisposable _mainRewardGameModeDisableHandle;
        private ActionDisposable _coinsRewardGameModeDisableHandle;

        public override bool Initialize(Popup.Args popupArgs)
        {
            if (!base.Initialize(popupArgs))
            {
                return false;
            }

            _args = popupArgs as Args;
            Assert.IsNotNull(_args, $"Passed wrong popup args! Expected {nameof(MissionCompletePopup.Args)}");

            _mainRewardItem = _args.MainRewardType;
            _mainRewardInfo = _rewardsInfo.Find(info => info.RewardItem == _mainRewardItem);
            _coinsRewardInfo = _rewardsInfo.Find(info => info.RewardItem == _moneyItem);

            return true;
        }

        protected override void OnShowing()
        {
            base.OnShowing();

            _gridAnimator.PlayAnimation(SimpleAnimationType.Hide);

            if (_mainRewardInfo.GameMode != null)
            {
                Assert.IsNull(_mainRewardGameModeDisableHandle);
                _mainRewardGameModeDisableHandle = _gameControlsSystem.EnableMode(_mainRewardInfo.GameMode);
            }

            if (_coinsRewardInfo.GameMode != null)
            {
                Assert.IsNull(_coinsRewardGameModeDisableHandle);
                _coinsRewardGameModeDisableHandle = _gameControlsSystem.EnableMode(_coinsRewardInfo.GameMode);
            }
        }

        protected new void Awake()
        {
            base.Awake();

            DisableRewardViews();

            Shown += OnPopupShown;
            Hidden += OnHidden;
        }

        private void OnHidden(Popup popup)
        {
            DisposeRewardsGameModeHandles();
        }

        private void DisposeRewardsGameModeHandles()
        {
            _mainRewardGameModeDisableHandle?.Dispose();
            _mainRewardGameModeDisableHandle = null;

            _coinsRewardGameModeDisableHandle?.Dispose();
            _coinsRewardGameModeDisableHandle = null;
        }

        protected void Start()
        {
            foreach (var objectActivity in _popupObjectsInitialActivity)
            {
                objectActivity.Key.SetActive(objectActivity.Value);
            }

            StartCoroutine(PlayAppearSequence());
        }

        private new void OnDestroy()
        {
            DisposeRewardsGameModeHandles();

            base.OnDestroy();
        }

        private IEnumerator PlayAppearSequence()
        {
            yield return new WaitForEvent<Popup, Popup>(this, nameof(Shown));
            yield return StartCoroutine(PlayCongratulationTextAppearAnimation());
            yield return StartCoroutine(PlayMainRewardAppearAnimation());
            yield return StartCoroutine(PlayShiningAppearAnimation());
            yield return StartCoroutine(PlayMissionCoinsAppearAnimation());
            yield return StartCoroutine(PlayTapToContinueAppearAnimation());
        }

        private IEnumerator PlayCongratulationTextAppearAnimation()
        {
            yield return new WaitForSeconds(_congratulationTextAppearDelay);

            _congratulationText.gameObject.SetActive(true);
            yield return _congratulationText.Show();
        }

        private IEnumerator PlayMainRewardAppearAnimation()
        {
            _mainRewardInfo.View.gameObject.SetActive(true);

            TrackEntry track = _mainRewardInfo.View.AnimationState.SetAnimation(
                    0,
                    _mainRewardInfo.SkeletonAnimations[SkeletonAnimationType.Show],
                    false
                );

            track.Complete += OnMainRewardAppeared;

            _mainRewardInfo.View.Update();
            _mainRewardInfo.View.freeze = true;

            yield return new WaitForSeconds(_mainRewardAppearDelay);

            _mainRewardInfo.View.freeze = false;
        }

        private void OnMainRewardAppeared(TrackEntry track)
        {
            _mainRewardInfo.View.AnimationState.SetAnimation(
                    0,
                    _mainRewardInfo.SkeletonAnimations[SkeletonAnimationType.Idle],
                    true
                );
        }

        private IEnumerator PlayShiningAppearAnimation()
        {
            yield return new WaitForSecondsRealtime(_shiningAppearDelay);

            _raysOfGodEffect.gameObject.SetActive(true);
            _raysOfGodEffectAdditional.gameObject.SetActive(true);

            if (_mainRewardInfo.AdditionalEffect != null)
            {
                _mainRewardInfo.AdditionalEffect.gameObject.SetActive(true);
            }
        }

        private IEnumerator PlayMissionCoinsAppearAnimation()
        {
            yield return new WaitForSeconds(_missionCoinsAppearDelay);

            _coinsContainerAnimation.gameObject.SetActive(true);
            _coinsContainerAnimation.AnimationFinished += OnCoinsContainerAnimationFinished;
            if (!_coinsContainerAnimation.Play(SimpleAnimationType.Show))
            {
                OnCoinsContainerAnimationFinished(_coinsContainerAnimation, SimpleAnimationType.Show);
            }
        }

        private void OnCoinsContainerAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            _coinsContainerAnimation.AnimationFinished -= OnCoinsContainerAnimationFinished;
            if (SimpleAnimationType.Hide == animationType)
            {
                _coinsContainerAnimation.gameObject.SetActive(false);
            }
        }

        private IEnumerator PlayTapToContinueAppearAnimation()
        {
            yield return new WaitForSeconds(_tapToContinueAppearDelay);

            StartCoroutine(PlayRewardsReceivingSequence());

            _tapToContinueAnimation.gameObject.SetActive(true);
            _tapToContinueAnimation.AnimationFinished += OnTapToContinueAnimationFinished;
            if (!_tapToContinueAnimation.Play(SimpleAnimationType.Show))
            {
                OnTapToContinueAnimationFinished(_tapToContinueAnimation, SimpleAnimationType.Show);
            }
        }

        private void OnTapToContinueAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            _tapToContinueAnimation.AnimationFinished -= OnTapToContinueAnimationFinished;
            if (SimpleAnimationType.Show == animationType)
            {
                _tapToContinueAnimation.Play(SimpleAnimationType.Idle);
            }
            else if (animationType == SimpleAnimationType.Hide)
            {
                _tapToContinueAnimation.gameObject.SetActive(false);
            }
        }

        private IEnumerator PlayRewardsReceivingSequence()
        {
            yield return new WaitForEvent<object, EventArgs, TapGesture>(_tapGesture, nameof(_tapGesture.Tapped));

            _coinsRewardInfo.Animator.FinishPoint = _transformLookupSystem.FindTransform(_coinsRewardInfo.TransformId);
            _mainRewardInfo.Animator.FinishPoint = _transformLookupSystem.FindTransform(_mainRewardInfo.TransformId);

            PlayShiningDisappearAnimation();
            PlayTapToContinueTextDisappearAnimation();
            Coroutine starConsumingCoroutine = StartCoroutine(PlayMainRewardConsumeAnimation());

            yield return StartCoroutine(PlayMissionCoinsConsumeAnimation());
            yield return StartCoroutine(PlayCongratulationTextDisappearAnimation());
            yield return starConsumingCoroutine;

            _popupSystem.Hide(gameObject.scene.name);
        }

        private void PlayShiningDisappearAnimation()
        {
            _raysOfGodEffect.Stop();
            _raysOfGodEffectAdditional.Stop();

            if (_mainRewardInfo.AdditionalEffect != null)
            {
                _mainRewardInfo.AdditionalEffect.Stop();
            }
        }

        private IEnumerator PlayMainRewardConsumeAnimation()
        {
            DisableRewardViews();

            _mainRewardInfo.RewardsLeftToReceive = _args.MainRewardsAmountReceived;
            _mainRewardInfo.Animator.Play();
            _mainRewardInfo.Animator.AnElementReached += OnMainRewardElementReachedDestination;

            yield return new WaitForEvent<GroupMotionAnimator>(
                _mainRewardInfo.Animator,
                nameof(_mainRewardInfo.Animator.MotionFinished));

            _mainRewardInfo.Animator.AnElementReached -= OnMainRewardElementReachedDestination;

            if (_mainRewardInfo.RewardsLeftToReceive > 0)
            {
                _args.GiveMainRewardCallback?.Invoke(_mainRewardInfo.RewardsLeftToReceive);
            }
        }

        private void OnMainRewardElementReachedDestination()
        {
            OnRewardProjectileReachedDestination(
                _args.MainRewardsAmountReceived,
                ref _mainRewardInfo,
                _args.GiveMainRewardCallback);
        }

        private void PlayTapToContinueTextDisappearAnimation()
        {
            _tapToContinueAnimation.AnimationFinished += OnTapToContinueAnimationFinished;
            if (!_tapToContinueAnimation.Play(SimpleAnimationType.Hide))
            {
                OnTapToContinueAnimationFinished(_tapToContinueAnimation, SimpleAnimationType.Hide);
            }
        }

        private IEnumerator PlayMissionCoinsConsumeAnimation()
        {
            yield return new WaitForSeconds(_missionCoinsConsumeDelay);

            _coinsRewardInfo.RewardsLeftToReceive = _args.CoinsAmountReceived;
            _coinsRewardInfo.Animator.Play();
            _coinsRewardInfo.Animator.AnElementReached += OnCoinRewardElementReachedDestination;

            if (!_coinsContainerAnimation.Play(SimpleAnimationType.Hide))
            {
                OnCoinsContainerAnimationFinished(_coinsContainerAnimation, SimpleAnimationType.Hide);
            }

            yield return new WaitForEvent<GroupMotionAnimator>(
                _coinsRewardInfo.Animator,
                nameof(_coinsRewardInfo.Animator.MotionFinished));

            _coinsRewardInfo.Animator.AnElementReached -= OnCoinRewardElementReachedDestination;

            if (_coinsRewardInfo.RewardsLeftToReceive > 0)
            {
                _args.GiveCoinsRewardCallback?.Invoke(_coinsRewardInfo.RewardsLeftToReceive);
            }
        }

        private void OnCoinRewardElementReachedDestination()
        {
            OnRewardProjectileReachedDestination(
                    _args.CoinsAmountReceived,
                    ref _coinsRewardInfo,
                    _args.GiveCoinsRewardCallback
                );
        }

        private void OnRewardProjectileReachedDestination(
                int initialAmount,
                ref RewardAnimationInfo animInfo,
                Action<int> callback
            )
        {
            SpawnComingEffect(animInfo);

            int giveAmount = initialAmount / animInfo.Animator.ElementsAmount;
            animInfo.RewardsLeftToReceive -= giveAmount;
            if (giveAmount > 0)
            {
                callback.Invoke(giveAmount);
            }
        }

        private IEnumerator PlayCongratulationTextDisappearAnimation()
        {
            yield return new WaitForSeconds(_congratulationTextDisappearDelay);
            yield return _congratulationText.Hide();
        }

        private void OnPopupShown(Popup popup)
        {
            if (_lindaIdleAnimationHandler.isActiveAndEnabled)
            {
                _lindaIdleAnimationHandler.PlayAnimation(false);
            }
        }

        private void SpawnComingEffect(RewardAnimationInfo animInfo)
        {
            if (animInfo.ComingEffectPrefab == null)
            {
                return;
            }

            Transform starsPanelTransform = _transformLookupSystem.FindTransform(animInfo.TransformId);
            ParticleSystemPlayer effect = Instantiate(animInfo.ComingEffectPrefab);
            effect.transform.SetParent(starsPanelTransform);
            effect.transform.localPosition = Vector3.zero;
            effect.transform.localRotation = Quaternion.identity;
            effect.transform.localScale = Vector3.one;
            effect.Play();
        }

        private void DisableRewardViews()
        {
            foreach (RewardAnimationInfo info in _rewardsInfo)
            {
                if (info.View != null)
                {
                    info.View.gameObject.SetActive(false);
                    info.View.canvasRenderers.ForEach(item => item.Clear());
                }
            }
        }

        #region Inner types

        public new class Args : Popup.Args
        {
            public Action<int> GiveMainRewardCallback { get; set; }
            public Action<int> GiveCoinsRewardCallback { get; set; }

            public CountedItem MainRewardType { get; set; }
            public int MainRewardsAmountReceived { get; set; }
            public int CoinsAmountReceived { get; set; }
        }

        public enum SkeletonAnimationType
        {
            Show,
            Idle
        }

        [Serializable] public class SkeletonAnimationNamesList : KeyValueList<SkeletonAnimationType, string>
        {
        }

        [Serializable] public class GameObjectActivityList : KeyValueList<GameObject, bool>
        {
        }

        [Serializable] public struct RewardAnimationInfo
        {
            [SerializeField, RequiredField] private CountedItem _rewardItem;
            [SerializeField] private GameControlsMode _gameMode;
            [FormerlySerializedAs("_projectile"), SerializeField] private SkeletonGraphicMultiObject _view;
            [SerializeField, RequiredField] private GroupMotionAnimator _animator;
            [SerializeField, RequiredField] private TransformLookupId _transformId;
            [SerializeField, ContainsAllEnumKeys] private SkeletonAnimationNamesList _skeletonAnimations;
            [SerializeField] private ParticleSystemPlayer _comingEffectPrefab;
            [SerializeField] private ParticleSystem _additionalEffect;

            public CountedItem RewardItem => _rewardItem;

            public GameControlsMode GameMode => _gameMode;

            public SkeletonGraphicMultiObject View => _view;

            public GroupMotionAnimator Animator => _animator;

            public TransformLookupId TransformId => _transformId;

            public SkeletonAnimationNamesList SkeletonAnimations => _skeletonAnimations;

            public ParticleSystemPlayer ComingEffectPrefab => _comingEffectPrefab;

            public ParticleSystem AdditionalEffect => _additionalEffect;

            public int RewardsLeftToReceive { get; set; }
        }

        #endregion Inner types
    }
}