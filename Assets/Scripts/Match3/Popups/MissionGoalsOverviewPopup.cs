﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Popups;
using DreamTeam.UI.SimpleAnimating;
using Zenject;

namespace DreamTeam.Match3.Popups
{
    public class MissionGoalsOverviewPopup : Popup
    {
        [Inject] private ChipBoosterGenerationSystem _chipBoosterGenerationSystem;
        [Inject] private GridAnimator _gridAnimator;

        public override bool Initialize(Popup.Args popupArgs)
        {
            base.Initialize(popupArgs);

            _gridAnimator.AnimationFinished += GridAnimatorOnAnimationFinished;

            return true;
        }

        public override void Hide(Action callback = null)
        {
            _gridAnimator.PlayAnimation(SimpleAnimationType.Show);
            base.Hide(callback);
        }

        private void GridAnimatorOnAnimationFinished(SimpleAnimationType animationType)
        {
            if (SimpleAnimationType.Show == animationType)
            {
                _chipBoosterGenerationSystem.GenerateEquippedBoosters();
            }
        }

        #region Inner types

        public new class Args : Popup.Args
        {
        }

        #endregion Inner types
    }
}