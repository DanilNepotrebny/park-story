﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.UI.HUD;
using DreamTeam.Popups;
using Zenject;

namespace DreamTeam.Match3.Popups
{
    public class MissionGoalsCompletedOverviewPopup : Popup
    {
        [Inject] private LevelCompletionSystem _levelCompletionSystem;
        [Inject] private ICurrentLevelInfo _currentLevelInfo;
        [Inject] private GoalsAndCoinsContainer _coinsPresenter;

        protected void Start()
        {
            Hidden += OnHidden;
        }

        private void OnHidden(Popup popup)
        {
            _coinsPresenter.ShowCoins();
        }
    }
}