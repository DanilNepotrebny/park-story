﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Utils.Movement;
using UnityEngine;

namespace DreamTeam.Match3
{
    public class IntermediatePointGravityOrientator : MonoBehaviour
    {
        [SerializeField] private FixedIntermediatePointMovementController _controller;

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void OrientByGravity(ChipPresenter presenter)
        {
            Direction gravity = presenter.ChipComponent.Cell.Gravity;

            _controller.IntermediatePointOffset =
                Quaternion.FromToRotation(Vector3.down, (Vector2)gravity.GetOffset()) *
                _controller.IntermediatePointOffset;
        }
    }
}