﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.ChipTags;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3.ChipBoosterPlacingRules
{
    /// <summary>
    /// Restricts placing chips close to the ones with <param name="_chipTag"/> tag
    /// </summary>
    public class DisallowClosePlacingRule : BaseChipBoosterPlacingRule
    {
        [SerializeField, RequiredField] private ChipTag _chipTag;

        [Inject] private GridSystem _gridSystem;

        public override bool CanBePlaced(ChipComponent chip, CellComponent cell)
        {
            if (!chip.Tags.Has(_chipTag, false))
            {
                return true;
            }

            bool isTopCellValid = IsValidCell(cell.GridPosition + Vector2Int.up);
            bool isBottomCellValid = IsValidCell(cell.GridPosition + Vector2Int.down);
            bool isLeftCellValid = IsValidCell(cell.GridPosition + Vector2Int.left);
            bool isRightCellValid = IsValidCell(cell.GridPosition + Vector2Int.right);

            return isTopCellValid && isBottomCellValid && isLeftCellValid && isRightCellValid;
        }

        private bool IsValidCell(Vector2Int cellGridPos)
        {
            CellComponent cell = _gridSystem.GetCell(cellGridPos);
            if (cell == null)
            {
                return true;
            }

            ChipComponent chip = cell.Chip;
            while (chip != null)
            {
                if (chip.Tags.Has(_chipTag, false))
                {
                    return false;
                }

                chip = chip.Child;
            }

            return true;
        }
    }
}
