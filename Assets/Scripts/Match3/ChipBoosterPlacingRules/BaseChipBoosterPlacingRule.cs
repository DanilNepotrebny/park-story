﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Match3.ChipBoosterPlacingRules
{
    public abstract class BaseChipBoosterPlacingRule : MonoBehaviour
    {
        /// <summary> Is <param name="chip"/> can be placed into <param name="cell"/> </summary>
        public abstract bool CanBePlaced([NotNull] ChipComponent chip, [NotNull] CellComponent cell);
    }
}
