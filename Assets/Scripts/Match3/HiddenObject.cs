﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    public partial class HiddenObject : MonoBehaviour, IPlaceable
    {
        [SerializeField] private int _releasingImpact = -1;

        [Header("Rotation")]
        [SerializeField]
        private bool _isRotated;

        [SerializeField] private Vector3 _rotation;

        [Inject] private GridSystem _gridCache;
        [Inject] private ImpactSystem _impactSystem;
        [InjectOptional] private HiddenObjectPresenter _presenter;

        private List<CellComponent> _lockedCells = new List<CellComponent>();
        private int _awaitedChipsCount;
        private int _awaitedUnderlaysCount;
        private DeferredInvocation _releasedInvocation;
        private Renderer[] _renderersCache = null;

        public HiddenObjectPresenter Presenter => _presenter;
        public IEnumerable<CellComponent> CellsToRelease => Cells.Where(cell => cell.Underlay != null);
        public RectInt Bounds => _grid.WorldToGridBounds(GetRendererBounds());
        public int Square => Bounds.width * Bounds.height;
        private bool _isEditMode => Application.isEditor && !Application.isPlaying;
        private Renderer[] _renderers
        {
            get
            {
                if (_renderersCache == null || _isEditMode)
                {
                    _renderersCache = GetComponentsInChildren<Renderer>(false);
                }
                return _renderersCache;
            }
        }

        private GridSystem _grid
        {
            get
            {
                if (_gridCache == null)
                {
                    _gridCache = FindObjectOfType<GridSystem>();
                }

                return _gridCache;
            }
        }

        public IEnumerable<CellComponent> Cells
        {
            get
            {
                for (int x = Bounds.x; x < Bounds.xMax; ++x)
                {
                    for (int y = Bounds.y; y < Bounds.yMax; ++y)
                    {
                        CellComponent cell = _grid.GetCell(new Vector2Int(x, y));
                        if (cell == null)
                        {
                            UnityEngine.Debug.LogError($"Hidden Object {name} doesn't lay on a cell at {x}x{y}.", this);
                            continue;
                        }

                        yield return cell;
                    }
                }
            }
        }

        public event Action<HiddenObject, IDeferredInvocationHandle> Released;

        protected void Start()
        {
            _releasedInvocation = new DeferredInvocation(OnReleased);

            _awaitedUnderlaysCount = 0;
            foreach (CellComponent cell in Cells)
            {
                if (cell.Underlay != null)
                {
                    _awaitedUnderlaysCount++;
                    cell.Underlay.Destroyed += OnUnderlayDestroyed;
                }
            }
        }

        private void OnUnderlayDestroyed(Underlay underlay)
        {
            underlay.Destroyed -= OnUnderlayDestroyed;

            --_awaitedUnderlaysCount;
            if (_awaitedUnderlaysCount == 0)
            {
                Release();
            }
        }

        private void Release()
        {
            List<CellComponent> cellsToImpact = Cells.ToList();
            List<Impact> impacts = _impactSystem.ScheduleImpact(
                    cellsToImpact,
                    _releasingImpact,
                    ImpactFlag.HiddenObjectRelease
                );

            foreach (CellComponent cell in cellsToImpact)
            {
                ChipComponent chip = cell.Chip;
                if (chip != null && chip.WillBeImpacted())
                {
                    _lockedCells.Add(cell);

                    _awaitedChipsCount++;
                    chip.Impacted += OnImpacted;
                    cell.BeginProcess(CellComponent.ProcessType.HiddenObjectRelease);
                }
            }

            impacts.ForEach(i => i.Perform());

            CheckChipsImpacted();
        }

        private void OnImpacted(ChipComponent chip)
        {
            chip.Impacted -= OnImpacted;

            _awaitedChipsCount--;
            CheckChipsImpacted();
        }

        private void CheckChipsImpacted()
        {
            if (_awaitedChipsCount > 0)
            {
                return;
            }

            foreach (CellComponent cell in _lockedCells)
            {
                cell.EndProcess(CellComponent.ProcessType.HiddenObjectRelease);
            }

            _lockedCells.Clear();

            using (IDeferredInvocationHandle handle = _releasedInvocation.Start())
            {
                Released?.Invoke(this, handle);
            }
        }

        private void OnReleased()
        {
            gameObject.Dispose();
        }

        private Rect GetRendererBounds()
        {
            Rect bounds = Rect.MinMaxRect(float.MaxValue, float.MaxValue, float.MinValue, float.MinValue);

            foreach (Renderer item in _renderers)
            {
                bounds = bounds.Encapsulate(item.bounds.ToRect());
            }

            return bounds;
        }

        #region IPlaceable

        Type IPlaceable.GetElementType()
        {
            return typeof(HiddenObject);
        }

        void IPlaceable.Place(Vector2Int gridPos)
        {
            GridSystem grid = _grid;

            Vector3 worldPos = grid.GridToWorld(gridPos);
            Rect worldBounds = grid.GridToWorldBounds(Bounds).WithCenter(worldPos - (Vector3)grid.CellSize / 2);

            transform.position = worldBounds.max;
        }

        void IPlaceable.Rotate()
        {
            Vector3 newRot = _isRotated ? Vector3.zero : _rotation;
            transform.rotation = Quaternion.Euler(newRot);
            _isRotated = !_isRotated;
        }

        #endregion IPlaceable
    }
}