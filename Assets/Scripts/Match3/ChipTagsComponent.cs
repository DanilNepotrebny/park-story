﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3.ChipTags;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Stores chip tags assigned to this game object. Can inherit chip tags from all child game objects.
    /// </summary>
    [DisallowMultipleComponent]
    public class ChipTagsComponent : MonoBehaviour
    {
        [SerializeField] private ChipTagSet _tags = new ChipTagSet(false);

        [NonSerialized] private ChipTagSet _ownTags;
        private readonly ChipTagSet _pushedTags = new ChipTagSet();

        private ChipContainerComponent _chipContainer;

        /// <summary>
        /// See <see cref="ChipTagSet.Has"/>. Can include tags inherited from child game objects. If you want to exclude children, set <paramref name="includeChildren"/> to false.
        /// </summary>
        public bool Has(ChipTag chipTag, bool includeChildren = true)
        {
            return GetTagsToCheck(includeChildren).Has(chipTag);
        }

        public ChipTag FindFirstOf(ChipTagSet set, bool includeChildren = true)
        {
            return GetTagsToCheck(includeChildren).FindFirstOf(set);
        }

        public ChipTag FindFirstOf([NotNull] ChipComponent chip, bool includeChildren = true)
        {
            return chip.Tags.FindFirstOf(GetTagsToCheck(includeChildren), includeChildren);
        }

        /// <summary>
        /// See <see cref="ChipTagSet.Get"/>. Can include tags inherited from child game objects.
        /// If you want to exclude children, set <paramref name="includeChildren"/> to false.
        /// </summary>
        public T Get<T>(bool includeChildren = true) where T : ChipTag
        {
            return GetTagsToCheck(includeChildren).Get<T>();
        }

        /// <summary>
        /// See <see cref="ChipTagSet.GetFirst"/>. Can include tags inherited from child game objects.
        /// If you want to exclude children, set <paramref name="includeChildren"/> to false.
        /// </summary>
        public ChipTag GetFirst(bool includeChildren = true, bool mightBeAbstract = false)
        {
            return GetTagsToCheck(includeChildren).GetFirst(mightBeAbstract);
        }

        public void AddRange([NotNull] ChipTagSet tagSet)
        {
            GetOwnTags().AddRange(tagSet);
        }

        public void Push([NotNull] ChipTag chipTag)
        {
            _pushedTags.Add(chipTag);
        }

        public void Pop([NotNull] ChipTag chipTag)
        {
            _pushedTags.Remove(chipTag);
        }

        protected void Awake()
        {
            _chipContainer = GetComponent<ChipContainerComponent>();
        }

        private ChipTagSet GetTagsToCheck(bool includeChildren)
        {
            ChipTagSet tagsToCheck = new ChipTagSet();
            tagsToCheck.AddRange(GetOwnTags());
            tagsToCheck.AddRange(_pushedTags);

            if (includeChildren &&
                _chipContainer != null &&
                _chipContainer.IsChipVisible)
            {
                tagsToCheck.AddRange(_chipContainer.Chip.Tags.GetTagsToCheck(true));
            }

            return tagsToCheck;
        }

        private ChipTagSet GetOwnTags()
        {
            // Create lazily because can be accecced from prefab.
            if (_ownTags == null)
            {
                _ownTags = new ChipTagSet();
                _ownTags.AddRange(_tags);
            }

            return _ownTags;
        }
    }
}