﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Match3
{
    /// <summary> Presenter for chips which may contain another chips (boxes, chains, etc.). </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(ChipPresenter))]
    public class ChipContainerPresenter : MonoBehaviour, IImpactController
    {
        [SerializeField] private int _childChipSortingOrderOffset = -20;

        private bool _wasStarted;
        private ChipPresenter _chipPresenter;
        private ChipComponent _chip => _chipPresenter.ChipComponent;
        private ChipContainerComponent _chipContainer;

        private DeferredInvocation _childAttached;

        public event Action<IDeferredInvocationHandle> ChildAttaching;

        protected void Awake()
        {
            _chipPresenter = GetComponent<ChipPresenter>();
            _chipPresenter.Appearing += OnAppearing;

            _chipContainer = _chip.GetComponent<ChipContainerComponent>();

            _childAttached = new DeferredInvocation(OnChildAttached);

            UnityEngine.Debug.Assert(_chipContainer != null);

            _chip.AddImpactController(this);
        }

        protected void Start()
        {
            _wasStarted = true;
            OnEnable();
        }

        protected void OnEnable()
        {
            if (!_wasStarted)
            {
                return;
            }

            _chip.ChildChanged += OnChildChanged;
            OnChildChanged(null, _chip?.Child);
        }

        protected void OnDisable()
        {
            OnChildChanged(_chip?.Child, null);
            _chip.ChildChanged -= OnChildChanged;
        }

        private void OnChildChanged(ChipComponent prevChild, ChipComponent child)
        {
            if (prevChild != null)
            {
                prevChild.ViewDetached -= OnChildViewDetached;

                ChipPresenter previousPresenter = prevChild.Presenter;
                if (previousPresenter != null)
                {
                    previousPresenter.gameObject.SetActive(true);
                    previousPresenter.SortingOptions.ResetParent();
                }
            }

            if (child != null)
            {
                child.ViewDetached += OnChildViewDetached;

                ChipPresenter childPresenter = child.Presenter;
                if (childPresenter != null)
                {
                    childPresenter.SortingOptions.SetParent(_chipPresenter.SortingOptions, _childChipSortingOrderOffset);

                    using (IDeferredInvocationHandle handle = _childAttached.Start())
                    {
                        ChildAttaching?.Invoke(handle);
                    }
                }
            }
        }

        private void OnChildAttached()
        {
            _chip.Child.Presenter.gameObject.SetActive(_chipContainer.IsChipVisible);
        }

        private void OnChildViewDetached(ChipComponent chip, ChipPresenter detachedView)
        {
            detachedView.SortingOptions.ResetParent();
        }

        private void OnAppearing()
        {
            if (_chipContainer.IsChipVisible)
            {
                _chip.Child.Presenter.Appear();
            }
        }

        void IImpactController.OnImpact(
                ChipComponent chip,
                ImpactFlag impactType,
                IDeferredInvocationHandle impactEndHandle,
                ref bool isModal
            )
        {
            ChipPresenter childPresenter = chip.Child?.Presenter;
            if (childPresenter != null)
            {
                if (chip.IsConsumed)
                {
                    childPresenter.gameObject.SetActive(true);
                    childPresenter.Appear();
                }
                
                if (_chipContainer.IsChipVisible)
                {
                    childPresenter.OnContainerImpacting(impactEndHandle);
                }
            }
        }

        ImpactPriority IImpactController.GetPriority()
        {
            return ImpactPriority.ChipContainer;
        }

        void IImpactController.Cancel()
        {
            _chip.Child?.Presenter.FinishContainerImpact();
        }
    }
}