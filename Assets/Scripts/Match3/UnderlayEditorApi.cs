﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Match3
{
    public partial class Underlay
    {
        [SerializeField] private int _maxEditorHealth = 1;

        private static Func<Underlay, EditorApi> _editorApiFactory;

        /// <summary>
        /// Editor API for the Underlay component. Should NOT be used in game logics code.
        /// </summary>
        [InitializeOnLoad]
        public class EditorApi
        {
            private WeakReference<Underlay> _hostRef;

            /// <summary>
            /// Underlay object
            /// </summary>
            public Underlay Host
            {
                get
                {
                    Underlay target;
                    if (_hostRef.TryGetTarget(out target))
                    {
                        return target;
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Max Health of the chip
            /// </summary>
            public int Health
            {
                get { return Host.Health; }
                set { Host.Health = Mathf.Clamp(value, 0, Host._maxEditorHealth); }
            }

            static EditorApi()
            {
                _editorApiFactory = host => new EditorApi(host);
            }

            private EditorApi(Underlay host)
            {
                _hostRef = new WeakReference<Underlay>(host, false);
            }
        }

        /// <summary>
        /// Returns the grid editor API object. Should NOT be used in game logics code.
        /// </summary>
        public EditorApi GetEditorApi()
        {
            return _editorApiFactory(this);
        }
    }
}

#endif