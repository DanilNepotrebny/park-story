﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Match3
{
    /// <summary>
    /// Shows / hides compatible <see cref="ChildChipDecalPresenter"/> on the child chip presenter.
    /// </summary>
    public class ChildChipDecalController : MonoBehaviour
    {
        [Inject] private ChipComponent _chip;

        private ChipComponent _chipWithDecal;

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<ChipComponent>().FromComponentInParents(includeInactive: true, useFirst: true);
        }

        protected void OnEnable()
        {
            _chip.ChildChanged += OnChildChanged;
            SetChipWithDecal(_chip.Child);
        }

        protected void OnDisable()
        {
            _chip.ChildChanged -= OnChildChanged;
            SetChipWithDecal(null);
        }

        private void OnChildChanged(ChipComponent prevchild, ChipComponent child)
        {
            SetChipWithDecal(child);
        }

        private void SetChipWithDecal(ChipComponent chip)
        {
            if (_chipWithDecal == chip)
            {
                return;
            }

            SetDecalActive(false);

            _chipWithDecal = chip;

            SetDecalActive(true);
        }

        private void SetDecalActive(bool isActive)
        {
            ChipPresenter presenter = _chipWithDecal?.Presenter;
            if (presenter == null)
            {
                return;
            }

            ChildChipDecalPresenter[] decals = presenter.GetComponentsInChildren<ChildChipDecalPresenter>(true);
            foreach (ChildChipDecalPresenter decal in decals)
            {
                if (decal.IsCompatible(_chip))
                {
                    decal.SetActive(isActive);
                }
            }
        }
    }
}