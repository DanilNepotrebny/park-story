﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Ads;
using DreamTeam.Analytics;
using DreamTeam.Clothing;
using DreamTeam.CustomerSupport;
using DreamTeam.GameControls;
using DreamTeam.GameRating;
using DreamTeam.IAP;
using DreamTeam.InputLock;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Items.UnlockNotification;
using DreamTeam.Inventory.Rewards;
using DreamTeam.InviteFriends;
using DreamTeam.KeyWordsReplacement;
using DreamTeam.Location;
using DreamTeam.Location.Bubbles;
using DreamTeam.Messaging;
using DreamTeam.PersistentCommands;
using DreamTeam.Popups;
using DreamTeam.SocialNetworking;
using DreamTeam.Synchronization;
using DreamTeam.TransformLookup;
using DreamTeam.UI.HUD;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;
#if !NO_MATCH3
using DreamTeam.Match3.Bot;
using DreamTeam.Match3.Levels.Rewards;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.UI;

#endif

namespace DreamTeam
{
    public partial class GlobalInstaller : MonoInstaller, ICountedItemQuantityProvider
    {
        public const string StarsId = "Stars";
        public const string MovesId = "Moves";
        public const string MoneyId = "Money";
        public const string BriefingBoosterGroupId = "BriefingBoosterGroup";

        #if !NO_MATCH3
        [Header("Match 3")]
        [SerializeField] private ChipTagIconSettings _chipTagIconSettings;
        [SerializeField] private SingleLevelPackController _mainLevelPackControllerPrefab;
        [SerializeField] private EndlessGrindLevelPackController _endlessGrindLevelPackControllerPrefab;
        [SerializeField] private LevelSystem _levelSystemPrefab;
        [SerializeField] private BotSystem _botSystemPrefab;
        #endif

        [Header("Counted items")]
        [SerializeField] private CountedItem _starsItem;
        [SerializeField] private CountedItem _movesItem;
        [SerializeField] private CountedItem _moneyItem;
        [SerializeField] private Lives _livesItem;
        [SerializeField] private BoosterGroup _briefingBoosterGroup;
        [SerializeField] private CountedItemContainer _countedItems;
        [SerializeField] private ProductPackInstanceContainer _productPackInstanceContainerPrefab;
        [SerializeField] private UnlockNotificationSystem _unlockNotificationSystemPrefab;

        [Header("Other")]
        [SerializeField] private TransformLookupSystem _transformLookupSystem;
        [SerializeField] private EquippedChipBoosters _equippedChipBoosters;
        [SerializeField] private LocationSystem _locationSystemPrefab;
        [SerializeField] private RewardSynchronizationFactory _rewardSyncFactoryPrefab;
        [SerializeField] private MessageSystem _messageSystemPrefab;
        [SerializeField] private List<ScriptableObject> _messageDataAssets;
        [SerializeField] private SocialProfileInventoryExchangeSystem _socialProfileExchangeSystemPrefab;
        [SerializeField] private CharactersDataCollection _charactersDataCollection;
        [SerializeField] private UserSettings _userSettingsPrefab;
        [SerializeField] private ClothingSystem _clothingSystemPrefab;
        [SerializeField] private InventoryLevelDrivenUnlockSystem _inventoryLevelDrivenUnlockPrefab;
        [SerializeField] private KeyWordsReplacementSystem _keyWordsReplacementSystemPrefab;
        [SerializeField] private HUDSystem _hudSystem;
        [SerializeField] private AnalyticsSystem _analyticsSystemPrefab;
        [FormerlySerializedAs("_persistentCommandsSystemPrefab"),SerializeField] private PersistentCommandsExecutionSystem _persistentCommandsExecutionSystemPrefab;
        [SerializeField] private SocialProfilesSystem _socialProfilesSystemPrefab;
        [SerializeField] private InviteFriendsSystem _inviteFriendsSystem;
        [SerializeField] private BubblesPositionSystem _bubblesPositionSystemPrefab;

        public const string MainLevelPackControllerId = "MainLevelPackController";

        public override void InstallBindings()
        {
            Container.Bind<ISynchronizer<CountedItem>>().To<CountedItemSynchronizer>().AsSingle();

            Container.Bind<CountedItem>().WithId(StarsId).FromInstance(_starsItem);
            Container.Bind<CountedItem>().WithId(MovesId).FromInstance(_movesItem);
            Container.Bind<CountedItem>().WithId(MoneyId).FromInstance(_moneyItem);
            Container.Bind<Lives>().FromInstance(_livesItem);

            Container.Bind<BoosterGroup>().WithId(BriefingBoosterGroupId).FromInstance(_briefingBoosterGroup);

            #if !NO_MATCH3
            {
                Container.Bind<LevelSystem>().FromComponentInNewPrefab(_levelSystemPrefab).AsSingle().NonLazy();
                Container.Bind<ICurrentLevelInfo>()
                    .FromMethod(context => context.Container.Resolve<LevelSystem>());
                Container.Bind<SingleLevelPackController>()
                    .WithId(MainLevelPackControllerId)
                    .FromComponentInNewPrefab(_mainLevelPackControllerPrefab)
                    .AsSingle()
                    .NonLazy();

                Container.Bind<EndlessGrindLevelPackController>()
                    .FromComponentInNewPrefab(_endlessGrindLevelPackControllerPrefab)
                    .AsSingle()
                    .NonLazy();
                Container.Bind<EndlessGrindLevelPackRewardsController>()
                    .FromMethod(
                        context => context.Container.Resolve<EndlessGrindLevelPackController>()
                            .gameObject.GetComponent<EndlessGrindLevelPackRewardsController>())
                    .AsSingle()
                    .NonLazy();

                Container.Bind<BotSystem>().FromComponentInNewPrefab(_botSystemPrefab).AsSingle().NonLazy();
                Container.BindInstance(_chipTagIconSettings).AsSingle();
            }
            #endif

            Container.Bind<GameControlsSystem>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
            Container.Bind<PopupQueueSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<RateGameSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<CustomerSupportSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<TransformLookupSystem>().FromComponentInNewPrefab(_transformLookupSystem).AsSingle();
            Container.Bind<LocationSystem>().FromComponentInNewPrefab(_locationSystemPrefab).AsSingle();
            Container.Bind<ISynchronizationFactory<Reward>>()
                .FromComponentInNewPrefab(_rewardSyncFactoryPrefab)
                .AsSingle();

            Container.Bind<MessageSystem>().FromComponentInNewPrefab(_messageSystemPrefab).AsSingle();
            foreach (ScriptableObject messageDataAsset in _messageDataAssets)
            {
                Container.Bind(messageDataAsset.GetType()).FromInstance(messageDataAsset);
            }

            Container.Bind<AnalyticsSystem>()
                .FromComponentInNewPrefab(_analyticsSystemPrefab)
                .AsSingle()
                .NonLazy();

            Container.Bind<SocialProfilesSystem>()
                .FromComponentInNewPrefab(_socialProfilesSystemPrefab)
                .AsSingle()
                .NonLazy();

            Container.Bind<InviteFriendsSystem>().FromInstance(_inviteFriendsSystem).AsSingle().NonLazy();

            Container.Bind<EquippedChipBoosters>().FromInstance(_equippedChipBoosters);

            Container.Bind<ICountedItemQuantityProvider>().FromInstance(this);

            Container.Bind<UnlockNotificationSystem>()
                .FromComponentInNewPrefab(_unlockNotificationSystemPrefab)
                .AsSingle()
                .NonLazy();

            Container.BindComponent<SaveGroup>(this);

            Container.Bind<ClothingSystem>()
                .FromComponentInNewPrefab(_clothingSystemPrefab)
                .AsSingle()
                .NonLazy();

            Container.Bind<UserSettings>().FromComponentInNewPrefab(_userSettingsPrefab).AsSingle();
            Container
                .Bind<InventoryLevelDrivenUnlockSystem>()
                .FromComponentInNewPrefab(_inventoryLevelDrivenUnlockPrefab)
                .AsSingle()
                .NonLazy();

            Container
                .Bind<KeyWordsReplacementSystem>()
                .FromComponentInNewPrefab(_keyWordsReplacementSystemPrefab)
                .AsSingle();

            Container.Bind<SocialProfileInventoryExchangeSystem>()
                .FromComponentInNewPrefab(_socialProfileExchangeSystemPrefab)
                .AsSingle()
                .NonLazy();

            Container.Bind<IReadOnlyCollection<CharacterData>>().FromInstance(_charactersDataCollection).AsSingle();

            Container.Bind<ProductPackInstanceContainer>()
                .FromComponentInNewPrefab(_productPackInstanceContainerPrefab)
                .AsSingle()
                .NonLazy();
            Container.Bind<PriceInstanceContainer>().FromNewComponentOn(gameObject).AsSingle().NonLazy();
            Container.Bind<IAPSystem>().FromNewComponentOn(gameObject).AsSingle().NonLazy();
            Container.Bind<IAPUserTrackingSystem>().FromComponentInHierarchy().AsSingle().NonLazy();

            Container.Bind<AdsSystem>().FromComponentInHierarchy().AsSingle().NonLazy();

            Container.Bind<HUDSystem>().FromInstance(_hudSystem).AsSingle();
            Container.Bind<IResourcesPanel>().FromInstance(_hudSystem).AsSingle();

            Container.Bind<PersistentCommandsExecutionSystem>()
                .FromComponentInNewPrefab(_persistentCommandsExecutionSystemPrefab)
                .AsSingle()
                .NonLazy();

            Container.Bind<BubblesPositionSystem>().FromComponentInNewPrefab(_bubblesPositionSystemPrefab).AsSingle();

            Container.Bind<InputLockSystem>()
                .FromNewComponentOn(gameObject)
                .AsSingle()
                .NonLazy();

            foreach (CountedItem item in _countedItems)
            {
                Container.Bind<CountedItem>().FromInstance(item);
                Container.QueueForInject(item);
            }
        }

        public ICountedItemQuantity GetQuantity(CountedItem item)
        {
            return new CountedItemQuantity(item);
        }

        #region Inner types

        private class CountedItemQuantity : ICountedItemQuantity
        {
            public int Count => Item.Count;
            public CountedItem Item { get; }

            public CountedItemQuantity(CountedItem item)
            {
                Item = item;
            }
        }

        #endregion
    }
}