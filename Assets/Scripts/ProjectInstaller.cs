﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.AssetBundles;
using DreamTeam.Audio;
using DreamTeam.Debug;
using DreamTeam.Localization;
using DreamTeam.Notifications;
using DreamTeam.Popups;
using DreamTeam.SceneLoading;
using DreamTeam.Screen;
using DreamTeam.SocialNetworking.Facebook;
using DreamTeam.Synchronization.Serializers;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

#if !NO_CLOUD
using DreamTeam.Cloud;
using DreamTeam.Cloud.DeviceId;
#endif

namespace DreamTeam
{
    public class ProjectInstaller : MonoInstaller
    {
        public const string SoundParameterId = "SoundParameter";
        public const string MusicParameterId = "MusicParameter";
        public const string BackgroundMusicPlayerId = "BackgroundMusicPlayer";

        #if !NO_CLOUD
        [SerializeField] private CloudSaveManager _cloudSaveManagerPrefab;
        [SerializeField] private DeviceIdManager _deviceIdManagerPrefab;
        #endif

        [SerializeField] private FacebookManager _facebookManagerPrefab;
        [SerializeField] private SaveManager _saveManagerPrefab;
        [SerializeField] private CheatPanel _cheatPanelPrefab;
        [SerializeField] private LogMessageContainer _logMessageContainer;
        [SerializeField] private LocalizationSystem _localizationSystem;
        [SerializeField] private NotificationsSystem _notificationsSystemPrefab;
        [SerializeField] private LoadingSystem _loadingSystemPrefab;
        [SerializeField] private AssetBundleManager _assetBundleManagerPrefab;
        [SerializeField] private PopupSystem _popupSystemPrefab;
        [SerializeField] private GameConstantsAsset _gameConstantsAsset;
        [SerializeField, RequiredField] private ScreenController _screenController;

        [Header("Audio:")]
        [SerializeField, RequiredField] private AudioSystem _audioSystemPrefab;
        [SerializeField, RequiredField] private AudioMixerExposedParameter _soundParameter;
        [SerializeField, RequiredField] private AudioMixerExposedParameter _musicParameter;
        [SerializeField, RequiredField] private AudioSourceProviderController _backgroundMusicPlayerPrefab;

        public override void InstallBindings()
        {
            Container.Bind<Instantiator>().AsSingle().CopyIntoAllSubContainers();

            #if !NO_CLOUD
            {
                Container.Bind<CloudSaveManager>()
                    .FromComponentInNewPrefab(_cloudSaveManagerPrefab)
                    .AsSingle()
                    .NonLazy();
                Container.Bind<DeviceIdManager>().FromComponentInNewPrefab(_deviceIdManagerPrefab).AsSingle();
            }
            #endif

            Container.Bind<JsonNodeSerializer>().AsSingle();
            Container.Bind<INodeSerializer>().FromMethod(context => context.Container.Resolve<JsonNodeSerializer>());

            Container.Bind<LogMessageContainer>().FromComponentInNewPrefab(_logMessageContainer).AsSingle().NonLazy();
            Container.Bind<FacebookManager>().FromComponentInNewPrefab(_facebookManagerPrefab).AsSingle();
            Container.Bind<SaveManager>().FromComponentInNewPrefab(_saveManagerPrefab).AsSingle().NonLazy();
            Container.Bind<CheatPanel>().FromComponentInNewPrefab(_cheatPanelPrefab).AsSingle().NonLazy();
            Container.Bind<LocalizationSystem>().FromComponentInNewPrefab(_localizationSystem).AsSingle().NonLazy();
            Container.Bind<NotificationsSystem>()
                .FromComponentInNewPrefab(_notificationsSystemPrefab)
                .AsSingle()
                .NonLazy();
            Container.Bind<DebugStatesSystem>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
            Container.Bind<ScreenOrientationNotifier>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<LoadingSystem>().FromComponentInNewPrefab(_loadingSystemPrefab).AsSingle().NonLazy();
            Container.Bind<AssetBundleManager>()
                .FromComponentInNewPrefab(_assetBundleManagerPrefab)
                .AsSingle()
                .NonLazy();
            Container.Bind<PopupSystem>().FromComponentInNewPrefab(_popupSystemPrefab).AsSingle().NonLazy();
            Container.Bind<GameConstantsAsset>().FromInstance(_gameConstantsAsset).AsSingle().NonLazy();
            Container.Bind<ScreenController>().FromInstance(_screenController).AsSingle().NonLazy();

            Container.Bind<AudioSystem>().FromComponentInNewPrefab(_audioSystemPrefab).AsSingle().NonLazy();
            Container.Bind<AudioMixerExposedParameter>().WithId(SoundParameterId).FromInstance(_soundParameter);
            Container.Bind<AudioMixerExposedParameter>().WithId(MusicParameterId).FromInstance(_musicParameter);
            Container.Bind<AudioSourceProviderController>()
                .WithId(BackgroundMusicPlayerId)
                .FromComponentInNewPrefab(_backgroundMusicPlayerPrefab)
                .AsSingle()
                .NonLazy();
        }
    }
}