﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Synchronization.States;

namespace DreamTeam.Synchronization
{
    public interface ISynchronizable
    {
        void Sync(State state);
    }
}