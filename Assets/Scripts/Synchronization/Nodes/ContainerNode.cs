﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine.Assertions;

namespace DreamTeam.Synchronization.Nodes
{
    public abstract class ContainerNode : Node
    {
        private List<Node> _nodes = new List<Node>();

        public int Count => _nodes.Count;

        protected ContainerNode()
        {
        }

        protected ContainerNode(string id) : base(id)
        {
        }

        public IEnumerator<Node> GetEnumerator()
        {
            return _nodes.GetEnumerator();
        }

        protected T GetNode<T>(int index) where T : Node
        {
            if (!Contains(index))
            {
                throw new IndexOutOfRangeException(GetErrorMessage(this, $"Index {index} is out of range."));
            }

            Node node = _nodes[index];
            if (!(node is T))
            {
                throw new InvalidOperationException(
                    GetErrorMessage(this, $"Invalid node type at {index}. The requiested type is '{typeof(T).Name}' but the node is '{node.GetType().Name}'."));
            }

            return (T)node;
        }

        protected bool Contains(int index)
        {
            return index >= 0 && index < _nodes.Count;
        }

        protected void AddNode([NotNull] Node node)
        {
            CheckNewNode(node);

            _nodes.Add(node);
            OnNodeAttached(node, _nodes.Count - 1);
        }

        protected void SetNode(int index, [NotNull] Node node)
        {
            if (!Contains(index))
            {
                throw new IndexOutOfRangeException(GetErrorMessage(this, $"Index {index} is out of range."));
            }

            CheckNewNode(node);

            OnNodeDetaching(_nodes[index]);
            _nodes[index] = node;
            OnNodeAttached(node, index);
        }

        protected virtual void OnNodeAttached([NotNull] Node node, int index)
        {
            SetParent(node, this);
        }

        protected virtual void OnNodeDetaching([NotNull] Node node)
        {
            SetParent(node, null);
        }

        private void CheckNewNode(Node node)
        {
            if (node == null)
            {
                throw new NullReferenceException(GetErrorMessage(this, "Trying to add a null node."));
            }

            string id = node.Id;

            Assert.IsTrue(_nodes.TrueForAll(n => n.Id != id), $"Node identificator {id} is not unique.");
            Assert.IsNull(node.Parent, $"Node {id} already has a parent.");
        }
    }
}