﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using JetBrains.Annotations;

namespace DreamTeam.Synchronization.Nodes
{
    public abstract class Node
    {
        private string _id;

        public string Id
        {
            get { return _id; }
            private set
            {
                if (_id != value)
                {
                    _id = value;
                    OnIdChanged();
                }
            }
        }

        public ContainerNode Parent { get; private set; }

        protected Node()
        {
        }

        protected Node(string id)
        {
            Id = id;
        }

        protected virtual void OnIdChanged()
        {
        }

        protected static void SetId([NotNull] Node node, string id)
        {
            node.Id = id;
        }

        protected static void SetParent([NotNull] Node node, ContainerNode parent)
        {
            node.Parent = parent;
        }

        protected static string GetErrorMessage(Node context, string message)
        {
            return $"[Node '{context.Id}'] {message}'.";
        }
    }
}