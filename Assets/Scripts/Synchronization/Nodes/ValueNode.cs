﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using JetBrains.Annotations;

namespace DreamTeam.Synchronization.Nodes
{
    public class ValueNode : Node
    {
        private object _value;

        public object Value
        {
            get { return _value;}

            [NotNull] set
            {
                CheckNull(value);
                CheckType(value.GetType(), this);
                _value = value;
            }
        }

        public ValueNode([NotNull] object value)
        {
            Value = value;
        }

        public bool GetBool()
        {
            return GetValue<bool>();
        }

        public bool GetBool(bool defaultValue)
        {
            return GetValue(defaultValue);
        }

        public void SetBool(bool value)
        {
            Value = value;
        }

        public int GetInt()
        {
            return GetValue<int>();
        }

        public int GetInt(int defaultValue)
        {
            return GetValue(defaultValue);
        }

        public void SetInt(int value)
        {
            Value = value;
        }

        public float GetFloat()
        {
            return GetValue<float>();
        }

        public float GetFloat(float defaultValue)
        {
            return GetValue(defaultValue);
        }

        public void SetFloat(float value)
        {
            Value = value;
        }

        public string GetString()
        {
            return GetValue<string>();
        }

        public string GetString(string defaultValue)
        {
            return GetValue(defaultValue);
        }

        public void SetString([NotNull] string value)
        {
            Value = value;
        }

        public T GetValue<T>()
        {
            CheckType<T>(this);

            if (Value is T)
            {
                return (T)Value;
            }

            throw new InvalidOperationException(
                    GetErrorMessage(this, $"Type mismatch: the requested type is '{typeof(T).Name}' but the value type is '{Value.GetType().Name}'.")
                );
        }

        public T GetValue<T>(T defaultValue)
        {
            try
            {
                return GetValue<T>();
            }
            catch
            {
                return defaultValue;
            }
        }

        public static void CheckType<T>(Node context)
        {
            CheckType(typeof(T), context);
        }

        private static void CheckType(Type t, Node context)
        {
            if (t != typeof(bool) &&
                t != typeof(int) &&
                t != typeof(float) &&
                t != typeof(string))
            {
                throw new InvalidOperationException(GetErrorMessage(context, $"Type '{t.Name}' is not supported."));
            }
        }

        private void CheckNull([NotNull] object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value), GetErrorMessage(this, "Null value is not supported."));
            }
        }
    }
}