﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using JetBrains.Annotations;

namespace DreamTeam.Synchronization.Nodes
{
    public class ListNode : ContainerNode
    {
        public new bool Contains(int index)
        {
            return base.Contains(index);
        }

        public bool GetBool(int index)
        {
            return GetValue<bool>(index);
        }

        public void SetBool(int index, bool value)
        {
            SetValue(index, value);
        }

        public int GetInt(int index)
        {
            return GetValue<int>(index);
        }

        public void SetInt(int index, int value)
        {
            SetValue(index, value);
        }

        public float GetFloat(int index)
        {
            return GetValue<float>(index);
        }

        public void SetFloat(int index, float value)
        {
            SetValue(index, value);
        }

        public string GetString(int index)
        {
            return GetValue<string>(index);
        }

        public void SetString(int index, string value)
        {
            SetValue(index, value);
        }

        public bool IsNull(int index)
        {
            Node node = GetNode<Node>(index);
            return node is NullNode;
        }

        public void SetNull(int index)
        {
            Node node = GetNode<Node>(index);
            if (!(node is NullNode))
            {
                SetNewNode<NullNode>(index);
            }
        }

        public DictionaryNode GetDictionary(int index)
        {
            return GetNode<DictionaryNode>(index);
        }

        public DictionaryNode SetDictionary(int index)
        {
            return SetNewNode<DictionaryNode>(index);
        }

        public ListNode GetList(int index)
        {
            return GetNode<ListNode>(index);
        }

        public ListNode SetList(int index)
        {
            return SetNewNode<ListNode>(index);
        }

        public ListNode AddList()
        {
            return AddNewNode<ListNode>();
        }

        public DictionaryNode AddDictionary()
        {
            return AddNewNode<DictionaryNode>();
        }

        public void AddBool(bool value)
        {
            AddValue(value);
        }

        public void AddInt(int value)
        {
            AddValue(value);
        }

        public void AddFloat(float value)
        {
            AddValue(value);
        }

        public void AddString(string value)
        {
            AddValue(value);
        }

        public void AddValue<T>(T value)
        {
            AddNode(new ValueNode(value));
        }

        public void AddNull()
        {
            AddNode(new NullNode());
        }

        public T GetValue<T>(int index)
        {
            ValueNode.CheckType<T>(this);
            return GetNode<ValueNode>(index).GetValue<T>();
        }

        public void SetValue<T>(int index, T value)
        {
            ValueNode.CheckType<T>(this);

            ValueNode node = GetNode<Node>(index) as ValueNode;
            if (node != null)
            {
                node.Value = value;
            }
            else
            {
                SetNode(index, new ValueNode(value));
            }
        }

        public Node GetNode(int index)
        {
            return GetNode<Node>(index);
        }

        public new void AddNode([NotNull] Node node)
        {
            base.AddNode(node);
        }

        public new void SetNode(int index, [NotNull] Node node)
        {
            base.SetNode(index, node);
        }

        public new T GetNode<T>(int index) where T : Node
        {
            return base.GetNode<T>(index);
        }

        protected override void OnNodeAttached(Node node, int index)
        {
            base.OnNodeAttached(node, index);

            SetId(node, GetNodeIdByIndex(index));
        }

        protected override void OnNodeDetaching(Node node)
        {
            base.OnNodeDetaching(node);

            SetId(node, null);
        }

        protected override void OnIdChanged()
        {
            base.OnIdChanged();

            for (int i = 0; i < Count; i++)
            {
                SetId(GetNode(i), GetNodeIdByIndex(i));
            }
        }

        private string GetNodeIdByIndex(int index)
        {
            return $"{Id}[{index}]";
        }

        private T SetNewNode<T>(int index) where T : Node, new()
        {
            T node = new T();
            SetNode(index, node);
            return node;
        }

        private T AddNewNode<T>() where T : Node, new()
        {
            T node = new T();
            AddNode(node);
            return node;
        }
    }
}