﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace DreamTeam.Synchronization.Nodes
{
    public class DictionaryNode : ContainerNode
    {
        public DictionaryNode()
        {
        }

        public DictionaryNode(string id) : base(id)
        {
        }

        public bool GetBool([NotNull] string id)
        {
            return GetValue<bool>(id);
        }

        public bool GetBool([NotNull] string id, bool defaultValue)
        {
            return GetValue(id, defaultValue);
        }

        public void SetBool([NotNull] string id, bool value)
        {
            SetValue(id, value);
        }

        public int GetInt([NotNull] string id)
        {
            return GetValue<int>(id);
        }

        public int GetInt([NotNull] string id, int defaultValue)
        {
            return GetValue(id, defaultValue);
        }

        public void SetInt([NotNull] string id, int value)
        {
            SetValue(id, value);
        }

        public float GetFloat([NotNull] string id)
        {
            return GetValue<float>(id);
        }

        public float GetFloat([NotNull] string id, float defaultValue)
        {
            return GetValue(id, defaultValue);
        }

        public void SetFloat([NotNull] string id, float value)
        {
            SetValue(id, value);
        }

        public string GetString([NotNull] string id)
        {
            return GetValue<string>(id);
        }

        public string GetString([NotNull] string id, string defaultValue)
        {
            return GetValue(id, defaultValue);
        }

        public void SetString([NotNull] string id, string value)
        {
            SetValue(id, value);
        }

        public T GetValue<T>([NotNull] string id)
        {
            ValueNode.CheckType<T>(this);
            return GetNode<ValueNode>(id).GetValue<T>();
        }

        public T GetValue<T>([NotNull] string id, T defaultValue)
        {
            ValueNode.CheckType<T>(this);
            ValueNode node = FindNode<ValueNode>(id);
            if (node != null)
            {
                return node.GetValue<T>();
            }

            return defaultValue;
        }

        public void SetValue<T>([NotNull] string id, T value)
        {
            ValueNode.CheckType<T>(this);

            int idx = GetNodeIndex(id);
            if (idx >= 0)
            {
                var node = GetNode<Node>(idx) as ValueNode;
                if (node != null)
                {
                    node.Value = value;
                    return;
                }
            }

            SetNode(id, new ValueNode(value));
        }

        public bool IsNull([NotNull] string id)
        {
            Node node = GetNode<Node>(id);
            return node is NullNode;
        }

        public void SetNull([NotNull] string id)
        {
            int idx = GetNodeIndex(id);
            if (idx >= 0)
            {
                Node node = GetNode<Node>(idx);
                if (node is NullNode)
                {
                    return;
                }
            }

            SetNewNode<NullNode>(id);
        }

        public DictionaryNode GetDictionary([NotNull] string id)
        {
            return GetNode<DictionaryNode>(id);
        }

        public DictionaryNode SetDictionary([NotNull] string id)
        {
            return SetNewNode<DictionaryNode>(id);
        }

        public ListNode GetList([NotNull] string id)
        {
            return GetNode<ListNode>(id);
        }

        public ListNode SetList([NotNull] string id)
        {
            return SetNewNode<ListNode>(id);
        }

        public bool Contains([NotNull] string id)
        {
            return FindNode<Node>(id) != null;
        }

        public Node GetNode([NotNull] string id)
        {
            return GetNode<Node>(id);
        }

        public void SetNode([NotNull] string id, [NotNull] Node node)
        {
            int idx = GetNodeIndex(id);

            if (idx >= 0)
            {
                SetNode(idx, node);
            }
            else
            {
                AddNode(node);
            }

            SetId(node, id);
        }

        public T GetNode<T>([NotNull] string id) where T : Node
        {
            T node = FindNode<T>(id);
            if (node == null)
            {
                throw new KeyNotFoundException(GetErrorMessage(this, $"The value with id '{id}' doesn't exist."));
            }

            return node;
        }

        public T FindNode<T>([NotNull] string id) where T : Node
        {
            int idx = GetNodeIndex(id);
            if (idx >= 0)
            {
                return GetNode<T>(idx);
            }

            return null;
        }

        protected override void OnNodeDetaching(Node node)
        {
            base.OnNodeDetaching(node);

            SetId(node, null);
        }

        private int GetNodeIndex(string id)
        {
            if (id == null)
            {
                throw new NullReferenceException($"[ Node {Id} ] Null identificator is not allowed.");
            }

            for (int i = 0; i < Count; i++)
            {
                var node = GetNode<Node>(i);
                if (node.Id == id)
                {
                    return i;
                }
            }

            return -1;
        }

        private T SetNewNode<T>([NotNull] string id) where T : Node, new()
        {
            T node = new T();
            SetNode(id, node);
            return node;
        }
    }
}