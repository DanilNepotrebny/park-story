﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Synchronization.Nodes;
using DreamTeam.Synchronization.States;

namespace DreamTeam.Synchronization
{
    public static class SyncronizationExtensions
    {
        public static void SyncDateTime(this State state, string id, ref DateTime dateTime)
        {
            string valueToSync = dateTime.ToString();
            state.SyncString(id, ref valueToSync);
            dateTime = DateTime.Parse(valueToSync);
        }

        public static void SyncEnum<T>(this State state, string id, ref T value, T defaultValue = default(T))
            where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException($"{typeof(T)} must be enum");
            }

            int syncValue = (int)Convert.ChangeType(value, typeof(int));
            int syncDefaultValue = (int)Convert.ChangeType(defaultValue, typeof(int));
            state.SyncInt(id, ref syncValue, syncDefaultValue);
            value = (T)Enum.ToObject(typeof(T), syncValue);
        }

        public static void SetDateTime(this DictionaryNode node, string id, DateTime dateTime)
        {
            string stringValue = dateTime.ToString();
            node.SetString(id, stringValue);
        }

        public static DateTime GetDateTime(this DictionaryNode node, string id)
        {
            string stringValue = node.GetString(id);
            return DateTime.Parse(stringValue);
        }
    }
}