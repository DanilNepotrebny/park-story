﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.IO;
using DreamTeam.Synchronization.Nodes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DreamTeam.Synchronization.Serializers
{
    public class JsonNodeSerializer : INodeSerializer
    {
        public void Serialize(Stream stream, Node node)
        {
            JToken t = CreateToken(node);
            using (StreamWriter streamWriter = new StreamWriter(stream))
            {
                using (JsonTextWriter jsonWriter = new JsonTextWriter(streamWriter))
                {
                    jsonWriter.Formatting = Formatting.Indented;
                    t.WriteTo(jsonWriter);
                }
            }
        }

        public Node Deserialize(Stream stream)
        {
            JToken t;

            using (StreamReader streamReader = new StreamReader(stream))
            {
                using (JsonTextReader jsonReader = new JsonTextReader(streamReader))
                {
                    t = JToken.ReadFrom(jsonReader);
                }
            }

            return CreateNode(t);
        }

        public JToken CreateToken(Node node)
        {
            ValueNode value = node as ValueNode;
            if (value != null)
            {
                return new JValue(value.Value);
            }

            if (node is NullNode)
            {
                return JValue.CreateNull();
            }

            DictionaryNode dictionary = node as DictionaryNode;
            if (dictionary != null)
            {
                JObject obj = new JObject();
                foreach (Node n in dictionary)
                {
                    obj[n.Id] = CreateToken(n);
                }

                return obj;
            }

            ListNode list = node as ListNode;
            if (list != null)
            {
                JArray obj = new JArray();
                foreach (Node n in list)
                {
                    obj.Add(CreateToken(n));
                }

                return obj;
            }

            throw new InvalidOperationException("Invalid node type.");
        }

        private Node CreateNode(JToken token)
        {
            JObject jobject = token as JObject;
            if (jobject != null)
            {
                DictionaryNode dictionary = new DictionaryNode();
                foreach (KeyValuePair<string, JToken> pair in jobject)
                {
                    AddTokenToDictionary(dictionary, pair.Key, pair.Value);
                }

                return dictionary;
            }

            JArray jarray = token as JArray;
            if (jarray != null)
            {
                ListNode list = new ListNode();
                foreach (JToken t in jarray)
                {
                    AddTokenToList(list, t);
                }

                return list;
            }

            JValue value = token as JValue;
            if (value != null)
            {
                switch (value.Type)
                {
                    case JTokenType.Object:
                        return CreateNode((JObject)value.Value);

                    case JTokenType.Array:
                        return CreateNode((JArray)value.Value);

                    case JTokenType.Integer:
                    case JTokenType.Float:
                    case JTokenType.String:
                    case JTokenType.Boolean:
                        return new ValueNode(value.Value);

                    case JTokenType.Null:
                        return new NullNode();
                }
            }

            return null;
        }

        private void AddTokenToList(ListNode parent, JToken token)
        {
            JValue value = token as JValue;
            if (value != null)
            {
                switch (value.Type)
                {
                    case JTokenType.Object:
                        AddTokenToList(parent, (JToken)value.Value);
                        return;

                    case JTokenType.Array:
                        AddTokenToList(parent, (JToken)value.Value);
                        break;

                    case JTokenType.Integer:
                        parent.AddInt((int)value.Value);
                        break;

                    case JTokenType.Float:
                        parent.AddFloat((float)value.Value);
                        break;

                    case JTokenType.String:
                        parent.AddString((string)value.Value);
                        break;

                    case JTokenType.Boolean:
                        parent.AddBool((bool)value.Value);
                        break;

                    case JTokenType.Null:
                        parent.AddNull();
                        break;
                }
            }

            JObject jobject = token as JObject;
            if (jobject != null)
            {
                DictionaryNode dictionary = parent.AddDictionary();
                foreach (KeyValuePair<string, JToken> pair in jobject)
                {
                    AddTokenToDictionary(dictionary, pair.Key, pair.Value);
                }

                return;
            }

            JArray jarray = token as JArray;
            if (jarray != null)
            {
                ListNode list = new ListNode();
                foreach (JToken t in jarray)
                {
                    AddTokenToList(list, t);
                }
            }
        }

        private void AddTokenToDictionary(DictionaryNode parent, string id, JToken token)
        {
            JValue value = token as JValue;
            if (value != null)
            {
                switch (value.Type)
                {
                    case JTokenType.Object:
                        AddTokenToDictionary(parent, id, (JToken)value.Value);
                        return;

                    case JTokenType.Array:
                        AddTokenToDictionary(parent, id, (JToken)value.Value);
                        break;

                    case JTokenType.Integer:
                        parent.SetInt(id, (int)value);
                        break;

                    case JTokenType.Float:
                        parent.SetFloat(id, (float)value);
                        break;

                    case JTokenType.String:
                        parent.SetString(id, (string)value);
                        break;

                    case JTokenType.Boolean:
                        parent.SetBool(id, (bool)value);
                        break;

                    case JTokenType.Null:
                        parent.SetNull(id);
                        break;
                }
            }

            JObject jobject = token as JObject;
            if (jobject != null)
            {
                DictionaryNode dictionary = parent.SetDictionary(id);
                foreach (KeyValuePair<string, JToken> pair in jobject)
                {
                    AddTokenToDictionary(dictionary, pair.Key, pair.Value);
                }

                return;
            }

            JArray jarray = token as JArray;
            if (jarray != null)
            {
                ListNode list = parent.SetList(id);
                foreach (JToken t in jarray)
                {
                    AddTokenToList(list, t);
                }
            }
        }
    }
}