﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.IO;
using DreamTeam.Synchronization.Nodes;

namespace DreamTeam.Synchronization.Serializers
{
    public interface INodeSerializer
    {
        void Serialize(Stream stream, Node node);

        Node Deserialize(Stream stream);
    }
}