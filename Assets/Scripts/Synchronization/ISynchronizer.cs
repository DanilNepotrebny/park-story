﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Synchronization.Nodes;

namespace DreamTeam.Synchronization
{
    public interface ISynchronizer<T>
    {
        Node Serialize(T obj);

        T Deserialize(Node node);
    }
}