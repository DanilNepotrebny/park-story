﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Synchronization.Nodes;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Synchronization.Factories
{
    public abstract class ObjectSynchronizationFactory<TRootType, TRootTypeReference, TKeyTypeList> :
        MonoBehaviour,
        ISynchronizationFactory<TRootType>
        where TKeyTypeList : KeyValueList<string, TRootTypeReference>
        where TRootTypeReference : TypeReference<TRootType>
        where TRootType : class, ISynchronizable
    {
        [SerializeField] private TRootTypeReference _defaultType;
        [SerializeField] private TKeyTypeList _keyTypesMap;

        [Inject] private Instantiator _instantiator;

        public TRootType Deserialize(Node factoryNode)
        {
            string key = ((ValueNode)factoryNode).GetString();
            TRootTypeReference typeReference = _keyTypesMap[key] ?? _defaultType;
            return Instantiate(_instantiator, typeReference.Type);
        }

        public Node Serialize(TRootType obj)
        {
            string key = _keyTypesMap.Find(pair => pair.Value.Type.Equals(obj.GetType())).Key;
            return string.IsNullOrEmpty(key) ? new NullNode() : new ValueNode(key) as Node;
        }

        protected abstract TRootType Instantiate(Instantiator instantiator, Type type);
    }
}