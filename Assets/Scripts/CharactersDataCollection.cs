﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam
{
    [CreateAssetMenu(menuName = "Characters/Characters data collection")]
    public class CharactersDataCollection : ScriptableObject,
        IReadOnlyCollection<CharacterData>
    {
        [SerializeField] private List<CharacterData> _characters = new List<CharacterData>();

        public int Count => _characters.Count;

        public void ClearDeletedAssets()
        {
            _characters.RemoveAll(item => item == null);
        }

        public void Add(CharacterData character)
        {
            if (!_characters.Contains(character))
            {
                _characters.Add(character);
            }
        }

        public IEnumerator<CharacterData> GetEnumerator()
        {
            return _characters.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_characters).GetEnumerator();
        }
    }
}