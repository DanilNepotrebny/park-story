﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Clothing
{
    public abstract class BaseClothingModelPart<TSlot> : BaseClothingModelPart where TSlot : BaseClothingModelSlot
    {
        [SerializeField] private TSlot _slot;

        public override BaseClothingModelSlot Slot => _slot;
    }

    /// <summary>
    /// Base class for character 3d-model parts which could be changed via <see cref="ClothingItem"/>.
    /// Can be attached to specific <see cref="BaseClothingModelSlot"/>.
    /// </summary>
    public abstract class BaseClothingModelPart : ScriptableObject
    {
        public abstract BaseClothingModelSlot Slot { get; }
    }
}