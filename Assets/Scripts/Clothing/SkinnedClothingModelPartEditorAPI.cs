﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

#if UNITY_EDITOR

namespace DreamTeam.Clothing
{
    public partial class SkinnedClothingModelPart
    {
        public static bool IsAssetCompatible(GameObject asset)
        {
            return asset.GetComponentInChildren<SkinnedMeshRenderer>() != null;
        }

        public static Object CreateAsset(GameObject source)
        {
            var meshRenderer = source.GetComponentInChildren<SkinnedMeshRenderer>();
            var item = CreateInstance<SkinnedClothingModelPart>();
            item._skinnedMeshRenderer = meshRenderer;
            item._materials = meshRenderer.sharedMaterials;
            return item;
        }
    }
}

#endif