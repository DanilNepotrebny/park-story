﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Linq;
using UnityEngine;
using Zenject;

namespace DreamTeam.Clothing
{
    /// <summary>
    /// Base class for component that updates <typeparamref name="TModelPart"/> attached to
    /// <typeparamref name="TModelSlot"/> when current <see cref="ClothingItem"/> has changed.
    /// Add this component to character presenter on location to update its meshes accordingly to current
    /// clothing.
    /// </summary>
    public abstract class BaseClothingModelSlotController<TModelSlot, TModelPart> : MonoBehaviour
        where TModelSlot : BaseClothingModelSlot
        where TModelPart : BaseClothingModelPart
    {
        [SerializeField] private TModelSlot _slot;
        [SerializeField] private Transform _viewRoot;

        [Inject] private ClothingSystem _clothingSystem;

        private TModelPart _part;

        protected Transform ViewRoot => _viewRoot;

        protected abstract void ApplyPart(TModelPart part);

        protected void Start()
        {
            _clothingSystem.ItemChanged += OnItemChanged;

            ClothingSlot slot = _clothingSystem.Slots.FirstOrDefault(IsCompatibleSlot);
            if (slot != null)
            {
                SetClothingItem(_clothingSystem.GetItem(slot));
            }
        }

        protected void OnDestroy()
        {
            _clothingSystem.ItemChanged -= OnItemChanged;
        }

        protected virtual Transform FindParent(TModelPart part)
        {
            return ViewRoot;
        }

        private void OnItemChanged(ClothingSlot slot, ClothingItem newItem, ClothingItem oldItem)
        {
            if (IsCompatibleSlot(slot))
            {
                SetClothingItem(newItem);
            }
        }

        private bool IsCompatibleSlot(ClothingSlot clothingSlot)
        {
            return clothingSlot.ClothingModelSlots.Contains(_slot);
        }

        private void SetClothingItem(ClothingItem clothingItem)
        {
            BaseClothingModelPart part = clothingItem?.ClothingModelParts.FirstOrDefault(i => i != null && i.Slot == _slot);
            SetPart(part);
        }

        private void SetPart(BaseClothingModelPart part)
        {
            if (part != null && _slot != part.Slot)
            {
                throw new ArgumentException($"Can't put item {part.name} into slot {_slot.name}.");
            }

            if (_part == part)
            {
                return;
            }

            _part = (TModelPart)part;

            if (_part != null)
            {
                ApplyPart(_part);
            }

            gameObject.SetActive(_part != null);

            transform.SetParent(FindParent(_part), false);
        }
    }
}