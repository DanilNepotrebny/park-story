﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Clothing
{
    [RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
    public class
        MeshClothingModelSlotController : BaseClothingModelSlotController<MeshClothingModelSlot, MeshClothingModelPart>
    {
        protected override void ApplyPart(MeshClothingModelPart part)
        {
            var meshRenderer = GetComponent<MeshRenderer>();
            meshRenderer.sharedMaterials = part.Materials;

            var meshFilter = GetComponent<MeshFilter>();
            meshFilter.sharedMesh = part.Mesh;

            Transform targetTransform = part.Transform;
            transform.localPosition = targetTransform.localPosition;
            transform.localRotation = targetTransform.localRotation;
            transform.localScale = targetTransform.localScale;
        }

        protected override Transform FindParent(MeshClothingModelPart part)
        {
            string bonePath = part?.BonePath;
            Transform root = bonePath != null ? ViewRoot.FindInChildren(bonePath) : null;
            return root ?? ViewRoot;
        }
    }
}