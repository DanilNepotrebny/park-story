﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

#if UNITY_EDITOR

namespace DreamTeam.Clothing
{
    public partial class MeshClothingModelPart
    {
        public static bool IsAssetCompatible(GameObject asset)
        {
            return asset.GetComponentInChildren<MeshRenderer>() != null;
        }

        public static Object CreateAsset(GameObject source)
        {
            var meshRenderer = source.GetComponentInChildren<MeshRenderer>();
            var item = CreateInstance<MeshClothingModelPart>();
            item._meshRenderer = meshRenderer;
            item._materials = meshRenderer.sharedMaterials;
            return item;
        }
    }
}

#endif