﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Clothing
{
    /// <summary> 
    /// Clothing model part which consists of a skinned mesh and all the bones needed for skinning.
    /// </summary>
    public partial class SkinnedClothingModelPart : BaseClothingModelPart<SkinnedClothingModelSlot>
    {
        [SerializeField] private SkinnedMeshRenderer _skinnedMeshRenderer;
        [SerializeField] private Material[] _materials;

        public IEnumerable<string> BonePathes => _skinnedMeshRenderer.bones.Select(t => t.GetFullPath(t.root, 1));

        public Material[] Materials => _materials;

        public Mesh Mesh => _skinnedMeshRenderer.sharedMesh;
    }
}