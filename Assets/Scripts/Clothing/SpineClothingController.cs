﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using Spine;
using Spine.Unity;
using Spine.Unity.Modules;
using Spine.Unity.Modules.AttachmentTools;
using UnityEngine;
using Zenject;

namespace DreamTeam.Clothing
{
    using AttachmentPair = KeyValuePair<Skin.AttachmentKeyTuple, Attachment>;

    /// <summary>
    /// Component which changes clothing skin of sibling spine <see cref="ISkeletonComponent"/> accordingly
    /// to currently selected <see cref="ClothingItem"/>.
    /// </summary>
    [RequireComponent(typeof(ISkeletonComponent))]
    public class SpineClothingController : MonoBehaviour
    {
        [Inject] private ClothingSystem _clothingSystem;

        private ISkeletonComponent _skeletonComponent;
        private SkeletonDataAsset _currentSkeletonDataAsset;

        protected void OnEnable()
        {
            _skeletonComponent = GetComponent<ISkeletonComponent>();

            _clothingSystem.ItemChanged += OnItemChanged;

            UpdateSkeletonDataAsset();
        }

        protected void OnDisable()
        {
            _currentSkeletonDataAsset = null;
        }

        protected void Update()
        {
            UpdateSkeletonDataAsset();
        }

        private void UpdateSkeletonDataAsset()
        {
            SkeletonDataAsset asset = _skeletonComponent.SkeletonDataAsset;
            if (asset == _currentSkeletonDataAsset)
            {
                return;
            }

            if (_skeletonComponent.Skeleton == null)
            {
                return;
            }

            _currentSkeletonDataAsset = asset;

            foreach (ClothingSlot slot in _clothingSystem.Slots)
            {
                ClothingItem item = _clothingSystem.GetItem(slot);
                ChangeItem(item, null);
            }

            UpdateSkin();
        }

        private void OnItemChanged(ClothingSlot slot, ClothingItem newItem, ClothingItem oldItem)
        {
            if (!IsItemCompatible(newItem) && !IsItemCompatible(oldItem))
            {
                return;
            }

            ChangeItem(newItem, oldItem);
            UpdateSkin();
        }

        private void ChangeItem(ClothingItem newItem, ClothingItem oldItem)
        {
            if (oldItem == newItem)
            {
                return;
            }

            Skeleton skeleton = _skeletonComponent.Skeleton;
            SkeletonData skeletonData = skeleton.Data;

            Skin currentSkin = skeleton.Skin;
            if (currentSkin == null)
            {
                currentSkin = skeletonData.DefaultSkin.GetClone();
                skeleton.Skin = currentSkin;
            }

            if (IsItemCompatible(oldItem))
            {
                foreach (string skinName in oldItem.SpineSkinNames)
                {
                    Skin skin = skeletonData.FindSkin(skinName);
                    foreach (AttachmentPair pair in skin.Attachments)
                    {
                        currentSkin.Attachments.Remove(pair.Key);
                    }
                }
            }

            if (IsItemCompatible(newItem))
            {
                foreach (string skinName in newItem.SpineSkinNames)
                {
                    Skin skin = skeletonData.FindSkin(skinName);
                    foreach (AttachmentPair pair in skin.Attachments)
                    {
                        currentSkin.Attachments[pair.Key] = pair.Value;
                    }
                }
            }
        }

        private bool IsItemCompatible(ClothingItem item)
        {
            return item != null && item.Slot.SkeletonDataAsset == _currentSkeletonDataAsset;
        }

        private void UpdateSkin()
        {
            Skeleton skeleton = _skeletonComponent.Skeleton;
            skeleton.SetSkin(skeleton.Skin);
            skeleton.SetSlotsToSetupPose();

            (_skeletonComponent as IAnimationStateComponent).AnimationState.Apply(skeleton);

            // In case SkeletonGraphic is freezed we have to update mesh manually.
            (_skeletonComponent as SkeletonGraphicMultiObject)?.UpdateMesh();
            (_skeletonComponent as SkeletonGraphic)?.UpdateMesh();
        }
    }
}