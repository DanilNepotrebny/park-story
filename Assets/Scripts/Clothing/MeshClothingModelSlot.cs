﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Clothing
{
    [CreateAssetMenu(fileName = "ClothingModelSlot1", menuName = "Clothing/Model slots/Mesh")]
    public class MeshClothingModelSlot : BaseClothingModelSlot
    {
    }
}