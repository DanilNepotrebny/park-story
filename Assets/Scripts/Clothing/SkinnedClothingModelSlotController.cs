﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Clothing
{
    [RequireComponent(typeof(SkinnedMeshRenderer))]
    public class
        SkinnedClothingModelSlotController : BaseClothingModelSlotController<SkinnedClothingModelSlot,
            SkinnedClothingModelPart>
    {
        protected override void ApplyPart(SkinnedClothingModelPart part)
        {
            var meshRenderer = GetComponent<SkinnedMeshRenderer>();
            meshRenderer.sharedMesh = part.Mesh;
            meshRenderer.sharedMaterials = part.Materials;

            var bones = new List<Transform>();
            foreach (string bonePath in part.BonePathes)
            {
                Transform resultBone = ViewRoot.FindInChildren(bonePath);
                if (resultBone == null)
                {
                    UnityEngine.Debug.LogError($"Can't find bone {bonePath}");
                }

                bones.Add(resultBone);
            }

            meshRenderer.bones = bones.ToArray();
        }
    }
}