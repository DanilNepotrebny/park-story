﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Clothing
{
    /// <summary>
    /// Representation of customizable clothing item. Contains all the data needed for activation of the clothing item
    /// in specified <see cref="ClothingSlot"/>.
    /// </summary>
    [CreateAssetMenu(fileName = "ClothingItem1", menuName = "Clothing/Item")]
    public class ClothingItem : ScriptableObject
    {
        [SerializeField] private ClothingSlot _slot;

        [SerializeField, SpineSkin(dataField = nameof(_slot))]
        private string[] _spineSkinNames;

        [SerializeField] private BaseClothingModelPart[] _clothingModelParts;

        public ClothingSlot Slot => _slot;
        public IEnumerable<BaseClothingModelPart> ClothingModelParts => _clothingModelParts;
        public IEnumerable<string> SpineSkinNames => _spineSkinNames;

        protected void OnValidate()
        {
            if (_slot == null)
            {
                return;
            }

            IEnumerable<BaseClothingModelSlot> compatibleSlots = _slot.ClothingModelSlots;

            Array.Resize(ref _clothingModelParts, compatibleSlots.Count());

            foreach (BaseClothingModelPart part in _clothingModelParts)
            {
                if (part == null)
                {
                    continue;
                }

                Assert.IsTrue(
                        compatibleSlots.Contains(part.Slot),
                        $"Part {part.name} is incompatible with current model slots"
                    );
            }

            foreach (BaseClothingModelSlot slot in compatibleSlots)
            {
                Assert.IsTrue(
                        _clothingModelParts.Count(part => part?.Slot == slot) <= 1,
                        $"Slot {slot.name} has more than one part assigned."
                    );
            }
        }
    }
}