﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Debug;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.Nodes;
using DreamTeam.Synchronization.States;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Clothing
{
    public class ClothingSystem : MonoBehaviour, ISynchronizable, ISynchronizer<ClothingItem>, IInitable
    {
        [SerializeField] private ClothingItem[] _items;
        [SerializeField] private ClothingSlot[] _slots;

        [Inject] private SaveGroup _saveGroup;
        [Inject] private CheatPanel _cheatPanel;

        private ClothingItem[] _currentItems;

        public IEnumerable<ClothingSlot> Slots => _slots;

        public event ItemChangedDelegate ItemChanged;

        public void Init()
        {
            _cheatPanel.AddDrawer(CheatPanel.Category.Clothing, DrawCheats);

            _currentItems = new ClothingItem[_slots.Length];
            for (int i = 0; i < _currentItems.Length; i++)
            {
                _currentItems[i] = _slots[i].DefaultItem;
            }

            _saveGroup.RegisterSynchronizable("clothes", this);
        }

        public ClothingItem GetItem([NotNull] ClothingSlot slot)
        {
            int idx = Array.IndexOf(_slots, slot);

            Assert.IsTrue(idx >= 0, $"Can't find slot {slot.name}");

            return _currentItems[idx];
        }

        public void SetItem([NotNull] ClothingSlot slot, ClothingItem item)
        {
            int idx = Array.IndexOf(_slots, slot);

            Assert.IsTrue(idx >= 0, $"Can't find slot {slot.name}");

            ClothingItem oldItem = _currentItems[idx];

            if (oldItem != item)
            {
                _currentItems[idx] = item;
                ItemChanged?.Invoke(slot, item, oldItem);
            }
        }

        protected void OnDestroy()
        {
            _cheatPanel.RemoveDrawer(DrawCheats);
        }

        private void DrawCheats(Rect areaRect)
        {
            // Looks like these cheats will be useless when customization shop is added

            foreach (ClothingSlot slot in _slots)
            {
                if (slot.CanAcceptNull)
                {
                    if (GUILayout.Button("No " + slot.name))
                    {
                        SetItem(slot, null);
                    }
                }
            }

            foreach (ClothingItem item in _items)
            {
                if (GUILayout.Button(item.name))
                {
                    SetItem(item.Slot, item);
                }
            }
        }

        void ISynchronizable.Sync(State state)
        {
            for (int i = 0; i < _currentItems.Length; i++)
            {
                ClothingSlot slot = _slots[i];

                state.SyncObject(slot.name, ref _currentItems[i], slot.DefaultItem, this);
            }
        }

        Node ISynchronizer<ClothingItem>.Serialize(ClothingItem item)
        {
            return new ValueNode(item.name);
        }

        ClothingItem ISynchronizer<ClothingItem>.Deserialize(Node node)
        {
            string itemName = (node as ValueNode).Value as string;
            return Array.Find(_items, item => item.name == itemName);
        }

        #region Inner types

        public delegate void ItemChangedDelegate(ClothingSlot slot, ClothingItem newItem, ClothingItem oldItem);

        #endregion
    }
}