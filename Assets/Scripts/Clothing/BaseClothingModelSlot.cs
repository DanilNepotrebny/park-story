﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Clothing
{
    /// <summary>
    /// Slot for <see cref="BaseClothingModelPart"/>. 3d-model of a character is separated into parts which can
    /// be changed via <see cref="ClothingItem"/> Each slot contains one part of specific kind, i.e. hair slot
    /// contains curved hair mesh or straight hair mesh or any other mesh for hair customization.
    /// </summary>
    public abstract class BaseClothingModelSlot : ScriptableObject
    {
    }
}