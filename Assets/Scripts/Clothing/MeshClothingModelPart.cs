﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Clothing
{
    /// <summary> 
    /// Clothing model part which consists of a mesh attached to one parent bone (without skinning).
    /// </summary>
    public partial class MeshClothingModelPart : BaseClothingModelPart<MeshClothingModelSlot>
    {
        [SerializeField] private MeshRenderer _meshRenderer;
        [SerializeField] private Material[] _materials;
        
        public Material[] Materials => _materials;

        public Mesh Mesh => _meshRenderer.GetComponent<MeshFilter>().sharedMesh;
        public Transform Transform => _meshRenderer.transform;

        public string BonePath
        {
            get
            {
                Transform bone = _meshRenderer.transform.parent;
                return bone.GetFullPath(bone.root);
            }
        }
    }
}