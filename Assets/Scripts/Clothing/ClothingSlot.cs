﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

namespace DreamTeam.Clothing
{
    /// <summary>
    /// Slot for attaching of compatible <see cref="ClothingItem"/>. Contains common data for all compatible items.
    /// </summary>
    [CreateAssetMenu(fileName = "ClothingSlot1", menuName = "Clothing/Slot")]
    public class ClothingSlot : ScriptableObject, IHasSkeletonDataAsset
    {
        [SerializeField] private ClothingItem _defaultItem;
        [SerializeField] private BaseClothingModelSlot[] _clothingModelsSlots;
        [SerializeField] private SkeletonDataAsset _skeletonDataAsset;

        public ClothingItem DefaultItem => _defaultItem;

        public bool CanAcceptNull => DefaultItem == null;

        public IEnumerable<BaseClothingModelSlot> ClothingModelSlots => _clothingModelsSlots;

        public SkeletonDataAsset SkeletonDataAsset => _skeletonDataAsset;
    }
}