﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DreamTeam.Debug;
using DreamTeam.GameControls;
using DreamTeam.Mirroring;
using DreamTeam.Mirroring.Resolvers;
using DreamTeam.SceneLoading;
using DreamTeam.Utils;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.Timeline;
using Zenject;

namespace DreamTeam.Cutscenes
{
    public class CutsceneSystem : MonoBehaviour
    {
        [SerializeField] private GameControlsMode _gameControlsMode;
        [SerializeField] private List<SceneReference> _cutscenes;

        [Inject] private MirrorObjectsRegistrator _registrator;
        [Inject] private CheatPanel _cheatPanel;
        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private LoadingSystem _loadingSystem;

        private GameControlsContext _gameControlsContext;
        private ActionDisposable _modeDisableHandle;
        private CutsceneController _currentController;
        private int _currentCutscenePauseCounter;
        private bool _shouldSkipCutscenes;
        private SceneReference _currentPlayingCutscene;
        private Coroutine _cutscenePlayingCoroutine;

        public bool IsPlaying { get; private set; }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public event Action<SceneReference> CutsceneStarted;
        [Obfuscation(Exclude = false, Feature = "-rename")]
        public event Action<SceneReference> CutsceneFinished;

        public void PlayCutscene(SceneReference cutscene)
        {
            if (!IsPlaying)
            {
                _currentPlayingCutscene = cutscene;
                _cutscenePlayingCoroutine = StartCoroutine(CutscenePlayingRoutine(cutscene));
            }
            else
            {
                UnityEngine.Debug.LogError($"Trying to play a cutscene {cutscene.Name} when " +
                    $"{_currentController.PlayableDirector.name} is already playing");
            }
        }

        public BaseAsyncProcess PreloadCutscene(SceneReference cutscene)
        {
            return _loadingSystem.LoadScene(
                    cutscene.AssetPath,
                    false,
                    LoadingSystem.LoadingScreenType.None,
                    LoadSceneMode.Additive
                );
        }

        public ActionDisposable PauseCurrentCutscene()
        {
            if (_currentController.PlayableDirector == null ||
                !_currentController.PlayableGraph.IsValid() ||
                !_currentController.PlayableGraph.IsPlaying())
            {
                return null;
            }

            if (_currentCutscenePauseCounter == 0)
            {
                SetCurrentCutsceneSpeed(0.0);
            }

            _currentCutscenePauseCounter++;

            return new ActionDisposable(
                d =>
                {
                    _currentCutscenePauseCounter--;

                    if (_currentCutscenePauseCounter == 0)
                    {
                        SetCurrentCutsceneSpeed(1.0);
                    }
                });
        }

        public void OverrideGameControlsMode([NotNull] GameControlsMode mode)
        {
            _gameControlsSystem.SwitchMode(ref _modeDisableHandle, mode);
        }

        public void ResetGameControlsModeOverride()
        {
            _gameControlsSystem.SwitchMode(ref _modeDisableHandle, _gameControlsMode);
        }

        protected void Awake()
        {
            if (_cheatPanel != null)
            {
                _cheatPanel.AddDrawer(CheatPanel.Category.Cutscenes, DrawCheatsGUI);
            }

            _gameControlsContext = _gameControlsSystem.ActiveContext;
        }

        protected void OnDestroy()
        {
            ReleaseDisableHandle();
            if (_cheatPanel != null)
            {
                _cheatPanel.RemoveDrawer(DrawCheatsGUI);
            }
        }

        private void DrawCheatsGUI(Rect areaRect)
        {
            GUILayout.BeginHorizontal();
            _shouldSkipCutscenes = GUILayout.Toggle(_shouldSkipCutscenes, _shouldSkipCutscenes ? "X" : "", GUI.skin.button, GUILayout.Width(GUI.skin.button.lineHeight));
            GUILayout.Label("Skip cutscenes?");
            GUILayout.EndHorizontal();

            GUILayout.Space(10f);
            GUI.enabled = IsPlaying && _cutscenePlayingCoroutine != null && _currentPlayingCutscene != null;
            GUI.color = GUI.enabled ? Color.green : Color.white;
            if (GUILayout.Button("Finish current cutscene"))
            {
                StopCoroutine(_cutscenePlayingCoroutine);

                SceneManager.UnloadSceneAsync(_currentPlayingCutscene.Name);

                ReleaseDisableHandle();

                IsPlaying = false;
                _cutscenePlayingCoroutine = null;

                CutsceneFinished?.Invoke(_currentPlayingCutscene);

            }
            GUI.enabled = true;
            GUI.color = Color.white;
            GUILayout.Space(10f);

            foreach (SceneReference cutscene in _cutscenes)
            {
                if (GUILayout.Button(cutscene.Name))
                {
                    PlayCutscene(cutscene);
                    _cheatPanel.Hide();
                }
            }
        }

        private void SetCurrentCutsceneSpeed(double speed)
        {
            PlayableGraph graph = _currentController.PlayableGraph;
            if (graph.IsValid())
            {
                int rootsCount = graph.GetRootPlayableCount();
                for (int i = 0; i < rootsCount; i++)
                {
                    graph.GetRootPlayable(i).SetSpeed(speed);
                }
            }
        }

        private IEnumerator CutscenePlayingRoutine(SceneReference cutsceneReference)
        {
            IsPlaying = true;

            _currentCutscenePauseCounter = 0;

            SetControlsModeEnabled(_gameControlsMode, true);

            if (!_shouldSkipCutscenes)
            {
                if (!_loadingSystem.IsSceneLoaded(cutsceneReference.AssetPath))
                {
                    yield return PreloadCutscene(cutsceneReference);
                }

                CutsceneStarted?.Invoke(cutsceneReference);

                Scene cutscene = SceneManager.GetSceneByName(cutsceneReference.Name);
                List<CutsceneController> controllers = cutscene.GetComponentsOnScene<CutsceneController>();
                Assert.IsTrue(
                    controllers.Count == 1,
                    $"[DESIGNERS] Incorrect count of directors on cutscene {cutsceneReference.Name}. Scene should contain only one director!");

                _currentController = controllers.FirstOrDefault();
                Assert.IsNotNull(
                    _currentController,
                    $"[DESIGNERS] Missing {nameof(CutsceneController)} on '{cutsceneReference.Name}' cutscene");

                if (_currentController != null)
                {
                    if (CheckCutscene(_currentController.PlayableDirector))
                    {
                        List<BaseMirrorsResolver> resolvers = cutscene.GetComponentsOnScene<BaseMirrorsResolver>();
                        resolvers.ForEach(resolver => resolver.Resolve(_registrator));

                        _loadingSystem.ActivateScene(cutsceneReference.AssetPath);

                        _currentController.PlayCutscene();

                        yield return new WaitUntil(() => !_currentController.IsPlaying);
                    }

                    _currentController = null;
                }

                yield return _loadingSystem.UnloadSceneCoroutine(cutsceneReference.AssetPath);
            }

            SetControlsModeEnabled(_gameControlsMode, false);

            IsPlaying = false;
            _cutscenePlayingCoroutine = null;
            _currentPlayingCutscene = null;

            CutsceneFinished?.Invoke(cutsceneReference);
        }

        private bool CheckCutscene([NotNull] PlayableDirector director)
        {
            PlayableAsset asset = director.playableAsset;
            string cutsceneName = asset.name;

            if (director.extrapolationMode != DirectorWrapMode.None)
            {
                UnityEngine.Debug.LogError($"Cutscene {cutsceneName} should have wrap mode set to none!", director);
                return false;
            }

            var timelineAsset = asset as TimelineAsset;
            if (timelineAsset == null)
            {
                UnityEngine.Debug.LogError($"Cutscene {cutsceneName} has to be {nameof(TimelineAsset)}", director);
                return false;
            }

            return true;
        }

        private void SetControlsModeEnabled([NotNull] GameControlsMode mode, bool isEnabled)
        {
            if (isEnabled)
            {
                Assert.IsNull(_modeDisableHandle, "_modeDisableHandle != null");
                _modeDisableHandle = _gameControlsContext.EnableMode(mode);
            }
            else
            {
                ReleaseDisableHandle();
            }
        }

        private void ReleaseDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }
    }
}
