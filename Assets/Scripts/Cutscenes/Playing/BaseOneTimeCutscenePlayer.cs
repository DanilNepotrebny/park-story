﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Cutscenes.Playing
{
    public abstract class BaseOneTimeCutscenePlayer : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private SceneReference _cutscene;

        [Inject] private CutsceneSystem _cutsceneSystem;
        [Inject] private SaveGroup _saveGroup;

        private bool _hasPlayed;

        protected bool CanBePlayed => !_hasPlayed && enabled;

        public void Init()
        {
            _saveGroup.RegisterSynchronizable($"{name}_{_cutscene.Name}", this);
        }

        protected void Awake()
        {
            if (!CanBePlayed)
            {
                gameObject.SetActive(false);
            }
        }

        protected bool PlayCutscene()
        {
            if (_hasPlayed || !enabled)
            {
                return false;
            }

            _cutsceneSystem.CutsceneFinished += OnCutsceneFinished;
            _cutsceneSystem.PlayCutscene(_cutscene);
            return true;
        }

        protected virtual void OnCutsceneFinished(SceneReference cutscene)
        {
            Assert.IsTrue(cutscene == _cutscene, "Finished cutscene is not the one we are waiting to");
            _cutsceneSystem.CutsceneFinished -= OnCutsceneFinished;
            _hasPlayed = true;
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncBool(nameof(_hasPlayed), ref _hasPlayed, false);
        }
    }
}
