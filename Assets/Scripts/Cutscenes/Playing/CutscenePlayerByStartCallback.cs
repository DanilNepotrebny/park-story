﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Cutscenes.Playing
{
    public class CutscenePlayerByStartCallback : BaseOneTimeCutscenePlayer
    {
        protected void Start()
        {
            PlayCutscene();
        }
    }
}