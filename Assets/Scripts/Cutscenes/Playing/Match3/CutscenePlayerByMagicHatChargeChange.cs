﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.MagicHat;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using NUnit.Framework;
using Zenject;

namespace DreamTeam.Cutscenes.Playing.Match3
{
    public class CutscenePlayerByMagicHatChargeChange : BaseOneTimeCutscenePlayer
    {
        [Inject] private MagicHatSystem _magicHatSystem;

        private IDeferredInvocationHandle _targetSelectionContinueHandle;

        protected void Start()
        {
            if (CanBePlayed)
            {
                _magicHatSystem.ChargeChanged += OnMagicHatSystemChargeChanged;
            }
        }

        protected override void OnCutsceneFinished(SceneReference cutscene)
        {
            base.OnCutsceneFinished(cutscene);

            _magicHatSystem.SelectingTargetCell -= OnMagicHatSystemSelectingTargetCell;
            _targetSelectionContinueHandle?.Unlock();
            _targetSelectionContinueHandle = null;
        }

        private void OnMagicHatSystemSelectingTargetCell(IDeferredInvocationHandle continueHandle)
        {
            _magicHatSystem.SelectingTargetCell -= OnMagicHatSystemSelectingTargetCell;
            Assert.IsNull(_targetSelectionContinueHandle, "_targetSelectionContinueHandle != null");
            _targetSelectionContinueHandle = continueHandle.Lock();
        }

        private void OnMagicHatSystemChargeChanged(float chargePercent)
        {
            _magicHatSystem.ChargeChanged -= OnMagicHatSystemChargeChanged;
            _magicHatSystem.SelectingTargetCell += OnMagicHatSystemSelectingTargetCell;

            PlayCutscene();
        }
    }
}
