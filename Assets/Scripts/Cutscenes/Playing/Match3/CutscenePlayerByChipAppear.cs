﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Cutscenes.Playing.Match3
{
    public class CutscenePlayerByChipAppear : BaseOneTimeCutscenePlayer
    {
        [SerializeField, RequiredField] private ChipTag _chipTag;

        [Inject] private GenerationSystem _generationSystem;
        [Inject] private LevelFailureHandler _levelFailureHandler;
        [Inject] private LevelCompletionSystem _levelCompletionSystem;

        private IDeferredInvocationHandle _levelFailureHandle;

        protected virtual void Start()
        {
            if (CanBePlayed)
            {
                _generationSystem.ChipGenerated += OnChipGenerated;
                _levelCompletionSystem.LevelSucceeded += OnLevelSucceeded;
            }
        }

        protected void OnDestroy()
        {
            _levelCompletionSystem.LevelSucceeded -= OnLevelSucceeded;
        }

        protected override void OnCutsceneFinished(SceneReference cutscene)
        {
            base.OnCutsceneFinished(cutscene);

            _levelFailureHandler.FailureHandleBeginning -= OnLevelFailureHandleBeginning;
            ReleaseLevelFailureHandle();
        }

        private void OnLevelSucceeded()
        {
            _generationSystem.ChipGenerated -= OnChipGenerated;
        }

        private void OnChipGenerated([NotNull] ChipComponent chip)
        {
            if (IsTargetChip(chip))
            {
                _levelFailureHandler.FailureHandleBeginning += OnLevelFailureHandleBeginning;
                _generationSystem.ChipGenerated -= OnChipGenerated;
                PlayCutscene();
            }
        }

        private void OnLevelFailureHandleBeginning(IDeferredInvocationHandle handle)
        {
            Assert.IsNull(_levelFailureHandle, "Multiple FailureHandleBeginning received");
            ReleaseLevelFailureHandle();
            _levelFailureHandle = handle.Lock();
        }

        private bool IsTargetChip([NotNull] ChipComponent chip)
        {
            return chip.Tags.Has(_chipTag, false);
        }

        private void ReleaseLevelFailureHandle()
        {
            _levelFailureHandle?.Unlock();
            _levelFailureHandle = null;
        }
    }
}
