﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.FieldStabilization;
using Zenject;

namespace DreamTeam.Cutscenes.Playing.Match3
{
    public class CutscenePlayerByFieldStabilized : BaseOneTimeCutscenePlayer
    {
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;

        private const FieldStabilizationHandlerOrder StabilizationOrder =
            FieldStabilizationHandlerOrder.CutscenePlayerByFieldStabilization;

        protected virtual void Start()
        {
            if (CanBePlayed)
            {
                PlayCutsceneOnFieldStable();
            }
        }

        protected void OnDestroy()
        {
            _fieldStabilizationSystem.RemoveStabilizationHandler(OnFieldStabilized);
        }

        protected void PlayCutsceneOnFieldStable()
        {
            if (_fieldStabilizationSystem.IsFieldStable)
            {
                OnFieldStabilized();
            }
            else
            {
                _fieldStabilizationSystem.AddStabilizationHandler(OnFieldStabilized, StabilizationOrder);
            }
        }

        private void OnFieldStabilized()
        {
            _fieldStabilizationSystem.RemoveStabilizationHandler(OnFieldStabilized);
            PlayCutscene();
        }
    }
}
