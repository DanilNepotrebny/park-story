﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Match3.MagicHat;
using DreamTeam.Match3.MagicHat.Placing;
using DreamTeam.Utils;
using DreamTeam.Utils.Coroutines;
using DreamTeam.Utils.Invocation;
using Zenject;

namespace DreamTeam.Cutscenes.Playing.Match3
{
    public class CutscenePlayerByMagicHatLaunching : BaseOneTimeCutscenePlayer
    {
        [Inject] private MagicHatSystem _magicHatSystem;
        [Inject] private Instantiator _instantiator;
        [Inject] private CutsceneSystem _cutsceneSystem;

        private NearRequiredChipMagicHatPlacingRule _placingRule;

        private void Start()
        {
            if (CanBePlayed)
            {
                _magicHatSystem.SelectingTargetCell += OnSelectingTargetCell;
            }
        }

        private void OnSelectingTargetCell(IDeferredInvocationHandle continueHandle)
        {
            _magicHatSystem.SelectingTargetCell -= OnSelectingTargetCell;
            StartCoroutine(TryPlayCutscene());
        }

        protected override void OnCutsceneFinished(SceneReference cutscene)
        {
            base.OnCutsceneFinished(cutscene);

            _magicHatSystem.UnregisterPlacingRule(_placingRule);
        }

        private IEnumerator TryPlayCutscene()
        {
            if (_cutsceneSystem.IsPlaying)
            {
                yield return new WaitForEvent<SceneReference, CutsceneSystem>(
                    _cutsceneSystem,
                    nameof(_cutsceneSystem.CutsceneFinished));
            }

            _placingRule = _instantiator.InstantiateComponentOnNewGameObject<NearRequiredChipMagicHatPlacingRule>();
            _placingRule.transform.parent = _magicHatSystem.transform;

            PlayCutscene();
        }
    }
}