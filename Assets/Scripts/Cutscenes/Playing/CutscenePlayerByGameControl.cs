﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.GameControls;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Cutscenes.Playing
{
    /// <summary>
    /// Start to play cutscene when <param name="_control"/> in Unlocked state
    /// </summary>
    public class CutscenePlayerByGameControl : BaseOneTimeCutscenePlayer
    {
        [SerializeField, RequiredField] private GameControl _control;

        [Inject] private GameControlsSystem _gameControlsSystem;

        private const GameControl.State StateToPlay = GameControl.State.Unlocked;

        protected void Start()
        {
            if (CanBePlayed)
            {
                if (_gameControlsSystem.IsControlInState(_control, StateToPlay))
                {
                    PlayCutscene();
                }
                else
                {
                    _gameControlsSystem.AddGameControlStateChangeHandler(_control, OnControlStateChanged);
                }
            }
        }

        protected void OnDestroy()
        {
            _gameControlsSystem.RemoveGameControlStateChangeHandler(_control, OnControlStateChanged);
        }

        private void OnControlStateChanged(GameControl.State newState)
        {
            if (newState != StateToPlay)
            {
                return;
            }

            _gameControlsSystem.RemoveGameControlStateChangeHandler(_control, OnControlStateChanged);
            PlayCutscene();
        }
    }
}
