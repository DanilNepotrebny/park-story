﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Items.UnlockNotification;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Cutscenes.Playing
{
    public class CutscenePlayerByUnlockNotification : BaseOneTimeCutscenePlayer
    {
        [SerializeField, RequiredField] private UnlockNotificationType _notificationType;
        [SerializeField, RequiredField] private CountedItem _item;

        [Inject] private UnlockNotificationSystem _notificationSystem;

        private IDeferredInvocationHandle _presentationEndHandle;

        protected void OnEnable()
        {
            if (CanBePlayed)
            {
                _notificationSystem.NotificationPresenting += OnNotificationPresenting;
            }
        }

        protected void OnDisable()
        {
            Unsubscribe();
        }

        private void OnNotificationPresenting(
            UnlockNotificationType type,
            CountedItem item,
            IDeferredInvocationHandle presentationEndHandle)
        {
            if (type == _notificationType && item == _item)
            {
                Assert.IsNull(_presentationEndHandle, "_presentationEndHandle != null");
                _presentationEndHandle = presentationEndHandle.Lock();

                Unsubscribe();
                PlayCutscene();
            }
        }

        protected override void OnCutsceneFinished(SceneReference cutscene)
        {
            base.OnCutsceneFinished(cutscene);

            ReleasePresentationEndHandle();
        }

        private void ReleasePresentationEndHandle()
        {
            _presentationEndHandle?.Unlock();
            _presentationEndHandle = null;
        }

        private void Unsubscribe()
        {
            _notificationSystem.NotificationPresenting -= OnNotificationPresenting;
        }
    }
}
