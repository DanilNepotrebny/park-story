﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Popups;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Playables;
using Zenject;

namespace DreamTeam.Cutscenes
{
    [RequireComponent(typeof(PlayableDirector))]
    public class CutsceneController : MonoBehaviour
    {
        [SerializeField] private bool _hideAllPopupsBeforePlay = true;

        [Inject] private PopupSystem _popupSystem;

        private Coroutine _coroutine;

        public bool IsPlaying { get; private set; }
        public PlayableDirector PlayableDirector => GetComponent<PlayableDirector>();
        public PlayableGraph PlayableGraph => PlayableDirector.playableGraph;

        public void PlayCutscene()
        {
            Assert.IsNull(_coroutine, "_coroutine != null");
            _coroutine = StartCoroutine(PlayCutsceneInternal());
        }

        protected void OnEnable()
        {
            PlayableDirector.stopped += OnDirectorStopped;
        }

        protected void OnDisable()
        {
            PlayableDirector.stopped -= OnDirectorStopped;
        }

        private void OnDirectorStopped(PlayableDirector director)
        {
            IsPlaying = false;
        }

        private IEnumerator PlayCutsceneInternal()
        {
            IsPlaying = true;

            if (_hideAllPopupsBeforePlay)
            {
                bool arePopupsHidden = false;
                _popupSystem.HideAll(() => arePopupsHidden = true);
                yield return new WaitUntil(() => arePopupsHidden);
            }

            PlayableDirector.Play();
        }
    }
}
