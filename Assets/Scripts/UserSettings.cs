﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using UnityEngine;
using Zenject;

namespace DreamTeam
{
    public class UserSettings : MonoBehaviour, ISynchronizable, IInitable
    {
        [Inject] private SaveGroup _saveGroup;

        private string _userName = string.Empty;
        private string _petName = string.Empty;
        private float _locationCameraZoom = 0.5f;
        private Vector3 _locationCameraPosition = new Vector3(0f, 0f, -100f);

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("UserSettings", this);
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string PetName
        {
            get { return _petName; }
            set { _petName = value; }
        }

        public float LocationCameraZoom
        {
            get { return _locationCameraZoom; }
            set { _locationCameraZoom = Mathf.Clamp01(value); }
        }

        public Vector3 LocationCameraPosition
        {
            get { return _locationCameraPosition; }
            set { _locationCameraPosition = value; }
        }

        public void Sync(State state)
        {
            state.SyncString("UserName", ref _userName, string.Empty);
            state.SyncString("PetName", ref _petName, string.Empty);
            state.SyncFloat("CameraZoom", ref _locationCameraZoom, 0.5f);
            state.SyncFloat("CameraPosition_X", ref _locationCameraPosition.x, 0f);
            state.SyncFloat("CameraPosition_Y", ref _locationCameraPosition.y, 0f);
            state.SyncFloat("CameraPosition_Z", ref _locationCameraPosition.z, -100f);
        }
    }
}