﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Cutscenes;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam
{
    public class PlayCutsceneByCutsceneFinish : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private SceneReference _cutsceneWaitingForFinish;
        [SerializeField] private SceneReference _cutsceneToStart;

        [Inject] private CutsceneSystem _cutsceneSystem;
        [Inject] private SaveGroup _saveGroup;

        private PlayerState _state;

        public void Init()
        {
            _saveGroup.RegisterSynchronizable($"CutscenePlayerAfterCutsceneFinished_{_cutsceneToStart.Name}", this);
        }

        protected void Start()
        {
            switch (_state)
            {
                case PlayerState.Waiting:
                    _cutsceneSystem.CutsceneFinished += OnCutsceneFinished;
                    break;

                case PlayerState.Started:
                    _cutsceneSystem.CutsceneFinished += OnCutsceneFinished;
                    _cutsceneSystem.PlayCutscene(_cutsceneToStart);
                    break;
            }
        }

        private void OnCutsceneFinished(SceneReference cutscene)
        {
            if (_cutsceneWaitingForFinish.AssetPath == cutscene.AssetPath)
            {
                _state = PlayerState.Started;
                _cutsceneSystem.PlayCutscene(_cutsceneToStart);
            }
            else if (_cutsceneToStart.AssetPath == cutscene.AssetPath)
            {
                _state = PlayerState.Finished;
                _cutsceneSystem.CutsceneFinished -= OnCutsceneFinished;
            }
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncEnum("PlayerState", ref _state);
        }

        private enum PlayerState
        {
            Waiting,
            Started,
            Finished
        }
    }
}