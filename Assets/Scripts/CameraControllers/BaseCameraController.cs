﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Screen;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.CameraControllers
{
    /// <summary>
    /// Component that configures camera to render 2d scene in a pixel perfect way.
    /// </summary>
    [ExecuteInEditMode]
    public abstract class BaseCameraController : MonoBehaviour
    {
        [Inject] private ScreenController _screenInjected;

        private float _prevHeight;
        private float _prevWidth;

        protected ScreenController Screen => _screenInjected;

        protected void UpdateCameraSize()
        {
            if (EditorUtils.IsPrefab(gameObject))
            {
                return;
            }

            if (Screen == null || Screen.Height == 0)
            {
                return;
            }

            _prevHeight = Screen.Height;
            _prevWidth = Screen.Width;

            OnScreenSizeChanged();
        }

        protected abstract void OnScreenSizeChanged();

        protected virtual void Awake()
        {
            #if UNITY_EDITOR
            {
                if (!Application.isPlaying)
                {
                    _screenInjected = SystemAssetAccessor<ScreenController>.Asset;
                }
            }
            #endif // UNITY_EDITOR
        }

        protected void Update()
        {
            if (Screen == null)
            {
                return;
            }

            if (Mathf.Abs(_prevHeight - Screen.Height) > Mathf.Epsilon ||
                Mathf.Abs(_prevWidth - Screen.Width) > Mathf.Epsilon)
            {
                UpdateCameraSize();
            }
        }

        #if UNITY_EDITOR
        protected void OnValidate()
        {
            if (!Application.isPlaying)
            {
                UpdateCameraSize();
            }
        }
        #endif // UNITY_EDITOR
    }
}