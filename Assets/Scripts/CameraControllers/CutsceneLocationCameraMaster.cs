﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Cinemachine;
using DreamTeam.Cutscenes;
using DreamTeam.Screen;
using UnityEngine;
using Zenject;

namespace DreamTeam.CameraControllers
{
    [RequireComponent(typeof(CinemachineVirtualCamera))]
    public class CutsceneLocationCameraMaster : MonoBehaviour
    {
        [Inject] private LocationCameraController _locationCameraController;
        [Inject] private CinemachineBrain _locationCameraBrain;
        [Inject] private CutsceneSystem _cutsceneSystem;
        [Inject] private ScreenController _screen;

        private CinemachineVirtualCamera _cachedVirtualCamera;

        private CinemachineVirtualCamera _virtualCamera
        {
            get
            {
                if (_cachedVirtualCamera == null)
                {
                    _cachedVirtualCamera = GetComponent<CinemachineVirtualCamera>();
                }

                return _cachedVirtualCamera;
            }
        }

        private void Update()
        {
            if (_locationCameraController != null &&
                _locationCameraBrain != null &&
                _cutsceneSystem != null &&
                _cutsceneSystem.IsPlaying &&
                ReferenceEquals(_locationCameraBrain.ActiveVirtualCamera, _virtualCamera))
            {
                _locationCameraController.CameraPosition = transform.position;

                float orthographicSize = _virtualCamera.m_Lens.OrthographicSize * _screen.CalculateOrthographicFactor();
                _locationCameraController.CameraZoomValue =
                    _locationCameraController.ConvertOrthographicSizeToZoomValue(orthographicSize);
            }
        }
    }
}