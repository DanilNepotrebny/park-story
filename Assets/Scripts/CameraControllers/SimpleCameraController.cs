﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.CameraControllers
{
    /// <summary>
    /// Extends <see cref="BaseCameraController"/> functionality. Adapts camera zoom to always fit specified reference resolution.
    /// </summary>
    [RequireComponent(typeof(Camera))]
    public class SimpleCameraController : BaseCameraController
    {
        private Camera _camera;

        protected override void Awake()
        {
            base.Awake();

            _camera = GetComponent<Camera>();
        }

        protected override void OnScreenSizeChanged()
        {
            if (_camera != null)
            {
                _camera.orthographicSize = Screen.Height / (2 * Screen.ReferencePPU * GetZoom());
            }
        }

        private float GetZoom()
        {
            int currentMin = Mathf.Min(Screen.Width, Screen.Height);
            int referenceMin = Mathf.Min(Screen.ReferenceLandscapeResolution.x, Screen.ReferenceLandscapeResolution.y);

            return (float)currentMin / referenceMin;
        }
    }
}