﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.CameraControllers
{
    /// <summary>
    /// Extends <see cref="BaseCameraController"/> functionality. Adapts camera zoom to always fit specified reference resolution.
    /// </summary>
    [RequireComponent(typeof(Camera))]
    public class AdaptiveCameraController : BaseCameraController
    {
        [SerializeField] private int _referenceWidth = 2048;
        [SerializeField] private int _referenceHeight = 1536;

        private Camera _camera;

        protected override void Awake()
        {
            base.Awake();

            _camera = GetComponent<Camera>();
        }

        protected void OnEnable()
        {
            UpdateCameraSize();
        }

        protected override void OnScreenSizeChanged()
        {
            if (_camera != null)
            {
                _camera.orthographicSize = Screen.Height / (2 * Screen.ReferencePPU * GetZoom());
            }
        }

        private float GetZoom()
        {
            int currentMin = Mathf.Min(Screen.Width, Screen.Height);
            int referenceMin = Mathf.Min(_referenceWidth, _referenceHeight);

            return (float)currentMin / referenceMin;
        }
    }
}