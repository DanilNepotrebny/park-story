﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using Cinemachine;
using DreamTeam.Utils;
using DreamTeam.Utils.Tweening;
using TouchScript.Gestures.TransformGestures;
using UnityEngine;
using Zenject;

namespace DreamTeam.CameraControllers
{
    [RequireComponent(typeof(CinemachineVirtualCamera))]
    public class LocationCameraController : BaseCameraController
    {
        [Header("Gestures")]
        [SerializeField] private TransformGesture _swipeGesture;
        [SerializeField] private ScreenTransformGesture _zoomGesture;

        [Header("Zoom settings")]
        [SerializeField] private float _fullZoomDistanceInches = 4;
        [SerializeField] private MinMaxRangedFloat _zoomRange = new MinMaxRangedFloat(0.1f, 1f);
        [SerializeField] private Easing _stretchBack = new Easing();
        [SerializeField] private float _stretchBackDuration = 0.7f;

        [Header("Positioning")]
        [SerializeField, Tooltip("Object to look at on initialization. If NULL using last position")]
        private GameObject _initialLookAtObject;

        [Inject] private UserSettings _userSettings;

        private CinemachineVirtualCamera _camera;
        private LTDescr _stretchTween;

        private float _cachedCameraZoomValue = 0.5f;

        public float CameraZoomValue
        {
            get
            {
                return _cachedCameraZoomValue;
            }

            set
            {
                if (Math.Abs(_cachedCameraZoomValue - value) > Mathf.Epsilon)
                {
                    _cachedCameraZoomValue = value;

                    if (_userSettings != null)
                    {
                        _userSettings.LocationCameraZoom = _cachedCameraZoomValue;
                    }

                    UpdateCameraSize();
                }
            }
        }

        public float CameraOrthographicSize => _camera.m_Lens.OrthographicSize;

        public Vector3 CameraPosition
        {
            get
            {
                return transform.position;
            }

            set
            {
                if (!transform.position.Equals(value))
                {
                    transform.position = value;

                    if (_userSettings != null)
                    {
                        _userSettings.LocationCameraPosition = transform.position;
                    }
                }
            }
        }

        public float ConvertOrthographicSizeToZoomValue(float size)
        {
            float result = 0f;

            float clampedZoomValue = Screen.Height / (2 * Screen.ReferencePPU * size);
            if (clampedZoomValue > 0.001f)
            {
                result = (clampedZoomValue / (Screen.ScreenSizeFactor + Screen.DPIFactor) * 2f - _zoomRange.MinLimit) / _zoomRange.Range;
            }
            else
            {
                result = 0.001f;
            }

            return result;
        }

        protected override void Awake()
        {
            base.Awake();

            _camera = GetComponent<CinemachineVirtualCamera>();
        }

        protected void OnEnable()
        {
            SubscribeGestures();
        }

        protected void Start()
        {
            if (_userSettings == null)
            {
                return;
            }

            CameraPosition = _initialLookAtObject != null ?
                new Vector3(_initialLookAtObject.transform.position.x, _initialLookAtObject.transform.position.y, CameraPosition.z) :
                _userSettings.LocationCameraPosition;

            CameraZoomValue = _userSettings.LocationCameraZoom;
        }

        protected void OnDisable()
        {
            UnSubscribeGestures();
        }

        protected sealed override void OnScreenSizeChanged()
        {
            if (_camera != null)
            {
                float clampedZoomValue = Mathf.Max(_zoomRange.GetValue(CameraZoomValue) *
                    (Screen.ScreenSizeFactor + Screen.DPIFactor) * 0.5f, 0.001f);
                _camera.m_Lens.OrthographicSize = Screen.Height / (2 * Screen.ReferencePPU * clampedZoomValue);
            }
        }

        private void SubscribeGestures()
        {
            if (_swipeGesture != null)
            {
                _swipeGesture.Transformed += OnSwiped;
            }

            if (_zoomGesture != null)
            {
                _zoomGesture.Transformed += OnZoomed;
                _zoomGesture.TransformCompleted += OnZoomCompleted;
            }
        }

        private void UnSubscribeGestures()
        {
            if (_swipeGesture != null)
            {
                _swipeGesture.Transformed -= OnSwiped;
            }

            if (_zoomGesture != null)
            {
                _zoomGesture.Transformed -= OnZoomed;
                _zoomGesture.TransformCompleted -= OnZoomCompleted;
            }
        }

        private void OnSwiped(object sender, EventArgs e)
        {
            CameraPosition += new Vector3(-_swipeGesture.DeltaPosition.x, -_swipeGesture.DeltaPosition.y, 0);
        }

        private void OnZoomed(object sender, EventArgs e)
        {
            float prevDistance = (_zoomGesture.ActivePointers[0].PreviousPosition - _zoomGesture.ActivePointers[1].PreviousPosition).magnitude;
            float distance = (_zoomGesture.ActivePointers[0].Position - _zoomGesture.ActivePointers[1].Position).magnitude;
            float delta = (distance - prevDistance) / Screen.DPI;
            float scaleDelta = delta / _fullZoomDistanceInches * GetDeltaStretchFactor();

            CameraZoomValue += scaleDelta;
        }

        private void OnZoomCompleted(object sender, EventArgs e)
        {
            HandleZoomStretch();
        }

        private void HandleZoomStretch()
        {
            if (CameraZoomValue < _zoomRange.MinNormalized)
            {
                StartStretchTween(_zoomRange.MinNormalized);
            }

            if (CameraZoomValue > _zoomRange.MaxNormalized)
            {
                StartStretchTween(_zoomRange.MaxNormalized);
            }
        }

        private void StartStretchTween(float to)
        {
            if (_stretchTween != null)
            {
                LeanTween.cancel(_stretchTween.id);
            }

            _stretchTween = LeanTween.value(CameraZoomValue, to, _stretchBackDuration);
            _stretchTween.setOnUpdate(val => CameraZoomValue = val);
            _stretchTween.setOnComplete(() => _stretchTween = null);
            _stretchBack.SetupDescriptor(_stretchTween);
        }

        private float GetDeltaStretchFactor()
        {
            if (CameraZoomValue < _zoomRange.MinNormalized)
            {
                return CameraZoomValue;
            }

            if (CameraZoomValue > _zoomRange.MaxNormalized)
            {
                return 1 - CameraZoomValue;
            }

            return 1;
        }
    }
}