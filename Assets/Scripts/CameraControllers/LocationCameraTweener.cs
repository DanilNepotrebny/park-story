﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Screen;
using DreamTeam.Utils.Tweening;
using UnityEngine;
using Zenject;

namespace DreamTeam.CameraControllers
{
    [RequireComponent(typeof(LocationCameraController))]
    public class LocationCameraTweener : MonoBehaviour
    {
        [Header("Movement")]
        [SerializeField] private float _movementTime;
        [SerializeField] private Easing _movementEasing;

        [Header("Zooming")]
        [SerializeField] private float _zoomingTime;
        [SerializeField] private Easing _zoomingEasing;

        [Inject] private ScreenController _screen;

        private LocationCameraController _cachedCameraController;
        private LTDescr _movementTween;
        private LTDescr _zoomingTween;

        private LocationCameraController _cameraController
        {
            get
            {
                if (_cachedCameraController == null)
                {
                    _cachedCameraController = GetComponent<LocationCameraController>();
                }

                return _cachedCameraController;
            }
        }

        public void MoveTo(Vector2 position)
        {
            StopMovementTween();

            _movementTween = LeanTween.move(gameObject, position, _movementTime);
            _movementEasing.SetupDescriptor(_movementTween);
        }

        public void ZoomTo(float orthographicSize)
        {
            StopZoomingTween();

            _zoomingTween = LeanTween.value(
                _cameraController.CameraOrthographicSize,
                orthographicSize * _screen.CalculateOrthographicFactor(),
                _zoomingTime);
            _zoomingEasing.SetupDescriptor(_zoomingTween);
            _zoomingTween.setOnUpdate(OnOrthographicSizeChanged);
        }

        protected void OnDestroy()
        {
            StopMovementTween();
            StopZoomingTween();
        }

        private void StopMovementTween()
        {
            if (_movementTween != null)
            {
                LeanTween.cancel(_movementTween.uniqueId, true);
                _movementTween = null;
            }
        }

        private void StopZoomingTween()
        {
            if (_zoomingTween != null)
            {
                LeanTween.cancel(_zoomingTween.uniqueId, true);
                _zoomingTween = null;
            }
        }

        private void OnOrthographicSizeChanged(float orthographicSize)
        {
            _cameraController.CameraZoomValue = _cameraController.ConvertOrthographicSizeToZoomValue(orthographicSize);
        }
    }
}