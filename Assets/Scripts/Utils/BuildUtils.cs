﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace DreamTeam.Utils
{
    [InitializeOnLoad]
    public static class BuildUtils
    {
        public static bool IsBuilding { get; private set; }

        public static event PlayerBuildingDelegate PlayerBuilding;

        static BuildUtils()
        {
            BuildPlayerWindow.RegisterBuildPlayerHandler(options => BuildProject(options));
        }

        public static void BuildProject(string outPath, string definesChange)
        {
            string[] scenes = EditorBuildSettingsScene.GetActiveSceneList(EditorBuildSettings.scenes)
                .Where(path => !string.IsNullOrEmpty(path))
                .ToArray();

            var options = new BuildPlayerOptions
            {
                target = EditorUserBuildSettings.activeBuildTarget,
                targetGroup = EditorUserBuildSettings.selectedBuildTargetGroup,
                scenes = scenes,
                locationPathName = outPath
            };
            BuildProject(options, definesChange);
        }

        private static void BuildProject(BuildPlayerOptions options, string definesChange = "")
        {
            IsBuilding = true;

            BuildTargetGroup group = options.targetGroup;

            string oldDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(group);

            try
            {
                if (!PreprocessBeforeBuild())
                {
                    UnityEngine.Debug.Log("Build is cancelled by user.");
                    return;
                }

                var definesSet = new HashSet<string>(oldDefines.Split(';'));
                UpdateDefines(definesSet, definesChange);

                PlayerBuilding?.Invoke(ref options, definesSet);

                string newDefines = string.Join(";", definesSet);
                UnityEngine.Debug.Log("Building with defines: " + newDefines);
                PlayerSettings.SetScriptingDefineSymbolsForGroup(group, newDefines);

                BuildPipeline.BuildPlayer(options);
            }
            finally
            {
                PlayerSettings.SetScriptingDefineSymbolsForGroup(group, oldDefines);
                IsBuilding = false;
            }
        }

        private static void UpdateDefines(HashSet<string> definesSet, string definesChange)
        {
            string[] changes = definesChange.Split(';');
            foreach (string change in changes)
            {
                if (string.IsNullOrEmpty(change))
                {
                    continue;
                }

                if (change.StartsWith("-"))
                {
                    definesSet.Remove(change.Remove(0, 1));
                }
                else
                {
                    definesSet.Add(change);
                }
            }
        }

        private static bool PreprocessBeforeBuild()
        {
            string[] pathes = AssetDatabase.GetAllAssetPaths();
            pathes = pathes
                .Where(path => path.StartsWith("Assets") && !path.StartsWith("Assets/Plugins") && (path.EndsWith(".prefab") || path.EndsWith(".asset")))
                .ToArray();

            int assetsCount = pathes.Length;

            for (int i = 0; i < assetsCount; i++)
            {
                string path = pathes[i];

                string caption = $"Preprocess assets [{i + 1}/{assetsCount}]";

                if (EditorUtility.DisplayCancelableProgressBar(caption, path, (i + 1f) / assetsCount))
                {
                    EditorUtility.ClearProgressBar();
                    return false;
                }

                Object[] assets = AssetDatabase.LoadAllAssetsAtPath(path);

                foreach (Object asset in assets)
                {
                    try
                    {
                        PreprocessAsset(asset, path);
                    }
                    catch (Exception e)
                    {
                        UnityEngine.Debug.LogError($"Error while preprocessing asset {asset.name} at {path}", asset);
                        UnityEngine.Debug.LogException(e);
                    }
                }
            }

            EditorUtility.ClearProgressBar();
            return true;
        }

        private static void PreprocessAsset(Object asset, string path)
        {
            var go = asset as GameObject;
            if (go != null)
            {
                foreach (var behaviour in go.GetComponentsInChildren<MonoBehaviour>(true))
                {
                    if (IsScriptMissing(behaviour))
                    {
                        UnityEngine.Debug.LogWarning($"Missing script on game object at {path}", asset);
                        continue;
                    }

                    PreprocessUnityObject(behaviour);
                }
            }

            var so = asset as ScriptableObject;
            if (so != null)
            {
                PreprocessUnityObject(so);
            }
        }

        private static void PreprocessUnityObject(Object obj)
        {
            if (obj == null)
            {
                // Skip destroyed unity object.
                return;
            }

            PreprocessObject(obj);
        }

        private static void PreprocessObject(object obj)
        {
            (obj as IBuildPreprocessor)?.PreprocessBeforeBuild();

            Type t = obj.GetType();
            PreprocessFields(obj, t, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

            t = t.BaseType;

            while (t != null && t != typeof(object) && t != typeof(ScriptableObject) && t != typeof(MonoBehaviour))
            {
                PreprocessFields(obj, t, BindingFlags.Instance | BindingFlags.NonPublic);
                t = t.BaseType;
            }
        }

        private static void PreprocessFields(object obj, Type type, BindingFlags flags)
        {
            FieldInfo[] fields = type.GetFields(flags);
            foreach (FieldInfo field in fields)
            {
                if (field.IsNotSerialized || field.IsLiteral)
                {
                    continue;
                }

                Type fieldType = field.FieldType;
                if (!ShouldProcessType(fieldType))
                {
                    continue;
                }

                if (field.IsPrivate && field.CustomAttributes.All(data => data.AttributeType != typeof(SerializeField)))
                {
                    continue;
                }

                Type elementType = GetElementType(fieldType);
                if (elementType != null)
                {
                    if (ShouldProcessType(elementType) && !IsForeignType(elementType))
                    {
                        object list = field.GetValue(obj);
                        if (list != null)
                        {
                            foreach (object element in (IEnumerable)list)
                            {
                                PreprocessObject(element);
                            }
                        }
                    }

                    continue;
                }

                if (IsForeignType(fieldType))
                {
                    continue;
                }

                object o = field.GetValue(obj);
                if (o != null)
                {
                    PreprocessObject(o);
                }
            }
        }

        private static bool ShouldProcessType(Type type)
        {
            return type.IsSerializable && !type.IsPrimitive && !type.IsEnum;
        }

        private static bool IsForeignType(Type type)
        {
            string @namespace = type.Namespace;
            return string.IsNullOrEmpty(@namespace) || !@namespace.StartsWith("DreamTeam");
        }

        private static Type GetElementType(Type type)
        {
            if (type.IsArray)
            {
                return type.GetElementType();
            }

            if (type == typeof(List<>))
            {
                return type.GetGenericArguments()[0];
            }

            return null;
        }

        private static bool IsScriptMissing(MonoBehaviour behaviour)
        {
            return ReferenceEquals(behaviour, null);
        }

        #region Inner types

        public delegate void PlayerBuildingDelegate(ref BuildPlayerOptions options, HashSet<string> defines);

        private class SceneProcessor : IProcessSceneWithReport
        {
            int IOrderedCallback.callbackOrder => 0;

            void IProcessSceneWithReport.OnProcessScene(Scene scene, BuildReport report)
            {
                List<MonoBehaviour> behaviours = scene.GetComponentsOnScene<MonoBehaviour>();
                foreach (var behaviour in behaviours)
                {
                    if (IsScriptMissing(behaviour))
                    {
                        UnityEngine.Debug.LogWarning($"Missing script on game object in scene {scene.path}.");
                        continue;
                    }

                    PreprocessUnityObject(behaviour);
                }
            }
        }

        #endregion
    }
 }

#endif