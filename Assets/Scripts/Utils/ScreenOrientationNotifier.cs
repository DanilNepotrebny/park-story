﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Screen;
using UnityEngine;
using Zenject;

namespace DreamTeam.Utils
{
    [ExecuteInEditMode, DisallowMultipleComponent]
    public class ScreenOrientationNotifier : MonoBehaviour
    {
        [Inject] private ScreenController _screenInjected;

        private Orientation _orientation;

        public Orientation CurrentOrientation
        {
            get { return _orientation; }
            private set
            {
                if (_orientation == value)
                {
                    return;
                }

                _orientation = value;
                OrientationChanged?.Invoke(_orientation);
            }
        }

        private ScreenController _screen
        {
            get
            {
                if (Application.isEditor)
                {
                    #if UNITY_EDITOR
                    {
                        return SystemAssetAccessor<ScreenController>.Asset;
                    }
                    #endif
                }

                return _screenInjected;
            }
        }

        public event Action<Orientation> OrientationChanged;

        public static Orientation CalculateOrientation(int screenWidth, int screenHeight)
        {
            return ((float)screenWidth / screenHeight > 1f) ? Orientation.Landscape : Orientation.Portrait;
        }

        protected void Awake()
        {
            UpdateOrientation();
        }

        protected void Update()
        {
            UpdateOrientation();
        }

        private void UpdateOrientation()
        {
            CurrentOrientation = CalculateOrientation(_screen.Width, _screen.Height);
        }

        #region Inner types

        public enum Orientation
        {
            Portrait,
            Landscape
        }

        #endregion Inner types
    }
}
