﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Triggers position change event
    /// </summary>
    [ExecuteInEditMode, DisallowMultipleComponent]
    public class PositionChangeNotifier : MonoBehaviour
    {
        [SerializeField] private float _epsilon;

        private Vector3 _oldPosition;

        /// <summary>
        /// Event for transform changes
        /// </summary>
        public event Action<Transform> PositionChanged;

        /// <summary>
        /// Unity callback
        /// </summary>
        protected void Update()
        {
            if (transform.hasChanged)
            {
                transform.hasChanged = false;

                Vector3 newPosition = transform.position;

                if ((_oldPosition - newPosition).sqrMagnitude > _epsilon)
                {
                    _oldPosition = transform.position;
                    PositionChanged?.Invoke(transform);
                }
            }
        }
    }
}