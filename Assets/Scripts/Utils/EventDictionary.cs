﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;

namespace DreamTeam.Utils
{
    public class EventDictionary<TKey> : BaseEventDictionary<TKey, Action>
    {
        public void Invoke(TKey key)
        {
            this[key]?.Invoke();
        }

        public void InvokeAll()
        {
            foreach (Action action in Handlers)
            {
                action?.Invoke();
            }
        }

        protected override void Add(ref Action handlers, Action handler)
        {
            handlers += handler;
        }

        protected override void Remove(ref Action handlers, Action handler)
        {
            handlers -= handler;
        }
    }

    public class EventDictionary<TKey, TParam> : BaseEventDictionary<TKey, Action<TParam>>
    {
        public void Invoke(TKey key, TParam parameter)
        {
            this[key]?.Invoke(parameter);
        }

        public void InvokeAll(TParam parameter)
        {
            foreach (Action<TParam> action in Handlers)
            {
                action?.Invoke(parameter);
            }
        }

        protected override void Add(ref Action<TParam> handlers, Action<TParam> handler)
        {
            handlers += handler;
        }

        protected override void Remove(ref Action<TParam> handlers, Action<TParam> handler)
        {
            handlers -= handler;
        }
    }

    public class EventDictionary<TKey, TParam1, TParam2> : BaseEventDictionary<TKey, Action<TParam1, TParam2>>
    {
        public void Invoke(TKey key, TParam1 parameter1, TParam2 parameter2)
        {
            this[key]?.Invoke(parameter1, parameter2);
        }

        public void InvokeAll(TParam1 parameter1, TParam2 parameter2)
        {
            foreach (Action<TParam1, TParam2> action in Handlers)
            {
                action?.Invoke(parameter1, parameter2);
            }
        }

        protected override void Add(ref Action<TParam1, TParam2> handlers, Action<TParam1, TParam2> handler)
        {
            handlers += handler;
        }

        protected override void Remove(ref Action<TParam1, TParam2> handlers, Action<TParam1, TParam2> handler)
        {
            handlers -= handler;
        }
    }

    /// <summary>
    /// Dictionary of event handlers.
    /// </summary>
    public abstract class BaseEventDictionary<TKey, TAction> where TAction : class
    {
        private Dictionary<TKey, TAction> _handlers = new Dictionary<TKey, TAction>();

        protected TAction this[TKey key] => _handlers.GetValueOrDefault(key);
        protected Dictionary<TKey, TAction>.ValueCollection Handlers => _handlers.Values;

        public void Add(TKey key, TAction handler)
        {
            TAction handlers;

            if (_handlers.TryGetValue(key, out handlers))
            {
                Add(ref handlers, handler);
                _handlers[key] = handlers;
            }
            else
            {
                _handlers.Add(key, handler);
            }
        }

        public void Remove(TKey key, TAction handler)
        {
            TAction handlers;

            if (_handlers.TryGetValue(key, out handlers))
            {
                Remove(ref handlers, handler);
                _handlers[key] = handlers;
            }
        }

        protected abstract void Add(ref TAction handlers, TAction handler);
        protected abstract void Remove(ref TAction handlers, TAction handler);
    }
}