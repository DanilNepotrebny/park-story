﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Utils
{
    public class Match3ObjectSpawner : PrefabSpawner
    {
        #if NO_MATCH3
        protected override bool IsSpawnEnabled => false;
        #endif
    }
}