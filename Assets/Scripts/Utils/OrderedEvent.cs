﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;

namespace DreamTeam.Utils
{
    public abstract class BaseOrderedEvent<TAction>
        where TAction : class
    {
        private List<EventHandler> _handlers;
        private bool _isInvocationCancelled;
        private bool _isInvocationActive;

        public void Add(TAction handler, int order)
        {
            if (_handlers == null)
            {
                _handlers = new List<EventHandler>();
            }

            _handlers.Add(
                    new EventHandler
                    {
                        Handler = handler,
                        Order = order
                    }
                );
            _handlers.Sort((a, b) => a.Order.CompareTo(b.Order));
        }

        public void Remove(TAction handler)
        {
            _handlers?.RemoveAll(e => e.Handler.Equals(handler));
        }

        public void Cancel()
        {
            _isInvocationCancelled = true;
        }

        protected IEnumerable<TAction> StartInvocation()
        {
            _isInvocationCancelled = false;

            List<EventHandler> handlers = new List<EventHandler>(_handlers);
            foreach (EventHandler e in handlers)
            {
                if (_isInvocationCancelled)
                {
                    break;
                }

                yield return e.Handler;
            }
        }

        #region

        private struct EventHandler
        {
            public TAction Handler;
            public int Order;
        }

        #endregion
    }

    public class OrderedEvent : BaseOrderedEvent<Action>
    {
        public void Invoke()
        {
            IEnumerable<Action> handlers = StartInvocation();
            foreach (Action handler in handlers)
            {
                handler.Invoke();
            }
        }
    }

    public class OrderedEvent<TArg> : BaseOrderedEvent<Action<TArg>>
    {
        public void Invoke(TArg arg)
        {
            IEnumerable<Action<TArg>> handlers = StartInvocation();
            foreach (Action<TArg> handler in handlers)
            {
                handler.Invoke(arg);
            }
        }
    }

    public class OrderedEvent<TArg1, TArg2> : BaseOrderedEvent<Action<TArg1, TArg2>>
    {
        public void Invoke(TArg1 arg1, TArg2 arg2)
        {
            IEnumerable<Action<TArg1, TArg2>> handlers = StartInvocation();
            foreach (Action<TArg1, TArg2> handler in handlers)
            {
                handler.Invoke(arg1, arg2);
            }
        }
    }

    public class OrderedEvent<TArg1, TArg2, TArg3> : BaseOrderedEvent<Action<TArg1, TArg2, TArg3>>
    {
        public void Invoke(TArg1 arg1, TArg2 arg2, TArg3 arg3)
        {
            IEnumerable<Action<TArg1, TArg2, TArg3>> handlers = StartInvocation();
            foreach (Action<TArg1, TArg2, TArg3> handler in handlers)
            {
                handler.Invoke(arg1, arg2, arg3);
            }
        }
    }
}