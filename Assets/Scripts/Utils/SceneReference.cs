﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using UnityEngine;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Serializable reference to scene asset.
    /// </summary>
    [Serializable]
    public partial class SceneReference : ISynchronizable
    {
        [SerializeField] private string _path;
        [SerializeField] private string _guid;

        /// <summary>
        /// Name of the scene that can be used for SceneManager operations.
        /// </summary>
        public string Name
        {
            get
            {
                CheckSceneValid();
                return _path.Substring(_path.LastIndexOf("/") + 1);
            }
        }

        /// <summary>
        /// Full path to the scene asset.
        /// </summary>
        public string AssetPath
        {
            get
            {
                CheckSceneValid();
                return _assetPath;
            }
        }

        /// <summary>
        /// Returns true if the reference is set, false otherwise.
        /// </summary>
        public bool HasValue => !string.IsNullOrEmpty(_path);

        private string _assetPath => "Assets/" + _path + ".unity";

        public void Sync(State state)
        {
            state.SyncString("path", ref _path);
        }

        partial void CheckSceneValid();
    }
}