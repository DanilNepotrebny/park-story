﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace DreamTeam.Utils
{
    public class CustomRandom
    {
        private readonly Random _random;

        public int Seed { get; }

        public CustomRandom([CanBeNull] IList<int> seeds = null)
        {
            if (seeds == null || seeds.Count == 0)
            {
                Seed = Guid.NewGuid().GetHashCode();
            }
            else
            {
                if (seeds.Count == 1)
                {
                    Seed = seeds.First();
                }
                else
                {
                    var tmpRnd = new Random();
                    Seed = seeds[tmpRnd.Next(seeds.Count)];
                }
            }

            _random = new Random(Seed);
        }

        /// <summary> Returns a non-negative random integer. </summary>
        /// <returns> A 32-bit signed integer that is greater than or equal to 0 and less than int.MaxValue. </returns>
        public int Next()
        {
            return _random.Next();
        }

        /// <summary> Returns a non-negative random integer that is less than the specified maximum. </summary>
        /// <returns> A 32-bit signed integer that is greater than or equal to 0 and less than MaxValue. </returns>
        public int Next(int maxValue)
        {
            return _random.Next(maxValue);
        }

        /// <summary> Returns a random integer that is within a specified range. </summary>
        /// <returns> A 32-bit signed integer that is greater than or equal to 0 and less than MaxValue. </returns>
        public int Next(int minValue, int maxValue)
        {
            return _random.Next(minValue, maxValue);
        }

        /// <summary> Fills the elements of a specified array of bytes with random numbers. </summary>
        public void NextBytes(byte[] buffer)
        {
            _random.NextBytes(buffer);
        }

        /// <summary> Returns a random floating-point number that is greater than or equal to 0.0, and less than 1.0. </summary>
        public double NextDouble()
        {
            return _random.NextDouble();
        }
    }
}
