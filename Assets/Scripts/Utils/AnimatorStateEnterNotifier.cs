﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Trigger for animator state machine to work in conjunction with <see cref="IAnimatorStateEnterHandler"/>. Put this trigger on a state to send state enter event
    /// to all <see cref="IAnimatorStateEnterHandler"/> on the same game object where <see cref="Animator"/> is assigned.
    /// </summary>
    [SharedBetweenAnimators]
    public class AnimatorStateEnterNotifier : StateMachineBehaviour
    {
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);

            var handlers = animator.GetComponents<IAnimatorStateEnterHandler>();
            foreach (var handler in handlers)
            {
                handler.OnStateEnter(animator, stateInfo, layerIndex);
            }
        }
    }
}