﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.IO;

namespace DreamTeam.Utils.FileFolders
{
    public interface IFileFolder : IDisposable
    {
        void PushPath(string path);
        void PopPath();

        bool Exists(string path);
        Stream OpenRead(string path);
        Stream OpenWrite(string path);
        void Delete(string path);
    }
}