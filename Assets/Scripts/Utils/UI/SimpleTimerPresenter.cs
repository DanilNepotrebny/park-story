﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Utils.UI
{
    public abstract class SimpleTimerPresenter : MonoBehaviour
    {
        [SerializeField, Tooltip("{0} - time")]
        private string _textFormat = "{0}";

        [Header("Display time format:")]
        [SerializeField, Tooltip("Time format if days > 0")]
        private string _daysTimeFormat = @"dd\:hh\:mm\:ss\:ffff";

        [SerializeField, Tooltip("Time format if hours > 0")]
        private string _hoursTimeFormat = @"hh\:mm\:ss\:ffff";

        [SerializeField, Tooltip("Time format if minutes > 0")]
        private string _minutesTimeFormat = @"mm\:ss\:ffff";

        [SerializeField, Tooltip("Time format if seconds > 0")]
        private string _secondsTimeFormat = @"ss\:ffff";

        [SerializeField, Tooltip("Default time format")]
        private string _defaultTimeFormat = @"ffff\ms";

        private Text _text;

        protected abstract TimeSpan DisplayTime { get; }

        private string _currentTimeFormat
        {
            get
            {
                if (DisplayTime.Days > 0)
                {
                    return _daysTimeFormat;
                }

                if (DisplayTime.Hours > 0)
                {
                    return _hoursTimeFormat;
                }

                if (DisplayTime.Minutes > 0)
                {
                    return _minutesTimeFormat;
                }

                if (DisplayTime.Seconds > 0)
                {
                    return _secondsTimeFormat;
                }

                return _defaultTimeFormat;
            }
        }

        protected void Awake()
        {
            _text = GetComponent<Text>();
        }

        protected void Update()
        {
            _text.text = string.Format(
                    _textFormat,
                    DisplayTime.ToString(_currentTimeFormat)
                );
        }
    }
}