﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Utils.UI
{
    public class ToggleStateSynchronizationGroup : MonoBehaviour
    {
        [SerializeField, RequiredField] private Toggle _mainToggle;
        [SerializeField, RequiredField] private TransformChildrenChangeNotifier[] _childrenNotifiers;

        private Toggle[] _handledToggles = new Toggle[0];
        private bool _defaultIsOn;

        protected void Awake()
        {
            _defaultIsOn = _mainToggle.isOn;
        }

        protected void OnEnable()
        {
            _mainToggle.onValueChanged.AddListener(SyncChildToggles);
            SubscribeChildrenNotifiers();
            _mainToggle.isOn = _defaultIsOn;
        }

        protected void OnDisable()
        {
            _mainToggle.onValueChanged.RemoveListener(SyncChildToggles);
            UnsubscribeChildrenNotifiers();
        }

        protected void OnTransformChildrenChanged()
        {
            SyncChildToggles(_mainToggle.isOn);
        }

        private void SubscribeChildrenNotifiers()
        {
            foreach (TransformChildrenChangeNotifier notifier in _childrenNotifiers)
            {
                notifier.TransformChildrenChanged += OnTransformChildrenChanged;
            }
        }

        private void UnsubscribeChildrenNotifiers()
        {
            foreach (TransformChildrenChangeNotifier notifier in _childrenNotifiers)
            {
                notifier.TransformChildrenChanged -= OnTransformChildrenChanged;
            }
        }

        private void SyncChildToggles(bool isOn)
        {
            Array.ForEach(_handledToggles, toggle => toggle.onValueChanged.RemoveListener(OnChildToggleChanged));
            _handledToggles = GetComponentsInChildren<Toggle>();

            foreach (Toggle toggle in _handledToggles)
            {
                toggle.isOn = isOn;
                toggle.onValueChanged.AddListener(OnChildToggleChanged);
            }
        }

        private void OnChildToggleChanged(bool isOn)
        {
            _mainToggle.onValueChanged.RemoveListener(SyncChildToggles);
            _mainToggle.isOn = Array.TrueForAll(_handledToggles, toggle => toggle.isOn);
            _mainToggle.onValueChanged.AddListener(SyncChildToggles);
        }
    }
}