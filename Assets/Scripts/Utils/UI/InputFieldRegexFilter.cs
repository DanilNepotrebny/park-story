﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Text.RegularExpressions;
using UnityEngine;

namespace DreamTeam.Utils.UI
{
    public class InputFieldRegexFilter : InputFieldValueValidator
    {
        [SerializeField] private string _matchPattern = string.Empty;

        protected override char OnValidateInput(string text, int charIndex, char addedChar)
        {
            string newText = text.Insert(charIndex, addedChar.ToString());
            return Regex.IsMatch(newText, _matchPattern) ? addedChar : '\0';
        }
    }
}