﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Utils.UI
{
    [RequireComponent(typeof(InputField))]
    public class InputFieldAutofocusSetter : MonoBehaviour
    {
        [Inject] private Popup _popup;

        protected void Awake()
        {
            _popup.Shown += OnPopupShown;
        }

        private void OnPopupShown(Popup popup)
        {
            _popup.Shown -= OnPopupShown;

            var field = GetComponent<InputField>();
            field.ActivateInputField();
        }
    }
}