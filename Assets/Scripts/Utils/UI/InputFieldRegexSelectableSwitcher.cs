﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Utils.UI
{
    [RequireComponent(typeof(InputField))]
    public class InputFieldRegexSelectableSwitcher : MonoBehaviour
    {
        [SerializeField] private string _matchPattern;
        [SerializeField] private Selectable[] _selectables;
        private InputField _inputField;

        public void SetMatchPattern(string pattern)
        {
            _matchPattern = pattern;
            UpdateSelectables(_inputField.text);
        }

        protected void Awake()
        {
            _inputField = GetComponent<InputField>();
        }

        protected void OnEnable()
        {
            _inputField.onValueChanged.AddListener(UpdateSelectables);
            UpdateSelectables(_inputField.text);
        }

        protected void OnDisable()
        {
            GetComponent<InputField>().onValueChanged.RemoveListener(UpdateSelectables);
        }

        private void UpdateSelectables(string newValue)
        {
            bool isInteractable = Regex.IsMatch(newValue, _matchPattern);

            foreach (Selectable item in _selectables)
            {
                if (item != null)
                {
                    item.interactable = isInteractable;
                }
            }
        }
    }
}