﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.UI
{
    public struct RectTransformData
    {
        private Vector3 _anchoredPosition;
        private Vector2 _anchorMin;
        private Vector2 _anchorMax;
        private Vector2 _pivot;
        private Vector2 _size;
        private Vector3 _localScale;
        private Quaternion _localRotation;

        public RectTransformData(RectTransform transform) :
            this(
                transform.anchoredPosition3D,
                transform.anchorMin,
                transform.anchorMax,
                transform.pivot,
                transform.rect.size,
                transform.localScale,
                transform.localRotation)
        {
        }

        private RectTransformData(
            Vector3 anchoredPosition,
            Vector2 anchorMin,
            Vector2 anchorMax,
            Vector2 pivot,
            Vector2 size,
            Vector3 localScale,
            Quaternion localRotation)
        {
            _anchoredPosition = anchoredPosition;
            _size = size;
            _anchorMin = anchorMin;
            _anchorMax = anchorMax;
            _pivot = pivot;
            _localScale = localScale;
            _localRotation = localRotation;
        }

        public void ApplyToTransform(RectTransform target)
        {
            target.localScale = _localScale;
            target.localRotation = _localRotation;
            target.anchoredPosition3D = _anchoredPosition;
            target.anchorMin = _anchorMin;
            target.anchorMax = _anchorMax;
            target.pivot = _pivot;
            target.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _size.x);
            target.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _size.y);
        }

        public static RectTransformData Lerp(RectTransformData from, RectTransformData to, float time)
        {
            return new RectTransformData(
                    Vector3.Lerp(from._anchoredPosition, to._anchoredPosition, time),
                    Vector2.Lerp(from._anchorMin, to._anchorMin, time),
                    Vector2.Lerp(from._anchorMax, to._anchorMax, time),
                    Vector2.Lerp(from._pivot, to._pivot, time),
                    Vector2.Lerp(from._size, to._size, time),
                    Vector3.Lerp(from._localScale, to._localScale, time),
                    Quaternion.Lerp(from._localRotation, to._localRotation, time)
                );
        }
    }
}