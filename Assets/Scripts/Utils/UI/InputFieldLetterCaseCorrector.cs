﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Globalization;
using UnityEngine;

namespace DreamTeam.Utils.UI
{
    public class InputFieldLetterCaseCorrector : InputFieldValueValidator
    {
        [SerializeField] private CaseCorrection _caseCorrection = CaseCorrection.None;

        protected override char OnValidateInput(string text, int charIndex, char addedChar)
        {
            string fullText = text.Insert(charIndex, addedChar.ToString());

            switch (_caseCorrection)
            {
                case CaseCorrection.ToLower:
                    fullText = CultureInfo.CurrentCulture.TextInfo.ToLower(fullText);
                    break;

                case CaseCorrection.ToUpper:
                    fullText = CultureInfo.CurrentCulture.TextInfo.ToUpper(fullText);
                    break;

                case CaseCorrection.ToTitleCase:
                    fullText = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(fullText);
                    break;
            }

            Field.text = fullText.Remove(charIndex, 1);
            return fullText[charIndex];
        }

        private enum CaseCorrection
        {
            None,
            ToLower,
            ToUpper,
            ToTitleCase
        }
    }
}