﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace DreamTeam.Utils.UI
{
    public class NotifyingText : Text, ITextPresenter
    {
        [SerializeField]
        private TextChangedEvent _textChangedEvent;

        public event Action TextChanged;

        public override string text
        {
            get { return base.text; }
            set
            {
                if (!base.text.Equals(value))
                {
                    base.text = value;
                    if (isActiveAndEnabled)
                    {
                        TextChanged?.Invoke();
                        _textChangedEvent?.Invoke();
                    }
                }
            }
        }

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        [Serializable]
        private class TextChangedEvent : UnityEvent
        {
        }
    }
}