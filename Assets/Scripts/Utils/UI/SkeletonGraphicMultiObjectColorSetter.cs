﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Spine.Unity;
using Spine.Unity.Modules;
using UnityEngine;

namespace DreamTeam.Utils.UI
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(SkeletonGraphicMultiObject))]
    public class SkeletonGraphicMultiObjectColorSetter : MonoBehaviour
    {
        [SerializeField] private Color _color = Color.white;

        private SkeletonGraphicMultiObject _skeleton;

        public Color SkeletonColor
        {
            get { return _color; }
            set { _color = value; }
        }

        protected void Awake()
        {
            _skeleton = GetComponent<SkeletonGraphicMultiObject>();
        }

        private void Update()
        {
            _skeleton?.Skeleton?.SetColor(SkeletonColor);
        }
    }
}
