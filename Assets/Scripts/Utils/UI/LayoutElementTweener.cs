﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Utils.UI
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(LayoutElement))]
    public class LayoutElementTweener : MonoBehaviour
    {
        [SerializeField] private Easing _easing;
        [SerializeField] private float _easingDuration = 1;
        [SerializeField, HideInInspector] private LayoutTweeningGroup _tweeningGroup;

        private bool _isInitialized;
        private RectTransformData _tweenFromData;
        private RectTransformData _tweenToData;
        private LTDescr _tweenDescr;

        public RectTransform TweenTarget { get; private set; }
        public LayoutElement LayoutElement => GetComponent<LayoutElement>();

        private RectTransform _thisRectTransform => transform as RectTransform;
        private LayoutGroup _layoutGroup => transform.parent?.GetComponent<LayoutGroup>();
        private bool _isTweening => _tweenDescr != null;

        protected void Awake()
        {
            FindTweeningGroup();
            AddToTweeningGroup();
        }

        protected void OnDestroy()
        {
            _tweeningGroup?.RemoveElement(this);
        }

        protected void OnEnable()
        {
            TweenTarget?.gameObject.SetActive(true);
        }

        protected void OnDisable()
        {
            BreakTweening();

            TweenTarget?.gameObject.SetActive(false);

            StopTweening();
        }

        protected void Reset()
        {
            FindTweeningGroup();
        }

        private void FindTweeningGroup()
        {
            if (_tweeningGroup == null)
            {
                _tweeningGroup = _layoutGroup?.GetComponent<LayoutTweeningGroup>();
                if (_tweeningGroup == null && _layoutGroup != null)
                {
                    _tweeningGroup = _layoutGroup.gameObject.AddComponent<LayoutTweeningGroup>();
                }
            }
        }

        private void AddToTweeningGroup()
        {
            if (_tweeningGroup != null)
            {
                RectTransformChangeNotifier transformNotifier =
                    _tweeningGroup.AddElement(this, transform.GetSiblingIndex());
                transformNotifier.RectangleChanged += OnTargetRectangleChanged;
                transformNotifier.PositionChanged += OnTargetPositionChanged;
                TweenTarget = transformNotifier.GetComponent<RectTransform>();
            }
        }

        private void OnTargetPositionChanged()
        {
            _tweenToData = new RectTransformData(TweenTarget);
            if (!_isTweening)
            {
                _tweenToData.ApplyToTransform(_thisRectTransform);
            }
        }

        private void OnTargetRectangleChanged()
        {
            if (_isInitialized)
            {
                StartTweening();
            }
            else
            {
                OnTargetPositionChanged();
                _isInitialized = true;
            }
        }

        private void StartTweening()
        {
            StopTweening();

            _tweenFromData = new RectTransformData(_thisRectTransform);
            _tweenToData = new RectTransformData(TweenTarget);

            _tweenDescr = LeanTween.value(0, 1, _easingDuration);
            _easing.SetupDescriptor(_tweenDescr);

            _tweenDescr.setOnUpdate(
                    (float ratio) =>
                    {
                        RectTransformData.Lerp(_tweenFromData, _tweenToData, ratio)
                            .ApplyToTransform(_thisRectTransform);
                    }
                );

            _tweenDescr.setOnComplete(
                    () =>
                    {
                        _tweenToData.ApplyToTransform(_thisRectTransform);
                        _tweenDescr = null;
                    }
                );
        }

        private void StopTweening()
        {
            if (_isTweening)
            {
                LeanTween.cancel(_tweenDescr.uniqueId);
                _tweenDescr = null;
            }
        }

        private void BreakTweening()
        {
            if (_isTweening)
            {
                StopTweening();
                _tweenToData.ApplyToTransform(_thisRectTransform);
            }
        }
    }
}