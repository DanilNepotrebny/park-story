﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Utils.UI
{
    [ExecuteInEditMode, DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    public class RectTransformChangeNotifier : MonoBehaviour
    {
        [SerializeField] private float _epsilon;

        private const float _defaultEpsilon = 0.01f;
        private RectTransform _rectTransform;

        private Vector2 _oldAnchoredPosition;
        private Vector2 _oldSizeDelta;
        private Vector2 _oldLocalPosition;
        private Vector2 _oldPosition;

        private bool _isAnchoredPositionChanged =>
            Vector2.Distance(_oldAnchoredPosition, _rectTransform.anchoredPosition) > _epsilon;

        private bool _isSizeChanged => Vector2.Distance(_oldSizeDelta, _rectTransform.sizeDelta) > _epsilon;

        private bool _isLocalPositionChanged =>
            Vector2.Distance(_oldLocalPosition, _rectTransform.localPosition) > _epsilon;

        private bool _isPositionChanged => Vector2.Distance(_oldPosition, _rectTransform.position) > _epsilon;

        public event Action RectangleChanged;
        public event Action LocalPositionChanged;
        public event Action PositionChanged;

        protected void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();

            var canvas = GetComponentInParent<Canvas>();
            _epsilon = canvas ? canvas.referencePixelsPerUnit * _defaultEpsilon : _defaultEpsilon;
        }

        protected void Start()
        {
            _oldAnchoredPosition = _rectTransform.anchoredPosition;
            _oldSizeDelta = _rectTransform.sizeDelta;
            _oldLocalPosition = _rectTransform.localPosition;
            _oldPosition = _rectTransform.position;
        }

        protected void Update()
        {
            if (transform.hasChanged)
            {
                if (_isAnchoredPositionChanged || _isSizeChanged)
                {
                    _oldAnchoredPosition = _rectTransform.anchoredPosition;
                    _oldSizeDelta = _rectTransform.sizeDelta;
                    RectangleChanged?.Invoke();
                }

                if (_isLocalPositionChanged)
                {
                    _oldLocalPosition = _rectTransform.localPosition;
                    LocalPositionChanged?.Invoke();
                }

                if (_isPositionChanged)
                {
                    _oldPosition = _rectTransform.position;
                    PositionChanged?.Invoke();
                }
            }
        }

        protected void OnValidate()
        {
            _epsilon = Mathf.Max(0, _epsilon);
        }
    }
}