﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Utils.UI
{
    public class TogglesStateSynchronizer : MonoBehaviour
    {
        [SerializeField, RequiredField] private Toggle[] _toggles;

        protected void Awake()
        {
            SubscribeToggles();
        }

        protected void OnDestroy()
        {
            UnsubscribeToggles();
        }

        private void SubscribeToggles()
        {
            foreach (Toggle toggle in _toggles)
            {
                toggle.onValueChanged.AddListener(OnToggleValueChanged);
            }
        }

        private void UnsubscribeToggles()
        {
            foreach (Toggle toggle in _toggles)
            {
                toggle.onValueChanged.RemoveListener(OnToggleValueChanged);
            }
        }

        private void OnToggleValueChanged(bool isOn)
        {
            UnsubscribeToggles();

            foreach (Toggle toggle in _toggles)
            {
                toggle.isOn = isOn;
            }

            SubscribeToggles();
        }
    }
}