﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Localization;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Utils.UI
{
    [RequireComponent(typeof(Text))]
    public class RandomTextPresenter : MonoBehaviour
    {
        [SerializeField] private TimeTicks _updateInterval;
        [SerializeField] private List<Item> _items;

        [Inject] private LocalizationSystem _localization;

        private float _nextUpdateTime;
        private List<KeyValuePair<string, float>> _pendingItems = new List<KeyValuePair<string, float>>();
        private Text _text;

        protected void Awake()
        {
            _text = GetComponent<Text>();
            FillPendingItems();
        }

        protected void Update()
        {
            if (Time.realtimeSinceStartup >= _nextUpdateTime)
            {
                _nextUpdateTime = Time.realtimeSinceStartup + ((TimeSpan)_updateInterval).Seconds;

                string text = GetNextLocalizationKey();
                if (!string.IsNullOrEmpty(text))
                {
                    _text.text = _localization.Localize(text, this);
                }
            }
        }

        private string GetNextLocalizationKey()
        {
            if (_pendingItems.Count == 0)
            {
                return string.Empty;
            }

            string result = RandomUtils.RandomElement(_pendingItems);
            _pendingItems.Remove(_pendingItems.Find(item => item.Key == result));
            return result;
        }

        private void FillPendingItems()
        {
            _pendingItems = _items.ConvertAll(item => new KeyValuePair<string, float>(item.Text, item.ShowWeight));
        }

        [Serializable]
        private class Item
        {
            [SerializeField, LocalizationKey] private string _text;
            [SerializeField] private float _showWeight = 1;

            public float ShowWeight => _showWeight;
            public string Text => _text;
        }
    }
}