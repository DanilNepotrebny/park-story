﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;

namespace DreamTeam.Utils.UI
{
    public interface ITextPresenter
    {
        string Text { get; set; }
        event Action TextChanged;
    }
}