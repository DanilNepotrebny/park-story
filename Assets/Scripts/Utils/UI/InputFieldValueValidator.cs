﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Utils.UI
{
    [RequireComponent(typeof(InputField)), DisallowMultipleComponent]
    public abstract class InputFieldValueValidator : MonoBehaviour
    {
        private InputField _field;

        protected InputField Field
        {
            get
            {
                if (_field == null)
                {
                    _field = GetComponent<InputField>();
                }

                return _field;
            }
        }

        protected void Awake()
        {
            Field.onValidateInput += OnValidateInput;
        }

        protected void OnValidate()
        {
            OnValidateInput(Field.text, 0, '\0');
        }

        protected abstract char OnValidateInput(string text, int charIndex, char addedChar);
    }
}