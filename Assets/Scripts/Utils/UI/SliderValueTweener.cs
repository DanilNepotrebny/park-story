﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Utils.UI
{
    [RequireComponent(typeof(Slider))]
    public class SliderValueTweener : MonoBehaviour
    {
        [SerializeField] private Easing _easing;
        [SerializeField] private float _easingDuration;

        private Slider _slider;
        private LTDescr _tween = null;
        private float _startValue;
        private bool _isValueChangedByThis = false;

        private bool _isTweening => _tween != null;
        private bool _isTweeningEnabled => enabled && _easing.IsEnabled;

        protected void Awake()
        {
            _slider = GetComponent<Slider>();
            _slider.onValueChanged.AddListener(OnValueChanged);
        }

        protected void OnDestroy()
        {
            _slider.onValueChanged.RemoveListener(OnValueChanged);
        }

        protected void OnEnable()
        {
            _startValue = _slider.value;
        }

        protected void OnDisable()
        {
            StopTweening();
        }

        private void StopTweening()
        {
            if (_isTweening)
            {
                LeanTween.cancel(_tween.uniqueId);
                _tween = null;
            }
        }

        private void StartTweening(float toValue)
        {
            _tween = LeanTween.value(_startValue, toValue, _easingDuration);
            _easing.SetupDescriptor(_tween);
            _tween.setOnUpdate(SetSliderValue);
        }

        private void OnValueChanged(float currentValue)
        {
            if (!_isTweeningEnabled || _isValueChangedByThis)
            {
                return;
            }

            StopTweening();
            StartTweening(currentValue);
        }

        private void SetSliderValue(float value)
        {
            _isValueChangedByThis = true;
            _slider.value = value;
            _startValue = value;
            _isValueChangedByThis = false;
        }
    }
}