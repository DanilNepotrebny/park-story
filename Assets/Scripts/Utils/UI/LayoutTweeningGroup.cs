﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Utils.UI
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(LayoutGroup))]
    public class LayoutTweeningGroup : MonoBehaviour
    {
        [SerializeField, ReadOnly] private RectTransform _elementsContainer;

        private LayoutGroup _layoutGroup => GetComponent<LayoutGroup>();
        private RectTransform _thisRectTransform => GetComponent<RectTransform>();

        public RectTransformChangeNotifier AddElement(LayoutElementTweener tweener, int siblingIndex)
        {
            RectTransformChangeNotifier result = CreateTweenTarget(tweener);
            tweener.transform.SetParent(_elementsContainer, false);
            tweener.transform.SetSiblingIndex(siblingIndex);
            return result;
        }

        public void RemoveElement(LayoutElementTweener tweener)
        {
            tweener?.TweenTarget?.gameObject.Dispose();
        }

        protected void Awake()
        {
            CreateElementsContainer();
        }

        protected void OnDestroy()
        {
            DestroyElementsContainer();
        }

        protected void LateUpdate()
        {
            if (_elementsContainer != null)
            {
                new RectTransformData(_thisRectTransform).ApplyToTransform(_elementsContainer);
            }
        }

        private void CreateElementsContainer()
        {
            if (_elementsContainer == null)
            {
                var containerGO = new GameObject($"{name} (Tweening container)", typeof(RectTransform));
                _elementsContainer = containerGO.GetComponent<RectTransform>();
                _elementsContainer.SetParent(transform.parent, false);
                _elementsContainer.SetSiblingIndex(transform.GetSiblingIndex() + 1);
            }
        }

        private void DestroyElementsContainer()
        {
            if (_elementsContainer != null)
            {
                LayoutElementTweener[] elements = _elementsContainer.GetComponentsInChildren<LayoutElementTweener>();
                foreach (LayoutElementTweener element in elements)
                {
                    element.transform.SetParent(_layoutGroup.transform, false);
                }

                _elementsContainer.gameObject.Dispose();
            }
        }

        private RectTransformChangeNotifier CreateTweenTarget(LayoutElementTweener tweener)
        {
            var tweenTargetGO = new GameObject(
                    $"{tweener.name} (TweenTarget)",
                    typeof(RectTransform),
                    typeof(LayoutElement),
                    typeof(RectTransformChangeNotifier)
                );

            tweenTargetGO.transform.SetParent(_layoutGroup.transform, false);
            tweenTargetGO.transform.SetSiblingIndex(tweener.transform.GetSiblingIndex());

            tweener.LayoutElement.CopyValuesTo(tweenTargetGO.GetComponent<LayoutElement>());
            return tweenTargetGO.GetComponent<RectTransformChangeNotifier>();
        }
    }
}