﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.UI
{
    [ExecuteInEditMode]
    public class ParentRectSizeToScaleAdapter : MonoBehaviour
    {
        [SerializeField] private Vector2 _referenceScale = Vector2.one;
        [SerializeField] private Vector2 _referenceSize;
        [SerializeField] private float _scaleFactor = 1;

        private RectTransform _parentRect => transform.parent as RectTransform;

        protected void Reset()
        {
            if (_parentRect != null)
            {
                _referenceSize = _parentRect.rect.size;
            }
        }

        protected void OnValidate()
        {
            _scaleFactor = Mathf.Max(_scaleFactor, 0);
        }

        protected void Update()
        {
            if (_parentRect != null)
            {
                Vector2 size = _parentRect.rect.size;
                transform.localScale = new Vector3(
                        _referenceScale.x * size.x / _referenceSize.x * _scaleFactor,
                        _referenceScale.y * size.y / _referenceSize.y * _scaleFactor,
                        1
                    );
            }
        }
    }
}