﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Utils.UI
{
    [RequireComponent(typeof(Toggle))]
    [RequireComponent(typeof(Animator))]
    public class ToggleStateAnimatorSetter : MonoBehaviour
    {
        [SerializeField] private string _isOnParameterName;

        private Animator _animator;

        protected void Awake()
        {
            _animator = GetComponent<Animator>();
            GetComponent<Toggle>().onValueChanged.AddListener(OnToggleValueChanged);
        }

        protected void OnEnable()
        {
            OnToggleValueChanged(GetComponent<Toggle>().isOn);
        }

        private void OnToggleValueChanged(bool isOn)
        {
            _animator.SetBool(_isOnParameterName, isOn);
        }
    }
}