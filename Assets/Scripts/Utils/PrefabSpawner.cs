﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using Zenject;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Workaround for nesting prefabs inside other prefab. This component instantiate prefab in Awake() and makes instantiated object to be own child.
    /// </summary>
    public class PrefabSpawner : MonoBehaviour, IInitable
    {
        [SerializeField] private GameObject _prefab;

        [Inject] private Instantiator _instantiator;

        protected virtual bool IsSpawnEnabled => true;

        public void Init()
        {
            if (!IsSpawnEnabled)
            {
                return;
            }

            if (_prefab == null)
            {
                throw new NullReferenceException("Prefab for PrefabSpawner is not specified");
            }

            SpawnPrefab();
        }

        private void SpawnPrefab()
        {
            var obj = _instantiator.Instantiate(_prefab, transform.position, gameObject);
            obj.hideFlags |= HideFlags.DontSaveInEditor;
        }
    }
}