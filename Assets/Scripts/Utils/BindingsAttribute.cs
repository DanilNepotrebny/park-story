﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using JetBrains.Annotations;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Used to mark private static method with internal bindings of your class. Method has to have signature as follows:
    /// private static IEnumerable&lt;ConditionCopyNonLazyBinder&gt; GetBindings(DiContainer container)
    /// Use <see cref="ExtensionMethods.BindInternals{TContract}"/> in your installer to bind all such methods.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method), MeansImplicitUse]
    public class BindingsAttribute : Attribute
    {
    }
}