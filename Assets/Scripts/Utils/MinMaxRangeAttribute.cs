﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Use a MinMaxRange class to replace twin float range values (eg: float minSpeed, maxSpeed; becomes MinMaxRange speed)
    /// Apply a [MinMaxRange( minLimit, maxLimit )] attribute to a MinMaxRange instance to control the limits and to show a
    /// slider in the inspector
    /// </summary>
    public class MinMaxRangeAttribute : PropertyAttribute
    {
        public float minLimit, maxLimit;

        public MinMaxRangeAttribute(float minLimit, float maxLimit)
        {
            this.minLimit = minLimit;
            this.maxLimit = maxLimit;
        }
    }

    [Serializable]
    public class MinMaxRange
    {
        public float rangeStart, rangeEnd;

        public float GetRandomValue(CustomRandom randomSource = null)
        {
            if (randomSource != null)
            {
                return randomSource.Next(
                        (int)(rangeStart * 100),
                        (int)(rangeEnd * 100)) /
                    100.0f;
            }
            else
            {
                return Random.Range(rangeStart, rangeEnd);
            }
        }
    }
}