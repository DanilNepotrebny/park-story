﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Utils
{
    [Serializable]
    public struct TimeTicks
    {
        [SerializeField] private long _ticks;

        public long Ticks => _ticks;

        private TimeTicks(long ticks)
        {
            _ticks = ticks;
        }

        public static implicit operator TimeTicks(TimeSpan from)
        {
            return new TimeTicks(from.Ticks);
        }

        public static implicit operator TimeSpan(TimeTicks from)
        {
            return TimeSpan.FromTicks(from._ticks);
        }
    }
}