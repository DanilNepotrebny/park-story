﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using DreamTeam.SceneLoading;
using JetBrains.Annotations;
using ModestTree;
using TouchScript;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;
using Assert = UnityEngine.Assertions.Assert;
using Object = UnityEngine.Object;
using Pointer = TouchScript.Pointers.Pointer;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace DreamTeam.Utils
{
    public static class ExtensionMethods
    {
        public static void Fill<T>(this IList<T> array, T value)
        {
            int length = array.Count;
            for (int i = 0; i < length; ++i)
            {
                array[i] = value;
            }
        }

        public static void Fill<T>(this IList<T> array, T value, int fromIdx, int toIdx)
        {
            for (int i = fromIdx; i < toIdx; ++i)
            {
                array[i] = value;
            }
        }

        public static bool Intersects(this RectInt self, RectInt other)
        {
            return self.x < other.xMax &&
                other.x < self.xMax &&
                self.y < other.yMax &&
                other.y < self.yMax;
        }

        /// <summary>
        /// If component of type <typeparamref name="T"/> doesn't exist it will be created and <paramref name="initializer"/> will be invoked for it.
        /// If component does already exist than it will be returned with no additional actions.
        /// </summary>
        public static T EnsureComponentExists<T>(this GameObject self, Action<T> initializer) where T : MonoBehaviour
        {
            T component = self.GetComponent<T>();
            if (component == null)
            {
                component = self.AddComponent<T>();
                initializer?.Invoke(component);
            }

            return component;
        }

        /// <summary>
        /// Returns full path between specified component and it's root or another parent in the hierarchy.
        /// Full path is a list of game object names separated with '/'.
        /// You can use this path in <see cref="Transform.Find"/>.
        /// </summary>
        public static string GetFullPath([NotNull] this Transform self, Transform root = null, int skipParents = 0)
        {
            var stack = new Stack<string>();
            Transform current = self;

            while (current != root && current != null)
            {
                stack.Push(current.name);
                current = current.parent;
            }

            if (current != root)
            {
                throw new ArgumentException($"There is no path between {self.name} and {root.name}", nameof(root));
            }

            while (skipParents > 0)
            {
                skipParents--;
                stack.Pop();
            }

            return string.Join("/", stack);
        }

        /// <summary>
        /// Extended <see cref="Transform.Find"/> method. Finds a child transform by name recoursively. Supports
        /// '/' delimited paths (first child in a chain will be found recoursively).
        /// </summary>
        public static Transform FindInChildren(this Transform self, [NotNull] string path)
        {
            int idx = path.IndexOf("/");

            string name = idx < 0 ? path : path.Substring(0, idx);

            Transform t = FindChildRecoursively(self, name);
            if (t == null)
            {
                return null;
            }

            return idx < 0 ? t : t.Find(path.Substring(idx + 1));
        }

        /// <summary>
        /// Extends Transform.SetParent(.) method. Set local scale that was before parenting.
        /// </summary>
        public static void SetParentPreserveLocalScale(
            this Transform self,
            Transform parent,
            bool worldPositionStays = true)
        {
            Vector3 prevLocalScale = self.localScale;
            self.SetParent(parent, worldPositionStays);
            self.localScale = prevLocalScale;
        }

        /// <summary>
        /// Extends Transform.SetParent(.) method. Calculates local scale to match world scale that was before
        /// </summary>
        public static void SetParentPreserveWorldScale(
            this Transform self,
            Transform parent,
            bool worldPositionStays = true)
        {
            Vector3 prevWorldScale = self.lossyScale;
            self.SetParent(parent, worldPositionStays);
            Vector3 worldScale = self.lossyScale;

            self.localScale = new Vector3(
                prevWorldScale.x / worldScale.x,
                prevWorldScale.y / worldScale.y,
                prevWorldScale.z / worldScale.z);
        }

        public static T GetOrAddComponent<T>(this Component self) where T : Component
        {
            T comp = self.GetComponent<T>() ?? self.gameObject.AddComponent<T>();
            return comp;
        }

        public static T GetComponentInParent<T>(this Component self, bool includeInactive)
        {
            if (!includeInactive)
            {
                return self.GetComponentInParent<T>();
            }

            T[] components = self.GetComponentsInParent<T>(true);
            if (components.Length > 0)
            {
                return components[0];
            }

            return default(T);
        }

        /// <summary>
        /// See <see cref="FromBinderGeneric{TContract}.FromComponentInParents"/>. This extension adds <paramref name="includeInactive" /> parameter.
        /// </summary>
        public static ScopeArgConditionCopyNonLazyBinder FromComponentInParents<TContract>(
            this FromBinderGeneric<TContract> self,
            bool excludeSelf = false,
            bool includeInactive = false,
            bool useFirst = false)
        {
            Func<InjectContext, IEnumerable<TContract>> method = ctx =>
            {
                Assert.IsTrue(ctx.ObjectType.DerivesFromOrEqual<MonoBehaviour>());
                Assert.IsNotNull(ctx.ObjectInstance);

                IEnumerable<TContract> res = ((MonoBehaviour)ctx.ObjectInstance)
                    .GetComponentsInParent<TContract>(includeInactive)
                    .Where(x => !ReferenceEquals(x, ctx.ObjectInstance));

                if (excludeSelf)
                {
                    res = res.Where(x => (x as Component).gameObject != (ctx.ObjectInstance as Component).gameObject);
                }

                return res;
            };

            return useFirst ?
                self.FromMethod(ctx => method.Invoke(ctx).First()) :
                self.FromMethodMultiple(method);
        }

        public static IdScopeConditionCopyNonLazyBinder BindComponent<T>(this DiContainer self, MonoInstaller installer)
            where T : Component
        {
            return self.BindInstance(installer.GetComponent<T>());
        }

        /// <summary>
        /// Performs internal bindings of the specified class: finds and invokes all private static methods marked with <see cref="BindingsAttribute"/> and
        /// does <see cref="ConditionCopyNonLazyBinder.WhenInjectedInto"/> for each internal binding those methods return.
        /// </summary>
        public static void BindInternals<TContract>(this DiContainer self)
        {
            Type type = typeof(TContract);
            IEnumerable<MethodInfo> methods = type
                .GetMethods(BindingFlags.Static | BindingFlags.NonPublic)
                .Where(method => method.HasAttribute<BindingsAttribute>());

            if (methods.IsEmpty())
            {
                UnityEngine.Debug.LogError($"No methods in {type.Name} with {nameof(BindingsAttribute)}.");
            }

            ;

            foreach (MethodInfo method in methods)
            {
                var bindings = method.Invoke(null, new object[] { self }) as IEnumerable<ConditionCopyNonLazyBinder>;
                if (bindings != null)
                {
                    foreach (ConditionCopyNonLazyBinder binding in bindings)
                    {
                        binding.WhenInjectedInto<TContract>();
                    }
                }
                else
                {
                    UnityEngine.Debug.LogError(
                        $"Incorrect return value type in bindings method {type.Name}.{method.Name}.");
                }
            }
        }

        /// <summary>
        /// Linear interpolation with easing specified by curve. Also see <see cref="Mathf.Lerp"/>.
        /// </summary>
        public static float Lerp(this AnimationCurve self, float a, float b, float t)
        {
            return Mathf.Lerp(a, b, self.Evaluate(t));
        }

        /// <summary>
        /// Linear interpolation with easing specified by curve. Also see <see cref="Vector3.Lerp"/>.
        /// </summary>
        public static Vector3 Lerp(this AnimationCurve self, Vector3 a, Vector3 b, float t)
        {
            return Vector3.Lerp(a, b, self.Evaluate(t));
        }

        /// <summary>
        /// Returns a value by specified key or default value if there is no such key in the dictionary.
        /// </summary>
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> self, TKey key)
        {
            TValue value;
            self.TryGetValue(key, out value);
            return value;
        }

        /// <summary>
        /// Shuffles the list
        /// </summary>
        public static void RandomShuffle<T>(this IList<T> list, CustomRandom _randomSource = null)
        {
            for (int i = 0; i < list.Count; i++)
            {
                int index = _randomSource?.Next(i, list.Count) ?? Random.Range(i, list.Count);
                list.SwapElements(i, index);
            }
        }

        /// <summary>
        /// Swaps two elements of the list
        /// </summary>
        public static void SwapElements<T>(
                this IList<T> list,
                int index1,
                int index2
            )
        {
            T tmp = list[index1];
            list[index1] = list[index2];
            list[index2] = tmp;
        }

        /// <summary>
        /// Shuffles the linked list
        /// </summary>
        public static void RandomShuffle<T>(this LinkedList<T> list, CustomRandom _randomSource = null)
        {
            for (int i = 0; i < list.Count; i++)
            {
                int index = _randomSource?.Next(i, list.Count) ?? Random.Range(i, list.Count);
                list.SwapElements(i, index);
            }
        }

        /// <summary>
        /// Swaps two elements of the linked list
        /// </summary>
        public static void SwapElements<T>(
                this LinkedList<T> list,
                int index1,
                int index2
            )
        {
            if (index1 == index2)
            {
                return;
            }

            LinkedListNode<T> first = list.GetNodeByIndex(index1);
            LinkedListNode<T> second = list.GetNodeByIndex(index2);

            LinkedListNode<T> prevFirst = first.Previous;
            if (prevFirst == second)
            {
                prevFirst = second.Previous;
            }

            LinkedListNode<T> prevSecond = second.Previous;
            if (prevSecond == first)
            {
                prevSecond = first.Previous;
            }

            list.Remove(first);
            list.Remove(second);

            if (prevFirst != null)
            {
                list.AddAfter(prevFirst, second);
            }
            else
            {
                list.AddFirst(second);
            }

            if (prevSecond != null)
            {
                list.AddAfter(prevSecond, first);
            }
            else
            {
                list.AddFirst(first);
            }
        }

        private static LinkedListNode<T> GetNodeByIndex<T>(this LinkedList<T> list, int index)
        {
            LinkedListNode<T> node = list.First;
            for (int i = 0; i < index; i++)
            {
                if (node == null)
                {
                    throw new IndexOutOfRangeException();
                }

                node = node.Next;
            }

            return node;
        }

        /// <summary>
        /// Shuffles the list
        /// </summary>
        public static void SwapElementTo<T>(
                this IList<T> list,
                int elementIndex,
                int indexTo
            )
        {
            Assert.IsTrue(elementIndex >= 0);
            Assert.IsTrue(indexTo >= 0);
            Assert.IsTrue(elementIndex < list.Count);
            Assert.IsTrue(elementIndex < list.Count);

            int minIdx = Mathf.Min(elementIndex, indexTo);
            int maxIdx = Mathf.Max(elementIndex, indexTo);

            for (int i = minIdx; i < maxIdx; i++)
            {
                list.SwapElements(i, i + 1);
            }
        }

        public static IEnumerable<int> GetRandomIndexEnumerable<T>(
            this IList<T> list,
            ListValueValidatorDelegate<T> validator = null,
            IList<int> indexerCache = null,
            CustomRandom randomSource = null)
        {
            if (indexerCache == null)
            {
                indexerCache = new List<int>(list.Count);
                for (int i = 0; i < list.Count; ++i)
                {
                    indexerCache.Add(i);
                }
            }

            if (list.Count != indexerCache.Count)
            {
                throw new ArgumentException($"Passed {nameof(indexerCache)} size is not equal to the {nameof(list)}");
            }

            indexerCache.RandomShuffle(randomSource);
            if (validator != null)
            {
                foreach (int index in indexerCache)
                {
                    if (validator.Invoke(list, index))
                    {
                        yield return index;
                    }
                }
            }
            else
            {
                foreach (int index in indexerCache)
                {
                    yield return index;
                }
            }
        }

        public delegate bool ListValueValidatorDelegate<T>(IList<T> list, int index);

        #if UNITY_EDITOR

        public static void ApplyModifiedPropertiesWithDirtyFlag(this SerializedObject self)
        {
            self.ApplyModifiedProperties();

            // SetDirty
            if (self.isEditingMultipleObjects)
            {
                foreach (var o in self.targetObjects)
                {
                    EditorUtility.SetDirty(o);
                }
            }
            else
            {
                EditorUtility.SetDirty(self.targetObject);
            }
        }

        #endif

        public static IEnumerator WaitState(this Animator self, string stateName, int layerIndex = 0)
        {
            while (!self.GetCurrentAnimatorStateInfo(layerIndex).IsName(stateName))
            {
                yield return null;
            }
        }

        public static bool InRange<T>(this T value, T min, T max, bool includeMin = true, bool includeMax = true)
            where T : IComparable<T>
        {
            int minRef = includeMin ? 0 : 1;
            int maxRef = includeMax ? 0 : -1;
            return value.CompareTo(min) >= minRef && value.CompareTo(max) <= maxRef;
        }

        public static double GetProgress(this Playable self)
        {
            return self.GetTime() / self.GetDuration();
        }

        public static List<T> GetComponentsInChildrenWhile<T>(
                this Transform node,
                Predicate<Transform> condition,
                bool includeInactive = false
            )
            where T : class
        {
            return GetComponentsInChildrenWhile(node, typeof(T), condition, includeInactive)
                .Select(c => c as T)
                .ToList();
        }

        public static List<object> GetComponentsInChildrenWhile(
                this Transform t,
                Type componentType,
                Predicate<Transform> condition,
                bool includeInactive = false
            )
        {
            List<object> result = new List<object>();

            Queue<Transform> queue = new Queue<Transform>();
            queue.Enqueue(t);

            while (queue.Count > 0)
            {
                Transform node = queue.Dequeue();
                if (node.gameObject.activeSelf || includeInactive)
                {
                    result.AddRange(node.GetComponents(componentType));
                }

                if (condition(node))
                {
                    for (int index = 0; index < node.childCount; index++)
                    {
                        queue.Enqueue(node.GetChild(index));
                    }
                }
            }

            return result;
        }

        public static void CopyValuesTo(this LayoutElement origin, LayoutElement target)
        {
            target.ignoreLayout = origin.ignoreLayout;
            target.layoutPriority = origin.layoutPriority;

            target.minWidth = origin.minWidth;
            target.minHeight = origin.minHeight;

            target.preferredWidth = origin.preferredWidth;
            target.preferredHeight = origin.preferredHeight;

            target.flexibleWidth = origin.flexibleWidth;
            target.flexibleHeight = origin.flexibleHeight;
        }

        public static Bounds Incapsulate(this Bounds thisBounds, Bounds bounds)
        {
            thisBounds.size = new Vector3(
                    Math.Min(thisBounds.size.x, bounds.size.x),
                    Math.Min(thisBounds.size.y, bounds.size.y),
                    Math.Min(thisBounds.size.z, bounds.size.z)
                );

            Vector3 min = new Vector3(
                    Mathf.Max(thisBounds.min.x, bounds.min.x),
                    Mathf.Max(thisBounds.min.y, bounds.min.y),
                    Mathf.Max(thisBounds.min.z, bounds.min.z)
                );

            Vector3 max = new Vector3(
                    Mathf.Min(min.x + thisBounds.size.x, bounds.max.x),
                    Mathf.Min(min.y + thisBounds.size.y, bounds.max.y),
                    Mathf.Min(min.z + thisBounds.size.z, bounds.max.z)
                );

            thisBounds.center += min - thisBounds.min;
            thisBounds.center += max - thisBounds.max;
            return thisBounds;
        }

        public static Rect Incapsulate(this Rect thisRect, Rect rect)
        {
            thisRect.size = new Vector3(
                    Math.Min(thisRect.size.x, rect.size.x),
                    Math.Min(thisRect.size.y, rect.size.y)
                );

            Vector2 min = new Vector3(
                    Mathf.Max(thisRect.min.x, rect.min.x),
                    Mathf.Max(thisRect.min.y, rect.min.y)
                );

            Vector2 max = new Vector3(
                    Mathf.Min(min.x + thisRect.size.x, rect.max.x),
                    Mathf.Min(min.y + thisRect.size.y, rect.max.y)
                );

            thisRect.center += min - thisRect.min;
            thisRect.center += max - thisRect.max;
            return thisRect;
        }

        public static Rect Encapsulate(this Rect thisRect, Rect other)
        {
            thisRect.xMin = Mathf.Min(thisRect.xMin, other.xMin);
            thisRect.xMax = Mathf.Max(thisRect.xMax, other.xMax);
            thisRect.yMin = Mathf.Min(thisRect.yMin, other.yMin);
            thisRect.yMax = Mathf.Max(thisRect.yMax, other.yMax);

            return thisRect;
        }

        public static bool Overlaps(this Rect rect, Rect other, int accuracy)
        {
            return
                Math.Round((double)other.xMax, accuracy) > Math.Round((double)rect.xMin, accuracy) &&
                Math.Round((double)other.xMin, accuracy) < Math.Round((double)rect.xMax, accuracy) &&
                Math.Round((double)other.yMax, accuracy) > Math.Round((double)rect.yMin, accuracy) &&
                Math.Round((double)other.yMin, accuracy) < Math.Round((double)rect.yMax, accuracy);
        }

        public static void StretchToParrent(this RectTransform rectTransform)
        {
            rectTransform.pivot = new Vector2(0.5f, 0.5f);
            rectTransform.anchorMin = new Vector2(0, 0);
            rectTransform.anchorMax = new Vector2(1, 1);
            rectTransform.offsetMin = Vector2.zero;
            rectTransform.offsetMax = Vector2.zero;
        }

        public static void SetAnchorsToCenter(this RectTransform rectTransform)
        {
            rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
            rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
        }

        /// <summary> Writes parameter to string buffer in format "name: value\n". </summary>
        public static void AppendParameter(this StringBuilder self, string name, string value)
        {
            self.Append(name);
            self.Append(": ");
            self.AppendLine(value);
        }

        public static void ReplaceGroup(this StringBuilder self, Group group, string text)
        {
            int index = group.Index;

            self.Remove(index, group.Value.Length);
            self.Insert(index, text);
        }

        public static IEnumerable<string> SplitBy(this string str, int chunkLength)
        {
            if (string.IsNullOrEmpty(str) || chunkLength < 1)
            {
                throw new ArgumentException();
            }

            for (int i = 0; i < str.Length; i += chunkLength)
            {
                if (chunkLength + i > str.Length)
                {
                    chunkLength = str.Length - i;
                }

                yield return str.Substring(i, chunkLength);
            }
        }

        public static List<T> GetComponentsOnScene<T>(this Scene scene)
        {
            List<T> result = new List<T>();

            GameObject[] rootObjects = scene.GetRootGameObjects();
            foreach (GameObject gameObject in rootObjects)
            {
                result.AddRange(gameObject.GetComponentsInChildren<T>(true));
            }

            return result;
        }

        public static SceneRoot FindSceneRoot(this Scene scene, SceneRootType rootType = SceneRootType.Main)
        {
            SceneRoot result = null;

            if (scene.IsValid() && scene.isLoaded)
            {
                result = scene
                    .GetComponentsOnScene<SceneRoot>()
                    .FirstOrDefault(root => root.RootType == rootType);
            }

            return result;
        }

        public static Bounds GetWorldBounds(this RectTransform transform)
        {
            var corners = new Vector3[4];
            transform.GetWorldCorners(corners);
            var result = new Bounds(transform.position, Vector3.zero);

            foreach (Vector3 corner in corners)
            {
                result.Encapsulate(corner);
            }

            return result;
        }

        public static Rect ToRect(this Bounds bounds)
        {
            return new Rect(bounds.min, bounds.size);
        }

        public static Rect GetWorldViewportRect(this Camera camera, float cameraToRectDistance)
        {
            Vector2 min = camera.ViewportToWorldPoint(new Vector3(0, 0, cameraToRectDistance));
            Vector2 max = camera.ViewportToWorldPoint(new Vector3(1, 1, cameraToRectDistance));
            return Rect.MinMaxRect(min.x, min.y, max.x, max.y);
        }

        public static Vector2 ClosestPointInDirection(this Rect rect, Vector2 direction)
        {
            direction.Normalize();

            float radius = Vector2.Distance(rect.min, rect.max) / 2;
            Vector2 pointOnSphere = rect.center + direction * radius;
            Vector2 clampedPointOnSphere = new Vector2(
                    Mathf.Clamp(pointOnSphere.x, rect.xMin, rect.xMax),
                    Mathf.Clamp(pointOnSphere.y, rect.yMin, rect.yMax)
                );

            Vector2 clampedDirection = (pointOnSphere - clampedPointOnSphere).normalized;
            Vector2 cathetusPoint =
                clampedDirection != Vector2.zero ? (pointOnSphere - rect.center) * clampedDirection : pointOnSphere;

            float cathetus = Vector2.Distance(Vector2.zero, cathetusPoint);
            float clampedCathetus = cathetus - Vector2.Distance(pointOnSphere, clampedPointOnSphere);
            float clampedRadius = clampedCathetus * radius / cathetus;

            return rect.center + direction * clampedRadius;
        }

        public static Rect Translate(this Rect rect, Vector2 translation)
        {
            rect.center += translation;
            return rect;
        }

        public static Rect WithCenter(this Rect rect, Vector2 center)
        {
            rect.center = center;
            return rect;
        }

        public static float GetDisplacementDistance(this Rect rect, Rect other, Vector2 direction)
        {
            Rect displacementRect = Rect.zero;

            if (rect.Contains(other) || other.Contains(rect))
            {
                Rect inside = rect.Contains(other) ? other : rect;
                Rect outside = rect.Contains(other) ? rect : other;

                var outsideCorner = new Vector2(
                        Mathf.Sign(direction.x) < 0 ? outside.xMax : outside.xMin,
                        Mathf.Sign(direction.y) < 0 ? outside.yMax : outside.yMin
                    );

                var insideCorner = new Vector2(
                        Mathf.Sign(direction.x) > 0 ? inside.xMax : inside.xMin,
                        Mathf.Sign(direction.y) > 0 ? inside.yMax : inside.yMin
                    );

                displacementRect = Rect.MinMaxRect(
                        Mathf.Min(insideCorner.x, outsideCorner.x),
                        Mathf.Min(insideCorner.y, outsideCorner.y),
                        Mathf.Max(insideCorner.x, outsideCorner.x),
                        Mathf.Max(insideCorner.y, outsideCorner.y)
                    );
            }
            else if (rect.Overlaps(other))
            {
                displacementRect = Rect.MinMaxRect(
                        Mathf.Max(rect.xMin, other.xMin),
                        Mathf.Max(rect.yMin, other.yMin),
                        Mathf.Min(rect.xMax, other.xMax),
                        Mathf.Min(rect.yMax, other.yMax)
                    );
            }

            Vector2 pointInDirection = displacementRect.ClosestPointInDirection(direction);
            return Vector2.Distance(displacementRect.center, pointInDirection) * 2;
        }

        public static bool IsSnappedTo(this Rect rect, Rect other, float accuracy)
        {
            return
                Mathf.Abs(rect.xMin - other.xMax) <= accuracy ||
                Mathf.Abs(rect.yMin - other.yMax) <= accuracy ||
                Mathf.Abs(rect.xMax - other.xMin) <= accuracy ||
                Mathf.Abs(rect.yMax - other.yMin) <= accuracy;
        }

        public static bool Contains(this Rect rect, Rect other)
        {
            return other.GetCorners().All(rect.Contains);
        }

        /// <summary>Returns an array of rectangle corners in clockwise order starting from bottom-left corner.</summary>>
        public static Vector2[] GetCorners(this Rect rect)
        {
            return new[]
            {
                new Vector2(rect.xMin, rect.yMin),
                new Vector2(rect.xMax, rect.yMin),
                new Vector2(rect.xMax, rect.yMax),
                new Vector2(rect.xMin, rect.yMax)
            };
        }

        public static bool IsPrefab(this GameObject gameObject)
        {
            return gameObject.scene.Equals(default(Scene));
        }

        private static Transform FindChildRecoursively(Transform self, string name)
        {
            Transform result = self.Find(name);
            if (result != null)
            {
                return result;
            }

            foreach (Transform child in self)
            {
                result = FindChildRecoursively(child, name);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        // Hack that cancels pointers and gives them directly to the exclusive transform
        // if other exclusive transform is not set
        public static void TryCancelGesture(this Gesture self, Transform exclusive)
        {
            if (!LayerManager.Instance.HasExclusive)
            {
                LayerManager.Instance.SetExclusive(exclusive);

                foreach (Pointer p in self.ActivePointers)
                {
                    TouchManager.Instance.CancelPointer(p.Id, true);
                }

                LayerManager.Instance.ClearExclusive();
            }
        }

        /// <summary> Extract translation from transform matrix. </summary>
        /// <param name="matrix"> Transform matrix. This parameter is passed by reference
        /// to improve performance; no changes will be made to it. </param>
        /// <returns> Translation offset. </returns>
        public static Vector3 ExtractTranslationFromMatrix(this Matrix4x4 matrix)
        {
            return matrix.GetColumn(3);
        }

        public static void Dispose(this Object obj)
        {
            if (Application.isPlaying)
            {
                GameObject go = obj as GameObject;
                if (go != null)
                {
                    ObjectLifetimeHandler.DisposeObjectComponents(go);
                }
                else
                {
                    (obj as IDisposable)?.Dispose();
                }

                Object.Destroy(obj);
            }
            else
            {
                Object.DestroyImmediate(obj);
            }
        }

        public static void DisposeImmediate(this Object obj)
        {
            if (Application.isPlaying)
            {
                GameObject go = obj as GameObject;
                if (go != null)
                {
                    ObjectLifetimeHandler.DisposeObjectComponents(go);
                }
                else
                {
                    (obj as IDisposable)?.Dispose();
                }
            }

            Object.DestroyImmediate(obj);
        }

        public static void CopyValuesTo(this AudioSource origin, AudioSource target)
        {
            if (target != null)
            {
                target.clip = origin.clip;
                target.loop = origin.loop;
                target.mute = origin.mute;
                target.pitch = origin.pitch;
                target.priority = origin.priority;
                target.spatialize = origin.spatialize;
                target.spread = origin.spread;
                target.time = origin.time;
                target.volume = origin.volume;
                target.bypassEffects = origin.bypassEffects;
                target.dopplerLevel = origin.dopplerLevel;
                target.maxDistance = origin.maxDistance;
                target.minDistance = origin.minDistance;
                target.panStereo = origin.panStereo;
                target.rolloffMode = origin.rolloffMode;
                target.spatialBlend = origin.spatialBlend;
                target.timeSamples = origin.timeSamples;
                target.bypassListenerEffects = origin.bypassListenerEffects;
                target.bypassReverbZones = origin.bypassReverbZones;
                target.ignoreListenerPause = origin.ignoreListenerPause;
                target.ignoreListenerVolume = origin.ignoreListenerVolume;
                target.playOnAwake = origin.playOnAwake;
                target.reverbZoneMix = origin.reverbZoneMix;
                target.spatializePostEffects = origin.spatializePostEffects;
                target.velocityUpdateMode = origin.velocityUpdateMode;
                target.outputAudioMixerGroup = origin.outputAudioMixerGroup;
            }
        }
    }
}