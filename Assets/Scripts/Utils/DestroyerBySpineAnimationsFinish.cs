﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Spine;
using UnityEngine;

namespace DreamTeam.Utils
{
    [RequireComponent(typeof(SpineAnimationsFinishNotifier))]
    public class DestroyerBySpineAnimationsFinish : MonoBehaviour
    {
        protected void Start()
        {
            SpineAnimationsFinishNotifier notifier = GetComponent<SpineAnimationsFinishNotifier>();
            notifier.WaitForAnimationsFinish(
                    () => { gameObject.Dispose(); }
                );
        }
    }
}