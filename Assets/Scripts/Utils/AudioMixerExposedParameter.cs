﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using UnityEngine;
using UnityEngine.Audio;

namespace DreamTeam.Utils
{
    [CreateAssetMenu]
    public class AudioMixerExposedParameter : ScriptableObject
    {
        [SerializeField, RequiredField] private AudioMixer _audioMixer;
        [SerializeField, RequiredField] private string _exposedParameterName;
        [SerializeField] private float _minAbsoluteValue;
        [SerializeField] private float _maxAbsoluteValue;

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public float NormalizedValue
        {
            get { return (AbsoluteValue - _minAbsoluteValue) / _range; }
            set
            {
                value = Mathf.Clamp01(value);
                AbsoluteValue = _minAbsoluteValue + _range * value;
            }
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public float AbsoluteValue
        {
            get
            {
                float result;
                _audioMixer.GetFloat(_exposedParameterName, out result);
                return result;
            }
            set
            {
                value = Mathf.Clamp(value, _minAbsoluteValue, _maxAbsoluteValue);
                _audioMixer.SetFloat(_exposedParameterName, value);
            }
        }

        public string ExposedParameterName => _exposedParameterName;
        private float _range => _maxAbsoluteValue - _minAbsoluteValue;

        protected void OnValidate()
        {
            float temp = Mathf.Min(_minAbsoluteValue, _maxAbsoluteValue);
            _maxAbsoluteValue = Mathf.Max(_minAbsoluteValue, _maxAbsoluteValue);
            _minAbsoluteValue = temp;
        }
    }
}