﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Trigger for animator state machine to work in conjunction with <see cref="IAnimatorStateExitHandler"/>. Put this trigger on a state to send state exit event
    /// to all <see cref="IAnimatorStateExitHandler"/> on the same game object where <see cref="Animator"/> is assigned.
    /// </summary>
    [SharedBetweenAnimators]
    public class AnimatorStateExitNotifier : StateMachineBehaviour
    {
        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateExit(animator, stateInfo, layerIndex);

            var handlers = animator.GetComponents<IAnimatorStateExitHandler>();
            foreach (var handler in handlers)
            {
                handler.OnStateExit(animator, stateInfo, layerIndex);
            }
        }
    }
}