﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Utils.Instance
{
    public class ScriptableInstanceContainer<TBaseInstance> : MonoBehaviour
        where TBaseInstance : class
    {
        [Inject] private Instantiator _instantiator;

        private Dictionary<IInstanceFactory<TBaseInstance>, TBaseInstance> _instances =
            new Dictionary<IInstanceFactory<TBaseInstance>, TBaseInstance>();

        public TInstance GetInstance<TInstance>(IInstanceFactory<TInstance> scriptableObject)
            where TInstance : class, TBaseInstance
        {
            Assert.IsTrue(scriptableObject is ScriptableObject, "Non scriptable objects are not supported");

            TBaseInstance baseInstance;
            if (!_instances.TryGetValue(scriptableObject, out baseInstance))
            {
                baseInstance = CreateInstance(scriptableObject);
            }

            if (baseInstance == null)
            {
                throw new InvalidOperationException(
                        $"Cached instance for the provider '{scriptableObject.GetType().Name}' became null"
                    );
            }

            TInstance instance = baseInstance as TInstance;         
            Assert.IsNotNull(
                    instance,
                    $"Invalid instance type for scriptable object '{scriptableObject}': " +
                    $"requested type is '{typeof(TInstance).Name}' " +
                    $"but returned type is '{baseInstance.GetType().Name}'"
                );
            return instance;
        }

        public IReadOnlyCollection<TInstance> GetInstances<TInstance>() where TInstance : class, TBaseInstance
        {
            return _instances.Values as IReadOnlyCollection<TInstance>;
        }

        protected TBaseInstance CreateInstance<TInstance>(IInstanceFactory<TInstance> scriptableObject) where TInstance : class, TBaseInstance
        {
            TBaseInstance baseInstance = scriptableObject.CreateInstance(_instantiator);
            if (baseInstance == null)
            {
                throw new InvalidOperationException(
                        $"Instance returned by the provider of type '{scriptableObject.GetType().Name}' is null"
                    );
            }

            _instances.Add(scriptableObject, baseInstance);
            return baseInstance;
        }
    }
}
