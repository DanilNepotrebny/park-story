﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Utils.Instance
{
    public interface IInstanceFactory<out TInstance>
    {
        TInstance CreateInstance(Instantiator instantiator);
    }
}
