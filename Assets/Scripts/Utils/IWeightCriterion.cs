﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Utils
{
    public interface IWeightCriterion
    {
        /// <summary> Calculate weight following the criterion </summary>
        /// <returns> Calculated weight </returns>
        float CalculateWeight();
    }
}