﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using UnityEngine;

namespace DreamTeam.Utils
{
    [RequireComponent(typeof(ParticleSystem))]
    public class ParticleSystemPlayer : MonoBehaviour
    {
        [SerializeField] private bool _playWithChildren = true;
        [SerializeField] private bool _detachOnPlay;
        [SerializeField] private bool _destroyOnDie;

        private ParticleSystem _particles;

        public void Play()
        {
            if (_particles.isPlaying)
            {
                return;
            }

            if (_detachOnPlay)
            {
                transform.SetParent(null);
            }

            _particles.Play(_playWithChildren);

            if (_destroyOnDie)
            {
                StartCoroutine(DestroyOnPlayRoutine());
            }
        }

        protected void Awake()
        {
            _particles = GetComponent<ParticleSystem>();

            ParticleSystem.MainModule mainModule = _particles.main;
            mainModule.playOnAwake = false;
        }

        private IEnumerator DestroyOnPlayRoutine()
        {
            yield return new WaitWhile(() => _particles.IsAlive(_playWithChildren));
            gameObject.Dispose();
        }
    }
}
