﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using Object = UnityEngine.Object;

namespace DreamTeam.Utils
{
    public class Instantiator
    {
        private DiContainer _container;

        public Instantiator(DiContainer container)
        {
            _container = container;
        }

        public virtual GameObject Instantiate(GameObject gameObject)
        {
            return Instantiate(gameObject, null);
        }

        /// <summary>
        /// Instantiates gameobject without changes in provided gameObject activity.
        /// For more information <see cref="ZenjectInstantiateHelper"/>.
        /// </summary>
        public virtual GameObject Instantiate(GameObject gameObject, bool doNotDeactivate)
        {
            if (gameObject == null)
            {
                throw new ArgumentNullException(nameof(gameObject));
            }

            if (!doNotDeactivate)
            {
                return Instantiate(gameObject);
            }

            if (!gameObject.activeSelf)
            {
                UnityEngine.Debug.LogWarning($"Instantiating already disabled object with 'doNotDeactivate' argument");
                return Instantiate(gameObject);
            }

            var helper = gameObject.GetComponent<ZenjectInstantiateHelper>();
            if (helper == null)
            {
                helper = gameObject.AddComponent<ZenjectInstantiateHelper>();
                helper.DeactivateOnAwake = true;
            }

            GameObject tmpObj = Object.Instantiate(gameObject);
            tmpObj.GetComponent<ZenjectInstantiateHelper>().DeactivateOnAwake = false;
            GameObject obj = Instantiate(tmpObj);
            tmpObj.Dispose();
            return obj;
        }

        public virtual GameObject Instantiate(GameObject gameObject, GameObject parent)
        {
            return Instantiate(gameObject, gameObject.transform.position, parent);
        }

        public virtual GameObject Instantiate(GameObject gameObject, Vector3 position)
        {
            return Instantiate(gameObject, position, gameObject.transform.rotation);
        }

        public virtual GameObject Instantiate(GameObject gameObject, Vector3 position, Quaternion rotation)
        {
            return Instantiate(gameObject, position, rotation, null);
        }

        public virtual GameObject Instantiate(GameObject gameObject, Vector3 position, GameObject parent)
        {
            return Instantiate(gameObject, position, gameObject.transform.rotation, parent);
        }

        public virtual GameObject Instantiate(
                GameObject gameObject,
                Vector3 position,
                Quaternion rotation,
                GameObject parent
            )
        {
            if (gameObject == null)
            {
                throw new ArgumentNullException(nameof(gameObject));
            }

            gameObject.SetActive(false);

            var result = _container.InstantiatePrefab(gameObject, position, rotation, parent?.transform);
            ObjectLifetimeHandler.InitObjectComponents(result);

            gameObject.SetActive(true);
            result.SetActive(true);

            return result;
        }

        public virtual T Instantiate<T>(T prefabComponent) where T : Component
        {
            if (prefabComponent == null)
            {
                throw new ArgumentNullException(nameof(prefabComponent));
            }

            var obj = Instantiate(prefabComponent.gameObject);
            return obj.GetComponent<T>();
        }

        public virtual T Instantiate<T>(T prefabComponent, bool doNotDeactivate) where T : Component
        {
            if (prefabComponent == null)
            {
                throw new ArgumentNullException(nameof(prefabComponent));
            }

            var obj = Instantiate(prefabComponent.gameObject, doNotDeactivate);
            return obj.GetComponent<T>();
        }

        public virtual T Instantiate<T>(T prefabComponent, GameObject parent) where T : Component
        {
            if (prefabComponent == null)
            {
                throw new ArgumentNullException(nameof(prefabComponent));
            }

            var obj = Instantiate(prefabComponent.gameObject, parent);
            return obj.GetComponent<T>();
        }

        public virtual T Instantiate<T>(T prefabComponent, Vector3 position) where T : Component
        {
            if (prefabComponent == null)
            {
                throw new ArgumentNullException(nameof(prefabComponent));
            }

            var obj = Instantiate(prefabComponent.gameObject, position, prefabComponent.transform.rotation);
            return obj.GetComponent<T>();
        }

        public virtual T Instantiate<T>(T prefabComponent, Vector3 position, Quaternion rotation) where T : Component
        {
            if (prefabComponent == null)
            {
                throw new ArgumentNullException(nameof(prefabComponent));
            }

            var obj = Instantiate(prefabComponent.gameObject, position, rotation);
            return obj.GetComponent<T>();
        }

        public virtual T Instantiate<T>(T prefabComponent, Vector3 position, GameObject parent) where T : Component
        {
            if (prefabComponent == null)
            {
                throw new ArgumentNullException(nameof(prefabComponent));
            }

            var obj = Instantiate(prefabComponent.gameObject, position, prefabComponent.transform.rotation, parent);
            return obj.GetComponent<T>();
        }

        public virtual T Instantiate<T>(T prefabComponent, Vector3 position, Quaternion rotation, GameObject parent)
            where T : Component
        {
            if (prefabComponent == null)
            {
                throw new ArgumentNullException(nameof(prefabComponent));
            }

            var obj = Instantiate(prefabComponent.gameObject, position, rotation, parent);
            return obj.GetComponent<T>();
        }

        public virtual T[] InstantiateForComponents<T>(T prefabComponent) where T : Component
        {
            return Instantiate(prefabComponent)?.gameObject.GetComponents<T>();
        }

        public virtual T Instantiate<T>(params object[] parameters)
        {
            return _container.Instantiate<T>(parameters);
        }

        public virtual T InstantiateComponentOnNewGameObject<T>(params object[] parameters) where T : Component
        {
            var gameObject = new GameObject(typeof(T).Name);
            gameObject.SetActive(false);

            T result = _container.InstantiateComponent<T>(gameObject, parameters);
            ObjectLifetimeHandler.InitComponent(result);

            gameObject.SetActive(true);

            return result;
        }

        public virtual object InstantiateExplicit(Type concreteType, params object[] parameters)
        {
            return _container.Instantiate(concreteType, new List<object>(parameters));
        }

        public virtual ScriptableObject InstantiateRuntimeScriptableObject(Type concreteType)
        {
            Assert.IsTrue(
                    typeof(ScriptableObject).IsAssignableFrom(concreteType),
                    $"{concreteType} must be inherited from {typeof(ScriptableObject)}."
                );

            var instance = ScriptableObject.CreateInstance(concreteType);
            _container.Inject(instance);
            return instance;
        }

        public void Inject(object injectable)
        {
            _container.Inject(injectable);
        }

        public void Inject(params object[] injectables)
        {
            foreach (object injectable in injectables)
            {
                Inject(injectable);
            }
        }
    }
}