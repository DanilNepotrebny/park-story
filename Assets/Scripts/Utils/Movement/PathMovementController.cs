﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.Movement
{
    /// <summary>
    /// Represents movement by simple bezier path based on four points.
    /// </summary>
    public abstract class PathMovementController : MovementController
    {
        private readonly Vector3[] _path = new Vector3[4];

        protected override LTDescr StartMovement(Vector3 end, float time)
        {
            _path[0] = transform.position;
            _path[3] = end;
            CalculatePath(_path[0], out _path[2], out _path[1], _path[3]);

            var descriptor = LeanTween.move(gameObject, _path, time);
            descriptor.setOrientToPath2d(IsOrientedByPath);
            return descriptor;
        }

        /// <summary>
        /// Implement your bezier path calculation logics there. If you don't need two control points make them equal to each other.
        /// </summary>
        protected abstract void CalculatePath(
            Vector3 start,
            out Vector3 controlPoint1,
            out Vector3 controlPoint2,
            Vector3 end);

        protected override void OnDrawPath()
        {
            base.OnDrawPath();

            if (_path != null)
            {
                CalculatePath(_path[0], out _path[2], out _path[1], _path[3]);

                Gizmos.color = Color.yellow;
                foreach (Vector3 point in _path)
                {
                    Gizmos.DrawSphere(point, 0.1f);
                }

                Gizmos.DrawLine(_path[0], _path[2]);
                Gizmos.DrawLine(_path[2], _path[1]);
                Gizmos.DrawLine(_path[1], _path[3]);

                new LTBezierPath(_path).gizmoDraw();
            }
        }
    }
}