﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

namespace DreamTeam.Utils.Movement
{
    public partial class MovementController
    {
        /// <summary> Whether to draw the path in editor </summary>
        public bool DrawPath { get; set; }

        protected void Start()
        {
            // To make "enabled" checkbox visible in editor
        }

        protected void OnDrawGizmos()
        {
            if (DrawPath)
            {
                OnDrawPath();
            }
        }
    }
}

#endif // UNITY_EDITOR