﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.Movement
{
    public class MovementArea : MonoBehaviour
    {
        [SerializeField] private RectTransform _area;

        public void FitToMovementArea(ref Vector3 point)
        {
            Bounds movementBounds = CalculateBounds();
            if (!movementBounds.Contains(point))
            {
                point = movementBounds.ClosestPoint(point);
            }
        }

        public Bounds CalculateBounds()
        {
            var movementAreaWorldCorners = new Vector3[4];
            _area.GetWorldCorners(movementAreaWorldCorners);

            var bounds = new Bounds();
            foreach (Vector3 corner in movementAreaWorldCorners)
            {
                bounds.Encapsulate(corner);
            }

            return bounds;
        }
    }
}