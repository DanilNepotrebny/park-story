﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.Movement
{
    /// <summary>
    /// Represents simple linear movement.
    /// </summary>
    public class LinearMovementController : MovementController
    {
        protected override LTDescr StartMovement(Vector3 end, float time)
        {
            if (IsOrientedByPath)
            {
                transform.right = end - gameObject.transform.position;
            }

            return LeanTween.move(gameObject, end, time);
        }
    }
}