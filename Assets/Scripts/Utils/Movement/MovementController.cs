﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils.Tweening;
using UnityEngine;

namespace DreamTeam.Utils.Movement
{
    /// <summary>
    /// Represents part of the <see cref="MovementSequence"/>. Used as a base class for all controllers.
    /// </summary>
    public abstract partial class MovementController : MonoBehaviour
    {
        [SerializeField] private float _time;
        [SerializeField] private float _speed;
        [SerializeField] private Easing _easing;
        [SerializeField] private bool _isOrientedByPath;

        public Easing Easing => _easing;

        public float Time
        {
            get { return _time; }
            set { _time = value; }
        }

        public float Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        /// <summary>
        /// Sequence that contains this controller.
        /// </summary>
        protected MovementSequence Sequence { get; private set; }

        /// <summary>
        /// Set to true if moved object should be oriented along path.
        /// </summary>
        protected bool IsOrientedByPath => _isOrientedByPath;

        protected bool HasOnUpdatedListeners => Updated != null;

        /// <summary>
        /// Triggered when the part of movement controlled by this controller is finished.
        /// </summary>
        public event Action Finished;

        /// <summary>
        /// Triggered on each tick of movement. You have to subscribe before the movement started.
        /// </summary>
        public event Action<float> Updated;

        /// <summary>
        /// Starts part of movement for specified sequence.
        /// </summary>
        public virtual void Move(Vector3 end, MovementSequence sequence)
        {
            if ((transform.position - end).sqrMagnitude < float.Epsilon)
            {
                Updated?.Invoke(1.0f);
                Finished?.Invoke();
                return;
            }

            Sequence = sequence;

            // Workaround for strange LeanTween behaviour when time is zero and speed is not.
            float time = _time;
            if (Speed != 0f)
            {
                time = 1f;
            }

            var descriptor = StartMovement(end, time);
            SetupMovement(descriptor);
        }

        /// <summary>
        /// Stops this part of movement.
        /// </summary>
        public void Stop()
        {
            LeanTween.cancel(gameObject);
        }

        /// <summary>
        /// Implementation of how this movement should be started.
        /// </summary>
        protected abstract LTDescr StartMovement(Vector3 end, float time);

        /// <summary>
        /// Setups current movement right after start. Not supposed to call this in derived classes.
        /// </summary>
        protected virtual void SetupMovement(LTDescr descriptor)
        {
            descriptor.setOnComplete(OnFinished);

            if (Speed != 0f)
            {
                descriptor.setSpeed(Speed);
            }

            if (Updated != null)
            {
                descriptor.setOnUpdateRatio(OnUpdated);
            }

            Easing.SetupDescriptor(descriptor);
        }

        protected void OnFinished()
        {
            Finished?.Invoke();
        }

        protected virtual void OnUpdated(float value, float progress)
        {
            Updated?.Invoke(progress);
        }

        protected virtual void OnDrawPath()
        {
            // override in children
        }
    }
}