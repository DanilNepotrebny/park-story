﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.Movement
{
    [RequireComponent(typeof(MovementSequence))]
    public class FixedIntermediatePointMovementController : RandomParabolicMovementController
    {
        [SerializeField,
         Tooltip("Intermediate point of movement path (2nd out of 4). Defines as offset from starting point")]
        private Vector3 _intermediatePointOffset;

        public Vector3 IntermediatePointOffset
        {
            get { return _intermediatePointOffset; }
            set { _intermediatePointOffset = value; }
        }

        protected override void CalculatePath(
            Vector3 start,
            out Vector3 controlPoint1,
            out Vector3 controlPoint2,
            Vector3 end)
        {
            base.CalculatePath(start, out controlPoint1, out controlPoint2, end);

            controlPoint1 = start + IntermediatePointOffset;
        }
    }
}