﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Utils.Movement
{
    /// <summary>
    /// Represents sequence of movements for a specified object to specified position. Sequence can be started in editor play mode
    /// via "Play" button in the inspector. Each movement within the sequence is controlled by <see cref="MovementController"/>.
    /// Whenever you need to move your object by some trajectory using of this class is recommended.
    /// </summary>
    public class MovementSequence : MonoBehaviour
    {
        [SerializeField] private MovementController[] _controllers;

        #if UNITY_EDITOR
        private bool _drawPath;
        #endif

        private int _controllerIdx;
        private bool _destroyOnFinish;

        /// <summary>
        /// Start position of the movement sequence
        /// </summary>
        public Vector3 StartPosition { get; private set; }

        /// <summary>
        /// Final position of the movement sequence
        /// </summary>
        public Vector3 TargetPosition { get; private set; }

        #if UNITY_EDITOR
        /// <summary>
        /// If true paths for all inner controllers will be drawn in editor
        /// </summary>
        public bool DrawPath
        {
            get { return _drawPath; }
            set
            {
                _drawPath = value;
                foreach (var controller in _controllers)
                {
                    controller.DrawPath = value;
                }
            }
        }
        #endif

        /// <summary>
        /// Trigerred when the sequence is started.
        /// </summary>
        public event Action<MovementSequence> Started;

        /// <summary>
        /// Trigerred when the whole sequence is finished.
        /// </summary>
        public event Action<MovementSequence> Finished;

        /// <summary>
        /// Trigerred on each tick of the movement. Passes current movement normalized completion ratio as a pararameter.
        /// </summary>
        public event Action<MovementSequence, float> Updated;

        /// <summary>
        /// Starts moving to specified point.
        /// </summary>
        public void Move(Vector3 start, Vector3 end, bool destroyOnFinish = false)
        {
            transform.position = start;
            Move(end, destroyOnFinish);
        }

        /// <summary>
        /// Starts moving to specified point.
        /// </summary>
        public void Move(Vector3 end, bool destroyOnFinish = false)
        {
            Stop();

            StartPosition = transform.position;
            TargetPosition = end;
            _destroyOnFinish = destroyOnFinish;
            _controllerIdx = 0;
            StartNextController(end);

            Started?.Invoke(this);
        }

        /// <summary>
        /// Stops movement if it is active.
        /// </summary>
        public void Stop()
        {
            if (IsMoving())
            {
                GetController().Stop();
                _controllerIdx = _controllers.Length;
            }
        }

        public float GetTotalTime()
        {
            float result = 0;
            foreach (MovementController controller in _controllers)
            {
                result += controller.Time;
            }

            return result;
        }

        public void SetTotalTimeFactor(float factor)
        {
            foreach (MovementController controller in _controllers)
            {
                controller.Time *= factor;
            }
        }

        /// <summary>
        /// Returns true if the sequence is active.
        /// </summary>
        public bool IsMoving()
        {
            return _controllers != null && _controllerIdx < _controllers.Length;
        }

        /// <summary>
        /// Unity callback.
        /// </summary>
        protected void Awake()
        {
            foreach (var controller in _controllers)
            {
                controller.Finished += OnControllerFinished;
                controller.Updated += OnControllerUpdated;
            }

            _controllerIdx = _controllers.Length;
        }

        private void OnControllerUpdated(float ratio)
        {
            Updated?.Invoke(this, (ratio + _controllerIdx) / _controllers.Length);
        }

        private void StartNextController(Vector3 end)
        {
            SkipInactiveControllers();

            if (_controllerIdx >= _controllers.Length)
            {
                Finished?.Invoke(this);
                if (_destroyOnFinish)
                {
                    gameObject.Dispose();
                }

                return;
            }

            MovementController next = GetController();
            next.Move(end, this);
        }

        private void SkipInactiveControllers()
        {
            while (_controllerIdx < _controllers.Length && !GetController().enabled)
            {
                _controllerIdx++;
            }
        }

        private void OnControllerFinished()
        {
            _controllerIdx++;
            StartNextController(TargetPosition);
        }

        private MovementController GetController()
        {
            return _controllers[_controllerIdx];
        }
    }
}