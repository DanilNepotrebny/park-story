﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.Movement
{
    /// <summary>
    /// Movement controller for an object that is blasted by explosion. Throws an object randomly in a predefined radius.
    /// </summary>
    public class ExplosionMovementController : LinearMovementController
    {
        [SerializeField] private float _radius;

        public override void Move(Vector3 end, MovementSequence sequence)
        {
            Vector3 explodePos = transform.position + _radius * (Vector3)Random.insideUnitCircle;
            base.Move(explodePos, sequence);
        }
    }
}