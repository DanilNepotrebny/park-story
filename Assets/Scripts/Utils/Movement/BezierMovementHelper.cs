﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Utils.Movement
{
    public static class BezierMovementHelper
    {
        public const int BezierPartPiecesAmount = 4; // [start_point; start_handle_point; end_point; end_handle_point]

        public delegate bool VectorComparator(Vector3 left, Vector3 right);

        public static bool IsPointLeftOfVector(Vector3 point, Vector3 vector, Vector3 planeNormal)
        {
            Vector3 pp = Vector3.ProjectOnPlane(point, planeNormal);
            Vector3 vp = Vector3.ProjectOnPlane(vector, planeNormal);

            float value = pp.x * vp.y - vp.x * pp.y;
            return value < 0;
        }

        public static List<BezierPathPart> BuildPath(Vector3 start, List<BezierPoint> bPoints)
        {
            Vector3 lookAtPoint = start;
            int listNeededSize =
                bPoints.Count *
                (BezierPartPiecesAmount - 2); // each beazier point represents 2 points of lean tween bezier path piece
            var path = new List<BezierPathPart>(listNeededSize);

            for (int i = 0; i < bPoints.Count - 1; ++i)
            {
                BezierPoint bPoint1 = bPoints[i];
                BezierPoint bPoint2 = bPoints[i + 1];

                path.Add(new BezierPathPart(lookAtPoint, bPoint1, bPoint2));

                lookAtPoint += bPoint1.PointOffset;
            }

            return path;
        }

        public static Vector3[] BuildLeanTweenPath(IList<BezierPathPart> path)
        {
            var ltPath = new Vector3[path.Count * BezierPartPiecesAmount];

            for (int i = 0; i < path.Count; i += 1)
            {
                int offset = i * BezierPartPiecesAmount;
                BezierPathPart part = path[i];

                ltPath[offset] = part[PointType.StartPoint];
                ltPath[offset + 1] = part[PointType.EndHandlePoint];
                ltPath[offset + 2] = part[PointType.StartHandlePoint];
                ltPath[offset + 3] = part[PointType.EndPoint];
            }

            return ltPath;
        }

        /// <summary> Calculate the point ratio where value suits the most to comparator </summary>
        /// <param name="path"> Bezier path </param>
        /// <param name="precisionFactor"> Amount of sample to calculate zero point. More samples -> higher precision. 0 value doesn't allowed </param>
        /// <param name="comparator"> Points comparator to determine zero point </param>
        /// <returns> Zero point [0; 1] </returns>
        public static float CalculateZeroPointRatio(
            IList<BezierPathPart> path,
            int precisionFactor,
            VectorComparator comparator)
        {
            LTBezierPath ltPath = new LTBezierPath(BuildLeanTweenPath(path));
            float step = 1f / precisionFactor;
            Vector3 zeroPoint = Vector3.negativeInfinity;
            float zeroPointRatio = 0;
            for (float ratio = 0; ratio <= 1f; ratio += step)
            {
                Vector3 point = ltPath.point(ratio);
                if (comparator(point, zeroPoint))
                {
                    zeroPoint = point;
                    zeroPointRatio = ratio;
                }
            }

            return zeroPointRatio;
        }

        /// <summary> Splits bezier path part into 2 parts according to ratio </summary>
        /// <param name="pathPart"> Bezier path part </param>
        /// <param name="splitPointRatio"> Part split ratio [0; 1] </param>
        /// <returns> Array of 2 bezier path parts </returns>
        public static IList<BezierPathPart> SplitPart(BezierPathPart pathPart, float splitPointRatio)
        {
            Assert.IsTrue(splitPointRatio >= 0f && splitPointRatio <= 1f);

            Vector3 startFirstDerivative = Vector3.Lerp(
                pathPart[PointType.StartPoint],
                pathPart[PointType.StartHandlePoint],
                splitPointRatio);
            Vector3 endFirstDerivative = Vector3.Lerp(
                pathPart[PointType.EndHandlePoint],
                pathPart[PointType.EndPoint],
                splitPointRatio);
            Vector3 handlesMidPoint = Vector3.Lerp(
                pathPart[PointType.StartHandlePoint],
                pathPart[PointType.EndHandlePoint],
                splitPointRatio);
            Vector3 startSecondDerivative = Vector3.Lerp(startFirstDerivative, handlesMidPoint, splitPointRatio);
            Vector3 endSecondDerivative = Vector3.Lerp(handlesMidPoint, endFirstDerivative, splitPointRatio);
            Vector3 zeroPoint = Vector3.Lerp(startSecondDerivative, endSecondDerivative, splitPointRatio);

            return new[]
            {
                new BezierPathPart(
                    pathPart[PointType.StartPoint],
                    startFirstDerivative,
                    zeroPoint,
                    startSecondDerivative),
                new BezierPathPart(zeroPoint, endSecondDerivative, pathPart[PointType.EndPoint], endFirstDerivative)
            };
        }

        public static void DrawPath(Vector3[] ltBezierPath)
        {
            Assert.IsTrue(ltBezierPath.Length % BezierPartPiecesAmount == 0, "Wrong amount of points");
            int bezierPiecesAmount = ltBezierPath.Length / BezierPartPiecesAmount;
            if (bezierPiecesAmount == 0)
            {
                return;
            }

            new LTBezierPath(ltBezierPath).gizmoDraw();

            // draw path points and handles
            for (int i = 0; i < bezierPiecesAmount; ++i)
            {
                int idxStart = i * BezierPartPiecesAmount;
                Vector3 startPoint = ltBezierPath[idxStart];
                Vector3 startHandlePoint = ltBezierPath[idxStart + 2];
                Vector3 endPoint = ltBezierPath[idxStart + 3];
                Vector3 endHandlePoint = ltBezierPath[idxStart + 1];

                Gizmos.color = Color.yellow;
                Gizmos.DrawSphere(startHandlePoint, 0.1f);
                Gizmos.DrawSphere(endHandlePoint, 0.1f);

                Gizmos.DrawLine(startPoint, startHandlePoint);
                Gizmos.DrawLine(endPoint, endHandlePoint);

                Gizmos.color = Color.red;
                Gizmos.DrawSphere(startPoint, 0.1f);
                Gizmos.DrawSphere(endPoint, 0.1f);
            }
        }

        public static PointType GetPointType(int index)
        {
            int value = index % Enum.GetValues(typeof(PointType)).Length;
            if (!typeof(PointType).IsEnumDefined(value))
            {
                throw new InvalidCastException($"Can't cast {value} to {nameof(PointType)} enum");
            }

            return (PointType)value;
        }

        public static BezierPointType GetBezierPointType(int index)
        {
            int value = index % Enum.GetValues(typeof(BezierPointType)).Length;
            if (!typeof(BezierPointType).IsEnumDefined(value))
            {
                throw new InvalidCastException($"Can't cast {value} to {nameof(BezierPointType)} enum");
            }

            return (BezierPointType)value;
        }

        #region Inner types

        public enum PointType
        {
            StartPoint = 0,
            StartHandlePoint = 1,
            EndPoint = 2,
            EndHandlePoint = 3
        }

        public enum BezierPointType
        {
            StartPoint = 0,
            EndPoint = 1
        }

        [Serializable]
        public struct BezierPoint
        {
            [SerializeField] private Vector3 _pointOffset; // offset from 'look at point'
            [SerializeField] private Vector3 _startPointHandleOffset; // offset from our point (when it's start point)
            [SerializeField] private Vector3 _endPointHandleOffset; // offset from our point (when it's end point)

            public Vector3 PointOffset
            {
                get { return _pointOffset; }
                set { _pointOffset = value; }
            }

            public Vector3 StartPointHandleOffset
            {
                get { return _startPointHandleOffset; }
                set { _startPointHandleOffset = value; }
            }

            public Vector3 EndPointHandleOffset
            {
                get { return _endPointHandleOffset; }
                set { _endPointHandleOffset = value; }
            }

            private Vector3 StartPointHandle => PointOffset + StartPointHandleOffset;
            private Vector3 EndPointHandle => PointOffset + EndPointHandleOffset;

            public void CalculateData(Vector3 previousPoint, bool isStartPoint, out Vector3 point, out Vector3 handle)
            {
                point = previousPoint + _pointOffset;
                handle = previousPoint + (isStartPoint ? StartPointHandle : EndPointHandle);
            }

            public static Vector3 GetHandle(int idx, List<BezierPoint> path)
            {
                BezierPointType type = GetBezierPointType(idx);
                Vector3 handle = type == BezierPointType.StartPoint ?
                    path[idx].StartPointHandle :
                    path[idx].EndPointHandle;

                return handle;
            }
        }

        public struct BezierPathPart
        {
            private readonly Vector3[] _points;

            public BezierPathPart(
                Vector3 startPoint,
                Vector3 startHandlePoint,
                Vector3 endPoint,
                Vector3 endHandlePoint)
            {
                _points = new[]
                {
                    startPoint, startHandlePoint, endPoint, endHandlePoint
                };
            }

            public BezierPathPart(Vector3 lookAtPoint, BezierPoint startBPoint, BezierPoint endBPoint)
            {
                _points = new Vector3[BezierPartPiecesAmount];

                startBPoint.CalculateData(lookAtPoint, true, out _points[0], out _points[1]);
                endBPoint.CalculateData(_points[0], false, out _points[2], out _points[3]);
            }

            public BezierPathPart(IList<Vector3> points)
                : this(points[0], points[1], points[2], points[3])
            {
                // nothing to do
            }

            public Vector3 this[PointType type]
            {
                get { return _points[(int)type]; }
                set { _points[(int)type] = value; }
            }

            public Vector3[] ToArray()
            {
                return _points.ToArray();
            }
        }

        #endregion Inner types
    }
}