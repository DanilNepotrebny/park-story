﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.Movement.Visualization
{
    public abstract class MovementSequenceCompletionRatioLauncher<TTarget> : MovementSequenceVisualization<TTarget>
        where TTarget : Object
    {
        [SerializeField, Range(0, 1)] private float _playTime = 1;

        private bool _hasLaunched;

        protected override void InitializeVisualization(MovementSequence sequence)
        {
            _hasLaunched = false;
        }

        protected override void UpdateVisulization(MovementSequence sequence, float ratio)
        {
            if (ratio >= _playTime && !_hasLaunched)
            {
                foreach (TTarget target in Targets)
                {
                    Launch(target);
                }
                _hasLaunched = true;
            }
        }

        protected abstract void Launch(TTarget target);
    }
}
