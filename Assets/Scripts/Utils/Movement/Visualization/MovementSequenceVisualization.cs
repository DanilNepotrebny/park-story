﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.Serialization;

namespace DreamTeam.Utils.Movement.Visualization
{
    [RequireComponent(typeof(MovementSequence))]
    public abstract class MovementSequenceVisualization<TTarget> : MonoBehaviour where TTarget : Object
    {
        [SerializeField, FormerlySerializedAs("Targets"), RequiredField] private TTarget[] _targets = new TTarget[0];

        private MovementSequence _sequence => GetComponent<MovementSequence>();

        protected TTarget[] Targets => _targets;

        protected virtual void OnEnable()
        {
            _sequence.Updated += OnSequenceUpdated;
            _sequence.Started += OnSequenceStarted;
        }

        protected void OnDisable()
        {
            _sequence.Updated -= OnSequenceUpdated;
            _sequence.Started -= OnSequenceStarted;
        }

        protected abstract void InitializeVisualization(MovementSequence sequence);
        protected abstract void UpdateVisulization(MovementSequence sequence, float ratio);

        private void OnSequenceStarted(MovementSequence sequence)
        {
            InitializeVisualization(sequence);
            UpdateVisulization(sequence, 0);
        }

        private void OnSequenceUpdated(MovementSequence sequence, float ratio)
        {
            UpdateVisulization(sequence, ratio);
        }
    }
}