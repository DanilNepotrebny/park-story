﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Utils.Movement.Visualization
{
    public class MovementSequenceParticlesLauncher : MovementSequenceCompletionRatioLauncher<ParticleSystemPlayer>
    {
        protected override void Launch(ParticleSystemPlayer target)
        {
            target.Play();
        }
    }
}