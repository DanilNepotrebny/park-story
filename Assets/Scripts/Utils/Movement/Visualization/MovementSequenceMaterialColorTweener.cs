﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.Movement.Visualization
{
    public class MovementSequenceMaterialColorTweener : MovementSequenceColorTweener<Renderer>
    {
        protected override void ApplyColor(Renderer target, Color color)
        {
            foreach (Material material in target.materials)
            {
                material.color = color;
            }
        }
    }
}