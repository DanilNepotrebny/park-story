﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Utils.Movement.Visualization
{
    public class MovementSequenceGraphicColorTweener : MovementSequenceColorTweener<Graphic>
    {
        private Color[] _initialColors;

        protected void Awake()
        {
            KeepInitialVisualization();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            ResetVisualization();
        }

        protected override void ApplyColor(Graphic target, Color color)
        {
            target.color = color;
        }

        private void KeepInitialVisualization()
        {
            _initialColors = new Color[Targets.Length];
            for (int index = 0; index < _initialColors.Length; index++)
            {
                _initialColors[index] = Targets[index].color;
            }
        }

        private void ResetVisualization()
        {
            for (int index = 0; index < _initialColors.Length; index++)
            {
                ApplyColor(Targets[index], _initialColors[index]);
            }
        }
    }
}