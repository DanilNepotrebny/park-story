// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.Movement.Visualization
{
    public class MovementSequenceAnimationLauncher : MovementSequenceCompletionRatioLauncher<Animation>
    {
        protected override void Launch(Animation target)
        {
            target.Play();
        }
    }
}