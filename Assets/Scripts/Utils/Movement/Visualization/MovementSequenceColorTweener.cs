﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.Movement.Visualization
{
    public abstract class MovementSequenceColorTweener<TTarget> : MovementSequenceVisualization<TTarget>
        where TTarget : Component
    {
        [SerializeField] private Gradient _gradient;

        protected override void InitializeVisualization(MovementSequence sequence)
        {
            // DO NOTHING
        }

        protected override void UpdateVisulization(MovementSequence sequence, float ratio)
        {
            Color evaluatedColor = _gradient.Evaluate(ratio);
            foreach (TTarget target in Targets)
            {
                ApplyColor(target, evaluatedColor);
            }
        }

        protected abstract void ApplyColor(TTarget target, Color color);
    }
}