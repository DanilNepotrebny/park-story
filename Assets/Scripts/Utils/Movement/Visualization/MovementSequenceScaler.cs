﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.Movement.Visualization
{
    public class MovementSequenceScaler : MovementSequenceVisualization<Transform>
    {
        [SerializeField] private Vector3 _startLocalScale = Vector3.one;

        [SerializeField] private Vector3 _finishLocalScale = Vector3.one;

        [SerializeField] private AnimationCurve _curve = AnimationCurve.Linear(1, 1, 1, 1);

        protected override void InitializeVisualization(MovementSequence sequence)
        {
            // DO NOTHING
        }

        protected override void UpdateVisulization(MovementSequence sequence, float ratio)
        {
            Vector3 evaluatedScale = Vector3.Lerp(_startLocalScale, _finishLocalScale, ratio) * _curve.Evaluate(ratio);

            foreach (Transform target in Targets)
            {
                target.localScale = evaluatedScale;
            }
        }
    }
}