﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.Movement.Visualization
{
    public class MovementSequenceActivator : MovementSequenceCompletionRatioLauncher<GameObject>
    {
        [SerializeField] private bool _detachTransform;

        protected override void InitializeVisualization(MovementSequence sequence)
        {
            base.InitializeVisualization(sequence);

            foreach (var target in Targets)
            {
                target.SetActive(false);
            }
        }

        protected override void Launch(GameObject target)
        {
            if (_detachTransform)
            {
                target.transform.SetParent(null);
            }

            target.SetActive(true);
        }
    }
}