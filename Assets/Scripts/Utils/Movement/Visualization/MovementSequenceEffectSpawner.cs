﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

namespace DreamTeam.Utils.Movement.Visualization
{
    /// <summary>
    /// Spawns effects on MovementSequence events.
    /// </summary>
    [RequireComponent(typeof(MovementSequence))]
    public class MovementSequenceEffectSpawner : MovementSequenceCompletionRatioLauncher<GameObject>
    {
        [Inject] private Instantiator _instantiator;

        protected override void Launch(GameObject target)
        {
            foreach (var effect in Targets)
            {
                _instantiator?.Instantiate(effect, transform.position);
            }
        }
    }
}