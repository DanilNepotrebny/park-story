﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Utils.Movement
{
    public class BezierMovementController : PathMovementController
    {
        [Space] [SerializeField] private Vector3 _startControlPointOffset;
        [SerializeField] private FlipToggles _autoFlipStartOffset;
        [Space] [SerializeField] private Vector3 _endControlPointOffset;
        [SerializeField] private FlipToggles _autoFlipEndOffset;

        protected override void CalculatePath(
            Vector3 start,
            out Vector3 controlPoint1,
            out Vector3 controlPoint2,
            Vector3 end)
        {
            controlPoint1 = start + CalculateOffset(_autoFlipStartOffset, start, end, _startControlPointOffset);
            controlPoint2 = end + CalculateOffset(_autoFlipEndOffset, end, start, _endControlPointOffset);
        }

        private Vector3 CalculateOffset(FlipToggles flipToggles, Vector3 from, Vector3 to, Vector3 offset)
        {
            return new Vector3(
                    flipToggles.X ? Mathf.Sign(from.x - to.x) * offset.x : offset.x,
                    flipToggles.Y ? Mathf.Sign(from.y - to.y) * offset.y : offset.y,
                    flipToggles.Z ? Mathf.Sign(from.z - to.z) * offset.z : offset.z
                );
        }

        [Serializable]
        private struct FlipToggles
        {
            public bool X;
            public bool Y;
            public bool Z;
        }
    }
}