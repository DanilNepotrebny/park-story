﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.Movement
{
    [RequireComponent(typeof(MovementSequence))]
    public class RandomParabolicMovementController : PathMovementController
    {
        [SerializeField] private float _amplitudeMin = -1;
        [SerializeField] private float _amplitudeMax = 1;

        public static void CalculateControlPoints(
            Vector3 start,
            out Vector3 controlPoint1,
            out Vector3 controlPoint2,
            Vector3 end,
            float amplitudeMin,
            float amplitudeMax)
        {
            Vector3 center = (end + start) / 2;
            Vector3 offset = (end - start).normalized;
            Vector3 normal = new Vector3(-offset.y, offset.x, 0);

            controlPoint1 = center + normal * Random.Range(amplitudeMin, amplitudeMax);
            controlPoint2 = controlPoint1;
        }

        protected override void CalculatePath(
            Vector3 start,
            out Vector3 controlPoint1,
            out Vector3 controlPoint2,
            Vector3 end)
        {
            CalculateControlPoints(start, out controlPoint1, out controlPoint2, end, _amplitudeMin, _amplitudeMax);
        }
    }
}