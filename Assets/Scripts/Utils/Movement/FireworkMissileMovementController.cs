﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using Helper = DreamTeam.Utils.Movement.BezierMovementHelper;
using Random = UnityEngine.Random;

namespace DreamTeam.Utils.Movement
{
    public class FireworkMissileMovementController : MovementController
    {
        [SerializeField] private Vector3 _startHandleOffset;
        [SerializeField] private BezierMovementHelper.BezierPoint[] _startPoints;
        [SerializeField] private Vector3 _endHandleOffset;

        [Header("Smoothness")]
        [SerializeField, Tooltip("Applying smoothing until the distance between two point less or equal")]
        private float _smoothnessDistance = 5;

        [SerializeField, Tooltip("Applying smoothing for edges greater than the angle")]
        private float _smoothnessAngle = 90;

        [SerializeField, MinMaxRange(1, 10),
         Tooltip("Radius at which will be generated additional point (smoothing one)")]
        private MinMaxRange _smoothnessRadius;

        [SerializeField, Tooltip("Randomaize side of the additional point: left or right from")]
        private bool _randomizeSmoothingPointSide;

        [Inject] private MovementArea _movementArea;

        private Vector3[] _ltPath = new Vector3[0];
        private Bounds _pathBounds;

        protected override LTDescr StartMovement(Vector3 end, float time)
        {
            Vector3 start = transform.position;

            var bPoints = new List<BezierMovementHelper.BezierPoint>();
            bPoints.Add(
                new BezierMovementHelper.BezierPoint { StartPointHandleOffset = _startHandleOffset }); // start point
            bPoints.AddRange(_startPoints);
            bPoints.Add(CreateEndBezelPoint(start, end, bPoints));

            SmoothPath(_startPoints.Length, bPoints); // excluding fixed points

            UpdateEndPointOffset(start, end, bPoints);

            List<BezierMovementHelper.BezierPathPart> bezierPath = Helper.BuildPath(start, bPoints);
            List<Vector3> ltPathList = Helper.BuildLeanTweenPath(bezierPath).ToList();
            FitPathToMovementArea(
                ltPathList,
                0,
                _startPoints.Length * Helper.BezierPartPiecesAmount + 1); // plus one for the very start point handle
            _ltPath = Helper.BuildLeanTweenPath(bezierPath);

            // tween movement
            var descriptor = LeanTween.move(gameObject, _ltPath, time);
            descriptor.setOrientToPath2d(IsOrientedByPath);
            return descriptor;
        }

        protected override void OnDrawPath()
        {
            base.OnDrawPath();

            Helper.DrawPath(_ltPath);
            DrawPathBounds();
        }

        protected void OnValidate()
        {
            float clampedAngle = Mathf.Abs(_smoothnessAngle % 180);
            if (!Mathf.Approximately(clampedAngle, _smoothnessAngle))
            {
                UnityEngine.Debug.Log(
                    $"Clamped {nameof(_smoothnessAngle)} from {_smoothnessAngle} to {clampedAngle}. " +
                    "It should not be greater than 180",
                    this);
                _smoothnessAngle = clampedAngle;
            }
        }

        /// <param name="path"></param>
        /// <param name="beginPointIdx">Begin index of point of proprotional area of points</param>
        /// <param name="endPointIdx">End index of point of proprotional area of points</param>
        private void FitPathToMovementArea(List<Vector3> path, int beginPointIdx, int endPointIdx)
        {
            if (beginPointIdx > endPointIdx)
            {
                throw new ArgumentException(
                    $"Wrong index value. {nameof(beginPointIdx)}({beginPointIdx}) greater " +
                    $"than {nameof(endPointIdx)}({endPointIdx})");
            }

            Bounds movementBounds = _movementArea.CalculateBounds();
            Assert.IsTrue(movementBounds.Contains(path[0]), "Start movement point does not lay in the movement area");
            Assert.IsTrue(
                movementBounds.Contains(path[path.Count - 1]),
                "End movement point does not lay in the movement area");

            _pathBounds = new Bounds();
            foreach (Vector3 point in path)
            {
                _pathBounds.Encapsulate(point);
            }

            FixTopOverflow(path, beginPointIdx, endPointIdx, movementBounds);
            FixRightOverflow(path, beginPointIdx, endPointIdx, movementBounds);
            FixLeftOverflow(path, beginPointIdx, endPointIdx, movementBounds);
            FixBottomOverflow(path, beginPointIdx, endPointIdx, movementBounds);

            _pathBounds = new Bounds();
            foreach (Vector3 point in path)
            {
                _pathBounds.Encapsulate(point);
            }
        }

        private void FixRightOverflow(List<Vector3> path, int beginPointIdx, int endPointIdx, Bounds movementBounds)
        {
            float pathRightX = _pathBounds.center.x + _pathBounds.extents.x;
            float pathLeftX = _pathBounds.center.x - _pathBounds.extents.x;
            float moveAreaRightX = movementBounds.center.x + movementBounds.extents.x;

            float overflow = pathRightX - moveAreaRightX;
            if (overflow > 0)
            {
                bool hasFixedInterval = beginPointIdx != endPointIdx;
                bool skipNextStartPoint = false, skipNextHandlePoint = false;

                for (int i = 0; i < path.Count; ++i)
                {
                    BezierMovementHelper.PointType pointType = Helper.GetPointType(i);

                    if (skipNextStartPoint && pointType == BezierMovementHelper.PointType.StartPoint)
                    {
                        skipNextStartPoint = false;
                        continue;
                    }

                    if (skipNextHandlePoint &&
                        (pointType == BezierMovementHelper.PointType.StartHandlePoint ||
                            pointType == BezierMovementHelper.PointType.EndHandlePoint))
                    {
                        skipNextHandlePoint = false;
                        continue;
                    }

                    Vector3 point = path[i];
                    float pointShiftCoeff, newX;

                    if (hasFixedInterval && i >= beginPointIdx && i <= endPointIdx)
                    {
                        float startPointX = path[beginPointIdx].x;
                        pointShiftCoeff = Math.Abs(point.x - startPointX) / Math.Abs(pathRightX - startPointX);
                        newX = startPointX + pointShiftCoeff * Math.Abs(moveAreaRightX - startPointX);

                        if (pointType == BezierMovementHelper.PointType.EndPoint)
                        {
                            int nextStartPointIdx = i + 2;
                            if (nextStartPointIdx < path.Count - 1 &&
                                point == path[nextStartPointIdx]) // end point equals next start point
                            {
                                path[nextStartPointIdx] = new Vector3(newX, point.y, point.z);
                                skipNextStartPoint = true;
                            }
                        }
                    }
                    else
                    {
                        pointShiftCoeff = Math.Abs(point.x - pathLeftX) / _pathBounds.size.x;
                        newX = pathLeftX + pointShiftCoeff * Math.Abs(moveAreaRightX - pathLeftX);
                    }

                    if ((pointType == BezierMovementHelper.PointType.StartPoint ||
                            pointType == BezierMovementHelper.PointType.EndPoint) &&
                        Mathf.Approximately(0, Vector3.Distance(point, path[i + 1])))
                    {
                        skipNextHandlePoint = true; // don't move zero length handle
                        if (skipNextStartPoint)
                        {
                            path[i + 1] = new Vector3(newX, point.y, point.z);
                        }
                    }

                    if (i == 0 || i == path.Count - 2)
                    {
                        continue; // don't shift movement start and end points
                    }

                    path[i] = new Vector3(newX, point.y, point.z);
                }
            }
        }

        private void FixLeftOverflow(List<Vector3> path, int beginPointIdx, int endPointIdx, Bounds movementBounds)
        {
            float pathRightX = _pathBounds.center.x + _pathBounds.extents.x;
            float pathLeftX = _pathBounds.center.x - _pathBounds.extents.x;
            float moveAreaLeftX = movementBounds.center.x - movementBounds.extents.x;

            float overflow = moveAreaLeftX - pathLeftX;
            if (overflow > 0)
            {
                bool hasFixedInterval = beginPointIdx != endPointIdx;
                bool skipNextStartPoint = false, skipNextHandlePoint = false;

                for (int i = 0; i < path.Count; ++i)
                {
                    BezierMovementHelper.PointType pointType = Helper.GetPointType(i);

                    if (skipNextStartPoint && pointType == BezierMovementHelper.PointType.StartPoint)
                    {
                        skipNextStartPoint = false;
                        continue;
                    }

                    if (skipNextHandlePoint &&
                        (pointType == BezierMovementHelper.PointType.StartHandlePoint ||
                            pointType == BezierMovementHelper.PointType.EndHandlePoint))
                    {
                        skipNextHandlePoint = false;
                        continue;
                    }

                    Vector3 point = path[i];
                    float pointShiftCoeff, newX;

                    if (hasFixedInterval && i >= beginPointIdx && i <= endPointIdx)
                    {
                        float startPointX = path[beginPointIdx].x;
                        pointShiftCoeff = Math.Abs(point.x - startPointX) / Math.Abs(startPointX - pathLeftX);
                        newX = startPointX + pointShiftCoeff * Math.Abs(startPointX - moveAreaLeftX);

                        if (pointType == BezierMovementHelper.PointType.EndPoint)
                        {
                            int nextStartPointIdx = i + 2;
                            if (nextStartPointIdx < path.Count - 1 &&
                                point == path[nextStartPointIdx]) // end point equals next start point
                            {
                                path[nextStartPointIdx] = new Vector3(newX, point.y, point.z);
                                skipNextStartPoint = true;
                            }
                        }
                    }
                    else
                    {
                        pointShiftCoeff = Math.Abs(pathRightX - point.x) / _pathBounds.size.x;
                        newX = pathRightX - pointShiftCoeff * Math.Abs(pathRightX - moveAreaLeftX);
                    }

                    if ((pointType == BezierMovementHelper.PointType.StartPoint ||
                            pointType == BezierMovementHelper.PointType.EndPoint) &&
                        Mathf.Approximately(0, Vector3.Distance(point, path[i + 1])))
                    {
                        skipNextHandlePoint = true; // don't move zero length handle
                        if (skipNextStartPoint)
                        {
                            path[i + 1] = new Vector3(newX, point.y, point.z);
                        }
                    }

                    if (i == 0 || i == path.Count - 2)
                    {
                        continue; // don't shift movement start and end points
                    }

                    path[i] = new Vector3(newX, point.y, point.z);
                }
            }
        }

        private void FixTopOverflow(List<Vector3> path, int beginPointIdx, int endPointIdx, Bounds movementBounds)
        {
            float pathTopY = _pathBounds.center.y + _pathBounds.extents.y;
            float pathBottomY = _pathBounds.center.y - _pathBounds.extents.y;
            float moveAreaTopY = movementBounds.center.y + movementBounds.extents.y;

            float overflow = pathTopY - moveAreaTopY;
            if (overflow > 0)
            {
                bool hasFixedInterval = beginPointIdx != endPointIdx;
                bool skipNextStartPoint = false, skipNextHandlePoint = false;

                for (int i = 0; i < path.Count; ++i)
                {
                    BezierMovementHelper.PointType pointType = Helper.GetPointType(i);

                    if (skipNextStartPoint && pointType == BezierMovementHelper.PointType.StartPoint)
                    {
                        skipNextStartPoint = false;
                        continue;
                    }

                    if (skipNextHandlePoint &&
                        (pointType == BezierMovementHelper.PointType.StartHandlePoint ||
                            pointType == BezierMovementHelper.PointType.EndHandlePoint))
                    {
                        skipNextHandlePoint = false;
                        continue;
                    }

                    Vector3 point = path[i];
                    float pointShiftCoeff, newY;

                    if (hasFixedInterval && i >= beginPointIdx && i <= endPointIdx)
                    {
                        float startPointY = path[beginPointIdx].y;
                        pointShiftCoeff = Math.Abs(point.y - startPointY) / Math.Abs(pathTopY - startPointY);
                        newY = startPointY + pointShiftCoeff * Math.Abs(moveAreaTopY - startPointY);

                        if (pointType == BezierMovementHelper.PointType.EndPoint)
                        {
                            int nextStartPointIdx = i + 2;
                            if (nextStartPointIdx < path.Count - 1 &&
                                point == path[nextStartPointIdx]) // end point equals next start point
                            {
                                path[nextStartPointIdx] = new Vector3(point.x, newY, point.z);
                                skipNextStartPoint = true;
                            }
                        }
                    }
                    else
                    {
                        pointShiftCoeff = Math.Abs(point.y - pathBottomY) / _pathBounds.size.y;
                        newY = pathBottomY + pointShiftCoeff * Math.Abs(moveAreaTopY - pathBottomY);
                    }

                    if ((pointType == BezierMovementHelper.PointType.StartPoint ||
                            pointType == BezierMovementHelper.PointType.EndPoint) &&
                        Mathf.Approximately(0, Vector3.Distance(point, path[i + 1])))
                    {
                        skipNextHandlePoint = true; // don't move zero length handle
                        if (skipNextStartPoint)
                        {
                            path[i + 1] = new Vector3(point.x, newY, point.z);
                        }
                    }

                    if (i == 0 || i == path.Count - 2)
                    {
                        continue; // don't shift movement start and end points
                    }

                    path[i] = new Vector3(point.x, newY, point.z);
                }
            }
        }

        private void FixBottomOverflow(List<Vector3> path, int beginPointIdx, int endPointIdx, Bounds movementBounds)
        {
            float pathBottomY = _pathBounds.center.y - _pathBounds.extents.y;
            float moveAreaBottomY = movementBounds.center.y - movementBounds.extents.y;

            float overflow = moveAreaBottomY - pathBottomY;
            if (overflow > 0)
            {
                bool hasFixedInterval = beginPointIdx != endPointIdx;
                bool skipNextStartPoint = false, skipNextHandlePoint = false;
                float pointShiftCoeff = overflow / _pathBounds.size.y;

                for (int i = 0; i < path.Count; ++i)
                {
                    if (hasFixedInterval && i >= beginPointIdx && i <= endPointIdx)
                    {
                        continue;
                    }

                    BezierMovementHelper.PointType pointType = Helper.GetPointType(i);

                    if (skipNextStartPoint && pointType == BezierMovementHelper.PointType.StartPoint)
                    {
                        skipNextStartPoint = false;
                        continue;
                    }

                    if (skipNextHandlePoint &&
                        (pointType == BezierMovementHelper.PointType.StartHandlePoint ||
                            pointType == BezierMovementHelper.PointType.EndHandlePoint))
                    {
                        skipNextHandlePoint = false;
                        continue;
                    }

                    Vector3 point = path[i];
                    float newY = path[i].y + pointShiftCoeff * _pathBounds.size.y;

                    if (pointType == BezierMovementHelper.PointType.EndPoint)
                    {
                        int nextStartPointIdx = i + 2;
                        if (nextStartPointIdx < path.Count - 1 &&
                            point == path[nextStartPointIdx]) // end point equals next start point
                        {
                            path[nextStartPointIdx] = new Vector3(point.x, newY, point.z);
                            skipNextStartPoint = true;
                        }
                    }

                    if (pointType == BezierMovementHelper.PointType.StartPoint ||
                        pointType == BezierMovementHelper.PointType.EndPoint)
                    {
                        if (Mathf.Approximately(0, Vector3.Distance(point, path[i + 1])))
                        {
                            skipNextHandlePoint = true; // don't move zero length handle
                            if (skipNextStartPoint)
                            {
                                path[i + 1] = new Vector3(point.x, newY, point.z);
                            }
                        }
                    }

                    if (i == 0 || i == path.Count - 2)
                    {
                        continue; // don't shift movement start and end points
                    }

                    path[i] = new Vector3(point.x, newY, point.z);
                }
            }
        }

        // smoothing point which has big angle or too close to each other
        private void SmoothPath(int startIdx, List<BezierMovementHelper.BezierPoint> path)
        {
            for (int i = startIdx; i < path.Count - 1; ++i)
            {
                Vector3 handlePoint1 = BezierMovementHelper.BezierPoint.GetHandle(i, path);
                Vector3 handlePoint2 = BezierMovementHelper.BezierPoint.GetHandle(i + 1, path);

                float handlesAngle = Mathf.Abs(Vector3.Angle(handlePoint1, handlePoint2));
                if (handlesAngle < _smoothnessAngle &&
                    path[i + 1].PointOffset.magnitude > _smoothnessDistance)
                {
                    continue;
                }

                // calculate offset
                Vector3 offsetDir = Vector3.Cross(path[i].PointOffset * 0.5f, Vector3.forward).normalized;
                if (_randomizeSmoothingPointSide)
                {
                    int val = Random.Range(0, 2);
                    offsetDir *= val % 2 == 0 ? 1 : -1;
                }
                else
                {
                    bool isLeft = Helper.IsPointLeftOfVector(handlePoint2, handlePoint1, Vector3.forward);
                    offsetDir *= isLeft ? 1 : -1;
                }

                float smoothnessRadius = _smoothnessRadius.GetRandomValue();
                float offsetMagnitude = Mathf.Clamp(smoothnessRadius, _smoothnessDistance, smoothnessRadius);

                // calculate handle
                Vector3 handleDir = (handlePoint2 - handlePoint1).normalized;

                var intermediatePoint = new BezierMovementHelper.BezierPoint
                {
                    PointOffset = offsetDir * offsetMagnitude,
                    StartPointHandleOffset = handleDir * _smoothnessDistance,
                    EndPointHandleOffset = -handleDir * _smoothnessDistance
                };

                path.Insert(i + 1, intermediatePoint);
                i += 1;
            }
        }

        private BezierMovementHelper.BezierPoint CreateEndBezelPoint(
            Vector3 beginPoint,
            Vector3 endPoint,
            List<BezierMovementHelper.BezierPoint> points)
        {
            Assert.IsTrue(points.Count > 0, "To build end point you must have start point");
            Vector3 lookAtPoint = points.Aggregate(beginPoint, (current, t) => current + t.PointOffset);

            return new BezierMovementHelper.BezierPoint
            {
                PointOffset = endPoint - lookAtPoint,
                EndPointHandleOffset = _endHandleOffset
            };
        }

        private void UpdateEndPointOffset(
            Vector3 beginPos,
            Vector3 endPos,
            List<BezierMovementHelper.BezierPoint> points)
        {
            Assert.IsTrue(points.Count > 1, "To build end point you must have start point");
            Vector3 lookAtPoint = beginPos;
            for (int i = 0; i < points.Count - 1; ++i)
            {
                lookAtPoint += points[i].PointOffset;
            }

            points[points.Count - 1] = new BezierMovementHelper.BezierPoint
            {
                PointOffset = endPos - lookAtPoint,
                EndPointHandleOffset = _endHandleOffset
            };
        }

        private void DrawPathBounds()
        {
            Gizmos.color = Color.green;
            Vector3 center = _pathBounds.center;
            Vector3 extents = _pathBounds.extents;

            Vector3 frontTopLeft = new Vector3(center.x - extents.x, center.y + extents.y, center.z - extents.z);
            Vector3 frontTopRight = new Vector3(center.x + extents.x, center.y + extents.y, center.z - extents.z);
            Vector3 frontBottomLeft = new Vector3(center.x - extents.x, center.y - extents.y, center.z - extents.z);
            Vector3 frontBottomRight = new Vector3(center.x + extents.x, center.y - extents.y, center.z - extents.z);
            Vector3 backTopLeft = new Vector3(center.x - extents.x, center.y + extents.y, center.z + extents.z);
            Vector3 backTopRight = new Vector3(center.x + extents.x, center.y + extents.y, center.z + extents.z);
            Vector3 backBottomLeft = new Vector3(center.x - extents.x, center.y - extents.y, center.z + extents.z);
            Vector3 backBottomRight = new Vector3(center.x + extents.x, center.y - extents.y, center.z + extents.z);

            Gizmos.DrawLine(frontTopLeft, frontTopRight);
            Gizmos.DrawLine(frontTopRight, frontBottomRight);
            Gizmos.DrawLine(frontBottomRight, frontBottomLeft);
            Gizmos.DrawLine(frontBottomLeft, frontTopLeft);

            Gizmos.DrawLine(backTopLeft, backTopRight);
            Gizmos.DrawLine(backTopRight, backBottomRight);
            Gizmos.DrawLine(backBottomRight, backBottomLeft);
            Gizmos.DrawLine(backBottomLeft, backTopLeft);

            Gizmos.DrawLine(frontTopLeft, backTopLeft);
            Gizmos.DrawLine(frontTopRight, backTopRight);
            Gizmos.DrawLine(frontBottomRight, backBottomRight);
            Gizmos.DrawLine(frontBottomLeft, backBottomLeft);
        }
    }
}