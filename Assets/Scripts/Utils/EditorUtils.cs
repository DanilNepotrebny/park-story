﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;
using Component = UnityEngine.Component;
using Object = UnityEngine.Object;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace DreamTeam.Utils
{
    public static class EditorUtils
    {
        public static RuntimePlatform RuntimePlatform
        {
            get
            {
                #if UNITY_EDITOR
                {
                    switch (EditorUserBuildSettings.activeBuildTarget)
                    {
                        case BuildTarget.Android:
                            return RuntimePlatform.Android;
                        case BuildTarget.tvOS:
                            return RuntimePlatform.tvOS;
                        case BuildTarget.iOS:
                            return RuntimePlatform.IPhonePlayer;
                        case BuildTarget.WebGL:
                            return RuntimePlatform.WebGLPlayer;
                        case BuildTarget.StandaloneWindows:
                        case BuildTarget.StandaloneWindows64:
                            return RuntimePlatform.WindowsPlayer;
                        case BuildTarget.StandaloneOSX:
                            return RuntimePlatform.OSXPlayer;
                        case BuildTarget.StandaloneLinux:
                        case BuildTarget.StandaloneLinux64:
                        case BuildTarget.StandaloneLinuxUniversal:
                            return RuntimePlatform.LinuxPlayer;
                        case BuildTarget.WSAPlayer:
                            return RuntimePlatform.WSAPlayerX64;
                        case BuildTarget.PSP2:
                            return RuntimePlatform.PSP2;
                        case BuildTarget.PS4:
                            return RuntimePlatform.PS4;
                        case BuildTarget.XboxOne:
                            return RuntimePlatform.XboxOne;
                        case BuildTarget.Switch:
                            return RuntimePlatform.Switch;
                        default:
                            string errorText =
                                $"Build target {EditorUserBuildSettings.activeBuildTarget} is unsupported. " +
                                "Please go to build setting and select support target.";

                            throw new InvalidEnumArgumentException(errorText);
                    }
                }
                #else
                {
                    return Application.platform;
                }
                #endif
            }
        }

        /// <summary>
        /// Workaround to manually inject dependencies into component in case the object was created via drag'n'drop from project panel.
        /// Will do nothing when built without UNITY_EDITOR define.
        /// </summary>
        [Conditional("UNITY_EDITOR")]
        public static void LateInject(MonoBehaviour component)
        {
            try
            {
                FindContainer(component)?.Inject(component);
            }
            catch
            {
                // This method is a dirty workaround so ignore all errors.
            }
        }

        private static DiContainer FindContainer(MonoBehaviour component)
        {
            Context context;

            context = component.GetComponentInParent<GameObjectContext>();
            if (context == null)
            {
                context = Object.FindObjectOfType<SceneContext>();
                if (context == null)
                {
                    context = Object.FindObjectOfType<ProjectContext>();
                }
            }

            if (context == null)
            {
                var root = component.gameObject.scene.FindSceneRoot();
                if (root != null && !IsParent(root.gameObject, component.gameObject))
                {
                    throw new InvalidOperationException(
                        $"Make your {component.name} object to be child of {root.name} to work correctly.");
                }

                throw new NullReferenceException($"Couldn't find context for {component.name}");
            }

            return context.Container;
        }

        private static bool IsParent(GameObject parent, GameObject child)
        {
            Transform current = child.transform.parent;
            while (current != null)
            {
                if (current == parent.transform)
                {
                    return true;
                }

                current = current.parent;
            }

            return false;
        }

        [Conditional("UNITY_EDITOR")]
        public static void DrawArrowGizmo(
            Vector3 pos,
            Vector3 direction,
            float arrowHeadLength = 0.25f,
            float arrowHeadAngle = 20.0f)
        {
            if (Mathf.Approximately(direction.sqrMagnitude, 0))
            {
                return;
            }

            Gizmos.DrawRay(pos, direction);

            Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(arrowHeadAngle, 0, 0) * Vector3.back;
            Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(-arrowHeadAngle, 0, 0) * Vector3.back;
            Vector3 up = Quaternion.LookRotation(direction) * Quaternion.Euler(0, arrowHeadAngle, 0) * Vector3.back;
            Vector3 down = Quaternion.LookRotation(direction) * Quaternion.Euler(0, -arrowHeadAngle, 0) * Vector3.back;

            Vector3 endPoint = pos + direction;
            Gizmos.DrawRay(endPoint, right * arrowHeadLength);
            Gizmos.DrawRay(endPoint, left * arrowHeadLength);
            Gizmos.DrawRay(endPoint, up * arrowHeadLength);
            Gizmos.DrawRay(endPoint, down * arrowHeadLength);
        }

        public static bool IsPlayingOrWillChangePlaymode()
        {
            #if UNITY_EDITOR
            {
                return EditorApplication.isPlayingOrWillChangePlaymode;
            }
            #else
            {
                return true;
            }
            #endif // UNITY_EDITOR
        }

        public static void QuitApplication()
        {
            #if UNITY_EDITOR
            {
                EditorApplication.isPlaying = false;
            }
            #else
            {
                Application.Quit();
            }
            #endif
        }

        public static bool ClearConsole()
        {
            #if UNITY_EDITOR
            {
                var logEntries = Type.GetType("UnityEditor.LogEntries,UnityEditor.dll");
                if (logEntries == null)
                {
                    UnityEngine.Debug.LogWarning(
                        "Didn't find LogEntries class in UnityEditor.dll. Update implementation!");
                    return false;
                }

                var clearMethod = logEntries.GetMethod("Clear", BindingFlags.Static | BindingFlags.Public);
                if (clearMethod == null)
                {
                    UnityEngine.Debug.LogWarning(
                        "Didn't find Clear method in LogEntries class. Update implementation!");
                    return false;
                }

                clearMethod.Invoke(null, null);
                return true;
            }
            #else
                return false;
            #endif
        }

        public static void ShowInFileManager(string path)
        {
            #if UNITY_EDITOR_WIN
            string itemPath = path.Replace(@"/", @"\"); // explorer doesn't like front slashes
            Process.Start("explorer.exe", itemPath);
            #elif UNITY_EDITOR_OSX
                EditorUtility.RevealInFinder(path);
            #endif
        }

        public static bool IsPrefab(GameObject gameObject)
        {
            #if UNITY_EDITOR
            {
                PrefabType type = PrefabUtility.GetPrefabType(gameObject);
                return type == PrefabType.Prefab || type == PrefabType.ModelPrefab;
            }
            #else
            {
                return false;
            }
            #endif
        }

        public static bool IsSceneBound(Object obj)
        {
            #if UNITY_EDITOR
            {
                return obj is GameObject && !IsPrefab((GameObject)obj) ||
                    obj is Component && !IsPrefab(((Component)obj).gameObject);
            }
            #else
                return false;
            #endif
        }

        public static void MarkPrefabInstanceAsDirty([NotNull] GameObject prefabInstance)
        {
            #if UNITY_EDITOR
            {
                if (PrefabUtility.GetPrefabType(prefabInstance) != PrefabType.PrefabInstance)
                {
                    UnityEngine.Debug.LogWarning($"Passed '{prefabInstance.name}' game object is not a prefab instance");
                    return;
                }

                PrefabUtility.DisconnectPrefabInstance(prefabInstance);
                EditorApplication.delayCall += () =>
                {
                    if (prefabInstance != null) // it might get deleted before end of frame
                    {
                        PrefabUtility.ReconnectToLastPrefab(prefabInstance);
                        EditorSceneManager.MarkSceneDirty(prefabInstance.scene);
                    }
                };
            }
            #endif // UNITY_EDITOR
        }

        public static void RecordObjectUndo(Object obj, string name)
        {
            #if UNITY_EDITOR
            {
                if (obj == null)
                {
                    return;
                }

                // Consider using Undo.RegisterCompleteObjectUndo as described at
                // https://gist.github.com/lazlo-bonin/a85586dd37fdf7cf4971d93fa5d2f6f7#file-undoutility-cs-L16
                Undo.RecordObject(obj, name);

                // The documentation seems to imply this is automatically done
                // by calling Undo.RecordObject, however upon testing that doesn't
                // appear to be true. TODO: report bug to unity.
                // https://docs.unity3d.com/ScriptReference/EditorUtility.SetDirty.html
                if (!IsSceneBound(obj))
                {
                    EditorUtility.SetDirty(obj);
                }

                // From the Unity documentation:
                // Call this method after making modifications to an instance of a Prefab 
                // to record those changes in the instance. If this method is not called, 
                // changes made to the instance are lost. Note that if you are not using 
                // SerializedProperty/SerializedObject, changes to the object are not recorded 
                // in the undo system whether or not this method is called.

                if (PrefabUtility.GetCorrespondingObjectFromSource(obj) != null)
                {
                    // One more catch: because we use RegisterCompleteObjectUndo
                    // instead of RecordObject, the object isn't added to the internal list
                    // of objects that need to be actually recorded when the undo
                    // record is flushed. 

                    // The problem is that RecordPrefabInstancePropertyModifications must
                    // be called *after* making modifications for it to work. This seems to
                    // be done automatically from undo record flushing, but since our object
                    // isn't registered for flushing, it will never get called. 

                    // The hacky workaround here is to wait one editor frame to actually
                    // apply the modifications at a later time. Another solution would have
                    // been to have manual BeginUndoCheck and EndUndoCheck methods,
                    // but it would be more complicated on usage and well, this seems to work.

                    EditorApplication.delayCall += () =>
                    {
                        PrefabUtility.RecordPrefabInstancePropertyModifications(obj);
                    };
                }
            }
            #endif
        }
    }
}