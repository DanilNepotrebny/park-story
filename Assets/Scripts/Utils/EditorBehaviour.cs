﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils
{
    [ExecuteInEditMode]
    public class EditorBehaviour : MonoBehaviour
    {
        #if UNITY_EDITOR
        protected virtual void Awake()
        {
            if (Application.isPlaying)
            {
                this.DisposeImmediate();
                return;
            }

            hideFlags = HideFlags.DontSaveInBuild;
        }
        #endif
    }
}