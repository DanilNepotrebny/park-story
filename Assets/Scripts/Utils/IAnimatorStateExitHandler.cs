﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Handler of the animator state exit event. Works in conjuction with <see cref="AnimatorStateExitNotifier"/>.
    /// </summary>
    public interface IAnimatorStateExitHandler
    {
        /// <summary>
        /// Called from <see cref="AnimatorStateExitNotifier"/> when animator exits specified state.
        /// </summary>
        void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex);
    }
}