﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Utils
{
    public class RandomUtils
    {
        public static T RandomElement<T>(KeyValueList<T, int> weights, CustomRandom randomSource = null)
        {
            Assert.IsTrue(weights.Count > 0, "Weights list is empty. Please assign weights item.");

            int weightSum = 0;
            foreach (KeyValuePair<T, int> pair in weights)
            {
                weightSum += pair.Value;
            }

            int r = randomSource?.Next(weightSum) ?? Random.Range(0, weightSum);

            int cumulativeWeight = 0;
            foreach (KeyValuePair<T, int> pair in weights)
            {
                cumulativeWeight += pair.Value;
                if (r < cumulativeWeight)
                {
                    return pair.Key;
                }
            }

            Assert.IsTrue(false, "Something went wrong with probabilities.");
            return weights[randomSource?.Next(weights.Count) ?? Random.Range(0, weights.Count)].Key;
        }

        public static T RandomElement<T>(IList<KeyValuePair<T, float>> weights)
        {
            Assert.IsTrue(weights.Count > 0, "Weights list is empty. Please assign weights item.");

            float weightSum = 0;
            foreach (KeyValuePair<T, float> pair in weights)
            {
                weightSum += pair.Value;
            }

            float r = Random.Range(0f, weightSum);

            float cumulativeWeight = 0f;
            foreach (KeyValuePair<T, float> pair in weights)
            {
                cumulativeWeight += pair.Value;
                if (r < cumulativeWeight)
                {
                    return pair.Key;
                }
            }

            Assert.IsTrue(false, "Something went wrong with probabilities.");
            return weights[Random.Range(0, weights.Count)].Key;
        }
    }
}
