﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils.Internal;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Utils.Internal
{
    public abstract class DrawableKeyValueList
    {
    }
}

namespace DreamTeam.Utils
{
    [Serializable]
    public class KeyValueList<TKey, TValue> :
        DrawableKeyValueList,
        IList<KeyValuePair<TKey, TValue>>,
        IDictionary<TKey, TValue>,
        ISerializationCallbackReceiver
    {
        [SerializeField, HideInInspector] protected TKey[] _serializedKeys;
        [SerializeField, HideInInspector] private TValue[] _serializedValues;

        private readonly List<KeyValuePair<TKey, TValue>> _pairs = new List<KeyValuePair<TKey, TValue>>();
        private readonly IEqualityComparer<TKey> _equalityComparer;

        public int Count => _pairs.Count;

        public bool IsReadOnly => false;

        public ICollection<TKey> Keys => _pairs.Select(pair => pair.Key).ToArray();
        public ICollection<TValue> Values => _pairs.Select(pair => pair.Value).ToArray();

        public KeyValueList()
        {
        }

        public KeyValueList([NotNull] IEqualityComparer<TKey> equalityComparer)
        {
            _equalityComparer = equalityComparer;
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            if (_pairs.Count == 0)
            {
                _serializedKeys = null;
                _serializedValues = null;
            }
            else
            {
                _serializedKeys = new TKey[_pairs.Count];
                _serializedValues = new TValue[_pairs.Count];

                int i = 0;
                foreach (var pair in _pairs)
                {
                    _serializedKeys[i] = pair.Key;
                    _serializedValues[i] = pair.Value;
                    ++i;
                }
            }
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            if (_serializedKeys != null &&
                _serializedValues != null &&
                _serializedKeys.Length == _serializedValues.Length)
            {
                _pairs.Clear();
                for (int i = 0; i < _serializedKeys.Length; ++i)
                {
                    _pairs.Add(new KeyValuePair<TKey, TValue>(_serializedKeys[i], _serializedValues[i]));
                }
            }

            _serializedKeys = null;
            _serializedValues = null;
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _pairs.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_pairs).GetEnumerator();
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            _pairs.Add(item);
        }

        public void Add(TKey key, TValue value)
        {
            _pairs.Add(new KeyValuePair<TKey, TValue>(key, value));
        }

        public void Clear()
        {
            _pairs.Clear();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return _pairs.Contains(item);
        }

        public bool ContainsKey(TKey key)
        {
            foreach (var pair in _pairs)
            {
                if (Equals(pair.Key, key))
                {
                    return true;
                }
            }

            return false;
        }

        public bool Remove(TKey key)
        {
            return Remove(Find(pair => Equals(pair.Key, key)));
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            if (ContainsKey(key))
            {
                value = FindFirst(key);
                return true;
            }

            value = default(TValue);
            return false;
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            _pairs.CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return _pairs.Remove(item);
        }

        /// <summary>
        /// Removes all pairs with the specified key.
        /// </summary>
        public bool RemoveAll(TKey key)
        {
            bool removed = false;
            for (int i = 0; i < _pairs.Count; ++i)
            {
                if (Equals(_pairs[i].Key, key))
                {
                    _pairs.RemoveAt(i);
                    --i;
                    removed = true;
                }
            }

            return removed;
        }

        public void RemoveRange(ICollection<TKey> range)
        {
            for (int i = 0; i < _pairs.Count; ++i)
            {
                if (range.Contains(_pairs[i].Key))
                {
                    _pairs.RemoveAt(i);
                    --i;
                }
            }
        }

        public int IndexOf(KeyValuePair<TKey, TValue> item)
        {
            return _pairs.IndexOf(item);
        }

        public void Insert(int index, KeyValuePair<TKey, TValue> item)
        {
            _pairs.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _pairs.RemoveAt(index);
        }

        public KeyValuePair<TKey, TValue> this[int index]
        {
            get { return _pairs[index]; }
            set { _pairs[index] = value; }
        }

        /// <summary>
        /// Finds first value by the specified key
        /// </summary>
        public TValue this[TKey key]
        {
            get
            {
                foreach (var pair in _pairs)
                {
                    if (Equals(pair.Key, key))
                    {
                        return pair.Value;
                    }
                }

                throw new ArgumentOutOfRangeException($"The key {key} is not found");
            }

            set
            {
                for (int i = 0; i < _pairs.Count; ++i)
                {
                    if (Equals(_pairs[i].Key, key))
                    {
                        _pairs[i] = new KeyValuePair<TKey, TValue>(key, value);
                        return;
                    }
                }

                throw new ArgumentOutOfRangeException($"The key {key} is not found");
            }
        }

        /// <summary>
        /// Finds first value by the specified key
        /// </summary>
        public TValue FindFirst(TKey key)
        {
            return this[key];
        }

        /// <summary>
        /// Finds first value by the specified key otherwise returns default value
        /// </summary>
        public TValue FindFirstOrDefault(TKey key)
        {
            if (ContainsKey(key))
            {
                return FindFirst(key);
            }

            return default(TValue);
        }

        /// <summary>
        /// Finds all values corresponding to the specified key
        /// </summary>
        public List<TValue> Find(TKey key)
        {
            var result = new List<TValue>();
            foreach (var pair in _pairs)
            {
                if (Equals(pair.Key, key))
                {
                    result.Add(pair.Value);
                }
            }

            return result;
        }

        public KeyValuePair<TKey, TValue> Find(Predicate<KeyValuePair<TKey, TValue>> condition)
        {
            return _pairs.Find(condition);
        }

        public List<KeyValuePair<TKey, TValue>> FindAll(Predicate<KeyValuePair<TKey, TValue>> condition)
        {
            return _pairs.FindAll(condition);
        }

        public void Sort(Comparison<KeyValuePair<TKey, TValue>> comparison)
        {
            _pairs.Sort(comparison);
        }

        private bool Equals(TKey first, TKey second)
        {
            return _equalityComparer?.Equals(first, second) ??
                first.Equals(second);
        }
    }

    /// <summary>
    /// Specifies that KeyValueList is allowed to have equal keys in different pairs
    /// </summary>
    public class NonUniqueKeysAttribute : Attribute
    {
    }

    public class ContainsAllEnumKeysAttribute : Attribute
    {
    }
}