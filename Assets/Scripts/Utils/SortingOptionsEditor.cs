﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using UnityEngine;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Use this component to edit sorting order/layer of all inner game object renderers. Useful to
    /// edit sorting options of mesh renderers (which don't have such options in inspector).
    /// After usage this component should be removed.
    /// </summary>
    [ExecuteInEditMode]
    public class SortingOptionsEditor : MonoBehaviour
    {
        [SerializeField, SortingLayer] private int _sortingLayer;
        [SerializeField] private int _sortingOrder;

        public int SortingLayer => _sortingLayer;
        public int SortingOrder => _sortingOrder;
        
        protected void Awake()
        {
            hideFlags |= HideFlags.DontSaveInEditor;
        }
    }
}

#endif