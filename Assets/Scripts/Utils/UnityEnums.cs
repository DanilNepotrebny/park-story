﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Utils
{
    public enum MouseButtonType
    {
        Left = 0,
        Right = 1,
        Middle = 2
    }

    public enum RectCorner
    {
        BottomLeft,
        TopLeft,
        TopRight,
        BottomRight,
        Max
    }
}