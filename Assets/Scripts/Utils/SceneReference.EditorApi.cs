﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace DreamTeam.Utils
{
    public partial class SceneReference :
        SceneReference.IEditorApi,
        ISerializationCallbackReceiver,
        IBuildPreprocessor
    {
        string IEditorApi.SafeAssetPath => _assetPath;

        void IEditorApi.SetAssetPath(string path)
        {
            _path = path;
            if (!string.IsNullOrEmpty(_path))
            {
                _path = _path.Substring("Assets/".Length);
                _path = _path.Remove(_path.Length - ".unity".Length);
            }

            _guid = AssetDatabase.AssetPathToGUID(path);

            CheckSceneValid();
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            if (HasValue && string.IsNullOrEmpty(_guid))
            {
                _guid = AssetDatabase.AssetPathToGUID(_assetPath);

                if (string.IsNullOrEmpty(_guid))
                {
                    UnityEngine.Debug.LogError($"Scene at path '{_assetPath} doesn't exist.'");
                }
            }

            ((IEditorApi)this).SetAssetPath(AssetDatabase.GUIDToAssetPath(_guid));
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
        }

        partial void CheckSceneValid()
        {
            if (HasValue && AssetDatabase.LoadAssetAtPath<SceneAsset>(_assetPath) == null)
            {
                UnityEngine.Debug.LogAssertion($"Scene at path '{_assetPath} doesn't exist.'");
            }
        }

        #region Inner types

        public interface IEditorApi
        {
            string SafeAssetPath { get; }
            void SetAssetPath(string path);
        }

        #endregion

        void IBuildPreprocessor.PreprocessBeforeBuild()
        {
            _guid = null;
        }
    }
}

#endif