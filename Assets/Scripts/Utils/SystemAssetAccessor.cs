﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Utils
{
    [InitializeOnLoad]
    public static class SystemAssetAccessor<T> where T : Object
    {
        private const string _systemPrefabReferences = "Assets/Settings/Editor/SystemAssetReferences.asset";

        private static SystemAssetReferences _references;
        private static T _instance;

        private static SystemAssetReferences _assetReferences
        {
            get
            {
                if (_references == null)
                {
                    _references = AssetDatabase.LoadAssetAtPath<SystemAssetReferences>(_systemPrefabReferences);
                    Assert.IsNotNull(
                            _references,
                            $"Failed to load {nameof(SystemAssetReferences)} at path: '{_systemPrefabReferences}'"
                        );
                }
                return _references; 
            }
        }

        public static T Asset => _assetReferences.GetAsset<T>();

        public static T Instance
        {
            get
            {
                MonoBehaviour prefab = Asset as MonoBehaviour;
                Assert.IsTrue(prefab != null);

                if (!EditorUtils.IsPlayingOrWillChangePlaymode() &&
                    _instance == null)
                {
                    MonoBehaviour instance = Object.Instantiate(prefab);
                    instance.gameObject.name = $"{prefab.name}_editorAccess";
                    instance.gameObject.hideFlags =
                        HideFlags.DontSave |
                        HideFlags.HideInHierarchy |
                        HideFlags.DontSaveInEditor |
                        HideFlags.NotEditable;

                    _instance = instance as T;
                }

                return _instance;
            }
        }

        static SystemAssetAccessor()
        {
            CheckType();
        }

        private static void CheckType()
        {
            Assert.IsTrue(
                    typeof(T).IsSubclassOf(typeof(MonoBehaviour)) ||
                    typeof(ScriptableObject).IsAssignableFrom(typeof(T))
                );
        }
    }
}

#endif // UNITY_EDITOR