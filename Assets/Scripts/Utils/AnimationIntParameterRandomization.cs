﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils
{
    public class AnimationIntParameterRandomization : StateMachineBehaviour
    {
        [SerializeField] private string _variableName;
        [SerializeField] private int _min;
        [SerializeField] private int _max;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.SetInteger(_variableName, Random.Range(_min, _max));
        }
    }
}
