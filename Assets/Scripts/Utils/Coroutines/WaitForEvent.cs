﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Utils.Coroutines
{
    public abstract class BaseWaitForEvent<TObservable> : CustomYieldInstruction where TObservable : class
    {
        private bool _isWaiting = true;
        private TObservable _observable;
        private EventInfo _eventInfo;
        private Delegate _eventHandler;

        public override bool keepWaiting => _isWaiting;

        protected BaseWaitForEvent(TObservable observable, string eventName)
        {
            _observable = observable;

            Type observableType = typeof(TObservable);
            _eventInfo = observableType.GetEvent(eventName);

            if (_eventInfo == null)
            {
                throw new ArgumentException($"Event {eventName} doesn't exsist in the type {observableType}");
            }

            Type delegateType = _eventInfo.EventHandlerType;

            MethodInfo invokeMethod = delegateType.GetMethod("Invoke");

            Assert.IsNotNull(invokeMethod);
            if (invokeMethod.ReturnType != typeof(void))
            {
                throw new ArgumentException(
                    $"Event {eventName} in the type {observableType} can't be used in WaitForEvent because it has non-void return value.");
            }

            _eventHandler = Delegate.CreateDelegate(_eventInfo.EventHandlerType, this, "HandleEvent");

            _eventInfo.AddEventHandlerAOT(_observable, _eventHandler);
        }

        protected void StopWaiting()
        {
            _eventInfo.RemoveEventHandlerAOT(_observable, _eventHandler);

            _isWaiting = false;
        }
    }

    public class WaitForEvent<TObservable> : BaseWaitForEvent<TObservable> where TObservable : class
    {
        public WaitForEvent(TObservable observable, string eventName) : base(observable, eventName)
        {
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        private void HandleEvent()
        {
            StopWaiting();
        }
    }

    public class WaitForEvent<TArg1, TObservable> : BaseWaitForEvent<TObservable> where TObservable : class
    {
        private Func<TArg1, bool> _predicateFunc;

        public WaitForEvent(TObservable observable, string eventName, Func<TArg1, bool> predicateFunc = null) :
            base(observable, eventName)
        {
            _predicateFunc = predicateFunc;
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        private void HandleEvent(TArg1 arg1)
        {
            if (_predicateFunc == null || _predicateFunc(arg1))
            {
                StopWaiting();
            }
        }
    }

    public class WaitForEvent<TArg1, TArg2, TObservable> : BaseWaitForEvent<TObservable> where TObservable : class
    {
        private Func<TArg1, TArg2, bool> _predicateFunc;

        public WaitForEvent(TObservable observable, string eventName, Func<TArg1, TArg2, bool> predicateFunc = null) :
            base(observable, eventName)
        {
            _predicateFunc = predicateFunc;
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        private void HandleEvent(TArg1 arg1, TArg2 arg2)
        {
            if (_predicateFunc == null || _predicateFunc(arg1, arg2))
            {
                StopWaiting();
            }
        }
    }

    public class WaitForEvent<TArg1, TArg2, TArg3, TObservable> : BaseWaitForEvent<TObservable>
        where TObservable : class
    {
        private Func<TArg1, TArg2, TArg3, bool> _predicateFunc;

        public WaitForEvent(
            TObservable observable,
            string eventName,
            Func<TArg1, TArg2, TArg3, bool> predicateFunc = null) :
            base(observable, eventName)
        {
            _predicateFunc = predicateFunc;
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        private void HandleEvent(TArg1 arg1, TArg2 arg2, TArg3 arg3)
        {
            if (_predicateFunc == null || _predicateFunc(arg1, arg2, arg3))
            {
                StopWaiting();
            }
        }
    }

    public class WaitForEvent<TArg1, TArg2, TArg3, TArg4, TObservable> : BaseWaitForEvent<TObservable>
        where TObservable : class
    {
        private Func<TArg1, TArg2, TArg3, TArg4, bool> _predicateFunc;

        public WaitForEvent(
                TObservable observable,
                string eventName,
                Func<TArg1, TArg2, TArg3, TArg4, bool> predicateFunc = null
            ) :
            base(observable, eventName)
        {
            _predicateFunc = predicateFunc;
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        private void HandleEvent(TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4)
        {
            if (_predicateFunc == null || _predicateFunc(arg1, arg2, arg3, arg4))
            {
                StopWaiting();
            }
        }
    }

    public class WaitForEvent<TArg1, TArg2, TArg3, TArg4, TArg5, TObservable> : BaseWaitForEvent<TObservable>
        where TObservable : class
    {
        private Func<TArg1, TArg2, TArg3, TArg4, TArg5, bool> _predicateFunc;

        public WaitForEvent(
                TObservable observable,
                string eventName,
                Func<TArg1, TArg2, TArg3, TArg4, TArg5, bool> predicateFunc = null
            ) :
            base(observable, eventName)
        {
            _predicateFunc = predicateFunc;
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        private void HandleEvent(TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5)
        {
            if (_predicateFunc == null || _predicateFunc(arg1, arg2, arg3, arg4, arg5))
            {
                StopWaiting();
            }
        }
    }
}