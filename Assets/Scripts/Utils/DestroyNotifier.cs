﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Utils
{
    public class DestroyNotifier : MonoBehaviour
    {
        public event Action<GameObject> Destroying;

        /// <summary>
        /// Unity callback
        /// </summary>
        protected void OnDestroy()
        {
            Destroying?.Invoke(gameObject);
        }
    }
}