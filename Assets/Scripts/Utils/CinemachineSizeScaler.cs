﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Cinemachine;
using DreamTeam.Screen;
using UnityEngine;
using Zenject;

namespace DreamTeam.Utils
{
    [DocumentationSorting(DocumentationSortingAttribute.Level.UserRef)]
    [ExecuteInEditMode]
    [AddComponentMenu("")]
    [SaveDuringPlay]
    public class CinemachineSizeScaler : CinemachineExtension
    {
        [Inject] private ScreenController _screenInjected;

        private ScreenController _screen
        {
            get
            {
                if (Application.isEditor)
                {
                    #if UNITY_EDITOR
                    {
                        return SystemAssetAccessor<ScreenController>.Asset;
                    }
                    #endif
                }

                return _screenInjected;
            }
        }

        protected override void PostPipelineStageCallback(
            CinemachineVirtualCameraBase vcam,
            CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
        {
            if (stage == CinemachineCore.Stage.Finalize)
            {
                LensSettings current = state.Lens;
                current.OrthographicSize *= _screen.CalculateOrthographicFactor();
                state.Lens = current;
            }
        }
    }
}
