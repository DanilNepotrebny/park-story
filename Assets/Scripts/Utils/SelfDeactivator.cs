﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils
{
    public class SelfDeactivator : MonoBehaviour
    {
        [SerializeField] private InvocationCallbackType _type;

        protected void Awake()
        {
            if (_type == InvocationCallbackType.Awake)
            {
                gameObject.SetActive(false);
            }
        }

        protected void Start()
        {
            if (_type == InvocationCallbackType.Start)
            {
                gameObject.SetActive(false);
            }
        }

        #region Inner types

        public enum InvocationCallbackType
        {
            Awake,
            Start
        }

        #endregion Inner types
    }
}