﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Utils
{
    [ExecuteInEditMode, DisallowMultipleComponent]
    public class TransformChildrenChangeNotifier : MonoBehaviour
    {
        public event Action TransformChildrenChanged;

        protected void OnTransformChildrenChanged()
        {
            TransformChildrenChanged?.Invoke();
        }
    }
}