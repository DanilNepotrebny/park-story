﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils.Invocation;
using UnityEngine.Assertions;

namespace DreamTeam.Utils
{
    public class ReferenceCounter
    {
        private int _refCounter;

        private Action _releaseCallback;

        public ReferenceCounter(Action releaseCallback)
        {
            _releaseCallback = releaseCallback;
        }

        public void Aquire(ActionDisposable handle)
        {
            Assert.IsNotNull(handle);
            handle.Disposing += OnHandleReleased;

            _refCounter++;
        }

        private void OnHandleReleased(ActionDisposable handle)
        {
            _refCounter--;

            if (_refCounter == 0)
            {
                _releaseCallback?.Invoke();
            }
        }
    }
}
