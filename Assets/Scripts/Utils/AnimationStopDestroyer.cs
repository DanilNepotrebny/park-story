﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using UnityEngine;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Automaticaly destroys game object when all non-looped animations in hierarchy have finished playing. Looped animations are ignored.
    /// </summary>
    public class AnimationStopDestroyer : MonoBehaviour
    {
        /// <summary>
        /// Unity callback.
        /// </summary>
        protected IEnumerator Start()
        {
            var animations = GetComponentsInChildren<Animation>();
            bool hasSuitableAnimation = false;

            while (true)
            {
                float maxLength = 0f;

                foreach (var animation in animations)
                {
                    if (animation.clip == null ||
                        !animation.isPlaying)
                    {
                        continue;
                    }

                    var wrapMode = animation.clip.wrapMode;
                    if (wrapMode == WrapMode.Once ||
                        wrapMode == WrapMode.Default)
                    {
                        maxLength = Mathf.Max(animation.clip.length, maxLength);
                    }
                }

                if (maxLength == 0f)
                {
                    break;
                }

                hasSuitableAnimation = true;
                yield return new WaitForSeconds(maxLength);
            }

            if (!hasSuitableAnimation)
            {
                UnityEngine.Debug.LogError(
                    $"There are no suitable animation in hierarchy of {gameObject.name}, AnimationStopDestroyer is useless. Check you have at least one non-looped auto-starting animation.");
            }

            gameObject.Dispose();
        }
    }
}