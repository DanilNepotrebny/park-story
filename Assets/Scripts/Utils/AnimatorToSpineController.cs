﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Events;
using Animation = Spine.Animation;
using Event = Spine.Event;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Adds possibility to use spine animations in the unity mecanim animator states. To make this thing work you need:
    /// - generate mecanim controller for your skeleton data (via skeleton data asset)
    /// - create new controller for your animation (you can use override controller if needed)
    /// - assign one or many generated clips (they are sub-assets of generated controller) to your controller states
    /// - assign <see cref="AnimatorStateEnterNotifier"/> to those states
    /// - if you want to listen to mecanim animation events in a consistant way you may assign handler function that recieves spine animation event name,
    /// and send all the mecanim events via that function too.
    /// </summary>
    [RequireComponent(typeof(SkeletonAnimation))]
    public class AnimatorToSpineController : MonoBehaviour, IAnimatorStateEnterHandler
    {
        [SerializeField] private EventHandler _eventHandler;
        [SerializeField, SpineAnimation] private string _defaultAnimation;

        private Dictionary<string, Animation> _animations = new Dictionary<string, Animation>();
        private SkeletonAnimation _animation;

        private List<AnimatorClipInfo> _clipInfoCache = new List<AnimatorClipInfo>();

        protected void Awake()
        {
            _animation = GetComponent<SkeletonAnimation>();

            SkeletonData data = _animation.SkeletonDataAsset.GetSkeletonData(true);
            foreach (var anim in data.Animations)
            {
                _animations.Add(anim.Name, anim);
            }

            _animation.Initialize(false);
            _animation.state.Event += OnEventOccured;
        }

        private void OnEventOccured(TrackEntry trackEntry, Event @event)
        {
            _eventHandler.Invoke(@event.Data.Name);
        }

        void IAnimatorStateEnterHandler.OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.GetNextAnimatorClipInfo(layerIndex, _clipInfoCache);

            if (_clipInfoCache.Count > 0)
            {
                Animation anim;
                if (_animations.TryGetValue(_clipInfoCache[0].clip.name, out anim) ||
                    _animations.TryGetValue(_defaultAnimation, out anim))
                {
                    _animation.Update(0);
                    TrackEntry trackEntry = _animation.state.SetAnimation(0, anim, stateInfo.loop);

                    AnimatorTransitionInfo transition = animator.GetAnimatorTransitionInfo(layerIndex);

                    trackEntry.MixDuration = transition.durationUnit == DurationUnit.Fixed ?
                        transition.duration :
                        transition.duration * stateInfo.length;

                    _animation.Update(0);
                }
            }
        }

        #region Inner types

        [Serializable]
        private class EventHandler : UnityEvent<string>
        {
        }

        #endregion
    }
}