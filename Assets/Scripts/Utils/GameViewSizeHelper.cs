﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;
using System.Reflection;
using JetBrains.Annotations;
using UnityEditor;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Helper to access Unity's Game View control bar.
    /// Was taken from <see cref="https://github.com/anchan828/unity-GameViewSizeHelper"/>
    /// </summary>
    public class GameViewSizeHelper
    {
        private const BindingFlags _bindingFlags = BindingFlags.Public | BindingFlags.Instance;
        private static GameViewSize _gameViewSize;
        private static object _instanceBackingField;

        private static object Instance
        {
            get
            {
                if (_instanceBackingField == null)
                {
                    PropertyInfo propertyInfo_gameViewSizes = Types.EditorGameViewSizes.GetProperty("instance");
                    _instanceBackingField = propertyInfo_gameViewSizes.GetValue(null, new object[0]);
                }
                return _instanceBackingField;
            }
        }

        public static void AddCustomSize(GameViewSizeGroupType groupType, [NotNull] GameViewSize gameViewSize)
        {
            _gameViewSize = gameViewSize;
            object sizeType = Enum.Parse(Types.EditorGameViewSizeType, gameViewSize.type.ToString());

            ConstructorInfo ctor = Types.EditorGameViewSize.GetConstructor(
                new []
                {
                    Types.EditorGameViewSizeType,
                    typeof(int),
                    typeof(int),
                    typeof(string)
                });

            object instance_gameViewSize = ctor.Invoke(
                new []
                {
                    sizeType,
                    gameViewSize.width,
                    gameViewSize.height,
                    gameViewSize.baseText
                });

            object instance_gameViewSizeGroup = GetGroup(groupType, Instance);

            if (!Contains(instance_gameViewSizeGroup))
            {
                AddCustomSize(instance_gameViewSizeGroup, instance_gameViewSize);
            }
        }

        public static void AddCustomSize(GameViewSizeGroupType groupType, GameViewSizeType type, int width, int height, string baseText)
        {
            AddCustomSize(groupType, new GameViewSize { type = type, width = width, height = height, baseText = baseText });
        }

        public static bool RemoveCustomSize(GameViewSizeGroupType groupType, GameViewSizeType type, int width, int height, string baseText)
        {
            _gameViewSize = new GameViewSize { type = type, width = width, height = height, baseText = baseText };
            return Remove(GetGroup(groupType, Instance));
        }

        public static bool RemoveCustomSize(GameViewSizeGroupType groupType, [NotNull] GameViewSize gameViewSize)
        {
            _gameViewSize = gameViewSize;
            return Remove(GetGroup(groupType, Instance));
        }

        public static bool Contains(GameViewSizeGroupType groupType, GameViewSizeType type, int width, int height, string baseText)
        {
            _gameViewSize = new GameViewSize { type = type, width = width, height = height, baseText = baseText };
            return Contains(GetGroup(groupType, Instance));
        }

        public static bool Contains(GameViewSizeGroupType groupType, [NotNull] GameViewSize gameViewSize)
        {
            _gameViewSize = gameViewSize;
            return Contains(GetGroup(groupType, Instance));
        }

        public static void ChangeGameViewSize(GameViewSizeGroupType groupType, GameViewSizeType type, int width, int height, string baseText)
        {
            ChangeGameViewSize(groupType, new GameViewSize { type = type, width = width, height = height, baseText = baseText });
        }

        public static void ChangeGameViewSize(GameViewSizeGroupType groupType, [NotNull] GameViewSize gameViewSize)
        {
            _gameViewSize = gameViewSize;

            EditorWindow gameView = GetGameViewWindow();
            PropertyInfo currentSizeGroupTypeProperty = Types.EditorGameView.
                GetProperty("currentSizeGroupType", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Static);
            var currentType = (GameViewSizeGroupType)currentSizeGroupTypeProperty.GetValue(gameView, null);
            if (groupType != currentType)
            {
                UnityEngine.Debug.LogError(
                    $"{nameof(GameViewSizeGroupType)} is {groupType}. " +
                    $"But Current {nameof(GameViewSizeGroupType)} is {currentType}.");
                return;
            }

            object group = GetGroup(groupType, Instance);
            int totalCount = GetTotalCount(group);
            int gameViewSizeLength = GetCustomCount(group);
            int index = -1;
            for (int i = totalCount - gameViewSizeLength; i < totalCount; i++)
            {
                object other_gameViewSize = GetGameViewSize(group, i);
                if (GameViewSize_Equals(_gameViewSize, other_gameViewSize))
                {
                    index = i;
                    break;
                }
            }

            if (index != -1)
            {
                PropertyInfo selectedSizeIndex = Types.EditorGameView.
                    GetProperty("selectedSizeIndex", BindingFlags.Instance | BindingFlags.NonPublic);
                selectedSizeIndex.SetValue(gameView, index, null);
            }
        }

        public static int GetSelectedGameViewSizeIndex(GameViewSizeGroupType groupType)
        {
            EditorWindow gameView = GetGameViewWindow();

            PropertyInfo selectedSizeIndexProperty = Types.EditorGameView.
                GetProperty("selectedSizeIndex", BindingFlags.Instance | BindingFlags.NonPublic);
            int selectedSizeIndex = (int)selectedSizeIndexProperty.GetValue(gameView, null);
            return selectedSizeIndex;
        }

        public static GameViewSizeGroupType GetCurrentGameViewSizeGroupType()
        {
            EditorWindow gameView = GetGameViewWindow();
            PropertyInfo property = Types.EditorGameView.GetProperty(
                "currentSizeGroupType",
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Static);
            
            var sizeType = (GameViewSizeGroupType)Enum.Parse(
                typeof(GameViewSizeGroupType),
                property.GetValue(gameView, null).ToString());
            
            return sizeType;
        }

        public static GameViewSize GetGameViewSize(GameViewSizeGroupType groupType, int index)
        {
            object gameViewSizeObject = GetGameViewSize(GetGroup(groupType, Instance), index);
            int width = (int)GetGameSizeProperty(gameViewSizeObject, "width");
            int height = (int)GetGameSizeProperty(gameViewSizeObject, "height");
            string baseText = (string)GetGameSizeProperty(gameViewSizeObject, "baseText");
            var sizeType = (GameViewSizeType)Enum.Parse(
                typeof(GameViewSizeType),
                GetGameSizeProperty(gameViewSizeObject, "sizeType").ToString());

            return new GameViewSize
            {
                baseText = baseText,
                height = height,
                type = sizeType,
                width = width
            };
        }

        public static int GetGameViewSizeWidth(GameViewSizeGroupType groupType, int index)
        {
            object gameViewSizeObject = GetGameViewSize(GetGroup(groupType, Instance), index);
            int width = (int)GetGameSizeProperty(gameViewSizeObject, "width");
            return width;
        }
        
        public static int GetGameViewSizeHeight(GameViewSizeGroupType groupType, int index)
        {
            object gameViewSizeObject = GetGameViewSize(GetGroup(groupType, Instance), index);
            int width = (int)GetGameSizeProperty(gameViewSizeObject, "height");
            return width;
        }
        
        public static string GetGameViewSizeBaseText(GameViewSizeGroupType groupType, int index)
        {
            object gameViewSizeObject = GetGameViewSize(GetGroup(groupType, Instance), index);
            string baseText = (string)GetGameSizeProperty(gameViewSizeObject, "baseText");
            return baseText;
        }
        
        public static GameViewSizeType GetGameViewSizeType(GameViewSizeGroupType groupType, int index)
        {
            object gameViewSizeObject = GetGameViewSize(GetGroup(groupType, Instance), index);
            var sizeType = (GameViewSizeType)Enum.Parse(
                typeof(GameViewSizeType),
                GetGameSizeProperty(gameViewSizeObject, "sizeType").ToString());
            return sizeType;
        }

        public static void SaveGameViewSizeModifications()
        {
            MethodInfo method = Instance.GetType().GetMethod("SaveToHDD", _bindingFlags);
            method.Invoke(Instance, null);
        }

        private static EditorWindow GetGameViewWindow()
        {
            return EditorWindow.GetWindow(Types.EditorGameView, false, null, false);
        }

        private static bool Remove([NotNull] object instance_gameViewSizeGroup)
        {
            int gameViewSizeLength = GetCustomCount(instance_gameViewSizeGroup);
            int totalCount = GetTotalCount(instance_gameViewSizeGroup);
            for (int i = totalCount - gameViewSizeLength; i < totalCount; i++)
            {
                object other_gameViewSize = GetGameViewSize(instance_gameViewSizeGroup, i);
                if (GameViewSize_Equals(_gameViewSize, other_gameViewSize))
                {
                    RemoveCustomSize(instance_gameViewSizeGroup, i);
                    return true;
                }
            }

            return false;
        }

        private static bool Contains([NotNull] object instance_gameViewSizeGroup)
        {
            int gameViewSizeLength = GetCustomCount(instance_gameViewSizeGroup);
            int totalCount = GetTotalCount(instance_gameViewSizeGroup);
            for (int i = totalCount - gameViewSizeLength; i < totalCount; i++)
            {
                if (GameViewSize_Equals(_gameViewSize, GetGameViewSize(instance_gameViewSizeGroup, i)))
                {
                    return true;
                }
            }

            return false;
        }

        private static bool GameViewSize_Equals([NotNull] GameViewSize a, [NotNull] object @object)
        {
            int b_width = (int)GetGameSizeProperty(@object, "width");
            int b_height = (int)GetGameSizeProperty(@object, "height");
            string b_baseText = (string)GetGameSizeProperty(@object, "baseText");
            var b_sizeType = (GameViewSizeType)Enum.Parse(
                typeof(GameViewSizeType),
                GetGameSizeProperty(@object, "sizeType").ToString());

            return a.type == b_sizeType && a.width == b_width && a.height == b_height && a.baseText == b_baseText;
        }

        private static object GetGameSizeProperty(object @object, string name)
        {
            PropertyInfo property = @object.GetType().GetProperty(name);
            return property.GetValue(@object, new object[0]);
        }

        private static object GetGroup(GameViewSizeGroupType groupType, [NotNull] object instance_gameViewSizes)
        {
            Type[] returnTypes = { groupType.GetType() };
            object[] parameters = { groupType };
            MethodInfo method = instance_gameViewSizes
                .GetType()
                .GetMethod("GetGroup", _bindingFlags, null, returnTypes, null);

            return method.Invoke(instance_gameViewSizes, parameters);
        }

        private static object GetGameViewSize([NotNull] object instance_gameViewSizeGroup, int i)
        {
            Type[] returnTypes = { typeof(int) };
            object[] parameters = { i };
            MethodInfo method = instance_gameViewSizeGroup
                .GetType()
                .GetMethod("GetGameViewSize", _bindingFlags, null, returnTypes, null);

            return method.Invoke(instance_gameViewSizeGroup, parameters);
        }

        private static int GetCustomCount([NotNull] object instance_gameViewSizeGroup)
        {
            MethodInfo method = instance_gameViewSizeGroup
                .GetType()
                .GetMethod("GetCustomCount", _bindingFlags, null, new Type[0], null);

            return (int)method.Invoke(instance_gameViewSizeGroup, new object[0]);
        }

        private static int GetTotalCount([NotNull] object instance_gameViewSizeGroup)
        {
            MethodInfo method = instance_gameViewSizeGroup
                .GetType()
                .GetMethod("GetTotalCount", _bindingFlags, null, new Type[0], null);

            return (int)method.Invoke(instance_gameViewSizeGroup, new object[0]);
        }

        private static void AddCustomSize(
            [NotNull] object instance_gameViewSizeGroup,
            [NotNull] object instance_gameViewSize)
        {
            Type[] returnTypes = { Types.EditorGameViewSize };
            object[] parameters = { instance_gameViewSize };
            MethodInfo method = instance_gameViewSizeGroup
                .GetType()
                .GetMethod("AddCustomSize", _bindingFlags, null, returnTypes, null);

            method.Invoke(instance_gameViewSizeGroup, parameters);
        }

        private static void RemoveCustomSize([NotNull] object instance_gameViewSizeGroup, int index)
        {
            Type[] returnTypes = { typeof(int) };
            object[] parameters = { index };
            MethodInfo method = instance_gameViewSizeGroup
                .GetType()
                .GetMethod("RemoveCustomSize", _bindingFlags, null, returnTypes, null);

            method.Invoke(instance_gameViewSizeGroup, parameters);
        }

        #region Inner types

        public class GameViewSize
        {
            public GameViewSizeType type;
            public int width;
            public int height;
            public string baseText;
        }

        public enum GameViewSizeType
        {
            FixedResolution,
            AspectRatio
        }

        private static class Types
        {
            private const string EditorAssemblyName = "UnityEditor.dll";
            private static readonly Assembly EditorAssembly = Assembly.Load(EditorAssemblyName);

            public static readonly Type EditorGameView = EditorAssembly.GetType("UnityEditor.GameView");

            public static readonly Type EditorGameViewSizeType = EditorAssembly.GetType("UnityEditor.GameViewSizeType");
            public static readonly Type EditorGameViewSize = EditorAssembly.GetType("UnityEditor.GameViewSize");

            public static readonly Type EditorGameViewSizes = EditorAssembly
                .GetType("UnityEditor.ScriptableSingleton`1")
                .MakeGenericType(EditorAssembly.GetType("UnityEditor.GameViewSizes"));
        }

        #endregion Inner types
    }
}

#endif // UNITY_EDITOR