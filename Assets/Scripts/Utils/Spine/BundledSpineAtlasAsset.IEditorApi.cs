﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.


namespace DreamTeam.Utils.Spine
{
    public partial class BundledSpineAtlasAsset
    {
        public partial class PageMaterialData : PageMaterialData.IEditorApi
        {
            void IEditorApi.SetName(string name)
            {
                _name = name;
            }
           
            public interface IEditorApi
            {
                void SetName(string name);
            }
        }
    }
}
