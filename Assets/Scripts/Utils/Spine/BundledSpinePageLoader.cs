﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.AssetBundles;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.ResourceLoading;
using Spine;
using UnityEngine;

namespace DreamTeam.Utils.Spine
{
    public class BundledSpinePageLoader : TextureLoader
    {
        private BundledSpineAtlasAsset _atlasAsset;

        private List<AtlasPage> _pages = new List<AtlasPage>();

        private Dictionary<AtlasPage, PageMaterialCounterData> _pageCounters =
            new Dictionary<AtlasPage, PageMaterialCounterData>();

        public BundledSpinePageLoader(BundledSpineAtlasAsset atlasAsset)
        {
            _atlasAsset = atlasAsset;
        }

        public void Load(AtlasPage page, string path)
        {
            _pages.Add(page);
            UpdatePageMaterial(page);
        }

        public void Unload(object material)
        {
        }

        public IResourceHandle LoadPageMaterial(AtlasPage page, AssetBundleManager assetBundleManager)
        {
            PageMaterialCounterData materialCounterData;

            if (!_pageCounters.TryGetValue(page, out materialCounterData))
            {
                BundledSpineAtlasAsset.PageMaterialData pageMaterialData = _atlasAsset.FindPageMaterialData(page);

                AssetHandle<Material> materialHandle = assetBundleManager.LoadAssetAsync(pageMaterialData.MaterialRef);
                materialHandle.LoadProcess.DoWhenCompleted(() => UpdatePageMaterial(page));

                materialCounterData = new PageMaterialCounterData()
                {
                    MaterialHandle = materialHandle,
                    RefCounter = new ReferenceCounter(() => UnloadPageMaterial(page))
                };
                _pageCounters.Add(page, materialCounterData);
            }

            SingleResourceHandle pageMaterialHandle = new SingleResourceHandle(
                    materialCounterData.MaterialHandle.LoadProcess
                );
            materialCounterData.RefCounter.Aquire(pageMaterialHandle);
            return pageMaterialHandle;
        }

        public void UpdateAllPageMaterials()
        {
            foreach (AtlasPage page in _pages)
            {
                UpdatePageMaterial(page);
            }
        }

        private void UnloadPageMaterial(AtlasPage page)
        {
            PageMaterialCounterData materialCounterData;
            if (_pageCounters.TryGetValue(page, out materialCounterData))
            {
                materialCounterData.MaterialHandle.Dispose();
                _pageCounters.Remove(page);
                UpdatePageMaterial(page);
            }
        }

        private void UpdatePageMaterial(AtlasPage page)
        {
            Material material = _atlasAsset.DummyPageMaterial;

            PageMaterialCounterData materialCounterData;
            if (_pageCounters.TryGetValue(page, out materialCounterData) &&
                materialCounterData.MaterialHandle?.Asset != null)
            {
                material = materialCounterData.MaterialHandle.Asset;
            }
            else
            {
                #if UNITY_EDITOR

                if (!Application.isPlaying)
                {
                    BundledSpineAtlasAsset.PageMaterialData pageMaterialData = _atlasAsset.FindPageMaterialData(page);
                    material = pageMaterialData.MaterialRef.SimulatedAsset;
                }

                #endif
            }

            page.rendererObject = material;
        }

        #region Inner types

        public class PageMaterialCounterData
        {
            public ReferenceCounter  RefCounter;
            public AssetHandle<Material> MaterialHandle;
        }

        #endregion
    }
}