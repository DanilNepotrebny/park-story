﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DreamTeam.AssetBundles;
using DreamTeam.Utils.ResourceLoading;
using Spine;
using Spine.Unity;
using UnityEngine;

namespace DreamTeam.Utils.Spine
{
    [CreateAssetMenu]
    public partial class BundledSpineAtlasAsset : AtlasAssetBase
    {
        [SerializeField] private TextAsset _atlasFile;
        [SerializeField] private Material _dummyPageMaterial;
        [SerializeField] private PageMaterialData[] _pageMaterialsData;

        private Atlas _atlas;
        private BundledSpinePageLoader _pageLoader;
        private bool _isAtlasPlaymode;

        public override bool IsLoaded => _atlas != null;
        public override IEnumerable<Material> Materials => Enumerable.Repeat(_dummyPageMaterial, MaterialCount);
        public override int MaterialCount => _pageMaterialsData?.Length ?? 0;
        public override Material PrimaryMaterial => _dummyPageMaterial;

        public Material DummyPageMaterial => _dummyPageMaterial;

        public static BundledSpineAtlasAsset CreateInstance(
                TextAsset atlasFile,
                PageMaterialData[] pageMaterialsData
            )
        {
            var instance = CreateInstance<BundledSpineAtlasAsset>();
            instance._atlasFile = atlasFile;
            instance._pageMaterialsData = pageMaterialsData;
            return instance;
        }

        public override void Clear()
        {
            _atlas = null;
            _pageLoader = null;
        }

        public override Atlas GetAtlas()
        {
            if (_atlasFile == null)
            {
                UnityEngine.Debug.LogError("Atlas file not set for atlas asset: " + name, this);
                Clear();
                return null;
            }

            if (_pageMaterialsData == null || _pageMaterialsData.Length == 0)
            {
                UnityEngine.Debug.LogError("Page materials not set for atlas asset: " + name, this);
                Clear();
                return null;
            }

            if (_atlas != null && _isAtlasPlaymode == Application.isPlaying)
            {
                return _atlas;
            }

            try
            {
                _pageLoader = new BundledSpinePageLoader(this);
                _atlas = new Atlas(new StringReader(_atlasFile.text), "", _pageLoader);
                _atlas.FlipV();

                _isAtlasPlaymode = Application.isPlaying;
                return _atlas;
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.LogError("Error reading atlas file for atlas asset: " + name + "\n" + ex.Message + "\n" + ex.StackTrace, this);
                return null;
            }
        }

        public PageMaterialData FindPageMaterialData(AtlasPage page)
        {
            string pageMaterialName = GetMaterialNameFromTextureName(page.name);

            PageMaterialData resultPage = null;

            foreach (PageMaterialData pageData in _pageMaterialsData)
            {
                if (pageData.Name == pageMaterialName)
                {
                    resultPage = pageData;
                    break;
                }
            }

            if (resultPage == null)
            {
                UnityEngine.Debug.LogError("Page \"" + name + "\" not found for atlas asset: " + name, this);
            }

            return resultPage;
        }

        public static string GetMaterialNameFromTextureName(string textureName)
        {
            return Path.GetFileNameWithoutExtension(textureName);
        }

        public IResourceHandle LoadPageMaterial(AtlasPage page, AssetBundleManager assetBundleManager)
        {
            return _pageLoader.LoadPageMaterial(page, assetBundleManager);
        }

        public Mesh GenerateMesh(string name, Mesh mesh, out Material material, float scale = 0.01f)
        {
            AtlasRegion region = _atlas.FindRegion(name);
            material = null;
            if (region != null)
            {
                if (mesh == null)
                {
                    mesh = new Mesh();
                    mesh.name = name;
                }

                Vector3[] verts = new Vector3[4];
                Vector2[] uvs = new Vector2[4];
                Color[] colors = { Color.white, Color.white, Color.white, Color.white };
                int[] triangles = { 0, 1, 2, 2, 3, 0 };

                float left, right, top, bottom;
                left = region.width / -2f;
                right = left * -1f;
                top = region.height / 2f;
                bottom = top * -1;

                verts[0] = new Vector3(left, bottom, 0) * scale;
                verts[1] = new Vector3(left, top, 0) * scale;
                verts[2] = new Vector3(right, top, 0) * scale;
                verts[3] = new Vector3(right, bottom, 0) * scale;
                float u, v, u2, v2;
                u = region.u;
                v = region.v;
                u2 = region.u2;
                v2 = region.v2;

                if (!region.rotate)
                {
                    uvs[0] = new Vector2(u, v2);
                    uvs[1] = new Vector2(u, v);
                    uvs[2] = new Vector2(u2, v);
                    uvs[3] = new Vector2(u2, v2);
                }
                else
                {
                    uvs[0] = new Vector2(u2, v2);
                    uvs[1] = new Vector2(u, v2);
                    uvs[2] = new Vector2(u, v);
                    uvs[3] = new Vector2(u2, v);
                }

                mesh.triangles = new int[0];
                mesh.vertices = verts;
                mesh.uv = uvs;
                mesh.colors = colors;
                mesh.triangles = triangles;
                mesh.RecalculateNormals();
                mesh.RecalculateBounds();

                material = (Material)region.page.rendererObject;
            }
            else
            {
                mesh = null;
            }

            return mesh;
        }

        #if UNITY_EDITOR

        public void UpdateAllPageMaterials()
        {
            _pageLoader?.UpdateAllPageMaterials();
        }

        #endif

        protected void Reset()
        {
            Clear();
        }

        #region Inner types

        [Serializable]
        public class MaterialReference : BundledAssetReference<Material> { }

        [Serializable]
        public partial class PageMaterialData
        {
            [SerializeField] private string _name;
            [SerializeField] private MaterialReference _materialRef = new MaterialReference();

            public string Name => _name;
            public MaterialReference MaterialRef => _materialRef;
        }

        #endregion
    }
}
