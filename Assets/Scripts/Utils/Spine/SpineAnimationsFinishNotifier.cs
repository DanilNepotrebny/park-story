﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using Spine;
using Spine.Unity;
using UnityEngine;

namespace DreamTeam.Utils.Spine
{
    public class SpineAnimationsFinishNotifier : MonoBehaviour
    {
        private int _playingSkeletonsCount;
        private Action _spineAnimationsFinished;

        public void WaitForAnimationsFinish(Action callback)
        {
            _spineAnimationsFinished += callback;

            IAnimationStateComponent[] stateComponents = GetComponentsInChildren<IAnimationStateComponent>(false);

            foreach (IAnimationStateComponent stateComponent in stateComponents)
            {
                TrackEntry track = stateComponent.AnimationState?.GetCurrent(0);
                if (track != null)
                {
                    _playingSkeletonsCount++;
                    track.Complete += OnAnimationFinished;
                    track.Interrupt += OnAnimationFinished;
                }
            }

            CheckAnimationsFinished();
        }

        private void OnAnimationFinished(TrackEntry entry)
        {
            entry.Complete -= OnAnimationFinished;
            entry.Interrupt -= OnAnimationFinished;

            _playingSkeletonsCount--;
            CheckAnimationsFinished();
        }

        private void CheckAnimationsFinished()
        {
            if (_playingSkeletonsCount <= 0)
            {
                _spineAnimationsFinished?.Invoke();
                _spineAnimationsFinished = null;
            }
        }
    }
}