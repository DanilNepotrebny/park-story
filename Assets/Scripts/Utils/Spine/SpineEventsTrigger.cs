﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Events;
using Event = Spine.Event;

namespace DreamTeam.Utils.Spine
{
    [RequireComponent(typeof(IAnimationStateComponent))]
    public class SpineEventsTrigger : MonoBehaviour
    {
        [SerializeField] private List<EventEntry> _events = new List<EventEntry>();
        [SerializeField] private List<AnimationEntry> _animations = new List<AnimationEntry>();

        private IAnimationStateComponent _skeleton => GetComponent<IAnimationStateComponent>();

        protected IEnumerator Start()
        {
            while (_skeleton.AnimationState == null)
            {
                yield return null;
            }

            _skeleton.AnimationState.Event += OnEventOccured;
            _skeleton.AnimationState.Start += OnAnimationStarted;
            _skeleton.AnimationState.End += OnAnimationEnded;
        }

        protected void OnDestroy()
        {
            if (_skeleton.AnimationState != null)
            {
                _skeleton.AnimationState.Event -= OnEventOccured;
                _skeleton.AnimationState.Start -= OnAnimationStarted;
                _skeleton.AnimationState.End -= OnAnimationEnded;
            }
        }

        protected void Reset()
        {
            if (_skeleton == null)
            {
                UnityEngine.Debug.LogError(
                        $"\"{typeof(SpineEventsTrigger)}\" requires some component implementing " +
                        $"\"{typeof(IAnimationStateComponent)}\""
                    );

                DestroyImmediate(this);
            }
        }

        private void OnEventOccured(TrackEntry trackEntry, Event trackEvent)
        {
            if (!enabled)
            {
                return;
            }

            foreach (EventEntry item in _events)
            {
                if (item.SpineEventName.Equals(trackEvent.Data.Name))
                {
                    item.Invoke();
                }
            }
        }

        private void OnAnimationEnded(TrackEntry trackEntry)
        {
            if (!enabled)
            {
                return;
            }

            foreach (AnimationEntry item in _animations)
            {
                if (item.SpineAnimationName.Equals(trackEntry.Animation.Name))
                {
                    item.InvokeEnded();
                }
            }
        }

        private void OnAnimationStarted(TrackEntry trackEntry)
        {
            if (!enabled)
            {
                return;
            }

            foreach (AnimationEntry item in _animations)
            {
                if (item.SpineAnimationName.Equals(trackEntry.Animation.Name))
                {
                    item.InvokeStarted();
                }
            }
        }

        [Serializable]
        public class EventEntry
        {
            [SerializeField, SpineEvent] private string _spineEventName;
            [SerializeField] private UnityEvent _occuredCallback;

            public string SpineEventName => _spineEventName;

            public void Invoke()
            {
                _occuredCallback?.Invoke();
            }
        }

        [Serializable]
        public class AnimationEntry
        {
            [SerializeField, SpineAnimation] private string _spineAnimationName;
            [SerializeField] private UnityEvent _startedCallback;
            [SerializeField] private UnityEvent _endedCallback;

            public string SpineAnimationName => _spineAnimationName;

            public void InvokeStarted()
            {
                _startedCallback?.Invoke();
            }

            public void InvokeEnded()
            {
                _endedCallback?.Invoke();
            }
        }
    }
}