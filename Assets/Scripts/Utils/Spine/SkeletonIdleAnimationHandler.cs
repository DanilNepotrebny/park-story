﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Linq;
using DreamTeam.Utils.Coroutines;
using Spine;
using Spine.Unity;
using UnityEngine;
using AnimationState = Spine.AnimationState;

namespace DreamTeam.Utils.Spine
{
    [RequireComponent(typeof(IAnimationStateComponent))]
    public class SkeletonIdleAnimationHandler : MonoBehaviour
    {
        [SerializeField, SpineAnimation] private string _idle;
        [SerializeField, SpineAnimation] private string[] _additional;
        [SerializeField, HideInInspector] private bool _switchOnTime;

        [SerializeField, MinMaxRange(0, 100), HideInInspector]
        private MinMaxRange _switchTime;

        private IAnimationStateComponent _animationStateComponent;

        public string IdleAnimation
        {
            get { return _idle; }
            set { _idle = value; }
        }

        public string[] AdditionalAnimations
        {
            get { return _additional; }
            set
            {
                _additional = value;
                if (_additionalPlayed.Length != _additional.Length)
                {
                    _additionalPlayed = new bool[_additional.Length];
                }
            }
        }

        private int _prevAnimIdx = -1;
        private bool[] _additionalPlayed;

        private AnimationState _animationState => _animationStateComponent.AnimationState;

        /// <summary> Immediately start to play Idle animation </summary>
        /// <param name="randomInitialTime"> Animation starts from random point of animation time </param>
        public void PlayAnimation(bool randomInitialTime)
        {
            PlayAnimationInternal(randomInitialTime, true);
        }

        /// <summary> Request to play Idle animation when specific conditions are met </summary>
        /// <param name="randomInitialTime"> Animation starts from random point of animation time </param>
        public void RequestAnimation(bool randomInitialTime)
        {
            PlayAnimationInternal(randomInitialTime, false);
        }

        public void StopAnimation()
        {
            StopAllCoroutines();
        }

        protected void Awake()
        {
            _additionalPlayed = new bool[_additional.Length];
            _animationStateComponent = GetComponent<IAnimationStateComponent>();
        }

        protected void OnValidate()
        {
            if (_additional.Length == 0)
            {
                UnityEngine.Debug.LogError("Missing additional animations!", this);
            }
            else
            {
                for (int i = 0; i < _additional.Length; ++i)
                {
                    if (_additional[i] == string.Empty)
                    {
                        UnityEngine.Debug.LogError($"Missing aditional animations at {i}!", this);
                    }
                }
            }
        }

        protected void OnDisable()
        {
            StopAnimation();
        }

        private void PlayAnimationInternal(bool randomInitialTime, bool forcePlay)
        {
            StopAnimation();

            for (int i = 0; i < _additionalPlayed.Length; ++i)
            {
                _additionalPlayed[i] = false;
            }

            StartCoroutine(PlayMainIdleAnimation(randomInitialTime, forcePlay));
        }

        private IEnumerator PlayMainIdleAnimation(bool randomInitialTime, bool forcePlay)
        {
            if (!forcePlay)
            {
                yield return new WaitUntil(CanPlay);
            }

            TrackEntry trackEntry = _animationState.SetAnimation(0, _idle, true);

            if (randomInitialTime)
            {
                trackEntry.TrackTime = Random.Range(trackEntry.AnimationStart, trackEntry.AnimationEnd);
            }

            if (_switchOnTime)
            {
                float waitTime = _switchTime.GetRandomValue();

                yield return new WaitForSeconds(waitTime);
            }
            else
            {
                yield return new WaitForEvent<TrackEntry, TrackEntry>(trackEntry, nameof(trackEntry.Complete));
            }

            StartCoroutine(PlayAdditionalAnimation());
        }

        private IEnumerator PlayAdditionalAnimation()
        {
            yield return new WaitUntil(CanPlay);

            bool startFromBegin = _additionalPlayed.All(e => e);
            if (startFromBegin)
            {
                PlayAnimationInternal(false, false);
                yield break;
            }

            string additionalAnimationName = GetAdditionalAnimationName();
            TrackEntry trackEntry = _animationState.SetAnimation(0, additionalAnimationName, false);
            trackEntry.Complete += OnAdditionalAnimationComplete;
        }

        private void OnAdditionalAnimationComplete(TrackEntry entry)
        {
            entry.Complete -= OnAdditionalAnimationComplete;

            StartCoroutine(PlayAdditionalAnimation());
        }

        private string GetAdditionalAnimationName()
        {
            int index = Random.Range(0, _additional.Length);
            if (_prevAnimIdx == index && _additional.Length > 1)
            {
                if (index == 0)
                {
                    index = 1;
                }
                else if (index == _additional.Length - 1)
                {
                    index -= 1;
                }
                else
                {
                    index += 1;
                }
            }

            _prevAnimIdx = index;
            _additionalPlayed[index] = true;
            return _additional[index];
        }

        private bool CanPlay()
        {
            TrackEntry track = _animationState.GetCurrent(0);
            if (track == null)
            {
                return true;
            }

            string currentAnimName = track.Animation.Name;
            return _idle == currentAnimName || _additional.Contains(currentAnimName);
        }
    }
}