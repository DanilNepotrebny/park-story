﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace DreamTeam.Utils.Spine
{
    public class SpineBoneFollower : MonoBehaviour
    {
        [SerializeField, RequiredField] private Component _skeletonProvider;
        [SerializeField, SpineBone(dataField = nameof(_skeletonProvider))] private string _targetBoneName;

        private CanvasScaler _canvasScaler;
        private Vector3 _result;

        private ISkeletonComponent _skeletonComponent => _skeletonProvider as ISkeletonComponent;
        private Skeleton _skeleton => _skeletonComponent?.Skeleton;

        private float _scaleFactor => _canvasScaler != null ? _canvasScaler.referencePixelsPerUnit : 1;
        private Bone _targetBone => _skeleton?.Bones.Find(item => item.Data.Name == _targetBoneName);
        private Bone _rootBone => _skeleton?.RootBone;

        public void SetTargetBoneName(string boneName)
        {
            _targetBoneName = boneName;
        }

        protected void Awake()
        {
            _canvasScaler = GetComponentInParent<CanvasScaler>();
        }

        protected void OnValidate()
        {
            if (_skeletonProvider != null)
            {
                _skeletonProvider = _skeletonComponent as Component;
                Assert.IsNotNull(
                        _skeletonProvider,
                        $"{nameof(_skeletonProvider)} must implement {nameof(ISkeletonComponent)}"
                    );
            }
        }

        protected void OnDrawGizmos()
        {
            Gizmos.color = new Color(1, 0, 1, 0.5f);
            Gizmos.DrawSphere(GetPosition(), 0.25f);
        }

        protected void Update()
        {
            transform.position = GetPosition();
        }

        private Vector3 GetPosition()
        {
            if (_targetBone != null && _rootBone != null)
            {
                _rootBone.UpdateWorldTransform();
                Vector3 rootPosition = _rootBone.GetWorldPosition(_skeletonProvider.transform);

                _targetBone.UpdateWorldTransform();
                Vector3 targetPosition = _targetBone.GetWorldPosition(_skeletonProvider.transform);

                return rootPosition + (targetPosition - rootPosition) * _scaleFactor;
            }

            return transform.position;
        }
    }
}