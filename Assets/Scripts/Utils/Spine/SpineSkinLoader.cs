﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.AssetBundles;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.ResourceLoading;
using Spine;
using Spine.Unity;
using Spine.Unity.Modules;
using Spine.Unity.Modules.AttachmentTools;
using UnityEngine;
using Zenject;

namespace DreamTeam.Utils.Spine
{
    public class SpineSkinLoader : MonoBehaviour, IResourceLoader
    {
        [Inject] private AssetBundleManager _assetBundleManager;
        
        private IResourceHandle _skinHandle;

        public IResourceHandle Load()
        {
            List<IResourceHandle> pageHandles = new List<IResourceHandle>();

            var skeletonDataAsset = GetComponent<IHasSkeletonDataAsset>().SkeletonDataAsset;
            Skin skin = skeletonDataAsset.GetSkeletonData(false).FindSkin(GetSkinName());

            Dictionary<AtlasPage, BundledSpineAtlasAsset> pagesToLoad = new Dictionary<AtlasPage, BundledSpineAtlasAsset>();

            foreach (Attachment attachment in skin.Attachments.Values)
            {
                AtlasRegion region = attachment.GetRegion();

                foreach (AtlasAssetBase atlasAsset in skeletonDataAsset.atlasAssets)
                {
                    BundledSpineAtlasAsset bundledAtlasAsset = atlasAsset as BundledSpineAtlasAsset;
                    if (bundledAtlasAsset != null)
                    {
                        if (bundledAtlasAsset.GetAtlas().FindRegion(region.name) != null &&
                            !pagesToLoad.ContainsKey(region.page))
                        {
                            pagesToLoad.Add(region.page, bundledAtlasAsset);
                        }
                    }
                }
            }

            foreach (KeyValuePair<AtlasPage, BundledSpineAtlasAsset> pair in pagesToLoad)
            {
                pageHandles.Add(
                        pair.Value.LoadPageMaterial(pair.Key, _assetBundleManager)
                    );
            }

            IEnumerable<IResourceHandle> handles = pageHandles;
            return new GroupResourceHandle(ref handles);
        }

        protected void OnEnable()
        {
            if (_skinHandle == null)
            {
                _skinHandle = Load();
            }
        }

        protected void OnDisable()
        {
            _skinHandle?.Dispose();
            _skinHandle = null;
        }

        private string GetSkinName()
        {
            SkeletonRenderer skeletonRenderer = GetComponent<SkeletonRenderer>();
            if (skeletonRenderer != null)
            {
                return skeletonRenderer.initialSkinName;
            }

            SkeletonGraphic skeletonGraphic = GetComponent<SkeletonGraphic>();
            if (skeletonGraphic != null)
            {
                return skeletonGraphic.initialSkinName;
            }

            SkeletonGraphicMultiObject skeletonGraphicMultiObject = GetComponent<SkeletonGraphicMultiObject>();
            if (skeletonGraphicMultiObject != null)
            {
                return skeletonGraphicMultiObject.initialSkinName;
            }

            return "";
        }

        #region IPreloader

        BaseAsyncProcess IResourceLoader.Preload()
        {
            if (_skinHandle == null)
            {
                _skinHandle = Load();
            }

            return _skinHandle.LoadProcess;
        }

        #endregion
    }
}