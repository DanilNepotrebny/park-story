﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Limits maximum value of the field in the inspector
    /// </summary>
    public class MaxAttribute : PropertyAttribute
    {
        public float max;

        public MaxAttribute(float max)
        {
            this.max = max;
        }
    }
}