﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(RectTransform))]
    public class WorldToUITransformer : MonoBehaviour
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private Vector3 _serializedWorldPosition;

        private RectTransform _rectTransform;

        private Camera _camera
        {
            get
            {
                Camera result = null;

                if (_canvas != null && _canvas.worldCamera != null)
                {
                    result = _canvas.worldCamera;
                }

                if (result != null)
                {
                    result = Camera.main;
                }

                return result;
            }
        }

        public Vector3 worldPosition
        {
            get { return _serializedWorldPosition; }
            set { _serializedWorldPosition = value; }
        }

        public Vector2 uiPosition
        {
            get
            {
                Vector2 result = _rectTransform.anchoredPosition;
                if (_camera != null && _canvas != null)
                {
                    RectTransform canvasTransform = _canvas.GetComponent<RectTransform>();

                    Vector2 viewportPosition = _camera.WorldToViewportPoint(worldPosition);
                    result = new Vector2(
                        ((viewportPosition.x * canvasTransform.sizeDelta.x) - (canvasTransform.sizeDelta.x * 0.5f)),
                        ((viewportPosition.y * canvasTransform.sizeDelta.y) - (canvasTransform.sizeDelta.y * 0.5f)));
                }

                return result;
            }
        }

        protected void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
        }

        protected void Update()
        {
            _rectTransform.anchoredPosition = uiPosition;
        }
    }
}