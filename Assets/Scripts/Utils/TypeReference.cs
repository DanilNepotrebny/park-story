﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Reflection;
using DreamTeam.Utils.Internal;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Utils.Internal
{
    public abstract class DrawableTypeReference
    {
    }
}

namespace DreamTeam.Utils
{
    [Serializable]
    public class TypeReference<TBaseType> : DrawableTypeReference, ISerializationCallbackReceiver
        where TBaseType : class
    {
        [SerializeField] private string _serializedType = "";

        public Type Type { get; private set; }

        protected TypeReference()
        {
        }

        public TypeReference(Type type, bool areAbstractTypesAllowed)
        {
            Assert.IsTrue(IsValidType(type, areAbstractTypesAllowed));
            _serializedType = Serialize(type);
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public static string Serialize(Type type)
        {
            return type.AssemblyQualifiedName;
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public static bool IsValidType(Type type, bool areAbstractTypesAllowed)
        {
            return (areAbstractTypesAllowed || !type.IsAbstract) && typeof(TBaseType).IsAssignableFrom(type);
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            Type = Type.GetType(_serializedType);
        }
    }

    /// <summary>
    /// Attribute for TypeReference
    /// </summary>
    public class AllowAbstractTypesAttribute : Attribute
    {
    }
}