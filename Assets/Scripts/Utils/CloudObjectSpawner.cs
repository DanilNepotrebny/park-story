﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Utils
{
    public class CloudObjectSpawner : PrefabSpawner
    {
        #if NO_CLOUD
        protected override bool IsSpawnEnabled => false;
        #endif
    }
}