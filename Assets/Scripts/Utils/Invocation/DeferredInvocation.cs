﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;

namespace DreamTeam.Utils.Invocation
{
    /// <summary>
    /// Delegate with possibility of deferring invocation. Useful for situations when some outer logics should be
    /// able to prolong process of your class until outer event is happened (e.g. animation is finished).
    /// </summary>
    public class DeferredInvocation : BaseDeferredInvocation
    {
        private readonly Action _action;

        /// <summary>
        /// Creates deferred delegated based on specified action.
        /// </summary>
        public DeferredInvocation(Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            _action = action;
        }

        protected override void Invoke()
        {
            _action();
        }
    }

    public class DeferredInvocation<T> : BaseDeferredInvocation
    {
        private readonly Action<T> _action;
        private readonly T _parameter;

        /// <summary>
        /// Creates deferred delegated based on specified action.
        /// </summary>
        public DeferredInvocation(Action<T> action, T parameter)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            _action = action;
            _parameter = parameter;
        }

        protected override void Invoke()
        {
            _action(_parameter);
        }
    }

    public class DeferredInvocation<T1, T2> : BaseDeferredInvocation
    {
        private readonly Action<T1, T2> _action;
        private readonly T1 _parameter1;
        private readonly T2 _parameter2;

        /// <summary>
        /// Creates deferred delegated based on specified action.
        /// </summary>
        public DeferredInvocation(Action<T1, T2> action, T1 parameter1, T2 parameter2)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            _action = action;
            _parameter1 = parameter1;
            _parameter2 = parameter2;
        }

        protected override void Invoke()
        {
            _action(_parameter1, _parameter2);
        }
    }

    public class DeferredInvocation<T1, T2, T3> : BaseDeferredInvocation
    {
        private readonly Action<T1, T2, T3> _action;
        private readonly T1 _parameter1;
        private readonly T2 _parameter2;
        private readonly T3 _parameter3;

        /// <summary>
        /// Creates deferred delegated based on specified action.
        /// </summary>
        public DeferredInvocation(Action<T1, T2, T3> action, T1 parameter1, T2 parameter2, T3 parameter3)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            _action = action;
            _parameter1 = parameter1;
            _parameter2 = parameter2;
            _parameter3 = parameter3;
        }

        protected override void Invoke()
        {
            _action(_parameter1, _parameter2, _parameter3);
        }
    }
}