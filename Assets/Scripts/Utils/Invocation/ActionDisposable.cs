﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using JetBrains.Annotations;
using UnityEngine.Assertions;

namespace DreamTeam.Utils.Invocation
{
    public class ActionDisposable : IDisposable
    {
        private bool _isDisposed;

        public event Action<ActionDisposable> Disposing;

        public ActionDisposable()
        {

        }

        public ActionDisposable([NotNull] Action<ActionDisposable> actionOnDispose)
        {
            Disposing += actionOnDispose;
        }

        public virtual void Dispose()
        {
            Assert.IsFalse(_isDisposed, $"Multiple Dispose() call");

            Disposing?.Invoke(this);
            _isDisposed = true;
        }
    }
}
