﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;
using UnityEngine.Assertions;

namespace DreamTeam.Utils.Invocation
{
    public interface IDeferredInvocationHandle : IDisposable
    {
        IDeferredInvocationHandle Lock();

        /// <summary>
        /// Finishes usage of this control object. If this is the last control object deferred invocation will be triggered.
        /// </summary>
        void Unlock();
    }

    /// <summary>
    /// Delegate with possibility of deferring invocation. Useful for situations when some outer logics should be
    /// able to prolong process of your class until outer event is happened (e.g. animation is finished).
    /// </summary>
    public abstract class BaseDeferredInvocation : IDeferredInvocationHandle
    {
        private int _usageCounter = 0;

        public bool IsLocked => _usageCounter > 0;
        public IDeferredInvocationHandle Handle => IsLocked ? this : null;

        public IDeferredInvocationHandle Start()
        {
            if (IsLocked)
            {
                throw new InvalidOperationException("Deferred invocation has been already started.");
            }

            return LockInternal();
        }

        /// <summary>
        /// Performs deferred invocation.
        /// </summary>
        protected abstract void Invoke();

        #region IDeferredInvocationHandle

        IDeferredInvocationHandle IDeferredInvocationHandle.Lock()
        {
            if (!IsLocked)
            {
                throw new InvalidOperationException("Deferred invocation hasn't been started yet.");
            }

            return LockInternal();
        }

        void IDeferredInvocationHandle.Unlock()
        {
            if (!IsLocked)
            {
                throw new InvalidOperationException("Deferred invocation has been already finished.");
            }

            _usageCounter--;
            if (!IsLocked)
            {
                Invoke();
            }
        }

        #endregion IDeferredInvocationHandle

        #region IDisposable

        void IDisposable.Dispose()
        {
            (this as IDeferredInvocationHandle).Unlock();
        }

        #endregion IDisposable

        #if !UNITY_EDITOR
        private IDeferredInvocationHandle LockInternal()
        {
            _usageCounter++;
            return this;
        }

        #else // UNITY_EDITOR

        // list of items to inspect while debugging
        private readonly List<DebuggableDeferredInvocation> _debuggables = new List<DebuggableDeferredInvocation>();

        internal void Add(DebuggableDeferredInvocation debuggable)
        {
            _debuggables.Add(debuggable);
        }

        internal void Remove(DebuggableDeferredInvocation debuggable)
        {
            _debuggables.Remove(debuggable);
        }

        private IDeferredInvocationHandle LockInternal()
        {
            _usageCounter++;
            return new DebuggableDeferredInvocation(this);
        }

        #endif // UNITY_EDITOR
    }

    #if UNITY_EDITOR

    public class DebuggableDeferredInvocation : IDeferredInvocationHandle
    {
        private readonly BaseDeferredInvocation _di;
        private StackTrace _trace;

        public DebuggableDeferredInvocation([NotNull] BaseDeferredInvocation di)
        {
            _di = di;
            _trace = new StackTrace(true);
            _di.Add(this);
        }

        #region IDeferredInvocationHandle

        IDeferredInvocationHandle IDeferredInvocationHandle.Lock()
        {
            return (_di as IDeferredInvocationHandle).Lock();
        }

        void IDeferredInvocationHandle.Unlock()
        {
            Assert.IsNotNull(
                _trace,
                "You unlocking handle twice. Check Lock/Unlock logic. " +
                "Probably you unlocking wrong handle. Keep in mind that Lock() provides new handle instance");
            _trace = null;
            _di.Remove(this);
            (_di as IDeferredInvocationHandle).Unlock();
        }

        #endregion IDeferredInvocationHandle

        #region IDisposable

        void IDisposable.Dispose()
        {
            (this as IDeferredInvocationHandle).Unlock();
        }

        #endregion IDisposable
    }

    #endif // UNITY_EDITOR
}