﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;

namespace DreamTeam.Utils
{
    public interface IWeightCalculator
    {
        /// <summary> Calculate total weight according to different criteria </summary>
        /// <param name="criteria"> List of different criterions </param>
        /// <returns> Calculated weight </returns>
        float Calculate(IList<IWeightCriterion> criteria);
    }
}