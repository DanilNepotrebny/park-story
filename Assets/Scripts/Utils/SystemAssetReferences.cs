﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Utils
{
    public class SystemAssetReferences : ScriptableObject
    {
        [SerializeField] private List<Object> _systemAssets;

        public T GetAsset<T>() where T : Object
        {
            foreach (Object asset in _systemAssets)
            {
                if (asset == null)
                {
                    continue;
                }

                if (asset.GetType() == typeof(T))
                {
                    return (T)asset;
                }

                GameObject go = asset as GameObject;
                if (go != null && typeof(T).IsSubclassOf(typeof(MonoBehaviour)))
                {
                    T result = go.GetComponent<T>();
                    if (result != null)
                    {
                        return result;
                    }
                }
            }

            return null;
        }
    }
}

#endif // UNITY_EDITOR