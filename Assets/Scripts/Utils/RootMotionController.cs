﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils
{
    [ExecuteInEditMode, RequireComponent(typeof(Animator))]
    public class RootMotionController : MonoBehaviour
    {
        [SerializeField] private Transform _isometryObject;
        [SerializeField] private Transform _compensator;
        [SerializeField] private float _maxAllowedDelta = 0.01f;
        [SerializeField] private bool _logRouteOffset = false;

        protected void LateUpdate()
        {
            if (_compensator != null)
            {
                // ATTENTION!!! Code below ignores returning of the root position to the animation origin
                Vector3 delta = transform.position - _isometryObject.position;
                if (delta.sqrMagnitude < _maxAllowedDelta)
                {
                    _isometryObject.position = transform.position;
                    _compensator.position -= delta;
                }
                else
                {
                    _compensator.position -= delta;
                    if (_logRouteOffset)
                    {
                        UnityEngine.Debug.Log($"Route offset delta = {delta.ToString("F4")}, sqr magnitude = {delta.sqrMagnitude}!");
                    }
                }
            }
        }
    }
}