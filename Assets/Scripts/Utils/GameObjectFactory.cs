﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using ModestTree;
using UnityEngine;

namespace DreamTeam.Utils
{
    [Serializable]
    public abstract class GameObjectFactory<TModel, TModelTypeReference, TValueMap> : ScriptableObject
        where TValueMap : KeyValueList<TModelTypeReference, GameObject>
        where TModelTypeReference : TypeReference<TModel>
        where TModel : class
    {
        [SerializeField] protected GameObject _default;
        [SerializeField] protected TValueMap _valueMap;

        /// <summary>
        /// Create instance of an TValue by TModel. If there is no corresponding TValue
        /// </summary>
        /// <param name="element"> Type of an TValue </param>
        /// <param name="instantiator"> Appropriate instantiator </param>
        /// <returns> Instantiated object. If there is no corresponding object, returns the default </returns>
        public GameObject Create(TModel element, Instantiator instantiator)
        {
            return Create(element, instantiator, Vector3.zero);
        }

        public virtual GameObject Create(TModel element, Instantiator instantiator, Vector3 position)
        {
            Type t = element.GetType();

            foreach (var pair in _valueMap)
            {
                if (t == pair.Key.Type)
                {
                    return pair.Value != null ? instantiator.Instantiate(pair.Value.gameObject, position) : null;
                }
            }

            foreach (Type baseType in t.GetParentTypes())
            {
                foreach (var pair in _valueMap)
                {
                    if (baseType == pair.Key.Type)
                    {
                        return pair.Value != null ? instantiator.Instantiate(pair.Value.gameObject, position) : null;
                    }
                }
            }

            foreach (var pair in _valueMap)
            {
                if (pair.Key.Type.IsAssignableFrom(t))
                {
                    return pair.Value != null ? instantiator.Instantiate(pair.Value.gameObject, position) : null;
                }
            }

            return _default != null ?
                instantiator.Instantiate(_default, position) :
                null;
        }
    }
}