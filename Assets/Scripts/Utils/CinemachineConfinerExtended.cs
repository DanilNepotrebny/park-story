﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using Cinemachine;
using Cinemachine.Utility;
using UnityEngine;

namespace DreamTeam.Utils
{
    [DocumentationSorting(DocumentationSortingAttribute.Level.UserRef)]
    [ExecuteInEditMode]
    [AddComponentMenu("")]
    [SaveDuringPlay]
    public class CinemachineConfinerExtended : CinemachineExtension
    {
        [Tooltip("The 2D shape within which the camera is to be contained")]
        [SerializeField]
        private Collider2D _boundingShape2D;

        [Tooltip("If camera is orthographic, screen edges will be confined to the volume.  If not checked, then only the camera center will be confined")]
        [SerializeField]
        private bool _confineScreenEdges = true;

        [Tooltip("How gradually to return the camera to the bounding volume if it goes beyond the borders.  Higher numbers are more gradual.")]
        [Range(0, 10)]
        [SerializeField]
        private float _rubberScaler = 0;

        private List<List<Vector2>> _pathCache;

        public bool IsValid => _boundingShape2D != null;

        protected void OnValidate()
        {
            _rubberScaler = Mathf.Max(0, _rubberScaler);
        }

        protected override void PostPipelineStageCallback(
            CinemachineVirtualCameraBase vcam,
            CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
        {
            if (IsValid)
            {
                if (stage == CinemachineCore.Stage.Body)
                {
                    Vector3 currentDisplacement;
                    if (_confineScreenEdges && state.Lens.Orthographic)
                    {
                        currentDisplacement = ConfineScreenEdges(vcam, ref state);
                    }
                    else
                    {
                        currentDisplacement = ConfinePoint(state.CorrectedPosition);
                    }

                    if (currentDisplacement.sqrMagnitude > float.Epsilon)
                    {
                        if (_rubberScaler > 0)
                        {
                            currentDisplacement = Vector3.Slerp(Vector3.zero, currentDisplacement, Time.deltaTime * _rubberScaler);
                        }

                        state.PositionCorrection += currentDisplacement;
                    }
                }
                else if (stage == CinemachineCore.Stage.Finalize)
                {
                    transform.localPosition = state.CorrectedPosition;
                }
            }
        }

        private bool ValidatePathCache()
        {
            Type colliderType = _boundingShape2D == null ? null : _boundingShape2D.GetType();
            if (colliderType == typeof(PolygonCollider2D))
            {
                PolygonCollider2D poly = _boundingShape2D as PolygonCollider2D;
                if (_pathCache == null || _pathCache.Count != poly.pathCount)
                {
                    _pathCache = new List<List<Vector2>>();
                    for (int i = 0; i < poly.pathCount; ++i)
                    {
                        Vector2[] path = poly.GetPath(i);
                        List<Vector2> dst = new List<Vector2>();
                        for (int j = 0; j < path.Length; ++j)
                        {
                            dst.Add(path[j]);
                        }

                        _pathCache.Add(dst);
                    }
                }
                return true;
            }
            else if (colliderType == typeof(CompositeCollider2D))
            {
                CompositeCollider2D poly = _boundingShape2D as CompositeCollider2D;
                if (_pathCache == null || _pathCache.Count != poly.pathCount)
                {
                    _pathCache = new List<List<Vector2>>();
                    Vector2[] path = new Vector2[poly.pointCount];
                    for (int i = 0; i < poly.pathCount; ++i)
                    {
                        int numPoints = poly.GetPath(i, path);
                        List<Vector2> dst = new List<Vector2>();
                        for (int j = 0; j < numPoints; ++j)
                        {
                            dst.Add(path[j]);
                        }

                        _pathCache.Add(dst);
                    }
                }

                return true;
            }

            _pathCache = null;

            return false;
        }

        private Vector3 ConfinePoint(Vector3 camPos)
        {
            if (_boundingShape2D.OverlapPoint(camPos))
            {
                return Vector3.zero;
            }

            if (!ValidatePathCache())
            {
                return Vector3.zero;
            }

            Vector2 p = camPos;
            Vector2 closest = p;
            float bestDistance = float.MaxValue;
            for (int i = 0; i < _pathCache.Count; ++i)
            {
                int numPoints = _pathCache[i].Count;
                if (numPoints > 0)
                {
                    Vector2 v0 = _boundingShape2D.transform.TransformPoint(_pathCache[i][numPoints - 1]);
                    for (int j = 0; j < numPoints; ++j)
                    {
                        Vector2 v = _boundingShape2D.transform.TransformPoint(_pathCache[i][j]);
                        Vector2 c = Vector2.Lerp(v0, v, p.ClosestPointOnSegment(v0, v));
                        float d = Vector2.SqrMagnitude(p - c);
                        if (d < bestDistance)
                        {
                            bestDistance = d;
                            closest = c;
                        }
                        v0 = v;
                    }
                }
            }
            return closest - p;
        }

        private Vector3 ConfineScreenEdges(CinemachineVirtualCameraBase vcam, ref CameraState state)
        {
            Quaternion rot = Quaternion.Inverse(state.CorrectedOrientation);
            float dy = state.Lens.OrthographicSize;
            float dx = dy * state.Lens.Aspect;
            Vector3 vx = (rot * Vector3.right) * dx;
            Vector3 vy = (rot * Vector3.up) * dy;

            Vector3 displacement = Vector3.zero;
            Vector3 camPos = state.RawPosition;
            const int kMaxIter = 12;
            for (int i = 0; i < kMaxIter; ++i)
            {
                Vector3 d = ConfinePoint((camPos - vy) - vx);

                if (d.AlmostZero())
                {
                    d = ConfinePoint((camPos - vy) + vx);
                }

                if (d.AlmostZero())
                {
                    d = ConfinePoint((camPos + vy) - vx);
                }

                if (d.AlmostZero())
                {
                    d = ConfinePoint((camPos + vy) + vx);
                }

                if (d.AlmostZero())
                {
                    break;
                }

                displacement += d;
                camPos += d;
            }

            return displacement;
        }
    }
}
