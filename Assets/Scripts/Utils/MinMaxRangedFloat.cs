﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Utils
{
    [Serializable]
    public struct MinMaxRangedFloat
    {
        [SerializeField] private float _minLimit;
        [SerializeField] private float _maxLimit;
        [SerializeField] private float _minValue;
        [SerializeField] private float _maxValue;

        public float MinLimit
        {
            get { return _minLimit; }
            set { _minLimit = value; }
        }

        public float MaxLimit
        {
            get { return _maxLimit; }
            set { _maxLimit = value; }
        }

        public float MinValue
        {
            get { return _minValue; }
            set { _minValue = Mathf.Clamp(value, _minLimit, _maxValue); }
        }

        public float MaxValue
        {
            get { return _maxValue; }
            set { _maxValue = Mathf.Clamp(value, _minValue, _maxLimit); }
        }

        public float MinNormalized => Range != 0 ? (_minValue - _minLimit) / Range : 0;
        public float MaxNormalized => Range != 0 ? (_maxValue - _minLimit) / Range : 0;
        public float Range => _maxLimit - _minLimit;

        public MinMaxRangedFloat(float minLimit, float maxLimit, float minValue, float maxValue)
        {
            _minLimit = minLimit;
            _maxLimit = maxLimit;
            _minValue = Mathf.Clamp(minValue, _minLimit, _maxLimit);
            _maxValue = Mathf.Clamp(maxValue, _minValue, _maxLimit);
        }

        public MinMaxRangedFloat(float minLimit, float maxLimit) : this(minLimit, maxLimit, minLimit, maxLimit)
        {
        }

        public float GetValue(float normalizedValue)
        {
            return _minLimit + normalizedValue * Range;
        }

        public float GetValueUnclamped(float normalizedValue)
        {
            return _minLimit + normalizedValue * Range;
        }

        public float GetNormalizedValue(float value)
        {
            return Mathf.Clamp01(GetNormalizedValueUnclamped(value));
        }

        public float GetNormalizedValueUnclamped(float value)
        {
            return Range != 0 ? (value - _minLimit) / Range : Range;
        }
    }
}