﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

namespace DreamTeam.Utils.RendererSorting
{
    /// <summary>
    /// Sets sorting order and layer of a renderer on the same game object
    /// according to the first <see cref="ISortingOptionsProvider"/> found up in hierarchy.
    /// </summary>
    [RequireComponent(typeof(Renderer))]
    public class SortingOptionsController : MonoBehaviour
    {
        [SerializeField] private int _relativeSortingOrder;

        [Inject] private Instantiator _instantiator;

        private SortingOptions _sortingOptions;
        private Renderer _renderer;

        [Inject]
        private void Construct()
        {
            _renderer = GetComponent<Renderer>();

            _sortingOptions = _instantiator.Instantiate<SortingOptions>(
                _renderer.sortingLayerID,
                _renderer.sortingOrder);
            _sortingOptions.SortingLayerChanged += OnSortingLayerChanged;
            _sortingOptions.SortingOrderChanged += OnSortingOrderChanged;
        }

        protected void Start()
        {
            var provider = GetComponentInParent<ISortingOptionsProvider>();
            if (provider != null)
            {
                _sortingOptions.SetParent(provider.SortingOptions, _relativeSortingOrder);
            }
            else
            {
                UnityEngine.Debug.LogError($"{nameof(ISortingOptionsProvider)} is not found in parents of {name}", this);
            }
        }

        protected void OnDestroy()
        {
            _sortingOptions.SortingLayerChanged -= OnSortingLayerChanged;
            _sortingOptions.SortingOrderChanged -= OnSortingOrderChanged;
            _sortingOptions.Dispose();
            _sortingOptions = null;
        }

        private void OnSortingOrderChanged(SortingOptions options, int prevOrder)
        {
            _renderer.sortingOrder = options.SortingOrder;
        }

        private void OnSortingLayerChanged(SortingOptions options)
        {
            _renderer.sortingLayerID = options.SortingLayerId;
        }
    }
}
