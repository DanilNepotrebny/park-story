﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Utils.RendererSorting
{
    public class UISortingOptionsProvider : MonoBehaviour, ISortingOptionsProvider
    {
        private int _initialSortingLayer;
        private int _initialSortingOrder;
        private bool _canvasHadBeenCreated;
        private bool _wasSortingOverriden;
        private Canvas _canvas;
        private GraphicRaycaster _raycaster;

        public SortingOptions SortingOptions { get; private set; }

        [Inject]
        private void Construct(Instantiator instantiator)
        {
            var canvas = this.GetComponentInParent<Canvas>(true);

            _initialSortingLayer = canvas.sortingLayerID;
            _initialSortingOrder = canvas.sortingOrder;

            if (!canvas.isRootCanvas && !canvas.overrideSorting)
            {
                _initialSortingLayer = canvas.rootCanvas.sortingLayerID;
                _initialSortingOrder = canvas.rootCanvas.sortingOrder;
            }

            SortingOptions = instantiator.Instantiate<SortingOptions>(_initialSortingLayer, _initialSortingOrder);
        }

        protected void Awake()
        {
            SortingOptions.SortingLayerChanged += OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged += OnSortingOrderChanged;
            UpdateSortingOptions();
        }

        protected void OnDestroy()
        {
            SortingOptions.SortingLayerChanged -= OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged -= OnSortingOrderChanged;
            SortingOptions.Dispose();
            SortingOptions = null;
        }

        protected void OnEnable()
        {
            TryToEnableSortingOverride();
        }

        private void OnSortingOrderChanged(SortingOptions options, int prevOrder)
        {
            UpdateSortingOptions();
        }

        private void OnSortingLayerChanged(SortingOptions options)
        {
            UpdateSortingOptions();
        }

        private void UpdateSortingOptions()
        {
            if (_initialSortingLayer == SortingOptions.SortingLayerId &&
                _initialSortingOrder == SortingOptions.SortingOrder)
            {
                if (_canvasHadBeenCreated)
                {
                    _raycaster.Dispose();
                    _canvas.Dispose();
                }
                else if (_wasSortingOverriden)
                {
                    _canvas.overrideSorting = false;
                }

                return;
            }

            if (_canvas == null)
            {
                _canvas = GetComponent<Canvas>();
                if (_canvas == null)
                {
                    _canvas = gameObject.AddComponent<Canvas>();
                    _raycaster = gameObject.AddComponent<GraphicRaycaster>();
                    _canvasHadBeenCreated = true;
                }
                else
                {
                    _canvasHadBeenCreated = false;
                    _wasSortingOverriden = _canvas.overrideSorting;
                }

                TryToEnableSortingOverride();
            }

            _canvas.sortingLayerID = SortingOptions.SortingLayerId;
            _canvas.sortingOrder = SortingOptions.SortingOrder;
        }

        private void TryToEnableSortingOverride()
        {
            if (_canvas != null && _canvas.isActiveAndEnabled)
            {
                _canvas.overrideSorting = true;
            }
        }
    }
}