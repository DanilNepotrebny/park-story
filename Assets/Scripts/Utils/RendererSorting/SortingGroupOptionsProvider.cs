﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.Rendering;
using Zenject;

namespace DreamTeam.Utils.RendererSorting
{
    [RequireComponent(typeof(SortingGroup))]
    public class SortingGroupOptionsProvider : MonoBehaviour, ISortingOptionsProvider
    {
        [Inject] private Instantiator _instantiator;

        private SortingGroup _sortingGroup;

        public SortingOptions SortingOptions { get; private set; }

        [Inject]
        private void Construct()
        {
            _sortingGroup = GetComponent<SortingGroup>();

            SortingOptions = _instantiator.Instantiate<SortingOptions>(
                _sortingGroup.sortingLayerID,
                _sortingGroup.sortingOrder);
            SortingOptions.SortingLayerChanged += OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged += OnSortingOrderChanged;
        }

        protected void OnDestroy()
        {
            SortingOptions.SortingLayerChanged -= OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged -= OnSortingOrderChanged;
            SortingOptions.Dispose();
            SortingOptions = null;
        }

        private void OnSortingOrderChanged(SortingOptions options, int prevOrder)
        {
            _sortingGroup.sortingOrder = options.SortingOrder;
        }

        private void OnSortingLayerChanged(SortingOptions options)
        {
            _sortingGroup.sortingLayerID = options.SortingLayerId;
        }
    }
}