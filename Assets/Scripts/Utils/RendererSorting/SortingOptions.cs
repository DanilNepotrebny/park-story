﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine.Assertions;

namespace DreamTeam.Utils.RendererSorting
{
    /// <summary> Adds possibility to read/modify sorting order and layer and subscribe to changes of them. </summary>
    public class SortingOptions : IDisposable
    {
        private readonly Stack<int> _sortingLayers = new Stack<int>();
        private readonly Stack<int> _sortingOrders = new Stack<int>();

        private int _sortingLayerId;
        private int _sortingOrder;

        private SortingOptions _parentOptions;
        private int _sortingOrderOffsetFromParent;

        private static SortingOptionsComparer _sortingOptionsComparer = new SortingOptionsComparer();

        public bool HasParent => _parentOptions != null;

        public int SortingOrder
        {
            get { return _sortingOrder; }
            set
            {
                if (SortingOrder != value)
                {
                    int prevSortingOrder = SortingOrder;
                    _sortingOrder = value;
                    SortingOrderChanged?.Invoke(this, prevSortingOrder);
                }
            }
        }

        public int SortingLayerId
        {
            get { return _sortingLayerId; }
            set
            {
                if (SortingLayerId != value)
                {
                    _sortingLayerId = value;
                    SortingLayerChanged?.Invoke(this);
                }
            }
        }

        public event Action<SortingOptions> Disposed;
        public event SortingLayerChangedDelegate SortingLayerChanged;
        public event SortingOrderChangedDelegate SortingOrderChanged;
        public event ParentChangedDelegate ParentChanged;

        public SortingOptions(int sortingLayerId = 0, int sortingOrder = 0)
        {
            _sortingLayerId = sortingLayerId;
            _sortingOrder = sortingOrder;
        }

        public void PushSortingOrder(int order)
        {
            _sortingOrders.Push(SortingOrder);
            SortingOrder = order;
        }

        public void PopSortingOrder()
        {
            Assert.IsTrue(_sortingOrders.Count > 0, $"There is no sorting order to Pop in SortingOptions. " +
                $"Probably mismatching Push/Pop logic");
            if (_sortingOrders.Any())
            {
                SortingOrder = _sortingOrders.Pop();
            }
        }

        public void PushSortingLayer(int layerId)
        {
            _sortingLayers.Push(SortingLayerId);
            SortingLayerId = layerId;
        }

        public void PopSortingLayer()
        {
            Assert.IsTrue(_sortingLayers.Count > 0, $"There is no layer to Pop in SortingOptions. " +
                $"Probably mismatching Push/Pop logic");
            if (_sortingLayers.Any())
            {
                SortingLayerId = _sortingLayers.Pop();
            }
        }

        public void SetParent([NotNull] SortingOptions newParent, int sortingOrderOffset)
        {
            if (_parentOptions == newParent)
            {
                return;
            }

            var prevParent = _parentOptions;

            ResetParentInternal();

            _parentOptions = newParent;
            _sortingOrderOffsetFromParent = sortingOrderOffset;
            _parentOptions.SortingOrderChanged += OnParentSortingOrderChanged;
            _parentOptions.SortingLayerChanged += OnParentSortingLayerChanged;

            SortingOrder = _parentOptions.SortingOrder + _sortingOrderOffsetFromParent;
            SortingLayerId = _parentOptions.SortingLayerId;

            ParentChanged?.Invoke(this, prevParent);
        }

        public void ResetParent()
        {
            var prevParent = _parentOptions;

            if (ResetParentInternal())
            {
                ParentChanged?.Invoke(this, prevParent);
            }
        }

        // IDisposable interface
        public void Dispose()
        {
            ResetParent();

            while (_sortingOrders.Any())
            {
                PopSortingOrder();
            }

            while (_sortingLayers.Any())
            {
                PopSortingLayer();
            }

            Disposed?.Invoke(this);
        }

       public static void PushToSortingLayer(List<SortingOptions> optionsList, int sortingLayer, int startingSortingOrder = 0)
        {
            optionsList.Sort(_sortingOptionsComparer);

            int sortingOrder = startingSortingOrder;
            foreach (SortingOptions options in optionsList)
            {
                options.PushSortingLayer(sortingLayer);
                options.PushSortingOrder(sortingOrder);
                sortingOrder++;
            }
        }

        public static void PopFromSortingLayer(List<SortingOptions> optionsList)
        {
            foreach (SortingOptions options in optionsList)
            {
                options.PopSortingLayer();
                options.PopSortingOrder();
            }
        }

        private bool ResetParentInternal()
        {
            if (_parentOptions == null)
            {
                return false;
            }

            _parentOptions.SortingOrderChanged -= OnParentSortingOrderChanged;
            _parentOptions.SortingLayerChanged -= OnParentSortingLayerChanged;
            _sortingOrderOffsetFromParent = 0;
            _parentOptions = null;

            return true;
        }

        private void OnParentSortingLayerChanged(SortingOptions parent)
        {
            SortingLayerId = _parentOptions.SortingLayerId;
        }

        private void OnParentSortingOrderChanged(SortingOptions parent, int prevOrder)
        {
            SortingOrder = parent.SortingOrder + _sortingOrderOffsetFromParent;
        }

        #region Inner types

        public delegate void SortingLayerChangedDelegate(SortingOptions options);

        public delegate void SortingOrderChangedDelegate(SortingOptions options, int prevOrder);

        public delegate void ParentChangedDelegate(SortingOptions options, SortingOptions prevParent);

        #endregion
    }
}