﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Utils.RendererSorting
{
    public class SortingOptionsComparer : IComparer<SortingOptions>
    {
        public int Compare(SortingOptions a, SortingOptions b)
        {
            int layerA = SortingLayer.GetLayerValueFromID(a.SortingLayerId);
            int layerB = SortingLayer.GetLayerValueFromID(b.SortingLayerId);
            int result = layerA.CompareTo(layerB);
            if (result == 0)
            {
                result = a.SortingOrder.CompareTo(b.SortingOrder);
            }
            return result;
        }
    }
}