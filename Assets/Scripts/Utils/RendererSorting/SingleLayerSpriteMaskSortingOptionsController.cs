﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Utils.RendererSorting
{
    [RequireComponent(typeof(SpriteMask))]
    public class SingleLayerSpriteMaskSortingOptionsController : MonoBehaviour
    {
        [Inject] private Instantiator _instantiator;

        private SortingOptions _sortingOptions;
        private SpriteMask _spriteMask;
        private int _frontSortingOrderDiff, _backSortingOrderDiff;

        protected void Awake()
        {
            _spriteMask = GetComponent<SpriteMask>();
            _backSortingOrderDiff = _spriteMask.backSortingOrder;
            _frontSortingOrderDiff = _spriteMask.frontSortingOrder;
            Assert.IsTrue(_spriteMask.frontSortingLayerID == _spriteMask.backSortingLayerID,
                $"{nameof(SpriteMask)} has different layers");

            var provider = GetComponentInParent<ISortingOptionsProvider>();
            if (provider != null)
            {
                _sortingOptions = _instantiator.Instantiate<SortingOptions>(
                    provider.SortingOptions.SortingLayerId,
                    provider.SortingOptions.SortingOrder);

                _sortingOptions.SortingLayerChanged += OnSortingLayerChanged;
                _sortingOptions.SortingOrderChanged += OnSortingOrderChanged;

                _sortingOptions.SetParent(provider.SortingOptions, 0);
            }
            else
            {
                UnityEngine.Debug.LogError($"{nameof(ISortingOptionsProvider)} is not found in parents of {name}", this);
            }
        }

        protected void OnDestroy()
        {
            _sortingOptions.SortingLayerChanged -= OnSortingLayerChanged;
            _sortingOptions.SortingOrderChanged -= OnSortingOrderChanged;
            _sortingOptions.Dispose();
            _sortingOptions = null;
        }

        private void OnSortingLayerChanged(SortingOptions options)
        {
            _spriteMask.frontSortingLayerID = options.SortingLayerId;
            _spriteMask.backSortingLayerID = options.SortingLayerId;
        }

        private void OnSortingOrderChanged(SortingOptions options, int prevOrder)
        {
            _spriteMask.frontSortingOrder = options.SortingOrder + _frontSortingOrderDiff;
            _spriteMask.backSortingOrder = options.SortingOrder + _backSortingOrderDiff;
        }
    }
}
