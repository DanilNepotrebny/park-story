﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Utils.Tweening
{
    /// <summary>
    /// Wrapper for LeanTween easing. May contain custom easing curve.
    /// </summary>
    [Serializable]
    public class Easing
    {
        [SerializeField] private LeanTweenType _type;
        [SerializeField] private AnimationCurve _curve;

        public bool IsEnabled => _type != LeanTweenType.notUsed;

        public LTDescr SetupDescriptor(LTDescr descriptor)
        {
            if (_type == LeanTweenType.animationCurve)
            {
                descriptor.setEase(_curve);
            }
            else
            {
                descriptor.setEase(_type);
            }

            return descriptor;
        }
    }
}