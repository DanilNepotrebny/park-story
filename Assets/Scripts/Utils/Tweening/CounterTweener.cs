﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Utils.Tweening
{
    [Serializable]
    public class CounterTweener
    {
        [SerializeField] private Easing _easing;
        [SerializeField] private float _easingDuration = 1;
        [SerializeField] private int _deltaThreshold = 1;

        private LTDescr _tween;

        private bool _isTweening => _tween != null;
        private bool _isEasingEnabled => _easing != null && _easing.IsEnabled;

        public void Start(int from, int to, Action<float> updateCallback, Action completeCallback)
        {
            Stop();

            if (_isEasingEnabled && Mathf.Abs(to - from) > _deltaThreshold)
            {
                _tween = LeanTween.value((float)from, (float)to, _easingDuration);
                _easing.SetupDescriptor(_tween);
                _tween.setOnUpdate(updateCallback);
                _tween.setOnComplete(
                        () =>
                        {
                            completeCallback.Invoke();
                            _tween = null;
                        }
                    );
            }
            else
            {
                updateCallback.Invoke(to);
                completeCallback.Invoke();
            }
        }

        public void Stop()
        {
            if (_isTweening)
            {
                LeanTween.cancel(_tween.uniqueId);
                _tween = null;
            }
        }
    }
}