﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Reflection;
using DreamTeam.Utils.Coroutines;
using UnityEngine;
using Zenject;

namespace DreamTeam.Utils
{
    [RequireComponent(typeof(Animator))]
    public class AnimatorStateWatcher : MonoBehaviour, IAnimatorStateEnterHandler, IAnimatorStateExitHandler
    {
        public Animator Animator { get; private set; }

        public delegate void StateEvent(Animator animator, AnimatorStateInfo stateInfo, int layerIndex);

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public event StateEvent StateEntered;

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public event StateEvent StateExited;

        [Inject]
        private void Construct()
        {
            Animator = GetComponent<Animator>();
        }

        public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            StateEntered?.Invoke(animator, stateInfo, layerIndex);
        }

        public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            StateExited?.Invoke(animator, stateInfo, layerIndex);
        }

        public IEnumerator WaitForStateEntered(string stateName, int layerIndex)
        {
            yield return WaitForStateEvent(nameof(StateEntered), stateName, layerIndex);
        }

        public IEnumerator WaitForStateExited(string stateName, int layerIndex)
        {
            yield return WaitForStateEvent(nameof(StateExited), stateName, layerIndex);
        }

        private IEnumerator WaitForStateEvent(string eventName, string stateName, int layerIndex)
        {
            yield return new WaitForEvent<Animator, AnimatorStateInfo, int, AnimatorStateWatcher>(
                    this,
                    eventName,
                    (animator, stateInfo, index) => layerIndex == index && stateInfo.IsName(stateName)
                );
        }
    }
}