﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils
{
    [RequireComponent(typeof(Canvas))]
    public class CanvasSortingOverrider : MonoBehaviour
    {
        [SerializeField, SortingLayer] private int _sortingLayer;
        [SerializeField] private int _sortingOrder;

        private bool _previousIsOverriding;
        private int _previousLayer;
        private int _previousOrder;

        private Canvas _canvas => GetComponent<Canvas>();

        protected void OnEnable()
        {
            _previousIsOverriding = _canvas.overrideSorting;
            _previousLayer = _canvas.sortingLayerID;
            _previousOrder = _canvas.sortingOrder;

            _canvas.overrideSorting = true;
            _canvas.sortingLayerID = _sortingLayer;
            _canvas.sortingOrder = _sortingOrder;
        }

        protected void OnDisable()
        {
            _canvas.overrideSorting = _previousIsOverriding;
            _canvas.sortingLayerID = _previousLayer;
            _canvas.sortingOrder = _previousOrder;
        }

        protected void Reset()
        {
            var parentCanvas = transform.parent.GetComponentInParent<Canvas>();
            if (parentCanvas != null)
            {
                _sortingLayer = parentCanvas.sortingLayerID;
                _sortingOrder = parentCanvas.sortingOrder + 1;
            }
        }
    }
}