﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils
{
    [RequireComponent(typeof(ParticleSystem))]
    public class ParticleSystemRendererModifier : MonoBehaviour
    {
        [SerializeField, SortingLayer] private int _sortingLayer;
        [SerializeField] private int _sortingOrder;

        private Renderer _psRenderer;

        protected void Awake()
        {
            _psRenderer = GetComponent<ParticleSystem>().GetComponent<Renderer>();
        }

        private void OnEnable()
        {
            _psRenderer.sortingLayerID = _sortingLayer;
            _psRenderer.sortingOrder = _sortingOrder;
        }
    }
}