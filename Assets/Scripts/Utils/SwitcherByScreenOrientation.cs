﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Utils
{
    [ExecuteInEditMode]
    public class SwitcherByScreenOrientation : MonoBehaviour, IInitable, IDisposable
    {
        [SerializeField] private bool _switchContent = true;
        [SerializeField, ContainsAllEnumKeys] private SwitchableObjectsList _switchableObjects;

        [SerializeField, HideInInspector] private ScreenOrientationNotifier.Orientation _orientation;

        [Inject] private ScreenOrientationNotifier _orientationNotifier;

        public GameObject Current => _switchableObjects[_orientation];
        public ScreenOrientationNotifier.Orientation Orientation => _orientationNotifier.CurrentOrientation;

        /// <summary>
        /// First game object - is the new one
        /// Second game object - is the previous one
        /// </summary>
        public event Action<GameObject, GameObject> Switched;

        public GameObject GetContent(ScreenOrientationNotifier.Orientation orientation)
        {
            return _switchableObjects[orientation];
        }

        public void Init()
        {
            _orientationNotifier.OrientationChanged += OnOrientationChanged;
        }

        public void Dispose()
        {
            if (_orientationNotifier != null)
            {
                _orientationNotifier.OrientationChanged -= OnOrientationChanged;
                _orientationNotifier = null;
            }
        }

        protected void Awake()
        {
            if (!EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                _orientationNotifier = gameObject.GetComponent<ScreenOrientationNotifier>();
                if (_orientationNotifier == null)
                {
                    _orientationNotifier = gameObject.AddComponent<ScreenOrientationNotifier>();
                    _orientationNotifier.hideFlags = HideFlags.HideAndDontSave;
                }
            }
        }

        protected void OnEnable()
        {
            if (!EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                _orientationNotifier.OrientationChanged += OnOrientationChanged;
            }

            if (_orientationNotifier != null)
            {
                SetOrientation(_orientationNotifier.CurrentOrientation);
            }
        }

        protected void OnDisable()
        {
            if (!EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                _orientationNotifier.OrientationChanged -= OnOrientationChanged;
            }
        }

        private void SetOrientation(ScreenOrientationNotifier.Orientation orientation)
        {
            if (_switchableObjects == null)
            {
                return;
            }

            _orientation = orientation;

            if (Current != null)
            {
                Current.SetActive(true);
            }

            foreach (KeyValuePair<ScreenOrientationNotifier.Orientation, GameObject> valuePair in _switchableObjects)
            {
                if (valuePair.Value == null)
                {
                    continue;
                }

                GameObject go = valuePair.Value;

                if (_switchContent && Current != null)
                {
                    SwitchGameObjectsChilds(go, Current);
                }

                if (go != Current)
                {
                    go.SetActive(false);
                }
            }
        }

        private void OnOrientationChanged(ScreenOrientationNotifier.Orientation newOrientation)
        {
            if (newOrientation == _orientation)
            {
                return;
            }

            GameObject previousGO = Current;

            SetOrientation(newOrientation);

            Switched?.Invoke(Current, previousGO);
        }

        private static void SwitchGameObjectsChilds([NotNull] GameObject sourceGO, [NotNull] GameObject targetGO)
        {
            if (sourceGO == targetGO)
            {
                return;
            }

            Transform[] children = new Transform[sourceGO.transform.childCount];
            for (int i = 0; i < children.Length; i++)
            {
                children[i] = sourceGO.transform.GetChild(i);
            }

            foreach (Transform child in children)
            {
                child.SetParent(targetGO.transform, false);
            }
        }

        #region Internal types

        [Serializable]
        private class SwitchableObjectsList : KeyValueList<ScreenOrientationNotifier.Orientation, GameObject>
        {
        }

        #endregion
    }
}