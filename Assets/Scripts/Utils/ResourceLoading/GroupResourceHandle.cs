﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils.AsyncProcesses;

namespace DreamTeam.Utils.ResourceLoading
{
    public class GroupResourceHandle : IResourceHandle
    {
        private IEnumerable<IResourceHandle> _handles;

        public BaseAsyncProcess LoadProcess { get; private set; }

        public GroupResourceHandle(ref IEnumerable<IResourceHandle> handles)
        {
            _handles = handles;
            handles = null;

            LoadProcess = new GroupAsyncProcess(_handles.Select(handle => handle.LoadProcess).ToArray());
        }

        public void Dispose()
        {
            if (_handles == null)
            {
                UnityEngine.Debug.LogError($"{nameof(GroupResourceHandle)} is already disposed.");
                return;
            }

            foreach (IResourceHandle handle in _handles)
            {
                handle.Dispose();
            }

            _handles = null;
            LoadProcess = null;
        }
    }
}
