﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.AsyncProcesses;

namespace DreamTeam.Utils.ResourceLoading
{
    public interface IResourceLoader
    {
        BaseAsyncProcess Preload();

        IResourceHandle Load();
    }
}