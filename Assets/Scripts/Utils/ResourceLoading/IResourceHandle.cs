﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils.AsyncProcesses;

namespace DreamTeam.Utils.ResourceLoading
{
    public interface IResourceHandle : IDisposable
    {
        BaseAsyncProcess LoadProcess { get; }
    }
}