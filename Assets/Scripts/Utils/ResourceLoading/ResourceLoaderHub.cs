﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;

namespace DreamTeam.Utils.ResourceLoading
{
    public class ResourceLoaderHub : MonoBehaviour, IResourceLoaderHub
    {
        public BaseAsyncProcess Preload()
        {
            IEnumerable<IResourceLoader> resourceLoaders = CollectResourceLoaders();
            return new GroupAsyncProcess(resourceLoaders.Select(p => p.Preload()).ToArray());
        }

        public IResourceHandle Load()
        {
            IEnumerable<IResourceLoader> loaders = CollectResourceLoaders();
            IEnumerable<IResourceHandle> handles = loaders.Select(p => p.Load()).ToArray();
            return new GroupResourceHandle(ref handles);
        }
        
        private IEnumerable<IResourceLoader> CollectResourceLoaders()
        {
            List<IResourceLoader> preloaders = transform.GetComponentsInChildrenWhile<IResourceLoader>(
                    node => node == transform || node.GetComponent<IResourceLoaderHub>() == null,
                    true
                );

            IEnumerable<IResourceLoader> resourceLoaders = preloaders.Where(p => p != (IResourceLoader)this);
            return resourceLoaders;
        }
    }
}
