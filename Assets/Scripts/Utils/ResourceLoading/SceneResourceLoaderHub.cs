﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.SceneLoading;
using DreamTeam.Utils.AsyncProcesses;

namespace DreamTeam.Utils.ResourceLoading
{
    public class SceneResourceLoaderHub : ResourceLoaderHub, ISceneLoadable
    {
        BaseAsyncProcess ISceneLoadable.Load()
        {
            return Preload();
        }
    }
}
