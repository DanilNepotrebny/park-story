﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;

namespace DreamTeam.Utils.ResourceLoading
{
    public class SingleResourceHandle : ActionDisposable, IResourceHandle
    {
        public BaseAsyncProcess LoadProcess { get; }

        public SingleResourceHandle(
                BaseAsyncProcess loadProcess
            )
        {
            LoadProcess = loadProcess;
        }

        public SingleResourceHandle(
                BaseAsyncProcess loadProcess,
                [NotNull] Action<ActionDisposable> actionOnDispose
            ) :
            base(actionOnDispose)
        {
            LoadProcess = loadProcess;
        }
    }
}