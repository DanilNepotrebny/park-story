﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DreamTeam.Utils
{
    [RequireComponent(typeof(RectTransform))]
    public class RandomSpawnerInRect : MonoBehaviour
    {
        [SerializeField] private float _minSpawnInterval;
        [SerializeField] private float _maxSpawnInterval;

        [Space, SerializeField, MinMaxRange(1, 10)] private MinMaxRange _spawnsCountPerTime;

        [Space, SerializeField, MinMaxRange(0, 10)] private MinMaxRange _initialDelay;

        [Space, SerializeField] private SampleWeightsList _sampleWeights;

        private RectTransform _rectTransform;
        private RectTransform _randomSample => RandomUtils.RandomElement(_sampleWeights);

        private Vector3 _randomPoint
        {
            get
            {
                Vector3[] corners = new Vector3[4];
                _rectTransform.GetLocalCorners(corners);
                Vector2 min = corners[0];
                Vector2 max = corners[2];

                return new Vector2(
                        Random.Range(min.x, max.x),
                        Random.Range(min.y, max.y)
                    );
            }
        }

        protected void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
        }

        protected void OnEnable()
        {
            StartCoroutine(nameof(SpawnRoutine));
        }

        protected void OnDisable()
        {
            StopCoroutine(nameof(SpawnRoutine));
        }

        private IEnumerator SpawnRoutine()
        {
            yield return WaitForInterval(_initialDelay.GetRandomValue());

            while (true)
            {
                yield return SpawnCount((int)_spawnsCountPerTime.GetRandomValue());
                yield return WaitForInterval(Random.Range(_minSpawnInterval, _maxSpawnInterval));
            }
        }

        private IEnumerator WaitForInterval(float interval)
        {
            if (interval > 0)
            {
                yield return new WaitForSeconds(interval);
            }
            else
            {
                yield return null;
            }
        }

        private IEnumerator SpawnCount(int count)
        {
            for (int counter = 0; counter < count; counter++)
            {
                Transform clone = Instantiate(_randomSample);
                clone.SetParent(_rectTransform, false);
                clone.localPosition = _randomPoint;
                yield return null;
            }
        }

        [Serializable]
        private class SampleWeightsList : KeyValueList<RectTransform, int>
        {
        }
    }
}