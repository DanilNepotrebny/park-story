﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Handler of the animator state enter event. Works in conjuction with <see cref="AnimatorStateEnterNotifier"/>.
    /// </summary>
    public interface IAnimatorStateEnterHandler
    {
        /// <summary>
        /// Called from <see cref="AnimatorStateEnterNotifier"/> when animator exits specified state.
        /// </summary>
        void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex);
    }
}