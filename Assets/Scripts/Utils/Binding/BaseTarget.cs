﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Utils.Binding
{
    /// <summary>
    /// Base class of data binding target. Can be used in <see cref="OneWayBinding"/>.
    /// </summary>
    [RequireComponent(typeof(OneWayBinding))]
    public abstract class BaseTarget : MonoBehaviour
    {
        /// <summary>
        /// Returns handler bound to binding target.
        /// </summary>
        public abstract Action<object, IDeferredInvocationHandle> Bind();

        /// <summary>
        /// Undoes binding.
        /// </summary>
        public abstract void Unbind();
    }
}