﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.Binding
{
    /// <summary>
    /// Base abstract class for data-binding adapters. Purpose of adapter is to adapt source property value to a
    /// target property in a some way (e.g. to convert types or to modify value according to specified rule).
    /// </summary>
    [RequireComponent(typeof(OneWayBinding))]
    public abstract class BaseBindingAdapter : MonoBehaviour
    {
        /// <summary>
        /// Returns modified source value.
        /// </summary>
        public abstract object AdaptValue(object value);
    }
}