﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DreamTeam.Utils.Binding
{
    /// <summary>
    /// Associates unity object (e.g. sprite or prefab) for each source integer value. Supports default value.
    /// One way you can think about it is that it's like switch-case operator.
    /// </summary>
    public class IntToUnityObjectAdapter : BaseBindingAdapter
    {
        [SerializeField] private int[] _keys;
        [SerializeField] private Object[] _values;
        [SerializeField] private Object _default;

        public override object AdaptValue(object value)
        {
            int index = Array.IndexOf(_keys, value);
            if (index >= 0)
            {
                return _values[index];
            }

            return _default;
        }
    }
}