﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Utils.Binding
{
    /// <summary>
    /// Component that performs one way data binding. Can modify value from source to target via binding adapter.
    /// Supports deferred invocation of source delegate.
    /// </summary>
    public class OneWayBinding : MonoBehaviour
    {
        [SerializeField] private BaseSource _source;
        [SerializeField] private BaseBindingAdapter _adapter;
        [SerializeField] private BaseTarget _target;

        private Action<object, IDeferredInvocationHandle> _updateValue;

        /// <summary>
        /// Unity callback
        /// </summary>
        protected void Start()
        {
            Bind();
        }

        /// <summary>
        /// Unity callback
        /// </summary>
        protected void OnDestroy()
        {
            Unbind();
        }

        private void Bind()
        {
            Unbind();

            _updateValue = _target.Bind();
            _source.Bind(UpdateValue);
        }

        private void UpdateValue(object value, IDeferredInvocationHandle handle)
        {
            if (_adapter != null)
            {
                value = _adapter.AdaptValue(value);
            }

            if (value != null && value.Equals(null))
            {
                // Workaround for serialized UnityEngine.Object "None" value. You can't set such value
                // through reflection for some reason, so we have to make it really null.
                value = null;
            }

            _updateValue(value, handle);
        }

        private void Unbind()
        {
            _source.Unbind();
            _target.Unbind();

            _updateValue = null;
        }
    }
}