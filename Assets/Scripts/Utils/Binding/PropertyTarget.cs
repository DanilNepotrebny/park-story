﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Reflection;
using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Utils.Binding
{
    /// <summary>
    /// Binding target that performs binding to property by name.
    /// </summary>
    public class PropertyTarget : BaseTarget
    {
        [SerializeField] private Component _targetComponent;
        [SerializeField] private string _targetPropertyName;

        private PropertyInfo _targetProperty;

        public override Action<object, IDeferredInvocationHandle> Bind()
        {
            _targetProperty = GetProperty();
            return UpdateValue;
        }

        public override void Unbind()
        {
            _targetProperty = null;
        }

        private void UpdateValue(object obj, IDeferredInvocationHandle handle)
        {
            _targetProperty.SetValue(_targetComponent, obj);
        }

        private PropertyInfo GetProperty()
        {
            PropertyInfo property = _targetComponent.GetType().GetProperty(_targetPropertyName);
            if (property == null)
            {
                throw new InvalidOperationException($"Property with name {_targetPropertyName} is not found");
            }

            return property;
        }
    }
}