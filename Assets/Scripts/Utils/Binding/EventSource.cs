﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Reflection;
using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Utils.Binding
{
    /// <summary>
    /// Binding source which performs binding to event by name.
    /// </summary>
    public class EventSource : BaseSource
    {
        [SerializeField] private Component _sourceComponent;
        [SerializeField] private string _sourceEventName;

        private EventInfo _event;
        private Delegate _eventHandler;

        /// <summary>
        /// Source component.
        /// </summary>
        protected Component Source => _sourceComponent;

        /// <summary>
        /// Name of the event.
        /// </summary>
        protected string EventName
        {
            get { return _sourceEventName; }
            set { _sourceEventName = value; }
        }

        protected override void Bind()
        {
            _event = GetEvent();

            Type delegateType = _event.EventHandlerType;
            MethodInfo invoke = delegateType.GetMethod("Invoke");
            ParameterInfo[] parameters = invoke.GetParameters();
            switch (parameters.Length)
            {
                case 0:
                    _eventHandler = (Action)OnEvent;
                    break;

                case 1:
                    Type type = parameters[0].ParameterType;
                    if (type == typeof(IDeferredInvocationHandle))
                    {
                        _eventHandler = (Action<IDeferredInvocationHandle>)OnEventWithDeferring;
                    }
                    else
                    {
                        _eventHandler = SelectHandler(type, false);
                    }

                    break;

                case 2:
                    _eventHandler = SelectHandler(parameters[0].ParameterType, true);
                    break;

                default:
                    throw new NotSupportedException(
                            $"Event {_event.Name} has unsupported number of parameters: {parameters.Length}"
                        );
            }

            _event.AddEventHandlerAOT(Source, _eventHandler);
        }

        public override void Unbind()
        {
            if (_event != null)
            {
                _event.RemoveEventHandlerAOT(Source, _eventHandler);

                _event = null;
                _eventHandler = null;
            }
        }

        private EventInfo GetEvent()
        {
            EventInfo eventInfo = Source.GetType().GetEvent(EventName);
            if (eventInfo == null)
            {
                throw new InvalidOperationException($"Event {EventName} is not found");
            }

            return eventInfo;
        }

        private void OnEvent()
        {
            Handler(null, null);
        }

        private void OnEventWithDeferring(IDeferredInvocationHandle invocation)
        {
            Handler(null, invocation);
        }

        private Delegate SelectHandler(Type type, bool hasDelegate)
        {
            // In C# there is no contravariance for non-reference parameters in delegates,
            // so we define handlers for most used primitive types

            Delegate handler;
            if (type == typeof(int))
            {
                if (hasDelegate)
                {
                    handler = (Action<int, IDeferredInvocationHandle>)OnIntChangedWithDeferring;
                }
                else
                {
                    handler = (Action<int>)OnIntChanged;
                }
            }
            else if (type == typeof(long))
            {
                if (hasDelegate)
                {
                    handler = (Action<long, IDeferredInvocationHandle>)OnLongChangedWithDeferring;
                }
                else
                {
                    handler = (Action<long>)OnLongChanged;
                }
            }
            else if (type == typeof(float))
            {
                if (hasDelegate)
                {
                    handler = (Action<float, IDeferredInvocationHandle>)OnFloatChangedWithDeferring;
                }
                else
                {
                    handler = (Action<float>)OnFloatChanged;
                }
            }
            else if (type == typeof(double))
            {
                if (hasDelegate)
                {
                    handler = (Action<double, IDeferredInvocationHandle>)OnDoubleChangedWithDeferring;
                }
                else
                {
                    handler = (Action<double>)OnDoubleChanged;
                }
            }
            else if (type == typeof(bool))
            {
                if (hasDelegate)
                {
                    handler = (Action<bool, IDeferredInvocationHandle>)OnBoolChangedWithDeferring;
                }
                else
                {
                    handler = (Action<bool>)OnBoolChanged;
                }
            }
            else
            {
                if (hasDelegate)
                {
                    handler = (Action<object, IDeferredInvocationHandle>)OnObjectChangedWithDeferring;
                }
                else
                {
                    handler = (Action<object>)OnObjectChanged;
                }
            }

            return handler;
        }

        private void OnIntChanged(int value)
        {
            Handler(value, null);
        }

        private void OnLongChanged(long value)
        {
            Handler(value, null);
        }

        private void OnFloatChanged(float value)
        {
            Handler(value, null);
        }

        private void OnDoubleChanged(double value)
        {
            Handler(value, null);
        }

        private void OnBoolChanged(bool value)
        {
            Handler(value, null);
        }

        private void OnObjectChanged(object value)
        {
            Handler(value, null);
        }

        private void OnIntChangedWithDeferring(int value, IDeferredInvocationHandle handle)
        {
            Handler(value, handle);
        }

        private void OnLongChangedWithDeferring(long value, IDeferredInvocationHandle handle)
        {
            Handler(value, handle);
        }

        private void OnFloatChangedWithDeferring(float value, IDeferredInvocationHandle handle)
        {
            Handler(value, handle);
        }

        private void OnDoubleChangedWithDeferring(double value, IDeferredInvocationHandle handle)
        {
            Handler(value, handle);
        }

        private void OnBoolChangedWithDeferring(bool value, IDeferredInvocationHandle handle)
        {
            Handler(value, handle);
        }

        private void OnObjectChangedWithDeferring(object value, IDeferredInvocationHandle handle)
        {
            Handler(value, handle);
        }
    }
}