﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Utils.Binding
{
    /// <summary>
    /// Base class of data binding source. Can be used in <see cref="OneWayBinding"/>.
    /// </summary>
    [RequireComponent(typeof(OneWayBinding))]
    public abstract class BaseSource : MonoBehaviour
    {
        /// <summary>
        /// Handler bound to binding source.
        /// </summary>
        protected Action<object, IDeferredInvocationHandle> Handler { get; private set; }

        /// <summary>
        /// Binds specified handler to binding source.
        /// </summary>
        public void Bind(Action<object, IDeferredInvocationHandle> handler)
        {
            Handler = handler;
            Bind();
        }

        /// <summary>
        /// Undoes binding.
        /// </summary>
        public abstract void Unbind();

        /// <summary>
        /// Binds inner handler to binding source.
        /// </summary>
        protected abstract void Bind();
    }
}