﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Reflection;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace DreamTeam.Utils.Binding
{
    /// <summary>
    /// Binding target that performs binding to a custom method (both static and instance) by name. Method can have no parameters, one parameter of compatible type or deferred invocation.
    /// </summary>
    public class MethodTarget : BaseTarget
    {
        [SerializeField] private Object _targetObject;
        [SerializeField] private string _targetMethodName;
        [Inject] private Instantiator _instantiator;

        private MethodInfo _targetMethod;
        private object[] _parameters;
        private bool _hasDeferredInvocation;

        /// <summary>
        /// Helper method for using in binding when you need to instantiate some object at current position.
        /// </summary>
        public void Instantiate(GameObject original)
        {
            _instantiator.Instantiate(original, transform.position, transform.rotation);
        }

        public override Action<object, IDeferredInvocationHandle> Bind()
        {
            InitMethod();

            return UpdateValue;
        }

        public override void Unbind()
        {
            _targetMethod = null;
            _parameters = null;
        }

        private void UpdateValue(object obj, IDeferredInvocationHandle handle)
        {
            int index = _parameters.Length - 1;
            if (_hasDeferredInvocation)
            {
                // Last parameter is always deferred invocation
                _parameters[index] = handle;
                index--;
            }

            if (index >= 0)
            {
                _parameters[index] = obj;
            }

            _targetMethod.Invoke(_targetObject, _parameters);
        }

        private void InitMethod()
        {
            MethodInfo[] methods = _targetObject.GetType()
                .GetMethods(
                        BindingFlags.Public |
                        BindingFlags.Instance |
                        BindingFlags.Static |
                        BindingFlags.FlattenHierarchy
                    );

            foreach (MethodInfo m in methods)
            {
                if (m.Name != _targetMethodName)
                {
                    continue;
                }

                var parameters = m.GetParameters();

                // TODO: Make method selection logic smarter (right now it just takes first suitable method)
                switch (parameters.Length)
                {
                    case 0:
                        _targetMethod = m;
                        _hasDeferredInvocation = false;
                        break;

                    case 1:
                        _targetMethod = m;
                        _hasDeferredInvocation = parameters[0].ParameterType == typeof(DeferredInvocation);
                        break;

                    case 2:
                        if (parameters[1].ParameterType == typeof(DeferredInvocation))
                        {
                            _targetMethod = m;
                            _hasDeferredInvocation = true;
                            break;
                        }

                        continue;

                    default:
                        continue;
                }

                if (_targetMethod != null)
                {
                    _parameters = new object[parameters.Length];
                    break;
                }
            }

            if (_targetMethod == null)
            {
                throw new InvalidOperationException($"Suitable method with name {_targetMethodName} is not found");
            }
        }
    }
}