﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Utils.Binding
{
    /// <summary>
    /// Binding target that performs binding to animator parameter. Supports deferring of source delegate until
    /// any of the specified animator states is exited.
    /// </summary>
    public class AnimatorTarget : BaseTarget, IAnimatorStateExitHandler
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private string _parameter;
        [SerializeField] private List<string> _waitStateFinish;

        private int _parameterHash;
        private Action<object> _updateParameter;

        private IDeferredInvocationHandle _deferredInvocationHandle;
        private int[] _waitStateFinishHashes;

        public override Action<object, IDeferredInvocationHandle> Bind()
        {
            _parameterHash = Animator.StringToHash(_parameter);
            _updateParameter = SelectHandler(GetParameter(_parameterHash));

            if (_waitStateFinish.Count > 0)
            {
                _waitStateFinishHashes = new int[_waitStateFinish.Count];
                for (int i = 0; i < _waitStateFinishHashes.Length; i++)
                {
                    _waitStateFinishHashes[i] = Animator.StringToHash(_waitStateFinish[i]);
                }
            }

            return UpdateParameter;
        }

        public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (_deferredInvocationHandle != null &&
                _waitStateFinishHashes != null &&
                Array.IndexOf(_waitStateFinishHashes, stateInfo.shortNameHash) >= 0)
            {
                _deferredInvocationHandle.Unlock();
                _deferredInvocationHandle = null;
            }
        }

        public override void Unbind()
        {
            _updateParameter = null;
            _waitStateFinishHashes = null;
        }

        private void UpdateParameter(object value, IDeferredInvocationHandle handle)
        {
            if (handle != null && _waitStateFinishHashes != null)
            {
                _deferredInvocationHandle = handle.Lock();
            }

            _updateParameter(value);
        }

        private AnimatorControllerParameter GetParameter(int hash)
        {
            foreach (var parameter in _animator.parameters)
            {
                if (parameter.nameHash == hash)
                {
                    return parameter;
                }
            }

            return null;
        }

        private Action<object> SelectHandler(AnimatorControllerParameter parameter)
        {
            switch (parameter.type)
            {
                case AnimatorControllerParameterType.Bool:
                    return UpdateBool;
                case AnimatorControllerParameterType.Float:
                    return UpdateFloat;
                case AnimatorControllerParameterType.Int:
                    return UpdateInt;
                case AnimatorControllerParameterType.Trigger:
                    return UpdateTrigger;
                default:
                    throw new NotSupportedException($"Parameter {parameter.name} has unsupported type.");
            }
        }

        private void UpdateInt(object value)
        {
            _animator.SetInteger(_parameterHash, (int)value);
        }

        private void UpdateBool(object value)
        {
            _animator.SetBool(_parameterHash, (bool)value);
        }

        private void UpdateFloat(object value)
        {
            _animator.SetFloat(_parameterHash, (float)value);
        }

        private void UpdateTrigger(object value)
        {
            _animator.SetTrigger(_parameterHash);
        }
    }
}