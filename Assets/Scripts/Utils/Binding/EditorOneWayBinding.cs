﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR
using UnityEngine;

namespace DreamTeam.Utils.Binding
{
    /// <summary>
    /// Editor version of the OneWayBinding
    /// </summary>
    [ExecuteInEditMode]
    public class EditorOneWayBinding : OneWayBinding
    {
    }
}
#endif