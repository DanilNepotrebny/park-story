﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace DreamTeam.Utils.Binding
{
    /// <summary>
    /// Binding source which performs binding to property by name. Property should be supplied with event
    /// "onPropertyChanged" for a property called "property".
    /// </summary>
    public class PropertySource : EventSource
    {
        [SerializeField] private string _sourcePropertyName;

        protected override void Bind()
        {
            base.Bind();

            Handler(GetProperty().GetValue(Source), null);
        }

        /// <summary>
        /// Unity event.
        /// </summary>
        protected void OnValidate()
        {
            // Get event called "PropertyChanged" if property is called "property" or "IsProperty"
            StringBuilder eventNameBuilder = new StringBuilder(_sourcePropertyName);
            if (eventNameBuilder.Length >= 3 &&
                eventNameBuilder[0] == 'I' &&
                eventNameBuilder[1] == 's' &&
                char.IsUpper(eventNameBuilder[2]))
            {
                eventNameBuilder.Remove(0, 2);
            }

            eventNameBuilder.Append("Changed");

            EventName = eventNameBuilder.ToString();
        }

        private PropertyInfo GetProperty()
        {
            PropertyInfo property = Source.GetType().GetProperty(_sourcePropertyName);
            if (property == null)
            {
                throw new InvalidOperationException($"Property with name {_sourcePropertyName} is not found");
            }

            return property;
        }
    }
}