﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using UnityEngine.Playables;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Used as a workaround to overcome compiler limitations when some code is accessed only via reflection or
    /// from native code.
    /// - Use method or property here to prevent code stripping.
    /// - Use generic class with value type parameter to make compiler include it's code into assembly when
    /// compiling in AOT mode.
    /// </summary>
    [Obsolete("This class should not be used directly", true)]
    public static class CompilerHints
    {
        private static void PreserveSpriteRendererSpriteProperty(SpriteRenderer spriteRenderer)
        {
            spriteRenderer.sprite = spriteRenderer.sprite;
        }

        private static void PreservePlayableDirectorStoppedProperty(PlayableDirector director)
        {
            director.stopped += (PlayableDirector d) => {};
        }
    }
}