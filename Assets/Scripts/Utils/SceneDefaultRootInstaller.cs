﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Zenject;

namespace DreamTeam.Utils
{
    public class SceneDefaultRootInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.DefaultParent = gameObject.scene.FindSceneRoot().transform;
        }
    }
}