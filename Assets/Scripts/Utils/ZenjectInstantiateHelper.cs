﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Helper that executes before any Zenject script and just disables self. Needed for Instantiation.
    ///
    /// The problem is that Zenject needs disabled object for instantiation to be able bind it's dependencies before
    /// object receives Awake callback. Internally Zenject uses Object.Instantiate which has the rule: whether you pass
    /// active gameobject - you receive active one and vice verse. That's why when you pass active gameobject to
    /// instantiate it Zenject disables it before actual instantiation. That might led to a problem when due to deactivation
    /// original object loses its state (for example Animator component).
    ///
    /// To address such unwanted behavior we add this component to the origin object and Object.Instantiate it.
    /// Then this component disables newly created object before any other which you must Instantiate again using Zenject
    /// </summary>
    public class ZenjectInstantiateHelper : MonoBehaviour
    {
        [SerializeField, HideInInspector] private bool _deactivateOnAwake;

        public bool DeactivateOnAwake
        {
            get { return _deactivateOnAwake; }
            set { _deactivateOnAwake = value; }
        }

        protected void Awake()
        {
            if (_deactivateOnAwake)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
