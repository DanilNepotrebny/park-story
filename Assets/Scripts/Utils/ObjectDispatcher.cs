﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;

namespace DreamTeam.Utils
{
    /// <summary>
    /// Sends specified object to subscribers that are waiting for objects of concrete type.
    /// </summary>
    /// <typeparam name="TBase">Base type of dispatched objects</typeparam>
    public class ObjectDispatcher<TBase> where TBase : class
    {
        private Dictionary<Type, IDispatcher> _dispatchers = new Dictionary<Type, IDispatcher>();

        public void AddSubscriber<TObject>(Action<TObject> subscriber)
            where TObject : class, TBase
        {
            Dispatcher<TObject> dispatcher = (Dispatcher<TObject>)GetDispatcher(typeof(TObject));
            if (dispatcher == null)
            {
                dispatcher = new Dispatcher<TObject>();
                _dispatchers.Add(typeof(TObject), dispatcher);
            }

            dispatcher.AddSubscriber(subscriber);
        }

        public void RemoveSubscriber<TObject>(Action<TObject> subscriber)
            where TObject : class, TBase
        {
            Dispatcher<TObject> dispatcher = (Dispatcher<TObject>)GetDispatcher(typeof(TObject));
            if (dispatcher == null)
            {
                dispatcher = new Dispatcher<TObject>();
                _dispatchers.Add(typeof(TObject), dispatcher);
            }

            dispatcher.RemoveSubscriber(subscriber);
        }

        public void Send(TBase obj)
        {
            foreach (Type type in obj.GetType().EnumerateTypeHierarchy())
            {
                GetDispatcher(type)?.Send(obj);
            }
        }

        private IDispatcher GetDispatcher(Type objectType)
        {
            IDispatcher dispatcher;
            if (_dispatchers.TryGetValue(objectType, out dispatcher))
            {
                return dispatcher;
            }

            return null;
        }

        #region Inner types

        private interface IDispatcher
        {
            void Send(TBase obj);
        }

        private class Dispatcher<TDerived> : IDispatcher
            where TDerived : class, TBase
        {
            private List<Action<TDerived>> _subscribers = new List<Action<TDerived>>();

            public void AddSubscriber(Action<TDerived> subscriber)
            {
                _subscribers.Add(subscriber);
            }

            public void RemoveSubscriber(Action<TDerived> subscriber)
            {
                _subscribers.Remove(subscriber);
            }

            public void Send(TBase obj)
            {
                TDerived concreteObject = (TDerived)obj;
                foreach (Action<TDerived> subscriber in _subscribers.ToArray())
                {
                    subscriber.Invoke(concreteObject);
                }
            }
        }

        #endregion
    }
}