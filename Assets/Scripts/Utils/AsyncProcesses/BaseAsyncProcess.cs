﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Utils.AsyncProcesses
{
    public abstract class BaseAsyncProcess : CustomYieldInstruction
    {
        public bool IsCompleted { get; private set; }
        public abstract float Progress { get; }

        public override bool keepWaiting => !IsCompleted;

        public event Action Completed;

        public void DoWhenCompleted([NotNull] Action action)
        {
            if (IsCompleted)
            {
                action();
            }
            else
            {
                Completed += action;
            }
        }

        protected void OnCompleted()
        {
            IsCompleted = true;
            Completed?.Invoke();
        }
    }
}