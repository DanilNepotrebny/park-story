﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;

namespace DreamTeam.Utils.AsyncProcesses
{
    public class GroupAsyncProcess : BaseAsyncProcess
    {
        private Task[] _tasks;
        private int _countToWait;

        public override float Progress => IsCompleted ? 1f : _tasks.Sum(t => t.Progress);

        public GroupAsyncProcess(IEnumerable<BaseAsyncProcess> processes)
        {
            Task[] tasks = processes
                .Where(p => p != null)
                .Select(p => new Task(p))
                .ToArray();

            SetTasks(tasks);
        }

        public GroupAsyncProcess(Task[] tasks)
        {
            SetTasks(tasks);
        }

        private void SetTasks(Task[] tasks)
        {
            _tasks = tasks;

            float sumWeight = 0f;

            foreach (Task task in tasks)
            {
                BaseAsyncProcess process = task.Process;

                if (process.IsCompleted)
                {
                    continue;
                }

                sumWeight += task.Weight;

                _countToWait++;
                process.Completed += OnProcessCompleted;
            }

            UpdateCompleteness();

            if (!IsCompleted)
            {
                foreach (Task task in tasks)
                {
                    task.InitProgress(sumWeight);
                }
            }
        }

        private void OnProcessCompleted()
        {
            _countToWait--;
            UpdateCompleteness();
        }

        private void UpdateCompleteness()
        {
            if (_countToWait == 0)
            {
                OnCompleted();
            }
        }

        #region Inner type

        public class Task
        {
            private float _maxProgress;

            public BaseAsyncProcess Process { get; }
            public float Weight { get; }
            public float Progress => Process.Progress * _maxProgress;

            public Task(BaseAsyncProcess process, float weight = 1f)
            {
                Process = process;
                Weight = weight;
            }

            public void InitProgress(float sumWeight)
            {
                _maxProgress = Weight / sumWeight;
            }
        }

        #endregion
    }
}