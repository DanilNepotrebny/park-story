﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Utils.AsyncProcesses
{
    public class DeferredAsyncProcess : BaseAsyncProcess
    {
        private BaseAsyncProcess _process;

        public override float Progress => _process?.Progress ?? 0f;

        public void BeginProcess(
                BaseAsyncProcess loadProcess
            )
        {
            _process = loadProcess;
            _process.DoWhenCompleted(OnCompleted);
        }
    }
}