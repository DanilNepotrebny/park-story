﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Utils.AsyncProcesses
{
    public class AsyncOperationProcess : BaseAsyncProcess
    {
        private AsyncOperation _operation;

        public override float Progress => _operation.progress;

        public AsyncOperationProcess(AsyncOperation operation)
        {
            _operation = operation;
            _operation.completed += OnOperationCompleted;
        }

        private void OnOperationCompleted(AsyncOperation operation)
        {
            OnCompleted();
        }
    }
}