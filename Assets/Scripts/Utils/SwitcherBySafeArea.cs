﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Screen;
using UnityEngine;
using Zenject;

namespace DreamTeam.Utils
{
    public class SwitcherBySafeArea : MonoBehaviour
    {
        [SerializeField] private GameObject _content;
        [SerializeField, ContainsAllEnumKeys] private SwitchableObjectsList _switchableObjects;

        [Inject] private ScreenController _screen;

        public GameObject Current => _switchableObjects[CurrentMode];
        public SwitchModeType CurrentMode { get; private set; }

        protected void Awake()
        {
            SetSwitchMode(GetCurrentSwitchMode());

            #if UNITY_EDITOR
            {
                _screen.EditorScreenSettings.SimulatingDeviceChanged += OnSimulatingDeviceChanged;
            }
            #endif
        }

        private void OnSimulatingDeviceChanged()
        {
            SetSwitchMode(GetCurrentSwitchMode());
        }

        private void SetSwitchMode(SwitchModeType mode)
        {
            if (_switchableObjects == null)
            {
                return;
            }

            CurrentMode = mode;

            if (Current != null)
            {
                Current.SetActive(true);
            }

            foreach (KeyValuePair<SwitchModeType, GameObject> valuePair in _switchableObjects)
            {
                if (valuePair.Value == null)
                {
                    continue;
                }

                GameObject go = valuePair.Value;

                if (_content != null && Current != null)
                {
                    _content.transform.SetParent(Current.transform, false);
                }

                if (go != Current)
                {
                    go.SetActive(false);
                }
            }
        }

        private SwitchModeType GetCurrentSwitchMode()
        {
            Rect safeAreaRect = _screen.SafeArea;
            if (Mathf.Approximately(safeAreaRect.width, _screen.Width) &&
                Mathf.Approximately(safeAreaRect.height, _screen.Height))
            {
                return SwitchModeType.NoSafeArea;
            }

            return SwitchModeType.SafeAreaExists;
        }

        #region Inner types

        public enum SwitchModeType
        {
            NoSafeArea,
            SafeAreaExists
        }

        [Serializable]
        private class SwitchableObjectsList : KeyValueList<SwitchModeType, GameObject>
        {
        }

        #endregion
    }
}
