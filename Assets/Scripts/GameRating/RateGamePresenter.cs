﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.GameRating
{
    public class RateGamePresenter : MonoBehaviour
    {
        [SerializeField] private SceneReference _popup;

        [Inject] private RateGameSystem _rateGameSystem;
        [Inject] private PopupQueueSystem _popupSystem;

        protected void Start()
        {
            _rateGameSystem.TriggerCheck(ShowPopup);
        }

        private void ShowPopup()
        {
            _popupSystem.Enqueue(_popup.Name);
        }
    }
}
