﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Inventory;
using UnityEngine;
using Zenject;

namespace DreamTeam.GameRating
{
    public class RateGameController : MonoBehaviour
    {
        [SerializeField] private GamePlace _gamePlace;

        [Inject] private RateGameSystem _rateSystem;

        [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
        public void RateGame()
        {
            _rateSystem.RateGame(_gamePlace);
        }

        [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
        public void RateGameOrOpenStorePage()
        {
            bool ratedSuccessfully = _rateSystem.RateGame(_gamePlace);
            if (!ratedSuccessfully)
            {
                _rateSystem.OpenGamePageOnStore();
            }
        }
    }
}
