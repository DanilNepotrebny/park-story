﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Inventory;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

#if UNITY_IOS
using System.Runtime.InteropServices;
using UnityEngine.iOS;
#endif

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam.GameRating
{
    public class RateGameSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private int _firstTriggerLevelIndex = 6;
        [SerializeField] private int _levelIncrementation = 20;

        [Inject] private SaveGroup _saveGroup;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private Instantiator _instantiator;

        #if !NO_MATCH3

        [Inject(Id = GlobalInstaller.MainLevelPackControllerId)]
        private SingleLevelPackController _levelPackController;

        #endif

        private bool _isCheckTriggerPulled;
        private int _triggerLevelIndex = DefaultValue;
        private const int DefaultValue = -1;
        private bool _isGameRated;
        private string _receivedRatingGameVersion = string.Empty;

        public void TriggerCheck(Action successCallback)
        {
            if (!_isCheckTriggerPulled)
            {
                return;
            }

            _isCheckTriggerPulled = false;
            IncrementTriggerLevelIndex();
            successCallback?.Invoke();
        }

        public bool RateGame(GamePlace gamePlace)
        {
            if (_isGameRated)
            {
                UnityEngine.Debug.Log("Attempt to rate the game. It has been already rated");
                return false;
            }
           
            if (!RequestStoreReview())
            {
                UnityEngine.Debug.LogWarning("Request to rate the game failed. Might be in-editor call, " +
                    "iOS version isn't recent enough or the StoreKit framework is not linked with the app.");
                return false;
            }

            _analyticsSystem.Send(
                    _instantiator.Instantiate<StoreRateEvent>(gamePlace)
                );

            if (!_receivedRatingGameVersion.Equals(Application.version))
            {
                _isGameRated = true;
                _receivedRatingGameVersion = Application.version;
            }

            return _isGameRated;
        }

        public void OpenGamePageOnStore()
        {
            #if UNITY_IOS
            {
                _openGamePageOnAppStore();
            }
            #endif
        }

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("RateUsSystem", this);
        }

        private void Start()
        {
            if (_triggerLevelIndex == DefaultValue)
            {
                _triggerLevelIndex = _firstTriggerLevelIndex + 1; // cause after level success
            }
        }

        protected void OnEnable()
        {
            #if !NO_MATCH3
            {
                _levelPackController.CurrentLevelAdvanced += OnLevelAdvanced;
            }
            #endif
        }

        protected void OnDisable()
        {
            #if !NO_MATCH3
            {
                _levelPackController.CurrentLevelAdvanced -= OnLevelAdvanced;
            }
            #endif
        }

        private void OnLevelAdvanced(int previousIndex, int currentIndex)
        {
            _isCheckTriggerPulled = CanTriggerBePulled(currentIndex);
        }

        private bool CanTriggerBePulled(int levelIndex)
        {
            return levelIndex >= _triggerLevelIndex && !_isGameRated;
        }

        private void IncrementTriggerLevelIndex()
        {
            _triggerLevelIndex += _levelIncrementation;
        }

        private bool RequestStoreReview()
        {
            #if UNITY_IOS
            {
                return Device.RequestStoreReview();
            }
            #else
            {
                return false;
            }
            #endif
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncInt("TriggerLevelIndex", ref _triggerLevelIndex);
            state.SyncBool("IsGameRated", ref _isGameRated);
            state.SyncString("ReceivedRatingGameVersion", ref _receivedRatingGameVersion);

            if (!_receivedRatingGameVersion.Equals(Application.version))
            {
                _isGameRated = false;
            }
        }

        #if UNITY_IOS
        [DllImport ("__Internal")] private static extern void _openGamePageOnAppStore();
        #endif
    }
}
