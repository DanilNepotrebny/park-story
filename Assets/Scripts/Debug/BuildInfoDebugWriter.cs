﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Text;
using DreamTeam.Utils;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Zenject;

namespace DreamTeam.Debug
{
    /// <summary>
    /// Writes build machine related data into <see cref="DebugStatesSystem"/>.
    /// </summary>
    public class BuildInfoDebugWriter
    {
        private JObject _parameters;

        [Inject]
        private void Construct(DebugStatesSystem debugStatesSystem)
        {
            // See https://developer.cloud.unity3d.com/support/guides/manifest/ for information.
            var manifestJson = (TextAsset)Resources.Load("UnityCloudBuildManifest.json");
            if (manifestJson == null)
            {
                return;
            }

            string manifest = manifestJson.text;

            _parameters = JToken.Parse(manifestJson.text) as JObject;
            if (_parameters == null)
            {
                UnityEngine.Debug.LogError("Can't deserialize build manifect.\n" + manifest);
                return;
            }

            debugStatesSystem.RegisterWriter("Build Info", WriteDebugState);
        }

        private void WriteDebugState(StringBuilder builder)
        {
            builder.AppendParameter("Build Version", Application.version);

            foreach (var pair in _parameters)
            {
                string name = pair.Key;
                JToken value = pair.Value;

                builder.AppendParameter(name, value.ToString());
            }
        }
    }
}