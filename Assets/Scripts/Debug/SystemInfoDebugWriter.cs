﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Text;
using DreamTeam.Screen;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Debug
{
    public class SystemInfoDebugWriter
    {
        [Inject] private ScreenController _screen;

        [Inject]
        private void Construct(DebugStatesSystem debugStatesSystem)
        {
            debugStatesSystem.RegisterWriter("System Info", WriteDebugState);
        }

        private void WriteDebugState(StringBuilder builder)
        {
            builder.AppendParameter("Device Model", SystemInfo.deviceModel);
            builder.AppendParameter("Device Name", SystemInfo.deviceName);

            builder.AppendParameter("Operating System", SystemInfo.operatingSystem);

            builder.AppendParameter("System Memory Size", MBtoGB(SystemInfo.systemMemorySize));

            builder.AppendParameter("Processor Type", SystemInfo.processorType);
            builder.AppendParameter("Processor Frequency", SystemInfo.processorFrequency.ToString());
            builder.AppendParameter("Processor Count", SystemInfo.processorCount.ToString());

            builder.AppendParameter("Graphics Device Name", SystemInfo.graphicsDeviceName);
            builder.AppendParameter("Graphics Device Type", SystemInfo.graphicsDeviceType.ToString());
            builder.AppendParameter("Graphics Device Version", SystemInfo.graphicsDeviceVersion);
            builder.AppendParameter("Graphics Memory Size", MBtoGB(SystemInfo.graphicsMemorySize));
            builder.AppendParameter("Graphics Shader Model", SystemInfo.graphicsShaderLevel.ToString());

            builder.AppendParameter("Battery Level", SystemInfo.batteryLevel.ToString());
            builder.AppendParameter("Battery Status", SystemInfo.batteryStatus.ToString());

            builder.AppendParameter("Screen Size", _screen.Width + "x" + _screen.Height);
            builder.AppendParameter("Screen DPI", _screen.DPI.ToString());
            builder.AppendParameter("Screen Orientation", _screen.Orientation.ToString());
            builder.AppendParameter("Screen Safe Area", _screen.SafeArea.ToString());
        }

        private string MBtoGB(int mb)
        {
            return Mathf.Ceil(mb / 1024f) + "GB";
        }
    }
}