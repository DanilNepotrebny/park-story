﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Debug
{
    public interface ICheatPanelDrawable
    {
        void DrawGUI(Rect rect, GUISkin skin);
        float GetGUIHeight(Rect rect, GUISkin skin);
    }
}