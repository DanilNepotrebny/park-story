﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Debug
{
    public class LogCheatDrawer : MonoBehaviour
    {
        [SerializeField] private int _messageFontSize;
        [SerializeField] private LogTypeColorMap _logTypeColorMap;
        [SerializeField] private string _messageFormat = "{0}";

        private LogMessageContainer _logMessageContainer;

        private LogMessageContainer.LogMessage _selectedMessage;

        [Inject]
        private void Construct(CheatPanel cheatPanel, LogMessageContainer logMessageContainer)
        {
            _logMessageContainer = logMessageContainer;

            cheatPanel.AddDrawer(CheatPanel.Category.Log, DrawLog);
        }

        private void DrawLog(Rect rect)
        {
            foreach (LogMessageContainer.LogMessage message in _logMessageContainer.LogMessages)
            {
                GUIStyle messageStyle = new GUIStyle(GUI.skin.button);
                messageStyle.alignment = TextAnchor.UpperLeft;
                messageStyle.wordWrap = true;
                messageStyle.fontSize = _messageFontSize;
                messageStyle.normal.textColor = _logTypeColorMap[message.LogType];

                if (GUILayout.Button(message.Format(_messageFormat), messageStyle))
                {
                    _selectedMessage = _selectedMessage == message ? null : message;
                }

                if (_selectedMessage == message)
                {
                    GUIStyle stackTraceStyle = new GUIStyle(GUI.skin.textArea);
                    stackTraceStyle.wordWrap = false;

                    GUILayout.TextArea(message.StackTrace, stackTraceStyle);
                }
            }
        }

        #region Inner types

        [Serializable, ContainsAllEnumKeys]
        private class LogTypeColorMap : KeyValueList<LogType, Color>
        {
        }

        #endregion
    }
}