﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Debug
{
    public class LogMessageContainer : MonoBehaviour
    {
        private static List<LogMessage> _logMessages = new List<LogMessage>();

        public IEnumerable<LogMessage> LogMessages => _logMessages;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void OnApplicationLoading()
        {
            Application.logMessageReceived += OnLogMessageReceived;
        }

        private static void OnLogMessageReceived(string message, string stackTrace, LogType type)
        {
            _logMessages.Add(new LogMessage(message, stackTrace, type));
        }

        #region Inner types

        public class LogMessage
        {
            private int _frameNumber;
            private string _message;

            public LogType LogType { get; }
            public string StackTrace { get; }

            public LogMessage(string message, string stackTrace, LogType logType)
            {
                _frameNumber = Time.frameCount;
                _message = message;

                StackTrace = stackTrace;
                LogType = logType;
            }

            public string Format(string format)
            {
                return string.Format(format, _message, _frameNumber, LogType);
            }
        }

        #endregion
    }
}