﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using DreamTeam.Screen;
using DreamTeam.SocialNetworking.Facebook;
using TouchScript;
using TouchScript.InputSources;
using UnityEngine;
using Zenject;

namespace DreamTeam.Debug
{
    /// <summary>
    /// Cheat panel that is capable of drawing custom UI for each game functionality that needs to implement cheats.
    /// Panel can be shown from any scene via long tap on top part of the screen.
    /// </summary>
    public class CheatPanel : MonoBehaviour
    {
        [SerializeField] private float _activeHeight = 0.1f;
        [SerializeField] private float _longTapTimeout = 1f;
        [SerializeField] private int _fontSize = 40;
        [SerializeField] private float _scrollBarWidth = 80f;
        [SerializeField] private float _windowMargin = 10f;
        [SerializeField] private float _buttonCloseWidth = 50f;
        [SerializeField] private List<string> _cheatersUserIds;

        [Inject] private ScreenController _screen;
        [Inject] private FacebookManager _facebookManager;

        private readonly Action<Rect>[] _drawers = new Action<Rect>[(int)Category.Max];
        private Category _activeCategory;
        private Vector2 _scrollPosition;
        private Rect _windowRect;

        private Coroutine _touchScriptInputSuppressingRoutine;

        public int GUIWindowId => 0;
        public int FontSize => _fontSize;

        // arg_1 - is enabled
        public event Action<bool> ActivityChanged;

        /// <summary> Adds delegate drawer into specified category. </summary>
        public void AddDrawer(Category category, Action<Rect> drawer)
        {
            _drawers[(int)category] += drawer;
        }

        /// <summary> Removes delegate drawer from every category. </summary>
        public void RemoveDrawer(Action<Rect> drawer)
        {
            for (int i = 0; i < (int)Category.Max; i++)
            {
                _drawers[i] -= drawer;
            }
        }

        public void Hide()
        {
            enabled = false;
            ActivityChanged?.Invoke(enabled);
        }

        protected void Awake()
        {
            TouchManager.Instance.PointersPressed += OnPointerPressed;
            TouchManager.Instance.PointersReleased += OnPointersReleased;
            Hide();
        }

        protected void OnDestroy()
        {
            if (TouchManager.Instance != null)
            {
                TouchManager.Instance.PointersPressed -= OnPointerPressed;
                TouchManager.Instance.PointersReleased -= OnPointersReleased;
            }
        }

        protected void OnEnable()
        {
            _touchScriptInputSuppressingRoutine = StartCoroutine(SuppressTouchScriptInput());
        }

        protected void OnDisable()
        {
            if (_touchScriptInputSuppressingRoutine != null)
            {
                StopCoroutine(_touchScriptInputSuppressingRoutine);
                _touchScriptInputSuppressingRoutine = null;
            }

            TrySetTouchScriptInputEnabled(true);
        }

        protected void OnGUI()
        {
            GUI.skin.label.fontSize = FontSize;
            GUI.skin.button.fontSize = FontSize;
            GUI.skin.textField.fontSize = FontSize;

            GUI.skin.verticalScrollbar.fixedWidth = _scrollBarWidth;
            GUI.skin.verticalScrollbarThumb.fixedWidth = _scrollBarWidth;
            GUI.skin.horizontalScrollbar.fixedHeight = _scrollBarWidth;
            GUI.skin.horizontalScrollbarThumb.fixedHeight = _scrollBarWidth;

            _windowRect = new Rect(
                    _windowMargin,
                    _windowMargin,
                    _screen.Width - _windowMargin * 2f,
                    _screen.Height - _windowMargin * 2f
                );

            _windowRect = GUILayout.Window(GUIWindowId, _windowRect, DrawPanel, "Cheat panel");

            Rect closeBtnRect = new Rect(_windowRect);
            closeBtnRect.xMin = closeBtnRect.xMax - _buttonCloseWidth;
            closeBtnRect.yMax = closeBtnRect.yMin + _buttonCloseWidth;
            if (GUI.Button(closeBtnRect, "X"))
            {
                Hide();
            }
        }

        private IEnumerator SuppressTouchScriptInput()
        {
            while (true)
            {
                TrySetTouchScriptInputEnabled(false);
                yield return null;
            }
        }

        private void TrySetTouchScriptInputEnabled(bool isEnabled)
        {
            StandardInput standardInput = FindObjectOfType<StandardInput>();
            if (standardInput != null)
            {
                standardInput.enabled = isEnabled;
            }
        }

        private void OnPointersReleased(object sender, PointerEventArgs pointerEventArgs)
        {
            CancelInvoke(nameof(OnLongTapped));
        }

        private void OnPointerPressed(object sender, PointerEventArgs pointerEventArgs)
        {
            if (pointerEventArgs.Pointers[0].Position.y > _screen.Height * (1 - _activeHeight))
            {
                #if PRODUCTION && !UNITY_EDITOR
                {
                    if (_facebookManager.IsLoggedIn && _cheatersUserIds.Contains(_facebookManager.UserID))
                    {
                        Invoke(nameof(OnLongTapped), _longTapTimeout);
                    }
                }
                #else
                {
                    Invoke(nameof(OnLongTapped), _longTapTimeout);
                }
                #endif // PRODUCTION
            }
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        private void OnLongTapped()
        {
            enabled = true;
            ActivityChanged?.Invoke(enabled);
        }

        private float CalculateCategoryPanelWidth(GUIStyle style)
        {
            float width = 0;

            for (int i = 0; i < (int)Category.Max; i++)
            {
                string caption = Enum.GetName(typeof(Category), i);
                Vector2 size = style.CalcSize(new GUIContent(caption));
                if (size.x > width)
                {
                    width = size.x;
                }
            }

            return width;
        }

        private void DrawPanel(int windowID)
        {
            GUILayout.Label("Category:");

            GUILayout.BeginHorizontal();
            {
                float categoriesAreaWidth = 0;

                #if !PLAYTESTS
                {
                    GUIStyle style = GUI.skin.button;
                    categoriesAreaWidth = CalculateCategoryPanelWidth(style);
                    GUILayout.BeginVertical(GUILayout.Width(categoriesAreaWidth));
                    {
                        for (int i = 0; i < (int)Category.Max; i++)
                        {
                            var drawer = _drawers[i];
                            if (drawer != null)
                            {
                                bool wasActive = (int)_activeCategory == i;
                                string caption = Enum.GetName(typeof(Category), i);

                                if (GUILayout.Toggle(wasActive, caption, style) &&
                                    !wasActive)
                                {
                                    _activeCategory = (Category)i;
                                    _scrollPosition = Vector2.zero;
                                }
                            }
                        }
                    }
                    GUILayout.EndVertical();
                }
                #else
                {
                    _activeCategory = Category.Levels;
                }
#endif

                GUILayout.BeginVertical(GUI.skin.textArea);
                {
                    _scrollPosition = GUILayout.BeginScrollView(_scrollPosition);

                    Rect contentRect = new Rect(_windowRect);
                    contentRect.xMin += categoriesAreaWidth;
                    contentRect.xMax -= GUI.skin.verticalScrollbar.CalcSize(new GUIContent()).x;

                    var drawer = _drawers[(int)_activeCategory];
                    drawer?.Invoke(contentRect);
                    GUILayout.FlexibleSpace();

                    GUILayout.EndScrollView();
                }
                GUILayout.EndVertical();
            }
            GUILayout.EndHorizontal();
        }

        #region Inner types

        public enum Category
        {
            General,
            Levels,
            VirtualFriends,
            Match3,
            Bot,
            ChipGeneration,
            Quests,
            Cutscenes,
            LocalNotifications,
            CountedItems,
            SocialProfilesExchange,
            EndlessGrind,
            MessageSystem,
            Clothing,
            Debug,
            FortuneWheel,
            Ads,
            Log,
            Max
        }

        #endregion
    }
}