﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Messaging;
using DreamTeam.Messaging.MessageAssets;
using DreamTeam.SocialNetworking;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Debug.Messaging
{
    public class CheatMessageAssetsSender : MonoBehaviour, ICheatPanelDrawable
    {
        [SerializeField] private List<CharacterRewardsMessageAsset> _messageAssets;

        [Inject] private MessageSystem _messageSystem;
        [Inject] private SocialProfilesSystem _socialProfilesSystem;
        [Inject] private Instantiator _instantiator;

        protected void Start()
        {
            (_messageSystem as MessageSystem.ICheatAPI).AddCheatDrawable(this);
        }

        void ICheatPanelDrawable.DrawGUI(Rect rect, GUISkin skin)
        {
            foreach (CharacterRewardsMessageAsset asset in _messageAssets)
            {
                GUIContent content = GetContent(asset);
                if (GUILayout.Button(content, skin.button))
                {
                    foreach (Reward reward in asset.Rewards)
                    {
                        _instantiator.Inject(reward);
                    }

                    CharacterRewardsMessage message = new CharacterRewardsMessage(_instantiator, asset.Rewards, GamePlace.Cheats, asset.PopupScene.Name);
                    message.Sender = _socialProfilesSystem.FindProfile(asset.SenderID);
                    _messageSystem.Add(message);
                }
            }
        }

        float ICheatPanelDrawable.GetGUIHeight(Rect rect, GUISkin skin)
        {
            float result = 0;
            foreach (CharacterRewardsMessageAsset asset in _messageAssets)
            {
                GUIContent content = GetContent(asset);
                result += skin.button.CalcHeight(content, rect.width);
            }
            return result;
        }

        private GUIContent GetContent(CharacterRewardsMessageAsset messageAsset)
        {
            Texture image = messageAsset.GUIIcon;
            string text = $"\tSend {messageAsset.name}";
            return new GUIContent(text, image);
        }
    }
}