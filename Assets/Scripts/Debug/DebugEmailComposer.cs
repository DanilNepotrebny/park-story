﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using DreamTeam.Screen;
using DreamTeam.Utils.FileFolders;
using UnityEngine;
using UT;
using Zenject;

namespace DreamTeam.Debug
{
    public class DebugEmailComposer : MonoBehaviour
    {
        [SerializeField] private string _defaultRecieverAddress;
        [SerializeField] private LogType[] _logTypesWithTrace;
        [SerializeField] private string _logMessageFormat = "{0}";

        [Inject] private LogMessageContainer _logMessageContainer;
        [Inject] private DebugStatesSystem _debugStatesSystem;
        [Inject] private SaveManager _saveManager;
        [Inject] private CheatPanel _cheatPanel;
        [Inject] private ScreenController _screen;

        protected void Awake()
        {
            _cheatPanel.AddDrawer(CheatPanel.Category.General, DrawComposeButton);
        }

        private void DrawComposeButton(Rect rect)
        {
            if (GUILayout.Button("Send info email"))
            {
                StartCoroutine(ComposeEmail());
            }
        }

        private IEnumerator ComposeEmail()
        {
            // Hide cheat panel and wait for end of frame for correct screenshot capture.
            _cheatPanel.Hide();
            yield return new WaitForEndOfFrame();

            var archive = new ZipFileFolder(ZipArchiveMode.Create);

            AddFileFromString(archive, "log.txt", GetLog());
            AddFileFromString(archive, "debug_dump.txt", _debugStatesSystem.Dump());

            AddSaves(archive, "old_saves");
            _saveManager.Save();
            AddSaves(archive, "new_saves");

            AddFileFromBytes(archive, "screenshot.png", TakeScreenshot());

            using (var message = new MailMessage())
            {
                byte[] buffer;
                archive.Dispose(out buffer);

                message
                    .AddTo(_defaultRecieverAddress)
                    .SetBodyHtml(false)
                    .AddAttachment(buffer, "info.zip");

                Mail.Compose(message, "Choose an app to send info email");
            }
        }

        private string GetLog()
        {
            var builder = new StringBuilder();

            foreach (LogMessageContainer.LogMessage message in _logMessageContainer.LogMessages)
            {
                builder.AppendLine(message.Format(_logMessageFormat));

                if (_logTypesWithTrace.Contains(message.LogType))
                {
                    builder.AppendLine(message.StackTrace);
                }
            }

            return builder.ToString();
        }

        private void AddFileFromString(ZipFileFolder folder, string fileName, string content)
        {
            using (Stream stream = folder.OpenWrite(fileName))
            {
                using (var streamWriter = new StreamWriter(stream))
                {
                    streamWriter.Write(content);
                }
            }
        }

        private void AddFileFromBytes(ZipFileFolder folder, string fileName, byte[] bytes)
        {
            using (Stream stream = folder.OpenWrite(fileName))
            {
                stream.Write(bytes, 0, bytes.Length);
            }
        }

        private void AddSaves(ZipFileFolder folder, string folderName)
        {
            folder.PushPath(folderName);
            _saveManager.ExportFiles(folder);
            folder.PopPath();
        }

        private byte[] TakeScreenshot()
        {
            var texture = new Texture2D(_screen.Width, _screen.Height);
            texture.ReadPixels(new Rect(0, 0, _screen.Width, _screen.Height), 0, 0, false);
            texture.Apply(false);
            return texture.EncodeToPNG();
        }
    }
}