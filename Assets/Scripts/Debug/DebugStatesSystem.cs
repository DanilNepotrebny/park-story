﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using System.Text;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Debug
{
    public class DebugStatesSystem : MonoBehaviour
    {
        private CheatPanel _cheatPanel;

        private readonly List<DebugWriter> _writers = new List<DebugWriter>();

        private StringBuilder _writerBuilder = new StringBuilder();

        [Inject]
        private void Construct(CheatPanel cheatPanel, Instantiator instantiator)
        {
            _cheatPanel = cheatPanel;
            _cheatPanel.AddDrawer(CheatPanel.Category.Debug, DrawDebugState);

            instantiator.Instantiate<SystemInfoDebugWriter>();
            instantiator.Instantiate<BuildInfoDebugWriter>();
        }

        public void RegisterWriter(string caption, DebugWriteDelegate writeDelegate)
        {
            if (_writers.Any(writer => writer.Delegate == writeDelegate))
            {
                return;
            }

            _writers.Add(new DebugWriter { Caption = caption, Delegate = writeDelegate });
        }

        public void UnregisterWriter(DebugWriteDelegate writeDelegate)
        {
            _writers.RemoveAll(writer => writer.Delegate == writeDelegate);
        }

        public string Dump()
        {
            for (int i = 0; i < _writers.Count; i++)
            {
                if (i > 0)
                {
                    _writerBuilder.AppendLine(string.Empty);
                }

                DebugWriter writer = _writers[i];

                _writerBuilder.AppendFormat("[{0}]\n", writer.Caption);
                writer.Delegate.Invoke(_writerBuilder);
            }

            string dump = _writerBuilder.ToString();

            _writerBuilder.Clear();

            return dump;
        }

        protected void OnDestroy()
        {
            _cheatPanel.RemoveDrawer(DrawDebugState);
        }

        private void DrawDebugState(Rect screenRect)
        {
            if (_writers.Count == 0)
            {
                GUILayout.Label("Nothing to show");
                return;
            }

            foreach (DebugWriter writer in _writers)
            {
                if (GUILayout.Button(writer.Caption))
                {
                    writer.IsOpened = !writer.IsOpened;
                }

                if (writer.IsOpened)
                {
                    writer.Delegate.Invoke(_writerBuilder);

                    const int chunkLength = 16382; // experimentally evaluated value
                    string builderString = _writerBuilder.ToString();

                    IEnumerable<string> chunks = builderString.SplitBy(chunkLength);
                    foreach (string chunk in chunks)
                    {
                        GUILayout.TextArea(chunk);
                    }

                    _writerBuilder.Clear();
                }
            }
        }

        #region Inner types

        public delegate void DebugWriteDelegate(StringBuilder builder);

        private class DebugWriter
        {
            public string Caption;
            public DebugWriteDelegate Delegate;
            public bool IsOpened;
        }

        #endregion
    }
}