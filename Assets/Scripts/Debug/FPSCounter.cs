﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Debug
{
    public class FPSCounter : MonoBehaviour
    {
        [SerializeField] private float _updateInterval = 0.1f;
        [SerializeField] private string _format = "{0:F1} FPS ({1:F1} ms)";
        [SerializeField] private Text _text;

        private float _accum;
        private int _frameCount;
        private float _timeleft;

        [Inject]
        private void Construct(CheatPanel cheatPanel)
        {
            cheatPanel.AddDrawer(CheatPanel.Category.General, DrawSwitcher);
        }

        protected void Start()
        {
            _timeleft = _updateInterval;
        }

        protected void Update()
        {
            _timeleft -= Time.deltaTime;
            _accum += Time.deltaTime;
            ++_frameCount;

            if (_timeleft <= 0.0)
            {
                float fps = _frameCount / _accum;
                float ms = 1000.0f / fps;

                _text.text = string.Format(_format, fps, ms);

                _timeleft = _updateInterval;
                _accum = 0.0f;
                _frameCount = 0;
            }
        }

        private void DrawSwitcher(Rect rect)
        {
            if (GUILayout.Button("Show FPS"))
            {
                gameObject.SetActive(!gameObject.activeSelf);
            }
        }
    }
}