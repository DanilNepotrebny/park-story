﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace DreamTeam.Debug
{
    /// <summary>
    /// Collects log messages in the memory without writing them into the output to exclude 
    /// an influence of the logging during debugging of performance critical bugs
    /// </summary>
    public static class Logger
    {
        private const string _logMessageFormat = "[{0}] {1}\n\n" +
            "##################################################\n" +
            "{2}" +
            "##################################################\n\n";

        private static List<DebugMessage> _logMessages = new List<DebugMessage>();

        [Conditional("DEBUG")]
        public static void Log(string message)
        {
            _logMessages.Add(new DebugMessage(message));
        }

        [Conditional("DEBUG")]
        public static void Clear()
        {
            _logMessages.Clear();
        }

        [Conditional("DEBUG")]
        public static void Flush()
        {
            foreach (DebugMessage message in _logMessages)
            {
                string msg = string.Format(
                        _logMessageFormat,
                        message.FrameCount,
                        message.Message,
                        message.StackTrace
                    );
                UnityEngine.Debug.Log(msg);
            }

            Clear();
        }

        #region Inner types

        private class DebugMessage
        {
            public string Message;
            public StackTrace StackTrace = new StackTrace(2);
            public int FrameCount;

            public DebugMessage(string message)
            {
                Message = message;
                FrameCount = Time.frameCount;
            }
        }

        #endregion
    }
}