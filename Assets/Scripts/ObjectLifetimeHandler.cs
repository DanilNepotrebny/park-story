﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam
{
    public class ObjectLifetimeHandler : MonoBehaviour
    {
        [SerializeField] private GameObject _rootObject;

        private bool _isDisposed;

        public static void InitObjectComponents(GameObject gameObject)
        {
            foreach (var initializable in gameObject.GetComponentsInChildren<IInitable>(true))
            {
                initializable.Init();
            }
        }

        public static void InitComponent(Component component)
        {
            var initializable = component as IInitable;
            initializable?.Init();
        }

        public static void DisposeObjectComponents(GameObject gameObject)
        {
            IDisposable[] disposableBehaviours = gameObject.GetComponentsInChildren<IDisposable>(true);
            foreach (IDisposable disposable in disposableBehaviours)
            {
                disposable.Dispose();
            }
        }

        public void DisposeComponents()
        {
            DisposeObjectComponents(GetRootObject());
        }

        protected void Awake()
        {
            InitObjectComponents(GetRootObject());
        }

        protected void OnApplicationQuit()
        {
            DisposeComponents();
        }

        private GameObject GetRootObject()
        {
            return _rootObject != null ? _rootObject : gameObject;
        }
    }
}