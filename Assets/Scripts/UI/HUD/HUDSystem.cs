﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory.Items;
using DreamTeam.UI.RewardPresenters;
using UnityEngine;

namespace DreamTeam.UI.HUD
{
    public class HUDSystem : MonoBehaviour, IResourcesPanel
    {
        private IResourcesPanel _resoucesPanel;

        public void SetResourcePanel(IResourcesPanel resourcePanel)
        {
            _resoucesPanel = resourcePanel;
        }

        bool IResourcesPanel.TryShow(CountedItem resource, Action<CountedItem> shownCallback)
        {
            return _resoucesPanel.TryShow(resource, shownCallback);
        }

        bool IResourcesPanel.TryHide(CountedItem resource, Action<CountedItem> hiddenCallback)
        {
            return _resoucesPanel.TryHide(resource, hiddenCallback);
        }

        ILockablePresenter<CountedItemMotionFinishData> IResourcesPanel.GetLockablePresenter(CountedItem recource)
        {
            return _resoucesPanel.GetLockablePresenter(recource);
        }
    }
}