﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.GameControls;
using DreamTeam.Inventory.Items;
using DreamTeam.UI.RewardPresenters;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.UI.HUD
{
    public class ResourcesPanel : MonoBehaviour, IResourcesPanel
    {
        [SerializeField] private List<ResourcePanel> _panels = new List<ResourcePanel>();

        [Inject]
        private void Construct(HUDSystem hudSystem, Instantiator instantiator)
        {
            hudSystem.SetResourcePanel(this);
            _panels.ForEach(item => instantiator.Inject(item as object));
        }

        public bool TryShow(CountedItem resource, Action<CountedItem> shownCallback)
        {
            ResourcePanel panel = _panels.FirstOrDefault(item => item.CountedItem == resource);
            panel?.Show(shownCallback);
            return panel != null;
        }

        public bool TryHide(CountedItem resource, Action<CountedItem> hiddenCallback)
        {
            ResourcePanel panel = _panels.FirstOrDefault(item => item.CountedItem == resource);
            panel?.Hide(hiddenCallback);
            return panel != null;
        }

        public ILockablePresenter<CountedItemMotionFinishData> GetLockablePresenter(CountedItem resource)
        {
            return _panels.FirstOrDefault(item => item.CountedItem == resource)?.MotionLockablePresenter;
        }

        #if UNITY_EDITOR
        private void OnValidate()
        {
            _panels.ForEach(item => item.OnValidate());
        }
        #endif

        [Serializable]
        private class ResourcePanel
        {
            [SerializeField, RequiredField] private CountedItem _countedItem;
            [SerializeField, RequiredField] private GameControlsMode _gameMode;
            [SerializeField, RequiredField] private Component _simpleAnimationComponent;
            [SerializeField, RequiredField] private Component _motionLockablePresenter;

            [Inject] private GameControlsSystem _gameControls;

            private IDisposable _hideHandler;

            public CountedItem CountedItem => _countedItem;

            public ILockablePresenter<CountedItemMotionFinishData> MotionLockablePresenter =>
                _motionLockablePresenter as ILockablePresenter<CountedItemMotionFinishData>;

            public ISimpleAnimationPlayer SimpleAnimation => _simpleAnimationComponent as ISimpleAnimationPlayer;

            private event Action<CountedItem> _shownCallbacks;
            private event Action<CountedItem> _hiddenCallbacks;

            public void Show(Action<CountedItem> shownCallback)
            {
                if (IsAllControlsInState(GameControl.State.Unlocked))
                {
                    shownCallback?.Invoke(_countedItem);
                    return;
                }

                _hideHandler = _gameControls.EnableMode(_gameMode);
                SimpleAnimation.AnimationFinished += OnAnimationFinished;
                _shownCallbacks += shownCallback;
            }

            public void Hide(Action<CountedItem> hiddenCallback)
            {
                if (_hideHandler == null || IsAllControlsInState(GameControl.State.Locked))
                {
                    hiddenCallback?.Invoke(_countedItem);
                    return;
                }

                _hideHandler.Dispose();
                _hideHandler = null;
                SimpleAnimation.AnimationFinished += OnAnimationFinished;
                _hiddenCallbacks += hiddenCallback;
            }

            #if UNITY_EDITOR
            public void OnValidate()
            {
                if (_motionLockablePresenter != null)
                {
                    _motionLockablePresenter = MotionLockablePresenter as Component;
                    Assert.IsNotNull(
                            _motionLockablePresenter,
                            $"{nameof(_motionLockablePresenter)} must implement " +
                                $"{typeof(ILockablePresenter<CountedItemMotionFinishData>).FullName}"
                        );
                }
            }
            #endif

            private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animation)
            {
                SimpleAnimation.AnimationFinished -= OnAnimationFinished;

                switch (animation)
                {
                    case SimpleAnimationType.Show:
                        _shownCallbacks?.Invoke(_countedItem);
                        _shownCallbacks = null;
                        break;

                    case SimpleAnimationType.Hide:
                        _hiddenCallbacks?.Invoke(_countedItem);
                        _hiddenCallbacks = null;
                        break;
                }
            }

            private bool IsAllControlsInState(GameControl.State state)
            {
                return _gameMode.All(item => _gameControls.IsControlInState(item.Key, state));
            }
        }
    }
}