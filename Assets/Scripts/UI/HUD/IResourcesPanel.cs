﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory.Items;
using DreamTeam.UI.RewardPresenters;

namespace DreamTeam.UI.HUD
{
    public interface IResourcesPanel
    {
        bool TryShow(CountedItem resource, Action<CountedItem> shownCallback);
        bool TryHide(CountedItem resource, Action<CountedItem> hiddenCallback);
        ILockablePresenter<CountedItemMotionFinishData> GetLockablePresenter(CountedItem recource);
    }
}