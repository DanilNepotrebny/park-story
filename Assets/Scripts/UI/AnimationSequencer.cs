﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.Utils.Coroutines;
using JetBrains.Annotations;
using Spine;
using Spine.Unity;
using Spine.Unity.Modules;
using UnityEngine;
using Animation = Spine.Animation;
using AnimationState = Spine.AnimationState;
using Event = Spine.Event;

namespace DreamTeam.UI
{
    [RequireComponent(typeof(SkeletonGraphicMultiObject))]
    public class AnimationSequencer : MonoBehaviour
    {
        private const string _loopInEventName = "loop_in";
        private const string _loopOutEventName = "loop_out";

        private SkeletonGraphicMultiObject _skeletonGraphic;
        private Queue<AnimationSequenceEntry> _animationSequenceEntries = new Queue<AnimationSequenceEntry>();
        private AnimationSequenceEntry _currentSequenceEntry;
        private Coroutine _animationSequence = null;

        public void SetupSkeletonGraphic([NotNull] SkeletonDataAsset animationDataAsset, bool flipX, bool flipY)
        {
            _skeletonGraphic.skeletonDataAsset = animationDataAsset;
            _skeletonGraphic.initialFlipX = flipX;
            _skeletonGraphic.initialFlipY = flipY;
            _skeletonGraphic.Initialize(true);
        }

        public void AddAnimation(string animationName, int replayAmount)
        {
            var animationSequenceEntry = new AnimationSequenceEntry(animationName, replayAmount, _skeletonGraphic);
            _animationSequenceEntries.Enqueue(animationSequenceEntry);

            if (_animationSequence == null)
            {
                _animationSequence = StartCoroutine(AnimationSequenceCoroutine());
            }
        }

        public void DropAnimations()
        {
            _animationSequenceEntries.Clear();
            _currentSequenceEntry?.Cancel();
            _currentSequenceEntry = null;
        }

        public void StopSequence()
        {
            if (_animationSequence != null)
            {
                StopCoroutine(_animationSequence);
                _animationSequence = null;
            }
        }

        protected void Start()
        {
            _skeletonGraphic = GetComponent<SkeletonGraphicMultiObject>();
        }

        private IEnumerator AnimationSequenceCoroutine()
        {
            while (_animationSequenceEntries.Count > 0)
            {
                _currentSequenceEntry = _animationSequenceEntries.Dequeue();
                yield return _currentSequenceEntry.Play();
            }

            _animationSequence = null;
        }

        private class AnimationSequenceEntry
        {
            private int _replayCounter;
            private bool _isLooped;
            private bool _isCanceled;
            private AnimationState _animationState = null;
            private Animation _animation = null;

            public AnimationSequenceEntry(string animationName, int replayAmount, SkeletonGraphicMultiObject graphic)
            {
                _replayCounter = replayAmount;
                _isLooped = replayAmount == 0;
                _isCanceled = false;

                _animationState = graphic.AnimationState;
                _animation = graphic.SkeletonData.FindAnimation(animationName);
            }

            public IEnumerator Play()
            {
                Spine.Event loopIn;
                Spine.Event loopOut;
                bool containsLoopEvents = ContainsLoopEvents(_animation, out loopIn, out loopOut);

                float loopInitialTime = 0f;
                while ((_isLooped || _replayCounter > 0) && !_isCanceled)
                {
                    _animationState.SetAnimation(0, _animation, false);
                    TrackEntry trackEntry = _animationState.GetCurrent(0);
                    trackEntry.TrackTime = loopInitialTime;

                    _replayCounter = Mathf.Clamp(_replayCounter - 1, 0, _replayCounter);
                    if (containsLoopEvents)
                    {
                        yield return new WaitForSpineEvent(_animationState, loopOut.Data);
                        loopInitialTime = loopIn.Time;

                        if ((_replayCounter == 0 && !_isLooped) || _isCanceled)
                        {
                            yield return new WaitForEvent<TrackEntry, AnimationState>(_animationState, nameof(_animationState.Complete));
                        }
                    }
                    else
                    {
                        yield return new WaitForEvent<TrackEntry, AnimationState>(_animationState, nameof(_animationState.Complete));
                    }
                }
            }

            public void Cancel()
            {
                _isCanceled = true;
            }

            private bool ContainsLoopEvents(Spine.Animation spineAnimation, out Spine.Event loopIn, out Spine.Event loopOut)
            {
                bool result = false;
                loopIn = null;
                loopOut = null;

                int eventsTimelineIndex = spineAnimation.Timelines.FindIndex(timeline => timeline is EventTimeline);
                if (eventsTimelineIndex >= 0)
                {
                    EventTimeline eventsTimeline = spineAnimation.Timelines.Items[eventsTimelineIndex] as EventTimeline;
                    if (eventsTimeline != null)
                    {
                        int loopInIndex = Array.FindIndex(eventsTimeline.Events, e => e.Data.Name == _loopInEventName);
                        int loopOutIndex = Array.FindIndex(eventsTimeline.Events, e => e.Data.Name == _loopOutEventName);
                        if (loopInIndex >= 0 && loopOutIndex >= 0)
                        {
                            loopIn = eventsTimeline.Events[loopInIndex];
                            loopOut = eventsTimeline.Events[loopOutIndex];
                            result = true;
                        }
                    }
                }

                return result;
            }
        }
    }
}