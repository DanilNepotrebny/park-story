﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using Zenject;

namespace DreamTeam.UI.CountedItemPresenters
{
    public class BriefingBoosterUnlockPresenter : ChipBoosterUnlockPresenter
    {
        [Inject] private Popup _popup;

        protected void Awake()
        {
            _popup.Shown += OnPopupShown;
        }

        protected void OnDestroy()
        {
            _popup.Shown -= OnPopupShown;
        }

        private void OnPopupShown(Popup popup)
        {
            UpdateView();
        }
    }
}