﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.CountedItemPresenters
{
    public class ChipBoosterEquippedActivityPresenter : MonoBehaviour, IPresenter<ChipBooster>
    {
        [SerializeField] private bool _activeIfUnequipped;
        [Inject] private EquippedChipBoosters _equippedBoosters;

        private ChipBooster _booster;

        public void Initialize(ChipBooster model)
        {
            _booster = model;
            UpdateView(model);
        }

        protected void Awake()
        {
            _equippedBoosters.EquippedOrUnequipped += UpdateView;
        }

        protected void OnDestroy()
        {
            _equippedBoosters.EquippedOrUnequipped -= UpdateView;
        }

        private void UpdateView(ChipBooster booster)
        {
            if (booster == _booster)
            {
                gameObject.SetActive(_equippedBoosters.IsEquipped(booster) ^ _activeIfUnequipped);
            }
        }
    }
}