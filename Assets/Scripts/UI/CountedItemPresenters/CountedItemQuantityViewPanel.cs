﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.CountedItemPresenters
{
    public class CountedItemQuantityViewPanel : MonoBehaviour, IPresenter<ICountedItemQuantity>
    {
        [SerializeField, RequiredField] private CountedItemViewFactoryByInstance _viewFactory;

        [Inject] private Instantiator _instantiator;

        private PresenterHub _presenter;

        public void Initialize(ICountedItemQuantity model)
        {
            if (model == null)
            {
                return;
            }

            if (_presenter != null)
            {
                _presenter.gameObject.Dispose();
            }

            _presenter = _viewFactory.Create(model.Item, _instantiator, transform, model);
            _presenter.GetComponent<RectTransform>().StretchToParrent();
        }
    }
}