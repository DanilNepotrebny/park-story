﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.CountedItemPresenters
{
    [RequireComponent(typeof(PresenterHub))]
    public class CountedItemQuantityPresenterInitializer : MonoBehaviour
    {
        [SerializeField] private CountedItem _item;

        [Inject] private ICountedItemQuantityProvider _provider;

        protected void Start()
        {
            var hub = GetComponent<PresenterHub>();
            hub.Initialize(_provider.GetQuantity(_item));
        }
    }
}