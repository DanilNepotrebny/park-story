﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.UI.CountedItemPresenters
{
    public class CountedItemHintController : BaseHintController<CountedItem>
    {
        [SerializeField, RequiredField] private CountedItemHint _hint;

        protected override BaseModelHint<CountedItem> Hint => _hint;
    }
}