﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;

namespace DreamTeam.UI.CountedItemPresenters
{
    public class CountedItemPresenterInitializer : PresenterInitializer<CountedItem>
    {
    }
}