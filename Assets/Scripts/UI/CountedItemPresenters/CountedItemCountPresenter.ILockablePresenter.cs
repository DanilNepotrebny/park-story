﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI.RewardPresenters;

namespace DreamTeam.UI.CountedItemPresenters
{
    public partial class CountedItemCountPresenter : ILockablePresenter<CountedItemMotionFinishData>
    {
        private bool _isLocked;

        void ILockablePresenter<CountedItemMotionFinishData>.Lock()
        {
            _isLocked = true;
            _currentValueTweener.Stop();
            _previousValueTweener.Stop();
            UnsubscribeItem();
        }

        void ILockablePresenter<CountedItemMotionFinishData>.Unlock()
        {
            _isLocked = false;
            SubscribeItem();
        }

        void ILockablePresenter<CountedItemMotionFinishData>.UpdateManualy(CountedItemMotionFinishData data)
        {
            UpdateView(data.CurrentCount, data.PreviousCount);
        }
    }
}