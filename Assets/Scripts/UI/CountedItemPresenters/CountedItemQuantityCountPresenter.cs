﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.CountedItemPresenters
{
    [RequireComponent(typeof(Text))]
    public class CountedItemQuantityCountPresenter : MonoBehaviour, IPresenter<ICountedItemQuantity>
    {
        [SerializeField, Tooltip("{0} - count")]
        private string _textFormat = "{0}";

        public void Initialize(ICountedItemQuantity model)
        {
            GetComponent<Text>().text = string.Format(_textFormat, model.Count.ToString());
        }
    }
}