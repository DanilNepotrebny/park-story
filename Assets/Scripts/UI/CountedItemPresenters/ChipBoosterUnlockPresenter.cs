﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Playables;

namespace DreamTeam.UI.CountedItemPresenters
{
    public abstract class ChipBoosterUnlockPresenter : CountedItemUnlockPresenter
    {
        [SerializeField, RequiredField] private PlayableDirector _unlockedPlayableDirector;
        [SerializeField, RequiredField] private RectTransform _lockedContainer;

        protected sealed override void InitializeView(bool isLocked)
        {
            _unlockedPlayableDirector.gameObject.SetActive(!isLocked);
            _lockedContainer.gameObject.SetActive(isLocked);
        }

        protected sealed override void UpdateView(bool isLocked, Action onUpdated)
        {
            if (!isLocked && enabled)
            {
                _unlockedPlayableDirector.gameObject.SetActive(!isLocked);
                _unlockedPlayableDirector.Play();
                StartCoroutine(WaitForPlayableDirectorEnd(onUpdated));
            }
        }

        private IEnumerator WaitForPlayableDirectorEnd(Action onEnd)
        {
            yield return new WaitWhile(() => _unlockedPlayableDirector.state == PlayState.Playing);
            onEnd.Invoke();
            _lockedContainer.gameObject.SetActive(false);
        }
    }
}