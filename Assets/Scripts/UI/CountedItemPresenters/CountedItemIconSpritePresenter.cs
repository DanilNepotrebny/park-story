﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using UnityEngine;

namespace DreamTeam.UI.CountedItemPresenters
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class CountedItemIconSpritePresenter : MonoBehaviour, IPresenter<CountedItem>
    {
        public void Initialize(CountedItem model)
        {
            GetComponent<SpriteRenderer>().sprite = model.Icon;
        }
    }
}