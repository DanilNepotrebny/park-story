﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.UI.CountedItemPresenters
{
    public class Match3BoosterUnlockPresenter : ChipBoosterUnlockPresenter
    {
        private bool _isUpdated;

        protected void OnEnable()
        {
            if (!_isUpdated)
            {
                UpdateView();
                _isUpdated = true;
            }
        }
    }
}