﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.Localization;
using DreamTeam.UI.DataProviders;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.CountedItemPresenters
{
    [RequireComponent(typeof(Text))]
    public class CountedItemDescriptionPresenter : MonoBehaviour, IPresenter<CountedItem>
    {
        [Inject] private LocalizationSystem _localizationSystem;

        public void Initialize(CountedItem model)
        {
            var provider = model as IDescriptionProvider;
            if (provider != null)
            {
                GetComponent<Text>().text = _localizationSystem.Localize(provider.Description, model);
            }
        }
    }
}
