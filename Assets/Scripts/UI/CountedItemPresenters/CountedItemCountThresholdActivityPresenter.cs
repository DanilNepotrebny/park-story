﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using UnityEngine;

namespace DreamTeam.UI.CountedItemPresenters
{
    /// <summary>
    /// Activates child <see cref="GameObjects"> by <see cref="CountedIem">'s count threshold.
    /// </summary>
    /// <typeparam name="CountedItem"></typeparam>
    public class CountedItemCountThresholdActivityPresenter : MonoBehaviour, IPresenter<CountedItem>
    {
        [SerializeField] private int _threshold;
        [SerializeField] private ThresholdCondition _activationCondition;

        private CountedItem _countedItem;

        public void Initialize(CountedItem model)
        {
            UnsubscribeCountedItem();
            _countedItem = model;
            SubscribeCountedItem();
            UpdateView(_countedItem.Count, _countedItem.Count);
        }

        protected void OnDestroy()
        {
            UnsubscribeCountedItem();
        }

        private void SubscribeCountedItem()
        {
            if (_countedItem != null)
            {
                _countedItem.CountChanged += UpdateView;
            }
        }

        private void UnsubscribeCountedItem()
        {
            if (_countedItem != null)
            {
                _countedItem.CountChanged -= UpdateView;
            }
        }

        private void UpdateView(int current, int previous)
        {
            switch (_activationCondition)
            {
                case ThresholdCondition.More:
                    SetChildsActive(current > _threshold);
                    break;

                case ThresholdCondition.MoreOrEqual:
                    SetChildsActive(current >= _threshold);
                    break;

                case ThresholdCondition.Less:
                    SetChildsActive(current < _threshold);
                    break;

                case ThresholdCondition.LessOrEqual:
                    SetChildsActive(current <= _threshold);
                    break;
            }
        }

        private void SetChildsActive(bool isActive)
        {
            for (int index = 0; index < transform.childCount; index++)
            {
                transform.GetChild(index).gameObject.SetActive(isActive);
            }
        }

        private enum ThresholdCondition
        {
            More,
            MoreOrEqual,
            Less,
            LessOrEqual
        }
    }
}