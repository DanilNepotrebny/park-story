﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory.Items;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.UI.CountedItemPresenters
{
    [CreateAssetMenu]
    public class CountedItemViewFactoryByInstance : ScriptableObject
    {
        [SerializeField] private PresenterHub _defaultView;
        [SerializeField] private ViewMap _viewMap = new ViewMap();

        public PresenterHub Create(CountedItem model, Instantiator instantiator, Transform parent, params object[] extraModels)
        {
            PresenterHub sample = _viewMap.FindFirstOrDefault(model) ?? _defaultView;
            PresenterHub result = sample != null ? instantiator.Instantiate(sample, parent.gameObject) : null;
            if (result != null)
            {
                result.Initialize(model);
                foreach (object extraModel in extraModels)
                {
                    result.Initialize(extraModel);
                }
            }

            return result;
        }

        [Serializable]
        public class ViewMap : KeyValueList<CountedItem, PresenterHub>
        {
        }
    }
}