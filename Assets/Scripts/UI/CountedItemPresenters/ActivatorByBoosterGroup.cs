﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.UI.CountedItemPresenters
{
    public class ActivatorByBoosterGroup : MonoBehaviour, IPresenter<CountedItem>
    {
        [SerializeField, RequiredField] private BoosterGroup _boosterGroup;

        public void Initialize(CountedItem model)
        {
            Booster booster = model as Booster;
            if (booster != null)
            {
                gameObject.SetActive(_boosterGroup.Contains(booster));
            }
        }
    }
}
