﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.UI.DataProviders;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.CountedItemPresenters
{
    [RequireComponent(typeof(Image))]
    public class CountedItemUnlockNotificationIconImagePresenter : MonoBehaviour, IPresenter<CountedItem>
    {
        public void Initialize(CountedItem model)
        {
            IUnlockNotificationIconProvider iconProvider = model as IUnlockNotificationIconProvider;
            if (iconProvider != null)
            {
                GetComponent<Image>().sprite = iconProvider.UnlockNotificationIcon;
            }
        }
    }
}