﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Items.UnlockNotification;
using DreamTeam.Utils;
using DreamTeam.Utils.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.CountedItemPresenters
{
    [RequireComponent(typeof(Text))]
    public class CountedItemUnlockCountTweener : MonoBehaviour, IPresenter<CountedItem>
    {
        [SerializeField] private CounterTweener _tweener;
        [SerializeField] private int _tweenFromValue = 0;
        [SerializeField, Tooltip("{0} - count")] private string _textFormat = "{0}";
        [SerializeField, RequiredField] private UnlockNotificationType _notificationType;

        [Inject] private UnlockNotificationSystem _notificationSystem;

        private CountedItem _countedItem;

        private Text _targetText;

        public void Initialize(CountedItem model)
        {
            _countedItem = model;
        }

        protected void Awake()
        {
            _targetText = GetComponent<Text>();
        }

        protected void OnEnable()
        {
            if (_notificationSystem.HasNotification(_notificationType, _countedItem))
            {
                _tweener.Start(_tweenFromValue, _countedItem.Count, SetCounter, CompleteTweener);
            }
        }

        protected void OnDisable()
        {
            _tweener.Stop();
        }

        private void SetCounter(float value)
        {
            _targetText.text = string.Format(_textFormat, (int)value);
        }

        private void CompleteTweener()
        {
            _countedItem = null;
        }
    }
}