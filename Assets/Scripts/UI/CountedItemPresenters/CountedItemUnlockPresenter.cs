﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Items.UnlockNotification;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.CountedItemPresenters
{
    public abstract class CountedItemUnlockPresenter : MonoBehaviour, IPresenter<CountedItem>
    {
        [SerializeField, RequiredField] private UnlockNotificationType _notificationType;

        [Inject] private UnlockNotificationSystem _notificationSystem;

        private CountedItem _countedItem;

        public void Initialize(CountedItem model)
        {
            _countedItem = model;

            if (_notificationSystem.HasNotification(_notificationType, _countedItem))
            {
                InitializeView(true);
            }
            else
            {
                InitializeView(_countedItem.IsLocked);
            }
        }

        protected abstract void InitializeView(bool isLocked);
        protected abstract void UpdateView(bool isLocked, Action onUpdated);

        protected void UpdateView()
        {
            if (!_notificationSystem.HasNotification(_notificationType, _countedItem))
            {
                return;
            }

            var di = new DeferredInvocation(
                () => _notificationSystem.HandleNotificationPresented(_notificationType, _countedItem));

            IDeferredInvocationHandle presentationEndHandle = di.Start();
            _notificationSystem.HandleNotificationPresenting(_notificationType, _countedItem, presentationEndHandle);

            UpdateView(
                _countedItem.IsLocked,
                () => presentationEndHandle.Unlock());
        }
    }
}