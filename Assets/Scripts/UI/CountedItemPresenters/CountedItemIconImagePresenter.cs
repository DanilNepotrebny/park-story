﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.CountedItemPresenters
{
    [RequireComponent(typeof(Image))]
    public class CountedItemIconImagePresenter : MonoBehaviour, IPresenter<CountedItem>
    {
        public void Initialize(CountedItem model)
        {
            GetComponent<Image>().sprite = model.Icon;
        }
    }
}