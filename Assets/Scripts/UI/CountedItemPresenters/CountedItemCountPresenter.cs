﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.Utils.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.CountedItemPresenters
{
    [RequireComponent(typeof(Text))]
    public partial class CountedItemCountPresenter : MonoBehaviour, IPresenter<CountedItem>
    {
        [SerializeField, Tooltip("{0} - current count;\n{1} - previous count;")] private string _textFormat = "{0}";

        [Header("Tweening")]
        [SerializeField]
        private CounterTweener _currentValueTweener;

        [SerializeField] private CounterTweener _previousValueTweener;

        private CountedItem _countedItem;
        private int _displayedCurrentValue;
        private int _displayedPreviousValue;
        private Text _textCached;

        private Text _text
        {
            get
            {
                if (_textCached == null)
                {
                    _textCached = GetComponent<Text>();
                }
                return _textCached;
            }
        }

        public void Initialize(CountedItem model)
        {
            UnsubscribeItem();

            _countedItem = model;
            if (isActiveAndEnabled)
            {
                InitializeInternal();
            }
        }

        protected void OnEnable()
        {
            if (isActiveAndEnabled)
            {
                InitializeInternal();
            }
        }

        protected void OnDisable()
        {
            UnsubscribeItem();

            _currentValueTweener.Stop();
            _previousValueTweener.Stop();
            _displayedCurrentValue = 0;
            _displayedPreviousValue = 0;
        }

        private void SubscribeItem()
        {
            if (_countedItem != null && !_isLocked)
            {
                _countedItem.CountChanged += UpdateView;
            }
        }

        private void UnsubscribeItem()
        {
            if (_countedItem != null)
            {
                _countedItem.CountChanged -= UpdateView;
            }
        }

        private void InitializeInternal()
        {
            SubscribeItem();

            if (_countedItem != null && !_isLocked)
            {
                int currentValue = _countedItem.Count;
                SetCounterText(currentValue, currentValue);
                _displayedCurrentValue = _displayedPreviousValue = currentValue;
            }
        }

        private void UpdateView(int currentValue, int previousValue)
        {
            int currentValueToSet = _displayedCurrentValue;
            int previousValueToSet = _displayedPreviousValue;

            _previousValueTweener.Start(
                    _displayedPreviousValue,
                    previousValue,
                    (value) =>
                    {
                        previousValueToSet = (int)value;
                        SetCounterText(currentValueToSet, previousValueToSet);
                    },
                    () => _displayedPreviousValue = previousValue
                );

            _currentValueTweener.Start(
                    _displayedCurrentValue,
                    currentValue,
                    (value) =>
                    {
                        currentValueToSet = (int)value;
                        SetCounterText(currentValueToSet, previousValueToSet);
                    },
                    () => _displayedCurrentValue = currentValue
                );
        }

        private void SetCounterText(int current, int previous)
        {
            if (_text != null)
            {
                _text.text = string.Format(_textFormat, current, previous);
            }
        }
    }
}