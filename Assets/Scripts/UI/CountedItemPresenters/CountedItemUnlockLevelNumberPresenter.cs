﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.Localization;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.CountedItemPresenters
{
    [RequireComponent(typeof(Text))]
    public class CountedItemUnlockLevelNumberPresenter : MonoBehaviour, IPresenter<CountedItem>
    {
        [SerializeField, LocalizationKey, Tooltip("{0} - level number")]
        private string _textFormat = "{0}";

        [SerializeField] private int _displayLevelNumberDelta = 1;

        [Inject] private InventoryLevelDrivenUnlockSystem _unlockSystem;

        public void Initialize(CountedItem model)
        {
            GetComponent<Text>().text = string.Format(
                    _textFormat,
                    _unlockSystem.GetUnlockLevelNumber(model) + _displayLevelNumberDelta
                );
        }
    }
}