﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.UI.CountedItemPresenters
{
    [RequireComponent(typeof(Animation))]
    public partial class CountedItemAnimationPresenter : MonoBehaviour, IPresenter<CountedItem>
    {
        [SerializeField] private CountedItem _item;

        private Animation _animation;

        public void Initialize(CountedItem model)
        {
            SetItem(model);
        }

        protected void Awake()
        {
            SetItem(_item);
        }

        protected void OnEnable()
        {
            _animation = GetComponent<Animation>();
        }

        protected void OnDisable()
        {
            _animation = null;
        }

        private void SetItem([NotNull] CountedItem model)
        {
            UnsubscribeCounterUpdate();
            _item = model;
            SubscribeCounterUpdate();
        }

        private void PlayAnimation(int current, int previous)
        {
            if (_animation != null && current != previous)
            {
                _animation.Play();
            }
        }

        private void SubscribeCounterUpdate()
        {
            if (_item != null)
            {
                _item.CountChanged += PlayAnimation;
            }
        }

        private void UnsubscribeCounterUpdate()
        {
            if (_item != null)
            {
                _item.CountChanged -= PlayAnimation;
            }
        }
    }
}