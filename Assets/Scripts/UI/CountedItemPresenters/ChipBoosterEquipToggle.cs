﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.CountedItemPresenters
{
    [RequireComponent(typeof(Toggle))]
    public class ChipBoosterEquipToggle : MonoBehaviour, IPresenter<ChipBooster>
    {
        [SerializeField] private GamePlace _gamePlace;

        [Inject] private EquippedChipBoosters _equippedChipBoosters;

        private const int SpendingAmount = 1;
        private Toggle _toggle;
        private ChipBooster _booster;

        public void Initialize(ChipBooster model)
        {
            _booster = model;
            UpdateView();
        }

        protected void Awake()
        {
            _toggle = GetComponent<Toggle>();
            _toggle.onValueChanged.AddListener(ToggleBoosterEquipped);
        }

        protected void OnEnable()
        {
            UpdateView();
        }

        private void UpdateView()
        {
            if (_booster != null)
            {
                _toggle.interactable = !_booster.IsLocked;
                _toggle.isOn = _equippedChipBoosters.IsEquipped(_booster);
            }
        }

        private void ToggleBoosterEquipped(bool isEquipped)
        {
            if (isEquipped)
            {
                if (!_equippedChipBoosters.IsEquipped(_booster))
                {
                    if (_booster.Count > 0)
                    {
                        _equippedChipBoosters.Equip(_booster);
                    }
                    else
                    {
                        _booster.CountChanged += OnBoosterCountChanged;
                        _booster.ShowNotEnoughPopup(_gamePlace);
                    }
                }
            }
            else
            {
                if (_equippedChipBoosters.IsEquipped(_booster))
                {
                    _equippedChipBoosters.Unequip(_booster);
                }
            }

            UpdateView();
        }

        private void OnBoosterCountChanged(int current, int previous)
        {
            if (previous == 0 && current > 0)
            {
                ToggleBoosterEquipped(true);
            }
        }
    }
}