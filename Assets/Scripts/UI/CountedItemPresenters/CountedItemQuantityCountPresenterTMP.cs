﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using TMPro;
using UnityEngine;

namespace DreamTeam.UI.CountedItemPresenters
{
    [RequireComponent(typeof(TMP_Text))]
    public class CountedItemQuantityCountPresenterTMP : MonoBehaviour, IPresenter<ICountedItemQuantity>
    {
        [SerializeField, Tooltip("{0} - count")]
        private string _textFormat = "{0}";

        public void Initialize(ICountedItemQuantity model)
        {
            TMP_Text textComponent = GetComponent<TMP_Text>();
            textComponent.text = string.Format(_textFormat, model.Count.ToString());
            textComponent.ForceMeshUpdate();
        }
    }
}