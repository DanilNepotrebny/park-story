﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI.RewardPresenters;

namespace DreamTeam.UI.CountedItemPresenters
{
    public partial class CountedItemAnimationPresenter : ILockablePresenter<CountedItemMotionFinishData>
    {
        void ILockablePresenter<CountedItemMotionFinishData>.Lock()
        {
            UnsubscribeCounterUpdate();
        }

        void ILockablePresenter<CountedItemMotionFinishData>.Unlock()
        {
            SubscribeCounterUpdate();
        }

        void ILockablePresenter<CountedItemMotionFinishData>.UpdateManualy(CountedItemMotionFinishData data)
        {
            PlayAnimation(data.CurrentCount, data.PreviousCount);
        }
    }
}