﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI
{
    [RequireComponent(typeof(Button))]
    public class ShowNotEnoughPopupButton : MonoBehaviour
    {
        [SerializeField] private CountedItem _countedItem;
        [SerializeField] private GamePlace _gamePlace;

        private void Awake()
        {
            if (_countedItem != null)
            {
                GetComponent<Button>().onClick.AddListener(() => _countedItem.ShowNotEnoughPopup(_gamePlace));
            }
        }
    }
}