﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam.UI
{
    [RequireComponent(typeof(Button))]
    public class SetLevelPackControllerTypeButton : MonoBehaviour
    {
        #if !NO_MATCH3
        [SerializeField] private LevelPackControllerType _controllerType;
        #else
        // Life-hack to make Unity inspector serialize enum field without using actual type
        [SerializeField] private int _controllerType;
        #endif

        #if !NO_MATCH3

        [Inject] private LevelSystem _levelSystem;
        private Button _button => GetComponent<Button>();

        protected void Awake()
        {
            _button.onClick.AddListener(OnButtonCkicked);
        }

        protected void OnDestroy()
        {
            _button.onClick.RemoveListener(OnButtonCkicked);
        }

        private void OnButtonCkicked()
        {
            _levelSystem.SetActiveControllerByType(_controllerType);
        }

        #endif
    }
}