﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.EndlessGrind;
using DreamTeam.Inventory;

namespace DreamTeam.UI.EndlessGrindPresenters
{
    public class EndlessGrindChestProductPackPresenter : PresenterProxy<EndlessGrindChest, ProductPack>
    {
        protected override ProductPack TargetModel => SourceModel.ProductPack;
    }
}