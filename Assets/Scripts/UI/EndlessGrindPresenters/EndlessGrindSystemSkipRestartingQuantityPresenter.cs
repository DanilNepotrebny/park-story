﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.EndlessGrind;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.EndlessGrindPresenters
{
    [RequireComponent(typeof(PresenterHub))]
    public class EndlessGrindSystemSkipRestartingQuantityPresenter : MonoBehaviour
    {
        [Inject] private EndlessGrindSystem _endlessGrind;

        protected void OnEnable()
        {
            GetComponent<PresenterHub>().Initialize(_endlessGrind.SkipRestartingQuantity);
        }
    }
}