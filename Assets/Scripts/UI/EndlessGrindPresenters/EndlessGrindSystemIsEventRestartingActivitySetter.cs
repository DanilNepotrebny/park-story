﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.EndlessGrind;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.EndlessGrindPresenters
{
    public class EndlessGrindSystemIsEventRestartingActivitySetter : MonoBehaviour
    {
        [SerializeField, RequiredField] private GameObject[] _targetsToActivate;
        [SerializeField, RequiredField] private GameObject[] _rargetsToDeactivate;

        [Inject] private EndlessGrindSystem _endlessGrind;

        protected void OnEnable()
        {
            _endlessGrind.IsEventRestartingChanged += UpdateView;
            UpdateView();
        }

        protected void OnDisable()
        {
            _endlessGrind.IsEventRestartingChanged -= UpdateView;
        }

        private void UpdateView()
        {
            foreach (GameObject target in _targetsToActivate)
            {
                target.SetActive(_endlessGrind.IsEventRestarting);
            }

            foreach (GameObject target in _rargetsToDeactivate)
            {
                target.SetActive(!_endlessGrind.IsEventRestarting);
            }
        }
    }
}