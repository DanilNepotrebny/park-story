﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.EndlessGrind;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.EndlessGrindPresenters
{
    public class EndlessGrindChestsCollectionPresenter : MonoBehaviour,
        IPresenter<IReadOnlyCollection<EndlessGrindChest>>
    {
        [SerializeField] private PresenterHub _chestPresenterPrefab;
        [SerializeField] private SwitcherByScreenOrientation _rootObjectProvider;

        [Inject] private Instantiator _instantiator;

        private Dictionary<EndlessGrindChest, PresenterHub> _presenters =
            new Dictionary<EndlessGrindChest, PresenterHub>();

        private GameObject _rootObject => _rootObjectProvider != null ? _rootObjectProvider.Current : gameObject;

        public void Initialize(IReadOnlyCollection<EndlessGrindChest> model)
        {
            ClearRemovedPresenters(model);
            UpdateOrCreatePresenters(model);
        }

        private void ClearRemovedPresenters(IReadOnlyCollection<EndlessGrindChest> chests)
        {
            var removed = new List<EndlessGrindChest>(_presenters.Keys.Where(item => !chests.Contains(item)));
            foreach (EndlessGrindChest chest in removed)
            {
                _presenters[chest].gameObject.Dispose();
                _presenters.Remove(chest);
            }
        }

        private void UpdateOrCreatePresenters(IReadOnlyCollection<EndlessGrindChest> chests)
        {
            foreach (EndlessGrindChest chest in chests)
            {
                PresenterHub _presenterHub;
                if (!_presenters.TryGetValue(chest, out _presenterHub))
                {
                    _presenterHub = _instantiator.Instantiate(_chestPresenterPrefab, _rootObject);
                    _presenters.Add(chest, _presenterHub);
                }

                _presenterHub.Initialize(chest);
            }
        }
    }
}