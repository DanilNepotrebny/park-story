﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.EndlessGrind;
using DreamTeam.Utils.UI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.EndlessGrindPresenters
{
    [RequireComponent(typeof(Text))]
    public class EndlessGrindSystemTimeTillEndEventPresenter : SimpleTimerPresenter
    {
        [Inject] private EndlessGrindSystem _system;

        protected override TimeSpan DisplayTime => _system.TimeTillEndEvent;
    }
}