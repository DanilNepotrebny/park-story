﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;
using DreamTeam.PersistentCommands;
using DreamTeam.PersistentCommands.Rewards;
using DreamTeam.Popups;
using DreamTeam.Popups.Destabilization;
using DreamTeam.TransformLookup;
using DreamTeam.UI.RewardPresentationSequences;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Movement;
using DreamTeam.Utils.UI;
using Spine;
using Spine.Unity;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.EndlessGrindPresenters
{
    [RequireComponent(typeof(RectTransform), typeof(IAnimationStateComponent))]
    public class EndlessGrindRewardsPresentationSequence : RewardPresentationSequenceBase, IPopupDestabilizer
    {
        [SerializeField] private SceneReference _rewardPopupScene;
        [SerializeField] private string _rewardPopupHeader = "Your reward!";
        [SerializeField] private bool _restoreCurrentPopupOnComplete = true;

        [Header("Animations:")]
        [SerializeField, SpineAnimation] private string _pingAnimation;
        [SerializeField, SpineAnimation] private string _consumeAnimation;

        [Header("Movement:")] [SerializeField, RequiredField] private MovementSequence _movementSequence;
        [SerializeField, RequiredField] private TransformLookupId _finishTransformId;

        [Inject] private Instantiator _instantiator;
        [Inject] private TransformLookupSystem _transfromLookupSystem;
        [Inject] private PopupSystem _popupSystem;
        [Inject] private Popup _popup;
        [Inject] private PersistentCommandsExecutionSystem _persistentCommands;

        private IAnimationStateComponent _animationStateComponent;
        private bool _isPresentationCompleted;
        private RectTransform _initialParent;
        private RectTransformData _initialRectTransformData;
        private IDeferredInvocationHandle _givingRewards;
        private IPersistentCommand _givingCommand;
        private bool _isPopupStable = true;

        public override bool IsPresentationComplete => _isPresentationCompleted;
        private RectTransform _rectTransform => transform as RectTransform;

        bool IPopupDestabilizer.IsStable => _isPopupStable;

        private event Action _popupStabilized;
        private event Action _popupDestabilized;

        event Action IPopupDestabilizer.Stabilized
        {
            add { _popupStabilized += value; }
            remove { _popupStabilized -= value; }
        }

        event Action IPopupDestabilizer.Destabilized
        {
            add { _popupDestabilized += value; }
            remove { _popupDestabilized -= value; }
        }

        public override void Present(ICollection<Reward> rewards, IDeferredInvocationHandle givingRewards)
        {
            StartCoroutine(PresentationRoutine(rewards, givingRewards));
        }

        protected void Awake()
        {
            _animationStateComponent = GetComponent<IAnimationStateComponent>();
            _initialParent = transform.parent as RectTransform;
            _initialRectTransformData = new RectTransformData(_rectTransform);

            _popup.AddDestabilizer(this);
        }

        protected void OnDestroy()
        {
            _popup.RemoveDestabilizer(this);
        }

        private IEnumerator PresentationRoutine(ICollection<Reward> rewards, IDeferredInvocationHandle givingRewards)
        {
            SetPopupStabilized(false);

            _isPresentationCompleted = false;
            _givingRewards = givingRewards;
            _givingCommand = GiveRewardsCollectionCommand.Construct(_instantiator, rewards, ReceivingType.Earned, GamePlace.EndlessGrind);
            _persistentCommands.Register(_givingCommand);

            string currentAnimation = _animationStateComponent.AnimationState.GetCurrent(0).Animation.Name;

            TrackEntry pingTrack = _animationStateComponent.AnimationState.SetAnimation(0, _pingAnimation, false);
            yield return new WaitUntil(() => pingTrack.IsComplete);

            Transform finishPoint = _transfromLookupSystem.FindTransform(_finishTransformId);
            transform.SetParent(finishPoint.transform, true);

            _movementSequence.Move(transform.position, finishPoint.position, false);
            yield return new WaitUntil(() => !_movementSequence.IsMoving());

            SetPopupStabilized(true);
            _popupSystem.Hide(_popup.SceneName);

            TrackEntry consumeTrack = _animationStateComponent.AnimationState.SetAnimation(0, _consumeAnimation, false);
            yield return new WaitUntil(() => consumeTrack.IsComplete);

            transform.SetParent(_initialParent, false);
            _initialRectTransformData.ApplyToTransform(_rectTransform);
            _animationStateComponent.AnimationState.SetAnimation(0, currentAnimation, false);

            var args = new RewardsPopup.Args()
            {
                RewardsTakenCallback = OnRewardsTaken,
                ClosedCallback = () =>
                {
                    if (_restoreCurrentPopupOnComplete)
                    {
                        _popupSystem.Show(_popup.SceneName);
                    }
                },
                Rewards = rewards,
                Header = _rewardPopupHeader
            };
            _popupSystem.Show(_rewardPopupScene.Name, args);

            _isPresentationCompleted = true;
        }

        private void SetPopupStabilized(bool isStable)
        {
            if (_isPopupStable != isStable)
            {
                _isPopupStable = isStable;
                Action action = _isPopupStable ? _popupStabilized : _popupDestabilized;
                action?.Invoke();
            }
        }

        private void OnRewardsTaken()
        {
            _givingRewards.Unlock();
            _persistentCommands.Unregister(_givingCommand);
        }
    }
}