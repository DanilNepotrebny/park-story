﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.EndlessGrind;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.EndlessGrindPresenters
{
    [RequireComponent(typeof(PresenterHub))]
    public class EndlessGrindSystemChestsPresenterInitializer : MonoBehaviour
    {
        [Inject] private EndlessGrindSystem _endlessGrindSystem;

        protected void Awake()
        {
            GetComponent<PresenterHub>().Initialize(_endlessGrindSystem.Chests);
        }
    }
}