﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.EndlessGrind;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.EndlessGrindPresenters
{
    [RequireComponent(typeof(Text))]
    public class EndlessGrindChestNamePresenter : MonoBehaviour, IPresenter<EndlessGrindChest>
    {
        [SerializeField, Tooltip("{0} - name")]
        private string _textFormat = "{0}";

        public void Initialize(EndlessGrindChest model)
        {
            GetComponent<Text>().text = string.Format(_textFormat, model.Name);
        }
    }
}