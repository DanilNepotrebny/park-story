﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.EndlessGrind;
using Spine.Unity;
using UnityEngine;

namespace DreamTeam.UI.EndlessGrindPresenters
{
    [RequireComponent(typeof(ISkeletonComponent))]
    public class EndlessGrindChestSkinPresenter : MonoBehaviour, IPresenter<EndlessGrindChest>
    {
        public void Initialize(EndlessGrindChest model)
        {
            var skeletonComponent = GetComponent<ISkeletonComponent>();
            skeletonComponent.Skeleton.SetSkin(model.Skin);
        }
    }
}