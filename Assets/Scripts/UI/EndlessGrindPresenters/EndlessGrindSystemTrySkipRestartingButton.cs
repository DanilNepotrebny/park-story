﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.EndlessGrind;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.EndlessGrindPresenters
{
    public class EndlessGrindSystemTrySkipRestartingButton : MonoBehaviour
    {
        [Inject] private EndlessGrindSystem _endlessGrind;

        private Button _button => GetComponent<Button>();

        protected void OnEnable()
        {
            _button.onClick.AddListener(OnButtonClicked);
        }

        protected void OnDisable()
        {
            _button.onClick.RemoveListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            _endlessGrind.TrySkipRestarting();
        }
    }
}