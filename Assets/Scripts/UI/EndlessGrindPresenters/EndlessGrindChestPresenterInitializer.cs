﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.EndlessGrind;

namespace DreamTeam.UI.EndlessGrindPresenters
{
    public class EndlessGrindChestPresenterInitializer : PresenterInitializer<EndlessGrindChest>
    {
    }
}