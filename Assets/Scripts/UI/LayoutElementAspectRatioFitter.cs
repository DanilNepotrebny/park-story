﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DreamTeam.UI
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(LayoutElement))]
    public class LayoutElementAspectRatioFitter : UIBehaviour
    {
        [SerializeField] private AspectMode _aspectMode = AspectMode.None;
        [SerializeField] private float _aspectRatio = 1;

        private RectTransform _rectTransform => GetComponent<RectTransform>();
        private LayoutElement _layoutElement => GetComponent<LayoutElement>();
        private LayoutGroup _layoutGroup => GetComponentInParent<LayoutGroup>();

        protected override void OnEnable()
        {
            OnRectTransformDimensionsChange();
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            OnRectTransformDimensionsChange();
            base.OnDisable();
        }

        #if UNITY_EDITOR
        protected override void OnValidate()
        {
            OnRectTransformDimensionsChange();
            base.OnValidate();
        }

        protected override void Reset()
        {
            OnRectTransformDimensionsChange();
            base.Reset();
        }
        #endif

        protected override void OnRectTransformDimensionsChange()
        {
            base.OnRectTransformDimensionsChange();

            switch (_aspectMode)
            {
                case AspectMode.HeightControlsWidth:
                    _layoutElement.preferredHeight = -1;
                    _layoutElement.preferredWidth = _rectTransform.rect.height * _aspectRatio;
                    break;

                case AspectMode.WidthControlsHeight:
                    _layoutElement.preferredWidth = -1;
                    _layoutElement.preferredHeight = _rectTransform.rect.width * _aspectRatio;
                    break;
            }
        }

        private enum AspectMode
        {
            None,
            WidthControlsHeight,
            HeightControlsWidth
        }
    }
}