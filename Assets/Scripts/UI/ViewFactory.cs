﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.UI
{
    [Serializable]
    public abstract class ViewFactory<TModel, TModelTypeReference, TModelPresenterMap> :
        GameObjectFactory<TModel, TModelTypeReference, TModelPresenterMap>
        where TModelPresenterMap : KeyValueList<TModelTypeReference, GameObject>
        where TModelTypeReference : TypeReference<TModel>
        where TModel : class
    {
        public override GameObject Create(TModel element, Instantiator instantiator, Vector3 position)
        {
            GameObject instance = base.Create(element, instantiator, position);

            var presenterHub = instance.GetComponent<PresenterHub>();
            if (presenterHub != null)
            {
                presenterHub.Initialize(element);
            }
            else
            {
                // Workaround to be compatible with prefabs not containing PresenterHub.
                // Finally should work only through PresenterHub.

                UnityEngine.Debug.LogWarning(
                        $"GameObject {instance.name} must have a {nameof(PresenterHub)} to initialize " +
                        $"{element.GetType().Name}. Trying to find all components implementing " +
                        $"{nameof(IPresenter<TModel>)}[{element.GetType().Name}] for initialization."
                    );

                IPresenter<TModel>[] presenters = instance.GetComponents<IPresenter<TModel>>();
                foreach (IPresenter<TModel> presenter in presenters)
                {
                    presenter?.Initialize(element);
                }
            }

            return instance;
        }

        public GameObject Create(
                TModel element,
                Instantiator instantiator,
                Transform parent,
                bool worldPositionStays = true
            )
        {
            GameObject instance = Create(element, instantiator);
            instance.transform.SetParent(parent, worldPositionStays);
            return instance;
        }
    }
}