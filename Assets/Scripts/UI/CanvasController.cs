﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.ComponentModel;
using DreamTeam.Screen;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using DeviceType = DreamTeam.Screen.DeviceType;

namespace DreamTeam.UI
{
    [ExecuteInEditMode, RequireComponent(typeof(CanvasScaler), typeof(Canvas))]
    public class CanvasController : MonoBehaviour
    {
        [SerializeField] private RenderMode _renderMode = RenderMode.ScreenSpaceCamera;
        [SerializeField] private ForcedScalingType _forcedScalingType = ForcedScalingType.None;

        [Inject] private ScreenController _screenInjected;
        [Inject] private ScreenOrientationNotifier _orientationNotifier;

        private ScreenController _screen
        {
            get
            {
                if (Application.isEditor)
                {
                    #if UNITY_EDITOR
                    {
                        return SystemAssetAccessor<ScreenController>.Asset;
                    }
                    #endif
                }

                return _screenInjected;
            }
        }

        protected void OnEnable()
        {
            if (!EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                _orientationNotifier = gameObject.EnsureComponentExists<ScreenOrientationNotifier>(
                        notifier => notifier.hideFlags = HideFlags.HideAndDontSave
                    );
            }

            var canvas = GetComponent<Canvas>();
            canvas.worldCamera = Camera.main;
            canvas.renderMode = _renderMode;
            canvas.planeDistance = _screen.PlaneDistance;

            #if UNITY_EDITOR
            {
                _screen.EditorScreenSettings.SimulatingDeviceChanged += UpdateReferenceResolution;
            }
            #endif

            _orientationNotifier.OrientationChanged += OnOrientationChanged;
            UpdateReferenceResolution();
        }

        protected void OnDisable()
        {
            _orientationNotifier.OrientationChanged -= OnOrientationChanged;

            #if UNITY_EDITOR
            {
                _screen.EditorScreenSettings.SimulatingDeviceChanged -= UpdateReferenceResolution;
            }
            #endif
        }

        protected void OnValidate()
        {
            if (isActiveAndEnabled)
            {
                UpdateReferenceResolution();
            }
        }

        private void OnOrientationChanged(ScreenOrientationNotifier.Orientation orientation)
        {
            UpdateReferenceResolution();
        }

        private void UpdateReferenceResolution()
        {
            Vector2 referenceLandscapeResolution = _screen.ReferenceLandscapeResolution;

            if ((_screen.Type == DeviceType.Tablet && _forcedScalingType == ForcedScalingType.None) ||
                _forcedScalingType == ForcedScalingType.ForceTablet)
            {
                referenceLandscapeResolution.y *= 2f - _screen.TabletScaleFactor;
            }

            var canvasScaler = GetComponent<CanvasScaler>();

            switch (_orientationNotifier.CurrentOrientation)
            {
                case ScreenOrientationNotifier.Orientation.Landscape:
                    canvasScaler.matchWidthOrHeight = 1f;
                    break;

                case ScreenOrientationNotifier.Orientation.Portrait:
                    canvasScaler.matchWidthOrHeight = 0f;

                    float tmp = referenceLandscapeResolution.x;
                    referenceLandscapeResolution.x = referenceLandscapeResolution.y;
                    referenceLandscapeResolution.y = tmp;

                    break;

                default:
                    throw new InvalidEnumArgumentException();
            }

            canvasScaler.referenceResolution = referenceLandscapeResolution;
        }

        private enum ForcedScalingType
        {
            None,
            ForceTablet,
            ForcePhone
        }
    }
}