﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using DreamTeam.Quests;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI
{
    [RequireComponent(typeof(QuestManager))]
    public class LastQuestPackSequence : MonoBehaviour
    {
        [SerializeField] private SceneReference _questsPopupScene;

        [Inject] private PopupSystem _popupSystem;

        private QuestManager _questManager => GetComponent<QuestManager>();

        protected void Awake()
        {
            _questManager.LastPackIsWaitingForCompletion += OnLastPackIsWaitingForCompletion;
        }

        protected void OnDestroy()
        {
            _questManager.LastPackIsWaitingForCompletion -= OnLastPackIsWaitingForCompletion;
        }

        private void OnLastPackIsWaitingForCompletion()
        {
            _popupSystem.Show(_questsPopupScene.Name);
        }
    }
}