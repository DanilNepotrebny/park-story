﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Messaging;
using DreamTeam.Messaging.Filtering;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.MessagePresenters
{
    public class ActivitySetterByMessageFilters : MonoBehaviour, IPresenter<ICollection<IMessage>>
    {
        [SerializeField, RequiredField] private List<MessageFilter> _includeFilters;
        [SerializeField, RequiredField] private List<MessageFilter> _excludeFilters;

        [Inject] private MessageSystem _messageSystem;

        private List<IMessage> _includedMessages = new List<IMessage>();
        private List<IMessage> _excludedMessages = new List<IMessage>();

        public void Initialize(ICollection<IMessage> messages)
        {
            ApplyFilters(messages, _includeFilters, _includedMessages);
            ApplyFilters(messages, _excludeFilters, _excludedMessages);
            UpdateView();
        }

        private void UpdateView()
        {
            gameObject.SetActive(_includedMessages.Count > 0 && _excludedMessages.Count == 0);
        }

        protected void Awake()
        {
            _messageSystem.MessageRemoved += OnMessageRemoved;
        }

        protected void OnDestroy()
        {
            _messageSystem.MessageRemoved -= OnMessageRemoved;
        }

        private void OnMessageRemoved(IMessage message)
        {
            _includedMessages.Remove(message);
            _excludedMessages.Remove(message);
            UpdateView();
        }

        private void ApplyFilters(ICollection<IMessage> messages, List<MessageFilter> filters, List<IMessage> result)
        {
            foreach (var filter in filters)
            {
                filter.Apply(messages, result);
            }
        }
    }
}