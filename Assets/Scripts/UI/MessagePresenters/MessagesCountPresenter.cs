﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Messaging;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.MessagePresenters
{
    [RequireComponent(typeof(Text))]
    public class MessagesCountPresenter : MonoBehaviour, IPresenter<ICollection<IMessage>>
    {
        [SerializeField, Tooltip("{0} - messages count")] private string _textFormat = "{0}";

        public void Initialize(ICollection<IMessage> model)
        {
            GetComponent<Text>().text = string.Format(_textFormat, model.Count);
        }
    }
}