﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.GameControls;
using DreamTeam.Messaging;
using UnityEngine;

namespace DreamTeam.UI.MessagePresenters
{
    public class GameControlsModeSetterByMessagesCount : BaseGameControlsModeHandler, IPresenter<ICollection<IMessage>>
    {
        [SerializeField] private int _valueThreshold;
        [SerializeField] private CompareType _comparison = CompareType.Equal;

        public void Initialize(ICollection<IMessage> model)
        {
            if (ShouldBeModeEnabled(model.Count))
            {
                if (!IsModeEnabled)
                {
                    EnableMode();
                }
            }
            else
            {
                if (IsModeEnabled)
                {
                    DisableMode();
                }
            }
        }

        protected void Awake()
        {
            _overridenContext = _controlsSystem.ActiveContext;
        }

        private bool ShouldBeModeEnabled(int messagesCount)
        {
            switch (_comparison)
            {
                case CompareType.Less:
                    return messagesCount < _valueThreshold;
                case CompareType.LessEqual:
                    return messagesCount <= _valueThreshold;
                case CompareType.Equal:
                    return messagesCount == _valueThreshold;
                case CompareType.GreaterEqual:
                    return messagesCount >= _valueThreshold;
                case CompareType.Greater:
                    return messagesCount > _valueThreshold;
                case CompareType.NotEqual:
                    return messagesCount != _valueThreshold;
                default:
                    throw new ArgumentOutOfRangeException(
                        nameof(_comparison), _comparison, $"Unhandled {nameof(CompareType)} value");
            }
        }

        #region Inner types

        public enum CompareType
        {
            Less,
            LessEqual,
            Equal,
            GreaterEqual,
            Greater,
            NotEqual,
        }

        #endregion Inner types
    }
}