﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Messaging;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.MessagePresenters
{
    [RequireComponent(typeof(Button))]
    public class ConsumeMessageButton : MonoBehaviour, IPresenter<IMessage>
    {
        [Inject] private MessageSystem _messageSystem;

        private IMessage _message;

        private Button _button => GetComponent<Button>();

        public void Initialize(IMessage model)
        {
            _message = model;
        }

        protected void Awake()
        {
            _button.onClick.AddListener(OnButtonClicked);
        }

        protected void OnDestroy()
        {
            _button.onClick.RemoveListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            if (_message == null)
            {
                return;
            }

            if (_message.CanBeConsumed)
            {
                _message.Consume();
            }
            else
            {
                _message.NotifyConsumptionRefusal();
            }
        }
    }
}