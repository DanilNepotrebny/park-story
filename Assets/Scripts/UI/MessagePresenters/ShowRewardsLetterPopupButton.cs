// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Messaging;
using DreamTeam.Popups;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.MessagePresenters
{
    [RequireComponent(typeof(Button))]
    public class ShowRewardsLetterPopupButton : MonoBehaviour, IPresenter<IMessage>
    {
        [SerializeField] private SceneReference _defaultPopupScene;
        [Inject] private PopupSystem _popupSystem;
        [Inject] private MessageSystem _messageSystem;

        private IMessage _message;

        public void Initialize(IMessage model)
        {
            _message = model;
        }

        protected void Awake()
        {
            _messageSystem.MessageRemoved += OnMessageRemoved;
        }

        protected void OnDestroy()
        {
            _messageSystem.MessageRemoved -= OnMessageRemoved;
        }

        protected void OnEnable()
        {
            GetComponent<Button>().onClick.AddListener(OnButtonClicked);
        }

        protected void OnDisable()
        {
            GetComponent<Button>().onClick.RemoveListener(OnButtonClicked);
        }

        private void OnMessageRemoved(IMessage message)
        {
            if (_message == message)
            {
                _message = null;
            }
        }

        private void OnButtonClicked()
        {
            if (_message != null)
            {
                var letter = _message as CharacterRewardsMessage;

                string sceneName = (letter != null && !string.IsNullOrEmpty(letter.PopupSceneName)) ? letter.PopupSceneName : _defaultPopupScene.Name;
                _popupSystem.Show(sceneName, new SingleMessagePopup.Args() { Message = _message });
            }
        }
    }
}