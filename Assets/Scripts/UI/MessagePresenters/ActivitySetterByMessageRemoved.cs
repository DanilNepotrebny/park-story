﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Messaging;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.MessagePresenters
{
    public class ActivitySetterByMessageRemoved : MonoBehaviour, IPresenter<IMessage>
    {
        [SerializeField] private bool _messageRemovedState;

        [Inject] private MessageSystem _system;

        private IMessage _message;

        public void Initialize(IMessage model)
        {
            _message = model;
            gameObject.SetActive(!_messageRemovedState);
        }

        protected void Awake()
        {
            _system.MessageRemoved += UpdateActivity;
        }

        protected void OnDestroy()
        {
            _system.MessageRemoved -= UpdateActivity;
        }

        private void UpdateActivity(IMessage message)
        {
            if (_message == message)
            {
                gameObject.SetActive(_messageRemovedState);
            }
        }
    }
}