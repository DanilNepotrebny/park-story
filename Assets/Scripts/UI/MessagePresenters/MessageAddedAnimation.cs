﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Messaging;
using DreamTeam.Messaging.Filtering;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.MessagePresenters
{
    [RequireComponent(typeof(Animation))]
    public class MessageAddedAnimation : MonoBehaviour, IPresenter<MessageFilter>
    {
        [Inject] private MessageSystem _messageSystem;

        private MessageFilter _filter;
        private List<IMessage> _filteredMessages = new List<IMessage>();

        private Animation _animation => GetComponent<Animation>();

        public void Initialize(MessageFilter model)
        {
            _filter = model;
        }

        protected void OnEnable()
        {
            if (_filter != null)
            {
                _messageSystem.MessageAdded += OnMessageAdded;

                _messageSystem.Filter(_filter, _filteredMessages);
                if (_filteredMessages.Count > 0)
                {
                    PlayAnimation();
                }
            }
        }

        protected void OnDisable()
        {
            if (_filter != null)
            {
                _messageSystem.MessageAdded -= OnMessageAdded;
            }
        }

        private void OnMessageAdded(IMessage message)
        {
            if (_filter.IsSuitable(message))
            {
                _filteredMessages.Add(message);
                PlayAnimation();
            }
        }

        private void PlayAnimation()
        {
            if (isActiveAndEnabled && !_animation.isPlaying)
            {
                _animation.Play();
            }
        }
    }
}