﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Messaging;
using DreamTeam.SocialNetworking;

namespace DreamTeam.UI.MessagePresenters
{
    public class RewardsMessageSenderPresenter : PresenterProxy<RewardsMessage, SocialProfile>
    {
        protected override SocialProfile TargetModel => SourceModel.Sender;
    }
}