﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Messaging.Filtering;
using DreamTeam.Popups;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.MessagePresenters
{
    [RequireComponent(typeof(Button))]
    public class ShowFilteredMessagesPopupButton : MonoBehaviour, IPresenter<MessageFilter>
    {
        [SerializeField] private SceneReference _messagesCollectionPopup;

        [Inject] private PopupSystem _popupSystem;

        private MessageFilter _filter;

        private Button _button => GetComponent<Button>();

        public void Initialize(MessageFilter model)
        {
            _filter = model;
        }

        protected void Awake()
        {
            _button.onClick.AddListener(OnButtonClicked);
        }

        protected void OnDestroy()
        {
            _button.onClick.RemoveListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            _popupSystem.Show(
                    _messagesCollectionPopup.Name,
                    new FilteredMessagesPopup.Args { Filter = _filter }
                );
        }
    }
}