﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Messaging;
using DreamTeam.Messaging.Filtering;
using Zenject;

namespace DreamTeam.UI.MessagePresenters
{
    public class MessageSystemFilteredMessagesPresenter : PresenterProxy<MessageFilter, ICollection<IMessage>>
    {
        [Inject] private MessageSystem _messageSystem;

        private List<IMessage> _filteredMessages = new List<IMessage>();

        protected override ICollection<IMessage> TargetModel
        {
            get
            {
                _messageSystem.Filter(SourceModel, _filteredMessages);
                return _filteredMessages.AsReadOnly();
            }
        }

        protected void Awake()
        {
            _messageSystem.MessageAdded += OnMessageAdded;
            _messageSystem.MessageRemoved += OnMessageRemoved;
        }

        protected void OnDestroy()
        {
            _messageSystem.MessageAdded -= OnMessageAdded;
            _messageSystem.MessageRemoved -= OnMessageRemoved;
        }

        private void OnMessageRemoved(IMessage obj)
        {
            Initialize(SourceModel);
        }

        private void OnMessageAdded(IMessage obj)
        {
            Initialize(SourceModel);
        }
    }
}