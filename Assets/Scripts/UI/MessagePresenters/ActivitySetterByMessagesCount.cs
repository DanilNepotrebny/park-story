﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Messaging;
using UnityEngine;

namespace DreamTeam.UI.MessagePresenters
{
    public class ActivitySetterByMessagesCount : MonoBehaviour, IPresenter<ICollection<IMessage>>
    {
        [SerializeField] private int _countTreshold = 0;

        public void Initialize(ICollection<IMessage> model)
        {
            gameObject.SetActive(model.Count > _countTreshold);
        }
    }
}