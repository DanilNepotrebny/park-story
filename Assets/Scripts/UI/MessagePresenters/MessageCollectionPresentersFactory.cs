// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Messaging;
using DreamTeam.Popups;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.MessagePresenters
{
    public class MessageCollectionPresentersFactory : MonoBehaviour, IPresenter<ICollection<IMessage>>
    {
        [SerializeField, RequiredField] private MessageViewFactory _viewFactory;

        [Inject] private Instantiator _instantiator;
        [Inject] private Popup _popup;

        private Dictionary<IMessage, PresenterHub> _presenters = new Dictionary<IMessage, PresenterHub>();

        public void Initialize(ICollection<IMessage> model)
        {
            SyncPresenters(model);
        }

        protected void Awake()
        {
            _popup.Hidden += OnPopupHidden;
        }

        protected void Ondestroy()
        {
            _popup.Hidden -= OnPopupHidden;
        }

        private void OnPopupHidden(Popup popup)
        {
            foreach (var presenter in _presenters.Values)
            {
                presenter.gameObject.Dispose();
            }
            _presenters.Clear();
        }

        private void SyncPresenters(ICollection<IMessage> messages)
        {
            List<IMessage> presentedMessages = new List<IMessage>(_presenters.Keys);
            for (int index = presentedMessages.Count - 1; index >= 0; index--)
            {
                if (!messages.Contains(presentedMessages[index]))
                {
                    _presenters.Remove(presentedMessages[index]);
                }
            }

            foreach (IMessage message in messages)
            {
                PresenterHub presenter;
                if (_presenters.TryGetValue(message, out presenter))
                {
                    presenter.Initialize(message);
                }
                else
                {
                    GameObject presenterGO = _viewFactory.Create(message, _instantiator, transform, false);
                    _presenters.Add(message, presenterGO.GetComponent<PresenterHub>());
                }
            }
        }
    }
}