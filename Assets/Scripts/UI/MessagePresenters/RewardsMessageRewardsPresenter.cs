﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Messaging;

namespace DreamTeam.UI.MessagePresenters
{
    public class RewardsMessageRewardsPresenter : PresenterProxy<RewardsMessage, ICollection<Reward>>
    {
        protected override ICollection<Reward> TargetModel => SourceModel.Rewards;
    }
}