﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Messaging;

namespace DreamTeam.UI.MessagePresenters
{
    public class MessageCollectionLastItemPresenter : PresenterProxy<ICollection<IMessage>, IMessage>
    {
        protected override IMessage TargetModel => SourceModel.LastOrDefault();
    }
}