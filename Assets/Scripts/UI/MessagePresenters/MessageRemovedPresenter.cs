﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Messaging;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.MessagePresenters
{
    public class MessageRemovedPresenter : MonoBehaviour, IPresenter<IMessage>
    {
        [SerializeField, RequiredField] private SimpleAnimation _animation;
        [Inject] private MessageSystem _messageSystem;

        private IMessage _message;
        private Mask _parentMask;

        public void Initialize(IMessage model)
        {
            _message = model;
        }

        protected void OnEnable()
        {
            _messageSystem.MessageRemoved += OnMessageRemoved;
            _animation.AnimationFinished += OnAnimationFinished;
        }

        protected void OnDisable()
        {
            _messageSystem.MessageRemoved -= OnMessageRemoved;
            _animation.AnimationFinished -= OnAnimationFinished;
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType obj)
        {
            if (_parentMask != null)
            {
                _animation.transform.SetParent(transform);
            }
            gameObject.Dispose();
        }

        private void OnMessageRemoved(IMessage message)
        {
            if (_message == message)
            {
                _parentMask = GetComponentInParent<Mask>();
                if (_parentMask != null)
                {
                    _animation.transform.SetParent(_parentMask.transform.parent);
                }

                if (!_animation.Play(SimpleAnimationType.Hide))
                {
                    OnAnimationFinished(_animation, SimpleAnimationType.Hide);
                }
            }
        }
    }
}