﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.Localization;
using DreamTeam.SocialNetworking;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.MessagePresenters
{
    [RequireComponent(typeof(Text))]
    public class SendRecieveMessageTextPresenter : MonoBehaviour, IPresenter<SocialProfile>, IPresenter<CountedItem>
    {
        [Inject] private LocalizationSystem _localizationSystem;

        [SerializeField, Tooltip("{0} - sender FirstName; {1} - CountedItem")] private string _textFormat = "{0}";

        private string _socialProfileName = "{0}";
        private string _countedItemName = "{1}";

        public void Initialize(SocialProfile model)
        {
            _socialProfileName = model.Name;
            UpdateView();
        }

        public void Initialize(CountedItem model)
        {
            _countedItemName = _localizationSystem.Localize(model.Name, model);
            UpdateView();
        }

        private void UpdateView()
        {
            GetComponent<Text>().text = string.Format(_textFormat, _socialProfileName, _countedItemName);
        }
    }
}