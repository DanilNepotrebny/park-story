﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Messaging.Filtering;

namespace DreamTeam.UI.MessagePresenters
{
    public class MessageFilterPresenterInitializer : PresenterInitializer<MessageFilter>
    {
    }
}