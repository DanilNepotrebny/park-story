﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Messaging;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.UI.MessagePresenters
{
    [CreateAssetMenu]
    public class MessageViewFactory :
        ViewFactory<IMessage, DrawableMessageType, MessageViewFactory.PresentersMap>
    {
        [Serializable]
        public class PresentersMap : KeyValueList<DrawableMessageType, GameObject>
        {
        }
    }
}