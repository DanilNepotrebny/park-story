﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.UI.UIParticles
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(ParticleSystem), typeof(RectTransform))]
    public class UIParticleSystem : MonoBehaviour
    {
        [ReadOnly, RequiredField, SerializeField]
        private ParticleSystem _particleSystem;

        [ReadOnly, RequiredField, SerializeField]
        private ParticleSystemRenderer _particleSystemRenderer;

        private UIParticleGraphic _particleUIGraphic;
        private UIParticleTrailGraphic _particleTrailUIGraphic;

        protected void OnEnable()
        {
            ResetRenderers();
        }

        protected void OnDisable()
        {
            CleanupRenderers();
        }

        private void ResetRenderers()
        {
            CleanupRenderers();
            InitRenderers();
        }

        private void InitRenderers()
        {
            _particleSystem = GetComponent<ParticleSystem>();
            _particleSystemRenderer = GetComponent<ParticleSystemRenderer>();

            Assert.IsNotNull(_particleSystem, "[PROGRAMMERS] Cannot find particle system!");
            Assert.IsNotNull(_particleSystemRenderer, "[PROGRAMMERS] cannot find particle system renderer!");

            if (_particleSystem != null)
            {
                const int regularGraphicSiblingIndex = 0;
                _particleUIGraphic = CreateParticleRendererObject<UIParticleGraphic>(regularGraphicSiblingIndex);
            }

            if (_particleSystem != null && _particleSystem.trails.enabled)
            {
                const int trailGraphicSiblingIndex = 1;
                _particleTrailUIGraphic = CreateParticleRendererObject<UIParticleTrailGraphic>(trailGraphicSiblingIndex);
            }
        }

        private void CleanupRenderers()
        {
            if (_particleUIGraphic != null)
            {
                _particleUIGraphic.gameObject.Dispose();
            }

            if (_particleTrailUIGraphic != null)
            {
                _particleTrailUIGraphic.gameObject.Dispose();
            }

            _particleUIGraphic = null;
            _particleTrailUIGraphic = null;
        }

        private T CreateParticleRendererObject<T>(int index) where T : UIParticleGraphicBase
        {
            GameObject particleObject = new GameObject("Renderer");
            particleObject.transform.SetParent(transform);
            particleObject.transform.SetSiblingIndex(index);

            RectTransform particleObjectTransform = particleObject.AddComponent<RectTransform>();
            particleObjectTransform.anchorMin = Vector2.zero;
            particleObjectTransform.anchorMax = Vector2.one;
            particleObjectTransform.anchoredPosition = Vector2.zero;
            particleObjectTransform.sizeDelta = Vector2.zero;
            particleObjectTransform.localRotation = Quaternion.identity;
            particleObjectTransform.localScale = Vector3.one;

            particleObject.hideFlags = HideFlags.DontSave | HideFlags.HideInHierarchy;

            var result = particleObject.AddComponent<T>();
            result.CachedParticleSystem = _particleSystem;
            result.CachedParticleSystemRenderer = _particleSystemRenderer;

            return result;
        }
    }
}