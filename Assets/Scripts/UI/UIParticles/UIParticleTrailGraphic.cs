﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.UI.UIParticles
{
    [ExecuteInEditMode]
    [AddComponentMenu("")]
    public sealed class UIParticleTrailGraphic : UIParticleGraphicBase
    {
        protected override Texture GetParticleTexture()
        {
            Texture texture = null;
            if (CachedParticleSystemRenderer)
            {
                Material trailMaterial = CachedParticleSystemRenderer.trailMaterial;
                if (trailMaterial && trailMaterial.HasProperty(_mainTextureId))
                {
                    texture = trailMaterial.mainTexture;
                }
            }

            return texture ?? s_WhiteTexture;
        }

        protected override void Bake(Mesh mesh, Camera currentCamera, bool useTransform)
        {
            CachedParticleSystemRenderer.BakeTrailsMesh(mesh, currentCamera, useTransform);
        }
    }
}