﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.UIParticles
{
    [ExecuteInEditMode]
    public abstract class UIParticleGraphicBase : MaskableGraphic
    {
        protected static readonly int _mainTextureId = Shader.PropertyToID("_MainTex");

        private Mesh _mesh;
        private List<Vector3> _vertices = new List<Vector3>();

        public ParticleSystem CachedParticleSystem { get; set; }
        public ParticleSystemRenderer CachedParticleSystemRenderer { get; set; }

        public sealed override Texture mainTexture => GetParticleTexture();

        public sealed override Material GetModifiedMaterial(Material baseMaterial)
        {
            return base.GetModifiedMaterial(CachedParticleSystemRenderer ? CachedParticleSystemRenderer.sharedMaterial : baseMaterial);
        }

        protected sealed override void OnEnable()
        {
            base.OnEnable();

            _mesh = new Mesh();
            _mesh.MarkDynamic();

            Canvas.willRenderCanvases += UpdateMesh;
        }

        protected sealed override void OnDisable()
        {
            Canvas.willRenderCanvases -= UpdateMesh;

            _mesh.DisposeImmediate();
            _mesh = null;

            base.OnDisable();
        }

        protected sealed override void UpdateGeometry()
        {
        }

        protected abstract Texture GetParticleTexture();
        protected abstract void Bake(Mesh mesh, Camera currentCamera, bool useTransform);

        private void UpdateMesh()
        {
            if (canvas && CachedParticleSystem)
            {
                Camera currentCamera = GetCamera();
                if (currentCamera != null)
                {
                    Matrix4x4 requiredTransform;
                    bool shouldIncludeLocalTransform;
                    CalculateTransform(out requiredTransform, out shouldIncludeLocalTransform);

                    _mesh.Clear();

                    if (CachedParticleSystem.particleCount > 0)
                    {
                        Bake(_mesh, currentCamera, shouldIncludeLocalTransform);

                        _mesh.GetVertices(_vertices);
                        for (int i = 0; i < _vertices.Count; i++)
                        {
                            _vertices[i] = requiredTransform.MultiplyPoint3x4(_vertices[i]);
                        }

                        _mesh.SetVertices(_vertices);
                        _vertices.Clear();
                    }

                    canvasRenderer.SetMesh(_mesh);
                    canvasRenderer.SetTexture(mainTexture);
                }
            }
        }

        private Camera GetCamera()
        {
            Camera result = canvas.worldCamera ?? Camera.main;

            #if UNITY_EDITOR
            {
                if (result == null)
                {
                    result = Camera.current;
                }
            }
            #endif // UNITY_EDITOR

            return result;
        }

        private void CalculateTransform(out Matrix4x4 requiredTransform, out bool shouldIncludeLocalTransform)
        {
            requiredTransform = Matrix4x4.identity;
            shouldIncludeLocalTransform = false;

            if (CachedParticleSystem && CachedParticleSystemRenderer)
            {
                switch (CachedParticleSystem.main.simulationSpace)
                {
                    case ParticleSystemSimulationSpace.Local:
                        requiredTransform = Matrix4x4.Rotate(CachedParticleSystem.transform.rotation).inverse * Matrix4x4.Scale(CachedParticleSystem.transform.lossyScale).inverse;
                        shouldIncludeLocalTransform = true;
                        break;

                    case ParticleSystemSimulationSpace.World:
                        requiredTransform = CachedParticleSystem.transform.worldToLocalMatrix;
                        break;

                    case ParticleSystemSimulationSpace.Custom:
                        break;
                }
            }
        }
    }
}