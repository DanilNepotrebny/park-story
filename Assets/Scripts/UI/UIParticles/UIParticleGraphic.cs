﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.UI.UIParticles
{
    [ExecuteInEditMode]
    [AddComponentMenu("")]
    public sealed class UIParticleGraphic : UIParticleGraphicBase
    {
        protected override Texture GetParticleTexture()
        {
            Texture texture = null;
            var textureSheet = CachedParticleSystem.textureSheetAnimation;
            if (textureSheet.enabled &&
                textureSheet.mode == ParticleSystemAnimationMode.Sprites &&
                textureSheet.spriteCount > 0)
            {
                texture = textureSheet.GetSprite(0).texture;
            }

            if (!texture && CachedParticleSystemRenderer)
            {
                Material particlesMaterial = Application.isPlaying ? CachedParticleSystemRenderer.material : CachedParticleSystemRenderer.sharedMaterial;
                if (particlesMaterial && particlesMaterial.HasProperty(_mainTextureId))
                {
                    texture = particlesMaterial.mainTexture;
                }
            }

            return texture ?? s_WhiteTexture;
        }

        protected override void Bake(Mesh mesh, Camera currentCamera, bool useTransform)
        {
            CachedParticleSystemRenderer.BakeMesh(mesh, currentCamera, useTransform);
        }
    }
}