// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.UI
{
    /// <summary>
    /// Base class for initialization <see cref="PresenterHub"/> from Unity inspector.
    /// </summary>
    /// <typeparam name="TModel">Model type</typeparam>
    [RequireComponent(typeof(PresenterHub))]
    public abstract class PresenterInitializer<TModel> : MonoBehaviour
        where TModel : class
    {
        [SerializeField, RequiredField] private TModel _model;

        private PresenterHub _hub => GetComponent<PresenterHub>();
        private bool _hasStarted;

        protected void OnEnable()
        {
            if (_hasStarted)
            {
                InitializeHub();
            }
        }

        protected void Start()
        {
            InitializeHub();
            _hasStarted = true;
        }

        private void InitializeHub()
        {
            Assert.IsNotNull(_model, $"{GetType().Name} on {name}. {nameof(_model)} is null.");
            _hub.Initialize(_model);
        }

        protected void OnValidate()
        {
            Assert.IsNotNull(
                    _hub,
                    $"{GetType().Name} on {name}. Can't get required component of type {nameof(PresenterHub)}."
                );
        }
    }
}
