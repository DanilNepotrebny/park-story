﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI
{
    [RequireComponent(typeof(Toggle))]
    public class ParentToggleGroupFinder : MonoBehaviour
    {
        protected void Awake()
        {
            ToggleGroup group = GetComponentInParent<ToggleGroup>();
            if (group != null)
            {
                GetComponent<Toggle>().group = group;
            }
        }
    }
}