﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using DreamTeam.Quests;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI
{
    [RequireComponent(typeof(QuestPack))]
    public class QuestPackRewardSequence : MonoBehaviour
    {
        [SerializeField] private SceneReference _questsPopupScene;

        [Inject] private PopupSystem _popupSystem;

        private QuestPack _questPack => GetComponent<QuestPack>();

        protected void Awake()
        {
            _questPack.GivingRewards += OnGivingRewards;
        }

        protected void OnDestroy()
        {
            _questPack.GivingRewards -= OnGivingRewards;
        }

        private void OnGivingRewards(IDeferredInvocationHandle handle, QuestPackReward rewards)
        {
            var popupArgs = new QuestsAndFriendsPopup.Args()
            {
                GivingRewardInvocation = handle.Lock(),
                RewardData = rewards
            };

            _popupSystem.Show(_questsPopupScene.Name, popupArgs);
        }
    }
}