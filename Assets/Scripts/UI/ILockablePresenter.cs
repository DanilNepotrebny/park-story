﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.UI
{
    public interface ILockablePresenter<TUpdateArgs>
    {
        void Lock();
        void Unlock();
        void UpdateManualy(TUpdateArgs updateArgs);
    }
}