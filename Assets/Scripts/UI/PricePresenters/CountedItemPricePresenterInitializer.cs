﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Prices;

namespace DreamTeam.UI.PricePresenters
{
    public class CountedItemPricePresenterInitializer : PresenterInitializer<CountedItemPrice>
    {
    }
}