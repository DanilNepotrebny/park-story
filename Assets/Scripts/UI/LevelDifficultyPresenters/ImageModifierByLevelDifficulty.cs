﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.LevelDifficultyPresenters
{
    public class ImageModifierByLevelDifficulty :
		ComponentModifierByLevelDifficulty<Image, ImageModifierByLevelDifficulty.ImageSettings>
    {
		[Serializable]
        public class ImageSettings : Settings
        {
            [SerializeField, RequiredField] private Sprite _sprite;

            public override void Apply(Image target)
            {
                target.sprite = _sprite;
            }
        }
    }
}