﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3.Levels;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.UI.LevelDifficultyPresenters
{
    public abstract class ComponentModifierByLevelDifficulty<TApplyTarget, TSettings> :
        MonoBehaviour, IPresenter<LevelDifficulty>
        where TApplyTarget : Component
        where TSettings : ComponentModifierByLevelDifficulty<TApplyTarget, TSettings>.Settings
    {
        [SerializeField, RequiredField] private TApplyTarget _target;
        [SerializeField] private List<TSettings> _settings;

        public void Initialize(LevelDifficulty model)
        {
            TSettings settings = _settings.FirstOrDefault(s => model == s.Dificulty);
            if (settings != null)
            {
                settings.Apply(_target);
            }
        }

        [Serializable]
        public abstract class Settings
        {
            [SerializeField, RequiredField] private LevelDifficulty _diffuculty;
            public LevelDifficulty Dificulty => _diffuculty;

            public abstract void Apply(TApplyTarget target);
        }
    }
}