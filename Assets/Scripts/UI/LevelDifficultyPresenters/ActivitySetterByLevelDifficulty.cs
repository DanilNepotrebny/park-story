﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Levels;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.UI.LevelDifficultyPresenters
{
    public class ActivitySetterByLevelDifficulty : MonoBehaviour, IPresenter<LevelDifficulty>
    {
        [SerializeField, RequiredField] private LevelDifficulty _targetDiffuculty;

        public void Initialize(LevelDifficulty model)
        {
            gameObject.SetActive(model == _targetDiffuculty);
        }
    }
}