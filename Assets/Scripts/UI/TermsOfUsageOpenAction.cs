﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

namespace DreamTeam.UI
{
    public class TermsOfUsageOpenAction : MonoBehaviour
    {
        [Inject] private GameConstantsAsset _gameConstantsAsset;

        public void OpenTermsOfUsage()
        {
            Application.OpenURL(_gameConstantsAsset.TermsOfUsageURL);
        }
    }
}