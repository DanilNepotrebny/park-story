﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Quests;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.Dialogs
{
    [RequireComponent(typeof(SimpleAnimation))]
    public class QuestNotification : MonoBehaviour, IPresenter<Quest>
    {
        [SerializeField] private Image _questIcon;

        private SimpleAnimation _animation;
        private Action _finishCallback;

        public void Initialize(Quest quest)
        {
            _questIcon.sprite = quest.NotificationIcon;
        }

        public void Show(Action finishCallback)
        {
            _finishCallback = finishCallback;
            _animation.Play(SimpleAnimationType.Show);
        }

        public void HideAndDestroy(Action finishCallback)
        {
            _finishCallback = finishCallback;
            _animation.Play(SimpleAnimationType.Hide);
        }

        protected void Awake()
        {
            _animation = GetComponent<SimpleAnimation>();
            _animation.AnimationFinished += OnAnimationFinished;
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            if (SimpleAnimationType.Hide == animationType)
            {
                gameObject.Dispose();
            }

            _finishCallback?.Invoke();
            _finishCallback = null;
        }
    }
}