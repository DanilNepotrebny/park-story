﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Quests;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.Dialogs
{
    public class QuestNotificationPanel : MonoBehaviour
    {
        [SerializeField] private QuestNotification _notificationPrefab;

        [Inject] private Instantiator _instantiator;

        private readonly List<QuestNotification> _notifications = new List<QuestNotification>();

        public bool HasNotifications => _notifications.Count > 0;

        public void ShowNotification(Quest quest, Action finishCallback)
        {
            QuestNotification notification = _instantiator.Instantiate(_notificationPrefab, gameObject);
            notification.Initialize(quest);
            _notifications.Add(notification);
            notification.Show(finishCallback);
        }

        public void Hide(Action finishCallback)
        {
            int waitingProcessesCount = 0;
            Action callback = () =>
            {
                waitingProcessesCount--;
                if (waitingProcessesCount <= 0)
                {
                    finishCallback?.Invoke();
                }
            };

            foreach (QuestNotification notification in _notifications)
            {
                waitingProcessesCount++;
                notification.HideAndDestroy(callback);
            }

            _notifications.Clear();
        }
    }
}