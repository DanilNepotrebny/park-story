﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.Utils;
using DreamTeam.Utils.Spine;
using DreamTeam.Utils.UI;
using Spine.Unity;
using Spine.Unity.Modules;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.Dialogs
{
    [RequireComponent(typeof(Animator))]
    public class DialogCharacterController : MonoBehaviour
    {
        [SerializeField] private GameObject _character;
        [SerializeField] private GameObject _characterAdditional;
        [SerializeField] [Range(0f, 10f)] private float _fadeTime;
        [SerializeField] private DialogController.CharacterSide _side;
        [SerializeField, RequiredField] private SpineBoneFollower _bubbleOrigin;
        [SerializeField] private GameObject _backgroundGameObject;
        [SerializeField] private Image _backgroundImage;

        [SerializeField] private string _isActiveParameterName;
        [SerializeField] private string _isOnScreenParameterName;

        [SerializeField] private string _offScreenStateName;
        [SerializeField] private string _onScreenStateName;
        [SerializeField] private string _activeStateName;
        [SerializeField] private string _inactiveStateName;

        [SerializeField] private string _crossfadeInAnimationName = "FadeIn";
        [SerializeField] private string _crossfadeOutAnimationName = "FadeOut";

        private Animator _animator;

        private bool _isActive;
        private bool _isOnScreen;

        private SkeletonDataAsset _currentSkeletonAsset = null;
        private List<CharaсterDisplayData> _displayData = new List<CharaсterDisplayData>();
        private int _currentDisplayCharacterIndex = -1;
        private bool _isCrossFadeEnabled = false;

        private DialogController.CharacterSettings _currentSettings;

        private struct CharaсterDisplayData
        {
            public GameObject Character;
            public AnimationSequencer Sequencer;
            public SkeletonGraphicMultiObject Skeleton;
            public SkeletonGraphicMultiObjectColorSetter SkeletonColorSetter;
            public Animation FadeAnimation;
        }

        public CharacterData CurrentCharacter => _currentSettings?.Character;

        public void ShowCharacter(
                DialogController.CharacterSettings characterSettings,
                bool isActive,
                Action finishCallback
            )
        {
            if (!_isOnScreen && characterSettings.Character == null)
            {
                gameObject.SetActive(false);
                finishCallback?.Invoke();
                return;
            }

            gameObject.SetActive(true);
            StartCoroutine(
                    GetShowingCharacterRoutine(
                            characterSettings,
                            isActive,
                            finishCallback
                        )
                );
        }

        public void Hide(Action finishCallback)
        {
            Action callback = () =>
            {
                _currentSettings = null;
                _currentSkeletonAsset = null;

                foreach (CharaсterDisplayData data in _displayData)
                {
                    data.Sequencer.DropAnimations();
                    data.Sequencer.StopSequence();
                    data.Skeleton.Clear();
                }

                finishCallback?.Invoke();
                gameObject.SetActive(false);
            };

            if (gameObject.activeInHierarchy && _isOnScreen)
            {
                StartCoroutine(MoveCharacterOut(callback));
            }
            else
            {
                callback();
            }
        }

        protected void Awake()
        {
            _animator = GetComponent<Animator>();

            _displayData.Add(new CharaсterDisplayData()
            {
                Character = _character,
                Sequencer = _character.GetComponent<AnimationSequencer>(),
                Skeleton = _character.GetComponent<SkeletonGraphicMultiObject>(),
                SkeletonColorSetter = _character.GetComponent<SkeletonGraphicMultiObjectColorSetter>(),
                FadeAnimation = _character.GetComponent<UnityEngine.Animation>()
            });

            _displayData.Add(new CharaсterDisplayData()
            {
                Character = _characterAdditional,
                Sequencer = _characterAdditional.GetComponent<AnimationSequencer>(),
                Skeleton = _characterAdditional.GetComponent<SkeletonGraphicMultiObject>(),
                SkeletonColorSetter = _characterAdditional.GetComponent<SkeletonGraphicMultiObjectColorSetter>(),
                FadeAnimation = _characterAdditional.GetComponent<UnityEngine.Animation>()
            });
        }

        private IEnumerator GetShowingCharacterRoutine(
                DialogController.CharacterSettings characterSettings,
                bool isActive,
                Action finishCallback
            )
        {
            _isCrossFadeEnabled =
                _currentSettings != null &&
                characterSettings != null &&
                _currentSettings.Character == characterSettings.Character &&
                _isOnScreen &&
                isActive &&
                _currentDisplayCharacterIndex >= 0;

            if (_currentDisplayCharacterIndex < 0)
            {
                _currentDisplayCharacterIndex = 0;
            }

            if (_currentSettings == null ||
                characterSettings == null ||
                _currentSettings.Character != characterSettings.Character ||
                _currentSettings.Background != characterSettings.Background)
            {
                yield return ChangeCharacter(characterSettings, isActive, finishCallback);
            }
            else
            {
                yield return ChangeCharacterActivationState(characterSettings, isActive, finishCallback);
            }
        }

        private void SetupDialogAnimations(DialogController.CharacterSettings characterSettings)
        {
            if (characterSettings != null &&
                characterSettings.Character != null)
            {
                if (_isCrossFadeEnabled)
                {
                    CrossFadeCharacterAnimations(characterSettings);
                }
                else
                {
                    DropAnimationsCharacterWithIndex(_currentDisplayCharacterIndex);
                    DisplayAnimationsCharacterWithIndex(_currentDisplayCharacterIndex, characterSettings);
                }
            }
        }

        private void DropAnimationsCharacterWithIndex(int characterIndex)
        {
            CharaсterDisplayData data = _displayData[characterIndex];
            data.Sequencer.DropAnimations();
        }

        private void DisplayAnimationsCharacterWithIndex(int characterIndex, DialogController.CharacterSettings settings)
        {
            CharaсterDisplayData data = _displayData[characterIndex];

            foreach (var animationSequenceData in settings.AnimationSequence)
            {
                data.Sequencer.AddAnimation(animationSequenceData.AnimationName, animationSequenceData.ReplayAmount);
            }

            data.Sequencer.AddAnimation(settings.Character.DialogIdleAnimationName, 0);
        }

        private void FadeOutCharacterWithIndex(int characterIndex)
        {
            CharaсterDisplayData data = _displayData[characterIndex];
            data.FadeAnimation.Play(_crossfadeOutAnimationName);
        }

        private void FadeInCharacterWithIndex(int characterIndex)
        {
            CharaсterDisplayData data = _displayData[characterIndex];
            data.Character.SetActive(true);
            data.FadeAnimation.Play(_crossfadeInAnimationName);
        }

        private void CrossFadeCharacterAnimations(DialogController.CharacterSettings characterSettings)
        {
            int currentIndex = _currentDisplayCharacterIndex;
            int nextIndex = Repeat(_currentDisplayCharacterIndex + 1, _displayData.Count);

            FadeOutCharacterWithIndex(currentIndex);
            DropAnimationsCharacterWithIndex(currentIndex);

            FadeInCharacterWithIndex(nextIndex);
            DisplayAnimationsCharacterWithIndex(nextIndex, characterSettings);

            _currentDisplayCharacterIndex = nextIndex;
        }

        private IEnumerator ChangeCharacter(
                DialogController.CharacterSettings characterSettings,
                bool isActive,
                Action finishCallback
            )
        {
            if (_isOnScreen)
            {
                foreach (CharaсterDisplayData data in _displayData)
                {
                    data.Sequencer.DropAnimations();
                    data.Sequencer.StopSequence();
                }

                yield return MoveCharacterOut();
            }

            yield return MoveCharacterIn(characterSettings, isActive);

            SetupDialogAnimations(characterSettings);

            finishCallback?.Invoke();
        }

        private IEnumerator ChangeCharacterActivationState(
                DialogController.CharacterSettings characterSettings,
                bool isActive,
                Action finishCallback
            )
        {
            SetActive(isActive);

            SetupDialogAnimations(characterSettings);

            string stateName = isActive ? _activeStateName : _inactiveStateName;
            yield return _animator.WaitState(stateName, 1);

            finishCallback?.Invoke();
        }

        private IEnumerator MoveCharacterOut(Action finishCallback = null)
        {
            SetOnScreen(false);

            yield return _animator.WaitState(_offScreenStateName, 0);

            finishCallback?.Invoke();
        }

        private IEnumerator MoveCharacterIn(
                DialogController.CharacterSettings characterSettings,
                bool isActive
            )
        {
            _currentSettings = characterSettings;

            if (_currentSettings.Character == null)
            {
                yield break;
            }

            if (characterSettings.Character.SkeletonDataAsset != null &&
                _currentSkeletonAsset != characterSettings.Character.SkeletonDataAsset)
            {
                _currentSkeletonAsset = characterSettings.Character.SkeletonDataAsset;

                foreach (CharaсterDisplayData data in _displayData)
                {
                    data.Sequencer.SetupSkeletonGraphic(
                            _currentSkeletonAsset,
                            _side != characterSettings.Character.DefaultSide,
                            false
                        );

                    data.Skeleton.unscaledTime = true;

                    Color skeletonColor = data.SkeletonColorSetter.SkeletonColor;
                    skeletonColor.a = 0f;
                    data.SkeletonColorSetter.SkeletonColor = skeletonColor;
                }
            }

            if (_currentSkeletonAsset != null)
            {
                CharaсterDisplayData data = _displayData[_currentDisplayCharacterIndex];
                data.SkeletonColorSetter.SkeletonColor = Color.white;
                if (!string.IsNullOrEmpty(characterSettings.AppearanceAnimation))
                {
                    data.Sequencer.AddAnimation(characterSettings.AppearanceAnimation, 0);
                }
            }

            _backgroundGameObject.SetActive(characterSettings.Background != null);
            if (characterSettings.Background != null)
            {
                _backgroundImage.sprite = characterSettings.Background;
            }

            _bubbleOrigin.SetTargetBoneName(_currentSettings.Character.BubbleBoneName);

            SetActive(isActive);
            SetOnScreen(true);

            yield return _animator.WaitState(_onScreenStateName, 0);

            DropAnimationsCharacterWithIndex(_currentDisplayCharacterIndex);
        }

        private void SetActive(bool isActive)
        {
            _isActive = isActive;
            _animator.SetBool(_isActiveParameterName, _isActive);
        }

        private void SetOnScreen(bool isOnScreen)
        {
            _isOnScreen = isOnScreen;
            _animator.SetBool(_isOnScreenParameterName, _isOnScreen);
        }

        private int Repeat(int value, int length)
        {
            if (value < 0)
            {
                return 0;
            }
            else if (value >= length)
            {
                return 0;
            }
            else
            {
                return value;
            }
        }
    }
}