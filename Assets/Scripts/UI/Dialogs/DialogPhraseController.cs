﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.Localization;
using DreamTeam.Quests;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.Dialogs
{
    [RequireComponent(typeof(Animator))]
    public class DialogPhraseController : MonoBehaviour
    {
        [Inject] private LocalizationSystem _localizationSystem;

        [SerializeField] private Text _messageText;
        [SerializeField] private SimpleAnimation _messageAnimation;
        [SerializeField] private string _isShownParameterName;
        [SerializeField] private string _shownStateName;
        [SerializeField] private string _hiddenStateName;
        [SerializeField] private QuestNotificationPanel _questNotificationPanel;

        private Animator _animator;

        private Action _finishCallback;

        private bool _isShown = false;

        public void Prepare(
                Action finishCallback
            )
        {
            _messageText.enabled = false;

            gameObject.SetActive(true);

            if (!_isShown)
            {
                _isShown = true;
                StartCoroutine(AnimateShowing(finishCallback));
            }
            else
            {
                if (_questNotificationPanel.HasNotifications)
                {
                    _questNotificationPanel.Hide(finishCallback);
                }
                else
                {
                    finishCallback?.Invoke();
                }
            }
        }

        public void Hide(
                Action finishCallback
            )
        {
            _messageText.enabled = false;

            Action callback = () =>
            {
                _isShown = false;
                gameObject.SetActive(false);
                _finishCallback = null;
                finishCallback?.Invoke();
            };

            if (_isShown)
            {
                int waitingProcessesCount = 0;
                Action waitingCallback = () =>
                {
                    waitingProcessesCount--;
                    if (waitingProcessesCount <= 0)
                    {
                        callback?.Invoke();
                    }
                };

                StartCoroutine(AnimateHiding(waitingCallback));
                _questNotificationPanel.Hide(waitingCallback);
            }
            else
            {
                callback.Invoke();
            }
        }

        public void ShowPhrase(
                DialogController.PhraseSettings phraseSettings,
                IExposedPropertyTable resolver,
                Action finishCallback
            )
        {
            _messageText.enabled = true;
            _messageText.text = _localizationSystem.Localize(phraseSettings.MessageID, this);

            int waitingProcessesCount = 0;
            _finishCallback = () =>
            {
                waitingProcessesCount--;
                if (waitingProcessesCount <= 0)
                {
                    finishCallback?.Invoke();
                }
            };

            {
                waitingProcessesCount++;
                _messageAnimation.Play(SimpleAnimationType.Show);
            }

            foreach (Quest quest in phraseSettings.GetQuests(resolver))
            {
                waitingProcessesCount++;
                _questNotificationPanel.ShowNotification(quest, _finishCallback);
            }
        }

        protected void Awake()
        {
            _animator = GetComponent<Animator>();

            _messageAnimation.AnimationFinished += OnElementAnimationFinished;
        }

        private void OnElementAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            _finishCallback?.Invoke();
            _finishCallback = null;
        }

        private IEnumerator AnimateShowing(Action finishCallback)
        {
            _animator.SetBool(_isShownParameterName, true);

            yield return _animator.WaitState(_shownStateName);

            finishCallback?.Invoke();
        }

        private IEnumerator AnimateHiding(Action finishCallback)
        {
            _animator.SetBool(_isShownParameterName, false);

            yield return _animator.WaitState(_hiddenStateName);

            finishCallback?.Invoke();
        }
    }
}