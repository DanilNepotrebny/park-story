﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.Dialogs
{
    [RequireComponent(typeof(DialogCharacterController), typeof(Animator))]
    public class DialogCharacterNamePlankController : MonoBehaviour, IAnimatorStateEnterHandler
    {
        [Inject] private LocalizationSystem _localizationSystem;

        [SerializeField] private Text _nameText;
        [SerializeField] private SimpleAnimation _plankAnimation;
        [SerializeField] private string _activeStateName;

        private DialogCharacterController _dialogCharacterController;

        protected void Awake()
        {
            _dialogCharacterController = GetComponent<DialogCharacterController>();
            _plankAnimation.AnimationFinished += OnAnimationFinished;
            _plankAnimation.gameObject.SetActive(false);
        }

        protected void OnDisable()
        {
            _plankAnimation.gameObject.SetActive(false);
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animType)
        {
            _plankAnimation.gameObject.SetActive(SimpleAnimationType.Hide != animType);
        }

        void IAnimatorStateEnterHandler.OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            bool isActive = stateInfo.IsName(_activeStateName);

            if (isActive)
            {
                _plankAnimation.gameObject.SetActive(true);
                _nameText.text = _localizationSystem.Localize(_dialogCharacterController.CurrentCharacter.Name, this);
            }

            if (_plankAnimation.gameObject.activeInHierarchy)
            {
                _plankAnimation.Play(
                        isActive ?
                            SimpleAnimationType.Show :
                            SimpleAnimationType.Hide
                    );
            }
        }
    }
}