﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.Localization;
using DreamTeam.Quests;
using DreamTeam.UI.SimpleAnimating;
using Spine.Unity;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.Dialogs
{
    [RequireComponent(typeof(Button))]
    [RequireComponent(typeof(Image))]
    public class DialogController : MonoBehaviour
    {
        [SerializeField] private SimpleAnimation _stripesAnimation;
        [SerializeField] private DialogCharacterController _leftCharacter;
        [SerializeField] private DialogCharacterController _rightCharacter;
        [SerializeField] private DialogPhraseController _phraseController;
        [SerializeField] private GameObject _inputIndicator;

        private Button _dialogButton;
        private Image _dialogButtonRaycast;
        private bool _isDialogStarted;
        private Action _waitForInputCallback;

        private bool _isHiding;

        private Coroutine _coroutine;

        public bool IsActive { get; private set; }

        public void ShowPhrase(PhraseSettings phrase, IExposedPropertyTable resolver, Action phraseShownCallback)
        {
            _coroutine = StartCoroutine(CreatePhraseCoroutine(phrase, resolver, phraseShownCallback));
        }

        public void WaitForInput(Action waitForInputCallback)
        {
            _waitForInputCallback = waitForInputCallback;
            _dialogButton.enabled = true;
            _inputIndicator.SetActive(true);
        }

        public void EndDialog(bool isImmediate, Action finishCallback)
        {
            if (isImmediate)
            {
                SetActive(false);
                finishCallback?.Invoke();
                return;
            }

            if (!_isHiding)
            {
                if (_coroutine != null)
                {
                    StopCoroutine(_coroutine);
                }

                _isHiding = true;

                StartCoroutine(GetHideDialogEnumerator(finishCallback));
            }
        }

        public IEnumerator GetHideDialogEnumerator(Action finishCallback)
        {
            int waitedProcessesCount = 3;

            Action callback = () => --waitedProcessesCount;

            _leftCharacter.Hide(callback);
            _rightCharacter.Hide(callback);
            _phraseController.Hide(callback);

            while (waitedProcessesCount > 0)
            {
                yield return null;
            }

            yield return _stripesAnimation.GetPlayEnumerator(SimpleAnimationType.Hide);

            _isDialogStarted = false;
            _isHiding = false;

            finishCallback?.Invoke();

            SetActive(false);
        }

        protected void Awake()
        {
            _dialogButton = GetComponent<Button>();
            _dialogButtonRaycast = GetComponent<Image>();

            _dialogButton.onClick.AddListener(OnInputReceived);
            _dialogButton.enabled = false;
            _dialogButtonRaycast.enabled = false;

            if (_inputIndicator != null)
            {
                _inputIndicator.SetActive(false);
            }
        }

        private IEnumerator CreatePhraseCoroutine(
                PhraseSettings phrase,
                IExposedPropertyTable resolver,
                Action finishCallback
            )
        {
            // [TimelineLateUpdate] issue fix [900ea014bb90f2c5a8640416d5b6cd7a1be87023]
            yield return null;

            if (!_isDialogStarted)
            {
                SetActive(true);

                _isDialogStarted = true;

                yield return _stripesAnimation.GetPlayEnumerator(SimpleAnimationType.Show);
            }

            yield return PreparePhrase(phrase);
            yield return ShowPhraseMessage(phrase, resolver);

            finishCallback?.Invoke();
        }

        private IEnumerator PreparePhrase(PhraseSettings phrase)
        {
            int waitedProcessesCount = 1;

            Action finishCallback = () => --waitedProcessesCount;

            _phraseController.Prepare(
                    finishCallback
                );

            {
                ++waitedProcessesCount;
                _leftCharacter.ShowCharacter(
                        phrase.LeftCharacter,
                        phrase.MainCharacterSide == CharacterSide.Left,
                        finishCallback
                    );
            }

            {
                ++waitedProcessesCount;
                _rightCharacter.ShowCharacter(
                        phrase.RightCharacter,
                        phrase.MainCharacterSide == CharacterSide.Right,
                        finishCallback
                    );
            }

            while (waitedProcessesCount > 0)
            {
                yield return null;
            }
        }

        private IEnumerator ShowPhraseMessage(PhraseSettings phrase, IExposedPropertyTable resolver)
        {
            bool isShown = false;

            _phraseController.ShowPhrase(
                    phrase,
                    resolver,
                    () => isShown = true
                );

            while (!isShown)
            {
                yield return null;
            }
        }

        private void OnInputReceived()
        {
            _dialogButton.enabled = false;
            _inputIndicator.SetActive(false);

            Action callback = _waitForInputCallback;
            _waitForInputCallback = null;
            callback?.Invoke();
        }

        private void SetActive(bool active)
        {
            IsActive = active;

            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(active);
            }

            _dialogButton.enabled = active;
            _dialogButtonRaycast.enabled = active;
        }

        #region Inner classes

        public enum CharacterSide
        {
            Left,
            Right
        }

        [Serializable]
        public class PhraseSettings
        {
            [SerializeField] private CharacterSettings _leftCharacter;
            [SerializeField] private CharacterSettings _rightCharacter;
            [SerializeField] private CharacterSide _mainCharacterSide;
            [SerializeField] private QuestExposedReference[] _quests;

            [SerializeField, LocalizationKey] private string _message;

            public CharacterSettings LeftCharacter => _leftCharacter;
            public CharacterSettings RightCharacter => _rightCharacter;
            public CharacterSide MainCharacterSide => _mainCharacterSide;
            public string MessageID => _message;

            public Quest[] GetQuests(IExposedPropertyTable resolver)
            {
                Quest[] quests = new Quest[_quests.Length];

                for (int i = 0; i < _quests.Length; ++i)
                {
                    quests[i] = _quests[i].Ref.Resolve(resolver);
                }

                return quests;
            }

            [Serializable]
            private struct QuestExposedReference
            {
                [SerializeField] private ExposedReference<Quest> _ref;

                public ExposedReference<Quest> Ref => _ref;
            }
        }

        [Serializable]
        public class CharacterSettings
        {
            [SerializeField] private CharacterData _character;
            [SerializeField] private Sprite _background;
            [SerializeField, SpineAnimation(dataField = ".~1._character")] private string _appearanceAnimation;
            [SerializeField] private List<AnimationSequenceData> _animationSequence;

            public CharacterData Character => _character;
            public Sprite Background => _background;
            public string AppearanceAnimation => _appearanceAnimation;
            public List<AnimationSequenceData> AnimationSequence => _animationSequence;

            [Serializable]
            public class AnimationSequenceData
            {
                [SerializeField, SpineAnimation(dataField = ".~4._character")] private string _animationName;

                [SerializeField] [Range(0, 10)] private int _replayAmount;

                public string AnimationName => _animationName;

                public int ReplayAmount => _replayAmount;
            }
        }

        #endregion
    }
}