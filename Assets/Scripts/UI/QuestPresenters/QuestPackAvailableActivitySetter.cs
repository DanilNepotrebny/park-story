﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Ads;
using DreamTeam.Popups;
using DreamTeam.Quests;
using DreamTeam.UI.SimpleAnimating;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.QuestPresenters
{
    public class QuestPackAvailableActivitySetter : MonoBehaviour
    {
        [SerializeField] private bool _isInverted = false;
        [SerializeField] private bool _areAdsIgnored = false;

        [SerializeField] private SimpleAnimation _animation;

        [Inject] private QuestManager _questManager;
        [Inject] private AdsSystem _adsSystem;
        [Inject] private Popup _popup;

        private bool _isActive => (_questManager.CurrentPack != null || (!_areAdsIgnored && _adsSystem.IsRewardedVideoAvailable)) ^ _isInverted;

        protected void Awake()
        {
            _popup.Showing += OnPopupShowing;
            _questManager.CompletionUpdated += UpdateView;
            if (_animation != null)
            {
                _animation.AnimationFinished += OnAnimationFinished;
            }
        }

        private void OnPopupShowing(Popup popup)
        {
            gameObject.SetActive(_isActive);
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            if (animationType == SimpleAnimationType.Hide)
            {
                gameObject.SetActive(_isActive);
            }
        }

        protected void OnDestroy()
        {
            _popup.Showing -= OnPopupShowing;
            _questManager.CompletionUpdated -= UpdateView;
            if (_animation != null)
            {
                _animation.AnimationFinished -= OnAnimationFinished;
            }
        }

        private void UpdateView()
        {
            SimpleAnimationType animType = _isActive ? SimpleAnimationType.Show : SimpleAnimationType.Hide;

            gameObject.SetActive(true);
            if (!TryPlayAnimation(animType))
            {
                OnAnimationFinished(_animation, animType);
            }
        }

        private bool TryPlayAnimation(SimpleAnimationType animType)
        {
            return _animation != null && gameObject.activeInHierarchy && _animation.Play(animType);
        }
    }
}