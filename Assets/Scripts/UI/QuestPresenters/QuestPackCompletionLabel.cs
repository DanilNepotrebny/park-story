﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.QuestPresenters
{
    [RequireComponent(typeof(Text))]
    public class QuestPackCompletionLabel : QuestPackCompletionPresenter
    {
        [SerializeField, Tooltip("{0} - current completion percents")]
        private string _textFormat = "{0}%";

        protected override void InitializeView(float completionRatio)
        {
            UpdateView(completionRatio);
        }

        protected override void UpdateView(float ratio)
        {
            GetComponent<Text>().text = string.Format(_textFormat, Mathf.RoundToInt(ratio * 100));
        }
    }
}