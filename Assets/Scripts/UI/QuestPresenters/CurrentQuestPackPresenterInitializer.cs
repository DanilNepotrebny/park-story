﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.QuestPresenters
{
    [RequireComponent(typeof(PresenterHub))]
    public class CurrentQuestPackPresenterInitializer : MonoBehaviour
    {
        [Inject] private QuestManager _questManager;

        private QuestPack _currentQuestPack;

        protected void Start()
        {
            UnsubscribeCurrentPack();

            if (_questManager.CurrentPack != null)
            {
                _currentQuestPack = _questManager.CurrentPack;
                GetComponent<PresenterHub>().Initialize(_currentQuestPack);
                _currentQuestPack.Completed += Start;
            }
        }

        protected void OnDestroy()
        {
            UnsubscribeCurrentPack();
        }

        private void UnsubscribeCurrentPack()
        {
            if (_currentQuestPack != null)
            {
                _currentQuestPack.Completed -= OnDestroy;
            }
        }
    }
}