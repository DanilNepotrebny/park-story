﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Ads;
using DreamTeam.Popups;
using DreamTeam.Popups.Destabilization;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.QuestPresenters
{
    [RequireComponent(typeof(SimpleAnimation))]
    public class QuestAdsPresenter : MonoBehaviour, IPopupDestabilizer
    {
        [Inject] private PopupSystem _popupSystem;
        [Inject] private Popup _popup;
        [Inject] private AdsSystem _adsSystem;

        [SerializeField] private Button _showAdsButton;

        private DeferredInvocation _hidingInvocation;
        private ISimpleAnimationPlayer _animPlayer;

        public event Action Stabilized;
        public event Action Destabilized;

        public bool IsStable => !_animPlayer.IsPlaying;

        protected void Awake()
        {
            _animPlayer = GetComponent<SimpleAnimation>();
            _animPlayer.AnimationFinished += OnAnimationFinished;
        }

        protected void OnDestroy()
        {
            _animPlayer.AnimationFinished -= OnAnimationFinished;
        }

        protected void OnEnable()
        {
            _popup.AddDestabilizer(this);

            Destabilized?.Invoke();
            _animPlayer.Play(SimpleAnimationType.Show);
            _showAdsButton.onClick.AddListener(OnShowAdsButtonClicked);
        }

        protected void OnDisable()
        {
            _popup.RemoveDestabilizer(this);
            _showAdsButton.onClick.RemoveListener(OnShowAdsButtonClicked);
        }

        private void ShowAds()
        {
            _adsSystem.ShowRewardedVideo();
        }

        private void OnShowAdsButtonClicked()
        {
            if (_adsSystem.IsConsentPopupShown)
            {
                Destabilized?.Invoke();
                _animPlayer.Play(SimpleAnimationType.Hide);
            }
            else
            {
                _adsSystem.ShowConsentPopup();
            }
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            Stabilized?.Invoke();
            if (SimpleAnimationType.Hide == animationType)
            {
                _popupSystem.Hide(_popup.SceneName, ShowAds);
                gameObject.Dispose();
            }
        }
    }
}