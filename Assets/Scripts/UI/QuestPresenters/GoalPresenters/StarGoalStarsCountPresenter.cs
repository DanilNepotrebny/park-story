﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.QuestPresenters.GoalPresenters
{
    [RequireComponent(typeof(Text))]
    public class StarGoalStarsCountPresenter : MonoBehaviour, IPresenter<StarGoal>
    {
        public void Initialize(StarGoal model)
        {
            GetComponent<Text>().text = model.StarsCountToStart.ToString();
        }
    }
}
