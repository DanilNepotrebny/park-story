﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.QuestPresenters.GoalPresenters
{
    public class GoalIsStartedInteractivityPresenter : MonoBehaviour, IPresenter<Goal>
    {
        [SerializeField, RequiredField] private Selectable[] _selectables;

        private Goal _goal;

        public void Initialize(Goal model)
        {
            UnsubscribeGoal();
            _goal = model;
            SubscribeGoal();
            UpdateView();
        }

        protected void OnEnable()
        {
            SubscribeGoal();
            UpdateView();
        }

        protected void OnDisable()
        {
            UnsubscribeGoal();
        }

        private void SubscribeGoal()
        {
            if (_goal != null)
            {
                _goal.CommandsExecutionStarting += OnGoalCommandsExecutionStarting;
            }
        }

        private void UnsubscribeGoal()
        {
            if (_goal != null)
            {
                _goal.CommandsExecutionStarting -= OnGoalCommandsExecutionStarting;
            }
        }

        private void OnGoalCommandsExecutionStarting(IDeferredInvocationHandle handle)
        {
            UpdateView();
        }

        private void UpdateView()
        {
            if (_goal != null)
            {
                foreach (Selectable selectable in _selectables)
                {
                    selectable.interactable = !_goal.IsStarted;
                }
            }
        }
    }
}
