﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;

namespace DreamTeam.UI.QuestPresenters.GoalPresenters
{
    public class FreeGoalStartButtonPresenter : GoalStartButtonPresenter<FreeGoal>
    {
    }
}