﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.QuestPresenters.GoalPresenters
{
    [RequireComponent(typeof(Button))]
    public abstract class GoalStartButtonPresenter<TModel> : MonoBehaviour, IPresenter<TModel> where TModel : Goal
    {
        private Button _button;

        protected TModel Goal { get; private set; }

        public void Initialize(TModel goal)
        {
            UnsubscribeGoal();
            Goal = goal;
            SubscribeGoal();
        }

        protected virtual void UnsubscribeGoal()
        {
            if (Goal != null)
            {
                Goal.CommandsExecutionStarting -= OnGoalCommandsExecutionStarting;
            }
        }

        protected virtual void SubscribeGoal()
        {
            if (Goal != null)
            {
                Goal.CommandsExecutionStarting += OnGoalCommandsExecutionStarting;
            }
        }

        protected void OnEnable()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnButtonClicked);
            SubscribeGoal();
        }

        protected void OnDisable()
        {
            _button.onClick.RemoveListener(OnButtonClicked);
            UnsubscribeGoal();
        }

        protected virtual void OnGoalCommandsExecutionStarting(IDeferredInvocationHandle commandsExecutionHandle)
        {
        }

        private void OnButtonClicked()
        {
            Goal.TryStart();
        }
    }
}