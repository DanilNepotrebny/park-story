﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Audio;
using DreamTeam.Popups;
using DreamTeam.Quests;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.UI.QuestPresenters.GoalPresenters
{
    [RequireComponent(typeof(SimpleAnimation))]
    public class GoalShowHidePresenter : MonoBehaviour, IPresenter<Goal>
    {
        [SerializeField, RequiredField] private AudioSystemPlayer _completeSound;
        [Inject] private Popup _popup;

        private Goal _goal;

        private SimpleAnimation _animation => GetComponent<SimpleAnimation>();

        public void Initialize(Goal model)
        {
            Assert.IsNotNull(model);

            UnsubscribeGoal();
            _goal = model;
            SubscribeGoal();
        }

        protected void Awake()
        {
            _popup.Shown += OnPopupShown;
            _animation.AnimationFinished += OnAnimationFinished;
        }

        protected void OnDestroy()
        {
            _popup.Shown -= OnPopupShown;
            _animation.AnimationFinished -= OnAnimationFinished;
        }

        protected void OnEnable()
        {
            SubscribeGoal();
            if (_popup.IsShown)
            {
                _animation.Play(SimpleAnimationType.Show);
            }
        }

        protected void OnDisable()
        {
            UnsubscribeGoal();
        }

        private void OnPopupShown(Popup obj)
        {
            TryCompleteGoal();
        }

        private void SubscribeGoal()
        {
            if (_goal != null)
            {
                _goal.WaitingForCompletion += TryCompleteGoal;
            }
        }

        private void UnsubscribeGoal()
        {
            if (_goal != null)
            {
                _goal.WaitingForCompletion -= TryCompleteGoal;
            }
        }

        private void TryCompleteGoal()
        {
            if (_goal != null && _goal.IsWaitingForCompletion)
            {
                _completeSound.Play();
                if (!_animation.Play(SimpleAnimationType.Hide))
                {
                    OnAnimationFinished(_animation, SimpleAnimationType.Hide);
                }
            }
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            if (SimpleAnimationType.Hide == animationType)
            {
                _goal.SetCompleted();
                gameObject.Dispose();
            }
        }
    }
}