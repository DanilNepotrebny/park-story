﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.QuestPresenters.GoalPresenters
{
    [RequireComponent(typeof(Text))]
    public class FreeGoalStartTextPresenter : MonoBehaviour, IPresenter<FreeGoal>
    {
        public void Initialize(FreeGoal model)
        {
            GetComponent<Text>().text = model.StartText;
        }
    }
}
