﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using DreamTeam.Quests;
using DreamTeam.TransformLookup;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.QuestPresenters.GoalPresenters
{
    [RequireComponent(typeof(GroupMotionAnimator))]
    public class StarGoalStartButtonPresenter : GoalStartButtonPresenter<StarGoal>
    {
        [SerializeField] private TransformLookupId _starsTransformId;

        [Inject] private TransformLookupSystem _transformLookupSystem;
        [Inject] private PopupSystem _popupSystem;

        private IDeferredInvocationHandle _goalCommandsExecutionHandle;

        protected override void OnGoalCommandsExecutionStarting(IDeferredInvocationHandle commandsExecutionHandle)
        {
            _goalCommandsExecutionHandle = commandsExecutionHandle.Lock();

            Transform startMotionPoint = _transformLookupSystem.FindTransform(_starsTransformId);
            GroupMotionAnimator motion = GetComponent<GroupMotionAnimator>();

            motion.StartPoint = startMotionPoint;
            motion.SetElementsParent(startMotionPoint);
            motion.SetElementsAmount(Goal.StarsCountToStart);
            motion.MotionFinished += OnMotionFinished;
            motion.Play();
        }

        private void OnMotionFinished()
        {
            _popupSystem.Hide(gameObject.scene.name, () => _goalCommandsExecutionHandle.Unlock());
        }
    }
}