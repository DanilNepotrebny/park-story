﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using DreamTeam.Quests;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.UI.QuestPresenters
{
    [RequireComponent(typeof(SimpleAnimation))]
    public class QuestShowHidePresenter : MonoBehaviour, IPresenter<Quest>
    {
        [Inject] private Popup _popup;

        private Quest _quest;
        private DeferredInvocation _hidingInvocation;
        private ISimpleAnimationPlayer _animPlayer;

        public IDeferredInvocationHandle HidingInvocation
        {
            get
            {
                if (_hidingInvocation == null)
                {
                    _hidingInvocation = new DeferredInvocation(() => _animPlayer.Play(SimpleAnimationType.Hide));
                }

                if (!_hidingInvocation.IsLocked)
                {
                    _hidingInvocation.Start();
                }

                return _hidingInvocation;
            }
        }

        public void Initialize(Quest model)
        {
            Assert.IsNotNull(model);

            UnsubscribeQuest();
            _quest = model;
            SubscribeQuest();
        }

        protected void Awake()
        {
            _animPlayer = GetComponent<SimpleAnimation>();
            _popup.Shown += OnPopupShown;
            _animPlayer.AnimationFinished += OnAnimationFinished;
        }

        protected void OnDestroy()
        {
            _popup.Shown -= OnPopupShown;
            _animPlayer.AnimationFinished -= OnAnimationFinished;
        }

        protected void OnEnable()
        {
            SubscribeQuest();
            if (_popup.IsShown)
            {
                _animPlayer.Play(SimpleAnimationType.Show);
            }
        }

        protected void OnDisable()
        {
            UnsubscribeQuest();
        }

        private void OnPopupShown(Popup popup)
        {
            TryCompleteQuest();
        }

        private void SubscribeQuest()
        {
            if (_quest != null)
            {
                _quest.WaitingForCompletion += TryCompleteQuest;
            }
        }

        private void UnsubscribeQuest()
        {
            if (_quest != null)
            {
                _quest.WaitingForCompletion -= TryCompleteQuest;
            }
        }

        private void TryCompleteQuest()
        {
            if (_quest != null && _quest.IsWaitingForCompletion)
            {
                HidingInvocation.Unlock();
            }
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            if (animationType == SimpleAnimationType.Hide)
            {
                _quest.SetCompleted();
                gameObject.Dispose();
            }
        }
    }
}