﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.UI.QuestPresenters
{
    public class QuestPackRewardPresentsPanel : MonoBehaviour, IPresenter<QuestPack>
    {
        [SerializeField, RequiredField] private QuestPackRewardPresent _presentPrefab;
        [SerializeField, RequiredField] private QuestPackCompletionProgressBar _progressBar;

        [Inject] private Instantiator _instantiator;

        private QuestPack _questPack;

        public void Initialize(QuestPack model)
        {
            Assert.IsNotNull(model);

            if (_questPack != model)
            {
                _questPack = model;
                ClearPresents();
                CreatePresents();
            }
        }

        private void CreatePresents()
        {
            foreach (QuestPackReward reward in _questPack.Rewards)
            {
                if (!reward.IsTaken)
                {
                    QuestPackRewardPresent present = _instantiator.Instantiate(_presentPrefab, _progressBar.gameObject);
                    present.Initialize(_progressBar, reward.CompletionRatio);
                }
            }
        }

        private void ClearPresents()
        {
            QuestPackRewardPresent[] presents = _progressBar.GetComponentsInChildren<QuestPackRewardPresent>();
            foreach (QuestPackRewardPresent present in presents)
            {
                present.gameObject.Dispose();
            }
        }
    }
}