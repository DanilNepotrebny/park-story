﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;
using DreamTeam.Utils.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.QuestPresenters
{
    public class QuestProgressPresenter : MonoBehaviour, IPresenter<Quest>
    {
        [SerializeField] private Slider _slider;
        [SerializeField] private Text _text;

        [SerializeField, Tooltip("{0} - completed goals count; {1} - total goals count")]
        private string _textFormat = "{0}/{1}";

        [SerializeField] private Easing _easing;
        [SerializeField] private float _easingDuration = 0.5f;

        [SerializeField] private QuestShowHidePresenter _questShowHidePresenter;
        [SerializeField] private QuestGoalsPanel _goalsPanel;

        private Quest _quest;
        private LTDescr _tween;

        public void Initialize(Quest model)
        {
            UnsubscribeQuest();
            _quest = model;
            SubscribeQuest();

            UpdateVisibility();
            SetCounterValue();
            SetSliderValue(_quest.CompletionRatio);
        }

        protected void OnEnable()
        {
            SubscribeQuest();
        }

        protected void OnDisable()
        {
            UnsubscribeQuest();
            StopProgressTweening();
        }

        private void SubscribeQuest()
        {
            if (_quest != null)
            {
                _quest.CompletionUpdated += UpdateProgress;
            }
        }

        private void UnsubscribeQuest()
        {
            if (_quest != null)
            {
                _quest.CompletionUpdated -= UpdateProgress;
            }
        }

        private void UpdateProgress()
        {
            StartProgressTweening(_quest.CompletionRatio);
            SetCounterValue();
        }

        private void UpdateVisibility()
        {
            GameObject parent = _slider.transform.parent.gameObject;
            parent.SetActive(_quest.Goals.Count > 1);
        }

        private void SetCounterValue()
        {
            if (_text != null)
            {
                _text.text = string.Format(_textFormat, _quest.CompletedCount, _quest.Goals.Count);
            }
        }

        private void SetSliderValue(float value)
        {
            if (_slider != null)
            {
                _slider.normalizedValue = value;
            }
        }

        private void StartProgressTweening(float to)
        {
            if (!_slider.gameObject.activeInHierarchy)
            {
                return;
            }

            StopProgressTweening();

            _goalsPanel?.CreateGoalPresenterInvokation.Lock();
            _questShowHidePresenter?.HidingInvocation.Lock();
            _tween = LeanTween.value(_slider.value, to, _easingDuration);
            _easing.SetupDescriptor(_tween);
            _tween.setOnUpdate(value => SetSliderValue(value));
            _tween.setOnComplete(
                () =>
                {
                    _goalsPanel?.CreateGoalPresenterInvokation.Unlock();
                    _questShowHidePresenter?.HidingInvocation.Unlock();
                    _tween = null;
                });
        }

        private void StopProgressTweening()
        {
            if (_tween != null)
            {
                LeanTween.cancel(_tween.uniqueId);
                _questShowHidePresenter?.HidingInvocation.Unlock();
                _tween = null;
            }
        }
    }
}