﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Popups;
using DreamTeam.Popups.Destabilization;
using DreamTeam.Utils.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.QuestPresenters
{
    [RequireComponent(typeof(Slider))]
    public class QuestPackCompletionProgressBar : QuestPackCompletionPresenter, IPopupDestabilizer
    {
        [SerializeField] private Easing _easing;
        [SerializeField] private float _easingDuration = 1;

        [Inject] private Popup _popup;

        private Slider _slider;
        private LTDescr _tween;
        private float _previousRatio;

        public float NormalizedValue => _slider.normalizedValue;

        public event Action<float> ValueUpdated;
        public event Action<float> ValueUpdateFinished;

        protected void OnEnable()
        {
            _slider = GetComponent<Slider>();

            _popup.AddDestabilizer(this);
        }

        protected void OnDisable()
        {
            StopTween();

            _popup.RemoveDestabilizer(this);
        }

        protected override void InitializeView(float completionRatio)
        {
            _slider.value = _previousRatio = completionRatio;
        }

        protected override void UpdateView(float ratio)
        {
            if (Math.Abs(ratio - _previousRatio) < Mathf.Epsilon)
            {
                return;
            }

            IsStable = false;
            Destabilized?.Invoke();

            _previousRatio = ratio;
            StopTween();
            StartTween(ratio);
        }

        private void StartTween(float ratio)
        {
            _tween = LeanTween.value(_slider.value, ratio, _easingDuration);
            _easing.SetupDescriptor(_tween);
            _tween.setOnUpdate(
                    value =>
                    {
                        _slider.value = value;
                        ValueUpdated?.Invoke(NormalizedValue);
                    }
                );
            _tween.setOnComplete(
                    () =>
                    {
                        _tween = null;
                        ValueUpdateFinished?.Invoke(NormalizedValue);
                        IsStable = true;
                        Stabilized?.Invoke();
                    }
                );
        }

        private void StopTween()
        {
            if (_tween != null)
            {
                LeanTween.cancel(_tween.uniqueId);
                _tween = null;
            }
        }

        public event Action Stabilized;
        public event Action Destabilized;
        public bool IsStable { get; private set; } = true;
    }
}