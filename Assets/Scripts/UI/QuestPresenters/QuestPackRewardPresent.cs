﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Popups;
using DreamTeam.Quests;
using DreamTeam.UI.RewardPresentationSequences;
using DreamTeam.Utils;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using Spine;

namespace DreamTeam.UI.QuestPresenters
{
    [RequireComponent(typeof(SkeletonGraphic))]
    [RequireComponent(typeof(QuestPackRewardPresentationSequence))]
    public class QuestPackRewardPresent : MonoBehaviour
    {
        [SerializeField, SpineAnimation] private string _updatedAnimation;
        [SerializeField, SpineAnimation] private string _appearenceAnimation;

        [Inject] private Popup _popup;

        private const float _epsilon = 0.01f;
        private QuestPackCompletionProgressBar _progressBar;
        private SkeletonGraphic _skeletonGraphic;
        private QuestPackRewardPresentationSequence _presentationSequence;
        private QuestsAndFriendsPopup _questsAndFriendsPopup => _popup as QuestsAndFriendsPopup;

        private float _completionRatio = 1;
        private RectTransform _thisRect => transform as RectTransform;
        private QuestPackReward _currentRewardData => _questsAndFriendsPopup?.RewardData;
        private float _currentCompetionRation => _currentRewardData?.CompletionRatio ?? 0;
        private bool _isCurrent => Mathf.Abs(_currentCompetionRation - _completionRatio) <= _epsilon;

        public void Initialize(QuestPackCompletionProgressBar progressBar, float completionRatio)
        {
            Assert.IsNotNull(progressBar);

            _completionRatio = completionRatio;

            UnsubscribeProgressBar();
            _progressBar = progressBar;
            SubscribeProgressBar();

            SetPosition();
            OnProgressBarValueUpdated(_completionRatio);
        }

        protected void Awake()
        {
            _skeletonGraphic = GetComponent<SkeletonGraphic>();
            _presentationSequence = GetComponent<QuestPackRewardPresentationSequence>();
        }

        protected IEnumerator Start()
        {
            if (_popup.IsShown)
            {
                TrackEntry track = _skeletonGraphic.AnimationState.SetAnimation(0, _appearenceAnimation, false);
                yield return new WaitUntil(() => track.IsComplete);
                _skeletonGraphic.AnimationState.SetAnimation(0, _skeletonGraphic.startingAnimation, false);
            }
        }

        private void SetPosition()
        {
            _thisRect.StretchToParrent();
            _thisRect.pivot = new Vector2(_completionRatio, 0.5f);
        }

        private void SubscribeProgressBar()
        {
            _progressBar.ValueUpdated += OnProgressBarValueUpdated;
            _progressBar.ValueUpdateFinished += OnProgressBarValueUpdateFinished;
        }

        private void UnsubscribeProgressBar()
        {
            if (_progressBar != null)
            {
                _progressBar.ValueUpdated -= OnProgressBarValueUpdated;
                _progressBar.ValueUpdateFinished -= OnProgressBarValueUpdateFinished;
            }
        }

        private void OnProgressBarValueUpdated(float ratio)
        {
            if (_isCurrent && ratio >= _completionRatio)
            {
                _presentationSequence.Present(
                    _questsAndFriendsPopup.RewardData.Rewards,
                    _questsAndFriendsPopup.GivingRewardInvocation);
                UnsubscribeProgressBar();
            }
        }

        private void OnProgressBarValueUpdateFinished(float ratio)
        {
            if (_isCurrent && ratio < _completionRatio)
            {
                _skeletonGraphic.AnimationState.SetAnimation(0, _updatedAnimation, false);
            }
        }
    }
}