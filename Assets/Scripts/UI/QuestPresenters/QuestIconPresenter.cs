﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace DreamTeam.UI.QuestPresenters
{
    [RequireComponent(typeof(Image))]
    public class QuestIconPresenter : MonoBehaviour, IPresenter<Quest>
    {
        public void Initialize(Quest model)
        {
            Assert.IsNotNull(model);
            GetComponent<Image>().sprite = model.Icon;
        }
    }
}