﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.UI.QuestPresenters
{
    public class QuestPackWaitingForCompletionActivityPresenter : MonoBehaviour, IPresenter<QuestPack>
    {
        [SerializeField, RequiredField] private Component _simpleAnimationComponent;

        private QuestPack _questPack;
        private SimpleAnimationType? _previousAnimation = null;

        private ISimpleAnimationPlayer _simpleAnimation => _simpleAnimationComponent as ISimpleAnimationPlayer;

        public void Initialize(QuestPack model)
        {
            UnsubscribeCurrentPack();
            _questPack = model;
            SubscribeCurrentPack();
            UpdateView();
        }

        protected void OnEnable()
        {
            SubscribeCurrentPack();
            UpdateView();
        }

        protected void OnDisable()
        {
            UnsubscribeCurrentPack();
        }

        protected void Reset()
        {
            _simpleAnimationComponent = GetComponent<ISimpleAnimationPlayer>() as Component;
        }

        protected void OnValidate()
        {
            if (_simpleAnimationComponent != null)
            {
                _simpleAnimationComponent = _simpleAnimation as Component;
                Assert.IsNotNull(
                        _simpleAnimationComponent,
                        $"{nameof(_simpleAnimationComponent)} must implement {nameof(ISimpleAnimationPlayer)}"
                    );
            }
        }

        private void UnsubscribeCurrentPack()
        {
            if (_questPack != null)
            {
                _questPack.CompletionUpdated -= OnQuestPackCompletionUpdated;
            }
        }

        private void SubscribeCurrentPack()
        {
            if (_questPack != null)
            {
                _questPack.CompletionUpdated += OnQuestPackCompletionUpdated;
            }
        }

        private void OnQuestPackCompletionUpdated(float completionRatio)
        {
            UpdateView();
        }

        private void UpdateView()
        {
            if (_questPack == null)
            {
                return;
            }

            SimpleAnimationType currentAnimation =
                _questPack.HasWaitingForCompletionGoals ?
                    SimpleAnimationType.Show :
                    SimpleAnimationType.Hide;

            if (_previousAnimation != currentAnimation)
            {
                _simpleAnimation.Play(currentAnimation);
                _previousAnimation = currentAnimation;
            }
        }

        private void SetChildrenActive(bool isActive)
        {
            for (int index = 0; index < transform.childCount; index++)
            {
                transform.GetChild(index).gameObject.SetActive(isActive);
            }
        }
    }
}
