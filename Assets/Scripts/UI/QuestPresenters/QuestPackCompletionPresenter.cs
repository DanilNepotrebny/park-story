﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.UI.QuestPresenters
{
    public abstract class QuestPackCompletionPresenter : MonoBehaviour, IPresenter<QuestPack>
    {
        protected QuestPack QuestPack { get; private set; }

        private float _displayRatio =>
            (QuestPack != null && !QuestPack.IsWaitingForCompletion) ? QuestPack.CompletionRatio : 0;

        public void Initialize(QuestPack model)
        {
            Assert.IsNotNull(model);

            OnDestroy();
            QuestPack = model;
            QuestPack.CompletionUpdated += OnCompletionUpdated;
            InitializeView(_displayRatio);
        }

        protected abstract void InitializeView(float completionRatio);
        protected abstract void UpdateView(float completionRatio);

        protected void Start()
        {
            InitializeView(_displayRatio);
        }

        protected void OnDestroy()
        {
            if (QuestPack != null)
            {
                QuestPack.CompletionUpdated -= OnCompletionUpdated;
            }
        }

        private void OnCompletionUpdated(float completionRatio)
        {
            UpdateView(_displayRatio);
        }
    }
}