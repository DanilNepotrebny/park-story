﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using DreamTeam.Quests;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.QuestPresenters
{
    [RequireComponent(typeof(Text))]
    public class QuestTitlePresenter : MonoBehaviour, IPresenter<Quest>
    {
        [Inject] private LocalizationSystem _localizationSystem;

        public void Initialize(Quest model)
        {
            Assert.IsNotNull(model);
            GetComponent<Text>().text = _localizationSystem.Localize(model.Title, model);
        }
    }
}