﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.UI
{
    /// <summary>
    /// Class for initializing any child <see cref="MonoBehaviour"/> that implements <see cref="IPresenter"> of specified type in the hierarchy.
    /// </summary>
    public class PresenterHub : MonoBehaviour
    {
        public virtual void Initialize(object model)
        {
            Assert.IsNotNull(model, $"{GetType().Name} on {name}. Model is null while initialization.");

            Type modelType = model.GetType();

            foreach (Type parentType in modelType.EnumerateTypeHierarchy())
            {
                InitializeAs(parentType, model);
            }
        }

        private void InitializeAs(Type modelType, object model)
        {
            Type presenterType = typeof(IPresenter<>).MakeGenericType(modelType);

            List<object> presenters = transform.GetComponentsInChildrenWhile(
                    presenterType,
                    node => node == transform || node.GetComponent<PresenterHub>() == null,
                    true
                );

            foreach (object presenter in presenters)
            {
                ReflectionUtils.Invoke(presenter, "Initialize", false, model);
            }
        }
    }
}
