﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.UI
{
    /// <summary>
    /// Base class to implement conversion from source model to target one if they have agregation relations.
    /// </summary>
    /// <typeparam name="TSourceModel">Source model type to convert from</typeparam>
    /// <typeparam name="TTargetModel">Target model type to convert to </typeparam>
    [RequireComponent(typeof(PresenterHub))]
    public abstract class PresenterProxy<TSourceModel, TTargetModel> : MonoBehaviour, IPresenter<TSourceModel>
        where TSourceModel : class
        where TTargetModel : class
    {
        protected TSourceModel SourceModel { get; private set; }

        protected abstract TTargetModel TargetModel { get; }

        public void Initialize(TSourceModel model)
        {
            PresenterHub presenterHub = GetComponent<PresenterHub>();
            Assert.IsNotNull(model, $"{GetType().Name} on {name}. {nameof(SourceModel)} is null while initialization.");
            SourceModel = model;

            if (TargetModel != null)
            {
                presenterHub.Initialize(TargetModel);
            }
        }
    }
}