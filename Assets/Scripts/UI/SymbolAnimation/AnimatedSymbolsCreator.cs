﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.UI.SymbolAnimation
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(TMP_Text))]
    public class AnimatedSymbolsCreator : MonoBehaviour
    {
        [SerializeField] private AnimatedSymbol _symbolPrefab;

        [Space]
        [SerializeField] private bool _showAutomatically;
        [SerializeField] [Range(0f, 100f)] private float _showTime;
        [SerializeField] [Range(0f, 100f)] private float _hideTime;

        [Space]
        [SerializeField] private bool _skipWhitespaces;
        [SerializeField] [Range(0f, 1f)] private float _charactersDistanceScale;

        public enum AnimationState
        {
            Hidden,
            InProgress,
            Idle
        }

        private TMP_Text _textMeshPro = null;
        private List<AnimatedSymbol> _animatedSymbols = new List<AnimatedSymbol>();
        private AnimationState _state = AnimationState.Hidden;
        private string _text = string.Empty;
        public AnimationState State => _state;

        public Action StateChanged;

        public IEnumerator Show()
        {
            yield return Show(_showTime);
        }

        public IEnumerator Show(float duration)
        {
            Assert.IsTrue(_textMeshPro?.text.Length > 0, "[DESIGNERS] Cannot animate empty text!");
            yield return Show(duration, _textMeshPro?.text);
        }

        public IEnumerator Show(string text)
        {
            Assert.IsTrue(text.Length > 0, "[DESIGNERS] Cannot animate empty text!");
            yield return Show(_showTime, text);
        }

        public IEnumerator Show(float duration, string text)
        {
            if (_state == AnimationState.Hidden && text.Length > 0)
            {
                _text = text;
                SetState(AnimationState.InProgress);
                yield return CreateSymbols();
                yield return ShowSymbols(duration);
                SetState(AnimationState.Idle);
            }
        }

        public IEnumerator Hide()
        {
            yield return Hide(_hideTime);
        }

        public IEnumerator Hide(float time)
        {
            Assert.IsTrue(_text.Length > 0, "[DESIGNERS] Cannot animate empty text!");
            if (_state == AnimationState.Idle && _text.Length > 0)
            {
                SetState(AnimationState.InProgress);
                yield return HideSymbols(time);
                DestroySymbols();
                SetState(AnimationState.Hidden);
            }
        }

        public void ResetState()
        {
            DestroySymbols();
            SetState(AnimationState.Hidden);
        }

        protected void Awake()
        {
            _textMeshPro = GetComponent<TMP_Text>();
        }

        protected void OnEnable()
        {
            if (_showAutomatically && _state == AnimationState.Hidden)
            {
                StartCoroutine(Show());
            }
        }

        protected void OnDisable()
        {
            DestroySymbols();
        }

        private IEnumerator CreateSymbols()
        {
            _textMeshPro.text = _text;
            _textMeshPro.ForceMeshUpdate();

            yield return null;

            TMP_TextInfo textInfo = _textMeshPro.textInfo;
            TMP_MeshInfo[] cachedMeshInfo = textInfo.CopyMeshInfoVertexData();

            float advancedWidth = 0;
            List<float> absoluteValues = new List<float>();
            List<float> relativeValues = new List<float>();

            for (int i = 0; i < textInfo.characterCount; i++)
            {
                int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;
                int vertexIndex = textInfo.characterInfo[i].vertexIndex;
                Vector3[] sourceVertices = cachedMeshInfo[materialIndex].vertices;
                Vector2 symbolCenter = (sourceVertices[vertexIndex + 0] + sourceVertices[vertexIndex + 2]) / 2;

                if (i > 0)
                {
                    advancedWidth += textInfo.characterInfo[i].xAdvance - textInfo.characterInfo[i - 1].xAdvance;
                }

                if (!_skipWhitespaces || !char.IsWhiteSpace(_text[i]))
                {
                    AnimatedSymbol symbol = ConstructSymbol(i);
                    _animatedSymbols.Add(symbol);

                    absoluteValues.Add(symbolCenter.x);
                    relativeValues.Add(advancedWidth);
                }
            }

            for (int i = 0; i < _animatedSymbols.Count; i++)
            {
                _animatedSymbols[i].Initialize(absoluteValues[i], relativeValues[i] / advancedWidth);
            }
        }

        private AnimatedSymbol ConstructSymbol(int index)
        {
            AnimatedSymbol symbolComponent = Instantiate(_symbolPrefab);
            symbolComponent.gameObject.name = _text[index].ToString();
            symbolComponent.gameObject.transform.SetParent(gameObject.transform);
            symbolComponent.gameObject.transform.SetSiblingIndex(index);
            symbolComponent.gameObject.hideFlags = HideFlags.DontSave;
            symbolComponent.Costruct(_text[index]);
            return symbolComponent;
        }

        private IEnumerator ShowSymbols(float duration)
        {
            AnimatedSymbol symbolInstance = _animatedSymbols.First();
            Assert.IsTrue(duration >= symbolInstance.ShowDuration, "Whole text show duration should be bigger than single symbol show duration");

            if (duration >= symbolInstance.ShowDuration)
            {
                float interval =
                    _animatedSymbols.Count > 1 ?
                    (duration - symbolInstance.ShowDuration) / (_animatedSymbols.Count - 1) :
                    0f;

                foreach (AnimatedSymbol symbol in _animatedSymbols)
                {
                    ShowSymbol(symbol);
                    if (duration > 0f)
                    {
                        yield return new WaitForSeconds(interval);
                    }
                }

                yield return new WaitForSeconds(symbolInstance.ShowDuration - interval);
            }
        }

        private void ShowSymbol(AnimatedSymbol symbol)
        {
            symbol.transform.localPosition = new Vector3(symbol.AbsoluteValue * _charactersDistanceScale, 0f, 0f);
            symbol.transform.localRotation = Quaternion.identity;
            symbol.transform.localScale = Vector3.one;

            symbol.Show();
        }

        private IEnumerator HideSymbols(float duration)
        {
            AnimatedSymbol symbolInstance = _animatedSymbols.First();
            Assert.IsTrue(duration >= symbolInstance.HideDuration, "Whole text hide duration should be bigger than single symbol hide duration");

            if (duration >= symbolInstance.HideDuration)
            {
                float interval =
                    _animatedSymbols.Count > 1 ?
                        (duration - symbolInstance.HideDuration) / (_animatedSymbols.Count - 1) :
                        0f;

                foreach (AnimatedSymbol symbol in _animatedSymbols)
                {
                    HideSymbol(symbol);
                    if (duration > 0f)
                    {
                        yield return new WaitForSeconds(interval);
                    }
                }

                yield return new WaitForSeconds(symbolInstance.HideDuration - interval);
            }
        }

        private void HideSymbol(AnimatedSymbol symbol)
        {
            symbol.Hide();
        }

        private void DestroySymbols()
        {
            if (_animatedSymbols.Count > 0)
            {
                foreach (var animatedSymbol in _animatedSymbols)
                {
                    animatedSymbol.gameObject.Dispose();   
                }

                _animatedSymbols.Clear();
            }
        }

        private void SetState(AnimationState value)
        {
            if (_state != value)
            {
                _state = value;
                StateChanged?.Invoke();
            }
        }
    }
}