﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.UI.SymbolAnimation
{
    [RequireComponent(typeof(AnimatedSymbol))]
    public abstract class AnimatedSymbolController : MonoBehaviour
    {
        public abstract float ShowDuration { get; }
        public abstract float HideDuration { get; }

        protected AnimatedSymbol Symbol { get; private set; }

        public abstract void Show();
        public abstract void Hide();

        protected virtual void Awake()
        {
            Symbol = GetComponent<AnimatedSymbol>();
        }
    }
}