﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.UI.SymbolAnimation
{
    public class AnimatedSymbolAnimationController : AnimatedSymbolController
    {
        [SerializeField] private Animation _animation;
        [SerializeField] private AnimationClip _showAnimation;
        [SerializeField] private AnimationClip _idleAnimation;
        [SerializeField] private AnimationClip _hideAnimation;

        public override float ShowDuration => _showAnimation != null ? _showAnimation.length : 0f;
        public override float HideDuration => _hideAnimation != null ? _hideAnimation.length : 0f;

        public override void Show()
        {
            if (_animation != null &&
                _showAnimation != null &&
                _idleAnimation != null)
            {
                _animation.Play(_showAnimation.name);
                _animation.PlayQueued(_idleAnimation.name, QueueMode.CompleteOthers);
            }
        }

        public override void Hide()
        {
            if (_animation != null &&
                _hideAnimation != null)
            {
                _animation.Play(_hideAnimation.name);
            }
        }

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(_animation, "[DESIGNERS] Animation component is not set!");
            Assert.IsNotNull(_showAnimation, "[DESIGNERS] Show animation clip asset is not set!");
            Assert.IsNotNull(_idleAnimation, "[DESIGNERS] Idle animation clip asset is not set!");
            Assert.IsNotNull(_hideAnimation, "[DESIGNERS] Hide animation clip asset is not set!");

            if (_animation != null &&
                _showAnimation != null &&
                _idleAnimation != null &&
                _hideAnimation != null)
            {
                _animation.Rewind();
                _animation.AddClip(_showAnimation, _showAnimation.name);
                _animation.AddClip(_idleAnimation, _idleAnimation.name);
                _animation.AddClip(_hideAnimation, _hideAnimation.name);
            }
        }
    }
}