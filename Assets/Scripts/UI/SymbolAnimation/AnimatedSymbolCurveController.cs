﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using UnityEngine;

namespace DreamTeam.UI.SymbolAnimation
{
    public class AnimatedSymbolCurveController : AnimatedSymbolController
    {
        [Serializable]
        private struct Curve3
        {
            public AnimationCurve X;
            public AnimationCurve Y;
            public AnimationCurve Z;
        }

        [Serializable]
        private struct TransformData
        {
            public bool IsActive;
            public Curve3 Position;
            public Curve3 Rotation;
            public AnimationCurve PositioningProcess;
            public AnimationCurve RotatingProcess;
            [Range(0f, 10f)] public float Duration;
        }

        [SerializeField] private TransformData _showData;
        [SerializeField] private TransformData _hideData;

        private Coroutine _currentTransformProcess = null;

        public override float ShowDuration => _showData.Duration;
        public override float HideDuration => _hideData.Duration;

        public override void Show()
        {
            if (_currentTransformProcess != null)
            {
                StopCoroutine(_currentTransformProcess);
            }

            _currentTransformProcess = StartCoroutine(MakeTransform(_showData));
        }

        public override void Hide()
        {
            if (_currentTransformProcess != null)
            {
                StopCoroutine(_currentTransformProcess);
            }

            _currentTransformProcess = StartCoroutine(MakeTransform(_hideData));
        }

        private IEnumerator MakeTransform(TransformData data)
        {
            if (data.IsActive)
            {
                float xRotation = data.Rotation.X.Evaluate(Symbol.AbsoluteValue);
                float yRotation = data.Rotation.Y.Evaluate(Symbol.AbsoluteValue);
                float zRotation = data.Rotation.Z.Evaluate(Symbol.AbsoluteValue);
                Quaternion targetRotation = Quaternion.Euler(xRotation, yRotation, zRotation);

                float xPosition = data.Position.X.Evaluate(Symbol.AbsoluteValue);
                float yPosition = data.Position.Y.Evaluate(Symbol.AbsoluteValue);
                float zPosition = data.Position.Z.Evaluate(Symbol.AbsoluteValue);
                Vector3 targetPosition = new Vector3(xPosition, yPosition, zPosition);

                float timer = 0f;
                Quaternion initialRotation = transform.localRotation;
                Vector3 initialPosition = transform.localPosition;
                while (timer < data.Duration)
                {
                    float time = timer / data.Duration;
                    float rotatingValue = data.RotatingProcess.Evaluate(time);
                    float positioningValue = data.PositioningProcess.Evaluate(time);

                    transform.localRotation = Quaternion.Lerp(initialRotation, targetRotation, rotatingValue);
                    transform.localPosition = Vector3.Lerp(initialPosition, targetPosition, positioningValue);

                    timer += Time.deltaTime;
                    yield return null;
                }

                transform.localRotation = targetRotation;
                transform.localPosition = targetPosition;
            }

            _currentTransformProcess = null;
        }
    }
}