﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

namespace DreamTeam.UI.SymbolAnimation
{
    public class AnimatedSymbol : MonoBehaviour
    {
        [SerializeField] private TMP_Text _textMeshProComponent;

        private List<AnimatedSymbolController> _controllers = new List<AnimatedSymbolController>();

        public char Symbol { get; private set; }
        public float AbsoluteValue { get; private set; }
        public float RelativeValue { get; private set; }

        public float ShowDuration => _controllers.Count > 0 ? _controllers.Max(controller => controller.ShowDuration) : 0f;
        public float HideDuration => _controllers.Count > 0 ? _controllers.Max(controller => controller.HideDuration) : 0f;

        public void Costruct(char symbol)
        {
            Symbol = symbol;
            _textMeshProComponent.SetText(Symbol.ToString());
        }

        public void Initialize(float absoluteValue, float relativeValue)
        {
            AbsoluteValue = absoluteValue;
            RelativeValue = relativeValue;   
        }

        public void Show()
        {
            foreach (var controller in _controllers)
            {
                controller.Show();
            }
        }

        public void Hide()
        {
            foreach (var controller in _controllers)
            {
                controller.Hide();
            }
        }

        protected void Awake()
        {
            _controllers = GetComponents<AnimatedSymbolController>().ToList();
        }
    }
}