﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Screen;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI
{
    /// <summary>
    /// Resizes <see cref="RectTransform"/> to fit safe zone of current device (i.e. to make offset from notch
    /// on iPhone X). Margin from each side of the screen will be changed only if safe zone margin is greater
    /// than source margin.
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class SafeAreaController : MonoBehaviour
    {
        [Inject] private ScreenController _screen;

        private Rect _safeArea;

        private RectTransform _rectTransform;
        private Canvas _canvas;

        private Vector2 _initialOffsetMin;
        private Vector2 _initialOffsetMax;

        protected void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
            _canvas = GetComponentInParent<Canvas>();

            _initialOffsetMin = _rectTransform.offsetMin;
            _initialOffsetMax = _rectTransform.offsetMax;
        }

        protected void OnEnable()
        {
            Canvas.willRenderCanvases += UpdateSafeArea;
        }

        protected void OnDisable()
        {
            Canvas.willRenderCanvases -= UpdateSafeArea;
        }

        private void UpdateSafeArea()
        {
            Rect safeArea = _screen.SafeArea;
            if (safeArea == _safeArea)
            {
                return;
            }

            _safeArea = safeArea;

            _rectTransform.offsetMin = _initialOffsetMin;
            _rectTransform.offsetMax = _initialOffsetMax;

            UpdateOffsetMin();
            UpdateOffsetMax();
        }

        private Rect GetLocalSafeAreaRect()
        {
            Vector2 safeAreaMin;
            Vector2 safeAreaMax;

            Camera canvasCamera = _canvas.renderMode != RenderMode.ScreenSpaceOverlay ? _canvas.worldCamera : null;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                    _rectTransform,
                    _safeArea.position,
                    canvasCamera,
                    out safeAreaMin
                );

            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                    _rectTransform,
                    _safeArea.position + _safeArea.size,
                    canvasCamera,
                    out safeAreaMax
                );

            return new Rect(safeAreaMin, safeAreaMax - safeAreaMin);
        }

        private void UpdateOffsetMin()
        {
            Rect safeAreaRect = GetLocalSafeAreaRect();
            Vector2 offsetMin = _rectTransform.offsetMin;

            if (safeAreaRect.xMin > _rectTransform.rect.xMin)
            {
                offsetMin.x += safeAreaRect.xMin - _rectTransform.rect.xMin;
            }

            if (safeAreaRect.yMin > _rectTransform.rect.yMin)
            {
                offsetMin.y += safeAreaRect.yMin - _rectTransform.rect.yMin;
            }

            _rectTransform.offsetMin = offsetMin;
        }

        private void UpdateOffsetMax()
        {
            Rect safeAreaRect = GetLocalSafeAreaRect();
            Vector2 offsetMax = _rectTransform.offsetMax;

            if (safeAreaRect.xMax < _rectTransform.rect.xMax)
            {
                offsetMax.x += safeAreaRect.xMax - _rectTransform.rect.xMax;
            }

            if (safeAreaRect.yMax < _rectTransform.rect.yMax)
            {
                offsetMax.y += safeAreaRect.yMax - _rectTransform.rect.yMax;
            }

            _rectTransform.offsetMax = offsetMax;
        }
    }
}