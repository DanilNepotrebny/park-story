﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Ads;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI
{
    public class AdsUserConsentAction : MonoBehaviour
    {
        [Inject] private AdsSystem _adsSystem;

        public void SetUserConsentAction(bool isConsent)
        {
            _adsSystem.SetUserConsent(isConsent);
        }
    }
}