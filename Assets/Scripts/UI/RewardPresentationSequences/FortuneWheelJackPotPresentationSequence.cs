﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Popups;
using DreamTeam.Popups.FortuneWheel;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Movement;
using Spine;
using Spine.Unity;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.RewardPresentationSequences
{
    public class FortuneWheelJackPotPresentationSequence : RewardPresentationSequenceBase
    {
        [Inject] private PopupSystem _popupSystem;
        [Inject] private Popup _popup;
        [Inject] private Instantiator _instantiator;

        [Header("Main data")]
        [SerializeField] private SceneReference _rewardsPopupScene;

        [Header("Reward box")]
        [SerializeField] private SkeletonGraphic _rewardBoxAnimationPrefab;
        [SerializeField] private Vector2 _spawnOffset;
        [SerializeField] private string _consumedAnimation;
        [SerializeField] private string _openAnimation;

        private RewardsPopup _rewardsPopup = null;
        private FortuneWheelPopup _fortuneWheelPopup => _popup as FortuneWheelPopup;
        private SkeletonGraphic _rewardBoxAnimation;
        private MovementSequence _boxMovementSequence;

        private bool _isPresentationComplete = false;

        public override void Present(ICollection<Reward> rewards, IDeferredInvocationHandle givingRewards)
        {
            StartCoroutine(PresentInternal(rewards, givingRewards));
        }

        public override bool IsPresentationComplete => _isPresentationComplete;

        private IEnumerator PresentInternal(ICollection<Reward> rewards, IDeferredInvocationHandle givingRewards)
        {
            _fortuneWheelPopup.FortuneManSpineAnimation.AnimationState?.SetAnimation(0, _fortuneWheelPopup.FortuneManSpineAnimationHappyName, true);

            _popupSystem.Hide(_popup.SceneName);

            var args = new RewardsPopup.Args()
            {
                Rewards = rewards
            };

            _popupSystem.Show(_rewardsPopupScene.Name, args, () => _rewardsPopup = _popupSystem.GetPopup(_rewardsPopupScene.Name) as RewardsPopup);
            yield return new WaitUntil(() => _rewardsPopup != null);

            _rewardBoxAnimation = _instantiator.Instantiate(_rewardBoxAnimationPrefab, _rewardsPopup.ContentTransform.gameObject);
            _rewardBoxAnimation.rectTransform.anchoredPosition = _spawnOffset;
            _boxMovementSequence = _rewardBoxAnimation.GetComponent<MovementSequence>();

            TrackEntry consumeAnimationTrack = _rewardBoxAnimation.AnimationState.SetAnimation(0, _consumedAnimation, false);
            yield return new WaitUntil(() => consumeAnimationTrack.IsComplete);

            _boxMovementSequence.Move(_rewardBoxAnimation.transform.position, _rewardsPopup.ContentTransform.position, false);
            yield return new WaitUntil(() => !_boxMovementSequence.IsMoving());

            TrackEntry openAnimationTrack = _rewardBoxAnimation.AnimationState.SetAnimation(0, _openAnimation, false);
            yield return new WaitUntil(() => openAnimationTrack.IsComplete);
            _rewardBoxAnimation.gameObject.Dispose();

            _rewardsPopup.PlayShowRewards();
            _isPresentationComplete = true;
            gameObject.Dispose();
        }
    }
}