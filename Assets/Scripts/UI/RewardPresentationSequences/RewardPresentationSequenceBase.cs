﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.UI.RewardPresentationSequences
{
    public abstract class RewardPresentationSequenceBase : MonoBehaviour
    {
        public abstract void Present(ICollection<Reward> rewards, IDeferredInvocationHandle givingRewards);
        public abstract bool IsPresentationComplete { get; }
    }
}