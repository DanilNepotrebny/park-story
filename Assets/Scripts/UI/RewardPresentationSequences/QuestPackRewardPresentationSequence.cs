﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Localization;
using DreamTeam.Popups;
using DreamTeam.Popups.Destabilization;
using DreamTeam.TransformLookup;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.Movement;
using Spine;
using Spine.Unity;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.RewardPresentationSequences
{
    public class QuestPackRewardPresentationSequence : RewardPresentationSequenceBase, IPopupDestabilizer
    {
        [Inject] private PopupSystem _popupSystem;
        [Inject] private Popup _popup;
        [Inject] private TransformLookupSystem _transfromLookupSystem;
        [Inject] private LocalizationSystem _localizationSystem;

        [SerializeField] private SkeletonGraphic _rewardBoxAnimation;
        [SerializeField, SpineAnimation] private string _consumedAnimation;
        [SerializeField, SpineAnimation] private string _openAnimation;
        [SerializeField, LocalizationKey] private string _rewardPopupHeaderTextKey;

        [Space] [SerializeField] private MovementSequence _boxMovementSequence;

        [Space]
        [SerializeField] private TransformLookupId _giftFinishTransformId;
        [SerializeField] private SceneReference _rewardsPopupScene;

        private bool _isPresentationComplete = false;

        public override void Present(ICollection<Reward> rewards, IDeferredInvocationHandle givingRewards)
        {
            StartCoroutine(PresentInternal(rewards, givingRewards));
        }

        public override bool IsPresentationComplete => _isPresentationComplete;

        private IEnumerator PresentInternal(ICollection<Reward> rewards, IDeferredInvocationHandle givingRewards)
        {
            _popup.AddDestabilizer(this);
            IsStable = false;
            Destabilized?.Invoke();

            TrackEntry consumeAnimationTrack = _rewardBoxAnimation.AnimationState.SetAnimation(0, _consumedAnimation, false);
            yield return new WaitUntil(() => consumeAnimationTrack.IsComplete);

            Transform finishPoint = _transfromLookupSystem.FindTransform(_giftFinishTransformId);
            transform.SetParent(finishPoint.transform, true);

            _boxMovementSequence.Move(transform.position, finishPoint.position, false);
            yield return new WaitUntil(() => !_boxMovementSequence.IsMoving());

            IsStable = true;
            Stabilized?.Invoke();
            _popup.RemoveDestabilizer(this);

            _popupSystem.Hide(_popup.SceneName);

            TrackEntry openAnimationTrack = _rewardBoxAnimation.AnimationState.SetAnimation(0, _openAnimation, false);
            yield return new WaitUntil(() => openAnimationTrack.IsComplete);

            var args = new RewardsPopup.Args()
            {
                RewardsTakenCallback = givingRewards.Unlock,
                ClosedCallback = () => _popupSystem.Show(_popup.SceneName),
                Header = _localizationSystem.Localize(_rewardPopupHeaderTextKey, this),
                Rewards = rewards
            };

            _popupSystem.Show(_rewardsPopupScene.Name, args);

            _isPresentationComplete = true;

            gameObject.Dispose();
        }

        public event Action Stabilized;
        public event Action Destabilized;
        public bool IsStable { get; private set; }
    }
}