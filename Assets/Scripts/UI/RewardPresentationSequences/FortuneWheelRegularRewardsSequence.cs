﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Popups;
using DreamTeam.Popups.FortuneWheel;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.RewardPresentationSequences
{
    public class FortuneWheelRegularRewardsSequence : RewardPresentationSequenceBase
    {
        [Inject] private PopupSystem _popupSystem;
        [Inject] private Popup _popup;

        [SerializeField] private SceneReference _rewardsPopupScene;

        private FortuneWheelPopup _fortuneWheelPopup => _popup as FortuneWheelPopup;
        private bool _isRewardsPopupShown = false;
        private bool _isPresentationComplete = false;

        public override void Present(ICollection<Reward> rewards, IDeferredInvocationHandle givingRewards)
        {
            StartCoroutine(PresentInternal(rewards, givingRewards));
        }

        public override bool IsPresentationComplete => _isPresentationComplete;

        private IEnumerator PresentInternal(ICollection<Reward> rewards, IDeferredInvocationHandle givingRewards)
        {
            var args = new RewardsPopup.Args()
            {
                Rewards = rewards
            };

            _popupSystem.Show(_rewardsPopupScene.Name, args, () => _isRewardsPopupShown = true);

            _fortuneWheelPopup.FortuneManSpineAnimation.AnimationState?.SetAnimation(
                    0,
                    _fortuneWheelPopup.FortuneManSpineAnimationHappyName,
                    true
                );

            _fortuneWheelPopup.FortuneManBubbleAnimation.Play(_fortuneWheelPopup.FortuneManBubbleAnimationShow.name);

            yield return new WaitUntil(IsRewardsPopupClosed);

            _fortuneWheelPopup.FortuneManSpineAnimation.AnimationState?.SetAnimation(
                    0,
                    _fortuneWheelPopup.FortuneManSpineAnimationIdleName,
                    true
                );
            _fortuneWheelPopup.FortuneManBubbleAnimation.Play(_fortuneWheelPopup.FortuneManBubbleAnimationHide.name);
            yield return new WaitForSeconds(_fortuneWheelPopup.FortuneManBubbleAnimationHide.length);

            _isPresentationComplete = true;
        }

        private bool IsRewardsPopupClosed()
        {
            Popup rewardsPopup = _popupSystem.GetPopup(_rewardsPopupScene.Name);
            return rewardsPopup != null && _isRewardsPopupShown && !rewardsPopup.IsShown;
        }
    }
}