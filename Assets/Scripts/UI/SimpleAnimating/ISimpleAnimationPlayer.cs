﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.UI.SimpleAnimating
{
    public enum SimpleAnimationType
    {
        Show,
        Hide,
        Idle,
    }

    public interface ISimpleAnimationPlayer
    {
        bool IsPlaying { get; }
        SimpleAnimationType PlayingAnimationType { get; }
        GameObject GameObject { get; }

        event Action<SimpleAnimationType> AnimationStarted;
        event Action<SimpleAnimationType> AnimationStopped;
        event Action<ISimpleAnimationPlayer, SimpleAnimationType> AnimationFinished;

        bool Play(SimpleAnimationType type);
    }
}