﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.UI.SimpleAnimating
{
    [RequireComponent(typeof(Animation))]
    public class SimpleAnimation : MonoBehaviour, ISimpleAnimationPlayer
    {
        private Coroutine _currentCoroutine;
        private Animation _animation => GetComponent<Animation>();

        public bool IsPlaying => _currentCoroutine != null;
        public SimpleAnimationType PlayingAnimationType { get; private set; }

        GameObject ISimpleAnimationPlayer.GameObject => gameObject;

        public event Action<ISimpleAnimationPlayer, SimpleAnimationType> AnimationFinished;
        public event Action<SimpleAnimationType> AnimationStarted;
        public event Action<SimpleAnimationType> AnimationStopped;
        public event Action<SimpleAnimationType, float> AnimationUpdated;

        /// <returns> Is animation started successfully </returns>
        public bool Play(SimpleAnimationType type)
        {
            if (!gameObject.activeInHierarchy)
            {
                UnityEngine.Debug.LogWarning($"{name} SimpleAnimator can't play {type}. GameObject is disabled");
                return false;
            }

            if (_animationClips[type] == null)
            {
                UnityEngine.Debug.LogWarning(
                    $"{name} SimpleAnimator can't play {type}. Animation clip is not assigned");
                return false;
            }

            Stop();
            _currentCoroutine = StartCoroutine(GetPlayEnumerator(type));

            return true;
        }

        public void Stop()
        {
            if (IsPlaying)
            {
                StopCoroutine(_currentCoroutine);
                _currentCoroutine = null;
                AnimationStopped?.Invoke(PlayingAnimationType);
            }
        }

        /// <returns> Animation duration in seconds. Returns 0 if animation has no assigned animation clip </returns>
        public float GetAnimationDuration(SimpleAnimationType type)
        {
            if (_animationClips[type] == null)
            {
                UnityEngine.Debug.LogWarning(
                    $"Can't determine clip length for {type} animation in {name} " +
                    $"SimpleAnimator. Assign it first! Return 0 seconds instead");
                return 0;
            }

            AnimationClip clip = _animation.GetClip(_animationClips[type].name);
            return clip.length;
        }

        public IEnumerator GetPlayEnumerator(SimpleAnimationType type)
        {
            PlayingAnimationType = type;

            AnimationStarted?.Invoke(type);
            _animation.AddClip(_animationClips[type], _animationClips[type].name);
            bool isPlayStarted = _animation.Play(_animationClips[type].name);
            Assert.IsTrue(isPlayStarted, "!isPlayStarted");

            yield return new WaitUntil(
                    () =>
                    {
                        float normalizedTime = _animation[_animationClips[type].name].normalizedTime;
                        AnimationUpdated?.Invoke(type, normalizedTime);
                        return (!_animation.isPlaying);
                    }
                );

            Stop();
            AnimationFinished?.Invoke(this, type);
        }

        protected void OnDisable()
        {
            Stop();
        }

        #region Inner types

        [Serializable]
        private class AnimationClipsMap : KeyValueList<SimpleAnimationType, AnimationClip>
        {
        }

        [SerializeField, ContainsAllEnumKeys] private AnimationClipsMap _animationClips;

        #endregion Inner types
    }
}