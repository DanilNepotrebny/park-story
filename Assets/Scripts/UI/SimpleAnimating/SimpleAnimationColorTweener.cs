﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.SimpleAnimating
{
    public class SimpleAnimationColorTweener : SimpleAnimationTweener<Graphic>
    {
        [SerializeField] private Gradient _gradient = new Gradient();

        protected override void ApplyTweening(Graphic target, float normalizedTime)
        {
            target.color = _gradient.Evaluate(normalizedTime);
        }
    }
}