﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.UI.SimpleAnimating
{
    [RequireComponent(typeof(Animator))]
    public class SimpleAnimator : MonoBehaviour, ISimpleAnimationPlayer, IAnimatorStateEnterHandler
    {
        [SerializeField] private string _visibilityParameterName = "IsVisible";
        [SerializeField] private string _visibilityChangedTriggerName = "IsVisibilityChanged";
        [SerializeField] private string _visibilityRestoredTriggerName = "IsVisibilityRestored";
        [SerializeField] private string _shownStateName = "Shown";
        [SerializeField] private string _hiddenStateName = "Hidden";


        private Animator _animator;
        private bool _isAnimatorStateInitialized;
        private SimpleAnimationType? _playingAnimationType;

        public bool IsPlaying { get; private set; }
        public SimpleAnimationType PlayingAnimationType => (SimpleAnimationType)_playingAnimationType;

        GameObject ISimpleAnimationPlayer.GameObject => gameObject;

        public event Action<SimpleAnimationType> AnimationStarted;
        public event Action<SimpleAnimationType> AnimationStopped;
        public event Action<ISimpleAnimationPlayer, SimpleAnimationType> AnimationFinished;

        [Inject]
        protected void Construct()
        {
            _animator = GetComponent<Animator>();

            Assert.IsTrue(!string.IsNullOrEmpty(_visibilityParameterName), $"Parameter {_visibilityParameterName} is not set.");
            Assert.IsTrue(!string.IsNullOrEmpty(_shownStateName), $"Parameter {_shownStateName} is not set.");
            Assert.IsTrue(!string.IsNullOrEmpty(_hiddenStateName), $"Parameter {_hiddenStateName} is not set.");
        }

        public bool Play(SimpleAnimationType type)
        {
            if (type != SimpleAnimationType.Show && type != SimpleAnimationType.Hide)
            {
                UnityEngine.Debug.LogError($"Animation type '{type}' is not supported by {nameof(SimpleAnimator)}", this);
                return false;
            }

            InitializeAnimatorState();

            _playingAnimationType = type;
            _animator.SetBool(_visibilityParameterName, type == SimpleAnimationType.Show);
            _animator.SetTrigger(_visibilityChangedTriggerName);
            return true;
        }

        protected void OnEnable()
        {
            InitializeAnimatorState();
            RestoreVisibility();
        }

        protected void OnDisable()
        {
            _isAnimatorStateInitialized = false;
        }

        private void InitializeAnimatorState()
        {
            if (_isAnimatorStateInitialized)
            {
                return;
            }

            _isAnimatorStateInitialized = true;
            _animator.Update(0);
        }

        private void RestoreVisibility()
        {
            if (_playingAnimationType != null)
            {
                _animator.SetBool(_visibilityParameterName, _playingAnimationType == SimpleAnimationType.Show);
                _animator.SetTrigger(_visibilityRestoredTriggerName);
            }
        }

        private void HandleStartEvent(Animator animator)
        {
            if (!animator.GetBool(_visibilityChangedTriggerName))
            {
                SimpleAnimationType animationType;
                if (animator.GetBool(_visibilityParameterName))
                {
                    animationType = SimpleAnimationType.Show;
                }
                else
                {
                    animationType = SimpleAnimationType.Hide;
                }

                IsPlaying = true;
                AnimationStarted?.Invoke(animationType);
            }
        }

        private void HandleStopEvent(AnimatorStateInfo stateInfo)
        {
            SimpleAnimationType animationType;
            if (stateInfo.IsName(_shownStateName))
            {
                animationType = SimpleAnimationType.Show;
            }
            else if (stateInfo.IsName(_hiddenStateName))
            {
                animationType = SimpleAnimationType.Hide;
            }
            else
            {
                return;
            }

            IsPlaying = false;
            AnimationStopped?.Invoke(animationType);
            AnimationFinished?.Invoke(this, animationType);
        }

        void IAnimatorStateEnterHandler.OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (IsPlaying)
            {
                HandleStopEvent(stateInfo);
            }
            else
            {
                HandleStartEvent(animator);
            }
        }
    }
}