﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.UI.SimpleAnimating
{
    public abstract class SimpleAnimationTweener<TTarget> : MonoBehaviour where TTarget : Object
    {
        [SerializeField] private SimpleAnimationType _animationType;
        [SerializeField] private TTarget[] _targets;

        private SimpleAnimation _animation => GetComponent<SimpleAnimation>();

        protected void Awake()
        {
            _animation.AnimationStarted += OnAnimationStarted;
            _animation.AnimationUpdated += OnAnimationUpdated;
            _animation.AnimationFinished += OnAnimationFinished;
        }

        protected void OnDestroy()
        {
            _animation.AnimationStarted -= OnAnimationStarted;
            _animation.AnimationUpdated -= OnAnimationUpdated;
            _animation.AnimationFinished -= OnAnimationFinished;
        }

        protected void Reset()
        {
            _targets = GetComponentsInChildren<TTarget>();
        }

        protected abstract void ApplyTweening(TTarget target, float normalizedTime);

        private void OnAnimationStarted(SimpleAnimationType animationType)
        {
            OnAnimationUpdated(animationType, 0);
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            OnAnimationUpdated(animationType, 1);
        }

        private void OnAnimationUpdated(SimpleAnimationType animationType, float normalizedTime)
        {
            if (animationType != _animationType)
            {
                return;
            }

            foreach (TTarget target in _targets)
            {
                if (target != null)
                {
                    ApplyTweening(target, normalizedTime);
                }
            }
        }
    }
}