﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;

namespace DreamTeam.UI.SimpleAnimating
{
    public class ActivitySetterBySimpleAnimationFinished : MonoBehaviour
    {
        [SerializeField, RequiredField] private Component _simpleAnimationComponent;
        [SerializeField] private SimpleAnimationType _animationType;
        [SerializeField] private bool _setIsActive;

        [Space, SerializeField, FormerlySerializedAs("_targets")]
        private GameObject[] _targetGameObjects = new GameObject[0];

        [SerializeField]
        private Behaviour[] _targetBehaviours = new Behaviour[0];

        private ISimpleAnimationPlayer _simpleAnimation => _simpleAnimationComponent as ISimpleAnimationPlayer;

        protected void OnEnable()
        {
            _simpleAnimation.AnimationFinished += OnAnimationFinished;
        }

        protected void OnDisable()
        {
            _simpleAnimation.AnimationFinished -= OnAnimationFinished;
        }

        protected void OnValidate()
        {
            if (_simpleAnimationComponent != null)
            {
                _simpleAnimationComponent = _simpleAnimation as Component;
                Assert.IsNotNull(
                        _simpleAnimationComponent,
                        $"{nameof(_simpleAnimationComponent)} must implement {nameof(ISimpleAnimationPlayer)}"
                    );
            }
        }

        private void OnAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            if (animationType == _animationType)
            {
                _simpleAnimation.AnimationFinished -= OnAnimationFinished;
                SetTargetsActive(_setIsActive);
            }
        }

        private void SetTargetsActive(bool isActive)
        {
            foreach (GameObject gameObject in _targetGameObjects)
            {
                gameObject.SetActive(isActive);
            }

            foreach (Behaviour behaviour in _targetBehaviours)
            {
                behaviour.enabled = isActive;
            }
        }
    }
}