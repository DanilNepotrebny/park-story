﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups.Destabilization;

namespace DreamTeam.UI.SimpleAnimating
{
    public class SimpleAnimationPopupDestabilizer : PopupDestabilizer
    {
        public override bool IsStable => !_animation.IsPlaying;
        private ISimpleAnimationPlayer _animation => GetComponent<ISimpleAnimationPlayer>();

        protected void Awake()
        {
            _animation.AnimationStarted += OnAnimationStarted;
            _animation.AnimationStopped += OnAnimationStopped;
        }

        protected void OnDestroy()
        {
            _animation.AnimationStarted -= OnAnimationStarted;
            _animation.AnimationStopped -= OnAnimationStopped;
        }

        #if UNITY_EDITOR
        protected void Reset()
        {
            if (_animation == null)
            {
                UnityEngine.Debug.LogAssertion(
                        $"{nameof(SimpleAnimationPopupDestabilizer)} requires some " +
                        $"component implementing {nameof(ISimpleAnimationPlayer)}"
                    );
                DestroyImmediate(this);
            }
        }
        #endif

        private void OnAnimationStopped(SimpleAnimationType animationType)
        {
            HandleStabilized();
        }

        private void OnAnimationStarted(SimpleAnimationType animationType)
        {
            HandleDestabilized();
        }
    }
}