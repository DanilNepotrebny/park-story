﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Popups;
using DreamTeam.Popups.Destabilization;
using DreamTeam.TransformLookup;
using DreamTeam.UI.HUD;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.RewardPresenters
{
    [RequireComponent(typeof(GroupMotionAnimator))]
    public class OnRewardReceivedMotionSequence : OnRewardReceivedPresenter, IPopupDestabilizer
    {
        [Inject] private TransformLookupSystem _transformLookupSystem;
        [Inject] private IResourcesPanel _resourcesPanel;

        private GroupMotionAnimator _motionAnimator;

        private int _baseRewardCount;
        private int _displayedPreviousCount;
        private int _totalRewardCount;
        private bool _isPopupStable = true;

        private Transform _finishPoint => _transformLookupSystem.FindTransform(Reward.TransformId);

        private ILockablePresenter<CountedItemMotionFinishData> _lockablePresenter =>
            _finishPoint.GetComponent<ILockablePresenter<CountedItemMotionFinishData>>();

        private CountedItemReward _countedItemReward => Reward as CountedItemReward;

        private int _rewardCountPerMotion => _totalRewardCount / _motionAnimator.ElementsAmount;
        private int _reachedRewardCount => _motionAnimator.ReachedElementsCount * _rewardCountPerMotion;
        private int _rewardItemCount => _countedItemReward?.Item.Count ?? 1;

        private int _displayedCurrentCount =>
            _motionAnimator.IsPlaying ? _baseRewardCount + _reachedRewardCount : _rewardItemCount;

        bool IPopupDestabilizer.IsStable => _isPopupStable;

        private event Action _stabilized;
        private event Action _destabilized;

        event Action IPopupDestabilizer.Stabilized
        {
            add { _stabilized += value; }
            remove { _stabilized -= value; }
        }

        event Action IPopupDestabilizer.Destabilized
        {
            add { _destabilized += value; }
            remove { _destabilized -= value; }
        }

        protected void Awake()
        {
            _motionAnimator = GetComponent<GroupMotionAnimator>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            SubscribeStartMotion();
            _motionAnimator.MotionFinished += OnMotionFinished;
            _motionAnimator.AnElementReached += OnMotionElementReached;
            Popup.AddDestabilizer(this);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            UnsubscribeStartMotion();
            _motionAnimator.MotionFinished -= OnMotionFinished;
            _motionAnimator.AnElementReached -= OnMotionElementReached;
            Popup.RemoveDestabilizer(this);
        }

        protected override void UpdatePresentation()
        {
            _isPopupStable = false;
            _destabilized?.Invoke();

            if (!_resourcesPanel.TryShow(_countedItemReward.Item, OnResourcePanelShown))
            {
                OnResourcePanelShown(_countedItemReward.Item);
            }
        }

        private void OnResourcePanelShown(CountedItem item)
        {
            if (item == _countedItemReward.Item)
            {
                _motionAnimator.StartPoint = transform;
                _motionAnimator.FinishPoint = _finishPoint;
                _motionAnimator.SetElementsParent(_motionAnimator.FinishPoint);
                _motionAnimator.Play();
            }
        }

        private void SubscribeStartMotion()
        {
            if (RewardsPopup != null)
            {
                RewardsPopup.GivingRewards += StartMotion;
            }
            else if (_countedItemReward != null)
            {
                _countedItemReward.Receiving += StartMotion;
            }
        }

        private void UnsubscribeStartMotion()
        {
            if (RewardsPopup != null)
            {
                RewardsPopup.GivingRewards -= StartMotion;
            }
            else if (_countedItemReward != null)
            {
                _countedItemReward.Receiving -= StartMotion;
            }
        }

        private void OnMotionElementReached()
        {
            var updateArgs = new CountedItemMotionFinishData(_displayedCurrentCount, _displayedPreviousCount);
            _lockablePresenter?.UpdateManualy(updateArgs);
            _displayedPreviousCount = _displayedCurrentCount;
        }

        private void OnMotionFinished()
        {
            if (!_resourcesPanel.TryHide(_countedItemReward.Item, OnResourcePanelHidden))
            {
                OnResourcePanelHidden(_countedItemReward.Item);
            }
        }

        private void OnResourcePanelHidden(CountedItem item)
        {
            _isPopupStable = true;
            _stabilized?.Invoke();
            _lockablePresenter?.Unlock();
        }

        private void StartMotion()
        {
            _lockablePresenter?.Lock();

            _baseRewardCount = _displayedPreviousCount = _rewardItemCount;
            _totalRewardCount = _countedItemReward != null ? _countedItemReward.Count : 1;
        }
    }
}