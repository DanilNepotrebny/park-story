﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.UI.RewardPresenters
{
    public struct CountedItemMotionFinishData
    {
        public int CurrentCount { get; }
        public int PreviousCount { get; }

        public CountedItemMotionFinishData(int currentCount, int previousCount)
        {
            CurrentCount = currentCount;
            PreviousCount = previousCount;
        }
    }
}