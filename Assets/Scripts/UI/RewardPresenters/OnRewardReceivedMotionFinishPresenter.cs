﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.UI.RewardPresenters
{
    public class OnRewardReceivedMotionFinishPresenter : MonoBehaviour, ILockablePresenter<CountedItemMotionFinishData>
    {
        [SerializeField]
        private List<LockablePresenterReference> _subPresenters = new List<LockablePresenterReference>();

        protected void OnValidate()
        {
            _subPresenters.ForEach(item => item.OnValidate());
        }

        void ILockablePresenter<CountedItemMotionFinishData>.Lock()
        {
            _subPresenters.ForEach(item => item.LockablePresenter?.Lock());
        }

        void ILockablePresenter<CountedItemMotionFinishData>.Unlock()
        {
            _subPresenters.ForEach(item => item.LockablePresenter?.Unlock());
        }

        void ILockablePresenter<CountedItemMotionFinishData>.UpdateManualy(CountedItemMotionFinishData data)
        {
            _subPresenters.ForEach(item => item.LockablePresenter?.UpdateManualy(data));
        }

        [Serializable]
        private class LockablePresenterReference
        {
            [SerializeField, RequiredField]
            private Component _lockablePresenterComponent;

            public ILockablePresenter<CountedItemMotionFinishData> LockablePresenter =>
                _lockablePresenterComponent as ILockablePresenter<CountedItemMotionFinishData>;

            public void OnValidate()
            {
                if (_lockablePresenterComponent != null)
                {
                    _lockablePresenterComponent = LockablePresenter as Component;
                    Assert.IsNotNull(
                            _lockablePresenterComponent,
                            $"{nameof(_lockablePresenterComponent)} must implement " +
                                "{nameof(ILockablePresenter<MotionElement>)}"
                        );
                }
            }
        }
    }
}