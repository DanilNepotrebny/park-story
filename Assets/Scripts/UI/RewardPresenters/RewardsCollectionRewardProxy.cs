﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Inventory.Rewards;
using UnityEngine;

namespace DreamTeam.UI.RewardPresenters
{
    public class RewardsCollectionRewardProxy : PresenterProxy<ICollection<Reward>, Reward>
    {
        [SerializeField] private int _rewardIndex = 0;

        protected override Reward TargetModel => SourceModel.ElementAt(_rewardIndex);
    }
}