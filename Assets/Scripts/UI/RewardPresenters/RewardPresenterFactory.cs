﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.UI.RewardPresenters
{
    [CreateAssetMenu]
    public class RewardPresenterFactory :
        ViewFactory<Reward, RewardPresenterFactory.ModelRefType, RewardPresenterFactory.PresentersMap>
    {
        [Serializable]
        public class ModelRefType : TypeReference<Reward>
        {
        }

        [Serializable]
        public class PresentersMap : KeyValueList<ModelRefType, GameObject>
        {
        }
    }
}