﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.UI.RewardPresenters
{
    public class RewardsCollectionPresenter : MonoBehaviour, IPresenter<ICollection<Reward>>
    {
        [SerializeField, RequiredField] private RewardPresenterFactory _rewardPresenterFactory;
        [SerializeField] private SwitcherByScreenOrientation _orientationSwitcher;
        [SerializeField] private SelectionType _selectionType = SelectionType.All;
        [SerializeField, HideInInspector] private List<int> _indexes = new List<int>();
        [SerializeField, HideInInspector] private Vector2Int _range;

        [Inject] private Instantiator _instantiator;

        private List<GameObject> _presenters = new List<GameObject>();

        public enum SelectionType
        {
            All,
            Indexes,
            Range,
        }

        public virtual void Initialize(ICollection<Reward> model)
        {
            Assert.IsNotNull(_rewardPresenterFactory);

            Clear();

            var rewards = SelectRewards(model);
            foreach (Reward reward in rewards)
            {
                Transform parentTransform = _orientationSwitcher != null && _orientationSwitcher.Current != null ?
                    _orientationSwitcher.Current.transform :
                    transform;

                GameObject presenter = _rewardPresenterFactory.Create(reward, _instantiator, parentTransform, false);
                _presenters.Add(presenter);
            }
        }

        private void Clear()
        {
            _presenters.ForEach(item => item?.Dispose());
            _presenters.Clear();
        }

        private IEnumerable<Reward> SelectRewards(ICollection<Reward> collection)
        {
            IEnumerable<Reward> result = null;

            switch (_selectionType)
            {
                case SelectionType.All:
                    result = collection;
                    break;

                case SelectionType.Indexes:
                    result = collection.Where((reward, index) => _indexes.Contains(index));
                    break;

                case SelectionType.Range:
                    result = collection.Skip(_range.x).Take(_range.y);
                    break;
            }

            return result;
        }
    }
}