﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.UI.RewardPresenters
{
    public class LevelFailureOfferPresenter : RewardsCollectionPresenter
    {
        [SerializeField, RequiredField] private PresenterHub _mainRewardPresenter;

        public override void Initialize(ICollection<Reward> model)
        {
            Assert.IsNotNull(_mainRewardPresenter);

            if (model.Count > 0)
            {
                var rewards = new List<Reward>(model);
                _mainRewardPresenter.Initialize(rewards[0]);
                rewards.RemoveAt(0);
                base.Initialize(rewards);
            }
        }
    }
}