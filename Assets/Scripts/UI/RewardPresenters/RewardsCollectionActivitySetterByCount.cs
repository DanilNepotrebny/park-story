﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory.Rewards;
using UnityEngine;

namespace DreamTeam.UI.RewardPresenters
{
    public class RewardsCollectionActivitySetterByCount : MonoBehaviour, IPresenter<ICollection<Reward>>
    {
        [SerializeField] private int _minThreshold = 0;

        public void Initialize(ICollection<Reward> model)
        {
            gameObject.SetActive(model.Count > _minThreshold);
        }

        protected void OnValidate()
        {
            _minThreshold = Mathf.Max(_minThreshold, 0);
        }
    }
}