﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Rewards;

namespace DreamTeam.UI.RewardPresenters
{
    public class CountedItemRewardPresenterInitializer : PresenterInitializer<CountedItemReward>
    {
    }
}