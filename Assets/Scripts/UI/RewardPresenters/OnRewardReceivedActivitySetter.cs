﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.UI.RewardPresenters
{
    public class OnRewardReceivedActivitySetter : OnRewardReceivedPresenter
    {
        [SerializeField] private bool _isActiveOnReceived;

        protected override void UpdatePresentation()
        {
            gameObject.SetActive(_isActiveOnReceived);
        }
    }
}