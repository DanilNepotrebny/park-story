﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Rewards;
using DreamTeam.Popups;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.RewardPresenters
{
    public abstract class OnRewardReceivedPresenter : MonoBehaviour, IPresenter<Reward>
    {
        [Inject] protected Popup Popup { get; private set; }

        protected RewardsPopup RewardsPopup => Popup as RewardsPopup;
        protected Reward Reward { get; private set; }

        public void Initialize(Reward model)
        {
            Reward = model;
        }

        protected virtual void OnEnable()
        {
            if (RewardsPopup != null)
            {
                RewardsPopup.Hiding += OnPopupHiding;
            }
            else if (Reward != null)
            {
                Reward.Received += OnRewardReceived;
            }
        }

        protected virtual void OnDisable()
        {
            if (RewardsPopup != null)
            {
                RewardsPopup.Hiding -= OnPopupHiding;
            }
            else if (Reward != null)
            {
                Reward.Received -= OnRewardReceived;
            }
        }

        protected abstract void UpdatePresentation();

        private void OnPopupHiding(Popup popup)
        {
            UpdatePresentation();
        }

        private void OnRewardReceived(Reward reward)
        {
            UpdatePresentation();
        }
    }
}