﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Ads;
using DreamTeam.Popups;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.RewardPresenters
{
    public class AdsRewardsViewNextVideoButtonPresenter : MonoBehaviour
    {
        [SerializeField] private Button _button;

        [Inject] private Popup _popup;
        [Inject] private AdsSystem _adsSystem;
        [Inject] private PopupSystem _popupSystem;

        protected void Awake()
        {
            _popup.Showing += OnPopupShowing;
            _popup.Hiding += OnPopupHiding;
        }

        protected void OnDestroy()
        {
            _popup.Showing -= OnPopupShowing;
            _popup.Hiding -= OnPopupHiding;
        }

        private void OnPopupShowing(Popup popup)
        {
            gameObject.SetActive(_adsSystem.IsRewardedVideoAvailable);
            _button.onClick.AddListener(OnViewVideoButtonPressed);
        }

        private void OnPopupHiding(Popup popup)
        {
            _button.onClick.RemoveListener(OnViewVideoButtonPressed);
        }

        private void OnViewVideoButtonPressed()
        {
            _popupSystem.HideAll(() => _adsSystem.ShowRewardedVideo());
        }
    }
}