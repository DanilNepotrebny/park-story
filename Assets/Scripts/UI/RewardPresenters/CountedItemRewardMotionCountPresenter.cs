﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Rewards;
using DreamTeam.Popups;
using UnityEngine;

namespace DreamTeam.UI.RewardPresenters
{
    [RequireComponent(typeof(GroupMotionAnimator))]
    public class CountedItemRewardMotionCountPresenter : MonoBehaviour, IPresenter<CountedItemReward>
    {
        public void Initialize(CountedItemReward model)
        {
            GetComponent<GroupMotionAnimator>().SetElementsAmount(model.Count);
        }
    }
}