﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DreamTeam.UI
{
    public abstract class BaseHintController<TModel> :
        MonoBehaviour,
        IPresenter<TModel>,
        ISelectHandler,
        IDeselectHandler,
        IPointerClickHandler
        where TModel : class
    {
        private TModel _model;
        private bool _isSelected;

        [SerializeField] private Selectable _tintPanel;

        protected abstract BaseModelHint<TModel> Hint { get; }

        public void Initialize(TModel model)
        {
            _model = model;
            ResetSelection();
        }

        public void OnDeselect(BaseEventData eventData)
        {
            _isSelected = false;
            Hint.Hide();
            HideTint();
        }

        public void OnSelect(BaseEventData eventData)
        {
            _isSelected = true;
            Hint.Show(_model, transform);
            ShowTint();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            EventSystem.current.SetSelectedGameObject(!_isSelected ? gameObject : null, eventData);
        }

        protected void OnDisable()
        {
            ResetSelection();
        }

        private void ResetSelection()
        {
            _isSelected = false;
            EventSystem.current?.SetSelectedGameObject(null);
            Hint.Hide();
            HideTint();
        }

        private void ShowTint()
        {
            if (_tintPanel != null)
            {
                _tintPanel.interactable = true;
                _tintPanel.image.raycastTarget = true;
            }
        }

        private void HideTint()
        {
            if (_tintPanel != null)
            {
                _tintPanel.interactable = false;
                _tintPanel.image.raycastTarget = false;
            }
        }
    }
}