﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location;
using UnityEngine;

namespace DreamTeam.UI.LocationDataPresenters
{
    public abstract class LocationRestoredDataPresenter : MonoBehaviour, IPresenter<LocationData>
    {
        public void Initialize(LocationData model)
        {
            if (model != null)
            {
                Initialize(model.RestoredData);
            }
        }

        protected abstract void Initialize(LocationRestoredData model);
    }
}