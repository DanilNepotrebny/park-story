﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.LocationDataPresenters
{
    [RequireComponent(typeof(Image))]
    public class LocationRestoredImagePresenter : LocationRestoredDataPresenter
    {
        protected override void Initialize(LocationRestoredData model)
        {
            GetComponent<Image>().sprite = model.Image;
        }
    }
}