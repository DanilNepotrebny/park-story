﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using DreamTeam.Location;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.LocationDataPresenters
{
    [RequireComponent(typeof(Text))]
    public class LocationRestoredMessagePresenter : LocationRestoredDataPresenter
    {
        [Inject] private LocalizationSystem _localizationSystem;

        protected override void Initialize(LocationRestoredData model)
        {
            GetComponent<Text>().text = _localizationSystem.Localize(model.Message, this);
        }
    }
}