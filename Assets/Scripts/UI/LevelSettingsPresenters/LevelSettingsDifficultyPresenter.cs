﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Levels;

namespace DreamTeam.UI.LevelSettingsPresenters
{
    public class LevelSettingsDifficultyPresenter : PresenterProxy<LevelSettings, LevelDifficulty>
    {
        protected override LevelDifficulty TargetModel => SourceModel.Difficulty;
    }
}