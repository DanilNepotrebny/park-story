﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI
{
    [RequireComponent(typeof(Animation))]
    public class AnimatedText : Text
    {
        private Animation _animation => GetComponent<Animation>();

        public override string text
        {
            get { return base.text; }
            set
            {
                if (!base.text.Equals(value))
                {
                    base.text = value;
                    PlayAnimation();
                }
            }
        }

        private void PlayAnimation()
        {
            _animation.Play();
        }
    }
}