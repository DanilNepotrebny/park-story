﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Rewards;
using DreamTeam.SocialNetworking;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.SocialNetworkingPresenters.ExchangeButton
{
    [RequireComponent(typeof(Button))]
    public abstract class SocialProfilesExchangeButton : MonoBehaviour
    {
        [SerializeField, RequiredField] private GameObject _togglesRoot;
        [SerializeField, RequiredField] private CountedItemReward _reward;

        [Inject] protected SocialProfileInventoryExchangeSystem ExchangeSystem { get; private set; }

        protected CountedItemReward Reward => _reward;

        protected void OnEnable()
        {
            GetComponent<Button>().onClick.AddListener(OnButtonClicked);
        }

        protected void OnDisable()
        {
            GetComponent<Button>().onClick.RemoveListener(OnButtonClicked);
        }

        protected abstract void ProcessSelections(SocialProfileSelectionPresenter[] selections);

        private void OnButtonClicked()
        {
            ProcessSelections(_togglesRoot.GetComponentsInChildren<SocialProfileSelectionPresenter>());
        }
    }
}