﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.UI.SocialNetworkingPresenters.ExchangeButton
{
    public class SocialProfilesSendItemButton : SocialProfilesExchangeButton
    {
        protected override void ProcessSelections(SocialProfileSelectionPresenter[] selections)
        {
            foreach (SocialProfileSelectionPresenter selection in selections)
            {
                if (selection.IsSelected)
                {
                    ExchangeSystem.SendItem(selection.Profile, Reward.Item, Reward.Count);
                }
            }
        }
    }
}