﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.SocialNetworking;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.SocialNetworkingPresenters
{
    [RequireComponent(typeof(SwitcherByScreenOrientation))]
    public class SocialProfilesCollectionPresenter : MonoBehaviour, IPresenter<ICollection<SocialProfile>>
    {
        [SerializeField, RequiredField] private PresenterHub _presenterPrefab;
        [Inject] private Instantiator _instantiator;

        private Dictionary<SocialProfile, PresenterHub> _presenters = new Dictionary<SocialProfile, PresenterHub>();
        private SwitcherByScreenOrientation _switcherByOrientation;

        public void Initialize(ICollection<SocialProfile> model)
        {
            ClearPresenters(model);
            InitializePresenters(model);
        }

        protected void Awake()
        {
            _switcherByOrientation = GetComponent<SwitcherByScreenOrientation>();
        }

        private void InitializePresenters(ICollection<SocialProfile> profiles)
        {
            foreach (var profile in profiles)
            {
                if (!_presenters.ContainsKey(profile))
                {
                    var hub = _instantiator.Instantiate<PresenterHub>(_presenterPrefab, _switcherByOrientation.Current);
                    _presenters.Add(profile, hub);
                }
                _presenters[profile].Initialize(profile);
            }
        }

        private void ClearPresenters(ICollection<SocialProfile> nextProfiles)
        {
            List<KeyValuePair<SocialProfile, PresenterHub>> _pairs = _presenters.ToList();
            foreach (var pair in _pairs)
            {
                if (pair.Value == null || !nextProfiles.Contains(pair.Key))
                {
                    _presenters[pair.Key].gameObject.Dispose();
                    _presenters.Remove(pair.Key);
                }
            }
        }
    }
}