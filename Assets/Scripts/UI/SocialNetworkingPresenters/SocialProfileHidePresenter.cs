﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory.Rewards;
using DreamTeam.SocialNetworking;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.SocialNetworkingPresenters
{
    public class SocialProfileHidePresenter : MonoBehaviour, IPresenter<SocialProfile>
    {
        [Inject] private SocialProfileInventoryExchangeSystem _exchangeSystem;

        private SocialProfile _profile;

        public void Initialize(SocialProfile model)
        {
            _profile = model;
        }

        protected void OnEnable()
        {
            _exchangeSystem.RewardsAsked += HidePresenter;
            _exchangeSystem.RewardsSent += HidePresenter;
        }

        protected void OnDisable()
        {
            _exchangeSystem.RewardsAsked -= HidePresenter;
            _exchangeSystem.RewardsSent -= HidePresenter;
        }

        private void HidePresenter(SocialProfile profile, List<Reward> rewards)
        {
            if (profile.Equals(_profile))
            {
                gameObject.Dispose();
            }
        }
    }
}