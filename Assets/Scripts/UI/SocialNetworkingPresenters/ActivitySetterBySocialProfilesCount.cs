﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.SocialNetworking;
using UnityEngine;

namespace DreamTeam.UI.SocialNetworkingPresenters
{
    public class ActivitySetterBySocialProfilesCount : MonoBehaviour, IPresenter<ICollection<SocialProfile>>
    {
        [SerializeField] private int _threshold;
        [SerializeField] private ThresholdCondition _activationCondition;

        public void Initialize(ICollection<SocialProfile> model)
        {
            switch (_activationCondition)
            {
                case ThresholdCondition.More:
                    gameObject.SetActive(model.Count > _threshold);
                    break;

                case ThresholdCondition.MoreOrEqual:
                    gameObject.SetActive(model.Count >= _threshold);
                    break;

                case ThresholdCondition.Less:
                    gameObject.SetActive(model.Count < _threshold);
                    break;

                case ThresholdCondition.LessOrEqual:
                    gameObject.SetActive(model.Count <= _threshold);
                    break;
            }
        }

        private enum ThresholdCondition
        {
            More,
            MoreOrEqual,
            Less,
            LessOrEqual
        }
    }
}