﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.SocialNetworking;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.SocialNetworkingPresenters
{
    [RequireComponent(typeof(Toggle))]
    public class SocialProfileSelectionPresenter : MonoBehaviour, IPresenter<SocialProfile>
    {
        public SocialProfile Profile { get; private set; }
        public bool IsSelected => GetComponent<Toggle>().isOn;

        public void Initialize(SocialProfile model)
        {
            Profile = model;
        }
    }
}