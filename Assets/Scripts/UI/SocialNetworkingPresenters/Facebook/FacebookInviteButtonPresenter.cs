﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Localization;
using DreamTeam.Popups;
using DreamTeam.SocialNetworking.Facebook;
using DreamTeam.SocialNetworking.Facebook.Request;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.SocialNetworkingPresenters.Facebook
{
    public class FacebookInviteButtonPresenter : MonoBehaviour
    {
        [Inject] private FacebookManager _facebookManager;
        [Inject] private PopupSystem _popupSystem;
        [Inject] private Popup _popup;
        [Inject] private LocalizationSystem _localizationSystem;

        [SerializeField] private Button _loginButton;
        [SerializeField] private Button _inviteButton;
        [SerializeField, LocalizationKey] private string _inviteMessageTitleKey;
        [SerializeField, LocalizationKey] private string _inviteMessageMessageKey;

        [SerializeField] private SceneReference _missingConnectionPopup;
        [SerializeField, LocalizationKey] private string _missingConnectionPopupTitle;
        [SerializeField, LocalizationKey] private string _missingConnectionPopupMessage;

        protected void OnEnable()
        {
            _inviteButton.onClick.AddListener(OnInviteButtonPressed);
            _loginButton.onClick.AddListener(OnLoginButtonPressed);

            _inviteButton.gameObject.SetActive(_facebookManager.IsLoggedIn);
            _loginButton.gameObject.SetActive(!_facebookManager.IsLoggedIn);
        }

        protected void OnDisable()
        {
            _inviteButton.onClick.RemoveListener(OnInviteButtonPressed);
            _loginButton.onClick.RemoveListener(OnLoginButtonPressed);
        }

        private void OnLoginButtonPressed()
        {
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                StartCoroutine(OnLoginInternal());
            }
            else
            {
                ShowMissingConnectionPopup();
            }
        }

        private void OnInviteButtonPressed()
        {
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                StartCoroutine(OnInviteInternal());
            }
            else
            {
                ShowMissingConnectionPopup();
            }
        }

        private void ShowMissingConnectionPopup()
        {
            var missingConnectionArgs = new InformationPopup.Args()
            {
                TitleKey = _missingConnectionPopupTitle,
                MessageKey = _missingConnectionPopupMessage
            };

            _popupSystem.HideAll(() => _popupSystem.Show(_missingConnectionPopup.Name, missingConnectionArgs));
        }

        private IEnumerator OnLoginInternal()
        {
            _loginButton.interactable = false;
            RequestLogin request = _facebookManager.Login();
            yield return request;

            bool isLoggedIn = false;
            if (request.Result != null)
            {
                if (!string.IsNullOrEmpty(request.Result.Error))
                {
                    UnityEngine.Debug.LogError($"LOGIN {request.Result.Error}");
                }
                else if (request.Result.Cancelled)
                {
                    UnityEngine.Debug.LogError("LOGIN cancel!");
                }
                else
                {
                    UnityEngine.Debug.Log("LOGIN success!");
                    isLoggedIn = true;
                }
            }
            else
            {
                UnityEngine.Debug.LogError("LOGIN result is null!");
            }

            if (isLoggedIn)
            {
                yield return OnInviteInternal();
            }
        }

        private IEnumerator OnInviteInternal()
        {
            _inviteButton.interactable = false;
            RequestInviteFriends request = _facebookManager.InviteFriends(
                    _localizationSystem.Localize(_inviteMessageTitleKey, this),
                    _localizationSystem.Localize(_inviteMessageMessageKey, this),
                    "Invite data"
                );
            yield return request;

            if (request.Result != null)
            {
                if (!string.IsNullOrEmpty(request.Result.Error))
                {
                    UnityEngine.Debug.LogError($"INVITE {request.Result.Error}");
                }
                else if (request.Result.Cancelled)
                {
                    UnityEngine.Debug.LogError("INVITE cancel!");
                }
                else
                {
                    UnityEngine.Debug.Log("INVITE success!");
                }
            }
            else
            {
                UnityEngine.Debug.LogError("INVITE result is null!");
            }

            _popupSystem.Hide(_popup.SceneName);
        }
    }
}