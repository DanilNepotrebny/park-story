﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Localization;
using DreamTeam.SocialNetworking.Facebook;
using DreamTeam.SocialNetworking.Facebook.Request;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.SocialNetworkingPresenters.Facebook
{
    [RequireComponent(typeof(Button))]
    [RequireComponent(typeof(Image))]
    public class FacebookSettingsLoginButtonPresenter : MonoBehaviour
    {
        [SerializeField] private Sprite _loggedInSprite;
        [SerializeField] private Sprite _loggedOutSprite;
        [SerializeField] private Text _buttonText;
        [SerializeField, LocalizationKey] private string _connectedTextID;
        [SerializeField, LocalizationKey] private string _disconnectedTextID;

        [Inject] private FacebookManager _facebookManager;
        [Inject] private LocalizationSystem _localizationSystem;

        private Button _button;
        private Image _buttonImage;

        protected void OnEnable()
        {
            _button = GetComponent<Button>();
            _buttonImage = GetComponent<Image>();

            if (_facebookManager.IsLoggedIn)
            {
                _button.onClick.AddListener(OnLogoutPressed);
            }
            else
            {
                _button.onClick.AddListener(OnLoginPressed);
            }

            UpdateButtonView();
        }

        protected void OnDisable()
        {
            if (_facebookManager.IsLoggedIn)
            {
                _button.onClick.RemoveListener(OnLogoutPressed);
            }
            else
            {
                _button.onClick.RemoveListener(OnLoginPressed);
            }
        }

        private void UpdateButtonView()
        {
            _button.interactable = !_facebookManager.HasProcessingRequests;
            _buttonImage.sprite = _facebookManager.IsLoggedIn ? _loggedInSprite : _loggedOutSprite;
            _buttonText.text = _facebookManager.IsLoggedIn ?
                _localizationSystem.Localize(_connectedTextID, this) :
                _localizationSystem.Localize(_disconnectedTextID, this);
        }

        private void OnLoginPressed()
        {
            StartCoroutine(OnLoginPressedInternal());
        }

        private IEnumerator OnLoginPressedInternal()
        {
            RequestLogin request = _facebookManager.Login();

            UpdateButtonView();

            yield return request;
            if (request.Result != null)
            {
                if (!string.IsNullOrEmpty(request.Result.Error))
                {
                    UnityEngine.Debug.LogError($"LOGIN {request.Result.Error}");
                }
                else if (request.Result.Cancelled)
                {
                    UnityEngine.Debug.LogError("LOGIN cancel!");
                }
                else
                {
                    UnityEngine.Debug.Log("LOGIN success!");
                    
                    _button.onClick.RemoveListener(OnLoginPressed);
                    _button.onClick.AddListener(OnLogoutPressed);
                }
            }
            else
            {
                UnityEngine.Debug.LogError("LOGIN result is null!");
            }

            UpdateButtonView();
        }

        private void OnLogoutPressed()
        {
            StartCoroutine(OnLogoutPressedInternal());
        }
        
        private IEnumerator OnLogoutPressedInternal()
        {
            RequestLogout request = _facebookManager.Logout();

            UpdateButtonView();

            yield return request;
            
            _button.onClick.AddListener(OnLoginPressed);
            _button.onClick.RemoveListener(OnLogoutPressed);
            
            UpdateButtonView();
        }
    }
}