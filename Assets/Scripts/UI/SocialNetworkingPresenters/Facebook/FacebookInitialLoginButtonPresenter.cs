﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.SocialNetworking.Facebook;
using DreamTeam.SocialNetworking.Facebook.Request;
using DreamTeam.UI.SimpleAnimating;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.SocialNetworkingPresenters.Facebook
{
    [RequireComponent(typeof(Button))]
    [RequireComponent(typeof(SimpleAnimation))]
    public class FacebookInitialLoginButtonPresenter : MonoBehaviour
    {
        [Inject] private FacebookManager _facebookManager;

        private Button _button;
        private SimpleAnimation _simpleAnimation;

        protected void OnEnable()
        {
            _button = GetComponent<Button>();
            _simpleAnimation = GetComponent<SimpleAnimation>();

            if (!_facebookManager.IsLoggedIn)
            {
                _button.onClick.AddListener(OnButtonPressed);

                StartCoroutine(UpdateButtonView());
            }            
        }

        protected void OnDisable()
        {
            if (!_facebookManager.IsLoggedIn)
            {
                _button.onClick.RemoveListener(OnButtonPressed);
            }
        }

        private IEnumerator UpdateButtonView()
        {
            while (!_facebookManager.IsLoggedIn)
            {
                _button.interactable = !_facebookManager.HasProcessingRequests;
                yield return null;
            }

            _button.onClick.RemoveListener(OnButtonPressed);
            _simpleAnimation.Play(SimpleAnimationType.Hide);
        }

        private void OnButtonPressed()
        {
            StartCoroutine(OnButtonPressedInternal());
        }

        private IEnumerator OnButtonPressedInternal()
        {
            RequestLogin request = _facebookManager.Login();
            yield return request;
            if (request.Result != null)
            {
                if (!string.IsNullOrEmpty(request.Result.Error))
                {
                    UnityEngine.Debug.LogError($"LOGIN {request.Result.Error}");
                }
                else if (request.Result.Cancelled)
                {
                    UnityEngine.Debug.LogError("LOGIN cancel!");
                }
                else
                {
                    UnityEngine.Debug.Log("LOGIN success!");
                }
            }
            else
            {
                UnityEngine.Debug.LogError("LOGIN result is null!");
            }
        }
    }
}