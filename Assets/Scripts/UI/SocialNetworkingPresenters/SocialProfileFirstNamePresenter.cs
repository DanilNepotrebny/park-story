﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.SocialNetworking;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.SocialNetworkingPresenters
{
    [RequireComponent(typeof(Text))]
    public class SocialProfileFirstNamePresenter : MonoBehaviour, IPresenter<SocialProfile>
    {
        public void Initialize(SocialProfile model)
        {
            GetComponent<Text>().text = model.Name;
        }
    }
}