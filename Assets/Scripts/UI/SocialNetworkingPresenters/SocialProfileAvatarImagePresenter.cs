﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.SocialNetworking;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.SocialNetworkingPresenters
{
    [RequireComponent(typeof(Image))]
    public class SocialProfileAvatarImagePresenter : MonoBehaviour, IPresenter<SocialProfile>
    {
        [SerializeField] private Sprite _noAvatarImage;

        public void Initialize(SocialProfile model)
        {
            GetComponent<Image>().sprite = model.Avatar ?? _noAvatarImage;
        }
    }
}