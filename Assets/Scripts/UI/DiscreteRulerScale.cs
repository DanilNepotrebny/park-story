﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Linq;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.UI
{
    public class DiscreteRulerScale : MonoBehaviour
    {
        [SerializeField] private MarksList _marks = new MarksList();

        public void PutOnScale(int scaleValue, RectTransform target)
        {
            _marks.Sort((A, B) => B.Key.CompareTo(A.Key));
            RectTransform markTransform =
                _marks.FirstOrDefault(item => scaleValue >= item.Key).Value ?? _marks.Last().Value;

            target.SetParent(markTransform, false);
        }

        [Serializable]
        private class MarksList : KeyValueList<int, RectTransform>
        {
        }
    }
}