﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam.UI.CurrentLevelInfoPresenters
{
    [RequireComponent(typeof(Text))]
    public class CurrentLevelNumberPresenter : MonoBehaviour
#if !NO_MATCH3
        ,IPresenter<ICurrentLevelInfo>
#endif
    {
        [SerializeField, LocalizationKey] private string _format = "Level {0}";

        #if !NO_MATCH3

        [Inject] private LocalizationSystem _localizationSystem;

        public void Initialize(ICurrentLevelInfo model)
        {
            int levelNumber = model.CurrentLevelIndex + 1;
            int levelsCount = model.CurrentLevelPack.Levels.Length;

            GetComponent<Text>().text = string.Format(
                _localizationSystem.Localize(_format, this),
                levelNumber,
                levelsCount);
        }

        #endif
    }
}