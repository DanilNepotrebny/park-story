﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Levels;

namespace DreamTeam.UI.CurrentLevelInfoPresenters
{
    public class CurrentLevelInfoLevelSettingsPresenter : PresenterProxy<ICurrentLevelInfo, LevelSettings>
    {
        protected override LevelSettings TargetModel => SourceModel.CurrentLevel;
    }
}