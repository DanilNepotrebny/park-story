﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam.UI.CurrentLevelInfoPresenters
{
    public class CurrentLevelInfoByTypeActivitySetter : MonoBehaviour
    {
        #if !NO_MATCH3
        [SerializeField] private TypeTargetMap _typeTargetMap;

        [Inject] private LevelSystem _levelSystem;

        private void OnEnable()
        {
            foreach (var pair in _typeTargetMap)
            {
                pair.Value.SetActive(pair.Key == _levelSystem.ActiveController.Type);
            }
        }

        [Serializable]
        private class TypeTargetMap : KeyValueList<LevelPackControllerType, GameObject>
        {
        }

        #else
        [SerializeField] private TypeTargetMap _typeTargetMap;

        [Serializable]
        private class TypeTargetMap : KeyValueList<int, GameObject>
        {
        }
        #endif
    }
}