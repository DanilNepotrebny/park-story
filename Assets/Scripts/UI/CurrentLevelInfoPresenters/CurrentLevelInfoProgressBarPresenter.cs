﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam.UI.CurrentLevelInfoPresenters
{
    [RequireComponent(typeof(Slider))]
    public class CurrentLevelInfoProgressBarPresenter : MonoBehaviour
#if !NO_MATCH3
        ,IPresenter<ICurrentLevelInfo>
#endif
    {
        #if !NO_MATCH3

        [Inject] private Popup _popup;

        private ICurrentLevelInfo _levelInfo;

        private Slider _slider => GetComponent<Slider>();

        public void Initialize(ICurrentLevelInfo model)
        {
            _levelInfo = model;
        }

        private void Awake()
        {
            _popup.Shown += OnPopupShown;
        }

        private void OnDestroy()
        {
            _popup.Shown -= OnPopupShown;
        }

        private void OnPopupShown(Popup popup)
        {
            _slider.value = _levelInfo.CurrentLevelIndex;
        }

        #endif
    }
}