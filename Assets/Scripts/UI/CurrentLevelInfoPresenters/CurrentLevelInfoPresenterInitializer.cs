﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam.UI.CurrentLevelInfoPresenters
{
    [RequireComponent(typeof(PresenterHub))]
    public class CurrentLevelInfoPresenterInitializer : MonoBehaviour
    {
        #if !NO_MATCH3

        [Inject] private ICurrentLevelInfo _currentLevelInfo;

        protected void OnEnable()
        {
            GetComponent<PresenterHub>().Initialize(_currentLevelInfo);
        }

        #endif
    }
}