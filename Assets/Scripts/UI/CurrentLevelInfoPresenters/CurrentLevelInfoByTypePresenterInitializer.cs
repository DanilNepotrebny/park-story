// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam.UI.CurrentLevelInfoPresenters
{
    [RequireComponent(typeof(PresenterHub))]
    public class CurrentLevelInfoByTypePresenterInitializer : MonoBehaviour
    {
        #if !NO_MATCH3
        [SerializeField] private LevelPackControllerType _levelPackControllerType;

        [Inject] private LevelSystem _levelSystem;

        /// <summary>
        /// First time should initialize after LevelSystem synced
        /// </summary>
        protected void Start()
        {
            InitializePresenterHub();
        }

        protected void OnEnable()
        {
            InitializePresenterHub();
        }

        private void InitializePresenterHub()
        {
            ICurrentLevelInfo currentLevelInfo;
            if (_levelSystem.FindCurrentLevelInfo(_levelPackControllerType, out currentLevelInfo))
            {
                GetComponent<PresenterHub>().Initialize(currentLevelInfo);
            }
        }
        #else
        [SerializeField] private int _levelPackControllerType;
        #endif
    }
}