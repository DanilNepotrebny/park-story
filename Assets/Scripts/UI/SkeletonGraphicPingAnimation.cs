﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using Spine.Unity;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DreamTeam.UI
{
    public class SkeletonGraphicPingAnimation : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField, RequiredField] private MonoBehaviour _targetGraphic;

        [SerializeField, SpineAnimation(dataField = nameof(_targetGraphic))]
        private string _pingAnimation;

        public void OnPointerClick(PointerEventData eventData)
        {
            var animationStateComponent = _targetGraphic as IAnimationStateComponent;
            if (animationStateComponent == null)
            {
                throw new InvalidOperationException("Target graphic is not a spine animation component.");
            }

            animationStateComponent.AnimationState.SetAnimation(0, _pingAnimation, false);
        }
    }
}