﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.UI.SimpleAnimating;
using UnityEngine;

namespace DreamTeam.UI
{
    [RequireComponent(typeof(PresenterHub), typeof(SimpleAnimation))]
    public abstract class BaseModelHint<TModel> : MonoBehaviour
        where TModel : class
    {
        private TModel _model;
        private TModel _pendingModel;
        private Transform _pendingParent;
        private Transform _initialParent;

        protected bool IsActive { get; private set; }
        private SimpleAnimation _simpleAnimation => GetComponent<SimpleAnimation>();

        public virtual void Show(TModel model, Transform parentTransform)
        {
            if (_model == model)
            {
                return;
            }

            if (_model != null)
            {
                _pendingModel = model;
                _pendingParent = parentTransform;

                Hide();
            }
            else
            {
                ShowWithModel(model, parentTransform);
            }
        }

        public void Show(TModel model)
        {
            Show(model, transform.parent);
        }

        public virtual void Hide()
        {
            if (_model == null)
            {
                return;
            }

            _model = null;

            if (_simpleAnimation.isActiveAndEnabled)
            {
                _simpleAnimation.Play(SimpleAnimationType.Hide);
            }
            else
            {
                OnAnimationStopped(SimpleAnimationType.Hide);
            }

            transform.SetParent(_initialParent);
        }

        protected void Awake()
        {
            _initialParent = transform.parent;
            _simpleAnimation.AnimationStopped += OnAnimationStopped;
            SetChidrenActive(false);
        }

        private void OnAnimationStopped(SimpleAnimationType animationType)
        {
            if (animationType == SimpleAnimationType.Hide)
            {
                SetChidrenActive(false);

                if (_pendingModel != null)
                {
                    ShowWithModel(_pendingModel, _pendingParent);

                    _pendingModel = null;
                }
            }
        }

        private void ShowWithModel(TModel model, Transform parentTransform)
        {
            _model = model;
            GetComponent<PresenterHub>()?.Initialize(_model);

            transform.SetParent(parentTransform);
            transform.position = parentTransform.position;

            StartCoroutine(PlayShowAnimation());
        }

        private IEnumerator PlayShowAnimation()
        {
            _simpleAnimation.Play(SimpleAnimationType.Show);
            yield return null;
            SetChidrenActive(true);
        }

        private void SetChidrenActive(bool isActive)
        {
            IsActive = isActive;

            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(isActive);
            }
        }
    }
}