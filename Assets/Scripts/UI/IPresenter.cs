﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.UI
{
    /// <summary>
    /// Base interface for presenters
    /// </summary>
    public interface IPresenter<TModel> where TModel : class
    {
        void Initialize(TModel model);
    }
}