﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Localization;
using TMPro;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.ProductPackPresenters
{
    [RequireComponent(typeof(TMP_Text))]
    public class ProductPackNamePresenterTMP : MonoBehaviour, IPresenter<ProductPack>
    {
        [SerializeField]
        private string _textFormat = "{0}";

        [Inject] private LocalizationSystem _localizationSystem;

        public void Initialize(ProductPack model)
        {
            TMP_Text textComponent = GetComponent<TMP_Text>();
            textComponent.text = string.Format(_textFormat, _localizationSystem.Localize(model.Name, this));
            textComponent.ForceMeshUpdate();
        }
    }
}