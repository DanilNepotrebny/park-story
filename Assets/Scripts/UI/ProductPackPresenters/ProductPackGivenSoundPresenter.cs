﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Audio;
using DreamTeam.Inventory;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.ProductPackPresenters
{
    public class ProductPackGivenSoundPresenter : MonoBehaviour, IPresenter<ProductPack>
    {
        [SerializeField, RequiredField] private AudioSystemPlayer _soundPlayer;

        [Inject] private ProductPackInstanceContainer _packInstanceContainer;

        private ProductPack.Instance _packInstance;

        public void Initialize(ProductPack model)
        {
            UnsubscribePack();
            _packInstance = _packInstanceContainer.GetInstance(model);
            SubscribePack();
        }

        protected void OnEnable()
        {
            SubscribePack();
        }

        protected void OnDisable()
        {
            UnsubscribePack();
        }

        private void SubscribePack()
        {
            if (_packInstance != null)
            {
                _packInstance.Given += OnPackGiven;
            }
        }

        private void UnsubscribePack()
        {
            if (_packInstance != null)
            {
                _packInstance.Given -= OnPackGiven;
            }
        }

        private void OnPackGiven(ProductPack.Instance pack, GamePlace gamePlace)
        {
            _soundPlayer.Play();
        }
    }
}