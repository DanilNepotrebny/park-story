﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.UI.ProductPackPresenters
{
    [CreateAssetMenu]
    public class ProductPackPresentersFactory : ViewFactory<ProductPack, ProductPackPresentersFactory.ModelRefType, ProductPackPresentersFactory.PresentersMap>
    {
        [Serializable]
        public class ModelRefType : TypeReference<ProductPack>
        {
        }

        [Serializable]
        public class PresentersMap : KeyValueList<ModelRefType, GameObject>
        {
        }
    }
}