﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.ProductPackPresenters
{
    [RequireComponent(typeof(Button))]
    public class ProductPackBuyButton : MonoBehaviour, IPresenter<ProductPack>
    {
        [SerializeField] private GamePlace _gamePlace;

        [Inject] private ProductPackInstanceContainer _productPackInstanceContainer;

        private ProductPack _productPack;

        private Button _button => GetComponent<Button>();

        public void Initialize(ProductPack model)
        {
            _productPack = model;
        }

        protected void Awake()
        {
            _button.onClick.AddListener(OnButtonClicked);
        }

        protected void OnDestroy()
        {
            _button.onClick.RemoveListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            _productPackInstanceContainer.GetInstance(_productPack)?.TryPurchase(_gamePlace);
        }
    }
}