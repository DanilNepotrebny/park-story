﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.UI.ProductPackPresenters
{
    public class ProductPackHintController : BaseHintController<ProductPack>
    {
        [SerializeField, RequiredField] private ProductPackHint _hint;

        protected override BaseModelHint<ProductPack> Hint => _hint;
    }
}