﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Inventory.Prices;

namespace DreamTeam.UI.ProductPackPresenters
{
    public class ProductPackPricePresenter : PresenterProxy<ProductPack, Price>
    {
        protected override Price TargetModel => SourceModel.Price;
    }
}