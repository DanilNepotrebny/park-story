﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;

namespace DreamTeam.UI.ProductPackPresenters
{
    public class ProductPackHint : BaseModelHint<ProductPack>
    {
    }
}