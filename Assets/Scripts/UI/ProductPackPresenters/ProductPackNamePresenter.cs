﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Localization;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.ProductPackPresenters
{
    [RequireComponent(typeof(Text))]
    public class ProductPackNamePresenter : MonoBehaviour, IPresenter<ProductPack>
    {
        [SerializeField]
        private string _textFormat = "{0}";

        [Inject] private LocalizationSystem _localizationSystem;

        public void Initialize(ProductPack model)
        {
            GetComponent<Text>().text = string.Format(_textFormat, _localizationSystem.Localize(model.Name, this));
        }
    }
}