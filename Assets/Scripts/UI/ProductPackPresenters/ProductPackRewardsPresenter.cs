﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;

namespace DreamTeam.UI.ProductPackPresenters
{
    public class ProductPackRewardsPresenter : PresenterProxy<ProductPack, ICollection<Reward>>
    {
        protected override ICollection<Reward> TargetModel => SourceModel.Rewards;
    }
}
