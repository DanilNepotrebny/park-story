﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.ProductPackPresenters
{
    [RequireComponent(typeof(Image))]
    public class ProductPackIconImagePresenter : MonoBehaviour, IPresenter<ProductPack>
    {
        public void Initialize(ProductPack model)
        {
            GetComponent<Image>().sprite = model.Icon;
        }
    }
}