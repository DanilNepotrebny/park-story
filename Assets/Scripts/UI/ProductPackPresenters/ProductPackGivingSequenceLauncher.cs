﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.UI.RewardPresentationSequences;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.UI.ProductPackPresenters
{
    public class ProductPackGivingSequenceLauncher : MonoBehaviour, IPresenter<ProductPack>
    {
        [SerializeField, RequiredField] private RewardPresentationSequenceBase _presentationSequence;

        [Inject] private ProductPackInstanceContainer _productPackInstanceContainer;

        private ProductPack _productPack;
        private ProductPack.Instance _productPackInstance;

        public void Initialize(ProductPack model)
        {
            UnsubscribeProductPack();
            _productPack = model;
            _productPackInstance = _productPackInstanceContainer.GetInstance(model);
            SubscribeProductPack();
        }

        protected void OnDestroy()
        {
            UnsubscribeProductPack();
        }

        private void SubscribeProductPack()
        {
            if (_productPackInstance != null)
            {
                _productPackInstance.Giving += OnProductPackGiving;
            }
        }

        private void UnsubscribeProductPack()
        {
            if (_productPackInstance != null)
            {
                _productPackInstance.Giving -= OnProductPackGiving;
            }
        }

        private void OnProductPackGiving(ProductPack.Instance pack, IDeferredInvocationHandle givingHandle)
        {
            _presentationSequence.Present(_productPack.Rewards, givingHandle.Lock());
        }
    }
}