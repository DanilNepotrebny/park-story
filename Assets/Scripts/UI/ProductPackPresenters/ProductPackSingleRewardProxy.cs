﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;
using UnityEngine;

namespace DreamTeam.UI.ProductPackPresenters
{
    public class ProductPackSingleRewardProxy : PresenterProxy<ProductPack, Reward>
    {
        [SerializeField] private int _rewardIndex = 0;

        protected override Reward TargetModel => SourceModel.Rewards[_rewardIndex];
    }
}