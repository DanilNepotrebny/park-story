﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.ProductPackPresenters
{
    [RequireComponent(typeof(Image))]
    public class ProductPackBestPriceIconPresenter : MonoBehaviour, IPresenter<ProductPack>
    {
        [SerializeField] private BankConfig _config;

        public void Initialize(ProductPack model)
        {
            GetComponent<Image>().enabled = _config.BestValuePack == model;
        }
    }
}