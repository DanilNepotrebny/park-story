﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;

#if !NO_MATCH3
using DreamTeam.Match3.Levels.Rewards;
#else
using UnityEngine;
#endif

namespace DreamTeam.UI.LevelPackRewardPresenters.LevelPackRewardInfoPresenters
{
    #if !NO_MATCH3
    public class LevelPackRewardProductPackPresenter :
        PresenterProxy<ILevelPackRewardInfo, ProductPack>
    {
        protected override ProductPack TargetModel => SourceModel.ProductPack;
    }
    #else
    public class LevelPackRewardProductPackPresenter : MonoBehaviour {}
    #endif
}