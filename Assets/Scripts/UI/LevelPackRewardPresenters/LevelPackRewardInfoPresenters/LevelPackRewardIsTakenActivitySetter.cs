﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

#if !NO_MATCH3
using DreamTeam.Match3.Levels.Rewards;
#endif

namespace DreamTeam.UI.LevelPackRewardPresenters.LevelPackRewardInfoPresenters
{
    public class LevelPackRewardIsTakenActivitySetter : MonoBehaviour
#if !NO_MATCH3
        ,IPresenter<ILevelPackRewardInfo>
#endif
    {
        #if !NO_MATCH3

        private ILevelPackRewardInfo _reward;

        public void Initialize(ILevelPackRewardInfo model)
        {
            UnsubscrubeReward();
            _reward = model;
            SubscribeReward();
            UpdateView(_reward);
        }

        protected void OnDestroy()
        {
            UnsubscrubeReward();
        }

        private void SubscribeReward()
        {
            if (_reward != null)
            {
                _reward.Taken += UpdateView;
            }
        }

        private void UnsubscrubeReward()
        {
            if (_reward != null)
            {
                _reward.Taken -= UpdateView;
            }
        }

        private void UpdateView(ILevelPackRewardInfo reward)
        {
            gameObject.SetActive(!reward.IsTaken);
        }

        #endif
    }
}