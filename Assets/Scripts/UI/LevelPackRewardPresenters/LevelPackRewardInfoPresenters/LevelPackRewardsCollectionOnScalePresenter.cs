﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels.Rewards;
#endif

namespace DreamTeam.UI.LevelPackRewardPresenters.LevelPackRewardInfoPresenters
{
    [RequireComponent(typeof(DiscreteRulerScale))]
    public class LevelPackRewardsCollectionOnScalePresenter : MonoBehaviour
#if !NO_MATCH3
        ,IPresenter<IReadOnlyCollection<ILevelPackRewardInfo>>
#endif
    {
        [SerializeField, RequiredField] private PresenterHub _presenterPrefab;

        #if !NO_MATCH3

        [Inject] private Instantiator _instantiator;

        private Dictionary<ILevelPackRewardInfo, PresenterHub> _presenters =
            new Dictionary<ILevelPackRewardInfo, PresenterHub>();

        private DiscreteRulerScale _scale => GetComponent<DiscreteRulerScale>();

        public void Initialize(IReadOnlyCollection<ILevelPackRewardInfo> model)
        {
            ClearRemovedPresenters(model);
            CreateOrUpdatePresenters(model);
        }

        private void CreateOrUpdatePresenters(IReadOnlyCollection<ILevelPackRewardInfo> rewards)
        {
            foreach (ILevelPackRewardInfo rewardInfo in rewards)
            {
                PresenterHub presenter;
                if (!_presenters.TryGetValue(rewardInfo, out presenter))
                {
                    presenter = _instantiator.Instantiate(_presenterPrefab);
                    _presenters.Add(rewardInfo, presenter);
                }

                _scale.PutOnScale(rewardInfo.LevelNumber, presenter.transform as RectTransform);
                presenter.Initialize(rewardInfo);
            }
        }

        private void ClearRemovedPresenters(IReadOnlyCollection<ILevelPackRewardInfo> rewards)
        {
            var presentedRewards = new List<ILevelPackRewardInfo>(_presenters.Keys);
            foreach (ILevelPackRewardInfo info in presentedRewards)
            {
                if (!rewards.Contains(info))
                {
                    _presenters[info].gameObject.Dispose();
                    _presenters.Remove(info);
                }
            }
        }

        #endif
    }
}