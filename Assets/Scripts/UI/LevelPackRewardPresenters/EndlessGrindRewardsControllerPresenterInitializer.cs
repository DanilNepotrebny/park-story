﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels.Rewards;
#endif

namespace DreamTeam.UI.LevelPackRewardPresenters
{
    [RequireComponent(typeof(PresenterHub))]
    public class EndlessGrindRewardsControllerPresenterInitializer : MonoBehaviour
    {
        #if !NO_MATCH3

        [Inject] private EndlessGrindLevelPackRewardsController _rewardsController;

        protected void OnEnable()
        {
            GetComponent<PresenterHub>().Initialize(_rewardsController);
        }

        #endif
    }
}