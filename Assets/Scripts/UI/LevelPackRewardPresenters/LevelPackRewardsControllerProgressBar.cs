﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels.Rewards;
#endif

namespace DreamTeam.UI.LevelPackRewardPresenters
{
    public class LevelPackRewardsControllerProgressBar : MonoBehaviour
#if !NO_MATCH3
        ,IPresenter<ILevelPackRewardsController>
#endif
    {
        [SerializeField, RequiredField] private Slider _slider;

        #if !NO_MATCH3

        [Inject] private Popup _popup;

        private ILevelPackRewardsController _rewardsController;

        public void Initialize(ILevelPackRewardsController model)
        {
            _rewardsController = model;
            _slider.value = _rewardsController.LastDisplayedLevelIndex;
        }

        protected void Awake()
        {
            _popup.Shown += OnPopupShown;
        }

        protected void OnDestroy()
        {
            _popup.Shown -= OnPopupShown;
        }

        private void OnPopupShown(Popup popup)
        {
            _rewardsController.UpdateLastDisplayedLevelIndex();
            _slider.value = _rewardsController.LastDisplayedLevelIndex;
        }

        #endif
    }
}