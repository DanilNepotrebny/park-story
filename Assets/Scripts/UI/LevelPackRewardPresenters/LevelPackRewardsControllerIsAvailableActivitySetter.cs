﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

#if !NO_MATCH3
using DreamTeam.Match3.Levels.Rewards;
#endif

namespace DreamTeam.UI.LevelPackRewardPresenters
{
    #if !NO_MATCH3
    public class LevelPackRewardsControllerIsAvailableActivitySetter : MonoBehaviour,
        IPresenter<ILevelPackRewardsController>
    {
        public void Initialize(ILevelPackRewardsController model)
        {
            gameObject.SetActive(model.IsRewardsAwailable);
        }
    }
    #else
    public class LevelPackRewardsControllerIsAvailableActivitySetter : MonoBehaviour {}
    #endif
}