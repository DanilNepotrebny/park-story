﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;

#if !NO_MATCH3
using DreamTeam.Match3.Levels.Rewards;
#else
using UnityEngine;
#endif

namespace DreamTeam.UI.LevelPackRewardPresenters
{
    #if !NO_MATCH3
    public class LevelPackRewardsControllerAllRewardsPresenter :
        PresenterProxy<ILevelPackRewardsController, IReadOnlyCollection<ILevelPackRewardInfo>>
    {
        protected override IReadOnlyCollection<ILevelPackRewardInfo> TargetModel => SourceModel.AllRewards;
    }
    #else
    public class LevelPackRewardsControllerAllRewardsPresenter : MonoBehaviour {}
    #endif
}