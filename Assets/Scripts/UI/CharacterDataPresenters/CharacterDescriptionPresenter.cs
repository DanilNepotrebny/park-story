﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.CharacterDataPresenters
{
    [RequireComponent(typeof(Text))]
    public class CharacterDescriptionPresenter : MonoBehaviour, IPresenter<CharacterData>
    {
        [Inject] private LocalizationSystem _localizationSystem;

        public void Initialize(CharacterData model)
        {
            Assert.IsNotNull(model);

            GetComponent<Text>().text = _localizationSystem.Localize(model.Description, this);
        }
    }
}