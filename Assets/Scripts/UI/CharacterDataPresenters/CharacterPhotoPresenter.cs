﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace DreamTeam.UI.CharacterDataPresenters
{
    [RequireComponent(typeof(Image))]
    public class CharacterPhotoPresenter : MonoBehaviour, IPresenter<CharacterData>
    {
        public void Initialize(CharacterData model)
        {
            Assert.IsNotNull(model);

            GetComponent<Image>().sprite = model.Photo;
        }
    }
}