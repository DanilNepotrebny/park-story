﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.SceneLoading;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.UI.Loading
{
    [RequireComponent(typeof(Button))]
    public class GameLoadingPlayButton : MonoBehaviour
    {
        [Inject] private GameLoader _gameLoader;

        private Button _playButton;

        protected void Awake()
        {
            _playButton = GetComponent<Button>();
            _playButton.interactable = false;
            _playButton.onClick.AddListener(OnPlayButtonClicked);

            _gameLoader.OnMetagameLoadingComplete += OnMetagameLoadingComplete;
        }

        protected void OnDestroy()
        {
            _playButton.onClick.RemoveListener(OnPlayButtonClicked);
            _gameLoader.OnMetagameLoadingComplete -= OnMetagameLoadingComplete;
        }

        private void OnMetagameLoadingComplete()
        {
            _playButton.interactable = true;
        }

        private void OnPlayButtonClicked()
        {
            _gameLoader.PlayButtonPressed();
        }
    }
}