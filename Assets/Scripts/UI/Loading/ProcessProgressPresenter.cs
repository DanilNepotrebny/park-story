﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Utils;
using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.Loading
{
    [RequireComponent(typeof(Slider))]
    public class ProcessProgressPresenter : MonoBehaviour, IPresenter<BaseAsyncProcess>
    {
        [SerializeField, MinMaxRange(0.06f, 1f)] private MinMaxRange _lerpRange;

        private Slider _slider;

        public void Initialize(BaseAsyncProcess model)
        {
            StartCoroutine(UpdateProgress(model));
        }

        protected void Awake()
        {
            _slider = GetComponent<Slider>();
        }

        private IEnumerator UpdateProgress(BaseAsyncProcess process)
        {
            do
            {
                SetProgress(process.Progress);
                yield return null;
            }
            while (!process.IsCompleted);

            SetProgress(1f);
        }

        private void SetProgress(float normalizedProgress)
        {
            _slider.normalizedValue = Mathf.Lerp(_lerpRange.rangeStart, _lerpRange.rangeEnd, normalizedProgress);
        }
    }
}