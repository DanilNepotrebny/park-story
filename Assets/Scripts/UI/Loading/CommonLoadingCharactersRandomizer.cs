﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.UI.Loading
{
    public class CommonLoadingCharactersRandomizer : MonoBehaviour
    {
        [SerializeField] private Image _charactersImage;
        [SerializeField] private List<Sprite> _characters = new List<Sprite>();

        private void OnEnable()
        {
            if (_charactersImage != null && _characters.Count > 0)
            {
                _charactersImage.sprite = _characters[Random.Range(0, _characters.Count - 1)];
            }
        }
    }
}