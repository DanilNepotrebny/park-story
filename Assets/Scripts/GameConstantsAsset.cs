﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam
{
    [CreateAssetMenu(fileName = "GameConstants", menuName = "DreamTeam/Game constants")]
    public class GameConstantsAsset : ScriptableObject
    {
        [SerializeField] private string _gameWebURL;
        [SerializeField] private string _privacyPolicyWebURLPostfix;
        [SerializeField] private string _termsOfUsageWebURLPostfix;

        public string PrivacyPolicyURL => $"{_gameWebURL}/{_privacyPolicyWebURLPostfix}";
        public string TermsOfUsageURL => $"{_gameWebURL}/{_termsOfUsageWebURLPostfix}";
    }
}