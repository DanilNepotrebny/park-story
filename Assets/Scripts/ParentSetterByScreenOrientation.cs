﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.TransformLookup;
using DreamTeam.Utils;
using Orientation = DreamTeam.Utils.ScreenOrientationNotifier.Orientation;
using System;
using UnityEngine;
using Zenject;

namespace DreamTeam
{
    public class ParentSetterByScreenOrientation : MonoBehaviour, IInitable, IDisposable
    {
        [SerializeField] private Transform _target;
        [SerializeField] private TransformationOptions _landscapeOptions;
        [SerializeField] private TransformationOptions _portraitOptions;

        [Inject] private TransformLookupSystem _transformLookupSystem;
        [Inject] private ScreenOrientationNotifier _notifier;

        public void Init()
        {
            _notifier.OrientationChanged += ApplyParentTransformId;
        }

        public void Dispose()
        {
            _notifier.OrientationChanged -= ApplyParentTransformId;

            if (_target != null)
            {
                _target.gameObject.Dispose();
            }
        }

        protected void Start()
        {
            ApplyParentTransformId(_notifier.CurrentOrientation);
        }

        private void ApplyParentTransformId(Orientation orientation)
        {
            switch (orientation)
            {
                case Orientation.Landscape:
                    _landscapeOptions.Apply(_transformLookupSystem, _target);
                    break;

                case Orientation.Portrait:
                    _portraitOptions.Apply(_transformLookupSystem, _target);
                    break;
            }
        }

        [Serializable]
        private class TransformationOptions
        {
            [SerializeField, RequiredField] private TransformLookupId _parentId;
            [SerializeField] private bool _worldPositionStays = true;
            [SerializeField] private int _siblingIndex = int.MaxValue;

            public void Apply(TransformLookupSystem transformLookupSystem, Transform target)
            {
                Transform parent = transformLookupSystem.FindTransform(_parentId);
                if (parent != null)
                {
                    target.SetParent(parent, _worldPositionStays);
                    target.SetSiblingIndex(_siblingIndex);
                }
            }
        }
    }
}