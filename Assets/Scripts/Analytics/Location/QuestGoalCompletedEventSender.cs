﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Analytics.Events.Location;
using DreamTeam.Quests;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam.Analytics.Location
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class QuestGoalCompletedEventSender : MonoBehaviour, ISynchronizable, IInitable
    {
        private int _completedGoalsPerLevel;

        #if !NO_MATCH3

        [Inject] private QuestManager _questManager;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private SaveGroup _saveGroup;

        [Inject(Id = GlobalInstaller.MainLevelPackControllerId)]
        private SingleLevelPackController _mainLevelPackController;

        [Inject] private Instantiator _instantiator;

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("QuestGoalStartedEventSender", this);
        }

        protected void Start()
        {
            _mainLevelPackController.CurrentLevelAdvanced += OnCurrentLevelChanged;

            foreach (QuestPack pack in _questManager.QuestPacks)
            {
                foreach (Quest quest in pack.Quests)
                {
                    foreach (Goal goal in quest.Goals)
                    {
                        goal.Started += () => OnGoalStarted(quest, goal);
                    }
                }
            }
        }

        private void OnCurrentLevelChanged(int previousindex, int currentindex)
        {
            _completedGoalsPerLevel = 0;
        }

        private void OnGoalStarted(Quest quest, Goal goal)
        {
            _completedGoalsPerLevel++;

            var goalEvent = _instantiator.Instantiate<QuestGoalCompletedEvent>(
                    quest,
                    goal,
                    _completedGoalsPerLevel
                );

            _analyticsSystem.Send(goalEvent);
        }

        #endif

        void ISynchronizable.Sync(State state)
        {
            state.SyncInt("CompletedGoalsPerLevel", ref _completedGoalsPerLevel);
        }
    }
}