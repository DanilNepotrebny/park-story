﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Reflection;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Analytics
{
    public class EventSender : MonoBehaviour
    {
        [SerializeField] private EventTypeReference _eventTypeReference;
        [SerializeField] private bool _sendOnEnable;

        [Inject] private Instantiator _instantiator;
        [Inject] private AnalyticsSystem _analyticsSystem;

        private bool _wasStarted;

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void SendEvent()
        {
            IAnalyticsEvent e = _instantiator.InstantiateExplicit(_eventTypeReference.Type) as IAnalyticsEvent;
            if (e != null)
            {
                _analyticsSystem.Send(e);
            }
            else
            {
                UnityEngine.Debug.LogError($"Couldn't create event for {name}", this);
            }
        }

        protected void OnEnable()
        {
            if (_sendOnEnable && _wasStarted)
            {
                SendEvent();
            }
        }

        protected void Start()
        {
            if (_sendOnEnable)
            {
                SendEvent();

                _wasStarted = true;
            }
        }

        #region Inner Types

        [Serializable]
        private class EventTypeReference : TypeReference<IAnalyticsEvent>
        {
        }

        #endregion
    }
}
