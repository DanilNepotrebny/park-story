﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Reflection;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Rewards;
using DreamTeam.SocialNetworking;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Analytics
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class LivesMessageEventSender : MonoBehaviour
    {
        [Inject] private SocialProfileInventoryExchangeSystem _exchangeSystem;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private Instantiator _instantiator;
        [Inject] private Lives _lives;

        protected void Awake()
        {
            _exchangeSystem.RewardsAsked += OnRewardsAsked;
            _exchangeSystem.RewardsSent += OnRewardsSent;
        }

        private void OnRewardsSent(SocialProfile profile, List<Reward> rewards)
        {
            if (!AreRewardsValid(rewards))
            {
                return;
            }

            var loginEvent = _instantiator.Instantiate<LivesSentEvent>();
            _analyticsSystem.Send(loginEvent);
        }

        private void OnRewardsAsked(SocialProfile profile, List<Reward> rewards)
        {
            if (!AreRewardsValid(rewards))
            {
                return;
            }

            var loginEvent = _instantiator.Instantiate<LivesAskedEvent>();
            _analyticsSystem.Send(loginEvent);
        }

        private bool AreRewardsValid(List<Reward> rewards)
        {
            if (rewards.Count != 1)
            {
                return false;
            }

            var livesReward = rewards[0] as CountedItemFixedReward;
            if (livesReward == null ||
                livesReward.Item != _lives)
            {
                return false;
            }

            return true;
        }
    }
}