﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

namespace DreamTeam.Analytics.Cloud
{
    public abstract class BaseAnalyticsWrapper : MonoBehaviour
    {
        [System.NonSerialized] private AnalyticsSystem _analyticsSystem;

        [Inject] private void Construct(AnalyticsSystem analyticsSystem)
        {
            _analyticsSystem = analyticsSystem;
            _analyticsSystem.UserIdInitialized += OnUserIdInitialized;
        }

        private void OnUserIdInitialized()
        {
            _analyticsSystem.UserIdInitialized -= OnUserIdInitialized;

            #if !UNITY_EDITOR
            {
                Initialize();
            }
            #endif
        }
        
        protected abstract void Initialize();

        protected void LogInfo(string message)
        {
            UnityEngine.Debug.Log(message, this);
        }
    }
}