﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.SocialNetworking;
using DreamTeam.SocialNetworking.Facebook;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Analytics
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class SocialNetworkLoginEventSender : MonoBehaviour
    {
        [Inject] private FacebookManager _facebookManager;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private Instantiator _instantiator;

        protected void Awake()
        {
            _facebookManager.LoginFinished += OnFacebookLogin;
        }

        private void OnFacebookLogin()
        {
            var loginEvent = _instantiator.Instantiate<SocialNetworkLoginEvent>(SocialNetworkType.Facebook);
            _analyticsSystem.Send(loginEvent);
        }
    }
}