﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Inventory.Items;
using Zenject;

namespace DreamTeam.Analytics.Events.Global
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class BankOpenedEvent : CustomEventWithMainLevelPack
    {
        public override string EventName => "ce_bank_open";

        public BankOpenedEvent(
                [Inject(Id = GlobalInstaller.MoneyId)] CountedItem money
            )
        {
            AddInt("coins", money.Count);
        }
    }
}