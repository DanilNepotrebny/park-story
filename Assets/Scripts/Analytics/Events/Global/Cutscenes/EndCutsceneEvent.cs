﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;

namespace DreamTeam.Analytics.Events.Global.Cutscenes
{
    public class EndCutsceneEvent : BaseCutsceneEvent
    {
        public override string EventName => "ce_cutscene_end";

        public EndCutsceneEvent(SceneReference scene) : base(scene)
        {
        }
    }
}