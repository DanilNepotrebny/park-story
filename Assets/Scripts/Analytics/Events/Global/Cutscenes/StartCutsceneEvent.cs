﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;

namespace DreamTeam.Analytics.Events.Global.Cutscenes
{
    public class StartCutsceneEvent : BaseCutsceneEvent
    {
        public override string EventName => "ce_cutscene_start";

        public StartCutsceneEvent(SceneReference scene) : base(scene)
        {
        }
    }
}