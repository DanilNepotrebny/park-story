﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.SocialNetworking;

namespace DreamTeam.Analytics.Events.Global
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class SocialNetworkLoginEvent : CustomEventWithMainLevelPack
    {
        public override string EventName => "ce_social_login";

        public SocialNetworkType NetworkType { get; }

        public SocialNetworkLoginEvent(SocialNetworkType networkType)
        {
            NetworkType = networkType;
            AddString("network", networkType.ToAnalyticsId());
        }

        #region Inner types

        public enum SocialNetwork
        {
            Facebook
        }

        #endregion
    }
}