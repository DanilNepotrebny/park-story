﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Popups;

namespace DreamTeam.Analytics.Events.Global
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class InGameRateEvent : CustomEventWithMainLevelPack
    {
        public override string EventName => "ce_rate_ingame";

        public InGameRateEvent(RateResult rateResult)
        {
            AddString("result", rateResult.ToAnalyticsId());
        }
    }
}