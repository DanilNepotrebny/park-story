﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Inventory.Items;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Analytics.Events.Global
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class ApplicationLaunchedEvent : CustomEventWithCurrentLevelInfo
    {
        public override string EventName => "ce_app_launch";

        public ApplicationLaunchedEvent(
                [Inject(Id = GlobalInstaller.MoneyId)] CountedItem money,
                [Inject(Id = GlobalInstaller.StarsId)] CountedItem stars,
                [Inject(Id = ProjectInstaller.SoundParameterId)] AudioMixerExposedParameter soundParameter,
                [Inject(Id = ProjectInstaller.MusicParameterId)] AudioMixerExposedParameter musicParameter
            )
        {
            AddString("internet_mode", GetInternetMode());
            AddInt("stars", stars.Count);
            AddInt("coins", money.Count);
            AddBool("sound", soundParameter.NormalizedValue > 0);
            AddBool("music", musicParameter.NormalizedValue > 0);
        }

        private string GetInternetMode()
        {
            switch (Application.internetReachability)
            {
                case NetworkReachability.ReachableViaCarrierDataNetwork:
                    return "3g/4g/5g";
                case NetworkReachability.ReachableViaLocalAreaNetwork:
                    return "wi-fi";
            }
            return "none";
        }
    }
}