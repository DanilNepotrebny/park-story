﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Prices;
using Zenject;

namespace DreamTeam.Analytics.Events.Global
{
    public class IngameProductPackPurchaseOfferedEvent : BaseIngameProductPackPurchaseEvent
    {
        public override string EventName => "ce_purchase_offered";

        public IngameProductPackPurchaseOfferedEvent(
                CountedItemPrice price,
                ProductPack productPack,
                GamePlace gamePlace,
                [Inject] Lives lives
            ) :
            base(price, productPack, gamePlace, lives)
        {

        }
    }
}