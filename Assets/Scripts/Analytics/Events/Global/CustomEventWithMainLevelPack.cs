﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.Levels;
using Zenject;

namespace DreamTeam.Analytics.Events.Global
{
    public abstract class CustomEventWithMainLevelPack : BaseCustomEvent
    {
        #if !NO_MATCH3

        public ICurrentLevelInfo CurrentLevelInfo { get; private set; }

        [Inject]
        public void Construct(
                [Inject(Id = GlobalInstaller.MainLevelPackControllerId)] SingleLevelPackController mainPackController
            )
        {
            CurrentLevelInfo = mainPackController;
            AddString("level", CurrentLevelInfo.CurrentLevelId);
        }

        #endif
    }
}