﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Inventory;

namespace DreamTeam.Analytics.Events.Global
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class StoreRateEvent : CustomEventWithMainLevelPack
    {
        public override string EventName => "ce_rate";

        public StoreRateEvent(GamePlace gamePlace)
        {
            AddString("game_place", gamePlace.ToAnalyticsId());
        }
    }
}