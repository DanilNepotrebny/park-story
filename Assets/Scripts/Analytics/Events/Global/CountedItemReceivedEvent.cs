﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;

namespace DreamTeam.Analytics.Events.Global
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class CountedItemReceivedEvent : CustomEventWithMainLevelPack
    {
        public override string EventName => "ce_items_recieved";

        public CountedItem Item { get; }
        public int Count { get; }
        public ReceivingType ReceivingType { get; }

        public CountedItemReceivedEvent(
                CountedItem item,
                int count,
                GamePlace gamePlace,
                ReceivingType receivingType
            )
        {
            Item = item;
            Count = count;
            ReceivingType = receivingType;

            AddString("item", item.Id);
            AddInt("count", count);
            AddString("game_place", gamePlace.ToAnalyticsId());
            AddString("receiving_type", receivingType.ToAnalyticsId());
        }
    }
}