﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Analytics.Events.Global.Tutorial
{
    [CreateAssetMenu]
    public class TutorialStep : ScriptableObject
    {
        [SerializeField] private int _stepIndex;

        public int StepIndex => _stepIndex;

        public string Id => name;
    }
}