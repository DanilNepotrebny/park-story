﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;

namespace DreamTeam.Analytics.Events.Global.Tutorial
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class TutorialEvent : IAnalyticsEvent
    {
        public TutorialStep TutorialStep { get; }

        public TutorialEvent(TutorialStep tutorialStep)
        {
            TutorialStep = tutorialStep;
        }
    }
}