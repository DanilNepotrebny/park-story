﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Prices;
using Zenject;

namespace DreamTeam.Analytics.Events.Global
{
    public class IngameProductPackPurchaseEvent : BaseIngameProductPackPurchaseEvent
    {
        public override string EventName => "ce_items_purchased";

        public CountedItemPrice Price { get; }
        public ProductPack ProductPack { get; }

        public IngameProductPackPurchaseEvent(
                CountedItemPrice price,
                ProductPack productPack,
                GamePlace gamePlace,
                [Inject] Lives lives
            ) :
            base(price, productPack, gamePlace, lives)
        {
            Price = price;
            ProductPack = productPack;
        }
    }
}