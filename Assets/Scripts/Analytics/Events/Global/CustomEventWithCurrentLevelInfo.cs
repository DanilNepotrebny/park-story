﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam.Analytics.Events.Global
{
    public abstract class CustomEventWithCurrentLevelInfo : BaseCustomEvent
    {
        #if !NO_MATCH3

        [Inject]
        private void Construct(ICurrentLevelInfo currentLevelInfo)
        {
            AddString("level", currentLevelInfo.CurrentLevelId);
        }

        #endif
    }
}