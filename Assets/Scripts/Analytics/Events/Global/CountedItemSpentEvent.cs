﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Inventory.Items;

namespace DreamTeam.Analytics.Events.Global
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class CountedItemSpentEvent : IAnalyticsEvent
    {
        public CountedItem Item { get; }
        public int Count { get; }

        public CountedItemSpentEvent(CountedItem item, int count)
        {
            Item = item;
            Count = count;
        }
    }
}