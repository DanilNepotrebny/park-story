﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Prices;
using Zenject;

namespace DreamTeam.Analytics.Events.Global
{
    public abstract class BaseIngameProductPackPurchaseEvent : CustomEventWithMainLevelPack
    {
        protected BaseIngameProductPackPurchaseEvent(
                CountedItemPrice price,
                ProductPack productPack,
                GamePlace gamePlace,
                [Inject] Lives lives
            )
        {
            AddString("pack", productPack.PurchaseId);
            AddString("currency", price.Item.Id);
            AddInt("price", price.Count);
            AddInt("currency_amount", price.Item.Count);
            AddString("game_place", gamePlace.ToAnalyticsId());
            AddInt("lives", lives.Count);
        }
    }
}