﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Prices;
using DreamTeam.Match3.Levels;
using UnityEngine.Purchasing;
using Zenject;

namespace DreamTeam.Analytics.Events.Global
{
    public class InAppPurchasedEvent : IAnalyticsEvent
    {
        public Product Product { get; }
        public IAPPrice.Instance PriceInstance { get; }
        public ICurrentLevelInfo LevelInfo { get; }

        public InAppPurchasedEvent(
                IAPPrice.Instance priceInstance,
                Product product,
                [Inject] ICurrentLevelInfo levelInfo
            )
        {
            Product = product;
            PriceInstance = priceInstance;
            LevelInfo = levelInfo;
        }
    }
}