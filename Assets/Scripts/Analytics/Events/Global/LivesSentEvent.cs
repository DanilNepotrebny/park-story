﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Inventory.Items;
using Zenject;

namespace DreamTeam.Analytics.Events.Global
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class LivesSentEvent : CustomEventWithMainLevelPack
    {
        public override string EventName => "ce_lives_sent";

        public LivesSentEvent(
                [Inject] Lives lives
            )
        {
            AddInt("lives", lives.Count);
        }
    }
}