﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine.Assertions;

namespace DreamTeam.Analytics.Events.Global
{
    public abstract class BaseCustomEvent : IAnalyticsEvent
    {
        public Dictionary<string, object> Parameters { get; } = new Dictionary<string, object>();

        public abstract string EventName { get; }

        protected void AddBool(string name, bool value)
        {
            Assert.IsFalse(Parameters.ContainsKey(name));
            Parameters.Add(name, value);
        }

        protected void AddInt(string name, int value)
        {
            Assert.IsFalse(Parameters.ContainsKey(name));
            Parameters.Add(name, value);
        }

        protected void AddFloat(string name, float value)
        {
            Assert.IsFalse(Parameters.ContainsKey(name));
            Parameters.Add(name, value);
        }

        protected void AddString(string name, string value)
        {
            Assert.IsFalse(Parameters.ContainsKey(name));
            Parameters.Add(name, value);
        }
    }
}