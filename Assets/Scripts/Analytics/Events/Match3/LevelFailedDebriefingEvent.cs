﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Reflection;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Inventory;
using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using Zenject;

namespace DreamTeam.Analytics.Events.Match3
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class LevelFailedDebriefingEvent : BaseCustomEvent
    {
        public override string EventName => "ce_debriefing_lose";

        public LevelFailedDebriefingEvent(
                DebriefingDecisionType decisionType,
                [Inject] ICurrentLevelInfo levelInfo,
                [Inject(Id = LevelSettings.GoalInstancesId)] IReadOnlyList<Requirement.Instance> goalInstances,
                [Inject] LevelFailureHandler levelFailureHandler
            )
        {
            AddString("level", levelInfo.CurrentLevelId);
            AddString("decision", decisionType.ToAnalyticsId());

            for (int i = 0; i < goalInstances.Count; i++)
            {
                Requirement.Instance goalInstance = goalInstances[i];
                AddFloat($"goal{i}_result", goalInstance.ProgressPercentage);
            }

            ProductPack offerPack = levelFailureHandler.LastPurchasedOutOfMovesOffer;

            AddString(
                    "out_of_moves_offer",
                    offerPack != null ? offerPack.PurchaseId : "none"
                );
        }
    }
}