﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Reflection;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using Zenject;

namespace DreamTeam.Analytics.Events.Match3
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public abstract class BaseLevelCompletedEvent : IAnalyticsEvent
    {
        [Inject]
        public ICurrentLevelInfo LevelInfo { get; }

        [Inject(Id = LevelSettings.GoalInstancesId)]
        public IReadOnlyList<Requirement.Instance> GoalInstances { get; }

        [Inject(Id = LevelSettings.LimitationInstancesId)]
        public IReadOnlyList<Requirement.Instance> LimitationInstances { get; }
    }
}