﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Match3.Levels;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Analytics.Events.Match3
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class BriefingPlayButtonClickedEvent : BaseCustomEvent
    {
        public override string EventName => "ce_briefing_play";

        public BriefingPlayButtonClickedEvent(
                [Inject] EquippedChipBoosters equippedBoosters,
                [Inject(Id = GlobalInstaller.BriefingBoosterGroupId)] BoosterGroup briefingBoosterGroup,
                [Inject] ICurrentLevelInfo levelInfo
            )
        {
            AddString("level", levelInfo.CurrentLevelId);

            foreach (Booster booster in briefingBoosterGroup)
            {
                ChipBooster chipBooster = booster as ChipBooster;
                Assert.IsNotNull(chipBooster, $"Briefing booster group contains booster '{booster.Id}' that is not inherited from Chip Booster.");
                if (chipBooster != null)
                {
                    AddBool(booster.Id, equippedBoosters.IsEquipped(chipBooster));
                }
            }
        }
    }
}