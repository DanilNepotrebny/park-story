﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Inventory;
using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using Zenject;

namespace DreamTeam.Analytics.Events.Match3
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class LevelSucceededEvent : BaseCustomEvent
    {
        public override string EventName => "ce_level_succeeded";

        public int LevelsMoneyEarned { get; }
        public int BonusMoneyEarned { get; }

        [Inject]
        public ICurrentLevelInfo LevelInfo { get; }
        
        [Inject(Id = LevelSettings.LimitationInstancesId)]
        public IReadOnlyList<Requirement.Instance> LimitationInstances { get; }

        public int RemainingMoves { get; }

        public LevelSucceededEvent(
                int levelsMoneyEarned,
                int bonusMoneyEarned,
                [Inject] ICurrentLevelInfo levelInfo,
                [Inject(Id = LevelSettings.LimitationInstancesId)]
                IReadOnlyList<Requirement.Instance> limitationInstances,
                [Inject] LevelFailureHandler levelFailureHandler
            )
        {
            LevelsMoneyEarned = levelsMoneyEarned;
            BonusMoneyEarned = bonusMoneyEarned;

            RemainingMoves = limitationInstances.OfType<MovesRequirement.Instance>()
                .Select(req => req.RemainingMoves)
                .FirstOrDefault();

            AddString("level", levelInfo.CurrentLevelId);
            AddInt("moves_left", RemainingMoves);

            ProductPack offerPack = levelFailureHandler.LastPurchasedOutOfMovesOffer;
            AddString(
                    "out_of_moves_offer",
                    offerPack != null ? offerPack.PurchaseId : "none"
                );
        }
    }
}