﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;

namespace DreamTeam.Analytics.Events.Match3
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class LevelFailedEvent : BaseLevelCompletedEvent
    {
    }
}