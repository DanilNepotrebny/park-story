﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Reflection;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Prices;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Analytics.Events.Match3
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public partial class OutOfMovesClosedEvent : BaseCustomEvent
    {
        public ProductPack ProductPack { get; }
        public PurchaseDecision Decision { get; }

        public override string EventName => "ce_out_of_moves_close";

        public OutOfMovesClosedEvent(
                ProductPack productPack,
                PurchaseDecision decisionType,
                bool wasPurchasePossible,
                [Inject] Lives lives,
                [Inject(Id = GlobalInstaller.MoneyId)] CountedItem money,
                [Inject] ICurrentLevelInfo levelInfo,
                [Inject(Id = LevelSettings.GoalInstancesId)] IReadOnlyList<Requirement.Instance> goalInstances
            )
        {
            ProductPack = productPack;
            Decision = decisionType;

            Assert.IsTrue(!string.IsNullOrEmpty(ProductPack.PurchaseId));
            AddString("pack", ProductPack.PurchaseId);
            AddString("level", levelInfo.CurrentLevelId);

            var countedItemPrice = productPack.Price as CountedItemPrice;
            Assert.IsNotNull(countedItemPrice);
            int countedItemPriceCount = countedItemPrice?.Count ?? 0;
            AddInt("coins_price", countedItemPriceCount);
            AddBool("coins_enough", wasPurchasePossible);
            AddInt("coins", money.Count);
            AddInt("lives", lives.Count);

            for (int i = 0; i < goalInstances.Count; i++)
            {
                Requirement.Instance goalInstance = goalInstances[i];
                AddFloat($"goal{i}_result", goalInstance.ProgressPercentage);
            }

            AddString("decision", decisionType.ToAnalyticsId());
        }
    }
}