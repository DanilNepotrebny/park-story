﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Quests;

namespace DreamTeam.Analytics.Events.Location
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class QuestGoalCompletedEvent : CustomEventWithMainLevelPack
    {
        public override string EventName => "ce_quest_goal_completed";

        public QuestGoalCompletedEvent(
                Quest quest,
                Goal goal,
                int completedGoalsPerLevel
            )
        {
            AddString("quest", quest.Id);
            AddInt("goal", quest.GetGoalNumber(goal));
            AddInt("quests_per_level", completedGoalsPerLevel);
        }
    }
}