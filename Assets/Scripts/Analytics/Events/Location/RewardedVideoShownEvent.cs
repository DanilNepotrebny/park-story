﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Analytics.Events.Global;

namespace DreamTeam.Analytics.Events.Location
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class RewardedVideoShownEvent : CustomEventWithMainLevelPack
    {
        public override string EventName => "ce_rewarded_video_shown";

        public bool IsSuccessful { get; }

        public RewardedVideoShownEvent(bool isSuccessful)
        {
            IsSuccessful = isSuccessful;
            AddBool("successful", isSuccessful);
        }
    }
}