﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Inventory.Items;
using DreamTeam.Quests;
using Zenject;

namespace DreamTeam.Analytics.Events.Location
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class QuestsOpenEvent : CustomEventWithMainLevelPack
    {
        public override string EventName => "ce_quests_open";

        public QuestsOpenEvent(
                [Inject] QuestManager questManager,
                [Inject(Id = GlobalInstaller.StarsId)] CountedItem stars
            )
        {
            int questsCount = 0;

            QuestPack currentPack = questManager.CurrentPack;
            if (currentPack != null)
            {
                foreach (Quest quest in currentPack.Quests)
                {
                    if (!quest.IsWaitingForCompletion &&
                        quest.CheckActiveState(q => q.IsCompleted || q.IsWaitingForCompletion))
                    {
                        questsCount++;
                    }
                }
            }

            AddInt("quests_count", questsCount);
            AddInt("stars", stars.Count);
        }
    }
}