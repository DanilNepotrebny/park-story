﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Analytics.Events.Global;

namespace DreamTeam.Analytics.Events.Location
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class LocationPlayButtonClickedEvent : BaseCustomEvent
    {
        public override string EventName => "ce_play_button_tap";
    }
}