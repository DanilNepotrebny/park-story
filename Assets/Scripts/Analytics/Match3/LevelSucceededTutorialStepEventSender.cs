﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Analytics.Events.Global.Tutorial;
using DreamTeam.Match3.LevelCompletion;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Analytics.Match3
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class LevelSucceededTutorialStepEventSender : MonoBehaviour
    {
        [SerializeField] private TutorialStep _step;

        [Inject] private LevelCompletionSystem _levelCompletionSystem;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private Instantiator _instantiator;

        protected void Start()
        {
            _levelCompletionSystem.LevelSucceeded += OnLevelSucceeded;
        }

        private void OnLevelSucceeded()
        {
            _analyticsSystem.Send(_instantiator.Instantiate<TutorialEvent>(_step));
        }
    }
}