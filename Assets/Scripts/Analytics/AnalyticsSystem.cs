﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Text;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Debug;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Analytics
{
    public class AnalyticsSystem : MonoBehaviour, ISynchronizable, IStateInitializationHandler, IInitable
    {
        [Inject] private SaveGroup _saveGroup;
        [Inject] private Instantiator _instantiator;
        [Inject] private DebugStatesSystem _debugStatesSystem;

        private ObjectDispatcher<IAnalyticsEvent> _eventDispatcher = new ObjectDispatcher<IAnalyticsEvent>();
        private string _userId;

        public string UserId => _userId;

        public event Action UserIdInitialized;

        public void Init()
        {
            _debugStatesSystem.RegisterWriter(nameof(AnalyticsSystem), DrawDebugState);

            _saveGroup.RegisterSynchronizable("AnalyticsSystem", this);
            _userId = Guid.NewGuid().ToString();
        }

        public void Send(IAnalyticsEvent analyticsEvent)
        {
            _eventDispatcher.Send(analyticsEvent);
        }

        public void AddSubscriber<TEvent>(Action<TEvent> subscriber)
            where TEvent : class, IAnalyticsEvent
        {
            _eventDispatcher.AddSubscriber(subscriber);
        }

        public void RemoveSubscriber<TEvent>(Action<TEvent> subscriber)
            where TEvent : class, IAnalyticsEvent
        {
            _eventDispatcher.RemoveSubscriber(subscriber);
        }

        public void Sync(State state)
        {
            state.SyncString("UserId", ref _userId);
        }

        private void DrawDebugState(StringBuilder builder)
        {
            builder.AppendParameter("UserId", UserId);
        }

        #region IStateInitializationHandler

        void IStateInitializationHandler.OnStateInitialized()
        {
            UnityEngine.Debug.Log($"[{nameof(AnalyticsSystem)}] Initialized UserId: {UserId}");

            UserIdInitialized?.Invoke();
            Send(_instantiator.Instantiate<ApplicationLaunchedEvent>());
        }

        #endregion
    }
}
