﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Analytics.Events.Global.Cutscenes;
using DreamTeam.Cutscenes;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Analytics
{
    public class CutsceneAnalyticsSender : MonoBehaviour
    {
        [Inject] private CutsceneSystem _cutsceneSystem;
        [Inject] private AnalyticsSystem _analyticsSystem;

        protected void OnEnable()
        {
            _cutsceneSystem.CutsceneStarted += OnCutsceneStarted;
            _cutsceneSystem.CutsceneFinished += OnCusceneFinished;
        }

        protected void OnDisable()
        {
            _cutsceneSystem.CutsceneStarted -= OnCutsceneStarted;
            _cutsceneSystem.CutsceneFinished -= OnCusceneFinished;
        }

        private void OnCutsceneStarted(SceneReference scene)
        {
            _analyticsSystem.Send(new StartCutsceneEvent(scene));
        }

        private void OnCusceneFinished(SceneReference scene)
        {
            _analyticsSystem.Send(new EndCutsceneEvent(scene));
        }
    }
}