﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.SocialNetworking.Facebook;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Analytics
{
    [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
    public class FriendsInvitationEventSender : MonoBehaviour
    {
        [Inject] private FacebookManager _facebookManager;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private Instantiator _instantiator;

        protected void Awake()
        {
            _facebookManager.FriendsInvitationFinished += OnFacebookFriendsInvitationFinished;
        }

        private void OnFacebookFriendsInvitationFinished(FacebookManager.RequestResult result)
        {
            if (result != FacebookManager.RequestResult.Success)
            {
                return;
            }

            var loginEvent = _instantiator.Instantiate<FriendsInvitationEvent>();
            _analyticsSystem.Send(loginEvent);
        }
    }
}