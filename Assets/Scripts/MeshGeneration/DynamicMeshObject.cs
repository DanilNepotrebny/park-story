﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.MeshGeneration
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [AddComponentMenu("")]
    public class DynamicMeshObject : MonoBehaviour
    {
        public event Action<DynamicMeshObject> TransformChangeEvent;
        public event Action<DynamicMeshObject> DestroyEvent;

        protected void OnEnable()
        {
            Component[] components = GetComponents<Component>();
            foreach (Component component in components)
            {
                if (component != transform)
                {
                    component.hideFlags |= HideFlags.NotEditable;
                }
            }
        }

        protected void Update()
        {
            if (transform.hasChanged)
            {
                TransformChangeEvent?.Invoke(this);
                transform.hasChanged = false;
            }
        }

        protected void OnDestroy()
        {
            DestroyEvent?.Invoke(this);
        }
    }
}