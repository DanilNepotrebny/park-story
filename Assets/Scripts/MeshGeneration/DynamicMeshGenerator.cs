﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.MeshGeneration
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public partial class DynamicMeshGenerator : MonoBehaviour
    {
        [SerializeField, SortingLayer] private int _sortingLayerId;
        [SerializeField] private int _sortingOrder;
        [SerializeField] private Color _color = Color.white;

        private MeshFilter _cachedFilter;
        private MeshRenderer _cachedRenderer;
        private MeshCollider _cachedCollider;
        private List<DynamicMeshObject> _cachedObjects;
        private int _flag = (int)Flags.None;

        private MeshFilter _filter
        {
            get
            {
                if (_cachedFilter == null)
                {
                    _cachedFilter = GetComponent<MeshFilter>();
                }

                return _cachedFilter;
            }
        }

        private MeshRenderer _renderer
        {
            get
            {
                if (_cachedRenderer == null)
                {
                    _cachedRenderer = GetComponent<MeshRenderer>();
                }

                return _cachedRenderer;
            }
        }

        private MeshCollider _collider
        {
            get
            {
                if (_cachedCollider == null)
                {
                    _cachedCollider = GetComponent<MeshCollider>();
                }

                return _cachedCollider;
            }
        }

        private List<DynamicMeshObject> _objects
        {
            get
            {
                if (_cachedObjects == null)
                {
                    InitCachedObjectsList();
                }

                return _cachedObjects;
            }

            set
            {
                _cachedObjects = value;
            }
        }

        protected void Awake()
        {
            _renderer.sortingLayerID = _sortingLayerId;
            _renderer.sortingOrder = _sortingOrder;
        }

        protected void OnEnable()
        {
            RefreshCachedObjectsList();
            _flag |= (int)Flags.Refresh;
        }

        protected void OnValidate()
        {
            _renderer.sortingLayerID = _sortingLayerId;
            _renderer.sortingOrder = _sortingOrder;

            RefreshCachedObjectsList();
            _flag |= (int)Flags.Refresh;
        }

        protected void OnTransformChildrenChanged()
        {
            RefreshCachedObjectsList();
            _flag |= (int)Flags.Refresh;
        }

        protected void LateUpdate()
        {
            if (_flag > 0 && _objects.Count > 2)
            {
                if ((_flag & (int)Flags.Refresh) == (int)Flags.Refresh)
                {
                    _filter.sharedMesh = GenerateMesh();

                    if (Application.isPlaying && _collider != null && _filter.sharedMesh != null)
                    {
                        _collider.sharedMesh = _filter.sharedMesh;
                    }
                }
                else if ((_flag & (int)Flags.TransformChanged) == (int)Flags.TransformChanged)
                {
                    CalculateMesh(_filter.sharedMesh);
                }
            }

            _flag = (int)Flags.None;
        }

        private Mesh GenerateMesh()
        {
            var mesh = new Mesh();
            mesh.name = gameObject.name;
            mesh.MarkDynamic();
            mesh.hideFlags = HideFlags.DontSave;
            CalculateMesh(mesh);
            return mesh;
        }

        private void CalculateMesh(Mesh mesh)
        {
            Vector2[] points = _objects
                .Select(meshObject => new Vector2(meshObject.transform.localPosition.x, meshObject.transform.localPosition.y))
                .ToArray();

            var triangulator = new Triangulator(points);

            mesh.vertices = points.Select(v => (Vector3)v).ToArray();
            mesh.triangles = triangulator.Triangulate();
            mesh.uv = CalculateUVs(points.ToList());
            mesh.colors32 = Enumerable.Repeat((Color32)_color, _objects.Count).ToArray();
        }

        private void ClearCachedObjectsList()
        {
            if (_cachedObjects != null)
            {
                foreach (DynamicMeshObject meshObject in _cachedObjects)
                {
                    meshObject.TransformChangeEvent -= OnMeshObjectTransformChanged;
                    meshObject.DestroyEvent -= OnMeshObjectDestroyed;
                }

                _cachedObjects.Clear();
            }
        }

        private void InitCachedObjectsList()
        {
            _cachedObjects = GetComponentsInChildren<DynamicMeshObject>(true).ToList();

            foreach (DynamicMeshObject meshObject in _cachedObjects)
            {
                meshObject.TransformChangeEvent += OnMeshObjectTransformChanged;
                meshObject.DestroyEvent += OnMeshObjectDestroyed;
            }
        }

        private void RefreshCachedObjectsList()
        {
            ClearCachedObjectsList();
            InitCachedObjectsList();
        }

        private void OnMeshObjectTransformChanged(DynamicMeshObject meshObject)
        {
            _flag |= (int)Flags.TransformChanged;
        }

        private void OnMeshObjectDestroyed(DynamicMeshObject meshObject)
        {
            _flag |= (int)Flags.Refresh;
        }

        private Vector2[] CalculateUVs(List<Vector2> points)
        {
            const float textureEdgesCount = 4;

            List<Vector2> uvs = new List<Vector2>();

            float partition = textureEdgesCount / points.Count;

            float uvX = 0f, uvY = 0f;
            while (uvX < 1f)
            {
                uvs.Add(new Vector2(uvX, 0f));
                uvX += partition;
            }

            uvY = uvX - 1f;
            while (uvY < 1f)
            {
                uvs.Add(new Vector2(1f, uvY));
                uvY += partition;
            }

            uvX = 1f - (uvY - 1f);
            while (uvX > 0f)
            {
                uvs.Add(new Vector2(uvX, 1f));
                uvX -= partition;
            }

            uvY = uvX + 1f;
            while (uvY > 0f)
            {
                uvs.Add(new Vector2(0f, uvY));
                uvY -= partition;
            }

            return uvs.ToArray();
        }

        private enum Flags
        {
            None = 0,
            TransformChanged = 0x01,
            Refresh = 0xff
        }
    }
}