﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.MeshGeneration
{
    public partial class DynamicMeshGenerator : DynamicMeshGenerator.IEditorAPI
    {
        public void Refresh()
        {
            RefreshCachedObjectsList();
            _flag |= (int)Flags.Refresh;
        }

        public List<DynamicMeshObject> MeshObjects => _objects;

        public Transform CachedTransform => transform;

        public interface IEditorAPI
        {
            void Refresh();

            List<DynamicMeshObject> MeshObjects { get; }
            Transform CachedTransform { get; }
        }
    }
}

#endif // UNITY_EDITOR