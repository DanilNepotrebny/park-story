﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace DreamTeam.Video
{
    [RequireComponent(typeof(RectTransform))]
    public class UIVideoPlayer : MonoBehaviour
    {
        private Coroutine _videoCoroutine;
        private VideoClip _clip;
        private VideoPlayer _player;
        private RawImage _videoRenderer;
        private Button _videoTappedButton;

        public event Action<VideoClip, TextAsset> OnVideoStarted;
        public event Action<VideoClip> OnVideoStopped;
        public event Action<VideoClip, double> OnVideoTicked; 
        public event Action<VideoClip> OnVideoTappedEvent;

        public bool IsPlaying => _videoCoroutine != null;

        public void Play(VideoClip clip, TextAsset subtitles)
        {
            if (!IsPlaying)
            {
                _videoCoroutine = StartCoroutine(PlayInternal(clip, subtitles));
            }
        }

        public void Stop()
        {
            if (IsPlaying)
            {
                _player.Stop();
                StopCoroutine(_videoCoroutine);
                PostVideoPlay();
            }
        }

        public void Clear()
        {
            _player.Dispose();
            _videoRenderer.Dispose();
            _videoTappedButton.Dispose();

            _player = null;
            _videoRenderer = null;
            _videoTappedButton = null;
        }

        private IEnumerator PlayInternal(VideoClip clipAsset, TextAsset subtitlesAsset)
        {
            _player = CreateVideoPlayer();
            _videoRenderer = CreateVideoRenderer();
            _videoTappedButton = CreateVideoTapButton();

            _clip = clipAsset;
            _player.clip = clipAsset;
            _player.Prepare();

            OnVideoStarted?.Invoke(_clip, subtitlesAsset);

            yield return new WaitUntil(() => _player.isPrepared);

            _videoRenderer.color = Color.white;
            _videoRenderer.texture = _player.texture;

            _player.Play();

            while (_player.isPlaying)
            {
                OnVideoTicked?.Invoke(_clip, _player.time);

                yield return null;
            }

            PostVideoPlay();
        }

        private void PostVideoPlay()
        {
            _videoCoroutine = null;
            OnVideoStopped?.Invoke(_clip);
            _clip = null;
        }

        private void OnVideoTapped()
        {
            OnVideoTappedEvent?.Invoke(_clip);
        }

        private VideoPlayer CreateVideoPlayer()
        {
            VideoPlayer player = gameObject.AddComponent<VideoPlayer>();
            player.source = VideoSource.VideoClip;
            player.clip = null;
            player.playOnAwake = false;
            player.waitForFirstFrame = false;
            player.isLooping = false;
            player.playbackSpeed = 1f;
            player.renderMode = VideoRenderMode.APIOnly;
            player.audioOutputMode = VideoAudioOutputMode.Direct;
            player.hideFlags = HideFlags.HideAndDontSave;
            return player;
        }

        private RawImage CreateVideoRenderer()
        {
            GameObject videoRendererObject = new GameObject("VideoRenderer");
            videoRendererObject.transform.SetParent(transform);
            videoRendererObject.hideFlags = HideFlags.HideAndDontSave;
            videoRendererObject.layer = gameObject.layer;

            RectTransform videoRendererTransform = videoRendererObject.AddComponent<RectTransform>();
            videoRendererTransform.anchorMin = Vector2.zero;
            videoRendererTransform.anchorMax = Vector2.one;
            videoRendererTransform.pivot = new Vector2(0.5f, 0.5f);
            videoRendererTransform.sizeDelta = Vector2.zero;
            videoRendererTransform.anchoredPosition3D = Vector3.zero;
            videoRendererTransform.localScale = Vector3.one;

            RawImage videoRenderer = videoRendererObject.AddComponent<RawImage>();
            videoRenderer.texture = null;
            videoRenderer.color = new Color(1f, 1f, 1f, 0f);
            videoRenderer.raycastTarget = false;

            return videoRenderer;
        }

        private Button CreateVideoTapButton()
        {
            GameObject videoTappedObject = new GameObject("VideoTappedButton");
            videoTappedObject.transform.SetParent(transform);
            videoTappedObject.hideFlags = HideFlags.HideAndDontSave;
            videoTappedObject.layer = gameObject.layer;

            RectTransform videoTappedTransform = videoTappedObject.AddComponent<RectTransform>();
            videoTappedTransform.anchorMin = Vector2.zero;
            videoTappedTransform.anchorMax = Vector2.one;
            videoTappedTransform.pivot = new Vector2(0.5f, 0.5f);
            videoTappedTransform.sizeDelta = Vector2.zero;
            videoTappedTransform.anchoredPosition3D = Vector3.zero;
            videoTappedTransform.localScale = Vector3.one;

            Image image = videoTappedObject.AddComponent<Image>();
            image.sprite = null;
            image.color = new Color(1f, 1f, 1f, 0f);
            image.raycastTarget = true;

            Button button = videoTappedObject.AddComponent<Button>();
            button.interactable = true;
            button.transition = Selectable.Transition.None;
            button.navigation = Navigation.defaultNavigation;
            button.onClick.AddListener(OnVideoTapped);

            return button;
        }
    }
}