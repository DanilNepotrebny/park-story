﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI.SimpleAnimating;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Zenject;

namespace DreamTeam.Video
{
    public class SkipVideoButtonController : MonoBehaviour
    {
        [SerializeField] private Button _skipButton;
        [SerializeField] private SimpleAnimation _skipButtonAnimation;

        [Inject] private UIVideoPlayer _videoPlayer;

        private bool _isSkipButtonHidden = true;

        protected void Awake()
        {
            _skipButton.interactable = false;

            _videoPlayer.OnVideoStarted += OnVideoStarted;
            _videoPlayer.OnVideoTappedEvent += OnVideoTappedEvent;
            _videoPlayer.OnVideoStopped += OnVideoStopped;

            _skipButton.onClick.AddListener(OnSkipButtonPressed);
        }

        protected void OnDestroy()
        {
            _skipButton.interactable = false;

            _videoPlayer.OnVideoStarted -= OnVideoStarted;
            _videoPlayer.OnVideoTappedEvent -= OnVideoTappedEvent;
            _videoPlayer.OnVideoStopped -= OnVideoStopped;

            _skipButton.onClick.RemoveListener(OnSkipButtonPressed);
        }

        private void OnVideoStarted(VideoClip clip, TextAsset subtitlesAsset)
        {
            _skipButton.interactable = false;
        }

        private void OnVideoStopped(VideoClip clip)
        {
            _skipButton.interactable = false;
        }

        private void OnVideoTappedEvent(VideoClip clip)
        {
            _skipButtonAnimation.Play(_isSkipButtonHidden ? SimpleAnimationType.Show : SimpleAnimationType.Hide);
            _isSkipButtonHidden = !_isSkipButtonHidden;
            _skipButton.interactable = !_isSkipButtonHidden;
        }

        private void OnSkipButtonPressed()
        {
            _videoPlayer.Stop();
        }
    }
}