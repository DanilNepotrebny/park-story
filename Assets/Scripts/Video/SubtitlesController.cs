﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Localization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Zenject;

namespace DreamTeam.Video
{
    public class SubtitlesController : MonoBehaviour
    {
        [SerializeField] private TMP_Text _subtitlesRenderer;
        [SerializeField] private Image _subtitlesBackground;

        [Inject] private UIVideoPlayer _videoPlayer;
        [Inject] private LocalizationSystem _localizationSystem;

        private int _previousSubtitleIndex = -1;
        private List<SRTParser.Subtitle> _subtitles = null;

        protected void Awake()
        {
            _subtitlesBackground.enabled = false;
            _subtitlesRenderer.enabled = false;

            _videoPlayer.OnVideoStarted += OnVideoStarted;
            _videoPlayer.OnVideoTicked += OnVideoTicked;
            _videoPlayer.OnVideoStopped += OnVideoStopped;
        }

        protected void OnDestroy()
        {
            _subtitlesBackground.enabled = false;
            _subtitlesRenderer.enabled = false;

            _videoPlayer.OnVideoStarted -= OnVideoStarted;
            _videoPlayer.OnVideoTicked -= OnVideoTicked;
            _videoPlayer.OnVideoStopped -= OnVideoStopped;
        }

        private void OnVideoStarted(VideoClip clip, TextAsset subtitlesAsset)
        {
            _previousSubtitleIndex = -1;

            if (subtitlesAsset != null && !string.IsNullOrEmpty(subtitlesAsset.text))
            {
                SRTParser parser = new SRTParser();
                _subtitles = parser.Parse(subtitlesAsset.text);
            }
        }

        private void OnVideoTicked(VideoClip clip, double videoTime)
        {
            if (_subtitles != null)
            {
                int index = _subtitles.FindIndex(subtitle => videoTime > subtitle.From && subtitle.To > videoTime);
                if (index != _previousSubtitleIndex)
                {
                    _previousSubtitleIndex = index;

                    _subtitlesBackground.enabled = index >= 0;
                    _subtitlesRenderer.enabled = index >= 0;

                    _subtitlesRenderer.text = index >= 0 ?
                        _localizationSystem.Localize(_subtitles[index].Text, this) :
                        string.Empty;
                }
            }
        }

        private void OnVideoStopped(VideoClip clip)
        {
            _subtitles = null;
        }
    }
}