﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;

namespace DreamTeam.Video
{
    public class SRTParser
    {
        public struct Subtitle
        {
            public static Subtitle Empty = new Subtitle
            {
                Index = 0,
                From = 0,
                To = 0,
                Text = string.Empty
            };

            public int Index;
            public double From;
            public double To;
            public string Text;
        }

        public List<Subtitle> Parse(string data)
        {
            List<Subtitle> subtitles = new List<Subtitle>();

            string[] lines = data.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
            bool finished = false;
            int index = 0;
            while (!finished)
            {
                if (index + 3 < lines.Length)
                {
                    Subtitle subtitle = Subtitle.Empty;

                    string indexLine = lines[index];
                    string timeLine = lines[index + 1];
                    string textLine = lines[index + 2];

                    if (!int.TryParse(indexLine, out subtitle.Index))
                    {
                        break;
                    }

                    timeLine = timeLine.Replace(',', '.');
                    string[] timeParts = timeLine.Split(new[] { "-->" }, StringSplitOptions.RemoveEmptyEntries);
                    if (timeParts.Length == 2)
                    {
                        TimeSpan fromTime;
                        if (TimeSpan.TryParse(timeParts[0], out fromTime))
                        {
                            subtitle.From = fromTime.TotalSeconds;
                        }
                        else
                        {
                            break;
                        }

                        TimeSpan toTime;
                        if (TimeSpan.TryParse(timeParts[1], out toTime))
                        {
                            subtitle.To = toTime.TotalSeconds;
                        }
                    }
                    else
                    {
                        break;
                    }

                    subtitle.Text = textLine;

                    subtitles.Add(subtitle);

                    index += 4;
                }
                else
                {
                    finished = true;
                }
            }

            return subtitles;
        }
    }
}