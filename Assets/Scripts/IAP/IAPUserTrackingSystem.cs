﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using DreamTeam.Debug;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Prices;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.IAP
{
    public class IAPUserTrackingSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private TimeTicks _averageToNoneRankDownInterval;
        [SerializeField] private TimeTicks _highToAverageRankDownInterval;
        [SerializeField] private TimeTicks _maxAbsence;

        public enum PayerType
        {
            None,
            Average,
            High
        }

        [Inject] private ProductPackInstanceContainer _productPacksContainer;
        [Inject] private SaveGroup _saveGroup;
        [Inject] private DebugStatesSystem _debugStatesSystem;

        public PayerType UserPayerType { get; private set; } = PayerType.None;

        private string _rankDownDate = string.Empty;
        private string _sessionFinishDate = string.Empty;

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("IAPUserTrackingSystem", this);
        }

        protected void Start()
        {
            IReadOnlyCollection<ProductPack.Instance> productPacks = _productPacksContainer.GetInstances<ProductPack.Instance>();
            foreach (ProductPack.Instance productPack in productPacks)
            {
                productPack.Purchased += OnProductPackPurchased;
            }

            ProcessRankDownCheck();

            _debugStatesSystem.RegisterWriter("IAPUserTracking", WriteDelegate);
        }

        protected void OnApplicationFocus(bool isFocused)
        {
            if (isFocused)
            {
                ProcessRankDownCheck();
            }
            else
            {
                _sessionFinishDate = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            }
        }

        private void OnProductPackPurchased(ProductPack.Instance pack, GamePlace place)
        {
            IAPPrice.Instance price = pack.Price as IAPPrice.Instance;
            if (price != null)
            {
                if (pack.IsSpecialOffer)
                {
                    switch (UserPayerType)
                    {
                        case PayerType.None:
                            UserPayerType = PayerType.Average;
                            break;
                        case PayerType.Average:
                            UserPayerType = PayerType.High;
                            break;
                    }
                }
                else
                {
                    UserPayerType = PayerType.High;
                }

                ResetRankDownDate();
            }
        }

        private void ProcessRankDownCheck()
        {
            if (!string.IsNullOrEmpty(_sessionFinishDate) &&
                !string.IsNullOrEmpty(_rankDownDate))
            {
                DateTime previousSessionFinishDate = DateTime.Parse(_sessionFinishDate, CultureInfo.InvariantCulture);
                TimeSpan diff = DateTime.Now - previousSessionFinishDate;
                if (diff < TimeSpan.Zero)
                {
                    UnityEngine.Debug.LogError("Cheater detected!");
                    diff = TimeSpan.Zero;
                }

                TimeSpan rankDownDiff = TimeSpan.Zero;
                if (diff >= _maxAbsence)
                {
                    rankDownDiff = diff - _maxAbsence;
                }

                DateTime rankDownDate = DateTime.Parse(_rankDownDate);
                if (rankDownDiff > TimeSpan.Zero)
                {
                    rankDownDate += rankDownDiff;
                }

                if (DateTime.Now > rankDownDate)
                {
                    RankDown();
                }
            }
            else
            {
                // Fallback
                if (UserPayerType != PayerType.None)
                {
                    _sessionFinishDate = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                    ResetRankDownDate();
                }
            }
        }

        private void ResetRankDownDate()
        {
            TimeSpan interval = TimeSpan.Zero;
            switch (UserPayerType)
            {
                case PayerType.Average:
                    interval = _averageToNoneRankDownInterval;
                    break;

                case PayerType.High:
                    interval = _highToAverageRankDownInterval;
                    break;
            }

            DateTime rankDownDate = DateTime.Now + interval;
            _rankDownDate = rankDownDate.ToString(CultureInfo.InvariantCulture);
        }

        private void RankDown()
        {
            switch (UserPayerType)
            {
                case PayerType.Average:
                    UserPayerType = PayerType.None;
                    break;

                case PayerType.High:
                    UserPayerType = PayerType.Average;
                    break;
            }

            ResetRankDownDate();
        }

        private void WriteDelegate(StringBuilder builder)
        {
            builder.AppendParameter("PayerType", UserPayerType.ToString());
        }

        void ISynchronizable.Sync(State state)
        {
            int payerType = (int)UserPayerType;
            state.SyncInt("PayerType", ref payerType);
            UserPayerType = (PayerType)payerType;

            state.SyncString("RankDownDate", ref _rankDownDate, string.Empty);
            state.SyncString("SessionFinishDate", ref _sessionFinishDate, string.Empty);
        }
    }
}