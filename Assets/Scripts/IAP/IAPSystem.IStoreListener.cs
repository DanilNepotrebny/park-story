﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if IAP

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Analytics;
using DreamTeam.Inventory.Prices;
using DreamTeam.Utils;
using UnityEngine.Purchasing;
using UnityEngine;
using UnityEngine.Purchasing.Security;

using Zenject;

namespace DreamTeam.IAP
{
    public partial class IAPSystem :  IStoreListener
    {
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private Instantiator _instantiator;

        private IStoreController _storeController;

        private CrossPlatformValidator _validator;

        protected void Start()
        {
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            foreach (KeyValuePair<string, IAPPrice.Instance> pair in _priceInstances)
            {
                IAPPrice price = pair.Value.Price;
                var ids = CreateStoreIds(price.StoreIds);
                builder.AddProduct(price.Id, price.Type, ids);
                UnityEngine.Debug.Log($"[IAPSystem] Added product '{price.Id}': {string.Join(", ", ids.Select(p => $"{p.Key} : {p.Value}"))}");
            }

            UnityPurchasing.Initialize(this, builder);
        }

        public IDs CreateStoreIds(IEnumerable<KeyValuePair<IAPPrice.Store, string>> priceStoreIds)
        {
            IDs storeIds = new IDs();

            foreach (KeyValuePair<IAPPrice.Store, string> pair in priceStoreIds)
            {
                string storeId = GetStoreName(pair.Key);
                if (storeId != null)
                {
                    storeIds.Add(pair.Value, storeId);
                }
            }

            return storeIds;
        }

        private string GetStoreName(IAPPrice.Store store)
        {
            switch (store)
            {
                case IAPPrice.Store.AppleAppStore:
                    return AppleAppStore.Name;
                case IAPPrice.Store.GooglePlay:
                    return GooglePlay.Name;
            }

            UnityEngine.Debug.LogError("Invalid store value.");
            return null;
        }

        private void InitiatePurchase(IAPPrice.Instance priceInstance)
        {
            _storeController?.InitiatePurchase(priceInstance.Price.Id);
        }

        #region IStoreListener

        void IStoreListener.OnInitializeFailed(InitializationFailureReason error)
        {
            UnityEngine.Debug.LogError("[InAppSystem] Initialization failed:" + error, this);
        }

        PurchaseProcessingResult IStoreListener.ProcessPurchase(PurchaseEventArgs e)
        {
            string productId = e.purchasedProduct.definition.id;

            IAPPrice.Instance priceInstance;
            if (_priceInstances.TryGetValue(productId, out priceInstance))
            {
                try
                {
                    IPurchaseReceipt[] purchaseReceipts = _validator.Validate(e.purchasedProduct.receipt);
                    IEnumerable<string> receiptContents = purchaseReceipts.Select(
                            r => "{{ " +
                                $"productID: {r.productID}, " +
                                $"purchaseDate: {r.purchaseDate}, " +
                                $"transactionID: {r.transactionID}" +
                                " }}"
                        );
                    UnityEngine.Debug.Log("Purchase receipt is valid.\n" + string.Join("\n", receiptContents));

                    foreach (IPurchaseReceipt productReceipt in purchaseReceipts)
                    {
                        if (productReceipt.productID != productId)
                        {
                            throw new IAPSecurityException(
                                    $"Invalid productID. " +
                                    $"Receipt productID: {productReceipt.productID}. " +
                                    $"Purchase productID: {productId}."
                                );
                        }

                        if (productReceipt.transactionID != e.purchasedProduct.transactionID)
                        {
                            throw new IAPSecurityException(
                                    $"Invalid transactionID. " +
                                    $"Receipt transactionID: {productReceipt.transactionID}. " +
                                    $"Purchase transactionID: {e.purchasedProduct.transactionID}."
                                );
                        }

                        UnityEngine.Debug.Log($"Purchase of {productId} is accepted.");
                        priceInstance.OnPurchaseCompleted();
                    }
                }
                catch (IAPSecurityException ex)
                {
                    UnityEngine.Debug.Log("Invalid receipt, purchase denied. " + ex);
                    return PurchaseProcessingResult.Complete;
                }
            }
            else
            {
                UnityEngine.Debug.LogError(
                        $"[InAppSystem] Can not process purchase. " +
                        $"IAPPrice with id:{e.purchasedProduct.definition.id} is not registered.",
                        this
                    );
            }

            return PurchaseProcessingResult.Complete;
        }
    
        void IStoreListener.OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            UnityEngine.Debug.LogError(
                    $"[InAppSystem] Purchase failed. Product: {product.definition.storeSpecificId}. " +
                    $"Reason: {failureReason}",
                    this
                );
        }

        void IStoreListener.OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            _storeController = controller;

            if (_storeController != null)
            {
                _validator = new CrossPlatformValidator(
                        GooglePlayTangle.Data(),
                        AppleTangle.Data(),
                        Application.identifier
                    );

                foreach (KeyValuePair<string, IAPPrice.Instance> pair in _priceInstances)
                {
                    Product product = _storeController.products.WithID(pair.Key);
                    if (product != null)
                    {
                        pair.Value.OnProductInitialized(product);
                    }
                }
            }
        }

        #endregion
    }
}

#endif