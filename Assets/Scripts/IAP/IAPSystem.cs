﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory.Prices;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using UnityEngine;

namespace DreamTeam.IAP
{
    public partial class IAPSystem : MonoBehaviour, ISynchronizable
    {
        private Dictionary<string, IAPPrice.Instance> _priceInstances = new Dictionary<string, IAPPrice.Instance>();

        public void RegisterPrice(IAPPrice.Instance instance)
        {
            if (!_priceInstances.ContainsKey(instance.Price.Id))
            {
                _priceInstances.Add(instance.Price.Id, instance);
            }
            else
            {
                UnityEngine.Debug.LogError($"[IAPSystem] Price with id '{instance.Price.Id}' already exist.", instance.Price);
            }
        }

        public void Purchase(IAPPrice.Instance priceInstance)
        {
            #if IAP
            {
                InitiatePurchase(priceInstance);
            }
            #else
            {
                priceInstance.OnPurchaseCompleted();
            }
            #endif
        }
        
        #region ISynchronizable

        void ISynchronizable.Sync(State state)
        {
            state.SyncObjectDictionary("Prices", ref _priceInstances);
        }

        #endregion
    }
}