﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.IO;
using DreamTeam.Debug;
using DreamTeam.Synchronization.Nodes;
using DreamTeam.Synchronization.Serializers;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using DreamTeam.Utils.FileFolders;
using UnityEngine;
using Zenject;

namespace DreamTeam
{
    public class SaveManager : MonoBehaviour
    {
        [SerializeField, ContainsAllEnumKeys] private GroupPathes _pathes;

        private IFileFolder _folder;
        private INodeSerializer _serializer;
        private CheatPanel _cheatPanel;

        private bool _isSaveLocked = false;

        private readonly Dictionary<Group, SaveGroup> _saveGroups = new Dictionary<Group, SaveGroup>();

        [Inject]
        private void Construct(
                CheatPanel cheatPanel,
                INodeSerializer serializer
            )
        {
            cheatPanel.AddDrawer(CheatPanel.Category.General, DrawCheatGUI);

            _serializer = serializer;

            _folder = new FileFolder();
            _folder.PushPath(Application.persistentDataPath);
        }

        public void Load(SaveGroup saveGroup, Action loadedCallback = null)
        {
            State state = CreateDeserializationState(saveGroup, _folder);
            if (state != null)
            {
                saveGroup.Sync(state);
            }

            saveGroup.OnStateInitialized();
            loadedCallback?.Invoke();
        }

        public void Save()
        {
            if (_serializer == null)
            {
                throw new InvalidOperationException("Can't save because serializer doesn't exist.");
            }

            if (_isSaveLocked)
            {
                return;
            }

            foreach (SaveGroup group in _saveGroups.Values)
            {
                Save(group);
            }
        }

        public void Save(SaveGroup group)
        {
            SerializationState state = new SerializationState();
            group.Sync(state);

            using (Stream stream = _folder.OpenWrite(GetSavePath(group.Group)))
            {
                _serializer.Serialize(stream, state.Root);
            }
        }

        public void LockSaving()
        {
            _isSaveLocked = true;
        }

        protected void OnDestroy()
        {
            Save();
            _saveGroups.Clear();

            _folder.Dispose();
            _folder = null;
        }

        protected void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                Save();
            }
        }

        public void RegisterGroup(SaveGroup saveGroup)
        {
            Group group = saveGroup.Group;

            if (_saveGroups.ContainsKey(group))
            {
                throw new ArgumentException($"Group {Enum.GetName(typeof(Group), group)} is already registered.");
            }

            _saveGroups.Add(group, saveGroup);
        }

        public void UnregisterGroup(SaveGroup saveGroup)
        {
            if (_saveGroups.ContainsKey(saveGroup.Group))
            {
                Save();
                _saveGroups.Remove(saveGroup.Group);
            }
        }

        public SaveGroup GetSaveGroup(Group group)
        {
            return _saveGroups[group];
        }

        public State CreateDeserializationState(SaveGroup saveGroup, IFileFolder folder)
        {
            string path = GetSavePath(saveGroup.Group);

            if (!_folder.Exists(path))
            {
                return null;
            }

            DictionaryNode root;

            using (Stream stream = folder.OpenRead(path))
            {
                root = (DictionaryNode)_serializer.Deserialize(stream);
            }

            if (root == null)
            {
                throw new InvalidDataException("Root node is null.");
            }

            return new DeserializationState(root);
        }

        public void ExportFiles(IFileFolder output, ICollection<Group> excludes = null)
        {
            if (excludes == null)
            {
                excludes = Array.Empty<Group>();
            }

            foreach (var pair in _pathes)
            {
                Group group = pair.Key;

                if (excludes.Contains(group))
                {
                    continue;
                }

                string path = GetSavePath(group);
                if (!_folder.Exists(path))
                {
                    continue;
                }

                using (Stream to = output.OpenWrite(path))
                {
                    using (Stream from = _folder.OpenRead(path))
                    {
                        from.CopyTo(to);
                    }
                }
            }
        }

        public void ImportFiles(IFileFolder input)
        {
            foreach (var pair in _pathes)
            {
                Group group = pair.Key;
                string path = GetSavePath(group);

                if (!input.Exists(path))
                {
                    continue;
                }

                using (Stream from = input.OpenRead(path))
                {
                    using (Stream to = _folder.OpenWrite(path))
                    {
                        from.CopyTo(to);
                    }
                }
            }
        }

        public bool HasFiles(ICollection<Group> excludes = null)
        {
            if (excludes == null)
            {
                excludes = Array.Empty<Group>();
            }

            foreach (var pair in _pathes)
            {
                Group group = pair.Key;
                if (!excludes.Contains(group) && _folder.Exists(GetSavePath(group)))
                {
                    return true;
                }
            }

            return false;
        }

        private string GetSavePath(Group group)
        {
            return _pathes[group];
        }

        private void DrawCheatGUI(Rect areaRect)
        {
            if (GUILayout.Button("Reset Progress"))
            {
                LockSaving();

                foreach (var pair in _pathes)
                {
                    Group group = pair.Key;
                    _folder.Delete(GetSavePath(group));
                }

                EditorUtils.QuitApplication();
            }
        }

        #region Inner types

        public enum Group
        {
            Global,
            Location,
            Match3,
            Settings,
            Loading,
            Localization
        }

        [Serializable]
        private class GroupPathes : KeyValueList<Group, string>
        {
        }

        #endregion
    }
}