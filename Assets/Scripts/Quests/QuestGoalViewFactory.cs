﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.UI;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Quests
{
    [CreateAssetMenu(fileName = "GoalViewFactory", menuName = "Quests/GoalViewFactory")]
    public class
        QuestGoalViewFactory : ViewFactory<Goal, QuestGoalViewFactory.ModelRefType, QuestGoalViewFactory.PresentersMap>
    {
        [Serializable]
        public class ModelRefType : TypeReference<Goal>
        {
        }

        [Serializable]
        public class PresentersMap : KeyValueList<ModelRefType, GameObject>
        {
        }
    }
}