﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.Quests
{
    public class QuestGoalsPanel : MonoBehaviour, IPresenter<Quest>
    {
        [SerializeField] private QuestGoalViewFactory _viewFactory;

        [Inject] private Instantiator _instantiator;

        private Quest _quest;
        private Goal _currentGoal;
        private GameObject _currentPresenter;
        private DeferredInvocation _createGoalPresenterInvokation;

        public IDeferredInvocationHandle CreateGoalPresenterInvokation
        {
            get
            {
                if (_createGoalPresenterInvokation == null)
                {
                    _createGoalPresenterInvokation = new DeferredInvocation(() => ChangeCurrentGoalPresenter());
                }

                if (!_createGoalPresenterInvokation.IsLocked)
                {
                    _createGoalPresenterInvokation.Start();
                }

                return _createGoalPresenterInvokation.Handle;
            }
        }

        public void Initialize(Quest quest)
        {
            if (_quest != quest)
            {
                UnsubscribeQuest();
                _quest = quest;
                SubscribeQuest();

                UpdateCurrentGoalPresenter();
            }
        }

        protected void OnEnable()
        {
            SubscribeQuest();
        }

        protected void OnDisable()
        {
            UnsubscribeQuest();
        }

        private void SubscribeQuest()
        {
            if (_currentGoal != null)
            {
                _quest.CompletionUpdated += UpdateCurrentGoalPresenter;
            }
        }

        private void UnsubscribeQuest()
        {
            if (_currentGoal != null)
            {
                _quest.CompletionUpdated -= UpdateCurrentGoalPresenter;
            }
        }

        private void UpdateCurrentGoalPresenter()
        {
            if (_currentGoal != _quest?.CurrentGoal)
            {
                _currentGoal = _quest?.CurrentGoal;
                CreateGoalPresenterInvokation.Unlock();
            }
        }

        private void ChangeCurrentGoalPresenter()
        {
            if (_currentGoal != null)
            {
                _currentPresenter?.gameObject.Dispose();
                _currentPresenter = _viewFactory.Create(_currentGoal, _instantiator, transform, false);
            }
        }
    }
}