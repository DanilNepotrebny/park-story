﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using UnityEngine;

namespace DreamTeam.Quests
{
    [Serializable]
    public class QuestPackReward : ISynchronizable
    {
        [SerializeField, Range(0.01f, 1)] private float _completionRatio = 1f;
        [SerializeField] private ProductPack _productPack;

        private bool _isTaken;

        public bool IsTaken => _isTaken;
        public float CompletionRatio => _completionRatio;
        public ICollection<Reward> Rewards => _productPack?.Rewards;

        public void Give(ProductPackInstanceContainer productPackInstanceContainer)
        {
            productPackInstanceContainer.GetInstance(_productPack).GiveForFree(GamePlace.DayReward);
            _isTaken = true;
        }

        public void Sync(State state)
        {
            state.SyncBool("isTaken", ref _isTaken, false);
        }
    }
}