﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.GameControls;
using DreamTeam.Quests.Commands;
using DreamTeam.SceneLoading;
using DreamTeam.Synchronization;
using DreamTeam.Utils;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Quests
{
    public abstract class Goal : MonoBehaviour, ISynchronizable, ISceneLoadable
    {
        [SerializeField] private GameControlsMode _goalControlsMode;

        [Inject] private List<Command> _startCommands;
        [Inject] private GameControlsSystem _gameControlsSystem;

        private State _state;
        private int _currentCommandIdx;
        private ActionDisposable _modeDisableHandle;

        private DeferredInvocation _commandsExecution;

        public abstract bool CanBeStarted { get; }
        public bool IsCompleted => _state == State.Completed;
        public bool IsWaitingForCompletion => _state == State.WaitingForCompletion;
        public bool IsStarted => _state != State.WaitingForStart;

        public virtual event Action Completed;
        public event Action WaitingForCompletion;

        public delegate void GoalStartingDelegate(IDeferredInvocationHandle commandsExecutionHandle);

        public event GoalStartingDelegate CommandsExecutionStarting;
        public event Action Started;

        private Command _currentCommand =>
            _currentCommandIdx < _startCommands.Count && _currentCommandIdx >= 0 ?
                _startCommands[_currentCommandIdx] :
                null;

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<Command>().FromComponentInChildren();
        }

        public virtual bool TryStart()
        {
            if (_goalControlsMode != null)
            {
                _modeDisableHandle = _gameControlsSystem.EnableMode(_goalControlsMode);
            }

            return true;
        }

        public void ExecuteCommands_CHEATS()
        {
            SetState(State.Started);
            for (int i = _currentCommandIdx; i < _startCommands.Count; i++)
            {
                Command cmd = _startCommands[i];

                OverrideGameControlsMode(cmd.GoalModeOverride);
                {
                    cmd.Do_CHEATS();
                }
                ResetGameControlsModeOverride();
            }

            _currentCommandIdx = _startCommands.Count;
            SetState(State.Completed);
        }

        public void SetCompleted()
        {
            if (IsWaitingForCompletion)
            {
                SetState(State.Completed);
            }
        }

        protected void Awake()
        {
            _commandsExecution = new DeferredInvocation(ExecuteCurrentCommand);
        }

        protected void Start()
        {
            if (_state == State.Started)
            {
                ExecuteCurrentCommand();
            }
        }

        protected void OnDestroy()
        {
            ReleaseGameModeDisableHandle();
        }

        protected void OnStarting()
        {
            SetState(State.Started);

            Started?.Invoke();

            PreparePendingCommands();

            using (IDeferredInvocationHandle handle = _commandsExecution.Start())
            {
                CommandsExecutionStarting?.Invoke(handle);
            }
        }

        private BaseAsyncProcess PreparePendingCommands()
        {
            var comandProcesses = new List<BaseAsyncProcess>();
            for (int i = _currentCommandIdx; i < _startCommands.Count; i++)
            {
                Command command = _startCommands[i];

                try
                {
                    BaseAsyncProcess p = command.Prepare();
                    if (p != null)
                    {
                        comandProcesses.Add(p);
                    }
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.LogException(e);
                    return null;
                }
            }

            return new GroupAsyncProcess(comandProcesses);
        }

        private void ExecuteCurrentCommand()
        {
            _currentCommandIdx--;
            ExecuteNextCommand();
        }

        private void ExecuteNextCommand()
        {
            if (_currentCommand != null)
            {
                ResetGameControlsModeOverride();
                _currentCommand.Done -= ExecuteNextCommand;
            }

            _currentCommandIdx++;

            if (_currentCommand != null)
            {
                _currentCommand.Done += ExecuteNextCommand;
                OverrideGameControlsMode(_currentCommand.GoalModeOverride);
                _currentCommand.Do();
            }
            else
            {
                ReleaseGameModeDisableHandle();

                SetState(State.WaitingForCompletion);
                WaitingForCompletion?.Invoke();
            }
        }

        private void OverrideGameControlsMode([CanBeNull] GameControlsMode mode)
        {
            if (mode != null)
            {
                _gameControlsSystem.SwitchMode(ref _modeDisableHandle, mode);
            }
        }

        private void ResetGameControlsModeOverride()
        {
            if (_goalControlsMode != null)
            {
                _gameControlsSystem.SwitchMode(ref _modeDisableHandle, _goalControlsMode);
            }
        }

        private void SetState(State state)
        {
            if (_state == state)
            {
                return;
            }

            _state = state;

            if (IsCompleted)
            {
                ReleaseGameModeDisableHandle();

                Completed?.Invoke();
            }
        }

        private void ReleaseGameModeDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }

        void ISynchronizable.Sync(Synchronization.States.State state)
        {
            state.SyncEnum("state", ref _state);
            state.SyncInt("currentCommandIdx", ref _currentCommandIdx, 0);
        }

        BaseAsyncProcess ISceneLoadable.Load()
        {
            if (_state != State.Started)
            {
                return null;
            }

            return PreparePendingCommands();
        }

        #region Inner types

        private enum State
        {
            WaitingForStart,
            Started,
            WaitingForCompletion,
            Completed
        }

        #endregion
    }
}