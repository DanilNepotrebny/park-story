﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Debug;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Quests
{
    public class QuestManager : MonoBehaviour
    {
        [Inject] private List<QuestPack> _packs;
        [Inject] private CheatPanel _cheatPanel;

        private int _currentPackIdx;

        public QuestPack CurrentPack => _currentPackIdx.InRange(0, _packs.Count - 1) ? _packs[_currentPackIdx] : null;
        public QuestPack NextPack => _nextPackIndex.InRange(0, _packs.Count - 1) ? _packs[_nextPackIndex] : null;
        public IEnumerable<QuestPack> QuestPacks => _packs;
        private int _nextPackIndex => _currentPackIdx + 1;

        public event Action LastPackIsWaitingForCompletion;
        public event Action CompletionUpdated;

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<QuestPack>().FromComponentInChildren(includeInactive: true);
        }

        protected void Awake()
        {
            if (_cheatPanel != null)
            {
                _cheatPanel.AddDrawer(CheatPanel.Category.Quests, DrawCheatsGUI);
            }
        }

        protected void OnDestroy()
        {
            if (_cheatPanel != null)
            {
                _cheatPanel.RemoveDrawer(DrawCheatsGUI);
            }
        }

        protected void Start()
        {
            foreach (QuestPack pack in _packs)
            {
                pack.Initialize();
            }

            _currentPackIdx = _packs.FindIndex(pack => !pack.IsCompleted);

            SubscribeCurrentPack();
        }

        private void DrawCheatsGUI(Rect areaRect)
        {
            List<Goal> goals = new List<Goal>();
            List<Quest> quests = new List<Quest>();
            foreach (QuestPack pack in _packs)
            {
                foreach (Quest quest in pack.Quests)
                {
                    if (!pack.IsCompleted && !quest.IsCompleted)
                    {
                        quests.Add(quest);
                    }

                    foreach (Goal goal in quest.Goals)
                    {
                        if (!pack.IsCompleted && !quest.IsCompleted && !goal.IsCompleted)
                        {
                            GUI.enabled = true;
                            goals.Add(goal);
                        }
                        else
                        {
                            GUI.enabled = false;
                        }

                        if (GUILayout.Button($"Quest:{quest.gameObject.name}__Goal:{goal.transform.GetSiblingIndex()}"))
                        {
                            foreach (Goal cachedCoal in goals)
                            {
                                cachedCoal.ExecuteCommands_CHEATS();
                            }

                            foreach (Quest cachedQuest in quests)
                            {
                                if (cachedQuest.IsWaitingForCompletion)
                                {
                                    cachedQuest.SetCompleted();
                                }
                            }
                        }

                        GUI.enabled = true;
                    }
                }
            }
        }

        private void OnPackWaitingForCompletion()
        {
            if (_currentPackIdx == _packs.Count - 1 && CurrentPack.IsWaitingForCompletion)
            {
                LastPackIsWaitingForCompletion?.Invoke();
            }
        }

        private void OnPackCompleted()
        {
            CurrentPack.Completed -= OnPackCompleted;
            CurrentPack.WaitingForCompletion -= OnPackWaitingForCompletion;

            _currentPackIdx++;
            SubscribeCurrentPack();

            CompletionUpdated?.Invoke();
        }

        private void SubscribeCurrentPack()
        {
            if (CurrentPack != null)
            {
                CurrentPack.Completed += OnPackCompleted;
                CurrentPack.WaitingForCompletion += OnPackWaitingForCompletion;
            }
        }
    }
}