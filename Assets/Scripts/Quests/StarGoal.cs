﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using UnityEngine;
using Zenject;

namespace DreamTeam.Quests
{
    public class StarGoal : Goal
    {
        [SerializeField] private int _starsCountToStart;

        [Inject(Id = GlobalInstaller.StarsId)]
        private CountedItem _stars;

        public int StarsCountToStart => _starsCountToStart;

        public override bool CanBeStarted => _stars.IsSpendingPossible(_starsCountToStart);

        public override bool TryStart()
        {
            base.TryStart();

            if (_stars.TrySpend(_starsCountToStart, GamePlace.Location))
            {
                OnStarting();

                return true;
            }

            return false;
        }
    }
}