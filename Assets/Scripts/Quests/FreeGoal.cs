﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Quests
{
    public class FreeGoal : Goal
    {
        [SerializeField]
        private string _startText = "Start";

        public string StartText => _startText;
        public override bool CanBeStarted => true;

        public override bool TryStart()
        {
            base.TryStart();
            OnStarting();
            return true;
        }
    }
}