﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Localization;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Quests
{
    public class Quest : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private Quest[] _requiredQuestsToActivate;

        [Header("View")] [SerializeField] private Sprite _icon;
        [SerializeField] private Sprite _notificationIcon;
        [SerializeField, LocalizationKey] private string _title;
        [SerializeField] private float _progressWeight = 1;

        [Inject] private List<Goal> _goals;
        [Inject] private SaveGroup _saveGroup;

        private int _currentGoalIdx;
        private bool _isCompleted;

        public Sprite Icon => _icon;
        public Sprite NotificationIcon => _notificationIcon;
        public string Title => _title;

        public int CompletedCount => Goals.Count(item => item.IsCompleted);
        public float CompletionRatio => (float)CompletedCount / Goals.Count;

        public int WaitingForCompletionCount => Goals.Count(item => item.IsWaitingForCompletion || item.IsCompleted);
        public float WaitingForCompletionRatio => (float)WaitingForCompletionCount / Goals.Count;

        public bool IsCompleted => _isCompleted;
        public virtual bool IsWaitingForCompletion => LastGoal.IsCompleted || LastGoal.IsWaitingForCompletion;

        public virtual bool IsActive => CheckActiveState(q => q.IsCompleted);

        public float ProgressWeight => _progressWeight;

        public Goal CurrentGoal => _currentGoalIdx < _goals.Count ? _goals[_currentGoalIdx] : null;
        public IReadOnlyList<Goal> Goals => _goals.AsReadOnly();
        public Goal LastGoal => Goals[Goals.Count - 1];

        public string Id => $"{QuestPack.Id}.{name}";

        [Inject] public QuestPack QuestPack { get; private set; }

        public virtual event Action<Quest> Completed;
        public event Action CompletionUpdated;
        public event Action WaitingForCompletion;

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<Goal>().FromComponentInChildren(includeInactive:true);
            yield return container.Bind<QuestPack>().FromComponentInParents(includeInactive:true);
        }

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("quests/" + name, this);
        }

        public virtual void Initialize()
        {
            for (int i = 0; i < _goals.Count; i++)
            {
                if (!CurrentGoal.IsCompleted)
                {
                    break;
                }

                _currentGoalIdx = i + 1;
            }

            SubscribeCurrentGoal();
        }

        public void SetCompleted()
        {
            if (IsWaitingForCompletion)
            {
                _isCompleted = true;
                Completed?.Invoke(this);
            }
        }

        public int GetGoalNumber(Goal goal)
        {
            int index = _goals.IndexOf(goal);
            return index >= 0 ? index + 1 : -1;
        }

        public bool CheckActiveState([NotNull] Predicate<Quest> requredQuestsPredicate)
        {
            if (IsCompleted)
            {
                return false;
            }

            foreach (Quest requiredQuest in _requiredQuestsToActivate)
            {
                if (!requredQuestsPredicate(requiredQuest))
                {
                    return false;
                }
            }

            return true;
        }

        protected void OnValidate()
        {
            _progressWeight = Mathf.Max(0, _progressWeight);
        }

        private void OnGoalCompleted()
        {
            UnsubscribeCurrentGoal();
            _currentGoalIdx++;

            CompletionUpdated?.Invoke();
            SubscribeCurrentGoal();
        }

        private void OnGoalWaitingForCompletion()
        {
            CompletionUpdated?.Invoke();
            if (IsWaitingForCompletion)
            {
                WaitingForCompletion?.Invoke();
            }
        }

        private void SubscribeCurrentGoal()
        {
            if (CurrentGoal != null)
            {
                CurrentGoal.Completed += OnGoalCompleted;
                CurrentGoal.WaitingForCompletion += OnGoalWaitingForCompletion;
            }
        }

        private void UnsubscribeCurrentGoal()
        {
            if (CurrentGoal != null)
            {
                CurrentGoal.Completed -= OnGoalCompleted;
                CurrentGoal.WaitingForCompletion -= OnGoalWaitingForCompletion;
            }
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncBool("isCompleted", ref _isCompleted, false);
            state.SyncObjectList("goals", _goals);
        }
    }
}