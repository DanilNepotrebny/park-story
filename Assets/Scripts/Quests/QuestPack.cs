﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Inventory;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.Quests
{
    public class QuestPack : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private string _title;
        [SerializeField] private Sprite _picture;
        [SerializeField] private List<QuestPackReward> _rewards;

        [Inject] private List<Quest> _quests;
        [Inject] private SaveGroup _saveGroup;
        [Inject] private ProductPackInstanceContainer _productPackInstanceContainer;

        private int _completedCount;

        public string Title => _title;
        public Sprite Picture => _picture;
        public List<Quest> Quests => _quests;
        public IEnumerable<Quest> ActiveQuests => _quests.Where(quest => quest.IsActive);
        public float CompletionRatio => CalculateWeightedRatio(quest => quest.CompletionRatio);
        public virtual bool IsCompleted => _completedCount >= _quests.Count;
        public ICollection<QuestPackReward> Rewards => _rewards.AsReadOnly();
        public bool IsGivingRewards { get; private set; }

        public bool HasWaitingForCompletionGoals =>
            ActiveQuests.Any(quest => quest.Goals.Any(goal => goal.IsWaitingForCompletion));

        public bool IsWaitingForCompletion => _quests.TrueForAll(
                quest => quest.IsCompleted ||
                quest.WaitingForCompletionRatio >= 1 ||
                quest.IsWaitingForCompletion
            );

        public string Id => name;

        public virtual event Action<float> CompletionUpdated;
        public virtual event Action WaitingForCompletion;
        public virtual event Action Completed;
        public event Action<IDeferredInvocationHandle, QuestPackReward> GivingRewards;

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<Quest>().FromComponentInChildren(includeInactive: true);
        }

        public virtual void Initialize()
        {
            _quests.ForEach(q => q.Initialize());

            _completedCount = 0;
            foreach (Quest quest in _quests)
            {
                if (!quest.IsCompleted)
                {
                    SubscribeQuest(quest);
                }
                else
                {
                    _completedCount++;
                }
            }

            TryGiveRewards();
        }

        public void Sync(State state)
        {
            QuestPackReward[] synchedRewards = _rewards.ToArray();
            state.SyncObjectArray("rewards", synchedRewards);
            _rewards = new List<QuestPackReward>(synchedRewards);
        }

        public void Init()
        {
            _rewards.Sort((a, b) => a.CompletionRatio.CompareTo(b.CompletionRatio));
            _saveGroup.RegisterSynchronizable("questsPacks/" + name, this);
        }

        protected void OnValidate()
        {
            if (_picture == null)
            {
                UnityEngine.Debug.LogAssertion($"Missing {nameof(_picture)}", this);
            }
        }

        private void SubscribeQuest(Quest quest)
        {
            quest.Completed += OnQuestCompleted;
            quest.WaitingForCompletion += OnQuestWaitingForCompletion;
            quest.CompletionUpdated += OnQuestCompletionUpdated;
        }

        private void UnsubscribeQuest(Quest quest)
        {
            quest.Completed -= OnQuestCompleted;
            quest.WaitingForCompletion -= OnQuestWaitingForCompletion;
            quest.CompletionUpdated -= OnQuestCompletionUpdated;
        }

        private void OnQuestCompletionUpdated()
        {
            CompletionUpdated?.Invoke(CompletionRatio);
            TryGiveRewards();
        }

        private void OnQuestWaitingForCompletion()
        {
            WaitingForCompletion?.Invoke();
        }

        private void OnQuestCompleted(Quest quest)
        {
            UnsubscribeQuest(quest);

            _completedCount++;
            CompletionUpdated?.Invoke(CompletionRatio);
            if (IsCompleted)
            {
                Completed?.Invoke();
            }
        }

        private void TryGiveRewards()
        {
            float waitingForCompletionRatio = CalculateWeightedRatio(quest => quest.WaitingForCompletionRatio);
            QuestPackReward currentRewards = _rewards.Find(
                    item => !item.IsTaken && item.CompletionRatio <= waitingForCompletionRatio
                );

            if (!IsGivingRewards && currentRewards?.Rewards != null)
            {
                IsGivingRewards = true;

                using (IDeferredInvocationHandle rewardInvocation = GetRewardInvocation(currentRewards))
                {
                    GivingRewards?.Invoke(rewardInvocation, currentRewards);
                }
            }
        }

        private IDeferredInvocationHandle GetRewardInvocation(QuestPackReward currentPackRewards)
        {
            return new DeferredInvocation(() => OnGivingRewardsUnlocked(currentPackRewards)).Start();
        }

        private void OnGivingRewardsUnlocked(QuestPackReward currentPackRewards)
        {
            currentPackRewards.Give(_productPackInstanceContainer);
            IsGivingRewards = false;
        }

        private float CalculateWeightedRatio(Func<Quest, float> getQuestRatio)
        {
            float totalSum = 0;
            float ratioSum = 0;

            foreach (Quest quest in _quests)
            {
                ratioSum += getQuestRatio(quest) * quest.ProgressWeight;
                totalSum += quest.ProgressWeight;
            }

            return ratioSum / totalSum;
        }

        [ContextMenu("Sort rewards")]
        private void SortRewards()
        {
            _rewards.Sort((a, b) => a.CompletionRatio.CompareTo(b.CompletionRatio));
        }
    }
}