﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory.Items;
using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;

namespace DreamTeam.Quests.Commands
{
    public class UnlockCountedItem : Command
    {
        [SerializeField] private CountedItem _countedItem;

        public override event Action Done;

        public override void Do()
        {
            _countedItem.Unlock();
            Done?.Invoke();
        }

        public override void Do_CHEATS()
        {
            Do();
        }

        public override BaseAsyncProcess Prepare()
        {
            return null;
        }
    }
}