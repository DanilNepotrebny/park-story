﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Quests.Commands
{
    public class PlayCutsceneCommandsRegistrator
    {
        private Dictionary<string, int> _registredCutscenes = new Dictionary<string, int>();

        public int GetCount(PlayCutsceneCommand command)
        {
            int count;
            _registredCutscenes.TryGetValue(command.CutscenePath, out count);
            return count;
        }

        public void RegisterCommand(PlayCutsceneCommand command)
        {
            if (!_registredCutscenes.ContainsKey(command.CutscenePath))
            {
                _registredCutscenes.Add(command.CutscenePath, 0);
            }

            _registredCutscenes[command.CutscenePath]++;
        }

        public void UnRegisterCommand(PlayCutsceneCommand command)
        {
            if (_registredCutscenes.ContainsKey(command.CutscenePath))
            {
                int count = _registredCutscenes[command.CutscenePath];
                _registredCutscenes[command.CutscenePath] = Mathf.Clamp(count - 1, 0, count);
            }
        }
    }
}