﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Linq;
using DreamTeam.Location.Characters;
using DreamTeam.Location.Decorations;
using DreamTeam.Location.Isometry;
using DreamTeam.Mirroring;
using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;
using Zenject;

namespace DreamTeam.Quests.Commands
{
    public class CustomizeDecorationCommand : Command
    {
        [SerializeField, MirrorId] private string _decorationMirrorId;

        [Inject] private DecorationCustomizationSystem _previewSystem;
        [Inject] private MirrorObjectsRegistrator _mirrorsRegistrator;

        [Inject(Id = Character.MainCharacterId)]
        private Character _character;

        private DecorationController _controller;

        private DecorationController _decorationController
        {
            get
            {
                if (_controller == null)
                {
                    try
                    {
                        _controller = _mirrorsRegistrator.GetInstanceObject<DecorationController>(
                                _decorationMirrorId
                            );
                    }
                    catch (Exception exception)
                    {
                        UnityEngine.Debug.Log(exception.Message);
                        _controller = null;
                    }
                }

                return _controller; 

            }
        }

        public override event Action Done;

        public override void Do()
        {
            if (_decorationController == null)
            {
                UnityEngine.Debug.LogError("Can't find decoration controller", this);
                return;
            }

            Decoration decoration = _decorationController.Decoration;
            if (decoration.InitialSkin != null)
            {
                UnityEngine.Debug.LogError("Decoration already has a skin. Set initial skin to null.", decoration);
            }

            StartCoroutine(DoRoutine());
        }

        public override void Do_CHEATS()
        {
            if (_decorationController != null)
            {
                _decorationController.State.CurrentSkin = _decorationController.Decoration.Skins.First(skin => skin != null);
            }
        }

        public override BaseAsyncProcess Prepare()
        {
            if (_decorationController == null)
            {
                UnityEngine.Debug.LogError("Can't find decoration controller", this);
                return null;
            }

            return _previewSystem.PrepareCustomization(_decorationController.State);
        }

        private IEnumerator DoRoutine()
        {
            yield return new WaitUntil(() => _decorationController.isActiveAndEnabled);

            _character.transform.position = _decorationController.Presenter.CharacterPresentationPosition;
            _character.GetComponent<IsometryMovable>().ScreenLookAt(_decorationController.Presenter.CharacterPresentationLookAt);

            _previewSystem.CustomizationDeactivated += OnCustomizationDeactivated;
            _previewSystem.ActivateCustomization(_decorationController.State, _decorationController.Presenter, true);
        }

        private void OnCustomizationDeactivated()
        {
            _previewSystem.CustomizationDeactivated -= OnCustomizationDeactivated;
            Done?.Invoke();
        }
    }
}