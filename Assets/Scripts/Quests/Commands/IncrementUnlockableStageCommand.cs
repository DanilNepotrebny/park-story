﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Location.Unlockables;
using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;
using Zenject;

namespace DreamTeam.Quests.Commands
{
    public class IncrementUnlockableStageCommand : Command
    {
        [SerializeField] private Unlockable _unlockable;
        [Inject] private UnlockableSystem _unlockableSystem;

        public override event Action Done;

        public override void Do()
        {
            _unlockableSystem.GetUnlockableState(_unlockable).Stage++;
            Done?.Invoke();
        }

        public override void Do_CHEATS()
        {
            Do();
        }

        public override BaseAsyncProcess Prepare()
        {
            return null;
        }
    }
}