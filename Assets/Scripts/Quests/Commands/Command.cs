﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.GameControls;
using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;

namespace DreamTeam.Quests.Commands
{
    /// <summary>
    /// Abstract command that can be assigned into some part of quest lifetime (i.e. to show cutscene after goal start).
    /// </summary>
    public abstract class Command : MonoBehaviour
    {
        [SerializeField] private GameControlsMode _goalModeOverride;

        public GameControlsMode GoalModeOverride => _goalModeOverride;

        public abstract event Action Done;

        public abstract void Do();
        public abstract void Do_CHEATS();

        public abstract BaseAsyncProcess Prepare();
    }
}
