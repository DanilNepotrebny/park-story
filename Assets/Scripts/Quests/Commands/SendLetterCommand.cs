﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Messaging;
using DreamTeam.Messaging.MessageAssets;
using DreamTeam.SocialNetworking;
using DreamTeam.Utils;
using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;
using Zenject;

namespace DreamTeam.Quests.Commands
{
    public class SendLetterCommand : Command
    {
        [SerializeField, RequiredField] private CharacterRewardsMessageAsset _letter;

        [Inject] private MessageSystem _messageSystem;
        [Inject] private Instantiator _instantiator;
        [Inject] private SocialProfilesSystem _socialProfilesSystem;

        public override event Action Done;

        public override void Do()
        {
            foreach (Reward reward in _letter.Rewards)
            {
                _instantiator.Inject(reward);
            }

            CharacterRewardsMessage message = new CharacterRewardsMessage(_instantiator, _letter.Rewards, GamePlace.Letter, _letter.PopupScene.Name);
            message.Sender = _socialProfilesSystem.FindProfile(_letter.SenderID);
            _messageSystem.Add(message);
            Done?.Invoke();
        }

        public override void Do_CHEATS()
        {
            Do();
        }

        public override BaseAsyncProcess Prepare()
        {
            return null;
        }
    }
}