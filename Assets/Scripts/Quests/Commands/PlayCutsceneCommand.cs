﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Cutscenes;
using DreamTeam.Utils;
using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;
using Zenject;

namespace DreamTeam.Quests.Commands
{
    public class PlayCutsceneCommand : Command
    {
        [SerializeField] private SceneReference _cutscene;

        [Inject] private CutsceneSystem _cutsceneSystem;
        [Inject] private Goal _goal;
        [Inject] private PlayCutsceneCommandsRegistrator _registrator;

        public string CutscenePath => _cutscene.AssetPath;

        public override event Action Done;

        public override void Do()
        {
            _registrator.UnRegisterCommand(this);

            if (_registrator.GetCount(this) == 0)
            {
                _cutsceneSystem.CutsceneFinished += OnCutsceneFinished;
                _cutsceneSystem.PlayCutscene(_cutscene);
            }
            else
            {
                Done?.Invoke();
            }
        }

        public override void Do_CHEATS()
        {
            _registrator.UnRegisterCommand(this);
            Done?.Invoke();
        }

        public override BaseAsyncProcess Prepare()
        {
            return _cutsceneSystem.PreloadCutscene(_cutscene);
        }

        protected void Start()
        {
            if (!_goal.IsCompleted)
            {
                _registrator.RegisterCommand(this);
            }
        }

        [Bindings]
        private static IEnumerable<ConditionCopyNonLazyBinder> GetBindings(DiContainer container)
        {
            yield return container.Bind<Goal>().FromComponentInParents(includeInactive: true);
        }

        private void OnCutsceneFinished(SceneReference cutscene)
        {
            _cutsceneSystem.CutsceneFinished -= OnCutsceneFinished;
            Done?.Invoke();
        }
    }
}