﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Ads;
using DreamTeam.Popups;
using DreamTeam.UI;
using DreamTeam.UI.QuestPresenters;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Quests
{
    public class QuestsPanel : MonoBehaviour
    {
        [SerializeField] private PresenterHub _questPresenterPrefab;
        [SerializeField] private QuestAdsPresenter _adsPanelPresenterPrefab;

        [Inject] private Popup _popup;
        [Inject] private Instantiator _instantiator;
        [Inject] private QuestManager _questManager;
        [Inject] private AdsSystem _adsSystem;

        private List<Quest> _presentedQuests = new List<Quest>();
        private QuestPack _currentQuestPack;
        private QuestAdsPresenter _adsPanelPresenter;

        protected void Awake()
        {
            _popup.Showing += OnPopupShowing;
            _popup.Hidden += OnPopupHidden;
        }

        protected void OnDestroy()
        {
            _popup.Showing -= OnPopupShowing;
            _popup.Hidden += OnPopupHidden;
        }

        private void OnPopupShowing(Popup popup)
        {
            SubscribeCurrentQuestPack();
            UpdateCurrentQuestPresenters();

            if (_adsSystem.IsRewardedVideoAvailable)
            {
                if (_adsPanelPresenter == null)
                {
                    _adsPanelPresenter = _instantiator.Instantiate(_adsPanelPresenterPrefab, gameObject);
                    _adsPanelPresenter.name = "AdsPresenter";
                }
            }
            else if (_adsPanelPresenter != null)
            {
                _adsPanelPresenter.gameObject.Dispose();
            }
        }

        private void OnPopupHidden(Popup popup)
        {
            UnsubscribeCurrentQuestPack();
        }

        private void OnCurrentPackCompletionUpdated(float progress)
        {
            UpdateCurrentQuestPresenters();
        }

        private void OnCurrentPackCompleted()
        {
            UnsubscribeCurrentQuestPack();
            SubscribeCurrentQuestPack();
            UpdateCurrentQuestPresenters();
        }

        private void SubscribeCurrentQuestPack()
        {
            if (_questManager.CurrentPack != null)
            {
                _currentQuestPack = _questManager.CurrentPack;
                _currentQuestPack.CompletionUpdated += OnCurrentPackCompletionUpdated;
                _currentQuestPack.Completed += OnCurrentPackCompleted;
            }
        }

        private void UnsubscribeCurrentQuestPack()
        {
            if (_currentQuestPack != null)
            {
                _currentQuestPack.CompletionUpdated -= OnCurrentPackCompletionUpdated;
                _currentQuestPack.Completed -= OnCurrentPackCompleted;
                _currentQuestPack = null;
            }
        }

        private void UpdateCurrentQuestPresenters()
        {
            if (_questManager.CurrentPack != null && !_questManager.CurrentPack.IsGivingRewards)
            {
                var activeQuests = new List<Quest>(_questManager.CurrentPack.ActiveQuests);
                ConstructNewQuestPresenters(activeQuests);
                _presentedQuests.RemoveAll(quest => !activeQuests.Contains(quest));
            }
            else
            {
                _presentedQuests.Clear();
            }
        }

        private void ConstructNewQuestPresenters(List<Quest> activeQuests)
        {
            foreach (Quest quest in activeQuests)
            {
                if (!_presentedQuests.Contains(quest))
                {
                    ConstructQuestPresenter(quest);
                    _presentedQuests.Add(quest);
                }
            }
        }

        private void ConstructQuestPresenter([NotNull] Quest quest)
        {
            PresenterHub presenter = _instantiator.Instantiate(_questPresenterPrefab, gameObject);
            presenter.name += $"_{quest.name}View";
            presenter.Initialize(quest);
        }
    }
}