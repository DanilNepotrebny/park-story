﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Mirroring
{
    [CreateAssetMenu(fileName = "MirrorObjectIdentifiers", menuName = "DreamTeam/Mirror objects identifier list")]
    public partial class MirrorObjectsIdentifiersAsset : ScriptableObject
    {
        [SerializeField] private List<string> _identifiers;

        public static string NotSetIdentifier { get; } = "Not Set";

        public bool Contains(string id)
        {
            return _identifiers.Contains(id);
        }
    }
}
