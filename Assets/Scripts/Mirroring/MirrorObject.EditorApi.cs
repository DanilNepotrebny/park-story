﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using DreamTeam.Utils;

namespace DreamTeam.Mirroring
{
    public partial class MirrorObject : IBuildPreprocessor
    {
        void IBuildPreprocessor.PreprocessBeforeBuild()
        {
            if (Flow == MirrorFlow.Mirror)
            {
                DestroyImmediate(gameObject);
            }
        }
    }
}

#endif