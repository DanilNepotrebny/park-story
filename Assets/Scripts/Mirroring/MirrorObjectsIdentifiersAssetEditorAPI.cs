﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System.Collections.Generic;
using System.Linq;
using UnityEditor;

namespace DreamTeam.Mirroring
{
    public partial class MirrorObjectsIdentifiersAsset : MirrorObjectsIdentifiersAsset.IEditorAPI
    {
        public List<string> Identifiers => _identifiers;

        public static MirrorObjectsIdentifiersAsset FindAsset()
        {
            MirrorObjectsIdentifiersAsset asset = null;

            List<string> guids = AssetDatabase.FindAssets($"t:{nameof(MirrorObjectsIdentifiersAsset)}").ToList();
            if (guids.Count != 1)
            {
                UnityEngine.Debug.LogWarning("\"MirrorObjectsIdentifiersAsset\" is not found or there are multiple instances of it!");
            }
            else
            {
                asset = AssetDatabase.LoadAssetAtPath<MirrorObjectsIdentifiersAsset>(AssetDatabase.GUIDToAssetPath(guids[0]));
            }

            return asset;
        }

        public interface IEditorAPI
        {
            List<string> Identifiers { get; }
        }
    }
}

#endif // UNITY_EDITOR