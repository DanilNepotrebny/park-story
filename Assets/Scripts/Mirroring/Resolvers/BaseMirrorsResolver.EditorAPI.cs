﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace DreamTeam.Mirroring.Resolvers
{
    public abstract partial class BaseMirrorsResolver : BaseMirrorsResolver.IEditorAPI
    {
        void IEditorAPI.SaveLinks()
        {
            SaveLinksInternal();
        }

        protected abstract void SaveLinksInternal();

        protected void OnEnable()
        {
            if (!EditorApplication.isPlaying)
            {
                EditorSceneManager.sceneSaving += OnSceneSaving;
                EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
            }
        }

        protected void OnDisable()
        {
            if (!EditorApplication.isPlaying)
            {
                EditorSceneManager.sceneSaving -= OnSceneSaving;
                EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
            }
        }

        private void OnSceneSaving(Scene scene, string path)
        {
            if (gameObject.scene == scene)
            {
                SaveLinksInternal();
            }
        }

        private void OnPlayModeStateChanged(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.ExitingEditMode)
            {
                SaveLinksInternal();
            }
        }

        public interface IEditorAPI
        {
            void SaveLinks();
        }
    }
}

#endif // UNITY_EDITOR