﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Mirroring.Resolvers
{
    [ExecuteInEditMode]
    public abstract partial class BaseMirrorsResolver : MonoBehaviour
    {
        public abstract void Resolve(MirrorObjectsRegistrator registrator, bool force = false);
        public abstract bool IsResolved { get; protected set; }
    }
}