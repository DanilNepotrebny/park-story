﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using UnityEngine;

namespace DreamTeam.Mirroring.Resolvers
{
    public partial class CinemachineVirtualCameraMirrorsResolver : CinemachineVirtualCameraMirrorsResolver.IEditorApi
    {
        CinemachineVirtualCamera IEditorApi.Camera
        {
            get
            {
                if (_camera == null)
                {
                    _camera = GetComponent<CinemachineVirtualCamera>();
                }

                return _camera;
            }
        }

        BindData IEditorApi.FollowBind => _followBind;
        BindData IEditorApi.LookAtBind => _lookAtBind;

        protected override void SaveLinksInternal()
        {
            FillBindingData();
        }

        private void FillBindingData()
        {
            ((IEditorApi)this).FollowBind.MirrorId = string.Empty;
            ((IEditorApi)this).LookAtBind.MirrorId = string.Empty;

            if (((IEditorApi)this).Camera.Follow != null)
            {
                ((IEditorApi)this).FollowBind.MirrorId = FindMirrorId(((IEditorApi)this).Camera.Follow.GetComponents<MirrorObject>().ToList());
            }

            if (((IEditorApi)this).Camera.LookAt != null)
            {
                ((IEditorApi)this).LookAtBind.MirrorId = FindMirrorId(((IEditorApi)this).Camera.LookAt.GetComponents<MirrorObject>().ToList());
            }
        }

        private string FindMirrorId(List<MirrorObject> mirrors)
        {
            string result = string.Empty;
            int counter = 0;
            foreach (MirrorObject mirrorObject in mirrors)
            {
                if (mirrorObject != null &&
                    mirrorObject.Flow == MirrorObject.MirrorFlow.Mirror &&
                    mirrorObject.InstanceId != MirrorObjectsIdentifiersAsset.NotSetIdentifier &&
                    mirrorObject.MirroredObject.GetType() == typeof(Transform))
                {
                    if (counter == 0)
                    {
                        result = mirrorObject.InstanceId;
                        ++counter;
                    }
                    else
                    {
                        UnityEngine.Debug.LogError("[DESIGNERS] Single transform cannot refer to multiple mirror ids!");
                        break;
                    }
                }
            }

            return result;
        }

        public interface IEditorApi : BaseMirrorsResolver.IEditorAPI
        {
            CinemachineVirtualCamera Camera { get; }
            BindData FollowBind { get; }
            BindData LookAtBind { get; }
        }
    }
}

#endif // UNITY_EDITOR