﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Object = UnityEngine.Object;

namespace DreamTeam.Mirroring.Resolvers
{
    [RequireComponent(typeof(PlayableDirector))]
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    public partial class PlayableDirectorMirrorsResolver : BaseMirrorsResolver
    {
        [SerializeField, HideInInspector] private List<BindData> _mirrorBinds = new List<BindData>();
        [SerializeField, HideInInspector] private List<ReferenceData> _mirrorReferences = new List<ReferenceData>();

        private PlayableDirector _director;

        public override void Resolve(MirrorObjectsRegistrator registrator, bool force)
        {
            if (!IsResolved || force)
            {
                if (_director == null)
                {
                    _director = GetComponent<PlayableDirector>();
                }

                if (_director.playableAsset != null)
                {
                    TimelineAsset asset = _director.playableAsset as TimelineAsset;
                    if (asset != null)
                    {
                        foreach (BindData bindData in _mirrorBinds)
                        {
                            if (!string.IsNullOrEmpty(bindData.MirrorId))
                            {
                                MirrorObject instanceObject = registrator.GetMirrorObject(bindData.MirrorId);
                                if (instanceObject != null)
                                {
                                    Type mirorredObjectType = Type.GetType($"{bindData.ReferenceType}, UnityEngine");
                                    if (mirorredObjectType != null)
                                    {
                                        PlayableBinding binding = asset.outputs.ElementAt(bindData.OutputIndex);
                                        Type bindType = binding.outputTargetType;
                                        if (bindType != null)
                                        {
                                            if (mirorredObjectType == bindType ||
                                                mirorredObjectType.IsSubclassOf(bindType))
                                            {
                                                Object referenceValue = null;
                                                if (mirorredObjectType.IsSubclassOf(typeof(Component)))
                                                {
                                                    List<Component> mirroredObjectComponents =
                                                        instanceObject.GetComponents(mirorredObjectType).ToList();
                                                    referenceValue = mirroredObjectComponents[bindData.ReferenceTypeIndex];
                                                }
                                                else
                                                {
                                                    referenceValue = instanceObject.gameObject;
                                                }

                                                if (referenceValue != null)
                                                {
                                                    TrackAsset track = binding.sourceObject as TrackAsset;
                                                    _director.SetGenericBinding(track, referenceValue);
                                                }
                                                else
                                                {
                                                    UnityEngine.Debug.LogError("[PROGRAMMERS] cannot resolve instance reference!", gameObject);
                                                }
                                            }
                                            else
                                            {
                                                UnityEngine.Debug.LogError($"[PROGRAMMERS] incorrect serialization of type {bindData.ReferenceType}!", gameObject);
                                            }
                                        }
                                        else
                                        {
                                            UnityEngine.Debug.LogError($"Didn't find {nameof(bindType)} for " +
                                                $"{binding.sourceObject.name} in {asset.name}. \n" +
                                                $"Try to re-save {asset.name}", asset);
                                        }
                                    }
                                    else
                                    {
                                        UnityEngine.Debug.LogError($"[PROGRAMMERS] Cannot resolve type {bindData.ReferenceType}!", gameObject);
                                    }
                                }
                                else
                                {
                                    UnityEngine.Debug.LogError($"[PROGRAMMERS] Cannot resolve mirror object with id: {bindData.MirrorId}!", gameObject);
                                }
                            }
                        }

                        foreach (ReferenceData referenceData in _mirrorReferences)
                        {
                            if (!string.IsNullOrEmpty(referenceData.MirrorId))
                            {
                                MirrorObject instanceObject = registrator.GetMirrorObject(referenceData.MirrorId);
                                if (instanceObject != null)
                                {
                                    Type mirorredObjectType = Type.GetType(referenceData.ReferenceType);
                                    if (mirorredObjectType != null)
                                    {
                                        Object referenceValue = null;
                                        if (mirorredObjectType.IsSubclassOf(typeof(Component)))
                                        {
                                            List<Component> mirroredObjectComponents =
                                                instanceObject.GetComponents(mirorredObjectType).ToList();
                                            referenceValue = mirroredObjectComponents[referenceData.ReferenceTypeIndex];
                                        }
                                        else
                                        {
                                            referenceValue = instanceObject.gameObject;
                                        }

                                        if (referenceValue != null)
                                        {
                                            _director.SetReferenceValue(referenceData.ExposedName, referenceValue);
                                        }
                                        else
                                        {
                                            UnityEngine.Debug.LogError("[PROGRAMMERS] cannot resolve instance reference!", gameObject);
                                        }
                                    }
                                    else
                                    {
                                        UnityEngine.Debug.LogError($"[PROGRAMMERS] Cannot resolve type {referenceData.ReferenceType}!", gameObject);
                                    }
                                }
                                else
                                {
                                    UnityEngine.Debug.LogError($"[PROGRAMMERS] Cannot resolve mirror object with id {referenceData.MirrorId}!", gameObject);
                                }
                            }
                            else
                            {
                                UnityEngine.Debug.LogError("[PROGRAMMERS] Incorrect mirror serialization!", gameObject);
                            }
                        }
                    }
                }

                IsResolved = true;
            }
        }

        public override bool IsResolved { get; protected set; }

        protected void Awake()
        {
            _director = GetComponent<PlayableDirector>();
        }

        [Serializable]
        public class BindData
        {
            [SerializeField] private int _outputIndex = -1;
            [SerializeField] private string _mirrorId = string.Empty;
            [SerializeField] private string _referenceType = string.Empty;
            [SerializeField] private int _referenceTypeIndex = -1;

            public int OutputIndex
            {
                get { return _outputIndex; }
                set { _outputIndex = value; }
            }

            public string MirrorId
            {
                get { return _mirrorId; }
                set { _mirrorId = value; }
            }

            public string ReferenceType
            {
                get { return _referenceType; }
                set { _referenceType = value; }
            }

            public int ReferenceTypeIndex
            {
                get { return _referenceTypeIndex; }
                set { _referenceTypeIndex = value; }
            }
        }

        [Serializable]
        public class ReferenceData
        {
            [SerializeField] private PropertyName _exposedName;
            [SerializeField] private string _mirrorId = string.Empty;
            [SerializeField] private string _referenceType = string.Empty;
            [SerializeField] private int _referenceTypeIndex = -1;

            public PropertyName ExposedName
            {
                get { return _exposedName; }
                set { _exposedName = value; }
            }

            public string MirrorId
            {
                get { return _mirrorId; }
                set { _mirrorId = value; }
            }

            public string ReferenceType
            {
                get { return _referenceType; }
                set { _referenceType = value; }
            }

            public int ReferenceTypeIndex
            {
                get { return _referenceTypeIndex; }
                set { _referenceTypeIndex = value; }
            }
        }
    }
}