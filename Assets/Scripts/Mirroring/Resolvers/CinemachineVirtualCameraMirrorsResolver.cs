﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using Cinemachine;
using UnityEngine;

namespace DreamTeam.Mirroring.Resolvers
{
    [RequireComponent(typeof(CinemachineVirtualCamera))]
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    public partial class CinemachineVirtualCameraMirrorsResolver : BaseMirrorsResolver
    {
        [SerializeField, HideInInspector] private BindData _followBind;
        [SerializeField, HideInInspector] private BindData _lookAtBind;

        private CinemachineVirtualCamera _camera;

        public override void Resolve(MirrorObjectsRegistrator registrator, bool force)
        {
            if (!IsResolved || force)
            {
                if (_camera == null)
                {
                    _camera = GetComponent<CinemachineVirtualCamera>();
                }

                if (!string.IsNullOrEmpty(_followBind.MirrorId))
                {
                    Transform followTransform = registrator.GetInstanceObject<Transform>(_followBind.MirrorId);
                    if (followTransform != null)
                    {
                        _camera.Follow = followTransform;
                    }
                }

                if (!string.IsNullOrEmpty(_lookAtBind.MirrorId))
                {
                    Transform lookTransform = registrator.GetInstanceObject<Transform>(_lookAtBind.MirrorId);
                    if (lookTransform != null)
                    {
                        _camera.LookAt = lookTransform;
                    }
                }

                IsResolved = true;
            }
        }

        public override bool IsResolved { get; protected set; }

        protected void Awake()
        {
            _camera = GetComponent<CinemachineVirtualCamera>();
        }

        [Serializable]
        public class BindData
        {
            [SerializeField] private string _mirrorId = string.Empty;

            public string MirrorId
            {
                get { return _mirrorId; }
                set { _mirrorId = value; }
            }
        }
    }
}