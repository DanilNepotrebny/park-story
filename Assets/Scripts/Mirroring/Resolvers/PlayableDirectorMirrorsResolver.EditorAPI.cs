﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Object = UnityEngine.Object;

namespace DreamTeam.Mirroring.Resolvers
{
    public partial class PlayableDirectorMirrorsResolver : PlayableDirectorMirrorsResolver.IEditorApi
    {
        PlayableDirector IEditorApi.Director
        {
            get
            {
                if (_director == null)
                {
                    _director = GetComponent<PlayableDirector>();
                }

                return _director;
            }
        }

        List<BindData> IEditorApi.MirrorBinds => _mirrorBinds;
        List<ReferenceData> IEditorApi.MirrorReferences => _mirrorReferences;

        protected override void SaveLinksInternal()
        {
            FillBindingData();
            FillReferenceData();
        }

        private void FillBindingData()
        {
            ((IEditorApi)this).MirrorBinds.Clear();

            TimelineAsset asset = ((IEditorApi)this).Director.playableAsset as TimelineAsset;
            if (asset != null)
            {
                for (int i = 0; i < asset.outputs.Count(); i++)
                {
                    PlayableBinding binding = asset.outputs.ElementAt(i);
                    if (binding.outputTargetType != null)
                    {
                        BindData data = new BindData();
                        data.OutputIndex = i;
                        Object bind = ((IEditorApi)this).Director.GetGenericBinding(binding.sourceObject);
                        if (bind != null)
                        {
                            Type bindType = binding.outputTargetType;
                            if (bindType.IsSubclassOf(typeof(Component)))
                            {
                                Component component = bind as Component;
                                MirrorObject[] mirrorComponents = component.GetComponents<MirrorObject>();
                                foreach (MirrorObject mirror in mirrorComponents)
                                {
                                    FillElementBindingData(data, bind, mirror);
                                }
                            }
                            else
                            {
                                GameObject gameObject = bind as GameObject;
                                MirrorObject[] mirrorComponents = gameObject.GetComponents<MirrorObject>();
                                foreach (MirrorObject mirror in mirrorComponents)
                                {
                                    FillElementBindingData(data, bind, mirror);
                                }
                            }
                        }

                        ((IEditorApi)this).MirrorBinds.Add(data);
                    }
                }
            }
        }

        private void FillElementBindingData(BindData data, Object bind, MirrorObject mirror)
        {
            if (mirror != null &&
                mirror.Flow == MirrorObject.MirrorFlow.Mirror &&
                mirror.InstanceId != MirrorObjectsIdentifiersAsset.NotSetIdentifier &&
                mirror.MirroredObject == bind)
            {
                SetBindingProperties(data, mirror);
            }
        }

        private void SetBindingProperties(BindData data, MirrorObject mirror)
        {
            data.MirrorId = mirror.InstanceId;
            data.ReferenceType = mirror.MirroredObject.GetType().AssemblyQualifiedName;

            if (mirror.MirroredObject.GetType().IsSubclassOf(typeof(Component)))
            {
                Type mirroredObjectType = mirror.MirroredObject.GetType();
                List<Component> mirroredObjectComponents = mirror.GetComponents(mirroredObjectType).ToList();
                data.ReferenceTypeIndex = mirroredObjectComponents.IndexOf(mirror.MirroredObject as Component);
            }
            else
            {
                data.ReferenceTypeIndex = -1;
            }
        }

        private void FillReferenceData()
        {
            ((IEditorApi)this).MirrorReferences.Clear();

            TimelineAsset asset = ((IEditorApi)this).Director.playableAsset as TimelineAsset;
            if (asset != null)
            {
                for (int i = 0; i < asset.outputTrackCount; i++)
                {
                    PlayableBinding binding = asset.outputs.ElementAt(i);
                    TrackAsset track = binding.sourceObject as TrackAsset;
                    foreach (TimelineClip clip in track.GetClips())
                    {
                        Type clipAssetType = clip.asset.GetType();
                        List<FieldInfo> fieldsInfo = clipAssetType.GetFields(
                                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                            .ToList();
                        HashSet<object> inspected = new HashSet<object>() { clip.asset };
                        foreach (FieldInfo info in fieldsInfo)
                        {
                            InspectFieldInfo(info, clip.asset, ref inspected);
                        }
                    }
                }
            }
        }

        private void InspectFieldInfo(FieldInfo info, object obj, ref HashSet<object> inspected)
        {
            if (HasSerializationAttributes(info))
            {
                if (info.FieldType.IsGenericType &&
                    info.FieldType.GetGenericTypeDefinition() == typeof(ExposedReference<>))
                {
                    RetrieveExposedReference(info, obj);
                }
                else if (CanHaveEmbeddedField(info))
                {
                    object fieldValue = info.GetValue(obj);
                    if (!inspected.Contains(fieldValue))
                    {
                        inspected.Add(fieldValue);
                        if (typeof(IEnumerable).IsAssignableFrom(info.FieldType))
                        {
                            IEnumerable enumerable = (IEnumerable)fieldValue;
                            foreach (object child in enumerable)
                            {
                                List<FieldInfo> fieldsInfo = child.GetType()
                                    .GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                                    .ToList();
                                foreach (FieldInfo fieldInfo in fieldsInfo)
                                {
                                    InspectFieldInfo(fieldInfo, child, ref inspected);
                                }
                            }
                        }
                        else
                        {
                            List<FieldInfo> fieldsInfo = info.FieldType.GetFields(
                                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                                .ToList();
                            foreach (FieldInfo fieldInfo in fieldsInfo)
                            {
                                InspectFieldInfo(fieldInfo, fieldValue, ref inspected);
                            }
                        }
                    }
                }
            }
        }

        private void RetrieveExposedReference(FieldInfo info, object asset)
        {
            object exposedReference = info.GetValue(asset);

            FieldInfo exposedNameField = Array.Find(
                    exposedReference.GetType().GetFields(),
                    field => field.FieldType == typeof(PropertyName)
                );

            PropertyName exposedName = (PropertyName)exposedNameField.GetValue(exposedReference);

            bool isValid;
            Object reference = ((IEditorApi)this).Director.GetReferenceValue(exposedName, out isValid);
            if (isValid && reference != null)
            {
                Type referenceType = reference.GetType();
                if (referenceType.IsSubclassOf(typeof(Component)))
                {
                    Component component = reference as Component;
                    MirrorObject[] mirrorComponents = component.GetComponents<MirrorObject>();
                    foreach (MirrorObject mirror in mirrorComponents)
                    {
                        FillElementReferenceData(exposedName, reference, mirror);
                    }
                }
                else
                {
                    GameObject gameObject = reference as GameObject;
                    MirrorObject[] mirrorComponents = gameObject.GetComponents<MirrorObject>();
                    foreach (MirrorObject mirror in mirrorComponents)
                    {
                        FillElementReferenceData(exposedName, reference, mirror);
                    }
                }
            }
        }

        private void FillElementReferenceData(PropertyName exposedName, Object reference, MirrorObject mirror)
        {
            if (mirror != null &&
                mirror.Flow == MirrorObject.MirrorFlow.Mirror &&
                mirror.InstanceId != MirrorObjectsIdentifiersAsset.NotSetIdentifier &&
                mirror.MirroredObject == reference)
            {
                InsertReferenceData(exposedName, mirror);
            }
        }

        private void InsertReferenceData(PropertyName exposedName, MirrorObject mirror)
        {
            ReferenceData data = new ReferenceData();

            data.ExposedName = exposedName;
            data.MirrorId = mirror.InstanceId;
            data.ReferenceType = mirror.MirroredObject.GetType().AssemblyQualifiedName;

            if (mirror.MirroredObject.GetType().IsSubclassOf(typeof(Component)))
            {
                Type mirroredObjectType = mirror.MirroredObject.GetType();
                List<Component> mirroredObjectComponents = mirror.GetComponents(mirroredObjectType).ToList();
                data.ReferenceTypeIndex = mirroredObjectComponents.IndexOf(mirror.MirroredObject as Component);
            }
            else
            {
                data.ReferenceTypeIndex = -1;
            }

            ((IEditorApi)this).MirrorReferences.Add(data);
        }

        private bool HasSerializationAttributes(FieldInfo info)
        {
            return info.IsPublic || info.IsDefined(typeof(SerializeField));
        }

        private bool CanHaveEmbeddedField(FieldInfo info)
        {
            return
                info.FieldType.IsDefined(typeof(SerializableAttribute)) &&
                (info.FieldType.IsClass || info.FieldType.IsValueType && !info.FieldType.IsPrimitive) &&
                info.FieldType != typeof(string);
        }

        public interface IEditorApi : IEditorAPI
        {
            PlayableDirector Director { get; }
            List<BindData> MirrorBinds { get; }
            List<ReferenceData> MirrorReferences { get; }
        }
    }
}

#endif // UNITY_EDITOR