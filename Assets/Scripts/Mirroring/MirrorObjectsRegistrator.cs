﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using JetBrains.Annotations;

namespace DreamTeam.Mirroring
{
    public class MirrorObjectsRegistrator
    {
        private Dictionary<string, List<MirrorObject>> _instanceObjects = new Dictionary<string, List<MirrorObject>>();

        public void Register([NotNull] MirrorObject mirrorObject)
        {
            List<MirrorObject> mirrorObjects;
            _instanceObjects.TryGetValue(mirrorObject.InstanceId, out mirrorObjects);

            if (mirrorObjects == null)
            {
                mirrorObjects = new List<MirrorObject>();
                _instanceObjects.Add(mirrorObject.InstanceId, mirrorObjects);
            }

            mirrorObjects.Add(mirrorObject);
        }

        public void UnRegister([NotNull] MirrorObject mirrorObject)
        {
            List<MirrorObject> mirrorObjects = GetMirrorObjectsList(mirrorObject.InstanceId);
            mirrorObjects?.Remove(mirrorObject);
        }

        public MirrorObject GetMirrorObject(string instanceId)
        {
            List<MirrorObject> mirrorObjects = GetMirrorObjectsList(instanceId);

            if (mirrorObjects.Count > 1)
            {
                UnityEngine.Debug.LogError($"Found multiple instances of mirror object '{instanceId}'.");
                return null;
            }

            return mirrorObjects[0];
        }

        public IEnumerable<TInstance> GetInstanceObjects<TInstance>(string instanceId)
            where TInstance : class
        {
            return GetInstanceObjectsList<TInstance>(instanceId);
        }

        public TInstance GetInstanceObject<TInstance>(string instanceId)
            where TInstance : class
        {
            List<TInstance> mirrorObjects = GetInstanceObjectsList<TInstance>(instanceId);

            if (mirrorObjects.Count > 1)
            {
                UnityEngine.Debug.LogError($"Found multiple instances of mirror object '{instanceId}'.");
                return null;
            }

            return mirrorObjects[0];
        }

        private List<MirrorObject> GetMirrorObjectsList(string instanceId)
        {
            List<MirrorObject> mirrorObjects;
            _instanceObjects.TryGetValue(instanceId, out mirrorObjects);

            if (mirrorObjects == null)
            {
                UnityEngine.Debug.LogError($"Mirror object instance '{instanceId}' is not found.");
                return null;
            }

            if (mirrorObjects.Count == 0)
            {
                UnityEngine.Debug.LogError($"Mirror object instance '{instanceId}' is not found.");
                return null;
            }

            return mirrorObjects;
        }

        private List<TInstance> GetInstanceObjectsList<TInstance>(string instanceId)
            where TInstance : class
        {
            List<TInstance> result = new List<TInstance>();

            List<MirrorObject> mirrorObjects = GetMirrorObjectsList(instanceId);
            if (mirrorObjects != null)
            {
                foreach (MirrorObject mirrorObject in mirrorObjects)
                {
                    if (mirrorObject.MirroredObject is TInstance)
                    {
                        TInstance instance = (TInstance)(object)mirrorObject.MirroredObject;
                        result.Add(instance);
                    }
                    else
                    {
                        UnityEngine.Debug.LogError(
                                $"[PROGRAMMERS] Mirror '{instanceId}': incorrect type! " +
                                $"It is '{mirrorObject.MirroredObject.GetType().Name}' but should be '{typeof(TInstance).Name}'"
                            );
                    }
                }
            }

            return result;
        }
    }
}