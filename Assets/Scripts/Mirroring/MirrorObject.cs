﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

namespace DreamTeam.Mirroring
{
    public partial class MirrorObject : MonoBehaviour
    {
        public enum MirrorFlow
        {
            Instance,
            Mirror
        }

        [SerializeField, HideInInspector] private MirrorObjectsIdentifiersAsset _asset;
        [SerializeField, HideInInspector] private Object _mirroredObject;
        [SerializeField, HideInInspector] private MirrorFlow _mirrorFlow = MirrorFlow.Mirror;
        [SerializeField, HideInInspector] private string _instanceId = MirrorObjectsIdentifiersAsset.NotSetIdentifier;

        private MirrorObjectsRegistrator _registrator;

        public Object MirroredObject => _mirroredObject;
        public MirrorFlow Flow => _mirrorFlow;
        public string InstanceId => _instanceId;

        [Inject]
        private void Construct(MirrorObjectsRegistrator registrator)
        {
            _registrator = registrator;

            switch (Flow)
            {
                case MirrorFlow.Instance:
                    ConstructInstance();
                    break;

                case MirrorFlow.Mirror:
                    UnityEngine.Debug.LogError("[PROGRAMMERS] Mirror object was not destroyed!", gameObject);
                    break;
            }
        }

        protected void OnDestroy()
        {
            _registrator?.UnRegister(this);
        }

        private void ConstructInstance()
        {
            if (_asset != null)
            {
                if (InstanceId != MirrorObjectsIdentifiersAsset.NotSetIdentifier)
                {
                    if (_asset.Contains(InstanceId))
                    {
                        _registrator?.Register(this);
                    }
                    else
                    {
                        UnityEngine.Debug.LogWarning($"[DESIGNERS] Mirror id is not found in asset for object {gameObject.name}", gameObject);
                    }
                }
                else
                {
                    UnityEngine.Debug.LogWarning($"[DESIGNERS] Mirror id is not set for object {gameObject.name}", gameObject);
                }
            }
            else
            {
                UnityEngine.Debug.LogError("[PROGRAMMERS] mirror identifiers asset is not found!", gameObject);
            }
        }
    }
}