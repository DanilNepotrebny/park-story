// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#import <Foundation/Foundation.h>
#import <UserNotifications/UserNotifications.h>
#import "UnityAppController.h"
#import "HelpshiftAll.h"
#import "HelpshiftSupport.h"
#import "HelpshiftInstaller.h"
#import "AppsFlyerTracker.h"
#import "AppDelegateListener.h"

#define APPLICATION_ID 1445039832

@interface GameAppController : UnityAppController<UNUserNotificationCenterDelegate, AppDelegateListener>
{
    bool didEnteredBackground;
}
@end

@implementation GameAppController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        UnityRegisterAppDelegateListener(self);
    }
    
    return self;
}

- (bool) application:(UIApplication *)app continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *))restorationHandler
{
    [[AppsFlyerTracker sharedTracker] continueUserActivity:userActivity restorationHandler:restorationHandler];
    return YES;
}

-(bool) application:(UIApplication *)app openUrl:(NSURL *)url options:(NSDictionary *)options
{
    if ([[url host] isEqualToString:@"helpshift"])
    {
        NSArray *components = [[url path] componentsSeparatedByString:@"/"];
        if ([components count] == 3)
        {
            if ([[components objectAtIndex:1] isEqualToString:@"section"])
            {
                [HelpshiftSupport showFAQSection:[components objectAtIndex:2] withController:[UIApplication sharedApplication].keyWindow.rootViewController withConfig:nil];
            }
            else if ([[components objectAtIndex:1] isEqualToString:@"faq"])
            {
                [HelpshiftSupport showSingleFAQ:[components objectAtIndex:2] withController:[UIApplication sharedApplication].keyWindow.rootViewController withConfig:nil];
            }
        }
    }
    
    [[AppsFlyerTracker sharedTracker] handleOpenUrl:url options:options];
    
    return true;
}

- (void) application:(UIApplication *)app onOpenURL:(NSNotification *)notification
{
    NSURL *url = notification.userInfo[@"url"];
    NSString *sourceApplication = notification.userInfo[@"sourceApplication"];
    
    if (sourceApplication == nil)
    {
        sourceApplication = @"";
    }
    
    if (url != nil)
    {
        [[AppsFlyerTracker sharedTracker] handleOpenURL:url sourceApplication:sourceApplication withAnnotation:nil];
    }
}

- (void) application:(UIApplication *)app didReceiveRemoteNotification:(NSNotification*)notification
{
    [[AppsFlyerTracker sharedTracker] handlePushNotification:notification.userInfo];
    
    if (![HelpshiftCore handleRemoteNotification:notification.userInfo isAppLaunch:false withController:self.window.rootViewController])
    {
        [super application:app didReceiveRemoteNotification:notification.userInfo];
    }
}

- (void) application:(UIApplication *)app didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    if (![HelpshiftCore handleRemoteNotification:userInfo isAppLaunch:false withController:self.window.rootViewController])
    {
        [super application:app didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
    }
}

- (void) application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notification
{
    if (![HelpshiftCore handleLocalNotification:notification withController:[UIApplication sharedApplication].keyWindow.rootViewController])
    {
        [super application:app didReceiveLocalNotification:notification];
    }
}

- (void) application:(UIApplication *)app handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler
{
    if (![HelpshiftCore handleInteractiveRemoteNotification:userInfo forAction:identifier completionHandler:completionHandler])
    {
        completionHandler();
    }
}

- (void) application:(UIApplication *)app handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler
{
    if (![HelpshiftCore handleInteractiveLocalNotification:notification forAction:identifier completionHandler:completionHandler])
    {
        completionHandler();
    }
}

- (void) userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    if ([[notification.request.content.userInfo objectForKey:@"origin"] isEqualToString:@"helpshift"])
    {
        [HelpshiftCore handleNotificationWithUserInfoDictionary:notification.request.content.userInfo isAppLaunch:false withController:self.window.rootViewController];
    }
    
    completionHandler(UNNotificationPresentationOptionNone);
}

- (void) userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler
{
    if ([[response.notification.request.content.userInfo objectForKey:@"origin"] isEqualToString:@"helpshift"])
    {
        [HelpshiftCore handleNotificationResponseWithActionIdentifier:response.actionIdentifier userInfo:response.notification.request.content.userInfo completionHandler:completionHandler];
    }
}

- (void) didFinishLaunching:(NSNotification *)notification
{
    if (notification.userInfo[@"url"])
    {
        [self onOpenURL:notification];
    }
}

- (bool) application:(UIApplication *)app didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    BOOL defaultConfigInstall = [HelpshiftUnityInstaller installDefault];
    if (defaultConfigInstall == NO)
    {
        NSLog(@"GameAppController: Helpshift was not initialized. Default config is not set correctly");
        // Default config is not set correctly. If you intend to use default configuration, please check api key, app id and
        // domain name are set correctly in the default config. Otherwise you can initialize Helpshift with custom
        // configuration by uncommenting the code below.
    }
    
    /*
    Uncomment the following code block to register for push notifications using UNUserNotification framework.
    */
    /*
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert)
    completionHandler:^(BOOL granted, NSError *_Nullable error) {
    if(error) {
    NSLog(@"Error while requesting Notification permissions.");
    }
    }];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    */
    
    /* Uncomment the following code block to register for push notification using UILocalNotification framework.
    * Please make sure that you are registering only from one framwork, Either from UNUserNotification or from UILocalNotification framework.
    */
    
    /**
    UIUserNotificationType notificationType = UIUserNotificationTypeBadge | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:notificationType categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    */
    
    return [super application:app didFinishLaunchingWithOptions:launchOptions];
}

- (void) didBecomeActive:(NSNotification*)notification
{
    if (didEnteredBackground == YES)
    {
        [[AppsFlyerTracker sharedTracker] trackAppLaunch];
        didEnteredBackground = NO;
    }
}

- (void) didEnterBackground:(NSNotification*)notification
{
    didEnteredBackground = YES;
}

- (void) application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [super application:app didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
    
    [HelpshiftCore registerDeviceToken:deviceToken];
}

- (void) application:(UIApplication *)app handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler
{
    if (![HelpshiftCore handleEventsForBackgroundURLSession:identifier completionHandler:completionHandler])
    {
        completionHandler();
    }
}

- (IBAction) openGamePageOnAppStore
{
    NSString *link = [NSString stringWithFormat: @"itms-apps://itunes.apple.com/app/id%d", APPLICATION_ID];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
}

@end

extern "C"
{
    void _openGamePageOnAppStore()
    {
        GameAppController* gameAppController = [(UIApplication*)[UIApplication sharedApplication] delegate];
        [gameAppController openGamePageOnAppStore];
    }
}

IMPL_APP_CONTROLLER_SUBCLASS(GameAppController)
