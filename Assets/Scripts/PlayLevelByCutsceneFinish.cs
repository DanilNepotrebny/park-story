﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Cutscenes;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam
{
    public class PlayLevelByCutsceneFinish : MonoBehaviour
    {
        [SerializeField] private SceneReference _cutscene;

        #if !NO_MATCH3

        [Inject(Id = GlobalInstaller.MainLevelPackControllerId)]
        private SingleLevelPackController _levelPackController;
        [Inject] private CutsceneSystem _cutsceneSystem;
        [Inject] private LevelSystem _levelSystem;

        protected void Awake()
        {
            _cutsceneSystem.CutsceneFinished += OnCutsceneFinished;
        }

        private void OnCutsceneFinished(SceneReference cutscene)
        {
            if (_cutscene.AssetPath == cutscene.AssetPath)
            {
                _levelSystem.LoadLevel(_levelPackController.CurrentLevel);
            }
        }

        #endif
    }
}