﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.Match3;
using NUnit.Framework;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using Object = UnityEngine.Object;

namespace DreamTeam.Tests
{
    public class MagicHatComponentTest
    {
        [UnityTest]
        public IEnumerator TestMagicHatConsumption(
                [Files("Assets/Scenes/Tests/MagicHatComponent", "*.unity")]
                string scenePath
            )
        {
            SceneManager.LoadScene(scenePath);
            yield return null;

            List<CellComponent> cellsToCheck = new List<CellComponent>();
            foreach (CellComponent cell in Object.FindObjectOfType<GridSystem>().Cells)
            {
                if (cell.Chip != null)
                {
                    cellsToCheck.Add(cell);
                }
            }

            TestComponent[] testComponents = Object.FindObjectsOfType<TestComponent>();
            ChipComponent magicHat = Array.Find(testComponents, t => t.Id == 1)
                .GetComponent<CellComponent>()
                .Chip;

            magicHat.ScheduleActivation().Perform();

            foreach (CellComponent cell in cellsToCheck)
            {
                Assert.AreEqual(
                        cell.GetComponent<TestComponent>() != null,
                        !cell.IsStable,
                        $"Incorrect chip behaviour at {cell}"
                    );
            }
        }
    }
}