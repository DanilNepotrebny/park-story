﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Match3;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace DreamTeam.Tests
{
    public class MatchSystemTest
    {
        [UnityTest]
        public IEnumerator TestMatch(
                [Files("Assets/Scenes/Tests/MatchSystem", "*.unity")]
                string scenePath
            )
        {
            SceneManager.LoadScene(scenePath);
            yield return null;

            int matchCounter = Object.FindObjectsOfType<TestComponent>().Length;

            foreach (CellComponent cell in Object.FindObjectOfType<GridSystem>().Cells)
            {
                if (!cell.IsStable)
                {
                    matchCounter--;
                }
            }

            Assert.Zero(matchCounter);
        }
    }
}