﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace DreamTeam.Tests
{
    /// <summary>
    /// Utility component for marking objects to be used in tests.
    /// </summary>
    public class TestComponent : MonoBehaviour
    {
        [SerializeField] private int _id;

        /// <summary>
        /// Gets the identifier that can be used in a test (e.g. for grouping objects)
        /// </summary>
        public int Id => _id;

        #if UNITY_EDITOR
        protected void OnDrawGizmos()
        {
            var style = new GUIStyle();
            style.fontSize = 20;
            Handles.Label(transform.position, $"T{_id}", style);
        }
        #endif
    }
}