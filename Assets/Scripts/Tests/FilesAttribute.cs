﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.IO;
using NUnit.Framework;

namespace DreamTeam.Tests
{
    /// <summary>
    /// Provides paths to all the files in the specified folder for an individual parameter of a test.
    /// </summary>
    /// <remarks>
    /// Unity doesn't automatically refresh test list when files in the folder change. You need to recreate the Test
    /// runner window or to change current mode (Play/Edit).
    /// </remarks>
    [AttributeUsage(AttributeTargets.Parameter)]
    public class FilesAttribute : ValuesAttribute
    {
        /// <summary>
        /// Constructs with a folder path and search pattern.
        /// </summary>
        public FilesAttribute(string folder, string pattern, bool includeExtension = true, string basePath = "") :
            base(GetFileNames(folder, pattern, includeExtension, basePath))
        {
        }

        private static string[] GetFileNames(string folder, string pattern, bool includeExtension, string basePath)
        {
            string[] fileNames = Directory.GetFiles(Path.Combine(basePath, folder), pattern);
            for (int i = 0; i < fileNames.Length; i++)
            {
                if (!includeExtension)
                {
                    fileNames[i] = Path.ChangeExtension(fileNames[i], null);
                }

                if (basePath != string.Empty)
                {
                    fileNames[i] = fileNames[i].Replace(basePath + Path.DirectorySeparatorChar, string.Empty);
                }

                fileNames[i] = fileNames[i].Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
            }

            return fileNames;
        }
    }
}