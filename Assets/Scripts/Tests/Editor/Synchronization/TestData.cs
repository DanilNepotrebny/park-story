﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Tests.Editor.Synchronization
{
    public static class TestData
    {
        public const string keyA = "Key A";
        public const string keyB = "Key B";
        public const string keyC = "Key C";
        public const string keyD = "Key D";

        public const string stringA = "A";
        public const string stringB = "B";
        public const string stringC = "C";
        public const float floatA = -134.2f;
        public const float floatB = 22.4f;
        public const float floatC = 0.0f;
        public const int intA = 1002;
        public const int intB = -223;
        public const int intC = 199;

        public static readonly bool[] boolListA = { true, false, true, false, true };
        public static readonly bool[] boolListB = { false, false, true, true, true };
        public static readonly bool[] boolListSmaller = { false, true, false };
        public static readonly bool[] boolListLarger = { true, true, false, false, false, true, true, false };

        public static readonly int[] intListA = { 1, 2, 3, 4, 5 };
        public static readonly int[] intListB = { -10, -20, -30, -40, 145 };
        public static readonly int[] intListSmaller = { -10, -20, -30 };
        public static readonly int[] intListLarger = { 100, 230, -30, -40, 145, -130, 20, 30, -40, 15 };

        public static readonly float[] floatListA = { 1f, 2f, 3f, 4f, 5f };
        public static readonly float[] floatListB = { -10f, -20f, -30f, -40f, 145f };
        public static readonly float[] floatListSmaller = { -10f, -20f, -30f };
        public static readonly float[] floatListLarger = { 100f, 230f, -30f, -40f, 145f, -130f, 20f, 30f, -40f, 15f };

        public static readonly string[] stringListA = { "A1", "B1", "C1", "D1", "E1" };
        public static readonly string[] stringListB = { "A2", "B2", "C2", "D2", "E2" };
        public static readonly string[] stringListSmaller = { "A", "B", "C" };
        public static readonly string[] stringListLarger = { "A", "B", "C", "D", "E", "B1", "C1", "D1", "E" };
    }
}