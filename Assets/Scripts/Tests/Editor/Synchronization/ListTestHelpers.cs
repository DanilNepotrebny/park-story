﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Synchronization.States;

namespace DreamTeam.Tests.Editor.Synchronization
{
    public interface IListTestHelper<TList>
    {
        void Initialize(out TList listA, out TList listB, out TList listSmaller, out TList listLarger);

        void SyncList(State state, string id, ref TList list, bool isResizeAllowed);
        void SyncListElement(State state, ref TList list, bool isResizeAllowed);
    }

    public class BoolArrayTestHelper : IListTestHelper<bool[]>
    {
        public void Initialize(
                out bool[] listA,
                out bool[] listB,
                out bool[] listSmaller,
                out bool[] listLarger
            )
        {
            listA = TestData.boolListA.Clone() as bool[];
            listB = TestData.boolListB.Clone() as bool[];
            listSmaller = TestData.boolListSmaller.Clone() as bool[];
            listLarger = TestData.boolListLarger.Clone() as bool[];
        }

        public void SyncList(State state, string id, ref bool[] list, bool isResizeAllowed)
        {
            state.SyncBoolArray(id, ref list, isResizeAllowed);
        }

        public void SyncListElement(State state, ref bool[] list, bool isResizeAllowed)
        {
            state.SyncBoolArrayElement(ref list, isResizeAllowed);
        }
    }

    public class IntArrayTestHelper : IListTestHelper<int[]>
    {
        public void Initialize(out int[] listA, out int[] listB, out int[] listSmaller, out int[] listLarger)
        {
            listA = TestData.intListA.Clone() as int[];
            listB = TestData.intListB.Clone() as int[];
            listSmaller = TestData.intListSmaller.Clone() as int[];
            listLarger = TestData.intListLarger.Clone() as int[];
        }

        public void SyncList(State state, string id, ref int[] list, bool isResizeAllowed)
        {
            state.SyncIntArray(id, ref list, isResizeAllowed);
        }

        public void SyncListElement(State state, ref int[] list, bool isResizeAllowed)
        {
            state.SyncIntArrayElement(ref list, isResizeAllowed);
        }
    }

    public class FloatArrayTestHelper : IListTestHelper<float[]>
    {
        public void Initialize(out float[] listA, out float[] listB, out float[] listSmaller, out float[] listLarger)
        {
            listA = TestData.floatListA.Clone() as float[];
            listB = TestData.floatListB.Clone() as float[];
            listSmaller = TestData.floatListSmaller.Clone() as float[];
            listLarger = TestData.floatListLarger.Clone() as float[];
        }

        public void SyncList(State state, string id, ref float[] list, bool isResizeAllowed)
        {
            state.SyncFloatArray(id, ref list, isResizeAllowed);
        }

        public void SyncListElement(State state, ref float[] list, bool isResizeAllowed)
        {
            state.SyncFloatArrayElement(ref list, isResizeAllowed);
        }
    }

    public class StringArrayTestHelper : IListTestHelper<string[]>
    {
        public void Initialize(
            out string[] listA,
            out string[] listB,
            out string[] listSmaller,
            out string[] listLarger)
        {
            listA = TestData.stringListA.Clone() as string[];
            listB = TestData.stringListB.Clone() as string[];
            listSmaller = TestData.stringListSmaller.Clone() as string[];
            listLarger = TestData.stringListLarger.Clone() as string[];
        }

        public void SyncList(State state, string id, ref string[] list, bool isResizeAllowed)
        {
            state.SyncStringArray(id, ref list, isResizeAllowed);
        }

        public void SyncListElement(State state, ref string[] list, bool isResizeAllowed)
        {
            state.SyncStringArrayElement(ref list, isResizeAllowed);
        }
    }

    public class BoolListTestHelper : IListTestHelper<List<bool>>
    {
        public void Initialize(
                out List<bool> listA,
                out List<bool> listB,
                out List<bool> listSmaller,
                out List<bool> listLarger
            )
        {
            listA = new List<bool>(TestData.boolListA);
            listB = new List<bool>(TestData.boolListB);
            listSmaller = new List<bool>(TestData.boolListSmaller);
            listLarger = new List<bool>(TestData.boolListLarger);
        }

        public void SyncList(State state, string id, ref List<bool> list, bool isResizeAllowed)
        {
            state.SyncBoolList(id, ref list, isResizeAllowed);
        }

        public void SyncListElement(State state, ref List<bool> list, bool isResizeAllowed)
        {
            state.SyncBoolListElement(ref list, isResizeAllowed);
        }
    }

    public class IntListTestHelper : IListTestHelper<List<int>>
    {
        public void Initialize(
            out List<int> listA,
            out List<int> listB,
            out List<int> listSmaller,
            out List<int> listLarger)
        {
            listA = new List<int>(TestData.intListA);
            listB = new List<int>(TestData.intListB);
            listSmaller = new List<int>(TestData.intListSmaller);
            listLarger = new List<int>(TestData.intListLarger);
        }

        public void SyncList(State state, string id, ref List<int> list, bool isResizeAllowed)
        {
            state.SyncIntList(id, ref list, isResizeAllowed);
        }

        public void SyncListElement(State state, ref List<int> list, bool isResizeAllowed)
        {
            state.SyncIntListElement(ref list, isResizeAllowed);
        }
    }

    public class FloatListTestHelper : IListTestHelper<List<float>>
    {
        public void Initialize(
            out List<float> listA,
            out List<float> listB,
            out List<float> listSmaller,
            out List<float> listLarger)
        {
            listA = new List<float>(TestData.floatListA);
            listB = new List<float>(TestData.floatListB);
            listSmaller = new List<float>(TestData.floatListSmaller);
            listLarger = new List<float>(TestData.floatListLarger);
        }

        public void SyncList(State state, string id, ref List<float> list, bool isResizeAllowed)
        {
            state.SyncFloatList(id, ref list, isResizeAllowed);
        }

        public void SyncListElement(State state, ref List<float> list, bool isResizeAllowed)
        {
            state.SyncFloatListElement(ref list, isResizeAllowed);
        }
    }

    public class StringListTestHelper : IListTestHelper<List<string>>
    {
        public void Initialize(
            out List<string> listA,
            out List<string> listB,
            out List<string> listSmaller,
            out List<string> listLarger)
        {
            listA = new List<string>(TestData.stringListA);
            listB = new List<string>(TestData.stringListB);
            listSmaller = new List<string>(TestData.stringListSmaller);
            listLarger = new List<string>(TestData.stringListLarger);
        }

        public void SyncList(State state, string id, ref List<string> list, bool isResizeAllowed)
        {
            state.SyncStringList(id, ref list, isResizeAllowed);
        }

        public void SyncListElement(State state, ref List<string> list, bool isResizeAllowed)
        {
            state.SyncStringListElement(ref list, isResizeAllowed);
        }
    }
}