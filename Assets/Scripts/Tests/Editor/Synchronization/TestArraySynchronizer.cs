﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.Nodes;

namespace DreamTeam.Tests.Editor.Synchronization
{
    public class TestArraySynchronizer<T> : ISynchronizer<T>
        where T : class, ISynchronizable
    {
        private readonly T[] _referenceSynchronizables;

        public TestArraySynchronizer(
                T[] referenceSynchronizables
            )
        {
            _referenceSynchronizables = referenceSynchronizables;
        }

        public Node Serialize(T synchronizable)
        {
            return new ValueNode(Array.IndexOf(_referenceSynchronizables, synchronizable));
        }

        public T Deserialize(Node node)
        {
            return _referenceSynchronizables[((ValueNode)node).GetInt()];
        }
    }
}