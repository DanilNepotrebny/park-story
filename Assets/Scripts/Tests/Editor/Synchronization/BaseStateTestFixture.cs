﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization
{
    public class BaseStateTestFixture
    {
        protected string stringA;
        protected string stringB;
        protected string stringC;
        protected float floatA;
        protected float floatB;
        protected float floatC;
        protected int intA;
        protected int intB;
        protected int intC;

        [SetUp]
        public virtual void Setup()
        {
            stringA = TestData.stringA;
            stringB = TestData.stringB;
            stringC = TestData.stringC;
            floatA = TestData.floatA;
            floatB = TestData.floatB;
            floatC = TestData.floatC;
            intA = TestData.intA;
            intB = TestData.intB;
            intC = TestData.intC;
        }
    }
}