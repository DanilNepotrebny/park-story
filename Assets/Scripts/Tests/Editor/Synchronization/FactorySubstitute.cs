﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Synchronization;
using DreamTeam.Synchronization.Nodes;
using NSubstitute;

namespace DreamTeam.Tests.Editor.Synchronization
{
    public static class FactorySubstitute
    {
        public static ISynchronizationFactory<T> Create<T>() where T : class, ISynchronizable
        {
            var factory = Substitute.For<ISynchronizationFactory<T>>();
            factory.Serialize(Arg.Any<T>()).Returns(call => Substitute.For<Node>());
            factory.Deserialize(Arg.Any<Node>()).Returns(call => Substitute.For<T>());
            return factory;
        }
    }
}