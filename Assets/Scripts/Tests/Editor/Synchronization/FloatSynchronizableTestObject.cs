﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;

namespace DreamTeam.Tests.Editor.Synchronization
{
    public class FloatSynchronizableTestObject : ISynchronizable
    {
        public const string valuesId = "values";

        private float[] _values;

        public float[] Values => _values;

        public FloatSynchronizableTestObject(float[] values)
        {
            _values = values.Clone() as float[];
        }

        public void Sync(State state)
        {
            state.SyncFloatArray(valuesId, ref _values, true);
        }
    }
}