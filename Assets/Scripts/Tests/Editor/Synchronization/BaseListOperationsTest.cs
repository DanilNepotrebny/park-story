﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization
{
    public abstract class BaseListOperationsTest<T, TList, TListHelper>
        where TList : IEnumerable<T>
        where TListHelper : IListTestHelper<TList>, new()
    {
        protected TList listA;
        protected TList listB;
        protected TList listSmaller;
        protected TList listLarger;

        protected IListTestHelper<TList> Helper { get; }

        protected BaseListOperationsTest()
        {
            Helper = new TListHelper();
        }

        [SetUp]
        public virtual void Setup()
        {
            Helper.Initialize(out listA, out listB, out listSmaller, out listLarger);
        }
    }
}