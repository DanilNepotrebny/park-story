﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization.Nodes;
using DreamTeam.Synchronization.States;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization
{
    [TestFixture(typeof(bool), typeof(bool[]), typeof(BoolArrayTestHelper))]
    [TestFixture(typeof(int), typeof(int[]), typeof(IntArrayTestHelper))]
    [TestFixture(typeof(float), typeof(float[]), typeof(FloatArrayTestHelper))]
    [TestFixture(typeof(string), typeof(string[]), typeof(StringArrayTestHelper))]
    [TestFixture(typeof(bool), typeof(List<bool>), typeof(BoolListTestHelper))]
    [TestFixture(typeof(int), typeof(List<int>), typeof(IntListTestHelper))]
    [TestFixture(typeof(float), typeof(List<float>), typeof(FloatListTestHelper))]
    [TestFixture(typeof(string), typeof(List<string>), typeof(StringListTestHelper))]
    public class DeserializationListTest<T, TList, TListHelper> : BaseListOperationsTest<T, TList, TListHelper>
        where TList : class, IEnumerable<T>
        where TListHelper : IListTestHelper<TList>, new()
    {
        protected DeserializationState state;
        protected DictionaryNode root;

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            root = new DictionaryNode();
        }

        [Test]
        public void ValidSyncSameSizeWithoutResize()
        {
            var listNode = root.SetList(TestData.keyA);
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            Helper.SyncList(state, TestData.keyA, ref listB, false);
            Assert.IsTrue(
                    listA.SequenceEqual(listB),
                    $"expected:[{string.Join(", ", listA)}] " +
                    $"actual:[{string.Join(", ", listB)}]"
                );
        }

        [Test]
        public void ValidSyncSameSizeWithResize()
        {
            var listNode = root.SetList(TestData.keyA);
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            Helper.SyncList(state, TestData.keyA, ref listB, true);
            Assert.IsTrue(
                    listA.SequenceEqual(listB),
                    $"expected:[{string.Join(", ", listA)}] " +
                    $"actual:[{string.Join(", ", listB)}]"
                );
        }

        [Test]
        public void ValidSyncNulledWithResize()
        {
            var listNode = root.SetList(TestData.keyA);
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            listB = null;

            Helper.SyncList(state, TestData.keyA, ref listB, true);
            Assert.IsTrue(
                    listA.SequenceEqual(listB),
                    $"expected:[{string.Join(", ", listA)}] " +
                    $"actual:[{string.Join(", ", listB)}]"
                );
        }

        [Test]
        public void InvalidSyncNulledWithoutResize()
        {
            var listNode = root.SetList(TestData.keyA);
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            listB = null;

            Assert.Throws<InvalidOperationException>(
                    () => Helper.SyncList(state, TestData.keyA, ref listB, false)
                );
        }

        [Test]
        public void InvalidNullDataSyncWithoutResize()
        {
            root.SetNull(TestData.keyA);
            SetupState();

            Assert.Throws<InvalidOperationException>(
                    () => Helper.SyncList(state, TestData.keyA, ref listB, false)
                );
        }

        [Test]
        public void ValidNullDataSyncWithResize()
        {
            root.SetNull(TestData.keyA);
            SetupState();

            Helper.SyncList(state, TestData.keyA, ref listB, true);
            Assert.IsNull(listB);
        }

        [Test]
        public void ValidSyncSmallerWithoutResize()
        {
            var listNode = root.SetList(TestData.keyA);
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            var expected = listA.Take(listSmaller.Count());

            Helper.SyncList(state, TestData.keyA, ref listSmaller, false);

            Assert.IsTrue(
                    expected.SequenceEqual(listSmaller),
                    $"expected:[{string.Join(", ", expected)}] " +
                    $"actual:[{string.Join(", ", listSmaller)}]"
                );
        }

        [Test]
        public void ValidSyncSmallerWithResize()
        {
            var listNode = root.SetList(TestData.keyA);
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            Helper.SyncList(state, TestData.keyA, ref listSmaller, true);
            Assert.IsTrue(
                    listA.SequenceEqual(listSmaller),
                    $"expected:[{string.Join(", ", listA)}] " +
                    $"actual:[{string.Join(", ", listSmaller)}]"
                );
        }

        [Test]
        public void ValidSyncLargerWithoutResize()
        {
            var listNode = root.SetList(TestData.keyA);
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            var expected = listA.Concat(listLarger.Skip(listA.Count()));

            Helper.SyncList(state, TestData.keyA, ref listLarger, false);
            Assert.IsTrue(
                    expected.SequenceEqual(listLarger),
                    $"expected:[{string.Join(", ", expected)}] " +
                    $"actual:[{string.Join(", ", listLarger)}]"
                );
        }

        [Test]
        public void ValidSyncLargerWithResize()
        {
            var listNode = root.SetList(TestData.keyA);
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            Helper.SyncList(state, TestData.keyA, ref listLarger, true);
            Assert.IsTrue(
                    listA.SequenceEqual(listLarger),
                    $"expected:[{string.Join(", ", listA)}] " +
                    $"actual:[{string.Join(", ", listLarger)}]"
                );
        }

        [TestCase(true)]
        [TestCase(false)]
        public void InvalidDictionaryKeySync(bool isResizeAllowed)
        {
            SetupState();

            var oldValues = listB.ToArray();

            Helper.SyncList(state, TestData.keyA, ref listB, isResizeAllowed);

            Assert.IsTrue(
                    oldValues.SequenceEqual(listB),
                    $"expected:[{string.Join(", ", oldValues)}] " +
                    $"actual:[{string.Join(", ", listB)}]"
                );
        }

        [TestCase(true)]
        [TestCase(false)]
        public void InvalidDictionarySyncOnList(bool isResizeAllowed)
        {
            root.SetList(TestData.keyA);
            SetupState();

            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(
                () => Helper.SyncList(state, TestData.keyA, ref listB, isResizeAllowed));
        }

        [Test, Combinatorial]
        public void InvalidListSyncOnValue<TValue>(
                [Values(TestData.intA, TestData.floatA, TestData.stringA)]
                TValue value,
                [Values(true, false)] bool isResizeAllowed
            )
        {
            root.SetValue(TestData.keyA, value);
            SetupState();

            Assert.Throws<InvalidOperationException>(
                () => Helper.SyncList(state, TestData.keyA, ref listLarger, isResizeAllowed));
        }

        [Test]
        public void InvalidListSyncWithDictionaryNode([Values(true, false)] bool isResizeAllowed)
        {
            root.SetDictionary(TestData.keyA);
            SetupState();

            Assert.Throws<InvalidOperationException>(
                () => Helper.SyncList(state, TestData.keyA, ref listLarger, isResizeAllowed));
        }

        [Test]
        public void InvalidListSyncWithDifferentTypes([Values(true, false)] bool isResizeAllowed)
        {
            var listNode = root.SetList(TestData.keyA);
            listNode.AddInt(TestData.intA);
            listNode.AddInt(TestData.intB);
            listNode.AddFloat(TestData.floatA);
            listNode.AddFloat(TestData.floatB);
            listNode.AddString(TestData.stringA);
            SetupState();

            Assert.Throws<InvalidOperationException>(
                    () => Helper.SyncList(state, TestData.keyA, ref listA, isResizeAllowed)
                );
        }

        [Test]
        public void ValidSyncElementSameSizeWithoutResize()
        {
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            state.BeginList(TestData.keyA);
            Helper.SyncListElement(state, ref listB, false);
            state.EndList();

            Assert.IsTrue(
                    listA.SequenceEqual(listB),
                    $"expected:[{string.Join(", ", listA)}] " +
                    $"actual:[{string.Join(", ", listB)}]"
                );
        }

        [Test]
        public void ValidSyncElementSameSizeWithResize()
        {
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            state.BeginList(TestData.keyA);
            Helper.SyncListElement(state, ref listB, true);
            state.EndList();

            Assert.IsTrue(
                    listA.SequenceEqual(listB),
                    $"expected:[{string.Join(", ", listA)}] " +
                    $"actual:[{string.Join(", ", listB)}]"
                );
        }

        [Test]
        public void ValidSyncNulledElementWithResize()
        {
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            listB = null;

            state.BeginList(TestData.keyA);
            Helper.SyncListElement(state, ref listB, true);
            state.EndList();

            Assert.IsTrue(
                    listA.SequenceEqual(listB),
                    $"expected:[{string.Join(", ", listA)}] " +
                    $"actual:[{string.Join(", ", listB)}]"
                );
        }

        [Test]
        public void InvalidSyncNulledElementWithoutResize()
        {
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            listB = null;

            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(
                    () => Helper.SyncListElement(state, ref listB, false)
                );
        }

        [Test]
        public void InvalidNullDataSyncElementWithoutResize()
        {
            root.SetList(TestData.keyA).AddNull();
            SetupState();

            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(
                    () => Helper.SyncList(state, TestData.keyA, ref listB, false)
                );
        }

        [Test]
        public void ValidNullDataSyncElementWithResize()
        {
            root.SetList(TestData.keyA).AddNull();
            SetupState();

            state.BeginList(TestData.keyA);
            Helper.SyncListElement(state, ref listB, true);
            state.EndList();

            Assert.IsNull(listB);
        }

        [Test]
        public void ValidSyncSmallerElementWithoutResize()
        {
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            var expected = listA.Take(listSmaller.Count());

            state.BeginList(TestData.keyA);
            Helper.SyncListElement(state, ref listSmaller, false);
            state.EndList();

            Assert.IsTrue(
                    expected.SequenceEqual(listSmaller),
                    $"expected:[{string.Join(", ", expected)}] " +
                    $"actual:[{string.Join(", ", listSmaller)}]"
                );
        }

        [Test]
        public void ValidSyncSmallerElementWithResize()
        {
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            state.BeginList(TestData.keyA);
            Helper.SyncListElement(state, ref listSmaller, true);
            state.EndList();

            Assert.IsTrue(
                    listA.SequenceEqual(listSmaller),
                    $"expected:[{string.Join(", ", listA)}] " +
                    $"actual:[{string.Join(", ", listSmaller)}]"
                );
        }

        [Test]
        public void ValidSyncLargerElementWithoutResize()
        {
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            var expected = listA.Concat(listLarger.Skip(listA.Count()));

            state.BeginList(TestData.keyA);
            Helper.SyncListElement(state, ref listLarger, false);
            state.EndList();

            Assert.IsTrue(
                    expected.SequenceEqual(listLarger),
                    $"expected:[{string.Join(", ", expected)}] " +
                    $"actual:[{string.Join(", ", listLarger)}]"
                );
        }

        [Test]
        public void ValidSyncLargerElementWithResize()
        {
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var e in listA)
            {
                listNode.AddValue(e);
            }

            SetupState();

            state.BeginList(TestData.keyA);
            Helper.SyncListElement(state, ref listLarger, true);
            state.EndList();

            Assert.IsTrue(
                    listA.SequenceEqual(listLarger),
                    $"expected:[{string.Join(", ", listA)}] " +
                    $"actual:[{string.Join(", ", listLarger)}]"
                );
        }

        [TestCase(false)]
        [TestCase(true)]
        public void ValidListElementAndFloatSync(bool isResizeAllowed)
        {
            var listNode = root.SetList(TestData.keyA);

            var childListNode = listNode.AddList();
            foreach (var e in listA)
            {
                childListNode.AddValue(e);
            }

            listNode.AddFloat(TestData.floatA);

            SetupState();

            state.BeginList(TestData.keyA);
            Helper.SyncListElement(state, ref listB, isResizeAllowed);
            float actualFloat = TestData.floatB;
            state.SyncFloatElement(ref actualFloat);
            state.EndList();
            Assert.IsTrue(
                    listA.SequenceEqual(listB),
                    $"expected:[{string.Join(", ", listA)}] " +
                    $"actual:[{string.Join(", ", listB)}]"
                );
            Assert.AreEqual(TestData.floatA, actualFloat);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void InvalidIndexListElementSync(bool isResizeAllowed)
        {
            root.SetList(TestData.keyA);
            SetupState();

            var oldValues = listB.ToArray();

            state.BeginList(TestData.keyA);
            Helper.SyncListElement(state, ref listB, isResizeAllowed);
            state.EndList();

            Assert.IsTrue(
                    oldValues.SequenceEqual(listB),
                    $"expected:[{string.Join(", ", oldValues)}] " +
                    $"actual:[{string.Join(", ", listB)}]"
                );
        }

        [TestCase(true)]
        [TestCase(false)]
        public void InvalidListElementSyncOnDictionary(bool isResizeAllowed)
        {
            root.SetDictionary(TestData.keyA);
            SetupState();

            state.BeginDictionary(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => Helper.SyncListElement(state, ref listB, isResizeAllowed));
        }

        [Test, Combinatorial]
        public void InvalidSyncElementOnValue<TValue>(
                [Values(TestData.intA, TestData.floatA, TestData.stringA)]
                TValue value,
                [Values(true, false)] bool isResizeAllowed
            )
        {
            root.SetList(TestData.keyA).AddValue(value);
            SetupState();
            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => Helper.SyncListElement(state, ref listB, isResizeAllowed));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void InvalidListElementSyncWithDictionaryNode(bool isResizeAllowed)
        {
            root.SetList(TestData.keyA).AddDictionary();
            SetupState();

            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(
                    () => Helper.SyncListElement(state, ref listB, isResizeAllowed)
                );
        }

        [TestCase(true)]
        [TestCase(false)]
        public void InvalidListElementSyncWithDifferentTypes(bool isResizeAllowed)
        {
            var listNode = root.SetList(TestData.keyA).AddList();
            listNode.AddInt(TestData.intA);
            listNode.AddInt(TestData.intB);
            listNode.AddFloat(TestData.floatA);
            listNode.AddFloat(TestData.floatB);
            listNode.AddString(TestData.stringA);
            SetupState();

            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => Helper.SyncListElement(state, ref listB, isResizeAllowed));
        }

        protected void SetupState()
        {
            state = new DeserializationState(root);
        }
    }
}