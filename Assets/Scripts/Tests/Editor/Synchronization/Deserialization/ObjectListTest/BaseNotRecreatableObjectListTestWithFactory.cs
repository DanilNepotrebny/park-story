﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Synchronization;
using NSubstitute;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization.ObjectListTest
{
    public abstract class BaseNotRecreatableObjectListTestWithFactory<TList> : BaseObjectListTestWithFactory<TList>
        where TList : class, IEnumerable<ISynchronizable>
    {
        [Test]
        public void InvalidSyncNullObjectArrayWithFactory()
        {
            var listNode = root.SetList(TestData.keyA);
            foreach (var obj in _arrayA)
            {
                var objectDict = listNode.AddDictionary();
                objectDict.SetDictionary("Factory");
                var objectData = objectDict.SetDictionary("Object");
                var valuesList = objectData.SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            _arrayB = null;

            var factory = Substitute.For<ISynchronizationFactory<ISynchronizable>>();
            factory.Deserialize(null)
                .ReturnsForAnyArgs(
                        info => new FloatSynchronizableTestObject(TestData.floatListA)
                    );

            Assert.Throws<InvalidOperationException>(
                    () => SyncObjectList(TestData.keyA, ref _arrayB, factory)
                );
        }

        [Test]
        public void InvalidSyncNullObjectArrayElementWithFactory()
        {
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var obj in _arrayA)
            {
                var objectDict = listNode.AddDictionary();
                objectDict.SetDictionary("Factory");
                var objectData = objectDict.SetDictionary("Object");
                var valuesList = objectData.SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            _arrayB = null;

            var factory = Substitute.For<ISynchronizationFactory<ISynchronizable>>();
            factory.Deserialize(null)
                .ReturnsForAnyArgs(
                        info => new FloatSynchronizableTestObject(TestData.floatListA)
                    );

            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(
                    () => SyncObjectListElement(ref _arrayB, factory)
                );
        }
    }
}