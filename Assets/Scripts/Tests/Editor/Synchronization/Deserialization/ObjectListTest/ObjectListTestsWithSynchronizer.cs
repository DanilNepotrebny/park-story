﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization.ObjectListTest
{
    [TestFixture]
    public class ObjectArrayTestWithSynchronizer : BaseRecreatableObjectListTestWithSynchronizer<ISynchronizable[]>
    {
        protected override ISynchronizable[] CreateList(ISynchronizable[] elements)
        {
            return elements.ToArray();
        }

        protected override void SyncObjectList(
            string id,
            ref ISynchronizable[] list,
            ISynchronizer<ISynchronizable> synchronizer)
        {
            state.SyncObjectArray(id, ref list, synchronizer);
        }

        protected override void SyncObjectListElement(
            ref ISynchronizable[] list,
            ISynchronizer<ISynchronizable> synchronizer)
        {
            state.SyncObjectArrayElement(ref list, synchronizer);
        }
    }

    [TestFixture]
    public class ObjectListTestWithSynchronizer : BaseRecreatableObjectListTestWithSynchronizer<List<ISynchronizable>>
    {
        protected override List<ISynchronizable> CreateList(ISynchronizable[] elements)
        {
            return new List<ISynchronizable>(elements);
        }

        protected override void SyncObjectList(
            string id,
            ref List<ISynchronizable> list,
            ISynchronizer<ISynchronizable> synchronizer)
        {
            state.SyncObjectList(id, ref list, synchronizer);
        }

        protected override void SyncObjectListElement(
            ref List<ISynchronizable> list,
            ISynchronizer<ISynchronizable> synchronizer)
        {
            state.SyncObjectListElement(ref list, synchronizer);
        }
    }

    [TestFixture]
    public class
        ObjectHashSetTestWithSynchronizer : BaseNotRecreatableObjectListTestWithSynchronizer<
            ICollection<ISynchronizable>>
    {
        protected override ICollection<ISynchronizable> CreateList(ISynchronizable[] elements)
        {
            return new HashSet<ISynchronizable>(elements);
        }

        protected override void SyncObjectList(
            string id,
            ref ICollection<ISynchronizable> collection,
            ISynchronizer<ISynchronizable> synchronizer)
        {
            state.SyncObjectCollection(id, collection, synchronizer);
        }

        protected override void SyncObjectListElement(
            ref ICollection<ISynchronizable> collection,
            ISynchronizer<ISynchronizable> synchronizer)
        {
            state.SyncObjectCollectionElement(collection, synchronizer);
        }
    }

    [TestFixture]
    public class
        ObjectLinkedListTestWithSynchronizer : BaseNotRecreatableObjectListTestWithSynchronizer<
            ICollection<ISynchronizable>>
    {
        protected override ICollection<ISynchronizable> CreateList(ISynchronizable[] elements)
        {
            return new LinkedList<ISynchronizable>(elements);
        }

        protected override void SyncObjectList(
            string id,
            ref ICollection<ISynchronizable> collection,
            ISynchronizer<ISynchronizable> synchronizer)
        {
            state.SyncObjectCollection(id, collection, synchronizer);
        }

        protected override void SyncObjectListElement(
            ref ICollection<ISynchronizable> collection,
            ISynchronizer<ISynchronizable> synchronizer)
        {
            state.SyncObjectCollectionElement(collection, synchronizer);
        }
    }
}