﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization.ObjectListTest
{
    public abstract class
        BaseRecreatableObjectListTestWithSynchronizer<TList> : BaseObjectListTestWithSynchronizer<TList>
        where TList : class, IEnumerable<ISynchronizable>
    {
        [Test]
        public void ValidSyncNullObjectArrayWithSynchronizer()
        {
            var synchronizer = new TestArraySynchronizer<ISynchronizable>(_arrayA.ToArray());
            var listNode = root.SetList(TestData.keyA);
            foreach (var obj in _arrayA)
            {
                listNode.AddNode(synchronizer.Serialize(obj));
            }

            SetupState();

            _arrayB = null;

            SyncObjectList(TestData.keyA, ref _arrayB, synchronizer);

            CheckEqual(_arrayA, _arrayB);
        }

        [Test]
        public void ValidSyncNullObjectArrayElementWithSynchronizer()
        {
            var synchronizer = new TestArraySynchronizer<ISynchronizable>(_arrayA.ToArray());
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var obj in _arrayA)
            {
                listNode.AddNode(synchronizer.Serialize(obj));
            }

            SetupState();

            _arrayB = null;

            state.BeginList(TestData.keyA);
            SyncObjectListElement(ref _arrayB, synchronizer);
            state.EndList();

            CheckEqual(_arrayA, _arrayB);
        }
    }
}