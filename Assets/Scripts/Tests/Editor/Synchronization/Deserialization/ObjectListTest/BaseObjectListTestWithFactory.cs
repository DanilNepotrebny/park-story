﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using NSubstitute;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization.ObjectListTest
{
    public abstract class BaseObjectListTestWithFactory<TList> : BaseObjectListTest<TList>
        where TList : class, IEnumerable<ISynchronizable>
    {
        [Test]
        public void ValidSyncObjectArrayWithFactory()
        {
            var listNode = root.SetList(TestData.keyA);
            foreach (var obj in _arrayA)
            {
                var objectDict = listNode.AddDictionary();
                objectDict.SetDictionary("Factory");
                var objectData = objectDict.SetDictionary("Object");
                var valuesList = objectData.SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            var factory = Substitute.For<ISynchronizationFactory<ISynchronizable>>();
            factory.Deserialize(null)
                .ReturnsForAnyArgs(
                        info => new FloatSynchronizableTestObject(TestData.floatListA)
                    );

            SyncObjectList(TestData.keyA, ref _arrayB, factory);

            CheckEqual(_arrayA, _arrayB);
        }

        [Test]
        public void ValidSyncObjectArrayLargerWithFactory()
        {
            var source = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger)
                    }
                );

            var target = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var listNode = root.SetList(TestData.keyA);
            foreach (var obj in source)
            {
                var objectDict = listNode.AddDictionary();
                objectDict.SetDictionary("Factory");
                var objectData = objectDict.SetDictionary("Object");
                var valuesList = objectData.SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            var factory = Substitute.For<ISynchronizationFactory<ISynchronizable>>();
            factory.Deserialize(null)
                .ReturnsForAnyArgs(
                        info => new FloatSynchronizableTestObject(TestData.floatListA)
                    );

            SyncObjectList(TestData.keyA, ref target, factory);

            CheckEqual(source, target);
        }

        [Test]
        public void ValidSyncObjectArraySmallerWithFactory()
        {
            var source = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var target = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger)
                    }
                );

            var listNode = root.SetList(TestData.keyA);
            foreach (var obj in source)
            {
                var objectDict = listNode.AddDictionary();
                objectDict.SetDictionary("Factory");
                var objectData = objectDict.SetDictionary("Object");
                var valuesList = objectData.SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            var factory = Substitute.For<ISynchronizationFactory<ISynchronizable>>();
            factory.Deserialize(null)
                .ReturnsForAnyArgs(
                        info => new FloatSynchronizableTestObject(TestData.floatListA)
                    );

            SyncObjectList(TestData.keyA, ref target, factory);

            CheckEqual(source, target);
        }

        [Test]
        public void ValidSyncNonExistentObjectArrayWithFactory()
        {
            SetupState();

            var oldElements = _arrayB.ToArray();

            var factory = Substitute.For<ISynchronizationFactory<ISynchronizable>>();
            factory.Deserialize(null)
                .ReturnsForAnyArgs(
                        info => new FloatSynchronizableTestObject(TestData.floatListA)
                    );

            SyncObjectList(TestData.keyA, ref _arrayB, factory);

            CheckEqual(oldElements, _arrayB);
        }

        [Test]
        public void ValidSyncObjectArrayElementWithFactory()
        {
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var obj in _arrayA)
            {
                var objectDict = listNode.AddDictionary();
                objectDict.SetDictionary("Factory");
                var objectData = objectDict.SetDictionary("Object");
                var valuesList = objectData.SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            var factory = Substitute.For<ISynchronizationFactory<ISynchronizable>>();
            factory.Deserialize(null)
                .ReturnsForAnyArgs(
                        info => new FloatSynchronizableTestObject(TestData.floatListA)
                    );

            state.BeginList(TestData.keyA);
            SyncObjectListElement(ref _arrayB, factory);
            state.EndList();

            CheckEqual(_arrayA, _arrayB);
        }

        [Test]
        public void ValidSyncObjectArrayElementLargerWithFactory()
        {
            var source = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger)
                    }
                );

            var target = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var obj in source)
            {
                var objectDict = listNode.AddDictionary();
                objectDict.SetDictionary("Factory");
                var objectData = objectDict.SetDictionary("Object");
                var valuesList = objectData.SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            var factory = Substitute.For<ISynchronizationFactory<ISynchronizable>>();
            factory.Deserialize(null)
                .ReturnsForAnyArgs(
                        info => new FloatSynchronizableTestObject(TestData.floatListA)
                    );

            state.BeginList(TestData.keyA);
            SyncObjectListElement(ref target, factory);
            state.EndList();

            CheckEqual(target, source);
        }

        [Test]
        public void ValidSyncObjectArrayElementSmallerWithFactory()
        {
            var source = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var target = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger)
                    }
                );

            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var obj in source)
            {
                var objectDict = listNode.AddDictionary();
                objectDict.SetDictionary("Factory");
                var objectData = objectDict.SetDictionary("Object");
                var valuesList = objectData.SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            var factory = Substitute.For<ISynchronizationFactory<ISynchronizable>>();
            factory.Deserialize(null)
                .ReturnsForAnyArgs(
                        info => new FloatSynchronizableTestObject(TestData.floatListA)
                    );

            state.BeginList(TestData.keyA);
            SyncObjectListElement(ref target, factory);
            state.EndList();

            CheckEqual(target, source);
        }

        [Test]
        public void ValidSyncNonExistentObjectArrayElementWithFactory()
        {
            root.SetList(TestData.keyA);
            SetupState();

            var oldElements = _arrayB.ToArray();

            var factory = Substitute.For<ISynchronizationFactory<ISynchronizable>>();
            factory.Deserialize(null)
                .ReturnsForAnyArgs(
                        info => new FloatSynchronizableTestObject(TestData.floatListA)
                    );

            state.BeginList(TestData.keyA);
            SyncObjectListElement(ref _arrayB, factory);
            state.EndList();

            CheckEqual(_arrayB, oldElements);
        }

        protected abstract void SyncObjectList(
                string id,
                ref TList collection,
                ISynchronizationFactory<ISynchronizable> factory
            );

        protected abstract void SyncObjectListElement(
                ref TList list,
                ISynchronizationFactory<ISynchronizable> factory
            );
    }
}