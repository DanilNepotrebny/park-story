﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization.ObjectListTest
{
    public abstract class BaseFixedObjectListTest<TList> : BaseObjectListTest<TList>
        where TList : class, IEnumerable<ISynchronizable>
    {
        [Test]
        public void ValidSyncObjectArray()
        {
            var listNode = root.SetList(TestData.keyA);
            foreach (var obj in _arrayA)
            {
                var valuesList = listNode.AddDictionary()
                    .SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            SyncObjectList(TestData.keyA, _arrayB);

            CheckEqual(_arrayA, _arrayB);
        }

        [Test]
        public void ValidSyncObjectArrayLarger()
        {
            var source = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger)
                    }
                );

            var target = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var expected = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var listNode = root.SetList(TestData.keyA);
            foreach (var obj in source)
            {
                var valuesList = listNode.AddDictionary()
                    .SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            SyncObjectList(TestData.keyA, target);

            CheckEqual(target, expected);
        }

        [Test]
        public void ValidSyncObjectArraySmaller()
        {
            var source = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var target = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger)
                    }
                );

            var expected = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var listNode = root.SetList(TestData.keyA);
            foreach (var obj in source)
            {
                var valuesList = listNode.AddDictionary()
                    .SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            SyncObjectList(TestData.keyA, target);

            CheckEqual(target, expected);
        }

        [Test]
        public void ValidSyncNonExistentObjectArray()
        {
            SetupState();

            var oldElements = _arrayB.ToArray();

            SyncObjectList(TestData.keyA, _arrayB);

            CheckEqual(oldElements, _arrayB);
        }

        [Test]
        public void InvalidSyncNullObjectArray()
        {
            var listNode = root.SetList(TestData.keyA);
            foreach (var obj in _arrayA)
            {
                var valuesList = listNode.AddDictionary()
                    .SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            _arrayB = null;

            Assert.Throws<InvalidOperationException>(
                    () => SyncObjectList(TestData.keyA, _arrayB)
                );
        }

        [Test]
        public void ValidSyncObjectArrayElement()
        {
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var obj in _arrayA)
            {
                var valuesList = listNode.AddDictionary()
                    .SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            state.BeginList(TestData.keyA);
            SyncObjectListElement(_arrayB);
            state.EndList();

            CheckEqual(_arrayA, _arrayB);
        }

        [Test]
        public void ValidSyncObjectArrayElementLarger()
        {
            var source = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger)
                    }
                );

            var target = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var expected = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var obj in source)
            {
                var dictionary = listNode.AddDictionary();
                var valuesList = dictionary.SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            state.BeginList(TestData.keyA);
            SyncObjectListElement(target);
            state.EndList();

            CheckEqual(target, expected);
        }

        [Test]
        public void ValidSyncObjectArrayElementSmaller()
        {
            var source = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var target = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger)
                    }
                );

            var expected = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var obj in source)
            {
                var dictionary = listNode.AddDictionary();
                var valuesList = dictionary.SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            state.BeginList(TestData.keyA);
            SyncObjectListElement(target);
            state.EndList();

            CheckEqual(target, expected);
        }

        [Test]
        public void ValidSyncNonExistentObjectArrayElement()
        {
            root.SetList(TestData.keyA);
            SetupState();

            var oldElements = _arrayB.ToArray();

            state.BeginList(TestData.keyA);
            SyncObjectListElement(_arrayB);
            state.EndList();

            Assert.IsTrue(
                    _arrayB.SequenceEqual(
                            oldElements,
                            new TestSynchronizableComparer()
                        )
                );
        }

        [Test]
        public void InvalidSyncNullObjectArrayElement()
        {
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var obj in _arrayA)
            {
                var valuesList = listNode.AddDictionary()
                    .SetList(FloatSynchronizableTestObject.valuesId);

                var values = (obj as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }

            SetupState();

            _arrayB = null;

            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(
                    () => SyncObjectListElement(_arrayB)
                );
        }

        protected abstract void SyncObjectList(string id, TList list);

        protected abstract void SyncObjectListElement(TList list);
    }
}