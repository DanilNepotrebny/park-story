﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization.ObjectListTest
{
    [TestFixture]
    public class ObjectArrayTestWithFactory : BaseRecreatableObjectListTestWithFactory<ISynchronizable[]>
    {
        protected override ISynchronizable[] CreateList(ISynchronizable[] elements)
        {
            return elements.ToArray();
        }

        protected override void SyncObjectList(
            string id,
            ref ISynchronizable[] collection,
            ISynchronizationFactory<ISynchronizable> factory)
        {
            state.SyncObjectArray(id, ref collection, factory);
        }

        protected override void SyncObjectListElement(
            ref ISynchronizable[] list,
            ISynchronizationFactory<ISynchronizable> factory)
        {
            state.SyncObjectArrayElement(ref list, factory);
        }
    }

    [TestFixture]
    public class ObjectListTestWithFactory : BaseRecreatableObjectListTestWithFactory<List<ISynchronizable>>
    {
        protected override List<ISynchronizable> CreateList(ISynchronizable[] elements)
        {
            return new List<ISynchronizable>(elements);
        }

        protected override void SyncObjectList(
            string id,
            ref List<ISynchronizable> collection,
            ISynchronizationFactory<ISynchronizable> factory)
        {
            state.SyncObjectList(id, ref collection, factory);
        }

        protected override void SyncObjectListElement(
            ref List<ISynchronizable> list,
            ISynchronizationFactory<ISynchronizable> factory)
        {
            state.SyncObjectListElement(ref list, factory);
        }
    }

    [TestFixture]
    public class
        ObjectHashSetTestWithFactory : BaseNotRecreatableObjectListTestWithFactory<ICollection<ISynchronizable>>
    {
        protected override ICollection<ISynchronizable> CreateList(ISynchronizable[] elements)
        {
            return new HashSet<ISynchronizable>(elements);
        }

        protected override void SyncObjectList(
            string id,
            ref ICollection<ISynchronizable> collection,
            ISynchronizationFactory<ISynchronizable> factory)
        {
            state.SyncObjectCollection(id, collection, factory);
        }

        protected override void SyncObjectListElement(
            ref ICollection<ISynchronizable> collection,
            ISynchronizationFactory<ISynchronizable> factory)
        {
            state.SyncObjectCollectionElement(collection, factory);
        }
    }

    [TestFixture]
    public class
        ObjectLinkedListTestWithFactory : BaseNotRecreatableObjectListTestWithFactory<ICollection<ISynchronizable>>
    {
        protected override ICollection<ISynchronizable> CreateList(ISynchronizable[] elements)
        {
            return new LinkedList<ISynchronizable>(elements);
        }

        protected override void SyncObjectList(
            string id,
            ref ICollection<ISynchronizable> collection,
            ISynchronizationFactory<ISynchronizable> factory)
        {
            state.SyncObjectCollection(id, collection, factory);
        }

        protected override void SyncObjectListElement(
            ref ICollection<ISynchronizable> collection,
            ISynchronizationFactory<ISynchronizable> factory)
        {
            state.SyncObjectCollectionElement(collection, factory);
        }
    }
}