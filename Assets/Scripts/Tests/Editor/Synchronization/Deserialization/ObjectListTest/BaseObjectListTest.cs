﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization.ObjectListTest
{
    public abstract class BaseObjectListTest<TList> : BaseDeserializationStateTest
        where TList : class, IEnumerable<ISynchronizable>
    {
        protected TList _arrayA;
        protected TList _arrayB;

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            _arrayA = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            _arrayB = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListSmaller)
                    }
                );
        }

        protected abstract TList CreateList(ISynchronizable[] elements);

        protected void CheckEqual(
                IEnumerable<ISynchronizable> listA,
                IEnumerable<ISynchronizable> listB
            )
        {
            Assert.IsTrue(
                    listA.SequenceEqual(
                            listB,
                            new TestSynchronizableComparer()
                        )
                );
        }

        #region Inner types

        protected class TestSynchronizableComparer : IEqualityComparer<ISynchronizable>
        {
            public bool Equals(
                    ISynchronizable a,
                    ISynchronizable b
                )
            {
                var objA = (a as FloatSynchronizableTestObject);
                var objB = (b as FloatSynchronizableTestObject);
                return objA.Values.SequenceEqual(objB.Values);
            }

            public int GetHashCode(ISynchronizable obj)
            {
                return obj.GetHashCode();
            }
        }

        #endregion
    }
}