﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization.ObjectListTest
{
    [TestFixture]
    public class FixedObjectArrayTest : BaseFixedObjectListTest<ISynchronizable[]>
    {
        protected override ISynchronizable[] CreateList(ISynchronizable[] elements)
        {
            return elements.ToArray();
        }

        protected override void SyncObjectList(string id, ISynchronizable[] list)
        {
            state.SyncObjectArray(id, list);
        }

        protected override void SyncObjectListElement(ISynchronizable[] list)
        {
            state.SyncObjectArrayElement(list);
        }
    }

    [TestFixture]
    public class FixedObjectListTest : BaseFixedObjectListTest<List<ISynchronizable>>
    {
        protected override List<ISynchronizable> CreateList(ISynchronizable[] elements)
        {
            return new List<ISynchronizable>(elements);
        }

        protected override void SyncObjectList(string id, List<ISynchronizable> list)
        {
            state.SyncObjectList(id, list);
        }

        protected override void SyncObjectListElement(List<ISynchronizable> list)
        {
            state.SyncObjectListElement(list);
        }
    }
}