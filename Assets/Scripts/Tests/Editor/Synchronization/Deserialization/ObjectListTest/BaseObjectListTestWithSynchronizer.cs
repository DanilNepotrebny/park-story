﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using NSubstitute;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization.ObjectListTest
{
    public abstract class BaseObjectListTestWithSynchronizer<TList> : BaseObjectListTest<TList>
        where TList : class, IEnumerable<ISynchronizable>
    {
        [Test]
        public void ValidSyncObjectArrayWithSynchronizer()
        {
            var synchronizer = new TestArraySynchronizer<ISynchronizable>(_arrayA.ToArray());
            var listNode = root.SetList(TestData.keyA);
            foreach (var obj in _arrayA)
            {
                listNode.AddNode(synchronizer.Serialize(obj));
            }

            SetupState();

            SyncObjectList(TestData.keyA, ref _arrayB, synchronizer);

            CheckEqual(_arrayA, _arrayB);
        }

        [Test]
        public void ValidSyncObjectArrayLargerWithSynchronizer()
        {
            var source = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger)
                    }
                );

            var target = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var synchronizer = new TestArraySynchronizer<ISynchronizable>(source.Union(target).ToArray());
            var listNode = root.SetList(TestData.keyA);
            foreach (var obj in source)
            {
                listNode.AddNode(synchronizer.Serialize(obj));
            }

            SetupState();

            SyncObjectList(TestData.keyA, ref target, synchronizer);

            CheckEqual(source, target);
        }

        [Test]
        public void ValidSyncObjectArraySmallerWithSynchronizer()
        {
            var source = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var target = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger)
                    }
                );

            var synchronizer = new TestArraySynchronizer<ISynchronizable>(source.Union(target).ToArray());
            var listNode = root.SetList(TestData.keyA);
            foreach (var obj in source)
            {
                listNode.AddNode(synchronizer.Serialize(obj));
            }

            SetupState();

            var factory = Substitute.For<ISynchronizationFactory<ISynchronizable>>();
            factory.Deserialize(null)
                .ReturnsForAnyArgs(
                        info => new FloatSynchronizableTestObject(TestData.floatListA)
                    );

            SyncObjectList(TestData.keyA, ref target, synchronizer);

            CheckEqual(source, target);
        }

        [Test]
        public void ValidSyncNonExistentObjectArrayWithSynchronizer()
        {
            SetupState();

            var oldElements = _arrayB.ToArray();

            var synchronizer = new TestArraySynchronizer<ISynchronizable>(_arrayA.ToArray());

            SyncObjectList(TestData.keyA, ref _arrayB, synchronizer);

            CheckEqual(oldElements, _arrayB);
        }

        [Test]
        public void ValidSyncObjectArrayElementWithSynchronizer()
        {
            var synchronizer = new TestArraySynchronizer<ISynchronizable>(_arrayA.ToArray());
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var obj in _arrayA)
            {
                listNode.AddNode(synchronizer.Serialize(obj));
            }

            SetupState();

            state.BeginList(TestData.keyA);
            SyncObjectListElement(ref _arrayB, synchronizer);
            state.EndList();

            CheckEqual(_arrayA, _arrayB);
        }

        [Test]
        public void ValidSyncObjectArrayElementLargerWithSynchronizer()
        {
            var source = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger)
                    }
                );

            var target = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var synchronizer = new TestArraySynchronizer<ISynchronizable>(source.Union(target).ToArray());
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var obj in source)
            {
                listNode.AddNode(synchronizer.Serialize(obj));
            }

            SetupState();

            state.BeginList(TestData.keyA);
            SyncObjectListElement(ref target, synchronizer);
            state.EndList();

            CheckEqual(target, source);
        }

        [Test]
        public void ValidSyncObjectArrayElementSmallerWithSynchronizer()
        {
            var source = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB)
                    }
                );

            var target = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListSmaller),
                        new FloatSynchronizableTestObject(TestData.floatListLarger)
                    }
                );

            var synchronizer = new TestArraySynchronizer<ISynchronizable>(source.Union(target).ToArray());
            var listNode = root.SetList(TestData.keyA).AddList();
            foreach (var obj in source)
            {
                listNode.AddNode(synchronizer.Serialize(obj));
            }

            SetupState();

            state.BeginList(TestData.keyA);
            SyncObjectListElement(ref target, synchronizer);
            state.EndList();

            CheckEqual(target, source);
        }

        [Test]
        public void ValidSyncNonExistentObjectArrayElementWithSynchronizer()
        {
            root.SetList(TestData.keyA);
            SetupState();

            var oldElements = _arrayB.ToArray();

            var synchronizer = new TestArraySynchronizer<ISynchronizable>(_arrayA.ToArray());

            state.BeginList(TestData.keyA);
            SyncObjectListElement(ref _arrayB, synchronizer);
            state.EndList();

            CheckEqual(_arrayB, oldElements);
        }

        protected abstract void SyncObjectList(
                string id,
                ref TList list,
                ISynchronizer<ISynchronizable> synchronizer
            );

        protected abstract void SyncObjectListElement(
                ref TList list,
                ISynchronizer<ISynchronizable> synchronizer
            );
    }
}