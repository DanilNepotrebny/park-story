﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Linq;
using DreamTeam.Synchronization;
using NSubstitute;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization
{
    public class ValueOperationsTest : BaseDeserializationStateTest
    {
        [Test]
        public void ValidSyncInt()
        {
            root.SetInt(TestData.keyA, intB);
            SetupState();

            state.SyncInt(TestData.keyA, ref intA);

            Assert.AreEqual(intB, intA);
        }

        [Test]
        public void ValidSyncNonExistedInt()
        {
            SetupState();

            int oldA = intA;
            state.SyncInt(TestData.keyA, ref intA);
            Assert.AreEqual(oldA, intA);
        }

        [Test]
        public void ValidSyncIntWithDefault()
        {
            root.SetInt(TestData.keyA, intB);
            SetupState();

            state.SyncInt(TestData.keyA, ref intA, intC);
            Assert.AreEqual(intB, intA);
        }

        [Test]
        public void ValidSyncNonExistedIntWithDefault()
        {
            SetupState();

            state.SyncInt(TestData.keyA, ref intA, intB);
            Assert.AreEqual(intB, intA);
        }

        [Test]
        public void ValidSyncFloat()
        {
            root.SetFloat(TestData.keyA, floatB);
            SetupState();

            state.SyncFloat(TestData.keyA, ref floatA);

            Assert.AreEqual(floatB, floatA);
        }

        [Test]
        public void ValidSyncNonExistedFloat()
        {
            SetupState();

            float oldA = floatA;
            state.SyncFloat(TestData.keyA, ref floatA);
            Assert.AreEqual(oldA, floatA);
        }

        [Test]
        public void ValidSyncFloatWithDefault()
        {
            root.SetFloat(TestData.keyA, floatB);
            SetupState();

            state.SyncFloat(TestData.keyA, ref floatA, floatC);
            Assert.AreEqual(floatB, floatA);
        }

        [Test]
        public void ValidSyncNonExistedFloatWithDefault()
        {
            SetupState();

            state.SyncFloat(TestData.keyA, ref floatA, floatB);
            Assert.AreEqual(floatB, floatA);
        }

        [Test]
        public void ValidSyncString()
        {
            root.SetString(TestData.keyA, stringB);
            SetupState();

            state.SyncString(TestData.keyA, ref stringA);

            Assert.AreEqual(stringB, stringA);
        }

        [Test]
        public void ValidSyncNonExistedString()
        {
            SetupState();

            string oldA = stringA;
            state.SyncString(TestData.keyA, ref stringA);
            Assert.AreEqual(oldA, stringA);
        }

        [Test]
        public void ValidSyncStringWithDefault()
        {
            root.SetString(TestData.keyA, stringB);
            SetupState();

            state.SyncString(TestData.keyA, ref stringA, stringC);
            Assert.AreEqual(stringB, stringA);
        }

        [Test]
        public void ValidSyncNonExistedStringWithDefault()
        {
            SetupState();

            state.SyncString(TestData.keyA, ref stringA, stringB);
            Assert.AreEqual(stringB, stringA);
        }

        [Test]
        public void ValidDictionarySyncInt()
        {
            root.SetDictionary(TestData.keyB).SetInt(TestData.keyA, intB);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            state.SyncInt(TestData.keyA, ref intA);
            state.EndDictionary();

            Assert.AreEqual(intB, intA);
        }

        [Test]
        public void ValidDictionarySyncIntWithDefault()
        {
            root.SetDictionary(TestData.keyB).SetInt(TestData.keyA, intB);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            state.SyncInt(TestData.keyA, ref intA, intC);
            state.EndDictionary();

            Assert.AreEqual(intB, intA);
        }

        [Test]
        public void ValidDictionarySyncFloat()
        {
            root.SetDictionary(TestData.keyB).SetFloat(TestData.keyA, floatB);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            state.SyncFloat(TestData.keyA, ref floatA);
            state.EndDictionary();

            Assert.AreEqual(floatB, floatA);
        }

        [Test]
        public void ValidDictionarySyncFloatWithDefault()
        {
            root.SetDictionary(TestData.keyB).SetFloat(TestData.keyA, floatB);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            state.SyncFloat(TestData.keyA, ref floatA, floatC);
            state.EndDictionary();

            Assert.AreEqual(floatB, floatA);
        }

        [Test]
        public void ValidDictionarySyncString()
        {
            root.SetDictionary(TestData.keyB).SetString(TestData.keyA, stringB);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            state.SyncString(TestData.keyA, ref stringA);
            state.EndDictionary();

            Assert.AreEqual(stringB, stringA);
        }

        [Test]
        public void ValidDictionarySyncStringWithDefault()
        {
            root.SetDictionary(TestData.keyB).SetString(TestData.keyA, stringB);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            state.SyncString(TestData.keyA, ref stringA, stringC);
            state.EndDictionary();

            Assert.AreEqual(stringB, stringA);
        }

        [Test]
        public void InvalidDictionarySyncIntElement()
        {
            root.SetDictionary(TestData.keyB);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.SyncIntElement(ref intA));
        }

        [Test]
        public void InvalidDictionarySyncFloatElement()
        {
            root.SetDictionary(TestData.keyB);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.SyncFloatElement(ref floatA));
        }

        [Test]
        public void InvalidDictionarySyncStringElement()
        {
            root.SetDictionary(TestData.keyB);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.SyncStringElement(ref stringA));
        }

        [Test]
        public void ValidSyncObject()
        {
            var listNode = root.SetDictionary(TestData.keyA).SetList(FloatSynchronizableTestObject.valuesId);
            foreach (float f in TestData.floatListB)
            {
                listNode.AddFloat(f);
            }

            SetupState();

            var obj = new FloatSynchronizableTestObject(TestData.floatListA);
            state.SyncObject(TestData.keyA, obj);

            Assert.IsTrue(
                    TestData.floatListB.SequenceEqual(obj.Values),
                    $"expected:[{string.Join(", ", TestData.floatListB)}] " +
                    $"actual:[{string.Join(", ", obj.Values)}]"
                );
        }

        [Test]
        public void ValidNonExistentSyncObject()
        {
            SetupState();

            var obj = new FloatSynchronizableTestObject(TestData.floatListA);
            state.SyncObject(TestData.keyA, obj);

            Assert.IsTrue(
                    TestData.floatListA.SequenceEqual(obj.Values),
                    $"expected:[{string.Join(", ", TestData.floatListB)}] " +
                    $"actual:[{string.Join(", ", obj.Values)}]"
                );
        }

        [Test]
        public void InvalidSyncObjectNull()
        {
            var listNode = root.SetDictionary(TestData.keyA).SetList(FloatSynchronizableTestObject.valuesId);
            foreach (float f in TestData.floatListB)
            {
                listNode.AddFloat(f);
            }

            SetupState();
            FloatSynchronizableTestObject obj = null;

            Assert.Throws<InvalidOperationException>(() => state.SyncObject(TestData.keyA, obj));
        }

        [Test]
        public void ValidSyncObjectWithFactory()
        {
            var dictionary = root.SetDictionary(TestData.keyA);
            dictionary.SetDictionary("Factory");
            var listNode = dictionary.SetDictionary("Object").SetList(FloatSynchronizableTestObject.valuesId);
            foreach (float f in TestData.floatListB)
            {
                listNode.AddFloat(f);
            }

            SetupState();

            var expectedObject = new FloatSynchronizableTestObject(TestData.floatListA);

            var factory = Substitute.For<ISynchronizationFactory<FloatSynchronizableTestObject>>();
            factory.Deserialize(null).ReturnsForAnyArgs(expectedObject);

            FloatSynchronizableTestObject obj = new FloatSynchronizableTestObject(TestData.floatListLarger);
            state.SyncObject(TestData.keyA, ref obj, factory);

            Assert.AreSame(expectedObject, obj);
            Assert.IsTrue(
                    TestData.floatListB.SequenceEqual(obj.Values),
                    $"expected:[{string.Join(", ", TestData.floatListB)}] " +
                    $"actual:[{string.Join(", ", obj.Values)}]"
                );
        }

        [Test]
        public void ValidSyncObjectWithSynchronizer()
        {
            var expectedObject = new FloatSynchronizableTestObject(TestData.floatListA);

            var synchronizer = new TestArraySynchronizer<FloatSynchronizableTestObject>(new[] { expectedObject });
            root.SetNode(TestData.keyA, synchronizer.Serialize(expectedObject));

            SetupState();

            FloatSynchronizableTestObject obj = new FloatSynchronizableTestObject(TestData.floatListLarger);
            state.SyncObject(TestData.keyA, ref obj, synchronizer);

            Assert.AreSame(expectedObject, obj);
            Assert.IsTrue(
                    TestData.floatListA.SequenceEqual(obj.Values),
                    $"expected:[{string.Join(", ", TestData.floatListA)}] " +
                    $"actual:[{string.Join(", ", obj.Values)}]"
                );
        }

        [Test]
        public void ValidSyncNonExistedObjectWithFactory()
        {
            SetupState();

            var factory = Substitute.For<ISynchronizationFactory<FloatSynchronizableTestObject>>();
            factory.Deserialize(null).ReturnsForAnyArgs(new FloatSynchronizableTestObject(TestData.floatListA));

            FloatSynchronizableTestObject obj = new FloatSynchronizableTestObject(TestData.floatListLarger);
            var oldObj = obj;

            state.SyncObject(TestData.keyA, ref obj, factory);

            Assert.AreSame(oldObj, obj);
            Assert.IsTrue(
                    TestData.floatListLarger.SequenceEqual(obj.Values),
                    $"expected:[{string.Join(", ", TestData.floatListLarger)}] " +
                    $"actual:[{string.Join(", ", obj.Values)}]"
                );
        }

        [Test]
        public void ValidSyncNullObjectWithFactory()
        {
            var dictionary = root.SetDictionary(TestData.keyA);
            dictionary.SetDictionary("Factory");
            var listNode = dictionary.SetDictionary("Object").SetList(FloatSynchronizableTestObject.valuesId);
            foreach (float f in TestData.floatListB)
            {
                listNode.AddFloat(f);
            }

            SetupState();

            var expectedObject = new FloatSynchronizableTestObject(TestData.floatListA);

            var factory = Substitute.For<ISynchronizationFactory<FloatSynchronizableTestObject>>();
            factory.Deserialize(null).ReturnsForAnyArgs(expectedObject);

            FloatSynchronizableTestObject obj = null;
            state.SyncObject(TestData.keyA, ref obj, factory);

            Assert.AreSame(expectedObject, obj);
            Assert.IsTrue(
                    TestData.floatListB.SequenceEqual(obj.Values),
                    $"expected:[{string.Join(", ", TestData.floatListB)}] " +
                    $"actual:[{string.Join(", ", obj.Values)}]"
                );
        }

        [Test]
        public void ValidNullDataSyncObjectWithFactory()
        {
            var dictionary = root.SetDictionary(TestData.keyA);
            dictionary.SetDictionary("Factory");
            SetupState();

            var factory = Substitute.For<ISynchronizationFactory<FloatSynchronizableTestObject>>();
            factory.Deserialize(null).ReturnsForAnyArgs((FloatSynchronizableTestObject)null);

            FloatSynchronizableTestObject obj = new FloatSynchronizableTestObject(TestData.floatListA);
            state.SyncObject(TestData.keyA, ref obj, factory);

            Assert.IsNull(obj);
        }

        [Test]
        public void InvalidSyncObjectWithFactoryWithNullNode()
        {
            root.SetNull(TestData.keyA);
            SetupState();

            var factory = Substitute.For<ISynchronizationFactory<FloatSynchronizableTestObject>>();
            FloatSynchronizableTestObject obj = new FloatSynchronizableTestObject(TestData.floatListLarger);
            Assert.Throws<InvalidOperationException>(() => state.SyncObject(TestData.keyA, ref obj, factory));
        }

        [Test]
        public void ValidSyncObjectElement()
        {
            var listNode = root.SetList(TestData.keyA).AddDictionary().SetList(FloatSynchronizableTestObject.valuesId);
            foreach (float f in TestData.floatListB)
            {
                listNode.AddFloat(f);
            }

            SetupState();

            var obj = new FloatSynchronizableTestObject(TestData.floatListA);
            state.BeginList(TestData.keyA);
            state.SyncObjectElement(obj);
            state.EndList();

            Assert.IsTrue(
                    TestData.floatListB.SequenceEqual(obj.Values),
                    $"expected:[{string.Join(", ", TestData.floatListB)}] " +
                    $"actual:[{string.Join(", ", obj.Values)}]"
                );
        }

        [Test]
        public void ValidNonExistentSyncObjectElement()
        {
            root.SetList(TestData.keyA);
            SetupState();

            var obj = new FloatSynchronizableTestObject(TestData.floatListA);
            state.BeginList(TestData.keyA);
            state.SyncObjectElement(obj);
            state.EndList();

            Assert.IsTrue(
                    TestData.floatListA.SequenceEqual(obj.Values),
                    $"expected:[{string.Join(", ", TestData.floatListB)}] " +
                    $"actual:[{string.Join(", ", obj.Values)}]"
                );
        }

        [Test]
        public void InvalidSyncObjectElementNull()
        {
            var listNode = root.SetList(TestData.keyA).AddDictionary().SetList(FloatSynchronizableTestObject.valuesId);
            foreach (float f in TestData.floatListB)
            {
                listNode.AddFloat(f);
            }

            SetupState();
            FloatSynchronizableTestObject obj = null;

            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => state.SyncObjectElement(obj));
        }

        [Test]
        public void ValidSyncObjectElementWithFactory()
        {
            var dictionary = root.SetList(TestData.keyA).AddDictionary();
            dictionary.SetDictionary("Factory");
            var listNode = dictionary.SetDictionary("Object").SetList(FloatSynchronizableTestObject.valuesId);
            foreach (float f in TestData.floatListB)
            {
                listNode.AddFloat(f);
            }

            SetupState();

            var expectedObject = new FloatSynchronizableTestObject(TestData.floatListA);

            var factory = Substitute.For<ISynchronizationFactory<FloatSynchronizableTestObject>>();
            factory.Deserialize(null).ReturnsForAnyArgs(expectedObject);

            FloatSynchronizableTestObject obj = new FloatSynchronizableTestObject(TestData.floatListLarger);

            state.BeginList(TestData.keyA);
            state.SyncObjectElement(ref obj, factory);
            state.EndList();

            Assert.AreSame(expectedObject, obj);
            Assert.IsTrue(
                    TestData.floatListB.SequenceEqual(obj.Values),
                    $"expected:[{string.Join(", ", TestData.floatListB)}] " +
                    $"actual:[{string.Join(", ", obj.Values)}]"
                );
        }

        [Test]
        public void ValidSyncNonExistedObjectElementWithFactory()
        {
            root.SetList(TestData.keyA);
            SetupState();

            var factory = Substitute.For<ISynchronizationFactory<FloatSynchronizableTestObject>>();
            factory.Deserialize(null).ReturnsForAnyArgs(new FloatSynchronizableTestObject(TestData.floatListA));

            FloatSynchronizableTestObject obj = new FloatSynchronizableTestObject(TestData.floatListLarger);
            var oldObj = obj;

            state.BeginList(TestData.keyA);
            state.SyncObjectElement(ref obj, factory);
            state.EndList();

            Assert.AreSame(oldObj, obj);
            Assert.IsTrue(
                    TestData.floatListLarger.SequenceEqual(obj.Values),
                    $"expected:[{string.Join(", ", TestData.floatListLarger)}] " +
                    $"actual:[{string.Join(", ", obj.Values)}]"
                );
        }

        [Test]
        public void ValidSyncNullObjectElementWithFactory()
        {
            var dictionary = root.SetList(TestData.keyA).AddDictionary();
            dictionary.SetDictionary("Factory");
            var listNode = dictionary.SetDictionary("Object").SetList(FloatSynchronizableTestObject.valuesId);
            foreach (float f in TestData.floatListB)
            {
                listNode.AddFloat(f);
            }

            SetupState();

            var expectedObject = new FloatSynchronizableTestObject(TestData.floatListA);

            var factory = Substitute.For<ISynchronizationFactory<FloatSynchronizableTestObject>>();
            factory.Deserialize(null).ReturnsForAnyArgs(expectedObject);

            FloatSynchronizableTestObject obj = null;

            state.BeginList(TestData.keyA);
            state.SyncObjectElement(ref obj, factory);
            state.EndList();

            Assert.AreSame(expectedObject, obj);
            Assert.IsTrue(
                    TestData.floatListB.SequenceEqual(obj.Values),
                    $"expected:[{string.Join(", ", TestData.floatListB)}] " +
                    $"actual:[{string.Join(", ", obj.Values)}]"
                );
        }

        [Test]
        public void ValidNullDataSyncObjectElementWithFactory()
        {
            var dictionary = root.SetList(TestData.keyA).AddDictionary();
            dictionary.SetDictionary("Factory");
            SetupState();

            var factory = Substitute.For<ISynchronizationFactory<FloatSynchronizableTestObject>>();
            factory.Deserialize(null).ReturnsForAnyArgs((FloatSynchronizableTestObject)null);

            FloatSynchronizableTestObject obj = new FloatSynchronizableTestObject(TestData.floatListA);

            state.BeginList(TestData.keyA);
            state.SyncObjectElement(ref obj, factory);
            state.EndList();

            Assert.IsNull(obj);
        }

        [Test]
        public void InvalidSyncObjectElementWithFactoryWithNullNode()
        {
            root.SetList(TestData.keyA).AddNull();
            SetupState();

            var factory = Substitute.For<ISynchronizationFactory<FloatSynchronizableTestObject>>();
            FloatSynchronizableTestObject obj = new FloatSynchronizableTestObject(TestData.floatListLarger);

            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => state.SyncObjectElement(ref obj, factory));
        }

        [Test]
        public void ListSyncInt()
        {
            root.SetList(TestData.keyB);
            SetupState();

            state.BeginList(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.SyncInt(TestData.keyA, ref intA));
        }

        [Test]
        public void ListSyncFloat()
        {
            root.SetList(TestData.keyB);
            SetupState();

            state.BeginList(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.SyncFloat(TestData.keyA, ref floatA));
        }

        [Test]
        public void ListSyncString()
        {
            root.SetList(TestData.keyB);
            SetupState();

            state.BeginList(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.SyncString(TestData.keyA, ref stringA));
        }
    }
}