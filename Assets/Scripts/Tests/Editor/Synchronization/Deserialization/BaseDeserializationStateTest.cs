﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Synchronization.Nodes;
using DreamTeam.Synchronization.States;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization
{
    public class BaseDeserializationStateTest : BaseStateTestFixture
    {
        protected DeserializationState state;
        protected DictionaryNode root;

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            root = new DictionaryNode();
        }

        protected void SetupState()
        {
            state = new DeserializationState(root);
        }
    }
}