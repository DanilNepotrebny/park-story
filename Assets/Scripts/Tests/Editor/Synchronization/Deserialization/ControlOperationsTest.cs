﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization
{
    public class ControlOperationsTest : BaseDeserializationStateTest
    {
        [Test]
        public void InvalidDictionaryBeginDictionaryElement()
        {
            root.SetDictionary(TestData.keyB);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.BeginDictionaryElement());
        }

        [Test]
        public void InvalidDictionaryEndDictionaryElement()
        {
            root.SetDictionary(TestData.keyB);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.EndDictionaryElement());
        }

        [Test]
        public void InvalidDictionaryBeginListElement()
        {
            root.SetDictionary(TestData.keyB);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.BeginListElement());
        }

        [Test]
        public void InvalidDictionaryEndListElement()
        {
            root.SetDictionary(TestData.keyB);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.EndListElement());
        }

        [Test]
        public void InvalidListBeginDictionary()
        {
            root.SetList(TestData.keyB);
            SetupState();

            state.BeginList(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.BeginDictionary(TestData.keyA));
        }

        [Test]
        public void InvalidListEndDictionary()
        {
            root.SetList(TestData.keyB);
            SetupState();

            state.BeginList(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.EndDictionary());
        }

        [Test]
        public void InvalidEndDictionaryBeforeEndListElement()
        {
            root.SetList(TestData.keyB).AddList();
            SetupState();

            state.BeginList(TestData.keyB);
            state.BeginListElement();
            Assert.Throws<InvalidOperationException>(() => state.EndDictionary());
        }

        [Test]
        public void InvalidEndDictionaryBeforeEndDictionaryElement()
        {
            root.SetList(TestData.keyB).AddDictionary();
            SetupState();

            state.BeginList(TestData.keyB);
            state.BeginDictionaryElement();
            Assert.Throws<InvalidOperationException>(() => state.EndDictionary());
        }

        [Test]
        public void InvalidEndDictionaryBeforeEndList()
        {
            root.SetDictionary(TestData.keyB).SetList(TestData.keyA);
            SetupState();

            state.BeginDictionary(TestData.keyB);
            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => state.EndDictionary());
        }

        [Test]
        public void InvalidEndDictionaryElementBeforeEndList()
        {
            root.SetList(TestData.keyB).AddDictionary().SetList(TestData.keyA);
            SetupState();

            state.BeginList(TestData.keyB);
            state.BeginDictionaryElement();
            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => state.EndDictionaryElement());
        }

        [Test]
        public void InvalidEndDictionaryElementBeforeEndDictionary()
        {
            root.SetList(TestData.keyB).AddDictionary().SetDictionary(TestData.keyA);
            SetupState();

            state.BeginList(TestData.keyB);
            state.BeginDictionaryElement();
            state.BeginDictionary(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => state.EndDictionaryElement());
        }

        [Test]
        public void InvalidListBeginList()
        {
            root.SetList(TestData.keyB);
            SetupState();

            state.BeginList(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.BeginList(TestData.keyA));
        }

        [Test]
        public void InvalidListEndDictionaryElement()
        {
            root.SetList(TestData.keyB);
            SetupState();

            state.BeginList(TestData.keyB);
            Assert.Throws<InvalidOperationException>(() => state.EndDictionaryElement());
        }

        [Test]
        public void InvalidEndListBeforeEndListElement()
        {
            root.SetList(TestData.keyB).AddList();
            SetupState();

            state.BeginList(TestData.keyB);
            state.BeginListElement();
            Assert.Throws<InvalidOperationException>(() => state.EndList());
        }

        [Test]
        public void InvalidEndListBeforeEndDictionaryElement()
        {
            root.SetList(TestData.keyB).AddDictionary();
            SetupState();

            state.BeginList(TestData.keyB);
            state.BeginDictionaryElement();
            Assert.Throws<InvalidOperationException>(() => state.EndList());
        }

        [Test]
        public void InvalidEndListElementBeforeEndDictionaryElement()
        {
            root.SetList(TestData.keyB).AddList().AddDictionary();
            SetupState();

            state.BeginList(TestData.keyB);
            state.BeginListElement();
            state.BeginDictionaryElement();
            Assert.Throws<InvalidOperationException>(() => state.EndListElement());
        }

        [Test]
        public void ValidTwoListsSync()
        {
            root.SetList(TestData.keyB).AddInt(intB);
            root.SetList(TestData.keyC).AddString(stringB);
            SetupState();

            state.BeginList(TestData.keyB);
            state.SyncIntElement(ref intA);
            state.EndList();

            state.BeginList(TestData.keyC);
            state.SyncStringElement(ref stringA);
            state.EndList();

            Assert.AreEqual(intB, intA);
            Assert.AreEqual(stringB, stringA);
        }

        [Test]
        public void ValidTwoValueElementsSync()
        {
            var node = root.SetList(TestData.keyB);
            node.AddInt(intB);
            node.AddString(stringB);
            SetupState();

            state.BeginList(TestData.keyB);
            state.SyncIntElement(ref intA);
            state.SyncStringElement(ref stringA);
            state.EndList();

            Assert.AreEqual(intB, intA);
            Assert.AreEqual(stringB, stringA);
        }

        [Test]
        public void ValidValueElementsBeforeDictionaryElement()
        {
            var node = root.SetList(TestData.keyB);
            node.AddString(stringB);
            node.AddFloat(floatB);
            node.AddDictionary().SetInt(TestData.keyA, intB);
            SetupState();

            state.BeginList(TestData.keyB);
            state.SyncStringElement(ref stringA);
            state.SyncFloatElement(ref floatA);
            state.BeginDictionaryElement();
            state.SyncInt(TestData.keyA, ref intA);
            state.EndDictionaryElement();
            state.EndList();

            Assert.AreEqual(intB, intA);
            Assert.AreEqual(stringB, stringA);
            Assert.AreEqual(floatB, floatA);
        }

        [Test]
        public void ValidDictionaryElementBeforeValueElements()
        {
            var node = root.SetList(TestData.keyB);
            node.AddDictionary().SetInt(TestData.keyA, intB);
            node.AddString(stringB);
            node.AddFloat(floatB);
            SetupState();

            state.BeginList(TestData.keyB);
            state.BeginDictionaryElement();
            state.SyncInt(TestData.keyA, ref intA);
            state.EndDictionaryElement();
            state.SyncStringElement(ref stringA);
            state.SyncFloatElement(ref floatA);
            state.EndList();

            Assert.AreEqual(intB, intA);
            Assert.AreEqual(stringB, stringA);
            Assert.AreEqual(floatB, floatA);
        }

        [Test]
        public void ValidListElementSync()
        {
            root.SetList(TestData.keyB).AddList().AddInt(intB);
            SetupState();

            state.BeginList(TestData.keyB);
            state.BeginListElement();
            state.SyncIntElement(ref intA);
            state.EndListElement();
            state.EndList();

            Assert.AreEqual(intB, intA);
        }

        [Test]
        public void ValidValueElementsBeforeListElement()
        {
            var node = root.SetList(TestData.keyB);
            node.AddString(stringB);
            var childList = node.AddList();
            childList.AddInt(intB);
            childList.AddFloat(floatB);
            SetupState();

            state.BeginList(TestData.keyB);
            state.SyncStringElement(ref stringA);
            state.BeginListElement();
            state.SyncIntElement(ref intA);
            state.SyncFloatElement(ref floatA);
            state.EndListElement();
            state.EndList();

            Assert.AreEqual(intB, intA);
            Assert.AreEqual(stringB, stringA);
        }

        [Test]
        public void ValidListElementBeforeValueElements()
        {
            var node = root.SetList(TestData.keyB);

            var childList = node.AddList();
            childList.AddInt(intB);
            childList.AddFloat(floatB);

            node.AddString(stringB);
            SetupState();

            state.BeginList(TestData.keyB);
            state.BeginListElement();
            state.SyncIntElement(ref intA);
            state.SyncFloatElement(ref floatA);
            state.EndListElement();
            state.SyncStringElement(ref stringA);
            state.EndList();

            Assert.AreEqual(intB, intA);
            Assert.AreEqual(floatB, floatA);
            Assert.AreEqual(stringB, stringA);
        }

        [Test]
        public void ValidListElementBetweenValueElements()
        {
            var node = root.SetList(TestData.keyB);
            node.AddInt(intB);
            node.AddList().AddFloat(floatB);
            node.AddString(stringB);
            SetupState();

            state.BeginList(TestData.keyB);
            state.SyncIntElement(ref intA);

            state.BeginListElement();
            state.SyncFloatElement(ref floatA);
            state.EndListElement();

            state.SyncStringElement(ref stringA);
            state.EndList();

            Assert.AreEqual(intB, intA);
            Assert.AreEqual(floatB, floatA);
            Assert.AreEqual(stringB, stringA);
        }

        [Test]
        public void ValidNonExistentListSync()
        {
            SetupState();

            int oldA = intA;

            state.BeginList(TestData.keyB);
            state.BeginListElement();
            state.SyncIntElement(ref intA);
            state.EndListElement();
            state.EndList();

            Assert.AreEqual(oldA, intA);
        }

        [Test]
        public void ValidNonExistentListElementSync()
        {
            root.SetList(TestData.keyB);
            SetupState();

            int oldA = intA;

            state.BeginList(TestData.keyB);
            state.BeginListElement();
            state.SyncIntElement(ref intA);
            state.EndListElement();
            state.EndList();

            Assert.AreEqual(oldA, intA);
        }

        [Test]
        public void ValidNonExistentValueElementSync()
        {
            root.SetList(TestData.keyB).AddList();
            SetupState();

            int oldA = intA;

            state.BeginList(TestData.keyB);
            state.BeginListElement();
            state.SyncIntElement(ref intA);
            state.EndListElement();
            state.EndList();

            Assert.AreEqual(oldA, intA);
        }

        [Test]
        public void ValidNonExistentDictionarySync()
        {
            SetupState();

            int oldA = intA;

            state.BeginDictionary(TestData.keyB);
            state.SyncInt(TestData.keyA, ref intA);
            state.EndDictionary();

            Assert.AreEqual(oldA, intA);
        }

        [Test]
        public void ValidNonExistentDictionaryElementSync()
        {
            root.SetList(TestData.keyB);
            SetupState();

            int oldA = intA;

            state.BeginList(TestData.keyB);
            state.BeginDictionaryElement();
            state.SyncInt(TestData.keyA, ref intA);
            state.EndDictionaryElement();
            state.EndList();

            Assert.AreEqual(oldA, intA);
        }

        [Test]
        public void ValidNonExistentValueSync()
        {
            root.SetList(TestData.keyB).AddDictionary();
            SetupState();

            int oldA = intA;

            state.BeginList(TestData.keyB);
            state.BeginDictionaryElement();
            state.SyncInt(TestData.keyA, ref intA);
            state.EndDictionaryElement();
            state.EndList();

            Assert.AreEqual(oldA, intA);
        }
    }
}