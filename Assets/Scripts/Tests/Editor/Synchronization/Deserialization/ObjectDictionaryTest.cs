﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.Nodes;
using NSubstitute;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Deserialization
{
    public class ObjectDictionaryTest : BaseDeserializationStateTest
    {
        private Dictionary<string, ISynchronizable> _dictionaryA;
        private Dictionary<string, ISynchronizable> _dictionaryB;
        private ISynchronizationFactory<ISynchronizable> _factory;

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            _dictionaryA = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListA) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListB) }
            };

            _dictionaryB = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListSmaller) }
            };

            _factory = Substitute.For<ISynchronizationFactory<ISynchronizable>>();
            _factory.Deserialize(null)
                .ReturnsForAnyArgs(
                        info => new FloatSynchronizableTestObject(TestData.floatListA)
                    );
        }

        [Test]
        public void ValidSyncObjectDictionary()
        {
            SerializeTestDictionary(
                    _dictionaryA,
                    root.SetDictionary(TestData.keyA)
                );

            SetupState();

            state.SyncObjectDictionary(TestData.keyA, ref _dictionaryB);

            bool areEqual = _dictionaryB.SequenceEqual(
                    _dictionaryA,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncObjectDictionaryLarger()
        {
            var dictSource = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListSmaller) }
            };

            var dictTarget = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyA, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListB) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListA) },
                { TestData.keyD, new FloatSynchronizableTestObject(TestData.floatListA) }
            };

            var dictExpected = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyA, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListSmaller) },
                { TestData.keyD, new FloatSynchronizableTestObject(TestData.floatListA) }
            };

            SerializeTestDictionary(
                    dictSource,
                    root.SetDictionary(TestData.keyA)
                );

            SetupState();

            state.SyncObjectDictionary(TestData.keyA, ref dictTarget);

            bool areEqual = dictTarget.SequenceEqual(
                    dictExpected,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncObjectDictionarySmaller()
        {
            var source = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyA, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListB) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListA) },
                { TestData.keyD, new FloatSynchronizableTestObject(TestData.floatListA) }
            };

            var target = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListSmaller) }
            };

            var expected = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListB) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListA) }
            };

            SerializeTestDictionary(
                    source,
                    root.SetDictionary(TestData.keyA)
                );

            SetupState();

            state.SyncObjectDictionary(TestData.keyA, ref target);

            bool areEqual = target.SequenceEqual(
                    expected,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncNonExistentObjectDictionary()
        {
            SetupState();

            var oldElements = new Dictionary<string, ISynchronizable>(_dictionaryB);

            state.SyncObjectDictionary(TestData.keyA, ref _dictionaryB);

            bool areEqual = _dictionaryB.SequenceEqual(
                    oldElements,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void InvalidSyncNullObjectDictionary()
        {
            SerializeTestDictionary(
                    _dictionaryA,
                    root.SetDictionary(TestData.keyA)
                );

            SetupState();

            _dictionaryB = null;

            Assert.Throws<InvalidOperationException>(
                    () => state.SyncObjectDictionary(TestData.keyA, ref _dictionaryB)
                );
        }

        [Test]
        public void ValidSyncObjectDictionaryElement()
        {
            SerializeTestDictionary(
                    _dictionaryA,
                    root.SetList(TestData.keyA).AddDictionary()
                );

            SetupState();

            state.BeginList(TestData.keyA);
            state.SyncObjectDictionaryElement(_dictionaryB);
            state.EndList();

            bool areEqual = _dictionaryB.SequenceEqual(
                    _dictionaryA,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncObjectDictionaryElementLarger()
        {
            var dictSource = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListSmaller) }
            };

            var dictTarget = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyA, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListB) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListA) },
                { TestData.keyD, new FloatSynchronizableTestObject(TestData.floatListA) }
            };

            var dictExpected = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyA, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListSmaller) },
                { TestData.keyD, new FloatSynchronizableTestObject(TestData.floatListA) }
            };

            SerializeTestDictionary(
                    dictSource,
                    root.SetList(TestData.keyA).AddDictionary()
                );

            SetupState();

            state.BeginList(TestData.keyA);
            state.SyncObjectDictionaryElement(dictTarget);
            state.EndList();

            bool areEqual = dictTarget.SequenceEqual(
                    dictExpected,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncObjectDictionaryElementSmaller()
        {
            var source = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyA, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListB) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListA) },
                { TestData.keyD, new FloatSynchronizableTestObject(TestData.floatListA) }
            };

            var target = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListSmaller) }
            };

            var expected = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListB) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListA) }
            };

            SerializeTestDictionary(
                    source,
                    root.SetList(TestData.keyA).AddDictionary()
                );

            SetupState();

            state.BeginList(TestData.keyA);
            state.SyncObjectDictionaryElement(target);
            state.EndList();

            bool areEqual = target.SequenceEqual(
                    expected,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncNonExistentObjectDictionaryElement()
        {
            root.SetList(TestData.keyA);
            SetupState();

            var oldElements = new Dictionary<string, ISynchronizable>(_dictionaryB);

            state.BeginList(TestData.keyA);
            state.SyncObjectDictionaryElement(_dictionaryB);
            state.EndList();

            bool areEqual = _dictionaryB.SequenceEqual(
                    oldElements,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void InvalidSyncNullObjectDictionaryElement()
        {
            SerializeTestDictionary(
                    _dictionaryA,
                    root.SetList(TestData.keyA).AddDictionary()
                );

            SetupState();

            _dictionaryB = null;

            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(
                    () => state.SyncObjectDictionaryElement(_dictionaryB)
                );
        }

        [Test]
        public void ValidSyncObjectDictionaryWithFactory()
        {
            SerializeTestDictionaryWithFactory(
                    _dictionaryA,
                    root.SetDictionary(TestData.keyA)
                );

            SetupState();

            state.SyncObjectDictionary(TestData.keyA, ref _dictionaryB, _factory);

            bool areEqual = _dictionaryB.SequenceEqual(
                    _dictionaryA,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncObjectDictionaryLargerWithFactory()
        {
            var source = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListSmaller) }
            };

            var target = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyA, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListB) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListA) },
                { TestData.keyD, new FloatSynchronizableTestObject(TestData.floatListA) }
            };

            SerializeTestDictionaryWithFactory(
                    source,
                    root.SetDictionary(TestData.keyA)
                );

            SetupState();

            state.SyncObjectDictionary(TestData.keyA, ref target, _factory);

            bool areEqual = target.SequenceEqual(
                    source,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncObjectDictionarySmallerWithFactory()
        {
            var source = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyA, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListB) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListA) },
                { TestData.keyD, new FloatSynchronizableTestObject(TestData.floatListA) }
            };

            var target = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListSmaller) }
            };

            SerializeTestDictionaryWithFactory(
                    source,
                    root.SetDictionary(TestData.keyA)
                );

            SetupState();

            state.SyncObjectDictionary(TestData.keyA, ref target, _factory);

            bool areEqual = target.SequenceEqual(
                    source,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncNonExistentObjectDictionaryWithFactory()
        {
            SetupState();

            var oldElements = new Dictionary<string, ISynchronizable>(_dictionaryB);

            state.SyncObjectDictionary(TestData.keyA, ref _dictionaryB, _factory);

            bool areEqual = _dictionaryB.SequenceEqual(
                    oldElements,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncNullObjectDictionaryWithFactory()
        {
            SerializeTestDictionaryWithFactory(
                    _dictionaryA,
                    root.SetDictionary(TestData.keyA)
                );

            SetupState();

            _dictionaryB = null;

            state.SyncObjectDictionary(TestData.keyA, ref _dictionaryB, _factory);

            bool areEqual = _dictionaryB.SequenceEqual(
                    _dictionaryA,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncObjectDictionaryElementWithFactory()
        {
            SerializeTestDictionaryWithFactory(
                    _dictionaryA,
                    root.SetList(TestData.keyA).AddDictionary()
                );

            SetupState();

            state.BeginList(TestData.keyA);
            state.SyncObjectDictionaryElement(ref _dictionaryB, _factory);
            state.EndList();

            bool areEqual = _dictionaryB.SequenceEqual(
                    _dictionaryA,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncObjectDictionaryElementLargerWithFactory()
        {
            var dictSource = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListSmaller) }
            };

            var dictTarget = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyA, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListB) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListA) },
                { TestData.keyD, new FloatSynchronizableTestObject(TestData.floatListA) }
            };

            SerializeTestDictionaryWithFactory(
                    dictSource,
                    root.SetList(TestData.keyA).AddDictionary()
                );

            SetupState();

            state.BeginList(TestData.keyA);
            state.SyncObjectDictionaryElement(ref dictTarget, _factory);
            state.EndList();

            bool areEqual = dictTarget.SequenceEqual(
                    dictSource,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncObjectDictionaryElementSmallerWithFactory()
        {
            var source = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyA, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListB) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListA) },
                { TestData.keyD, new FloatSynchronizableTestObject(TestData.floatListA) }
            };

            var target = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListLarger) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListSmaller) }
            };

            SerializeTestDictionaryWithFactory(
                    source,
                    root.SetList(TestData.keyA).AddDictionary()
                );

            SetupState();

            state.BeginList(TestData.keyA);
            state.SyncObjectDictionaryElement(ref target, _factory);
            state.EndList();

            bool areEqual = target.SequenceEqual(
                    source,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncNonExistentObjectDictionaryElementWithFactory()
        {
            root.SetList(TestData.keyA);
            SetupState();

            var oldElements = new Dictionary<string, ISynchronizable>(_dictionaryB);

            state.BeginList(TestData.keyA);
            state.SyncObjectDictionaryElement(_dictionaryB);
            state.EndList();

            bool areEqual = _dictionaryB.SequenceEqual(
                    oldElements,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        [Test]
        public void ValidSyncNullObjectDictionaryElementWithFactory()
        {
            SerializeTestDictionaryWithFactory(
                    _dictionaryA,
                    root.SetList(TestData.keyA).AddDictionary()
                );

            SetupState();

            _dictionaryB = null;

            state.BeginList(TestData.keyA);
            state.SyncObjectDictionaryElement(ref _dictionaryB, _factory);
            bool areEqual = _dictionaryB.SequenceEqual(
                    _dictionaryA,
                    new TestKeyValuePairComparer()
                );
            Assert.IsTrue(areEqual);
        }

        private static void SerializeTestDictionary(
            Dictionary<string, ISynchronizable> dictionary,
            DictionaryNode dictionaryNode)
        {
            foreach (var pair in dictionary)
            {
                ListNode valuesList = dictionaryNode.SetDictionary(pair.Key)
                    .SetList(
                            FloatSynchronizableTestObject.valuesId
                        );

                var values = (pair.Value as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }
        }

        private static void SerializeTestDictionaryWithFactory(
            Dictionary<string, ISynchronizable> dictionary,
            DictionaryNode dictionaryNode)
        {
            foreach (var pair in dictionary)
            {
                DictionaryNode objectNode = dictionaryNode.SetDictionary(pair.Key);
                objectNode.SetDictionary("Factory");
                DictionaryNode objectDataNode = objectNode.SetDictionary("Object");
                ListNode valuesList = objectDataNode.SetList(
                        FloatSynchronizableTestObject.valuesId
                    );

                var values = (pair.Value as FloatSynchronizableTestObject).Values;
                foreach (float value in values)
                {
                    valuesList.AddFloat(value);
                }
            }
        }

        #region Inner types

        private class TestKeyValuePairComparer : IEqualityComparer<KeyValuePair<string, ISynchronizable>>
        {
            public bool Equals(
                    KeyValuePair<string, ISynchronizable> a,
                    KeyValuePair<string, ISynchronizable> b
                )
            {
                if (a.Key != b.Key)
                {
                    return false;
                }

                var objA = (a.Value as FloatSynchronizableTestObject);
                var objB = (b.Value as FloatSynchronizableTestObject);
                return objA.Values.SequenceEqual(objB.Values);
            }

            public int GetHashCode(KeyValuePair<string, ISynchronizable> obj)
            {
                return obj.GetHashCode();
            }
        }

        #endregion
    }
}