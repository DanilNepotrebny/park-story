﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization.Nodes;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Nodes
{
    public class DictionaryNodeTest
    {
        public abstract class BaseFixture
        {
            protected DictionaryNode _node;

            protected const string key = "Key";
            protected const string stringA = "A";
            protected const string stringB = "B";
            protected const float floatA = 10.0f;
            protected const float floatB = 500.5f;
            protected const int intA = 110;
            protected const int intB = 1500;

            [SetUp]
            public void Setup()
            {
                _node = Create();
            }

            [Test]
            public void NotNull()
            {
                Assert.IsNotNull(_node);
            }

            [Test]
            public void GetNotExistedInt()
            {
                Assert.Throws<KeyNotFoundException>(() => _node.GetInt("NotExistedInt"));
            }

            [Test]
            public void GetNotExistedIntWithDefault()
            {
                Assert.AreEqual(intA, _node.GetInt(key, intA));
            }

            [Test]
            public void SetInt()
            {
                _node.SetInt(key, intA);
                Assert.AreEqual(intA, _node.GetInt(key));
            }

            [Test]
            public void SetExistedInt()
            {
                _node.SetInt(key, intA);
                _node.SetInt(key, intB);
                Assert.AreEqual(intB, _node.GetInt(key));
            }

            [Test]
            public void GetNotExistedFloat()
            {
                Assert.Throws<KeyNotFoundException>(() => _node.GetFloat(key));
            }

            [Test]
            public void GetNotExistedFloatWithDefault()
            {
                Assert.AreEqual(floatA, _node.GetFloat(key, floatA));
            }

            [Test]
            public void SetFloat()
            {
                _node.SetFloat(key, floatA);
                Assert.AreEqual(floatA, _node.GetFloat(key));
            }

            [Test]
            public void SetExistedFloat()
            {
                _node.SetFloat(key, floatA);
                _node.SetFloat(key, floatB);
                Assert.AreEqual(floatB, _node.GetFloat(key));
            }

            [Test]
            public void GetNotExistedString()
            {
                Assert.Throws<KeyNotFoundException>(() => _node.GetString(key));
            }

            [Test]
            public void GetNotExistedStringWithDefault()
            {
                Assert.AreEqual(stringA, _node.GetString(key, stringA));
            }

            [Test]
            public void SetString()
            {
                _node.SetString(key, stringA);
                Assert.AreEqual(stringA, _node.GetString(key));
            }

            [Test]
            public void SetExistedString()
            {
                _node.SetString(key, stringA);
                _node.SetString(key, stringB);
                Assert.AreEqual(stringB, _node.GetString(key));
            }

            [TestCase(intA)]
            [TestCase(floatA)]
            [TestCase(stringA)]
            public void GetNotExistedValue<T>(T value)
            {
                Assert.Throws<KeyNotFoundException>(() => _node.GetValue<T>(key));
            }

            [TestCase(intA)]
            [TestCase(floatA)]
            [TestCase(stringA)]
            public void GetNotExistedValueWithDefault<T>(T defaultValue)
            {
                Assert.AreEqual(defaultValue, _node.GetValue<T>(key, defaultValue));
            }

            [TestCase(intA)]
            [TestCase(floatA)]
            [TestCase(stringA)]
            public void SetValue<T>(T value)
            {
                _node.SetValue<T>(key, value);
                Assert.AreEqual(value, _node.GetValue<T>(key));
            }

            [TestCase(intA, intB)]
            [TestCase(floatA, floatB)]
            [TestCase(stringA, stringB)]
            public void SetExistedValue<T>(T valueA, T valueB)
            {
                _node.SetValue<T>(key, valueA);
                _node.SetValue<T>(key, valueB);
                Assert.AreEqual(valueB, _node.GetValue<T>(key));
            }

            [Test]
            public void GetValueNonValidType()
            {
                Assert.Throws<InvalidOperationException>(() => _node.GetValue<object>(key));
            }

            [Test]
            public void GetValueNonValidTypeWithDefault()
            {
                Assert.Throws<InvalidOperationException>(() => _node.GetValue<object>(key, this));
            }

            [Test]
            public void SetValueNonValidType()
            {
                Assert.Throws<InvalidOperationException>(() => _node.SetValue<object>(key, this));
            }

            [Test]
            public void SetIntGetFloatWithDefault()
            {
                _node.SetInt(key, intA);
                Assert.Throws<InvalidOperationException>(() => _node.GetFloat(key, floatA));
            }

            [Test]
            public void SetIntGetStringWithDefault()
            {
                _node.SetInt(key, intA);
                Assert.Throws<InvalidOperationException>(() => _node.GetString(key, stringA));
            }

            public void SetIntGetValueWithDefault()
            {
                _node.SetInt(key, intA);
                Assert.AreEqual(intA, _node.GetValue<int>(key, intB));
            }

            [TestCase(floatA)]
            [TestCase(stringA)]
            public void SetIntGetValueWithDefault<T>(T defaultValue)
            {
                _node.SetInt(key, intA);
                Assert.Throws<InvalidOperationException>(() => _node.GetValue(key, defaultValue));
            }

            [Test]
            public void SetFloatGetIntWithDefault()
            {
                _node.SetFloat(key, floatA);
                Assert.Throws<InvalidOperationException>(() => _node.GetInt(key, intA));
            }

            [Test]
            public void SetFloatGetStringWithDefault()
            {
                _node.SetFloat(key, floatA);
                Assert.Throws<InvalidOperationException>(() => _node.GetString(key, stringA));
            }

            public void SetFloatGetValueWithDefault()
            {
                _node.SetFloat(key, floatA);
                Assert.AreEqual(floatA, _node.GetValue<float>(key, floatB));
            }

            [TestCase(intA)]
            [TestCase(stringA)]
            public void SetFloatGetValueWithDefault<T>(T defaultValue)
            {
                _node.SetFloat(key, floatA);
                Assert.Throws<InvalidOperationException>(() => _node.GetValue(key, defaultValue));
            }

            [Test]
            public void SetStringGetIntWithDefault()
            {
                _node.SetString(key, stringA);
                Assert.Throws<InvalidOperationException>(() => _node.GetInt(key, intA));
            }

            [Test]
            public void SetStringGetFloatWithDefault()
            {
                _node.SetString(key, stringA);
                Assert.Throws<InvalidOperationException>(() => _node.GetFloat(key, floatA));
            }

            public void SetStringGetValueWithDefault()
            {
                _node.SetString(key, stringA);
                Assert.AreEqual(stringA, _node.GetValue<string>(key, stringB));
            }

            [TestCase(intA)]
            [TestCase(floatA)]
            public void SetStringGetValueWithDefault<T>(T defaultValue)
            {
                _node.SetString(key, stringA);
                Assert.Throws<InvalidOperationException>(() => _node.GetValue(key, defaultValue));
            }

            [Test]
            public void CreateDictionaryGetIntWithDefault()
            {
                _node.SetDictionary(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetInt(key, intA));
            }

            [Test]
            public void CreateDictionaryGetFloatWithDefault()
            {
                _node.SetDictionary(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetFloat(key, floatA));
            }

            [Test]
            public void CreateDictionaryGetStringWithDefault()
            {
                _node.SetDictionary(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetString(key, stringA));
            }

            [TestCase(intA)]
            [TestCase(floatA)]
            [TestCase(stringA)]
            public void CreateDictionaryGetValueWithDefault<T>(T defaultValue)
            {
                _node.SetDictionary(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetValue(key, defaultValue));
            }

            [Test]
            public void CreateListGetIntWithDefault()
            {
                _node.SetList(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetInt(key, intA));
            }

            [Test]
            public void CreateListGetFloatWithDefault()
            {
                _node.SetList(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetFloat(key, floatA));
            }

            [Test]
            public void CreateListGetStringWithDefault()
            {
                _node.SetList(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetString(key, stringA));
            }

            [TestCase(intA)]
            [TestCase(floatA)]
            [TestCase(stringA)]
            public void CreateListGetValueWithDefault<T>(T defaultValue)
            {
                _node.SetList(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetValue(key, defaultValue));
            }

            [Test]
            public void SetIntGetFloat()
            {
                _node.SetInt(key, intA);
                Assert.Throws<InvalidOperationException>(() => _node.GetFloat(key));
            }

            [Test]
            public void SetIntGetString()
            {
                _node.SetInt(key, intA);
                Assert.Throws<InvalidOperationException>(() => _node.GetString(key));
            }

            [Test]
            public void SetIntGetDictionary()
            {
                _node.SetInt(key, intA);
                Assert.Throws<InvalidOperationException>(() => _node.GetDictionary(key));
            }

            [Test]
            public void SetIntGetList()
            {
                _node.SetInt(key, intA);
                Assert.Throws<InvalidOperationException>(() => _node.GetList(key));
            }

            public void SetIntGetValue()
            {
                _node.SetInt(key, intA);
                Assert.AreEqual(intA, _node.GetValue<int>(key));
            }

            [TestCase(floatB)]
            [TestCase(stringB)]
            public void SetIntGetValue<T>(T value)
            {
                _node.SetInt(key, intA);
                Assert.Throws<InvalidOperationException>(() => _node.GetValue<T>(key));
            }

            [Test]
            public void SetFloatGetInt()
            {
                _node.SetFloat(key, floatA);
                Assert.Throws<InvalidOperationException>(() => _node.GetInt(key));
            }

            [Test]
            public void SetFloatGetString()
            {
                _node.SetFloat(key, floatA);
                Assert.Throws<InvalidOperationException>(() => _node.GetString(key));
            }

            [Test]
            public void SetFloatGetDictionary()
            {
                _node.SetFloat(key, floatA);
                Assert.Throws<InvalidOperationException>(() => _node.GetDictionary(key));
            }

            [Test]
            public void SetFloatGetList()
            {
                _node.SetFloat(key, floatA);
                Assert.Throws<InvalidOperationException>(() => _node.GetList(key));
            }

            public void SetFloatGetValue()
            {
                _node.SetFloat(key, floatA);
                Assert.AreEqual(floatA, _node.GetValue<float>(key));
            }

            [TestCase(intA)]
            [TestCase(stringA)]
            public void SetFloatGetValue<T>(T value)
            {
                _node.SetFloat(key, floatA);
                Assert.Throws<InvalidOperationException>(() => _node.GetValue<T>(key));
            }

            [Test]
            public void SetStringGetInt()
            {
                _node.SetString(key, stringA);
                Assert.Throws<InvalidOperationException>(() => _node.GetInt(key));
            }

            [Test]
            public void SetStringGetFloat()
            {
                _node.SetString(key, stringA);
                Assert.Throws<InvalidOperationException>(() => _node.GetFloat(key));
            }

            [Test]
            public void SetStringGetDictionary()
            {
                _node.SetString(key, stringA);
                Assert.Throws<InvalidOperationException>(() => _node.GetDictionary(key));
            }

            [Test]
            public void SetStringGetList()
            {
                _node.SetString(key, stringA);
                Assert.Throws<InvalidOperationException>(() => _node.GetList(key));
            }

            public void SetStringGetValue()
            {
                _node.SetString(key, stringA);
                Assert.AreEqual(stringA, _node.GetValue<string>(key));
            }

            [TestCase(intA)]
            [TestCase(floatA)]
            public void SetStringGetValue<T>(T value)
            {
                _node.SetString(key, stringA);
                Assert.Throws<InvalidOperationException>(() => _node.GetValue<T>(key));
            }

            [Test]
            public void CreateDictionaryGetInt()
            {
                _node.SetDictionary(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetInt(key));
            }

            [Test]
            public void CreateDictionaryGetFloat()
            {
                _node.SetDictionary(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetFloat(key));
            }

            [Test]
            public void CreateDictionaryGetString()
            {
                _node.SetDictionary(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetString(key));
            }

            [Test]
            public void CreateDictionaryGetList()
            {
                _node.SetDictionary(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetList(key));
            }

            [TestCase(intA)]
            [TestCase(floatA)]
            [TestCase(stringA)]
            public void CreateDictionaryGetValue<T>(T value)
            {
                _node.SetDictionary(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetValue<T>(key));
            }

            [Test]
            public void CreateListGetInt()
            {
                _node.SetList(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetInt(key));
            }

            [Test]
            public void CreateListGetFloat()
            {
                _node.SetList(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetFloat(key));
            }

            [Test]
            public void CreateListGetString()
            {
                _node.SetList(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetString(key));
            }

            [Test]
            public void CreateListGetDictionary()
            {
                _node.SetList(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetDictionary(key));
            }

            [TestCase(intA)]
            [TestCase(floatA)]
            [TestCase(stringA)]
            public void CreateListGetValue<T>(T value)
            {
                _node.SetList(key);
                Assert.Throws<InvalidOperationException>(() => _node.GetValue<T>(key));
            }

            [Test]
            public void SetIntSetFloat()
            {
                _node.SetInt(key, intA);
                _node.SetFloat(key, floatA);
                Assert.AreEqual(floatA, _node.GetFloat(key));
            }

            [Test]
            public void SetIntSetString()
            {
                _node.SetInt(key, intA);
                _node.SetString(key, stringA);
                Assert.AreEqual(stringA, _node.GetString(key));
            }

            [Test]
            public void SetIntCreateDictionary()
            {
                _node.SetInt(key, intA);
                var node = _node.SetDictionary(key);
                Assert.AreEqual(node, _node.GetDictionary(key));
            }

            [Test]
            public void SetIntCreateList()
            {
                _node.SetInt(key, intA);
                var node = _node.SetList(key);
                Assert.AreEqual(node, _node.GetList(key));
            }

            [TestCase(intB)]
            [TestCase(floatB)]
            [TestCase(stringB)]
            public void SetIntSetValue<T>(T value)
            {
                _node.SetInt(key, intA);
                _node.SetValue(key, value);
                Assert.AreEqual(value, _node.GetValue<T>(key));
            }

            [Test]
            public void SetFloatSetInt()
            {
                _node.SetFloat(key, floatA);
                _node.SetInt(key, intA);
                Assert.AreEqual(intA, _node.GetInt(key));
            }

            [Test]
            public void SetFloatSetString()
            {
                _node.SetFloat(key, floatA);
                _node.SetString(key, stringA);
                Assert.AreEqual(stringA, _node.GetString(key));
            }

            [Test]
            public void SetFloatCreateDictionary()
            {
                _node.SetFloat(key, floatA);
                var node = _node.SetDictionary(key);
                Assert.AreEqual(node, _node.GetDictionary(key));
            }

            [Test]
            public void SetFloatCreateList()
            {
                _node.SetFloat(key, floatA);
                var node = _node.SetList(key);
                Assert.AreEqual(node, _node.GetList(key));
            }

            [TestCase(intB)]
            [TestCase(floatB)]
            [TestCase(stringB)]
            public void SetFloatSetValue<T>(T value)
            {
                _node.SetFloat(key, floatA);
                _node.SetValue(key, value);
                Assert.AreEqual(value, _node.GetValue<T>(key));
            }

            [Test]
            public void SetStringSetInt()
            {
                _node.SetString(key, stringA);
                _node.SetInt(key, intA);
                Assert.AreEqual(intA, _node.GetInt(key));
            }

            [Test]
            public void SetStringSetFloat()
            {
                _node.SetString(key, stringA);
                _node.SetFloat(key, floatA);
                Assert.AreEqual(floatA, _node.GetFloat(key));
            }

            [Test]
            public void SetStringCreateDictionary()
            {
                _node.SetString(key, stringA);
                var node = _node.SetDictionary(key);
                Assert.AreEqual(node, _node.GetDictionary(key));
            }

            [Test]
            public void SetStringCreateList()
            {
                _node.SetString(key, stringA);
                var node = _node.SetList(key);
                Assert.AreEqual(node, _node.GetList(key));
            }

            [TestCase(intB)]
            [TestCase(floatB)]
            [TestCase(stringB)]
            public void SetStringSetValue<T>(T value)
            {
                _node.SetString(key, stringA);
                _node.SetValue(key, value);
                Assert.AreEqual(value, _node.GetValue<T>(key));
            }

            [Test]
            public void CreateDictionarySetInt()
            {
                _node.SetDictionary(key);
                _node.SetInt(key, intA);
                Assert.AreEqual(intA, _node.GetInt(key));
            }

            [Test]
            public void CreateDictionarySetFloat()
            {
                _node.SetDictionary(key);
                _node.SetFloat(key, floatA);
                Assert.AreEqual(floatA, _node.GetFloat(key));
            }

            [Test]
            public void CreateDictionarySetString()
            {
                _node.SetDictionary(key);
                _node.SetString(key, stringA);
                Assert.AreEqual(stringA, _node.GetString(key));
            }

            [Test]
            public void CreateDictionaryCreateList()
            {
                _node.SetDictionary(key);
                var node = _node.SetList(key);
                Assert.AreEqual(node, _node.GetList(key));
            }

            [TestCase(intB)]
            [TestCase(floatB)]
            [TestCase(stringB)]
            public void CreateDictionarySetValue<T>(T value)
            {
                _node.SetDictionary(key);
                _node.SetValue(key, value);
                Assert.AreEqual(value, _node.GetValue<T>(key));
            }

            [Test]
            public void CreateListSetInt()
            {
                _node.SetList(key);
                _node.SetInt(key, intA);
                Assert.AreEqual(intA, _node.GetInt(key));
            }

            [Test]
            public void CreateListSetFloat()
            {
                _node.SetList(key);
                _node.SetFloat(key, floatA);
                Assert.AreEqual(floatA, _node.GetFloat(key));
            }

            [Test]
            public void CreateListSetString()
            {
                _node.SetList(key);
                _node.SetString(key, stringA);
                Assert.AreEqual(stringA, _node.GetString(key));
            }

            [Test]
            public void CreateListCreateDictionary()
            {
                _node.SetList(key);
                var node = _node.SetDictionary(key);
                Assert.AreEqual(node, _node.GetDictionary(key));
            }

            [TestCase(intB)]
            [TestCase(floatB)]
            [TestCase(stringB)]
            public void CreateListSetValue<T>(T value)
            {
                _node.SetList(key);
                _node.SetValue(key, value);
                Assert.AreEqual(value, _node.GetValue<T>(key));
            }

            [Test]
            public void GetNotExistedDictionary()
            {
                Assert.Throws<KeyNotFoundException>(() => _node.GetDictionary(key));
            }

            [Test]
            public void CreateDictionary()
            {
                _node.SetDictionary(key);
                Assert.IsNotNull(_node.GetDictionary(key));
            }

            [Test]
            public void SetExistedDictionary()
            {
                DictionaryNode oldNode = _node.SetDictionary(key);
                DictionaryNode newNode = _node.SetDictionary(key);
                Assert.AreNotSame(oldNode, _node.GetDictionary(key));
                Assert.AreSame(newNode, _node.GetDictionary(key));
            }

            [Test]
            public void GetNotExistedList()
            {
                Assert.Throws<KeyNotFoundException>(() => _node.GetList(key));
            }

            [Test]
            public void CreateList()
            {
                _node.SetList(key);
                Assert.IsNotNull(_node.GetList(key));
            }

            [Test]
            public void SetExistedList()
            {
                ListNode oldNode = _node.SetList(key);
                ListNode newNode = _node.SetList(key);
                Assert.AreNotSame(oldNode, _node.GetList(key));
                Assert.AreSame(newNode, _node.GetList(key));
            }

            [Test]
            public void CheckParentOfDictionary()
            {
                _node.SetDictionary(key);
                Assert.AreSame(_node, _node.GetDictionary(key).Parent);
            }

            [Test]
            public void CheckIdOfDictionary()
            {
                _node.SetDictionary(key);
                Assert.AreEqual(key, _node.GetDictionary(key).Id);
            }

            [Test]
            public void CheckParentOfList()
            {
                _node.SetList(key);
                Assert.AreSame(_node, _node.GetList(key).Parent);
            }

            [Test]
            public void CheckIdOfList()
            {
                _node.SetList(key);
                Assert.AreEqual(key, _node.GetList(key).Id);
            }

            [Test]
            public void ForEach()
            {
                List<object> expectedValues = new List<object>();

                _node.SetInt("intA", intA);
                expectedValues.Add(intA);

                _node.SetFloat("floatA", floatA);
                expectedValues.Add(floatA);

                _node.SetString("stringA", stringA);
                expectedValues.Add(stringA);

                expectedValues.Add(_node.SetDictionary("dictionary"));
                expectedValues.Add(_node.SetList("list"));

                List<object> actualValues = new List<object>();
                foreach (Node node in _node)
                {
                    if (node is ValueNode)
                    {
                        actualValues.Add((node as ValueNode).Value);
                    }
                    else
                    {
                        actualValues.Add(node);
                    }
                }

                Assert.IsTrue(
                        expectedValues.SequenceEqual(actualValues),
                        "Expected values and actual values are not equal " +
                        $"(expected: [{string.Join(", ", expectedValues)}], actual: [{string.Join(", ", actualValues)}])"
                    );
            }

            protected abstract DictionaryNode Create();
        }

        [TestFixture]
        public class TestRoot : BaseFixture
        {
            protected override DictionaryNode Create()
            {
                return new DictionaryNode();
            }

            [Test]
            public void ParentIsNull()
            {
                Assert.IsNull(_node.Parent);
            }
        }

        [TestFixture]
        public class TestChild : BaseFixture
        {
            private DictionaryNode _root;

            protected override DictionaryNode Create()
            {
                _root = new DictionaryNode();
                return _root.SetDictionary("Child");
            }

            [Test]
            public void ParentIsRoot()
            {
                Assert.AreSame(_root, _node.Parent);
            }
        }
    }
}