﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization.Nodes;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Nodes
{
    public class ListNodeTest
    {
        public abstract class BaseFixture
        {
            protected ListNode _node;

            protected const string stringA = "A";
            protected const string stringB = "B";
            protected const string stringC = "C";
            protected const float floatA = 10.0f;
            protected const float floatB = 500.5f;
            protected const float floatC = -1500.3f;
            protected const int intA = 110;
            protected const int intB = 1500;
            protected const int intC = -10500;

            [SetUp]
            public void Setup()
            {
                _node = CreateNode();
            }

            [Test]
            public void NotNull()
            {
                Assert.IsNotNull(_node);
            }

            [Test]
            public void CheckCount()
            {
                _node.AddInt(intA);
                _node.AddFloat(floatA);
                _node.AddString(stringA);
                _node.AddInt(intB);
                _node.AddFloat(floatB);
                _node.AddString(stringB);
                _node.AddInt(intC);
                _node.AddFloat(floatC);
                _node.AddString(stringC);
                Assert.AreEqual(9, _node.Count);
            }

            [Test]
            public void GetNotExistedInt()
            {
                Assert.Throws<IndexOutOfRangeException>(() => _node.GetInt(555));
            }

            [Test]
            public void AddIntGetValue()
            {
                _node.AddInt(intA);
                _node.AddInt(intB);
                Assert.AreEqual(intB, _node.GetInt(1));
            }

            [Test]
            public void SetExistedIntValue()
            {
                _node.AddInt(intA);
                _node.AddInt(intB);
                _node.SetInt(1, intC);
                Assert.AreEqual(intC, _node.GetInt(1));
            }

            [Test]
            public void SetNotExistedIntValue()
            {
                _node.AddInt(intA);
                _node.AddInt(intB);
                _node.AddInt(intC);
                Assert.Throws<IndexOutOfRangeException>(() => _node.SetInt(14, intA));
            }

            [Test]
            public void GetNotExistedFloat()
            {
                Assert.Throws<IndexOutOfRangeException>(() => _node.GetFloat(555));
            }

            [Test]
            public void AddFloatGetValue()
            {
                _node.AddFloat(floatA);
                _node.AddFloat(floatB);
                _node.AddFloat(floatC);
                Assert.AreEqual(floatB, _node.GetFloat(1));
            }

            [Test]
            public void SetExistedFloatValue()
            {
                _node.AddFloat(floatA);
                _node.AddFloat(floatB);
                _node.SetFloat(1, floatC);
                Assert.AreEqual(floatC, _node.GetFloat(1));
            }

            [Test]
            public void SetNotExistedFloatValue()
            {
                Assert.Throws<IndexOutOfRangeException>(() => _node.SetFloat(10, floatA));
            }

            [Test]
            public void GetNotExistedString()
            {
                Assert.Throws<IndexOutOfRangeException>(() => _node.GetString(100));
            }

            [Test]
            public void AddStringGetValue()
            {
                _node.AddString(stringA);
                _node.AddString(stringB);
                _node.AddString(stringC);
                Assert.AreEqual(stringB, _node.GetString(1));
            }

            [Test]
            public void SetExistedStringValue()
            {
                _node.AddString(stringA);
                _node.AddString(stringB);
                _node.SetString(1, stringC);
                Assert.AreEqual(stringC, _node.GetString(1));
            }

            [Test]
            public void SetNotExistedStringValue()
            {
                Assert.Throws<IndexOutOfRangeException>(() => _node.SetString(144, stringA));
            }

            [Test]
            public void AddIntGetFloat()
            {
                _node.AddInt(intA);
                Assert.Throws<InvalidOperationException>(() => _node.GetFloat(0));
            }

            [Test]
            public void AddIntGetString()
            {
                _node.AddInt(intA);
                Assert.Throws<InvalidOperationException>(() => _node.GetString(0));
            }

            [Test]
            public void AddIntGetList()
            {
                _node.AddInt(intA);
                Assert.Throws<InvalidOperationException>(() => _node.GetList(0));
            }

            [Test]
            public void AddIntGetDictionary()
            {
                _node.AddInt(intA);
                Assert.Throws<InvalidOperationException>(() => _node.GetDictionary(0));
            }

            [Test]
            public void AddFloatGetInt()
            {
                _node.AddFloat(floatA);
                Assert.Throws<InvalidOperationException>(() => _node.GetInt(0));
            }

            [Test]
            public void AddFloatGetString()
            {
                _node.AddFloat(floatA);
                Assert.Throws<InvalidOperationException>(() => _node.GetString(0));
            }

            [Test]
            public void AddFloatGetList()
            {
                _node.AddFloat(floatA);
                Assert.Throws<InvalidOperationException>(() => _node.GetList(0));
            }

            [Test]
            public void AddFloatGetDictionary()
            {
                _node.AddFloat(floatA);
                Assert.Throws<InvalidOperationException>(() => _node.GetDictionary(0));
            }

            [Test]
            public void AddStringGetInt()
            {
                _node.AddString(stringA);
                Assert.Throws<InvalidOperationException>(() => _node.GetInt(0));
            }

            [Test]
            public void AddStringGetFloat()
            {
                _node.AddString(stringA);
                Assert.Throws<InvalidOperationException>(() => _node.GetFloat(0));
            }

            [Test]
            public void AddStringGetList()
            {
                _node.AddString(stringA);
                Assert.Throws<InvalidOperationException>(() => _node.GetList(0));
            }

            [Test]
            public void AddStringGetDictionary()
            {
                _node.AddString(stringA);
                Assert.Throws<InvalidOperationException>(() => _node.GetDictionary(0));
            }

            [Test]
            public void AddListGetInt()
            {
                _node.AddList();
                Assert.Throws<InvalidOperationException>(() => _node.GetInt(0));
            }

            [Test]
            public void AddListGetFloat()
            {
                _node.AddList();
                Assert.Throws<InvalidOperationException>(() => _node.GetFloat(0));
            }

            [Test]
            public void AddListGetString()
            {
                _node.AddList();
                Assert.Throws<InvalidOperationException>(() => _node.GetString(0));
            }

            [Test]
            public void AddListGetDictionary()
            {
                _node.AddList();
                Assert.Throws<InvalidOperationException>(() => _node.GetDictionary(0));
            }

            [Test]
            public void AddDictionaryGetInt()
            {
                _node.AddDictionary();
                Assert.Throws<InvalidOperationException>(() => _node.GetInt(0));
            }

            [Test]
            public void AddDictionaryGetFloat()
            {
                _node.AddDictionary();
                Assert.Throws<InvalidOperationException>(() => _node.GetFloat(0));
            }

            [Test]
            public void AddDictionaryGetString()
            {
                _node.AddDictionary();
                Assert.Throws<InvalidOperationException>(() => _node.GetString(0));
            }

            [Test]
            public void AddDictionaryGetList()
            {
                _node.AddDictionary();
                Assert.Throws<InvalidOperationException>(() => _node.GetList(0));
            }

            [Test]
            public void AddIntSetFloat()
            {
                _node.AddInt(intA);
                _node.SetFloat(0, floatA);
                Assert.AreEqual(floatA, _node.GetFloat(0));
            }

            [Test]
            public void AddIntSetString()
            {
                _node.AddInt(intA);
                _node.SetString(0, stringA);
                Assert.AreEqual(stringA, _node.GetString(0));
            }

            [Test]
            public void AddIntCreateList()
            {
                _node.AddInt(intA);
                ListNode node = _node.SetList(0);
                Assert.AreEqual(node, _node.GetList(0));
            }

            [Test]
            public void AddIntCreateDictionary()
            {
                _node.AddInt(intA);
                DictionaryNode node = _node.SetDictionary(0);
                Assert.AreEqual(node, _node.GetDictionary(0));
            }

            [Test]
            public void AddFloatSetInt()
            {
                _node.AddFloat(floatA);
                _node.SetInt(0, intA);
                Assert.AreEqual(intA, _node.GetInt(0));
            }

            [Test]
            public void AddFloatSetString()
            {
                _node.AddFloat(floatA);
                _node.SetString(0, stringA);
                Assert.AreEqual(stringA, _node.GetString(0));
            }

            [Test]
            public void AddFloatCreateList()
            {
                _node.AddFloat(floatA);
                ListNode node = _node.SetList(0);
                Assert.AreEqual(node, _node.GetList(0));
            }

            [Test]
            public void AddFloatCreateDictionary()
            {
                _node.AddFloat(floatA);
                DictionaryNode node = _node.SetDictionary(0);
                Assert.AreEqual(node, _node.GetDictionary(0));
            }

            [Test]
            public void AddStringSetInt()
            {
                _node.AddString(stringA);
                _node.SetInt(0, intA);
                Assert.AreEqual(intA, _node.GetInt(0));
            }

            [Test]
            public void AddStringSetFloat()
            {
                _node.AddString(stringA);
                _node.SetFloat(0, floatA);
                Assert.AreEqual(floatA, _node.GetFloat(0));
            }

            [Test]
            public void AddStringCreateList()
            {
                _node.AddString(stringA);
                ListNode node = _node.SetList(0);
                Assert.AreEqual(node, _node.GetList(0));
            }

            [Test]
            public void AddStringCreateDictionary()
            {
                _node.AddString(stringA);
                DictionaryNode node = _node.SetDictionary(0);
                Assert.AreEqual(node, _node.GetDictionary(0));
            }

            [Test]
            public void AddListSetInt()
            {
                _node.AddList();
                _node.SetInt(0, intA);
                Assert.AreEqual(intA, _node.GetInt(0));
            }

            [Test]
            public void AddListSetFloat()
            {
                _node.AddList();
                _node.SetFloat(0, floatA);
                Assert.AreEqual(floatA, _node.GetFloat(0));
            }

            [Test]
            public void AddListSetString()
            {
                _node.AddList();
                _node.SetString(0, stringA);
                Assert.AreEqual(stringA, _node.GetString(0));
            }

            [Test]
            public void AddListCreateDictionary()
            {
                _node.AddList();
                DictionaryNode node = _node.SetDictionary(0);
                Assert.AreEqual(node, _node.GetDictionary(0));
            }

            [Test]
            public void AddDictionarySetInt()
            {
                _node.AddDictionary();
                _node.SetInt(0, intA);
                Assert.AreEqual(intA, _node.GetInt(0));
            }

            [Test]
            public void AddDictionarySetFloat()
            {
                _node.AddDictionary();
                _node.SetFloat(0, floatA);
                Assert.AreEqual(floatA, _node.GetFloat(0));
            }

            [Test]
            public void AddDictionarySetString()
            {
                _node.AddDictionary();
                _node.SetString(0, stringA);
                Assert.AreEqual(stringA, _node.GetString(0));
            }

            [Test]
            public void AddDictionaryCreateList()
            {
                _node.AddDictionary();
                ListNode node = _node.SetList(0);
                Assert.AreEqual(node, _node.GetList(0));
            }

            [Test]
            public void GetNotExistedDictionary()
            {
                Assert.Throws<IndexOutOfRangeException>(() => _node.GetDictionary(5));
            }

            [Test]
            public void CreateDictionary()
            {
                _node.AddDictionary();
                Assert.IsNotNull(_node.GetDictionary(0));
            }

            [Test]
            public void GetNotExistedListNode()
            {
                Assert.Throws<IndexOutOfRangeException>(() => _node.GetList(66));
            }

            [Test]
            public void CreateListNode()
            {
                _node.AddList();
                Assert.IsNotNull(_node.GetList(0));
            }

            [Test]
            public void CheckParentOfDictionaryNode()
            {
                _node.AddDictionary();
                Assert.AreSame(_node, _node.GetDictionary(0).Parent);
            }

            [Test]
            public void CheckParentOfListNode()
            {
                _node.AddList();
                Assert.AreSame(_node, _node.GetList(0).Parent);
            }

            [Test]
            public void ForEach()
            {
                List<object> expectedValues = new List<object>();

                _node.AddInt(intA);
                expectedValues.Add(intA);

                _node.AddFloat(floatA);
                expectedValues.Add(floatA);

                _node.AddString(stringA);
                expectedValues.Add(stringA);

                expectedValues.Add(_node.AddDictionary());
                expectedValues.Add(_node.AddList());

                List<object> actualValues = new List<object>();
                foreach (Node node in _node)
                {
                    if (node is ValueNode)
                    {
                        actualValues.Add((node as ValueNode).Value);
                    }
                    else
                    {
                        actualValues.Add(node);
                    }
                }

                Assert.IsTrue(
                        expectedValues.SequenceEqual(actualValues),
                        "Expected values and actual values are not equal " +
                        $"(expected: [{string.Join(", ", expectedValues)}], actual: [{string.Join(", ", actualValues)}])"
                    );
            }

            protected abstract ListNode CreateNode();
        }

        [TestFixture]
        public class TestRoot : BaseFixture
        {
            protected override ListNode CreateNode()
            {
                return new ListNode();
            }

            [Test]
            public void ParentIsNull()
            {
                Assert.IsNull(_node.Parent);
            }
        }

        [TestFixture]
        public class TestChild : BaseFixture
        {
            private ListNode _root;

            protected override ListNode CreateNode()
            {
                _root = new ListNode();
                return _root.AddList();
            }

            [Test]
            public void ParentIsRoot()
            {
                Assert.AreSame(_root, _node.Parent);
            }
        }
    }
}