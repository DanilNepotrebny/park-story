﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.Nodes;
using DreamTeam.Synchronization.States;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Serialization
{
    public class ObjectDictionaryTest
    {
        protected SerializationState state;
        protected DictionaryNode root;

        private Dictionary<string, ISynchronizable> _dictionary;
        private Dictionary<string, ISynchronizable> _dictionaryWithNullElement;

        [SetUp]
        public void Setup()
        {
            state = new SerializationState();
            root = state.Root;

            _dictionary = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, new FloatSynchronizableTestObject(TestData.floatListA) },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListB) }
            };

            _dictionaryWithNullElement = new Dictionary<string, ISynchronizable>
            {
                { TestData.keyB, null },
                { TestData.keyC, new FloatSynchronizableTestObject(TestData.floatListB) }
            };
        }

        [Test]
        public void ValidSyncObjectDictionary()
        {
            state.SyncObjectDictionary(TestData.keyA, ref _dictionary);

            var dictionaryNode = root.GetDictionary(TestData.keyA);
            foreach (Node node in dictionaryNode)
            {
                var valuesListNode = (node as DictionaryNode).GetList(FloatSynchronizableTestObject.valuesId);

                float[] actualListValues = new float[valuesListNode.Count];
                for (int j = 0; j < valuesListNode.Count; ++j)
                {
                    actualListValues[j] = valuesListNode.GetFloat(j);
                }

                float[] expected = (_dictionary[node.Id] as FloatSynchronizableTestObject).Values;

                Assert.IsTrue(
                        expected.SequenceEqual(actualListValues),
                        $"expected:[{string.Join(", ", expected)}] " +
                        $"actual:[{string.Join(", ", actualListValues)}]"
                    );
            }
        }

        [Test]
        public void InvalidSyncNullObjectDictionary()
        {
            _dictionary = null;
            Assert.Throws<InvalidOperationException>(() => state.SyncObjectDictionary(TestData.keyA, ref _dictionary));
        }

        [Test]
        public void InvalidSyncObjectDictionaryWithNullElements()
        {
            Assert.Throws<InvalidOperationException>(
                () => state.SyncObjectDictionary(TestData.keyA, ref _dictionaryWithNullElement));
        }

        [Test]
        public void ValidSyncPreallocatedObjectDictionaryWithFactory()
        {
            ISynchronizationFactory<ISynchronizable> factory = FactorySubstitute.Create<ISynchronizable>();
            state.SyncObjectDictionary(TestData.keyA, ref _dictionary, factory);

            var dictionaryNode = root.GetDictionary(TestData.keyA);
            foreach (Node node in dictionaryNode)
            {
                var valuesListNode = (node as DictionaryNode).GetDictionary("Object")
                    .GetList(FloatSynchronizableTestObject.valuesId);

                float[] actualListValues = new float[valuesListNode.Count];
                for (int j = 0; j < valuesListNode.Count; ++j)
                {
                    actualListValues[j] = valuesListNode.GetFloat(j);
                }

                float[] expected = (_dictionary[node.Id] as FloatSynchronizableTestObject).Values;

                Assert.IsTrue(
                        expected.SequenceEqual(actualListValues),
                        $"expected:[{string.Join(", ", expected)}] " +
                        $"actual:[{string.Join(", ", actualListValues)}]"
                    );
            }
        }

        [Test]
        public void ValidSyncObjectDictionaryElement()
        {
            state.BeginList(TestData.keyA);
            state.SyncObjectDictionaryElement(_dictionary);
            state.EndList();

            var dictionaryNode = root.GetList(TestData.keyA).GetDictionary(0);
            foreach (Node node in dictionaryNode)
            {
                var valuesListNode = (node as DictionaryNode).GetList(FloatSynchronizableTestObject.valuesId);

                float[] actualListValues = new float[valuesListNode.Count];
                for (int j = 0; j < valuesListNode.Count; ++j)
                {
                    actualListValues[j] = valuesListNode.GetFloat(j);
                }

                float[] expected = (_dictionary[node.Id] as FloatSynchronizableTestObject).Values;

                Assert.IsTrue(
                        expected.SequenceEqual(actualListValues),
                        $"expected:[{string.Join(", ", expected)}] " +
                        $"actual:[{string.Join(", ", actualListValues)}]"
                    );
            }
        }

        [Test]
        public void InvalidSyncNullObjectDictionaryElement()
        {
            _dictionary = null;
            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => state.SyncObjectDictionaryElement(_dictionary));
        }

        [Test]
        public void InvalidSyncObjectDictionaryElementWithNullElements()
        {
            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(
                () => state.SyncObjectDictionaryElement(_dictionaryWithNullElement));
        }

        [Test]
        public void ValidSyncPreallocatedObjectDictionaryElementWithFactory()
        {
            ISynchronizationFactory<ISynchronizable> factory = FactorySubstitute.Create<ISynchronizable>();

            state.BeginList(TestData.keyA);
            state.SyncObjectDictionaryElement(ref _dictionary, factory);
            state.EndList();

            var dictionaryNode = root.GetList(TestData.keyA).GetDictionary(0);
            foreach (Node node in dictionaryNode)
            {
                var valuesListNode = (node as DictionaryNode).GetDictionary("Object")
                    .GetList(FloatSynchronizableTestObject.valuesId);

                float[] actualListValues = new float[valuesListNode.Count];
                for (int j = 0; j < valuesListNode.Count; ++j)
                {
                    actualListValues[j] = valuesListNode.GetFloat(j);
                }

                float[] expected = (_dictionary[node.Id] as FloatSynchronizableTestObject).Values;

                Assert.IsTrue(
                        expected.SequenceEqual(actualListValues),
                        $"expected:[{string.Join(", ", expected)}] " +
                        $"actual:[{string.Join(", ", actualListValues)}]"
                    );
            }
        }
    }
}