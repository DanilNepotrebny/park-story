﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.Nodes;
using DreamTeam.Synchronization.States;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Serialization
{
    public abstract class BaseObjectListTest<TList>
        where TList : class
    {
        protected SerializationState state;
        protected DictionaryNode root;

        private TList _list;

        protected TList List => _list;

        [SetUp]
        public void Setup()
        {
            state = new SerializationState();
            root = state.Root;

            _list = CreateList(
                    new ISynchronizable[]
                    {
                        new FloatSynchronizableTestObject(TestData.floatListA),
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListLarger)
                    }
                );
        }

        [Test]
        public void ValidSyncObjectList()
        {
            SyncList(TestData.keyA, _list);

            var listNode = root.GetList(TestData.keyA);
            for (int i = 0; i < listNode.Count; ++i)
            {
                var valuesListNode = listNode.GetDictionary(i).GetList(FloatSynchronizableTestObject.valuesId);

                float[] actualListValues = new float[valuesListNode.Count];
                for (int j = 0; j < valuesListNode.Count; ++j)
                {
                    actualListValues[j] = valuesListNode.GetFloat(j);
                }

                float[] expected = (GetListValue(i) as FloatSynchronizableTestObject).Values;

                Assert.IsTrue(
                        expected.SequenceEqual(actualListValues),
                        $"expected:[{string.Join(", ", expected)}] " +
                        $"actual:[{string.Join(", ", actualListValues)}]"
                    );
            }
        }

        [Test]
        public void InvalidSyncNullObjectList()
        {
            _list = null;
            Assert.Throws<InvalidOperationException>(() => SyncList(TestData.keyA, _list));
        }

        [Test]
        public void InvalidSyncObjectListWithNullElements()
        {
            _list = CreateListWithNullElement();

            Assert.Throws<InvalidOperationException>(() => SyncList(TestData.keyA, _list));
        }

        [Test]
        public void ValidSyncPreallocatedObjectListWithFactory()
        {
            ISynchronizationFactory<ISynchronizable> factory = FactorySubstitute.Create<ISynchronizable>();
            SyncList(TestData.keyA, ref _list, factory);

            var listNode = root.GetList(TestData.keyA);
            for (int i = 0; i < listNode.Count; ++i)
            {
                var valuesListNode = listNode.GetDictionary(i)
                    .GetDictionary("Object")
                    .GetList(FloatSynchronizableTestObject.valuesId);

                float[] actualListValues = new float[valuesListNode.Count];
                for (int j = 0; j < valuesListNode.Count; ++j)
                {
                    actualListValues[j] = valuesListNode.GetFloat(j);
                }

                float[] expected = (GetListValue(i) as FloatSynchronizableTestObject).Values;

                Assert.IsTrue(
                        expected.SequenceEqual(actualListValues),
                        $"expected:[{string.Join(", ", expected)}] " +
                        $"actual:[{string.Join(", ", actualListValues)}]"
                    );
            }
        }

        [Test]
        public void ValidSyncObjectListElement()
        {
            state.BeginList(TestData.keyA);
            SyncListElement(_list);
            state.EndList();

            var listNode = root.GetList(TestData.keyA).GetList(0);
            for (int i = 0; i < listNode.Count; ++i)
            {
                var valuesListNode = listNode.GetDictionary(i).GetList(FloatSynchronizableTestObject.valuesId);

                float[] actualListValues = new float[valuesListNode.Count];
                for (int j = 0; j < valuesListNode.Count; ++j)
                {
                    actualListValues[j] = valuesListNode.GetFloat(j);
                }

                float[] expected = (GetListValue(i) as FloatSynchronizableTestObject).Values;

                Assert.IsTrue(
                        expected.SequenceEqual(actualListValues),
                        $"expected:[{string.Join(", ", expected)}] " +
                        $"actual:[{string.Join(", ", actualListValues)}]"
                    );
            }
        }

        [Test]
        public void InvalidSyncNullObjectListElement()
        {
            _list = null;

            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => SyncListElement(_list));
        }

        [Test]
        public void InvalidSyncObjectListElementWithNullElements()
        {
            _list = CreateListWithNullElement();

            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => SyncListElement(_list));
        }

        [Test]
        public void ValidSyncPreallocatedObjectListElementWithFactory()
        {
            ISynchronizationFactory<ISynchronizable> factory = FactorySubstitute.Create<ISynchronizable>();

            state.BeginList(TestData.keyA);
            SyncListElement(ref _list, factory);
            state.EndList();

            var listNode = root.GetList(TestData.keyA).GetList(0);
            for (int i = 0; i < listNode.Count; ++i)
            {
                var valuesListNode = listNode.GetDictionary(i)
                    .GetDictionary("Object")
                    .GetList(FloatSynchronizableTestObject.valuesId);

                float[] actualListValues = new float[valuesListNode.Count];
                for (int j = 0; j < valuesListNode.Count; ++j)
                {
                    actualListValues[j] = valuesListNode.GetFloat(j);
                }

                float[] expected = (GetListValue(i) as FloatSynchronizableTestObject).Values;

                Assert.IsTrue(
                        expected.SequenceEqual(actualListValues),
                        $"expected:[{string.Join(", ", expected)}] " +
                        $"actual:[{string.Join(", ", actualListValues)}]"
                    );
            }
        }

        protected abstract TList CreateList(ISynchronizable[] elements);
        protected abstract ISynchronizable GetListValue(int index);

        protected abstract void SyncList(string id, TList list);
        protected abstract void SyncList(string id, ref TList list, ISynchronizationFactory<ISynchronizable> factory);
        protected abstract void SyncListElement(TList list);
        protected abstract void SyncListElement(ref TList list, ISynchronizationFactory<ISynchronizable> factory);

        private TList CreateListWithNullElement()
        {
            return CreateList(
                    new ISynchronizable[]
                    {
                        null,
                        new FloatSynchronizableTestObject(TestData.floatListB),
                        new FloatSynchronizableTestObject(TestData.floatListLarger),
                    }
                );
        }
    }

    [TestFixture]
    public class ObjectArrayTest : BaseObjectListTest<ISynchronizable[]>
    {
        protected override ISynchronizable[] CreateList(ISynchronizable[] elements)
        {
            return elements.Clone() as ISynchronizable[];
        }

        protected override ISynchronizable GetListValue(int index)
        {
            return List[index];
        }

        protected override void SyncList(string id, ISynchronizable[] list)
        {
            state.SyncObjectArray(id, list);
        }

        protected override void SyncList(
                string id,
                ref ISynchronizable[] list,
                ISynchronizationFactory<ISynchronizable> factory
            )
        {
            state.SyncObjectArray(id, ref list, factory);
        }

        protected override void SyncListElement(ISynchronizable[] list)
        {
            state.SyncObjectArrayElement(list);
        }

        protected override void SyncListElement(
                ref ISynchronizable[] list,
                ISynchronizationFactory<ISynchronizable> factory
            )
        {
            state.SyncObjectArrayElement(ref list, factory);
        }
    }

    [TestFixture]
    public class ObjectListTest : BaseObjectListTest<List<ISynchronizable>>
    {
        protected override List<ISynchronizable> CreateList(ISynchronizable[] elements)
        {
            return new List<ISynchronizable>(elements);
        }

        protected override ISynchronizable GetListValue(int index)
        {
            return List[index];
        }

        protected override void SyncList(string id, List<ISynchronizable> list)
        {
            state.SyncObjectList(id, list);
        }

        protected override void SyncList(
                string id,
                ref List<ISynchronizable> list,
                ISynchronizationFactory<ISynchronizable> factory
            )
        {
            state.SyncObjectList(id, ref list, factory);
        }

        protected override void SyncListElement(List<ISynchronizable> list)
        {
            state.SyncObjectListElement(list);
        }

        protected override void SyncListElement(
                ref List<ISynchronizable> list,
                ISynchronizationFactory<ISynchronizable> factory
            )
        {
            state.SyncObjectListElement(ref list, factory);
        }
    }
}