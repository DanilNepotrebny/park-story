﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Synchronization.Nodes;
using DreamTeam.Synchronization.States;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Serialization
{
    public class BaseSerializationStateTest : BaseStateTestFixture
    {
        protected SerializationState state;

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            state = new SerializationState();
        }

        protected virtual DictionaryNode GetRoot()
        {
            return state.Root;
        }
    }
}