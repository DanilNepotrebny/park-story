﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization.Nodes;
using DreamTeam.Synchronization.States;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Serialization
{
    [TestFixture(typeof(int), typeof(int[]), typeof(IntArrayTestHelper))]
    [TestFixture(typeof(float), typeof(float[]), typeof(FloatArrayTestHelper))]
    [TestFixture(typeof(string), typeof(string[]), typeof(StringArrayTestHelper))]
    [TestFixture(typeof(int), typeof(List<int>), typeof(IntListTestHelper))]
    [TestFixture(typeof(float), typeof(List<float>), typeof(FloatListTestHelper))]
    [TestFixture(typeof(string), typeof(List<string>), typeof(StringListTestHelper))]
    public class SerializationListTest<T, TList, TListHelper> : BaseListOperationsTest<T, TList, TListHelper>
        where TList : class, IEnumerable<T>
        where TListHelper : IListTestHelper<TList>, new()
    {
        protected SerializationState state;
        protected DictionaryNode root;

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            state = new SerializationState();
            root = state.Root;
        }

        [Test]
        public void ValidListSync(
                [Values(true, false)] bool isResizeAllowed
            )
        {
            Helper.SyncList(state, TestData.keyA, ref listA, isResizeAllowed);

            var listNode = root.GetList(TestData.keyA);

            T[] actualListValues = new T[listNode.Count];
            for (int i = 0; i < listNode.Count; ++i)
            {
                actualListValues[i] = listNode.GetValue<T>(i);
            }

            Assert.IsTrue(
                    listA.SequenceEqual(actualListValues),
                    $"expected:[{string.Join(", ", listA)}] " +
                    $"actual:[{string.Join(", ", actualListValues)}]"
                );
        }

        [Test]
        public void ValidListSyncNulled(
                [Values(true, false)] bool isResizeAllowed
            )
        {
            listA = null;
            Helper.SyncList(state, TestData.keyA, ref listA, isResizeAllowed);

            Assert.IsTrue(root.IsNull(TestData.keyA));
        }

        [Test]
        public void InvalidListSyncOnList(
                [Values(true, false)] bool isResizeAllowed
            )
        {
            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(
                () => Helper.SyncList(state, TestData.keyA, ref listB, isResizeAllowed));
        }

        [Test]
        public void ValidListElementSync(
                [Values(true, false)] bool isResizeAllowed
            )
        {
            state.BeginList(TestData.keyA);
            Helper.SyncListElement(state, ref listA, isResizeAllowed);
            state.EndList();

            var listNode = root.GetList(TestData.keyA).GetList(0);

            T[] actualListValues = new T[listNode.Count];
            for (int i = 0; i < listNode.Count; ++i)
            {
                actualListValues[i] = listNode.GetValue<T>(i);
            }

            Assert.IsTrue(
                    listA.SequenceEqual(actualListValues),
                    $"expected:[{string.Join(", ", listA)}] " +
                    $"actual:[{string.Join(", ", actualListValues)}]"
                );
        }

        [Test]
        public void ValidListElementSyncNulled(
                [Values(true, false)] bool isResizeAllowed
            )
        {
            listA = null;
            state.BeginList(TestData.keyA);
            Helper.SyncListElement(state, ref listA, isResizeAllowed);
            state.EndList();

            Assert.IsTrue(root.GetList(TestData.keyA).IsNull(0));
        }

        [Test]
        public void InvalidListElementSyncOnDictionary(
                [Values(true, false)] bool isResizeAllowed
            )
        {
            state.BeginDictionary(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => Helper.SyncListElement(state, ref listB, isResizeAllowed));
        }
    }
}