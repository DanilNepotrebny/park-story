﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Linq;
using DreamTeam.Synchronization;
using NUnit.Framework;

namespace DreamTeam.Tests.Editor.Synchronization.Serialization
{
    [TestFixture]
    public class SimpleOperationsTest : BaseSerializationStateTest
    {
        [Test]
        public void SyncIntElementOnRoot()
        {
            Assert.Throws<InvalidOperationException>(() => state.SyncIntElement(ref intA));
        }

        [Test]
        public void SyncFloatElementOnRoot()
        {
            Assert.Throws<InvalidOperationException>(() => state.SyncFloatElement(ref floatA));
        }

        [Test]
        public void SyncStringElementOnRoot()
        {
            Assert.Throws<InvalidOperationException>(() => state.SyncStringElement(ref stringA));
        }

        [Test]
        public void EndDictionaryOnRoot()
        {
            Assert.Throws<InvalidOperationException>(() => state.EndDictionary());
        }

        [Test]
        public void EndListOnRoot()
        {
            Assert.Throws<InvalidOperationException>(() => state.EndList());
        }

        [Test]
        public void SyncInt()
        {
            state.SyncInt(TestData.keyA, ref intA);
            Assert.AreEqual(intA, GetRoot().GetInt(TestData.keyA));
        }

        [Test]
        public void SyncIntWithDefault()
        {
            state.SyncInt(TestData.keyA, ref intA, intB);
            Assert.AreEqual(intA, GetRoot().GetInt(TestData.keyA, intB));
        }

        [Test]
        public void SyncIntWithDefaultCheckOptimization()
        {
            state.SyncInt(TestData.keyA, ref intA, intB);
            Assert.IsTrue(GetRoot().Contains(TestData.keyA));
        }

        [Test]
        public void SyncIntSameWithDefaultCheckOptimization()
        {
            state.SyncInt(TestData.keyA, ref intB, intB);
            Assert.IsFalse(GetRoot().Contains(TestData.keyA));
        }

        [Test]
        public void SyncFloat()
        {
            state.SyncFloat(TestData.keyA, ref floatA);
            Assert.AreEqual(floatA, GetRoot().GetFloat(TestData.keyA));
        }

        [Test]
        public void SyncFloatWithDefault()
        {
            state.SyncFloat(TestData.keyA, ref floatA, floatB);
            Assert.AreEqual(floatA, GetRoot().GetFloat(TestData.keyA, floatB));
        }

        [Test]
        public void SyncFloatWithDefaultCheckOptimization()
        {
            state.SyncFloat(TestData.keyA, ref floatA, floatB);
            Assert.IsTrue(GetRoot().Contains(TestData.keyA));
        }

        [Test]
        public void SyncFloatSameWithDefaultCheckOptimization()
        {
            state.SyncFloat(TestData.keyA, ref floatB, floatB);
            Assert.IsFalse(GetRoot().Contains(TestData.keyA));
        }

        [Test]
        public void SyncString()
        {
            state.SyncString(TestData.keyA, ref stringA);
            Assert.AreEqual(stringA, GetRoot().GetString(TestData.keyA));
        }

        [Test]
        public void SyncStringWithDefaultCheckValue()
        {
            state.SyncString(TestData.keyA, ref stringA, stringB);
            Assert.AreEqual(stringA, GetRoot().GetString(TestData.keyA, stringB));
        }

        [Test]
        public void SyncStringWithDefaultCheckOptimization()
        {
            state.SyncString(TestData.keyA, ref stringA, stringB);
            Assert.IsTrue(GetRoot().Contains(TestData.keyA));
        }

        [Test]
        public void SyncStringSameWithDefaultCheckOptimization()
        {
            state.SyncString(TestData.keyA, ref stringB, stringB);
            Assert.IsFalse(GetRoot().Contains(TestData.keyA));
        }

        [Test]
        public void BeginDictionary()
        {
            state.BeginDictionary(TestData.keyA);

            Assert.IsNotNull(GetRoot().GetDictionary(TestData.keyA));
        }

        [Test]
        public void BeginDictionarySyncInt()
        {
            state.BeginDictionary(TestData.keyA);
            state.SyncInt(TestData.keyB, ref intA);

            Assert.AreEqual(intA, GetRoot().GetDictionary(TestData.keyA).GetInt(TestData.keyB));
        }

        [Test]
        public void BeginDictionarySyncFloat()
        {
            state.BeginDictionary(TestData.keyA);
            state.SyncFloat(TestData.keyB, ref floatA);

            Assert.AreEqual(floatA, GetRoot().GetDictionary(TestData.keyA).GetFloat(TestData.keyB));
        }

        [Test]
        public void BeginDictionarySyncString()
        {
            state.BeginDictionary(TestData.keyA);
            state.SyncString(TestData.keyB, ref stringA);

            Assert.AreEqual(stringA, GetRoot().GetDictionary(TestData.keyA).GetString(TestData.keyB));
        }

        [Test]
        public void EndDictionary()
        {
            state.BeginDictionary(TestData.keyA);
            state.EndDictionary();
            state.SyncString(TestData.keyB, ref stringA);

            Assert.AreEqual(stringA, GetRoot().GetString(TestData.keyB));
        }

        [Test]
        public void SyncSameKeyOnDifferentHierarchyLevels_Dictionary()
        {
            state.BeginDictionary(TestData.keyB);
            state.SyncInt(TestData.keyA, ref intA);
            state.EndDictionary();
            state.SyncString(TestData.keyA, ref stringA);

            Assert.AreEqual(stringA, GetRoot().GetString(TestData.keyA));
            Assert.AreEqual(intA, GetRoot().GetDictionary(TestData.keyB).GetInt(TestData.keyA));
        }

        [Test]
        public void BeginList()
        {
            state.BeginList(TestData.keyA);

            Assert.IsNotNull(GetRoot().GetList(TestData.keyA));
        }

        [Test]
        public void BeginListSyncInt()
        {
            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => state.SyncInt(TestData.keyA, ref intA));
        }

        [Test]
        public void BeginListSyncFloat()
        {
            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => state.SyncFloat(TestData.keyA, ref floatA));
        }

        [Test]
        public void BeginListSyncString()
        {
            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => state.SyncString(TestData.keyA, ref stringA));
        }

        [Test]
        public void BeginListBeginDictionary()
        {
            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => state.BeginDictionary(TestData.keyA));
        }

        [Test]
        public void BeginListBeginList()
        {
            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => state.BeginList(TestData.keyA));
        }

        [Test]
        public void BeginListSyncIntElement()
        {
            state.BeginList(TestData.keyA);
            state.SyncIntElement(ref intA);

            Assert.AreEqual(intA, GetRoot().GetList(TestData.keyA).GetInt(0));
        }

        [Test]
        public void BeginListSyncFloatElement()
        {
            state.BeginList(TestData.keyA);
            state.SyncFloatElement(ref floatA);

            Assert.AreEqual(floatA, GetRoot().GetList(TestData.keyA).GetFloat(0));
        }

        [Test]
        public void BeginListSyncStringElement()
        {
            state.BeginList(TestData.keyA);
            state.SyncStringElement(ref stringA);

            Assert.AreEqual(stringA, GetRoot().GetList(TestData.keyA).GetString(0));
        }

        [Test]
        public void BeginListBeginDictionaryElement()
        {
            state.BeginList(TestData.keyA);
            state.BeginDictionaryElement();
            Assert.IsNotNull(GetRoot().GetList(TestData.keyA).GetDictionary(0));
        }

        [Test]
        public void BeginListBeginListElement()
        {
            state.BeginList(TestData.keyA);
            state.BeginListElement();
            Assert.IsNotNull(GetRoot().GetList(TestData.keyA).GetList(0));
        }

        [Test]
        public void EndList()
        {
            state.BeginList(TestData.keyA);
            state.EndList();
            state.SyncString(TestData.keyB, ref stringA);

            Assert.AreEqual(stringA, GetRoot().GetString(TestData.keyB));
        }

        [Test]
        public void SyncSameKeyOnDifferentHierarchyLevels_List()
        {
            state.BeginList(TestData.keyB);
            state.SyncIntElement(ref intA);
            state.EndList();
            state.SyncString(TestData.keyA, ref stringA);

            Assert.AreEqual(stringA, GetRoot().GetString(TestData.keyA));
            Assert.AreEqual(intA, GetRoot().GetList(TestData.keyB).GetInt(0));
        }

        [Test]
        public void ValidSyncObject()
        {
            var obj = new FloatSynchronizableTestObject(TestData.floatListA);
            state.SyncObject(TestData.keyA, obj);

            var listNode = GetRoot().GetDictionary(TestData.keyA).GetList(FloatSynchronizableTestObject.valuesId);

            float[] actualListValues = new float[listNode.Count];
            for (int i = 0; i < listNode.Count; ++i)
            {
                actualListValues[i] = listNode.GetFloat(i);
            }

            Assert.IsTrue(TestData.floatListA.SequenceEqual(actualListValues));
        }

        [Test]
        public void ValidSyncObjectNull()
        {
            FloatSynchronizableTestObject obj = null;

            Assert.Throws<InvalidOperationException>(() => state.SyncObject(TestData.keyA, obj));
        }

        [Test]
        public void ValidSyncObjectWithFactory()
        {
            ISynchronizationFactory<FloatSynchronizableTestObject> factory =
                FactorySubstitute.Create<FloatSynchronizableTestObject>();
            var obj = new FloatSynchronizableTestObject(TestData.floatListA);
            state.SyncObject(TestData.keyA, ref obj, factory);

            var listNode = GetRoot()
                .GetDictionary(TestData.keyA)
                .GetDictionary("Object")
                .GetList(FloatSynchronizableTestObject.valuesId);

            float[] actualListValues = new float[listNode.Count];
            for (int i = 0; i < listNode.Count; ++i)
            {
                actualListValues[i] = listNode.GetFloat(i);
            }

            Assert.IsTrue(TestData.floatListA.SequenceEqual(actualListValues));
        }

        [Test]
        public void ValidSyncObjectFactorySync()
        {
            ISynchronizationFactory<FloatSynchronizableTestObject> factory =
                FactorySubstitute.Create<FloatSynchronizableTestObject>();
            var obj = new FloatSynchronizableTestObject(TestData.floatListA);
            state.SyncObject(TestData.keyA, ref obj, factory);

            Assert.IsNotNull(GetRoot().GetDictionary(TestData.keyA).GetNode("Factory"));
        }

        [Test]
        public void ValidSyncObjectElement()
        {
            var obj = new FloatSynchronizableTestObject(TestData.floatListA);
            state.BeginList(TestData.keyA);
            state.SyncObjectElement(obj);
            state.EndList();

            var listNode = GetRoot()
                .GetList(TestData.keyA)
                .GetDictionary(0)
                .GetList(FloatSynchronizableTestObject.valuesId);

            float[] actualListValues = new float[listNode.Count];
            for (int i = 0; i < listNode.Count; ++i)
            {
                actualListValues[i] = listNode.GetFloat(i);
            }

            Assert.IsTrue(TestData.floatListA.SequenceEqual(actualListValues));
        }

        [Test]
        public void ValidSyncObjectElementNull()
        {
            FloatSynchronizableTestObject obj = null;
            state.BeginList(TestData.keyA);
            Assert.Throws<InvalidOperationException>(() => state.SyncObjectElement(obj));
        }

        [Test]
        public void ValidSyncObjectElementWithFactory()
        {
            ISynchronizationFactory<FloatSynchronizableTestObject> factory =
                FactorySubstitute.Create<FloatSynchronizableTestObject>();
            var obj = new FloatSynchronizableTestObject(TestData.floatListA);
            state.BeginList(TestData.keyA);
            state.SyncObjectElement(ref obj, factory);
            state.EndList();

            var listNode = GetRoot()
                .GetList(TestData.keyA)
                .GetDictionary(0)
                .GetDictionary("Object")
                .GetList(FloatSynchronizableTestObject.valuesId);

            float[] actualListValues = new float[listNode.Count];
            for (int i = 0; i < listNode.Count; ++i)
            {
                actualListValues[i] = listNode.GetFloat(i);
            }

            Assert.IsTrue(TestData.floatListA.SequenceEqual(actualListValues));
        }
    }
}