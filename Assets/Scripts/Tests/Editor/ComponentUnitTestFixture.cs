﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using NSubstitute;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using Zenject;

namespace DreamTeam.Tests.Editor
{
    /// <summary>
    /// Unit test fixture for unit testing MonoBehaviours using Zenject
    /// </summary>
    public abstract class ComponentUnitTestFixture : ZenjectUnitTestFixture
    {
        private HashSet<GameObject> _gameObjects;

        /// <summary>
        /// Initializes each test
        /// </summary>
        [SetUp]
        public void InitializeTest()
        {
            _gameObjects = new HashSet<GameObject>();
        }

        /// <summary>
        /// Creates new GameObject with new component and emulates Unity initialization events on it.
        /// </summary>
        protected T CreateComponent<T>(Dictionary<string, object> properties = null) where T : MonoBehaviour
        {
            return CreateComponent<T>(string.Empty, properties);
        }

        /// <summary>
        /// Creates and emulates Unity initialization events for new GameObject with component on it.
        /// </summary>
        protected T CreateComponent<T>(string name, Dictionary<string, object> properties = null)
            where T : MonoBehaviour
        {
            GameObject gameObject = new GameObject(name);
            _gameObjects.Add(gameObject);
            return CreateComponent<T>(gameObject, properties);
        }

        /// <summary>
        /// Creates new component on the existing GameObject and emulates Unity initialization events.
        /// </summary>
        protected T CreateComponent<T>(GameObject gameObject, Dictionary<string, object> properties = null)
            where T : MonoBehaviour
        {
            _gameObjects.Add(gameObject);

            T component = gameObject.AddComponent<T>();

            SetSerializedFields(component, properties);
            Container.Inject(component);
            CallComponentsEvent("Awake", gameObject);
            CallComponentsEvent("OnEnable", gameObject);

            return component;
        }

        /// <summary>
        /// Creates new ScriptableObject and emulates Unity initialization events for it.
        /// </summary>
        protected T CreateScriptableObject<T>(Dictionary<string, object> properties = null) where T : ScriptableObject
        {
            T scriptableObject = ScriptableObject.CreateInstance<T>();

            SetSerializedFields(scriptableObject, properties);
            Container.Inject(scriptableObject);
            ReflectionUtils.Invoke(scriptableObject, "Awake", true);
            ReflectionUtils.Invoke(scriptableObject, "OnEnable", true);

            return scriptableObject;
        }

        protected T BindSubstitute<T>(params object[] parameters) where T : class
        {
            T result = Substitute.For<T>(parameters);
            Container.BindInstance(result);
            return result;
        }

        /// <summary>
        /// Calls Unity event on all components of specified game objects.
        /// </summary>
        protected void CallComponentsEvent(string eventName, params GameObject[] gameObjects)
        {
            SortedSet<MonoBehaviour> components = new SortedSet<MonoBehaviour>(new SortingOrderComparer());

            foreach (GameObject go in gameObjects)
            {
                components.UnionWith(go.GetComponents<MonoBehaviour>());
            }

            foreach (MonoBehaviour component in components)
            {
                ReflectionUtils.Invoke(component, eventName, true);
            }
        }

        /// <summary>
        /// Calls Unity event on all created Components in the test.
        /// </summary>
        protected void CallUnityEventForAll(string eventName)
        {
            GameObject[] gameObjects = new GameObject[_gameObjects.Count];
            _gameObjects.CopyTo(gameObjects);
            CallComponentsEvent(eventName, gameObjects);
        }

        /// <summary>
        /// Calls 'Start' Unity event on all created Components in the test.
        /// </summary>
        protected void StartTest()
        {
            CallUnityEventForAll("Start");
        }

        private void SetSerializedFields(Object obj, Dictionary<string, object> properties)
        {
            if (properties != null)
            {
                foreach (var pair in properties)
                {
                    obj.SetSerializedField(pair.Key, pair.Value);
                }
            }
        }

        #region Inner classes

        private class SortingOrderComparer : IComparer<MonoBehaviour>
        {
            public int Compare(MonoBehaviour component1, MonoBehaviour component2)
            {
                return GetExecutionOrder(component1).CompareTo(GetExecutionOrder(component2));
            }

            private int GetExecutionOrder(MonoBehaviour component)
            {
                MonoScript monoScript = MonoScript.FromMonoBehaviour(component);
                return MonoImporter.GetExecutionOrder(monoScript);
            }
        }

        #endregion
    }
}