﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using NUnit.Framework;
using UnityEngine;

namespace DreamTeam.Tests.Editor.Utils
{
    [TestFixture]
    public class RectIntExtensionTest
    {
        [Test]
        public void NotIntersectingFarByPositiveX()
        {
            RectInt r1 = new RectInt(0, 0, 5, 5);
            RectInt r2 = new RectInt(10, 0, 5, 5);
            Assert.IsFalse(r1.Intersects(r2));
        }

        [Test]
        public void NotIntersectingFarBySemiNegativeX()
        {
            RectInt r1 = new RectInt(2, 0, 3, 4);
            RectInt r2 = new RectInt(-10, 0, 5, 3);
            Assert.IsFalse(r1.Intersects(r2));
        }

        [Test]
        public void NotIntersectingFarByNegativeX()
        {
            RectInt r1 = new RectInt(-2, 0, 3, 4);
            RectInt r2 = new RectInt(-10, 0, 5, 3);
            Assert.IsFalse(r1.Intersects(r2));
        }

        [Test]
        public void NotIntersectingFarByPositiveY()
        {
            RectInt r1 = new RectInt(0, 2, 6, 5);
            RectInt r2 = new RectInt(0, 10, 7, 5);
            Assert.IsFalse(r1.Intersects(r2));
        }

        [Test]
        public void NotIntersectingFarByNegativeY()
        {
            RectInt r1 = new RectInt(0, -10, 6, 5);
            RectInt r2 = new RectInt(0, -2, 7, 5);
            Assert.IsFalse(r1.Intersects(r2));
        }

        [Test]
        public void NotIntersectingFarBySemiNegativeY()
        {
            RectInt r1 = new RectInt(0, -10, 6, 5);
            RectInt r2 = new RectInt(0, 2, 7, 5);
            Assert.IsFalse(r1.Intersects(r2));
        }

        [Test]
        public void NonIntersectingCloseHorizontallyPositive()
        {
            RectInt r1 = new RectInt(0, 2, 3, 2);
            RectInt r2 = new RectInt(3, 2, 5, 5);
            Assert.IsFalse(r1.Intersects(r2));
        }

        [Test]
        public void NonIntersectingCloseHorizontallyNegative()
        {
            RectInt r1 = new RectInt(-10, 2, 3, 2);
            RectInt r2 = new RectInt(-15, 2, 5, 5);
            Assert.IsFalse(r1.Intersects(r2));
        }

        [Test]
        public void NotIntersectingCloseVerticallyPositive()
        {
            RectInt r1 = new RectInt(10, 10, 3, 2);
            RectInt r2 = new RectInt(10, 12, 5, 5);
            Assert.IsFalse(r1.Intersects(r2));
        }

        [Test]
        public void NonIntersectingCloseVerticallyNegative()
        {
            RectInt r1 = new RectInt(10, -10, 3, 2);
            RectInt r2 = new RectInt(10, -5, 5, 5);
            Assert.IsFalse(r1.Intersects(r2));
        }

        [Test]
        public void NotIntersectingSameZero()
        {
            RectInt r1 = new RectInt(20, -10, 0, 0);
            Assert.IsFalse(r1.Intersects(r1));
        }

        [Test]
        public void IntersectingSame()
        {
            RectInt r1 = new RectInt(20, -10, 3, 2);
            Assert.IsTrue(r1.Intersects(r1));
        }

        [Test]
        public void IntersectingByPositiveEdgeX()
        {
            RectInt r1 = new RectInt(10, 2, 3, 2);
            RectInt r2 = new RectInt(12, 2, 5, 5);
            Assert.IsTrue(r1.Intersects(r2));
        }

        [Test]
        public void IntersectingByNegativeEdgeX()
        {
            RectInt r1 = new RectInt(-10, 2, 3, 2);
            RectInt r2 = new RectInt(-14, 2, 5, 5);
            Assert.IsTrue(r1.Intersects(r2));
        }

        [Test]
        public void IntersectingByPositiveEdgeY()
        {
            RectInt r1 = new RectInt(10, 2, 3, 2);
            RectInt r2 = new RectInt(10, 3, 5, 5);
            Assert.IsTrue(r1.Intersects(r2));
        }

        [Test]
        public void IntersectingByNegativeEdgeY()
        {
            RectInt r1 = new RectInt(10, -2, 3, 2);
            RectInt r2 = new RectInt(10, -6, 5, 5);
            Assert.IsTrue(r1.Intersects(r2));
        }

        [Test]
        public void IntersectingInside()
        {
            RectInt r1 = new RectInt(10, 10, 20, 30);
            RectInt r2 = new RectInt(15, 15, 5, 10);
            Assert.IsTrue(r1.Intersects(r2));
        }
    }
}