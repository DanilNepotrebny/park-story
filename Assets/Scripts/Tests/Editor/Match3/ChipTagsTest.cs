﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3.ChipTags;
using NUnit.Framework;
using UnityEngine;

namespace DreamTeam.Tests.Editor.Match3
{
    public class ChipTagsTest
    {
        private ChipTagSet _tags;
        private FooChild1 _foo;
        private Bar _bar;

        [SetUp]
        public void Init()
        {
            _foo = CreateTagInstance<FooChild1>();
            _bar = CreateTagInstance<Bar>();

            _tags = new ChipTagSet();
            _tags.Add(_foo);
            _tags.Add(_bar);
        }

        [Test]
        public void TestSearchByClass()
        {
            Assert.AreEqual(_foo, _tags.Get<FooChild1>());
            Assert.AreEqual(_bar, _tags.Get<Bar>());
            Assert.Null(_tags.Get<FooChild2>());
        }

        [Test]
        public void TestSearchByBaseClass()
        {
            Assert.AreEqual(_foo, _tags.Get<Foo>());
        }

        [Test]
        public void TestSearchByInstance()
        {
            Assert.True(_tags.Has(_foo));
            Assert.True(_tags.Has(_bar));

            FooChild2 foo = CreateTagInstance<FooChild2>();
            Assert.False(_tags.Has(foo));
        }

        [Test]
        public void TestAdditionSameClass()
        {
            FooChild1 foo = CreateTagInstance<FooChild1>();
            _tags.Add(foo);
            Assert.True(_tags.Has(foo));
        }

        [Test]
        public void TestAdditionDerived()
        {
            FooChild1 foo = CreateTagInstance<FooChild2>();
            _tags.Add(foo);
            Assert.True(_tags.Has(foo));
        }

        [Test]
        public void TestAdditionParent()
        {
            _tags.Add(CreateTagInstance<FooChild2>());
            _tags.Add(CreateTagInstance<FooChild1>());
            Assert.True(_tags.Get<FooChild2>() != null);
            Assert.True(_tags.Get<FooChild1>() != null);
            Assert.True(_tags.Get<Foo>() != null);
        }

        [Test]
        public void TestAdditionAmbiguous()
        {
            _tags.Add(CreateTagInstance<FooChild2>());
            Assert.Throws<ChipTagSet.AmbiguousTagException>(() => _tags.Add(CreateTagInstance<FooChild3>()));
        }

        [Test]
        public void TestRangeAdditionSameClass()
        {
            ChipTagSet tags = new ChipTagSet();
            FooChild1 foo = CreateTagInstance<FooChild1>();
            tags.Add(foo);

            _tags.AddRange(tags);
            Assert.True(_tags.Has(foo));
        }

        [Test]
        public void TestRangeAdditionDerived()
        {
            ChipTagSet tags = new ChipTagSet();
            FooChild1 foo = CreateTagInstance<FooChild2>();
            tags.Add(foo);

            _tags.AddRange(tags);
            Assert.True(_tags.Has(foo));
        }

        [Test]
        public void TestRangeAdditionParent()
        {
            _tags.Add(CreateTagInstance<FooChild2>());

            ChipTagSet tags = new ChipTagSet();
            tags.Add(CreateTagInstance<FooChild1>());

            _tags.AddRange(tags);
            Assert.True(_tags.Get<FooChild2>() != null);
            Assert.True(_tags.Get<FooChild1>() != null);
            Assert.True(_tags.Get<Foo>() != null);
        }

        [Test]
        public void TestRangeAdditionAmbiguous()
        {
            _tags.Add(CreateTagInstance<FooChild2>());

            ChipTagSet tags = new ChipTagSet();
            tags.Add(CreateTagInstance<FooChild3>());

            Assert.Throws<ChipTagSet.AmbiguousTagException>(() => _tags.AddRange(tags));
        }

        [Test]
        public void TestClear()
        {
            _tags.Clear();
            Assert.False(_tags.Has(_foo));
            Assert.False(_tags.Has(_bar));
        }

        [Test]
        public void TestNonAbstractRestriction()
        {
            ChipTagSet tags = new ChipTagSet(false);

            Assert.Throws<ArgumentException>(() => tags.Add(_foo));
            Assert.Throws<ArgumentException>(() => tags.AddRange(_tags));
        }

        [Test]
        public void TestFindFirstOf()
        {
            ChipTagSet tags = new ChipTagSet();
            Assert.Null(_tags.FindFirstOf(tags));
            Assert.Null(tags.FindFirstOf(_tags));

            FooChild2 fooChild2 = CreateTagInstance<FooChild2>();
            tags.Add(fooChild2);
            Assert.Null(_tags.FindFirstOf(tags));
            Assert.AreEqual(fooChild2, tags.FindFirstOf(_tags));

            tags.Clear();
            tags.Add(_bar);
            Assert.AreEqual(_bar, _tags.FindFirstOf(tags));
            Assert.AreEqual(_bar, tags.FindFirstOf(_tags));
        }

        private T CreateTagInstance<T>() where T : ChipTag
        {
            return ScriptableObject.CreateInstance<T>();
        }

        #region Inner classes

        private abstract class Foo : ChipTag
        {
        }

        private class FooChild1 : Foo
        {
        }

        private class FooChild2 : FooChild1
        {
        }

        private class FooChild3 : Foo
        {
        }

        private class Bar : ChipTag
        {
            public override bool IsAbstract => false;
        }

        #endregion
    }
}