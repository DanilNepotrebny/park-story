﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.Match3;
using NUnit.Framework;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using Object = UnityEngine.Object;

namespace DreamTeam.Tests
{
    public class ExplosiveComponentTest
    {
        [UnityTest]
        public IEnumerator TestExplosion(
                [Files("Assets/Scenes/Tests/ExplosiveComponent", "*.unity")]
                string scenePath
            )
        {
            SceneManager.LoadScene(scenePath);
            yield return null;

            TestComponent[] testComponents = Object.FindObjectsOfType<TestComponent>();
            ChipComponent explosive = Array.Find(testComponents, t => t.Id == 1)
                .GetComponent<CellComponent>()
                .Chip;

            explosive.ScheduleActivation().Perform();

            foreach (CellComponent cell in Object.FindObjectOfType<GridSystem>().Cells)
            {
                Assert.AreEqual(
                    cell.GetComponent<TestComponent>() != null,
                    !cell.IsStable,
                    $"Incorrect chip behaviour at {cell}");
            }
        }
    }
}