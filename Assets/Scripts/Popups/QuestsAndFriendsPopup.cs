﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;
using DreamTeam.Utils.Invocation;

namespace DreamTeam.Popups
{
    public class QuestsAndFriendsPopup : Popup
    {
        private Args _args;

        public IDeferredInvocationHandle GivingRewardInvocation => _args?.GivingRewardInvocation;
        public QuestPackReward RewardData => _args?.RewardData;

        public override bool Initialize(Popup.Args popupArgs)
        {
            base.Initialize(popupArgs);
            _args = popupArgs as Args;
            return true;
        }

        public new class Args : Popup.Args
        {
            public IDeferredInvocationHandle GivingRewardInvocation { get; set; }
            public QuestPackReward RewardData { get; set; }
        }
    }
}