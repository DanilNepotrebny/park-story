﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Zenject;

namespace DreamTeam.Popups.InputString
{
    public class InputUserNamePopupController : InputStringPopupController
    {
        [Inject] private UserSettings _userSettings;

        public override string DefaultInputValue => _userSettings.UserName;

        protected override void OnOnValueAccepted(string value)
        {
            _userSettings.UserName = value;
        }
    }
}