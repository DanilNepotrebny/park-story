﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Popups.InputString
{
    [RequireComponent(typeof(Button))]
    public abstract class InputStringPopupController : MonoBehaviour
    {
        [SerializeField] private SceneReference _popupScene;
        [SerializeField] private InputStringPopup.Args _popupArgs;

        [Inject] private PopupSystem _popupSystem;

        public abstract string DefaultInputValue { get; }

        protected void OnEnable()
        {
            GetComponent<Button>().onClick.AddListener(OnButonClicked);
        }

        protected void OnDisable()
        {
            GetComponent<Button>().onClick.RemoveListener(OnButonClicked);
        }

        protected abstract void OnOnValueAccepted(string value);

        private void OnButonClicked()
        {
            _popupArgs.ValueAcceptedCallback = OnOnValueAccepted;
            _popupArgs.DefaultValue = DefaultInputValue;
            _popupSystem.Show(_popupScene.Name, _popupArgs);
        }
    }
}