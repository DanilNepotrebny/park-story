﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Popups
{
    public class ClosePopupWithButtonPressed : MonoBehaviour
    {
        [Inject] private PopupSystem _popupSystem;

        [SerializeField] private Button _closeButton;

        public event Action CloseButtonClicked;

        /// <summary>
        /// Unity callback
        /// </summary>
        protected void Awake()
        {
            if (_closeButton != null)
            {
                _closeButton.onClick.AddListener(OnCloseButtonClicked);
            }
        }

        private void OnCloseButtonClicked()
        {
            Popup popup = _popupSystem.GetPopup(gameObject.scene.name);
            if (popup != null && popup.IsShown)
            {
                CloseButtonClicked?.Invoke();
                _popupSystem.Hide(gameObject.scene.name);
            }
        }
    }
}