﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using DreamTeam.Utils.UI;
using UnityEngine;

namespace DreamTeam.Popups
{
    public class ConfirmStringPopup : InputStringPopup
    {
        [SerializeField, RequiredField] private InputFieldRegexSelectableSwitcher _regexSelectableSwitcher;

        public override bool Initialize(Popup.Args popupArgs)
        {
            if (base.Initialize(popupArgs))
            {
                var args = popupArgs as Args;
                if (args != null)
                {
                    _regexSelectableSwitcher.SetMatchPattern(args.ConfirmPattern);
                    return true;
                }
            }

            return false;
        }

        [Serializable]
        public new class Args : InputStringPopup.Args
        {
            [SerializeField] private string _confirmPattern;
            public string ConfirmPattern => _confirmPattern;
        }
    }
}