﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Popups
{
    public class PopupInstaller : MonoInstaller<PopupInstaller>
    {
        [SerializeField, RequiredField] private Popup _popup;

        public override void InstallBindings()
        {
            Container.Bind<Popup>().FromInstance(_popup).AsSingle();
        }

        protected void Reset()
        {
            _popup = GetComponentInParent<Popup>();
        }
    }
}