﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.EndlessGrind;
using DreamTeam.UI;
using UnityEngine;

namespace DreamTeam.Popups
{
    [RequireComponent(typeof(PresenterHub))]
    public class SingleEndlessGrindChestPopup : Popup
    {
        [Space] [SerializeField] private GameObject[] _chestAvailablePresenters;
        [SerializeField] private GameObject[] _chestNotAvailablePresenters;

        public override bool Initialize(Popup.Args popupArgs)
        {
            if (base.Initialize(popupArgs))
            {
                var args = popupArgs as Args;
                if (args != null)
                {
                    ApplyArgs(args);
                }

                return args != null;
            }

            return false;
        }

        public new class Args : Popup.Args
        {
            public EndlessGrindChest Chest { get; set; }
        }

        private void ApplyArgs(Args args)
        {
            SetChestAvailable(args.Chest != null);
            if (args.Chest != null)
            {
                GetComponent<PresenterHub>().Initialize(args.Chest);
            }
        }

        private void SetChestAvailable(bool isAvailable)
        {
            foreach (GameObject presenter in _chestAvailablePresenters)
            {
                presenter.SetActive(isAvailable);
            }

            foreach (GameObject presenter in _chestNotAvailablePresenters)
            {
                presenter.SetActive(!isAvailable);
            }
        }
    }
}