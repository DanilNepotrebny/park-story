﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Popups
{
    [RequireComponent(typeof(RandomSpawnerInRect))]
    public class LocationFinishedFireworksController : MonoBehaviour
    {
        [Inject] private Popup _popup;

        private RandomSpawnerInRect _spawner => GetComponent<RandomSpawnerInRect>();

        protected void Awake()
        {
            _popup.Shown += OnPopupShown;
            _popup.Hiding += OnPopupHiding;
        }

        protected void OnDestroy()
        {
            _popup.Shown -= OnPopupShown;
            _popup.Hiding -= OnPopupHiding;
        }

        protected void Reset()
        {
            _spawner.enabled = false;
        }

        private void OnPopupHiding(Popup popup)
        {
            _spawner.enabled = false;
        }

        private void OnPopupShown(Popup popup)
        {
            _spawner.enabled = true;
        }
    }
}