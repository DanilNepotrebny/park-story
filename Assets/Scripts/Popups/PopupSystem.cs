﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace DreamTeam.Popups
{
    public class PopupSystem : MonoBehaviour
    {
        [SerializeField, SortingLayer] private int _sortingLayerId;
        [SerializeField] private SceneReference _popupTintScene;

        private List<Tuple<Popup, Popup.Args>> _popupsStack = new List<Tuple<Popup, Popup.Args>>(3);
        private List<Tuple<Popup, Popup.Args>> _stashedPopups = new List<Tuple<Popup, Popup.Args>>(3);
        private readonly Dictionary<string, Popup> _loadedPopups = new Dictionary<string, Popup>(10);

        private readonly List<Tuple<string, Popup.Args, Action>> _loadingPopupScenes =
            new List<Tuple<string, Popup.Args, Action>>();

        private Popup _tintPopup;
        private int _shownPopupsCounter;

        public bool IsPopupLoading => _loadingPopupScenes.Count != 0;

        private int _shownPopupsCount
        {
            get { return _shownPopupsCounter; }
            set
            {
                if (_shownPopupsCounter != value)
                {
                    _shownPopupsCounter = value;

                    if (_tintPopup != null)
                    {
                        switch (_shownPopupsCounter)
                        {
                            case 0:
                                _tintPopup.Hide();
                                break;

                            case 1:
                                if (!_tintPopup.IsShown)
                                {
                                    ShowInternal(_tintPopup, null, null);
                                }
                                break;
                        }
                    }
                }
            }
        }

        public event Action PopupClosed;

        /// <summary>
        /// Get loaded popup. It is possible to get null popup after show if it was not loaded before.
        /// </summary>
        public Popup GetPopup(string popupSceneName)
        {
            Popup popup;
            return _loadedPopups.TryGetValue(popupSceneName, out popup) ? popup : null;
        }

        public Popup Show(string popupSceneName, Popup.Args popupArgs = null, Action callback = null)
        {
            Popup popup;
            if (!_loadedPopups.TryGetValue(popupSceneName, out popup))
            {
                LoadPopupScene(popupSceneName, popupArgs, callback);
                return null;
            }

            if (ShowInternal(popup, popupArgs, callback))
            {
                _popupsStack.Add(new Tuple<Popup, Popup.Args>(popup, popupArgs));
                ResetSortingOrder();
            }
            else
            {
                UnityEngine.Debug.LogError($"Popup {popupSceneName} was not initialized");
            }

            return popup;
        }

        public Popup Hide(string popupSceneName, Action callback = null)
        {
            Assert.IsTrue(_popupsStack.Count > 0);

            Popup popup;
            if (!_loadedPopups.TryGetValue(popupSceneName, out popup))
            {
                UnityEngine.Debug.LogWarning($"Didn't find popup scene '{popupSceneName}' to hide.");
                return null;
            }

            popup.Hide(callback);
            return popup;
        }

        public void HideAll(Action callback = null)
        {
            int childCallbacksCounter = _popupsStack.Count;

            if (childCallbacksCounter == 0)
            {
                callback?.Invoke();
                return;
            }

            Action hidingCallback = () =>
            {
                childCallbacksCounter -= 1;
                if (childCallbacksCounter == 0)
                {
                    callback?.Invoke();
                }
            };

            foreach (Tuple<Popup, Popup.Args> tuple in _popupsStack)
            {
                tuple.Item1.Hide(hidingCallback);
            }
        }

        public void StashAll(Action callback = null)
        {
            if (_stashedPopups.Count > 0)
            {
                throw new InvalidOperationException($"Already have stashed popups. Pls, unstash existing one before.");
            }

            _stashedPopups.AddRange(_popupsStack);
            HideAll(callback);
        }

        public void UnstashAll()
        {
            foreach (Tuple<Popup, Popup.Args> tuple in _stashedPopups)
            {
                Show(tuple.Item1.SceneName, tuple.Item2);
            }
            ClearStashedPopups();
        }

        public void ClearStashedPopups()
        {
            _stashedPopups.Clear();
        }

        public bool HasShowingPopup()
        {
            return _loadingPopupScenes.Count > 0 || _popupsStack.Count > 0;
        }

        private bool ShowInternal([NotNull] Popup popup, [CanBeNull] Popup.Args popupArgs, [CanBeNull] Action callback)
        {
            if (!popup.Initialize(popupArgs))
            {
                return false;
            }

            popup.Showing += OnPopupShowing;
            popup.Hiding += OnPopupHiding;
            popup.Hidden += OnPopupHidden;

            return popup.Show(callback);
        }

        private void OnPopupShowing(Popup popup)
        {
            if (popup != _tintPopup)
            {
                _shownPopupsCount++;
            }
        }

        private void OnPopupHiding(Popup popup)
        {
            if (popup != _tintPopup)
            {
                _shownPopupsCount--;
            }
        }

        private void OnPopupHidden(Popup popup)
        {
            popup.Showing -= OnPopupShowing;
            popup.Hiding -= OnPopupHiding;
            popup.Hidden -= OnPopupHidden;

            _popupsStack.RemoveAll(tuple => tuple.Item1 == popup);
            ResetSortingOrder();
            PopupClosed?.Invoke();
        }

        private void LoadPopupScene(string sceneName, [CanBeNull] Popup.Args popupArgs, [CanBeNull] Action callback)
        {
            if (_loadingPopupScenes.Find(a => a.Item1 == sceneName) != null)
            {
                return;
            }

            // temporal solution until preload buckets will be implemented
            if (_tintPopup == null)
            {
                AsyncOperation o = SceneManager.LoadSceneAsync(_popupTintScene.Name, LoadSceneMode.Additive);
                o.completed += (asyncOperation) =>
                {
                    Scene scene = SceneManager.GetSceneByName(_popupTintScene.Name);
                    _tintPopup = scene.GetRootGameObjects()[0].GetComponent<Popup>();
                    Assert.IsNotNull(_tintPopup);
                    _tintPopup.gameObject.SetActive(true); // set to true to perform Awake() callback
                    _tintPopup.SortingOptions.SortingLayerId = _sortingLayerId; // now we can set SortingLayer
                    _tintPopup.gameObject.SetActive(false);
                    _tintPopup.Destroying += OnPopupIsDestroying;
                };
            }

            Scene sceneToLoad = SceneManager.GetSceneByName(sceneName);
            Assert.IsFalse(sceneToLoad.isLoaded, $"Somehow scene {sceneName} already loaded");

            AsyncOperation op = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            op.completed += OnSceneLoadingCompleted;
            _loadingPopupScenes.Add(new Tuple<string, Popup.Args, Action>(sceneName, popupArgs, callback));
        }

        private void OnSceneLoadingCompleted(AsyncOperation asyncOperation)
        {
            Tuple<string, Popup.Args, Action> loadedSceneTuple =
                _loadingPopupScenes.Find(tuple => SceneManager.GetSceneByName(tuple.Item1).IsValid());
            _loadingPopupScenes.Remove(loadedSceneTuple);
            string loadedSceneName = loadedSceneTuple.Item1;

            Scene loadedScene = SceneManager.GetSceneByName(loadedSceneName);
            Assert.IsTrue(loadedScene.IsValid());

            GameObject[] rootGameObjects = loadedScene.GetRootGameObjects();
            Popup popup = null;

            foreach (GameObject go in rootGameObjects)
            {
                popup = go.GetComponent<Popup>();
                if (popup != null)
                {
                    break;
                }
            }

            if (popup == null)
            {
                UnityEngine.Debug.LogAssertion(
                    $"Popup scene {loadedSceneName} root must have {nameof(Popup)} component");
                return;
            }

            popup.gameObject.SetActive(true); // set to true to perform Awake() callback
            popup.SortingOptions.SortingLayerId = _sortingLayerId; // now we can set SortingLayer
            popup.gameObject.SetActive(false);

            _loadedPopups.Add(loadedSceneName, popup);
            popup.Destroying += OnPopupIsDestroying;

            if (_loadingPopupScenes.Count == 0)
            {
                Show(loadedSceneName, loadedSceneTuple.Item2, loadedSceneTuple.Item3);
            }
            else
            {
                Hide(loadedSceneName);
            }
        }

        private void OnPopupIsDestroying([NotNull] Popup popup)
        {
            _popupsStack.RemoveAll(tuple => tuple.Item1 == popup);
            ResetSortingOrder();

            _loadedPopups.Remove(popup.gameObject.scene.name);

            if (_popupsStack.Count == 0)
            {
                _shownPopupsCount = 0;
            }
        }

        private void ResetSortingOrder()
        {
            if (_popupsStack.Count > 0)
            {
                for (int i = 0; i < _popupsStack.Count - 1; i++)
                {
                    _popupsStack[i].Item1.SortingOptions.SortingOrder = i;
                }

                if (_tintPopup != null)
                {
                    _tintPopup.SortingOptions.SortingOrder = _popupsStack.Count;
                }

                _popupsStack[_popupsStack.Count - 1].Item1.SortingOptions.SortingOrder = _popupsStack.Count + 1;
            }
        }
    }
}