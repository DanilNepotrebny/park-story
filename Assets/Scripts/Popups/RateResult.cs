﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Popups
{
    public enum RateResult
    {
        Closed,
        Unhappy,
        Confused,
        Happy
    }

    public static class RateResultExtensions
    {
        public static string ToAnalyticsId(this RateResult self)
        {
            switch (self)
            {
                case RateResult.Closed:
                    return "closed";
                case RateResult.Unhappy:
                    return "unhappy";
                case RateResult.Confused:
                    return "confused";
                case RateResult.Happy:
                    return "happy";
            }

            UnityEngine.Debug.LogError($"Ivalid analytics value '{self}' for {self.GetType().Name} enum.");
            return "";
        }
    }
}