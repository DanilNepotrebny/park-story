﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.InviteFriends;
using DreamTeam.Localization;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Popups
{
    public class LivesPopup : SingleCountedItemRewardPurchasePopup
    {
        [SerializeField] private SceneReference _missingConnectionPopup;
        [SerializeField, LocalizationKey] private string _missingConnectionPopupTitle;
        [SerializeField, LocalizationKey] private string _missingConnectionPopupMessage;

        [Inject] private InviteFriendsSystem _inviteFriendsSystem;
        [Inject] private PopupSystem _popupSystem;

        private bool _isExchangeTriggered = false;

        public override bool Initialize(Popup.Args popupArgs)
        {
            Hidden += OnHidden;

            return base.Initialize(popupArgs);
        }

        public void ShowInventoryExchange(SceneReference exchangePopupScene, SocialProfilesCollectionPopup.Args args)
        {
            _isExchangeTriggered = true;

            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                _popupSystem.HideAll(() => _popupSystem.Show(exchangePopupScene.Name, args));
            }
            else
            {
                var missingConnectionArgs = new InformationPopup.Args()
                {
                    TitleKey = _missingConnectionPopupTitle,
                    MessageKey = _missingConnectionPopupMessage
                };

                _popupSystem.HideAll(() => _popupSystem.Show(_missingConnectionPopup.Name, missingConnectionArgs));
            }
        }

        private void OnHidden(Popup self)
        {
            Hidden -= OnHidden;

            if (!_isExchangeTriggered && _inviteFriendsSystem.CanShowInvitePopup())
            {
                _inviteFriendsSystem.EnqueueInvitePopup();
            }
        }
    }
}