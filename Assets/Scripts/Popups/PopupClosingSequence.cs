﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.TransformLookup;
using DreamTeam.Utils;
using DreamTeam.Utils.Movement;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Popups
{
    [RequireComponent(typeof(MovementSequence), typeof(RectTransform))]
    [RequireComponent(typeof(Canvas), typeof(CanvasSortingOverrider))]
    public class PopupClosingSequence : MonoBehaviour
    {
        [SerializeField] private TransformLookupId _finishTransformId;

        [Inject] private TransformLookupSystem _transformLookupSystem;

        [Inject] private Popup _popup;

        private MovementSequence _sequence => GetComponent<MovementSequence>();
        private Canvas _canvas => GetComponent<Canvas>();
        private CanvasSortingOverrider _canvasSortingOverrider => GetComponent<CanvasSortingOverrider>();

        protected void OnEnable()
        {
            if (_popup != null)
            {
                _popup.Hiding += OnPopupHiding;
            }

            _canvasSortingOverrider.enabled = false;
        }

        protected void OnDisable()
        {
            if (_popup != null)
            {
                _popup.Hiding -= OnPopupHiding;
            }
        }

        protected void OnValidate()
        {
            Assert.IsNotNull(
                _finishTransformId,
                $"{GetType().Name}: Please assign \"{nameof(_finishTransformId)}\" field.");
        }

        private void OnPopupHiding(Popup popup)
        {
            Transform finishPoint = _transformLookupSystem.FindTransform(_finishTransformId);

            PopupClosingSequence clone = Instantiate(this, finishPoint, true);
            clone._canvasSortingOverrider.enabled = true;
            clone._sequence.Move(finishPoint.position, true);

            gameObject.SetActive(false);
            _popup.Hidden += p => gameObject.SetActive(true);
        }
    }
}