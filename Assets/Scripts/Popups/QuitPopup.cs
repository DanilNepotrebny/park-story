﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Popups
{
    public class QuitPopup : Popup
    {
        [SerializeField] private SceneReference _debriefingPopup;
        [SerializeField] private Button _continueButton;
        [SerializeField] private Button _quitButton;

        [Inject] private PopupSystem _popupSystem;

        private Args _args;

        public override bool Initialize([CanBeNull] Popup.Args popupArgs)
        {
            base.Initialize(popupArgs);

            _args = popupArgs as Args;

            return true;
        }

        protected void Start()
        {
            _continueButton.onClick.AddListener(() => _popupSystem.Hide(gameObject.scene.name));
            _quitButton.onClick.AddListener(() => _args?.QuitButtonCallback?.Invoke());
        }

        public new class Args : Popup.Args
        {
            public Action QuitButtonCallback { get; set; }
        }
    }
}