﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Popups
{
    public class InputStringPopup : Popup
    {
        [SerializeField, Space] private InputField _inputField;
        [SerializeField] private Text _captionText;
        [SerializeField] private Text _descriptionText;
        [SerializeField] private GameObject[] _hideInConstrainedMode;

        private Args _args;

        public override bool Initialize(Popup.Args popupArgs)
        {
            if (!base.Initialize(popupArgs))
            {
                return false;
            }

            _args = popupArgs as Args;
            if (_args != null)
            {
                SetConstrainedMode(_args.IsConstrained);
                SetCaption(_args.Caption);
                SetDescription(_args.Description);
                _inputField.text = _args.DefaultValue;
            }

            return _args != null;
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void AcceptValue()
        {
            _args.ValueAcceptedCallback?.Invoke(_inputField.text);
        }

        private void SetConstrainedMode(bool isConstrained)
        {
            foreach (GameObject item in _hideInConstrainedMode)
            {
                item?.SetActive(!isConstrained);
            }
        }

        private void SetCaption(string caption)
        {
            if (_captionText != null)
            {
                _captionText.text = caption;
            }
        }

        private void SetDescription(string description)
        {
            if (_descriptionText != null)
            {
                _descriptionText.text = description;
            }
        }

        [Serializable]
        public new class Args : Popup.Args
        {
            [SerializeField] private bool _isConstrained;
            [SerializeField] private string _caption;
            [SerializeField] private string _description;

            public bool IsConstrained => _isConstrained;
            public string Caption => _caption;
            public string Description => _description;
            public string DefaultValue { get; set; }

            public Action<string> ValueAcceptedCallback;
        }
    }
}