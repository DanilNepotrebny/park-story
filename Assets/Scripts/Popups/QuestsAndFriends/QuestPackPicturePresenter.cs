﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Popups.QuestsAndFriends
{
    public class QuestPackPicturePresenter : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [Inject] private QuestManager _questManager;

        protected void Start()
        {
            _image.sprite = _questManager.CurrentPack?.Picture;
        }

        protected void OnValidate()
        {
            if (_image == null)
            {
                UnityEngine.Debug.LogAssertion($"Missing {nameof(_image)}", this);
            }
        }
    }
}