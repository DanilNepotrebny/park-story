﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Quests;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Popups.QuestsAndFriends
{
    public class QuestPackTitlePresenter : MonoBehaviour
    {
        [SerializeField] private Text _label;
        [Inject] private QuestManager _questManager;

        protected void OnEnable()
        {
            if (_questManager.CurrentPack.IsWaitingForCompletion)
            {
                SetTitleText(_questManager.NextPack);
            }
            else
            {
                SetTitleText(_questManager.CurrentPack);
            }
        }

        private void SetTitleText(QuestPack pack)
        {
            if (pack != null)
            {
                _label.text = pack.Title;
            }
        }

        protected void OnValidate()
        {
            if (_label == null)
            {
                UnityEngine.Debug.LogAssertion($"Missing {nameof(_label)}", this);
            }
        }
    }
}