﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.GameControls;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Popups
{
    public class PopupQueueSystem : MonoBehaviour
    {
        [SerializeField, RequiredField]
        private GameControl _popupQueueControl;

        [Inject] private PopupSystem _popupSystem;
        [Inject] private GameControlsSystem _gameControlsSystem;

        private Queue<PopupData> _queue = new Queue<PopupData>();

        public void Enqueue(string popupSceneName, Popup.Args popupArgs = null, Action callback = null)
        {
            _queue.Enqueue(
                    new PopupData
                    {
                        SceneName = popupSceneName,
                        Args = popupArgs,
                        Callback = callback
                    }
                );

            TryShowNextPopup();
        }

        protected void Awake()
        {
            _gameControlsSystem.AddGameControlStateChangeHandler(_popupQueueControl, OnControlStateChanged);
            _popupSystem.PopupClosed += TryShowNextPopup;
        }

        private void OnControlStateChanged(GameControl.State controlState)
        {
            if (controlState == GameControl.State.Unlocked)
            {
                TryShowNextPopup();
            }
        }

        private void TryShowNextPopup()
        {
            if (!_popupSystem.HasShowingPopup() &&
                _queue.Count > 0 &&
                _gameControlsSystem.IsControlInState(_popupQueueControl, GameControl.State.Unlocked))
            {
                PopupData popupData = _queue.Dequeue();
                _popupSystem.Show(
                        popupData.SceneName,
                        popupData.Args,
                        popupData.Callback
                    );
            }
        }

        #region Inner types

        private class PopupData
        {
            public string SceneName;
            public Popup.Args Args;
            public Action Callback;
        }

        #endregion
    }
}