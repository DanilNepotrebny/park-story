﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Popups.Destabilization
{
    [RequireComponent(typeof(GroupMotionAnimator))]
    public class GroupMotionAnimatorDestabiliser : PopupDestabilizer
    {
        public override bool IsStable => !_groupMotionAnimator.IsPlaying;

        private GroupMotionAnimator _groupMotionAnimator => GetComponent<GroupMotionAnimator>();

        protected override void OnEnable()
        {
            base.OnEnable();

            _groupMotionAnimator.MotionStarted += HandleDestabilized;
            _groupMotionAnimator.MotionFinished += HandleStabilized;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            _groupMotionAnimator.MotionStarted -= HandleDestabilized;
            _groupMotionAnimator.MotionFinished -= HandleStabilized;
        }
    }
}