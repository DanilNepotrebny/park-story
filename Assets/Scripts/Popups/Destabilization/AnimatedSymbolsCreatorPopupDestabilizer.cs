﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI.SymbolAnimation;
using UnityEngine;

namespace DreamTeam.Popups.Destabilization
{
    [RequireComponent(typeof(AnimatedSymbolsCreator))]
    public class AnimatedSymbolsCreatorPopupDestabilizer : PopupDestabilizer
    {
        private AnimatedSymbolsCreator _symbolsCreator;

        public override bool IsStable => _symbolsCreator.State != AnimatedSymbolsCreator.AnimationState.InProgress;

        protected void Awake()
        {
            _symbolsCreator = GetComponent<AnimatedSymbolsCreator>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _symbolsCreator.StateChanged += OnSymbolsCreatorStateChanged;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            _symbolsCreator.StateChanged -= OnSymbolsCreatorStateChanged;
        }

        private void OnSymbolsCreatorStateChanged()
        {
            if (IsStable)
            {
                HandleStabilized();
            }
            else
            {
                HandleDestabilized();
            }
        }
    }
}