﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;

namespace DreamTeam.Popups.Destabilization
{
    public interface IPopupDestabilizer
    {
        event Action Stabilized;
        event Action Destabilized;

        bool IsStable { get; }
    }
}