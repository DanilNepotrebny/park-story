﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using Zenject;

namespace DreamTeam.Popups.Destabilization
{
    public abstract class PopupDestabilizer : MonoBehaviour, IPopupDestabilizer
    {
        [Inject] private Popup _popup;

        public event Action Stabilized;
        public event Action Destabilized;

        public abstract bool IsStable { get; }

        protected virtual void OnEnable()
        {
            _popup.AddDestabilizer(this);
        }

        protected virtual void OnDisable()
        {
            _popup.RemoveDestabilizer(this);
        }

        protected void HandleStabilized()
        {
            Stabilized?.Invoke();
        }

        protected void HandleDestabilized()
        {
            Destabilized?.Invoke();
        }
    }
}