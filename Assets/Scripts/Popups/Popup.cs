﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using DreamTeam.GameControls;
using DreamTeam.InputLock;
using DreamTeam.Popups.Destabilization;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.RendererSorting;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Popups
{
    public class Popup : MonoBehaviour, ISortingOptionsProvider
    {
        [SerializeField] private SimpleAnimation _animation;
        [SerializeField] private CanvasGroup _canvasGroup;

        [Header("Controls")]
        [SerializeField] private bool _pushNewContext = true;
        [SerializeField, FormerlySerializedAs("_selectionMode")] private GameControlsMode _controlsMode;

        [Inject] private Instantiator _instantiator;
        [InjectOptional] private GameControlsSystem _gameControlsSystem;
        [Inject] private InputLockSystem _inputLockSystem;

        private Args _args;
        private Canvas _canvas;
        private ActionDisposable _contextRemoveHandle;
        private ScreenOrientationNotifier.Orientation _orientationOnDisable;
        private readonly List<IPopupDestabilizer> _destabilizers = new List<IPopupDestabilizer>();
        private ActionDisposable _modeDisableHandle;
        private IDisposable _inputLockDisposable;

        private State _state = State.Hidden;

        public SortingOptions SortingOptions { get; private set; }

        public bool IsShown => _state == State.Shown;
        public bool IsChangingState => _state == State.Showing || _state == State.Hiding;
        public bool IsHidden => _state == State.Hidden;

        public string SceneName => gameObject.scene.name;

        /// <summary>
        /// Called when popup will be destroyed
        /// </summary>
        public event Action<Popup> Destroying;

        /// <summary>
        /// Called when popup played all appearing animations and is a stable state
        /// </summary>
        [Obfuscation(Exclude = false, Feature = "-rename")]
        public event Action<Popup> Shown;

        /// <summary>
        /// Called when popup starts playing appearing animation
        /// </summary>
        public event Action<Popup> Showing;

        /// <summary>
        /// Called when popup begins closing
        /// </summary>
        public event Action<Popup> Hiding;

        /// <summary>
        /// Called when popup was completely closed
        /// </summary>
        public event Action<Popup> Hidden;

        private Action _onShownCallback;
        private Action _onHiddenCallback;
        private Action _onStabilizedCallback;

        /// <summary>
        /// Initialize popup with passed popup arguments
        /// </summary>
        /// <param name="popupArgs">popup arguments</param>
        /// <returns>Is initialized successfuly</returns>
        public virtual bool Initialize([CanBeNull] Args popupArgs)
        {
            _args = popupArgs;
            return true;
        }

        /// <summary>
        /// Must be called only by PopupSystem. Use PopupsSystem instead
        /// </summary>
        public virtual bool Show(Action onShownCallback = null)
        {
            switch (_state)
            {
                case State.Hiding:
                case State.Hidden:
                    ShowInternal(onShownCallback);
                    break;
                case State.Showing:
                    _onShownCallback += onShownCallback;
                    break;
                case State.Shown:
                    onShownCallback?.Invoke();
                    break;
            }

            return true;
        }

        /// <summary>
        /// Must be called only by PopupSystem. Use PopupsSystem instead
        /// </summary>
        public virtual void Hide(Action onHiddenCallback = null)
        {
            switch (_state)
            {
                case State.Showing:
                case State.Shown:
                    HideInternal(onHiddenCallback);
                    break;
                case State.Hiding:
                    _onShownCallback += onHiddenCallback;
                    break;
                case State.Hidden:
                    onHiddenCallback?.Invoke();
                    break;
            }
        }

        public void AddDestabilizer(IPopupDestabilizer destabilizer)
        {
            if (!_destabilizers.Contains(destabilizer))
            {
                _destabilizers.Add(destabilizer);

                destabilizer.Stabilized += OnDestabilizerStabilized;
                destabilizer.Destabilized += OnDestabilizerDestabilized;
            }
        }

        public void RemoveDestabilizer(IPopupDestabilizer destabilizer)
        {
            _destabilizers.Remove(destabilizer);

            destabilizer.Stabilized -= OnDestabilizerStabilized;
            destabilizer.Destabilized -= OnDestabilizerDestabilized;
        }

        protected void Awake()
        {
            _canvas = GetComponentInChildren<Canvas>();
            Assert.IsNotNull(_canvas, $"Missing Canvas component in object {name}");

            SortingOptions = _instantiator.Instantiate<SortingOptions>(_canvas.sortingLayerID, _canvas.sortingOrder);
        }

        protected void OnEnable()
        {
            _animation.AnimationStopped += OnAnimationStopped;
            SubscribeToSortingOptionsEvents();
        }

        protected void OnDisable()
        {
            _animation.AnimationStopped -= OnAnimationStopped;
            UnsubscribeFromSortingOptionsEvents();

            UnlockInput();
        }

        protected void OnValidate()
        {
            if (_animation == null)
            {
                UnityEngine.Debug.LogWarning($"Missed Animation on {name}", this);
            }
        }

        protected void OnDestroy()
        {
            ReleaseModeDisableHandle();
            _contextRemoveHandle?.Dispose();
            UnsubscribeFromSortingOptionsEvents();
            Destroying?.Invoke(this);
        }

        /// <summary>
        /// Override this to make child class execute some logic on popup showing.
        /// </summary>
        protected virtual void OnShowing()
        {
            _state = State.Showing;

            Showing?.Invoke(this);
            LockInput();
        }

        /// <summary>
        /// Override this to make child class execute some logic on popup hiding.
        /// </summary>
        protected virtual void OnHiding()
        {
            _state = State.Hiding;

            Hiding?.Invoke(this);
            if (_args != null)
            {
                _args.ClosingCallback?.Invoke();
                _args.ClosingCallback = null;
            }

            LockInput();
        }

        protected void SetPopupInteractable(bool isInteractable)
        {
            if (_canvasGroup != null)
            {
                _canvasGroup.interactable = isInteractable;
            }
        }

        private void ShowInternal(Action onShownCallback = null)
        {
            _animation.Stop();

            _onShownCallback = onShownCallback;
            SetControlsModeEnabled(true);
            gameObject.SetActive(true);

            OnShowing();

            if (!_animation.Play(SimpleAnimationType.Show))
            {
                OnAnimationStopped(SimpleAnimationType.Show);
            }
        }

        private void HideInternal(Action onHiddenCallback)
        {
            _animation.Stop();
            OnHiding();

            _onHiddenCallback = onHiddenCallback;

            ExecuteAfterStabilization(
                    () =>
                    {
                        if (!_animation.Play(SimpleAnimationType.Hide))
                        {
                            OnAnimationStopped(SimpleAnimationType.Hide);
                        }
                    }
                );
        }

        private void OnAnimationStopped(SimpleAnimationType animationType)
        {
            switch (animationType)
            {
                case SimpleAnimationType.Show:
                {
                    OnShown();
                    break;
                }
                case SimpleAnimationType.Hide:
                {
                    OnHidden();
                    break;
                }
                case SimpleAnimationType.Idle:
                    break;
                default:
                    throw new InvalidEnumArgumentException(animationType.ToString(), (int)animationType, typeof(SimpleAnimationType));
            }
        }

        private void OnShown()
        {
            _state = State.Shown;

            Shown?.Invoke(this);

            _onShownCallback?.Invoke();
            _onShownCallback = null;

            if (_args != null)
            {
                _args.ShowedCallback?.Invoke();
                _args.ShowedCallback = null;
            }

            UnlockInput();
        }

        private void OnHidden()
        {
            _state = State.Hidden;

            gameObject.SetActive(false);
            SetControlsModeEnabled(false);

            _onHiddenCallback?.Invoke();
            _onHiddenCallback = null;

            if (_args != null)
            {
                _args.ClosedCallback?.Invoke();
                _args.ClosedCallback = null;
            }

            Hidden?.Invoke(this);

            UnlockInput();
        }

        private void LockInput()
        {
            _inputLockDisposable = _inputLockSystem.LockTargets();
        }

        private void UnlockInput()
        {
            if (_inputLockDisposable != null)
            {
                _inputLockDisposable.Dispose();
                _inputLockDisposable = null;
            }
        }

        private bool IsPopupStable()
        {
            return _destabilizers.Find(item => !item.IsStable) == null;
        }

        private void ExecuteAfterStabilization(Action onStabilizedCallback)
        {
            if (IsPopupStable())
            {
                onStabilizedCallback.Invoke();
            }
            else
            {
                _onStabilizedCallback = onStabilizedCallback;
            }
        }

        private void OnDestabilizerDestabilized()
        {
            SetPopupInteractable(false);
        }

        private void OnDestabilizerStabilized()
        {
            if (IsPopupStable())
            {
                SetPopupInteractable(true);
                _onStabilizedCallback?.Invoke();
                _onStabilizedCallback = null;
            }
        }

        private void SubscribeToSortingOptionsEvents()
        {
            SortingOptions.SortingLayerChanged += OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged += OnSortingOrderChanged;
        }

        private void UnsubscribeFromSortingOptionsEvents()
        {
            SortingOptions.SortingLayerChanged -= OnSortingLayerChanged;
            SortingOptions.SortingOrderChanged -= OnSortingOrderChanged;
        }

        private void OnSortingLayerChanged(SortingOptions options)
        {
            _canvas.sortingLayerID = options.SortingLayerId;
        }

        private void OnSortingOrderChanged(SortingOptions options, int prevOrder)
        {
            _canvas.sortingOrder = options.SortingOrder;
        }

        private void SetControlsModeEnabled(bool isEnabled)
        {
            if (_gameControlsSystem == null)
            {
                return;
            }

            if (_pushNewContext)
            {
                if (isEnabled)
                {
                    Assert.IsNull(_contextRemoveHandle, $"Context remove handle is not disposed");
                    var context = _instantiator.Instantiate<GameControlsContext>(
                            gameObject.scene.name,
                            _gameControlsSystem
                        );

                    if (_controlsMode != null)
                    {
                        context.EnableMode(_controlsMode);
                    }

                    _contextRemoveHandle = _gameControlsSystem.PushContext(context);
                }
                else
                {
                    IDisposable handle = _contextRemoveHandle;
                    _contextRemoveHandle = null;
                    handle.Dispose();
                }
            }
            else if (_controlsMode != null)
            {
                if (isEnabled)
                {
                    Assert.IsNull(_modeDisableHandle, "_modeDisableHandle != null");
                    _modeDisableHandle = _gameControlsSystem.EnableMode(_controlsMode);
                }
                else
                {
                    ReleaseModeDisableHandle();
                }
            }
        }

        private void ReleaseModeDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }

        #region Inner types

        /// <summary>
        /// Override to pass popup specific data into the popup
        /// </summary>
        public class Args
        {
            public Action ShowedCallback { get; set; }
            public Action ClosingCallback { get; set; }
            public Action ClosedCallback { get; set; }
        }

        public enum State
        {
            Hidden,
            Showing,
            Shown,
            Hiding
        }

        #endregion Inner types
    }
}