﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.InviteFriends;
using DreamTeam.Messaging;
using DreamTeam.Messaging.Filtering;
using DreamTeam.UI;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Popups
{
    [RequireComponent(typeof(PresenterHub))]
    public class FilteredMessagesPopup : Popup
    {
        [Inject] private MessageSystem _messageSystem;
        [Inject] private InviteFriendsSystem _inviteFriendsSystem;

        private List<IMessage> _filteredMessages = new List<IMessage>();
        private Args _args;

        private bool _isMessageConsumed = false;

        public override bool Initialize([CanBeNull] Popup.Args popupArgs)
        {
            if (base.Initialize(popupArgs))
            {
                var args = popupArgs as Args;
                if (args != null)
                {
                    _args = args;

                    Hidden += OnHidden;

                    UpdateContent();
                }
                return args != null;
            }
            return false;
        }

        protected override void OnShowing()
        {
            base.OnShowing();

            _messageSystem.MessageAdded += OnMessageAdded;
            _messageSystem.MessageRemoved += OnMessageRemoved;
        }

        protected override void OnHiding()
        {
            base.OnHiding();

            _messageSystem.MessageAdded -= OnMessageAdded;
            _messageSystem.MessageRemoved -= OnMessageRemoved;
        }

        private void OnMessageRemoved(IMessage message)
        {
            _isMessageConsumed = true;

            if (_filteredMessages.Contains(message))
            {
                UpdateContent();
            }
        }

        private void OnMessageAdded(IMessage message)
        {
            if (_args.Filter.IsSuitable(message))
            {
                UpdateContent();
            }
        }

        private void UpdateContent()
        {
            _messageSystem.Filter(_args.Filter, _filteredMessages);
            GetComponent<PresenterHub>().Initialize(_filteredMessages);
        }

        private void OnHidden(Popup self)
        {
            Hidden -= OnHidden;

            if (!_isMessageConsumed && _inviteFriendsSystem.CanShowInvitePopup())
            {
                _inviteFriendsSystem.EnqueueInvitePopup();
            }
        }

        public new class Args : Popup.Args
        {
            public MessageFilter Filter { get; set; }
        }
    }
}