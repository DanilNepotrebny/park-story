﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.CustomerSupport;
using DreamTeam.GameRating;
using DreamTeam.Inventory;
using DreamTeam.Utils;
using Zenject;

namespace DreamTeam.Popups
{
    public class RateUsPopup : Popup
    {
        [Inject] private CustomerSupportSystem _customerSupportSystem;
        [Inject] private RateGameSystem _rateSystem;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private Instantiator _instantiator;

        private RateResult _rateResult = RateResult.Closed;

        [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
        public void RateHappy()
        {
            Rate(RateResult.Happy);
        }

        [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
        public void RateConfused()
        {
            Rate(RateResult.Confused);
        }

        [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
        public void RateUnhappy()
        {
            Rate(RateResult.Unhappy);
        }

        protected override void OnHiding()
        {
            base.OnHiding();

            if (_rateResult == RateResult.Closed)
            {
                SendAnalyticsEvent(_rateResult);
            }
        }

        private void Rate(RateResult rateResult)
        {
            _rateResult = rateResult;

            switch (rateResult)
            {
                case RateResult.Happy:
                    _rateSystem.RateGame(GamePlace.RateUs);
                    break;

                case RateResult.Confused:
                    _customerSupportSystem.ShowFAQ();
                    break;

                case RateResult.Unhappy:
                    _customerSupportSystem.ShowConversation();
                    break;
            }

            SendAnalyticsEvent(rateResult);
        }

        private void SendAnalyticsEvent(RateResult rateResult)
        {
            _analyticsSystem.Send(
                    _instantiator.Instantiate<InGameRateEvent>(rateResult)
                );
        }
    }
}