﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using DreamTeam.Inventory.Rewards;
using DreamTeam.UI;
using DreamTeam.UI.SymbolAnimation;
using DreamTeam.Utils;
using DreamTeam.Utils.Coroutines;
using UnityEngine;

namespace DreamTeam.Popups
{
    public class RewardsPopup : Popup
    {
        [Space, Header("Rewards popup data")] [SerializeField] private RectTransform _contentTransform;
        [SerializeField] private Animation _rewardsAnimation;
        [SerializeField] private bool _playShowRewardsAutomatically = true;
        [SerializeField] private GameObjectActivityList _popupObjectsInitialActivity;
        [SerializeField] private float _headerTextAppearDelay;
        [SerializeField] private AnimatedSymbolsCreator _headerText;

        private Args _args;

        public event Action GivingRewards;

        public RectTransform ContentTransform => _contentTransform;

        public override bool Initialize(Popup.Args popupArgs)
        {
            if (!base.Initialize(popupArgs))
            {
                return false;
            }

            _rewardsAnimation.gameObject.SetActive(false);

            _args = popupArgs as Args;
            if (_args != null)
            {
                var presenter = GetComponentInChildren<PresenterHub>(true);
                presenter?.Initialize(_args.Rewards);
            }

            return _args != null;
        }

        public void PlayShowRewards()
        {
            _rewardsAnimation.gameObject.SetActive(true);
            _rewardsAnimation?.Play();
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void GiveRewards()
        {
            if (_args.RewardsTakenCallback != null)
            {
                GivingRewards?.Invoke();

                _args.RewardsTakenCallback.Invoke();
                _args.RewardsTakenCallback = null;
            }
        }

        protected override void OnShowing()
        {
            base.OnShowing();

            foreach (var objectActivity in _popupObjectsInitialActivity)
            {
                objectActivity.Key.SetActive(objectActivity.Value);
            }

            if (_playShowRewardsAutomatically)
            {
                PlayShowRewards();
            }

            StartCoroutine(PlayAppearSequence());
        }

        protected override void OnHiding()
        {
            base.OnHiding();
            StartCoroutine(_headerText.Hide());
        }

        private IEnumerator PlayAppearSequence()
        {
            SetPopupInteractable(false);
            yield return new WaitForEvent<Popup, Popup>(this, nameof(Shown));
            yield return StartCoroutine(PlayHeaderAppearAnimation());
            SetPopupInteractable(true);
        }

        private IEnumerator PlayHeaderAppearAnimation()
        {
            yield return new WaitForSeconds(_headerTextAppearDelay);

            _headerText.gameObject.SetActive(true);
            _headerText.ResetState();
            if (!string.IsNullOrEmpty(_args.Header))
            {
                yield return _headerText.Show(_args.Header);
            }
            else
            {
                yield return _headerText.Show();
            }
        }

        public new class Args : Popup.Args
        {
            public ICollection<Reward> Rewards { get; set; }
            public Action RewardsTakenCallback { get; set; }
            public string Header { get; set; }
        }

        [Serializable]
        public class GameObjectActivityList : KeyValueList<GameObject, bool>
        {
        }
    }
}