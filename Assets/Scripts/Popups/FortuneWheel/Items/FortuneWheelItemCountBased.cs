﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DreamTeam.Popups.FortuneWheel.Items
{
    [CreateAssetMenu(fileName = "FortuneWheelItem", menuName = "FortuneWheel/Count based item")]
    public class FortuneWheelItemCountBased : FortuneWheelItemBase, ISynchronizable
    {
        [SerializeField] private List<RollsRange> _ranges = new List<RollsRange>();

        private ItemState _state;
        private int _currentRangeIndex = 0;
        private int _desiredRoll = 0;
        private int _rollsCounter = 0;

        public void Roll()
        {
            switch (_state)
            {
                case ItemState.NoAcquireInfo:
                    _currentRangeIndex = 0;
                    _state = ItemState.WaitForDesiredRoll;
                    _desiredRoll = Random.Range(_ranges[_currentRangeIndex].Min, _ranges[_currentRangeIndex].Max);
                    break;

                case ItemState.WaitForDesiredRoll:
                    ++_rollsCounter;
                    break;

                case ItemState.WaitForNewCalculation:
                    ++_rollsCounter;
                    if (_rollsCounter >= _ranges[_currentRangeIndex].Max)
                    {
                        _state = ItemState.NoAcquireInfo;
                        _currentRangeIndex = Mathf.Min(_currentRangeIndex + 1, _ranges.Count - 1);
                        _desiredRoll = 0;
                        _rollsCounter = 0;
                    }
                    break;
            }            
        }

        public bool CanAcquire()
        {
            return _rollsCounter >= _desiredRoll && _state == ItemState.WaitForDesiredRoll;
        }

        public void Acquire()
        {
            if (CanAcquire())
            {
                _state = ItemState.WaitForNewCalculation;
            }
        }

        public void Sync(State state)
        {
            int tempState = (int)_state;
            state.SyncInt(nameof(_state), ref tempState);
            _state = (ItemState)tempState;

            state.SyncInt(nameof(_currentRangeIndex), ref _currentRangeIndex);
            state.SyncInt(nameof(_desiredRoll), ref _desiredRoll);
            state.SyncInt(nameof(_rollsCounter), ref _rollsCounter);
        }

        [Serializable]
        private struct RollsRange
        {
            public int Min;
            public int Max;
        }

        private enum ItemState
        {
            NoAcquireInfo,
            WaitForDesiredRoll,
            WaitForNewCalculation
        }
    }
}