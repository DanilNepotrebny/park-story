﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory.Rewards;
using DreamTeam.UI.RewardPresentationSequences;
using UnityEngine;

namespace DreamTeam.Popups.FortuneWheel.Items
{
    public abstract class FortuneWheelItemBase : ScriptableObject
    {
        public enum Sectors
        {
            Sector0,
            Sector1,
            Sector2,
            Sector3,
            Sector4,
            Sector5,
            Sector6,
            Sector7
        }

        [SerializeField, HideInInspector] private Sectors _sector;
        [SerializeField, HideInInspector] private Sprite _icon;
        [SerializeField, HideInInspector] private string _text;
        [SerializeField, HideInInspector] private RewardPresentationSequenceBase _rewardsPresentationSequence;
        [SerializeField, HideInInspector] private List<Reward> _rewards = new List<Reward>();

        public Sectors Sector => _sector;
        public Sprite Icon => _icon;
        public string Text => _text;
        public RewardPresentationSequenceBase RewardsSequence => _rewardsPresentationSequence;
        public IList<Reward> Rewards => _rewards.AsReadOnly();
    }
}