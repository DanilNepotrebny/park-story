﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Popups.FortuneWheel.Items
{
    [CreateAssetMenu(fileName = "FortuneWheelItem", menuName = "FortuneWheel/Weight based item")]
    public class FortuneWheelItemWeightBased : FortuneWheelItemBase
    {
        [SerializeField] private int _weight;

        public int Weight => _weight;
    }
}