﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Popups.FortuneWheel
{
    public class FortuneWheelLampsController : MonoBehaviour
    {
        [SerializeField] private float _duration;
        [SerializeField] private List<FortuneWheelLampController> _lamps;

        private Coroutine _controlRoutine;

        protected void OnEnable()
        {
            if (_controlRoutine != null)
            {
                StopCoroutine(_controlRoutine);
            }

            _controlRoutine = StartCoroutine(ControlLampsRoutine());
        }

        protected void OnDisable()
        {
            if (_controlRoutine != null)
            {
                StopCoroutine(_controlRoutine);
                _controlRoutine = null;
            }
        }

        private IEnumerator ControlLampsRoutine()
        {
            foreach (FortuneWheelLampController controller in _lamps)
            {
                controller.SetDisabled(0f);
            }

            while (true)
            {
                for (int i = 0; i < _lamps.Count; i += 2)
                {
                    FortuneWheelLampController controller = _lamps[i];
                    controller.SetHighlighted(_duration);
                }

                yield return new WaitForSeconds(_duration);

                for (int i = 0; i < _lamps.Count; i += 2)
                {
                    FortuneWheelLampController controller = _lamps[i];
                    controller.SetDisabled(_duration);
                }

                for (int i = 1; i < _lamps.Count; i += 2)
                {
                    FortuneWheelLampController controller = _lamps[i];
                    controller.SetHighlighted(_duration);
                }

                yield return new WaitForSeconds(_duration);

                for (int i = 1; i < _lamps.Count; i += 2)
                {
                    FortuneWheelLampController controller = _lamps[i];
                    controller.SetDisabled(_duration);
                }
            }
        }
    }
}
