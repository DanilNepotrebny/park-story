﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Popups.FortuneWheel
{
    public class FortuneWheelLampController : MonoBehaviour
    {
        [SerializeField] private Color _enabledColor;
        [SerializeField] private Color _disabledColor;
        [SerializeField] private Image _highlighted;

        private Coroutine _currentCoroutine;

        public void SetHighlighted(float duration)
        {
            if (_currentCoroutine != null)
            {
                StopCoroutine(_currentCoroutine);
            }

            _currentCoroutine = StartCoroutine(SetColorTimed(_disabledColor, _enabledColor, duration));
        }

        public void SetDisabled(float duration)
        {
            if (_currentCoroutine != null)
            {
                StopCoroutine(_currentCoroutine);
            }

            _currentCoroutine = StartCoroutine(SetColorTimed(_enabledColor, _disabledColor, duration));
        }

        protected void OnDestroy()
        {
            if (_currentCoroutine != null)
            {
                StopCoroutine(_currentCoroutine);
                _currentCoroutine = null;
            }
        }

        private IEnumerator SetColorTimed(Color colorFrom, Color colorTo, float duration)
        {
            duration = Mathf.Clamp(duration, 0f, duration);
            float timer = 0f;
            while (timer < duration)
            {
                _highlighted.color = Color.Lerp(colorFrom, colorTo, timer);
                timer += Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }

            _highlighted.color = colorTo;
        }
    }
}