﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Popups.FortuneWheel.Items;
using DreamTeam.UI.RewardPresentationSequences;
using DreamTeam.Utils;
using Spine.Unity.Modules;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Popups.FortuneWheel
{
    [RequireComponent(typeof(FortuneWheelController))]
    [RequireComponent(typeof(FortuneWheelLampsController))]
    public class FortuneWheelPopup : Popup
    {
        [Inject] private PopupSystem _popupSystem;
        [Inject] private FortuneWheelSystem _fortuneWheelSystem;
        [Inject] private Instantiator _instantiator;

        [Header("Wheel data")]
        [SerializeField] private List<Button> _quitButtons = new List<Button>();
        [SerializeField] private Button _stopButton;
        [SerializeField] private List<FortuneWheelItemSector> _sectors = new List<FortuneWheelItemSector>();

        [Header("Animation data")]
        [SerializeField] private SkeletonGraphicMultiObject _fortuneManSpineAnimation;
        [SerializeField] private string _fortuneManSpineAnimationIdle;
        [SerializeField] private string _fortuneManSpineAnimationHappy;

        [Space]
        [SerializeField] private Animation _fortuneManBubbleAnimation;
        [SerializeField] private AnimationClip _fortuneManBubbleShow;
        [SerializeField] private AnimationClip _fortuneManBubbleHide;

        [Space]
        [SerializeField] private Animation _fortuneManAnimation;
        [SerializeField] private AnimationClip _fortuneManFadeIn;
        [SerializeField] private AnimationClip _fortuneManFadeOut;

        private FortuneWheelController _wheelController;
        private FortuneWheelItemBase _currentReward = null;
        private PopupState _state;

        public SkeletonGraphicMultiObject FortuneManSpineAnimation => _fortuneManSpineAnimation;
        public string FortuneManSpineAnimationIdleName => _fortuneManSpineAnimationIdle;
        public string FortuneManSpineAnimationHappyName => _fortuneManSpineAnimationHappy;

        public Animation FortuneManBubbleAnimation => _fortuneManBubbleAnimation;
        public AnimationClip FortuneManBubbleAnimationShow => _fortuneManBubbleShow;
        public AnimationClip FortuneManBubbleAnimationHide => _fortuneManBubbleHide;

        public Animation FortuneManAnimation => _fortuneManAnimation;
        public AnimationClip FortuneManAnimationFadeIn => _fortuneManFadeIn;
        public AnimationClip FortuneManAnimationFadeOut => _fortuneManFadeOut;

        protected new void Awake()
        {
            base.Awake();

            _wheelController = GetComponent<FortuneWheelController>();
        }

        protected new void OnEnable()
        {
            base.OnEnable();

            FortuneManAnimation.Play(_fortuneManFadeIn.name);
            FortuneManSpineAnimation.AnimationState?.SetAnimation(0, FortuneManSpineAnimationIdleName, true);

            foreach (FortuneWheelItemSector sector in _sectors)
            {
                sector.Initialize(_fortuneWheelSystem.WheelElementsMap[sector.Sector]);
            }

            SetState(PopupState.WheelRotating);
        }

        protected override void OnHiding()
        {
            base.OnHiding();

            FortuneManAnimation.Play(_fortuneManFadeOut.name);
        }

        private void SetState(PopupState state)
        {
            _state = state;

            ResetUI();
        }

        private void ResetUI()
        {
            switch (_state)
            {
                case PopupState.WheelRotating:
                    SetWheelRotatingUIState();
                    break;

                case PopupState.WheelStopping:
                    SetWheelStoppingUIState();
                    break;

                case PopupState.PrizeAnimation:
                    SetPrizeAnimationUIState();
                    break;
            }
        }

        private void SetWheelRotatingUIState()
        {
            foreach (Button quitButton in _quitButtons)
            {
                quitButton.interactable = true;
            }

            _stopButton.interactable = true;
            _stopButton.gameObject.SetActive(true);
            _stopButton.onClick.RemoveAllListeners();
            _stopButton.onClick.AddListener(ActionButtonStopRotating);
        }

        private void SetWheelStoppingUIState()
        {
            foreach (Button quitButton in _quitButtons)
            {
                quitButton.interactable = false;
            }

            _stopButton.interactable = false;
            _stopButton.gameObject.SetActive(true);
            _stopButton.onClick.RemoveAllListeners();
        }

        private void SetPrizeAnimationUIState()
        {
            foreach (Button quitButton in _quitButtons)
            {
                quitButton.interactable = false;
            }

            _stopButton.interactable = false;
            _stopButton.gameObject.SetActive(false);
            _stopButton.onClick.RemoveAllListeners();
        }

        private void ActionButtonStopRotating()
        {
            StartCoroutine(WheelStoppingRoutine());
        }

        private IEnumerator WheelStoppingRoutine()
        {
            SetState(PopupState.WheelStopping);

            _currentReward = _fortuneWheelSystem.PopNextReward();
            foreach (Reward reward in _currentReward.Rewards)
            {
                reward.Receive(_instantiator, ReceivingType.Earned, GamePlace.FortuneWheel);
            }

            FortuneWheelItemSector sector = _sectors.Find(s => s.Sector == _currentReward.Sector);
            _wheelController.StopRotation(sector.transform);
            yield return new WaitUntil(() => _wheelController.CurrentState == FortuneWheelController.State.Stopped);

            SetState(PopupState.PrizeAnimation);
            RewardPresentationSequenceBase presentationSequence = _instantiator.Instantiate(_currentReward.RewardsSequence, gameObject);
            presentationSequence.transform.SetParent(null);
            presentationSequence.gameObject.hideFlags = HideFlags.DontSave;
            presentationSequence.Present(new Collection<Reward>(_currentReward.Rewards), null);
            yield return new WaitUntil(() => presentationSequence.IsPresentationComplete);
            presentationSequence.gameObject.Dispose();
            _popupSystem.Hide(gameObject.scene.name);
        }

        private enum PopupState
        {
            WheelRotating,
            WheelStopping,
            PrizeAnimation
        }
    }
}