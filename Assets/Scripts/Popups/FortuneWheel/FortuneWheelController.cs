﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using DreamTeam.Audio;
using UnityEngine;

namespace DreamTeam.Popups.FortuneWheel
{
    public class FortuneWheelController : MonoBehaviour
    {
        public enum Direction
        {
            ClockWise,
            AntiClockWise
        }

        public enum State
        {
            Rotating,
            Stopping,
            Stopped
        }

        [SerializeField] private Direction _rotationDirection;
        [SerializeField] private AudioSystemPlayer _rotationSound;
        [SerializeField] private float _initialSpeed;
        [SerializeField] private AnimationCurve _stoppingSpeedEvolutionCurve;
        [SerializeField] private AudioSystemPlayer _stopingSound;
        [SerializeField] private float _stoppingTime = 0f;
        [SerializeField] private AnimationCurve _finalizingAnglesEvolutionCurve;
        [SerializeField] private float _finalizingTime = 0f;
        [SerializeField] private List<Transform> _wheelComponents = new List<Transform>();

        private float _initialDeltaTime = 0f;
        private Vector3 _rotationAxis = Vector3.forward;
        private float _currentAngle = 0f;
        private Coroutine _rotationCoroutine;
        private Transform _desiredSector;

        public State CurrentState { get; private set; } = State.Stopped;

        public void StopRotation(Transform desiredSector)
        {
            if (CurrentState == State.Rotating)
            {
                _desiredSector = desiredSector;
                CurrentState = State.Stopping;
            }
        }

        protected void Awake()
        {
            _initialDeltaTime = Time.fixedDeltaTime;
            _rotationAxis = _rotationDirection == Direction.ClockWise ? Vector3.back : Vector3.forward;
        }

        protected void OnEnable()
        {
            StartRotation();
        }

        protected void OnDisable()
        {
            StopRotation();
        }

        private void StopRotation()
        {
            if (CurrentState == State.Rotating)
            {
                if (_rotationCoroutine != null)
                {
                    StopCoroutine(_rotationCoroutine);
                    _rotationCoroutine = null;
                }

                CurrentState = State.Stopped;
            }
        }

        private void StartRotation()
        {
            if (CurrentState == State.Stopped)
            {
                _rotationCoroutine = StartCoroutine(RotatingRoutine());
            }
        }

        private IEnumerator RotatingRoutine()
        {
            CurrentState = State.Rotating;
            Time.fixedDeltaTime = 0.002f;

            _rotationSound?.Play();
            float currentSpeed = _initialSpeed;

            while (CurrentState == State.Rotating)
            {
                _currentAngle += currentSpeed * Time.fixedDeltaTime;
                SetRotation();
                yield return new WaitForFixedUpdate();
            }

            _rotationSound?.Stop();
            _stopingSound?.Play();
            float initialStoppingTimer = 0f;
            while (initialStoppingTimer < _stoppingTime)
            {
                currentSpeed = _initialSpeed *
                    _stoppingSpeedEvolutionCurve.Evaluate(initialStoppingTimer / _stoppingTime);
                _currentAngle += currentSpeed * Time.fixedDeltaTime;
                SetRotation();
                initialStoppingTimer += Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }

            float currentRotation = _desiredSector.rotation.eulerAngles.z % 360;
            float previousRotation = currentRotation;

            while (_rotationDirection == Direction.ClockWise ?
                currentRotation <= previousRotation :
                currentRotation >= previousRotation)
            {
                previousRotation = currentRotation;
                _currentAngle += currentSpeed * Time.fixedDeltaTime;
                SetRotation();
                currentRotation = _desiredSector.rotation.eulerAngles.z % 360;
                yield return new WaitForFixedUpdate();
            }

            float finalizingStoppingTimer = 0f;
            float initialOffset = _currentAngle;

            while (finalizingStoppingTimer < _finalizingTime)
            {
                _currentAngle = _finalizingAnglesEvolutionCurve.Evaluate(finalizingStoppingTimer / _finalizingTime) +
                    initialOffset;
                SetRotation();
                finalizingStoppingTimer += Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }

            _stopingSound?.Stop();
            Time.fixedDeltaTime = _initialDeltaTime;
            CurrentState = State.Stopped;
        }

        private void SetRotation()
        {
            foreach (Transform wheelComponent in _wheelComponents)
            {
                if (wheelComponent != null)
                {
                    wheelComponent.rotation = Quaternion.AngleAxis(_currentAngle, _rotationAxis);
                }
            }
        }
    }
}