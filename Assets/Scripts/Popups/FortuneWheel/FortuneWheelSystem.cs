﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DreamTeam.Debug;
using DreamTeam.GameControls;
using DreamTeam.KeyWordsReplacement;
using DreamTeam.Localization;
using DreamTeam.Notifications;
using DreamTeam.Popups.FortuneWheel.Items;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using Random = UnityEngine.Random;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam.Popups.FortuneWheel
{
    public class FortuneWheelSystem : MonoBehaviour, ISynchronizable, ILocalNotificationsProvider, IInitable
    {
        [Inject] private SaveGroup _saveGroup;
        [Inject] private GameControlsSystem _gameControlsSystem;
        [Inject] private CheatPanel _cheatPanel;
        [Inject] private NotificationsSystem _notificationsSystem;
        [Inject] private LocalizationSystem _localizationSystem;
        [Inject] private KeyWordsReplacementSystem _keyWordsReplacementSystem;

        #if !NO_MATCH3
        [Inject(Id = GlobalInstaller.MainLevelPackControllerId)] private SingleLevelPackController _levelPackController;
        #endif

        [Header("Basic")]
        [SerializeField] private int _wheelUnlockLevelIndex;
        [SerializeField] private GameControlsMode _fortuneWheelLockMode;
        [SerializeField] private FortuneWheelItemWeightBased _initialReward;
        [SerializeField] private List<FortuneWheelItemWeightBased> _itemsWeightBased = new List<FortuneWheelItemWeightBased>();
        [SerializeField] private List<FortuneWheelItemCountBased> _itemsCountBased = new List<FortuneWheelItemCountBased>();

        [Header("Notifications")]
        [SerializeField] private List<FortuneWheelNotification> _notifications;

        private bool _isInitialRewardGiven = false;
        private string _previousPopRewardDate = string.Empty;
        private List<string> _rewardsSequence = new List<string>();
        private bool _isUnlocked = false;

        private GameControlsContext _defaultContext;
        private bool _isJackPotsOnly = false;
        private Coroutine _wheelWaitingRoutine = null;
        private ActionDisposable _lockHandle;

        public Dictionary<FortuneWheelItemBase.Sectors, FortuneWheelItemBase> WheelElementsMap { get; private set; }

        public FortuneWheelItemBase PopNextReward()
        {
            FortuneWheelItemBase result = null;

            if (!_isInitialRewardGiven)
            {
                result = _initialReward;
                _isInitialRewardGiven = true;
            }

            if (result == null)
            {
                result = GetCountBasedReward();
            }

            if (result == null)
            {
                result = GetWeightBasedReward();
            }

            SetRewardLocked(true);

            DateTime currentTime = DateTime.Now;
            _previousPopRewardDate = currentTime.ToString(CultureInfo.InvariantCulture);
            _wheelWaitingRoutine = StartCoroutine(FortuneWheelWaitingRoutine(currentTime));

            Assert.IsNotNull(result, "[PROGRAMMERS] Fortune wheel was not able to generate next reward.");
            return result;
        }

        public void Init()
        {
            _saveGroup.RegisterSynchronizable(nameof(FortuneWheelSystem), this);
        }

        protected void Awake()
        {
            _cheatPanel.AddDrawer(CheatPanel.Category.FortuneWheel, DrawCheats);
            _notificationsSystem.RegisterLocalNotificationsProvider(this);
            _defaultContext = _gameControlsSystem.ActiveContext;

            WheelElementsMap = new Dictionary<FortuneWheelItemBase.Sectors, FortuneWheelItemBase>();
            foreach (FortuneWheelItemCountBased itemCountBased in _itemsCountBased)
            {
                WheelElementsMap.Add(itemCountBased.Sector, itemCountBased);
            }

            foreach (FortuneWheelItemWeightBased itemWeightBased in _itemsWeightBased)
            {
                WheelElementsMap.Add(itemWeightBased.Sector, itemWeightBased);
            }

            #if !NO_MATCH3
            {
                _isUnlocked = _levelPackController.CurrentLevelIndex >= _wheelUnlockLevelIndex;
            }
            #else
            {
                _isUnlocked = true;
            }
            #endif

            if (_isUnlocked)
            {
                if (string.IsNullOrEmpty(_previousPopRewardDate))
                {
                    SetRewardLocked(false);
                }
                else
                {
                    SetRewardLocked(true);
                    DateTime previousPopRewardDate = DateTime.Parse(_previousPopRewardDate, CultureInfo.InvariantCulture);
                    _wheelWaitingRoutine = StartCoroutine(FortuneWheelWaitingRoutine(previousPopRewardDate));
                }
            }
            else
            {
                SetRewardLocked(true);
            }
        }

        protected void OnDestroy()
        {
            ReleaseLockHandle();
            _cheatPanel.RemoveDrawer(DrawCheats);
            _notificationsSystem.UnRegisterLocalNotificationsProvider(this);
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncBool("_isInitialRewardGiven", ref _isInitialRewardGiven);
            state.SyncString("_previousPopRewardDate", ref _previousPopRewardDate, string.Empty);
            state.SyncStringList("_rewardsSequence", ref _rewardsSequence, true);
            state.SyncBool("_isUnlocked", ref _isUnlocked);

            foreach (FortuneWheelItemCountBased itemCountBased in _itemsCountBased)
            {
                itemCountBased.Sync(state);
            }
        }

        IEnumerable<NotificationsSystem.LocalNotificationParams> ILocalNotificationsProvider.Provide()
        {
            List<NotificationsSystem.LocalNotificationParams> result = new List<NotificationsSystem.LocalNotificationParams>();

            foreach (FortuneWheelNotification notification in _notifications)
            {
                string title = _keyWordsReplacementSystem.ReplaceKeyWords(_localizationSystem.Localize(notification.TitleKey, this));
                string message = _keyWordsReplacementSystem.ReplaceKeyWords(_localizationSystem.Localize(notification.MessageKey, this));

                NotificationsSystem.LocalNotificationParams parameters =
                    new NotificationsSystem.LocalNotificationParams
                    {
                        TriggerTime = GetNotificationTriggerTime(notification.Schedule),
                        Title = title,
                        Message = message,
                        SoundName = notification.SoundName
                    };

                result.Add(parameters);
            }

            return result;
        }

        private DateTime GetNotificationTriggerTime(TimeSpan notificationSpan)
        {
            return !_defaultContext.IsModeEnabled(_fortuneWheelLockMode) ?
                DateTime.Now + notificationSpan :
                DateTime.Today.AddDays(1).AddHours(DateTime.Now.Hour > 23 ? 1 : 0) + notificationSpan;
        }

        private IEnumerator FortuneWheelWaitingRoutine(DateTime previousPopRewardDate)
        {
            yield return new WaitUntil(() => DateTime.Now.Day > previousPopRewardDate.Day);
            DateTime currentDate = DateTime.Now;
            if (currentDate.Day - previousPopRewardDate.Day == 1 &&
                previousPopRewardDate.Hour > 23 &&
                currentDate.Hour == 0)
            {
                yield return new WaitUntil(() => DateTime.Now.Minute > previousPopRewardDate.Minute);
            }

            MakeWheelAvailable();
        }

        private void GenerateRewardsList()
        {
            foreach (FortuneWheelItemWeightBased itemWeightBased in _itemsWeightBased)
            {
                _rewardsSequence.AddRange(Enumerable.Repeat(itemWeightBased.name, itemWeightBased.Weight));
            }

            System.Random rnd = new System.Random();
            _rewardsSequence = _rewardsSequence.OrderBy(item => rnd.Next()).ToList();
        }

        private FortuneWheelItemCountBased GetCountBasedReward()
        {
            FortuneWheelItemCountBased result = null;
            if (_isJackPotsOnly)
            {
                result = _itemsCountBased[Random.Range(0, _itemsCountBased.Count - 1)];
            }
            else
            {
                foreach (FortuneWheelItemCountBased itemCountBased in _itemsCountBased)
                {
                    itemCountBased.Roll();
                }

                result = _itemsCountBased.FirstOrDefault(itemCountBased => itemCountBased.CanAcquire());
                if (result != null)
                {
                    result.Acquire();
                }
            }

            return result;
        }

        private FortuneWheelItemWeightBased GetWeightBasedReward()
        {
            if (_rewardsSequence.Count == 0)
            {
                GenerateRewardsList();
            }

            string nextRewardId = _rewardsSequence.First();
            _rewardsSequence.RemoveAt(0);

            return _itemsWeightBased.FirstOrDefault(itemWeightBased => itemWeightBased.name == nextRewardId);
        }

        private void MakeWheelAvailable()
        {
            _isUnlocked = true;
            _previousPopRewardDate = string.Empty;
            _wheelWaitingRoutine = null;

            SetRewardLocked(false);
        }

        private void SetRewardLocked(bool isLocked)
        {
            if (isLocked)
            {
                Assert.IsNull(_lockHandle, "_modeDisableHandle != null");
                _lockHandle = _defaultContext.EnableMode(_fortuneWheelLockMode);
            }
            else
            {
                ReleaseLockHandle();
            }
        }

        private void DrawCheats(Rect areaRect)
        {
            GUI.enabled = _wheelWaitingRoutine != null;
            if (GUILayout.Button("Make wheel available again"))
            {
                StopCoroutine(_wheelWaitingRoutine);
                MakeWheelAvailable();
            }
            GUI.enabled = true;

            GUILayout.BeginHorizontal();
            _isJackPotsOnly = GUILayout.Toggle(_isJackPotsOnly, _isJackPotsOnly ? "X" : "", GUI.skin.button, GUILayout.Width(GUI.skin.button.lineHeight));
            GUILayout.Label("Jack pots only");
            GUILayout.EndHorizontal();
        }

        private void ReleaseLockHandle()
        {
            _lockHandle?.Dispose();
            _lockHandle = null;
        }

        [Serializable]
        private struct FortuneWheelNotification
        {
            [SerializeField] public TimeTicks Schedule;
            [SerializeField, LocalizationKey] public string TitleKey;
            [SerializeField, LocalizationKey] public string MessageKey;
            [SerializeField] public string SoundName;
        }
    }
}