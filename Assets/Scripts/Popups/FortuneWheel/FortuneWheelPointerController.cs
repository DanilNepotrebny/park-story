﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Popups.FortuneWheel
{
    [ExecuteInEditMode]
    public class FortuneWheelPointerController : MonoBehaviour
    {
        [SerializeField] private Transform _lookAtTarget;

        private void Awake()
        {
            LookAt();
        }

        private void Update()
        {
            LookAt();
        }

        private void LookAt()
        {
            if (_lookAtTarget != null)
            {
                Vector3 diff = _lookAtTarget.position - transform.position;
                diff.Normalize();
                float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.Euler(0f, 0f, rot_z + 90);
            }
        }
    }
}