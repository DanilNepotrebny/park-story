﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups.FortuneWheel.Items;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Popups.FortuneWheel
{
    public class FortuneWheelItemSector : MonoBehaviour
    {
        [SerializeField] private FortuneWheelItemBase.Sectors _sector;
        [SerializeField] private Image _itemIcon;
        [SerializeField] private Text _itemText;

        public FortuneWheelItemBase.Sectors Sector => _sector;

        public void Initialize(FortuneWheelItemBase data)
        {
            _itemIcon.color = Color.white;
            _itemIcon.sprite = data.Icon;
            _itemText.text = data.Text;
        }
    }
}