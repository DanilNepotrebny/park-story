﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Cloud;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Popups.Cloud
{
    public class SynchronizationPopup : Popup
    {
        [SerializeField] private Button _useServerSaveButton;
        [SerializeField] private Button _useLocalSaveButton;

        [Header("Confirmation")] [SerializeField]
        private SceneReference _confirmationPopupScene;

        [SerializeField] private ConfirmStringPopup.Args _confirmationPopupArgs;

        [Inject] private PopupSystem _popupSystem;

        private ConflictPresenter.ResolveDelegate _resolveDelegate;

        public override bool Initialize(Popup.Args args)
        {
            if (!base.Initialize(args))
            {
                return false;
            }

            var popupArgs = args as Args;
            Assert.IsNotNull(popupArgs, $"Passed wrong popup args! Pass {nameof(Args)} instead");

            _resolveDelegate = popupArgs.ResolveDelegate;

            _useLocalSaveButton.onClick.AddListener(OnUseLocalButtonPressed);
            _useServerSaveButton.onClick.AddListener(OnUseServerButtonPressed);

            return true;
        }

        private void Resolve(bool useServer)
        {
            _resolveDelegate.Invoke(useServer);

            Popup popup = _popupSystem.GetPopup(gameObject.scene.name);
            if (popup != null && popup.IsShown)
            {
                _popupSystem.Hide(gameObject.scene.name);
            }
        }

        private void OnUseServerButtonPressed()
        {
            ShowResolveConfirmation(true);
        }

        private void OnUseLocalButtonPressed()
        {
            ShowResolveConfirmation(false);
        }

        private void ShowResolveConfirmation(bool useServer)
        {
            _confirmationPopupArgs.ValueAcceptedCallback = (value) => { Resolve(useServer); };
            _popupSystem.Show(_confirmationPopupScene.Name, _confirmationPopupArgs);
        }

        #region Inner classes

        public new class Args : Popup.Args
        {
            public ConflictPresenter.ResolveDelegate ResolveDelegate { get; set; }
        }

        #endregion
    }
}