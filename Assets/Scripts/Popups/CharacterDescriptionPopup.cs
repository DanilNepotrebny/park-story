// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.UI;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Popups
{
    [RequireComponent(typeof(PresenterHub))]
    public class CharacterDescriptionPopup : Popup
    {
        private Args _args;

        public override bool Initialize([CanBeNull] Popup.Args popupArgs)
        {
            base.Initialize(popupArgs);

            _args = popupArgs as Args;
            Assert.IsNotNull(_args);

            GetComponent<PresenterHub>().Initialize(_args.CharacterData);
            return true;
        }

        [Serializable]
        public new class Args : Popup.Args
        {
            [SerializeField] private CharacterData _characterData;
            public CharacterData CharacterData => _characterData;

            public Args()
            {
                // DO NOTHING
            }

            public Args(CharacterData characterData)
            {
                _characterData = characterData;
            }
        }
    }
}