﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Notifications;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Popups
{
    public class NotificationsInformationPopup : Popup
    {
        [SerializeField] private Button _enableNotificationsButton;
        [SerializeField] private SceneReference _informationPopup;
        [SerializeField] private InformationPopup.Args _notificationsDisabledPopupArgs;

        [Inject] private NotificationsSystem _notificationsSystem;
        [Inject] private PopupSystem _popupSystem;

        public override bool Initialize(Args popupArgs)
        {
            _enableNotificationsButton.onClick.AddListener(EnableNotifications);

            return base.Initialize(popupArgs);
        }

        protected override void OnHiding()
        {
            base.OnHiding();

            _enableNotificationsButton.onClick.RemoveListener(EnableNotifications);
        }

        private void EnableNotifications()
        {
            StartCoroutine(EnableNotificationsRoutine());
        }

        private IEnumerator EnableNotificationsRoutine()
        {
            if (!_notificationsSystem.AreNotificationsEnabled)
            {
                _enableNotificationsButton.interactable = false;

                if (!_notificationsSystem.AreNotificationsInitialized)
                {
                    yield return _notificationsSystem.Initialize();
                }
                else if (_notificationsSystem.AreNotificationsAllowed)
                {
                    _notificationsSystem.EnableLocalNotifications();
                }
                else
                {
                    _popupSystem.Show(_informationPopup.Name, _notificationsDisabledPopupArgs);
                }

                _enableNotificationsButton.interactable = true;
            }

            _popupSystem.Hide(SceneName);
        }
    }
}