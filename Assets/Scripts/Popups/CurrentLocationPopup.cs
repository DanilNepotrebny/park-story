﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location;
using DreamTeam.UI;
using Zenject;

namespace DreamTeam.Popups
{
    public class CurrentLocationPopup : Popup
    {
        [Inject] private LocationSystem _locationSystem;

        public override bool Initialize(Args popupArgs)
        {
            if (!base.Initialize(popupArgs))
            {
                return false;
            }

            var presenters = GetComponentsInChildren<IPresenter<LocationData>>();
            foreach (var presenter in presenters)
            {
                presenter.Initialize(_locationSystem.CurrentLocation);
            }

            return true;
        }
    }
}