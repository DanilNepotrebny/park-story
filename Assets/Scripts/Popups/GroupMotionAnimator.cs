﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Reflection;
using DreamTeam.Utils;
using DreamTeam.Utils.Movement;
using DreamTeam.Utils.UI;
using UnityEngine;
using Zenject;

namespace DreamTeam.Popups
{
    /// <summary>
    /// Plays MovementSequence animation
    /// </summary>
    public class GroupMotionAnimator : MonoBehaviour
    {
        [SerializeField] private MovementSequence _elementPrefab;
        [SerializeField] private int _elementsAmount;
        [SerializeField] private int _maxElementsEmount = LeanTween.maxSimulataneousTweens;
        [SerializeField] private float _elemetLaunchInterval;
        [Space, SerializeField] private Transform _startPoint;
        [SerializeField] private Transform _finishPoint;
        [SerializeField] private bool _isScaleApplied;
        [SerializeField] private GameObject _elementsParent;
        [SerializeField] private ElementsSiblingIndex _setElementsSiblingIndex = ElementsSiblingIndex.Last;

        [Inject] private Instantiator _instantiator;

        private int _movingElementsCount;
        private Vector3 _initialScale;
        private IEnumerator _launchElementsRoutine;

        public bool IsPlaying => _movingElementsCount > 0;
        public int ReachedElementsCount => _elementsAmount - _movingElementsCount;
        public int ElementsAmount => _elementsAmount;

        public Transform StartPoint
        {
            get { return _startPoint != null ? _startPoint.transform : transform; }
            set { _startPoint = value; }
        }

        public Transform FinishPoint
        {
            get { return _finishPoint != null ? _finishPoint.transform : transform; }
            set { _finishPoint = value; }
        }

        public event Action AnElementReached;
        public event Action MotionStarted;

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public event Action MotionFinished;

        public void SetElementsAmount(int amount)
        {
            _elementsAmount = Mathf.Clamp(amount, 1, _maxElementsEmount);
        }

        public void SetElementsParent(Transform transform)
        {
            _elementsParent = transform.gameObject;
        }

        public void Play()
        {
            _initialScale = _elementPrefab.transform.localScale;
            _movingElementsCount = _elementsAmount;

            if (_launchElementsRoutine != null)
            {
                StopCoroutine(_launchElementsRoutine);
            }

            _launchElementsRoutine = LaunchElements();
            StartCoroutine(_launchElementsRoutine);

            if (IsPlaying)
            {
                MotionStarted?.Invoke();
            }
        }

        protected void OnValidate()
        {
            _maxElementsEmount = Mathf.Clamp(_maxElementsEmount, 1, LeanTween.maxSimulataneousTweens);
            SetElementsAmount(_elementsAmount);
        }

        private IEnumerator LaunchElements()
        {
            for (int i = 0; i < _elementsAmount; ++i)
            {
                MovementSequence movSequence;
                if (_elementsParent != null)
                {
                    movSequence = _instantiator.Instantiate(_elementPrefab, _elementsParent);
                }
                else
                {
                    movSequence = _instantiator.Instantiate(_elementPrefab);
                }

                ResetElementRectTransform(movSequence);
                SetElementSiblingIndex(movSequence);
                movSequence.gameObject.SetActive(true);
                movSequence.Finished += OnMovementFinished;
                movSequence.Updated += OnMovementUpdated;
                movSequence.Move(StartPoint.position, FinishPoint.position, true);

                yield return new WaitForSeconds(_elemetLaunchInterval);
            }

            _launchElementsRoutine = null;
        }

        private void ResetElementRectTransform(MovementSequence sequence)
        {
            RectTransform sampleRect = _elementPrefab.GetComponent<RectTransform>();
            RectTransform sequenceRect = sequence.GetComponent<RectTransform>();

            if (sequenceRect != null && sampleRect != null)
            {
                new RectTransformData(sampleRect).ApplyToTransform(sequenceRect);
                sequenceRect.SetAnchorsToCenter();
            }
        }

        private void OnMovementUpdated(MovementSequence sequence, float progress)
        {
            if (_isScaleApplied)
            {
                sequence.gameObject.transform.localScale = Vector3.Lerp(
                    _initialScale,
                    FinishPoint.localScale,
                    progress);
            }
        }

        private void OnMovementFinished(MovementSequence mover)
        {
            _movingElementsCount -= 1;

            AnElementReached?.Invoke();

            if (!IsPlaying)
            {
                MotionFinished?.Invoke();
            }
        }

        private void SetElementSiblingIndex(MovementSequence element)
        {
            switch (_setElementsSiblingIndex)
            {
                case ElementsSiblingIndex.First:
                    element.transform.SetAsFirstSibling();
                    break;

                case ElementsSiblingIndex.Last:
                default:
                    element.transform.SetAsLastSibling();
                    break;
            }
        }

        private enum ElementsSiblingIndex
        {
            Last,
            First
        }
    }
}