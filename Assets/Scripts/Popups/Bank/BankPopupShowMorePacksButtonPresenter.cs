﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Inventory;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Popups.Bank
{
    public class BankPopupShowMorePacksButtonPresenter : MonoBehaviour
    {
        [SerializeField] private BankConfig _config;
        [SerializeField] private Button _button;

        [Inject] private Popup _popup;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private Instantiator _instantiator;

        private BankPopup _bankPopup => _popup as BankPopup;

        protected void OnEnable()
        {
            _button.onClick.AddListener(OnShowMorePacksButtonPressed);
        }

        protected void OnDisable()
        {
            _button.onClick.RemoveAllListeners();
        }

        private void OnShowMorePacksButtonPressed()
        {
            _analyticsSystem.Send(_instantiator.Instantiate<BankShowMoreEvent>());
            _bankPopup.ShowPacks(_config.All);
        }
    }
}
