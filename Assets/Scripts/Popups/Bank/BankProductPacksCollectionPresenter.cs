﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Inventory;
using DreamTeam.Popups.Destabilization;
using DreamTeam.UI;
using DreamTeam.UI.ProductPackPresenters;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Popups.Bank
{
    public class BankProductPacksCollectionPresenter : MonoBehaviour, IPresenter<ICollection<ProductPack>>, IPopupDestabilizer
    {
        [SerializeField, RequiredField] private BankConfig _config;
        [SerializeField, RequiredField] private ProductPackPresentersFactory _singleRewardFactory;
        [SerializeField, RequiredField] private ProductPackPresentersFactory _multipleRewardsFactory;
        [SerializeField, RequiredField] private SwitcherByScreenOrientation _multipleRewardsGroupsSwitcher;
        [SerializeField, RequiredField] private SwitcherByScreenOrientation _singleRewardGroupSwitcher;
        [SerializeField] private float _creationInterval = 0.1f;
        [SerializeField] private float _destroyInterval = 0.05f;
        [SerializeField] private float _destroyDuration = 0.05f;
        [SerializeField, RequiredField] private GameObject _showMorePacksPrefab;
        [SerializeField] private ScrollRect _horizontalScroll;
        [SerializeField] private ScrollRect _verticalScroll;

        [Inject] private Instantiator _instantiator;
        [Inject] private Popup _popup;

        private BankPopup _bankPopup => _popup as BankPopup;

        private List<ProductPack> _multipleRewardsProductPacks;
        private List<ProductPack> _singleRewardsProductPacks;
        private List<GameObject> _productObjectsList = new List<GameObject>();
        private GameObject _moreProductsButton = null;
        private ScreenOrientationNotifier _orientationNotifier;
        private Coroutine _presentationCoroutine;

        public void Initialize(ICollection<ProductPack> products)
        {
            _multipleRewardsProductPacks = products.Where(product => product.Rewards.Count > 1).ToList();
            _singleRewardsProductPacks = products.Where(product => product.Rewards.Count == 1).ToList();
        }

        public event Action Stabilized;
        public event Action Destabilized;

        public bool IsStable => _presentationCoroutine == null;

        protected void OnEnable()
        {
            _popup.Showing += OnPopupShowing;
            _popup.Hiding += OnPopupHiding;
            _popup.AddDestabilizer(this);

            _bankPopup.OnPacksReset += ResetProductsList;

            _orientationNotifier = gameObject.EnsureComponentExists<ScreenOrientationNotifier>(null);
            _orientationNotifier.OrientationChanged += OnOrientationChanged;
        }

        protected void OnDisable()
        {
            _popup.Showing -= OnPopupShowing;
            _popup.Hiding -= OnPopupHiding;
            _popup.RemoveDestabilizer(this);

            _bankPopup.OnPacksReset -= ResetProductsList;

            _orientationNotifier.OrientationChanged -= OnOrientationChanged;

            _productObjectsList.ForEach(item => item?.Dispose());
            _productObjectsList.Clear();
            _moreProductsButton?.Dispose();
        }

        private void OnPopupShowing(Popup popup)
        {
            CreateProductsLists();
        }

        private void OnPopupHiding(Popup obj)
        {
            DestroyProductsLists();
        }

        private void OnOrientationChanged(ScreenOrientationNotifier.Orientation orientation)
        {
            if (IsStable)
            {
                if (_singleRewardFactory != null && _multipleRewardsFactory != null)
                {
                    if (!ShouldCreateMoreProductsButton() && _moreProductsButton != null)
                    {
                        _moreProductsButton.Dispose();
                    }

                    if (ShouldCreateMoreProductsButton() && _moreProductsButton == null)
                    {
                        _moreProductsButton = CreateMoreProductsButton();
                    }
                }

                _horizontalScroll.horizontalNormalizedPosition = 0f;
                _verticalScroll.verticalNormalizedPosition = 1f;
            }
        }

        private void ResetProductsList()
        {
            Assert.IsNull(_presentationCoroutine);
            if (_presentationCoroutine == null)
            {
                _presentationCoroutine = StartCoroutine(ResetProductsListsInternal());
            }
        }

        private void CreateProductsLists()
        {
            Assert.IsNull(_presentationCoroutine);
            if (_presentationCoroutine == null)
            {
                _presentationCoroutine = StartCoroutine(CreateProductsListsInternal(1f, 0.5f));
            }
        }

        private void DestroyProductsLists()
        {
            Assert.IsNull(_presentationCoroutine);
            if (_presentationCoroutine == null)
            {
                _presentationCoroutine = StartCoroutine(DestroyProductsListsInternal());
            }
        }

        private IEnumerator ResetProductsListsInternal()
        {
            yield return DestroyProductsListsInternal();
            yield return CreateProductsListsInternal(1f, 0f);
        }

        private IEnumerator CreateProductsListsInternal(float verticalScroll, float horizontalScroll)
        {
            Destabilized?.Invoke();

            Assert.IsTrue(_singleRewardFactory != null && _multipleRewardsFactory != null);
            if (_singleRewardFactory != null && _multipleRewardsFactory != null)
            {
                foreach (ProductPack productData in _multipleRewardsProductPacks)
                {
                    GameObject productObject = _multipleRewardsFactory.Create(productData, _instantiator, _multipleRewardsGroupsSwitcher.Current.transform, false);
                    _productObjectsList.Add(productObject);
                }

                foreach (ProductPack productData in _singleRewardsProductPacks)
                {
                    GameObject productObject = _singleRewardFactory.Create(productData, _instantiator, _singleRewardGroupSwitcher.Current.transform, false);
                    _productObjectsList.Add(productObject);
                }

                yield return null;

                _horizontalScroll.horizontalNormalizedPosition = horizontalScroll;
                _verticalScroll.verticalNormalizedPosition = verticalScroll;

                foreach (GameObject productObject in _productObjectsList)
                {
                    ISimpleAnimationPlayer productAnimation = productObject.GetComponent<ISimpleAnimationPlayer>();
                    productAnimation.Play(SimpleAnimationType.Show);

                    yield return new WaitForSeconds(_creationInterval);
                }

                if (ShouldCreateMoreProductsButton())
                {
                    _moreProductsButton = CreateMoreProductsButton();
                    yield return new WaitForSeconds(_creationInterval);
                }
            }

            _presentationCoroutine = null;

            Stabilized?.Invoke();
        }

        private IEnumerator DestroyProductsListsInternal()
        {
            Destabilized?.Invoke();

            foreach (GameObject productObject in _productObjectsList)
            {
                ISimpleAnimationPlayer productAnimation = productObject.GetComponent<ISimpleAnimationPlayer>();
                productAnimation.Play(SimpleAnimationType.Hide);

                yield return new WaitForSeconds(_destroyInterval);
            }

            if (_moreProductsButton != null)
            {
                ISimpleAnimationPlayer buttonAnimation = _moreProductsButton.GetComponent<ISimpleAnimationPlayer>();
                buttonAnimation.Play(SimpleAnimationType.Hide);

                yield return new WaitForSeconds(_destroyInterval);
            }

            yield return new WaitForSeconds(_destroyDuration);

            _productObjectsList.ForEach(item => item?.Dispose());
            _productObjectsList.Clear();
            _moreProductsButton?.Dispose();

            _presentationCoroutine = null;

            Stabilized?.Invoke();
        }

        private bool ShouldCreateMoreProductsButton()
        {
            return
                _orientationNotifier.CurrentOrientation == ScreenOrientationNotifier.Orientation.Landscape &&
                _multipleRewardsProductPacks.Count + _singleRewardsProductPacks.Count < _config.All.Count;
        }

        private GameObject CreateMoreProductsButton()
        {
            GameObject moreProductsButton = _instantiator.Instantiate(_showMorePacksPrefab);
            moreProductsButton.transform.SetParent(_singleRewardGroupSwitcher.Current.transform, false);

            ISimpleAnimationPlayer buttonAnimation = moreProductsButton.GetComponent<ISimpleAnimationPlayer>();
            buttonAnimation.Play(SimpleAnimationType.Show);

            return moreProductsButton;
        }
    }
}