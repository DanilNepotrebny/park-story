﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Inventory;
using DreamTeam.UI;
using UnityEngine;

namespace DreamTeam.Popups.Bank
{
    public class BankPopup : Popup
    {
        [Header("Bank popup")]
        [SerializeField] private BankConfig _config;

        private Args _popupArgs;

        public event Action OnPacksReset;

        public override bool Initialize(Popup.Args popupArgs)
        {
            base.Initialize(popupArgs);

            _popupArgs = popupArgs as Args;

            if (_popupArgs != null)
            {
                switch (_popupArgs.CollectionType)
                {
                    case Args.PacksCollectionType.Default:
                        InitializeWithPacks(_config.Default);
                        break;

                    case Args.PacksCollectionType.All:
                        InitializeWithPacks(_config.All);
                        break;

                    case Args.PacksCollectionType.Custom:
                        if (_popupArgs.CustomPacksList != null)
                        {
                            InitializeWithPacks(_popupArgs.CustomPacksList);
                        }

                        break;
                }
            }
            else
            {
                InitializeWithPacks(_config.Default);
            }

            return true;
        }

        public void ShowPacks(IReadOnlyList<ProductPack> packs)
        {
            InitializeWithPacks(packs);
            OnPacksReset?.Invoke();
        }

        private void InitializeWithPacks(IReadOnlyList<ProductPack> packs)
        {
            PresenterHub hub = GetComponent<PresenterHub>();
            if (hub != null)
            {
                hub.Initialize(packs);
            }
        }

        #region Inner classes

        public new class Args : Popup.Args
        {
            public enum PacksCollectionType
            {
                Default,
                All,
                Custom
            }

            public PacksCollectionType CollectionType { get; set; }
            public List<ProductPack> CustomPacksList { get; set; }
        }

        #endregion
    }
}