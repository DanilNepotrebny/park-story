﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Inventory;
using DreamTeam.UI;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Popups.Bank
{
    public class BankPopupShowMorePacksStaticButtonPresenter : MonoBehaviour, IPresenter<ICollection<ProductPack>>
    {
        [SerializeField] private BankConfig _config;
        [SerializeField] private Button _button;

        [Inject] private Popup _popup;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private Instantiator _instantiator;

        private BankPopup _bankPopup => _popup as BankPopup;

        public void Initialize(ICollection<ProductPack> packs)
        {
            if (gameObject.activeInHierarchy)
            {
                gameObject.SetActive(packs.Count < _config.All.Count);
            }
        }

        protected void OnEnable()
        {
            _button.onClick.AddListener(OnShowMorePacksButtonPressed);
        }

        protected void OnDisable()
        {
            _button.onClick.RemoveAllListeners();
        }

        private void OnShowMorePacksButtonPressed()
        {
            _analyticsSystem.Send(_instantiator.Instantiate<BankShowMoreEvent>());
            _bankPopup.ShowPacks(_config.All);
        }
    }
}
