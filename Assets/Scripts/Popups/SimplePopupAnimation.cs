﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI.SimpleAnimating;
using UnityEngine;
using Zenject;

namespace DreamTeam.Popups
{
    [RequireComponent(typeof(SimpleAnimation))]
    public class SimplePopupAnimation : MonoBehaviour
    {
        [Inject] private Popup _popup;

        private SimpleAnimation _animation => GetComponent<SimpleAnimation>();

        protected void Awake()
        {
            _popup.Showing += OnPopupShowing;
            _popup.Hiding += OnPopupHiding;
        }

        protected void OnDestroy()
        {
            _popup.Showing -= OnPopupShowing;
            _popup.Hiding -= OnPopupHiding;
        }

        private void OnPopupHiding(Popup popup)
        {
            _animation.Play(SimpleAnimationType.Hide);
        }

        private void OnPopupShowing(Popup popup)
        {
            _animation.Play(SimpleAnimationType.Show);
        }
    }
}