﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Localization;
using TMPro;
using UnityEngine;
using Zenject;

namespace DreamTeam.Popups
{
    public class InformationPopup : Popup
    {
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _message;

        [Inject] private LocalizationSystem _localizationSystem;

        public override bool Initialize(Popup.Args popupArgs)
        {
            Args args = (Args)popupArgs;
            _title.text = _localizationSystem.Localize(args.TitleKey, this);
            _message.text = _localizationSystem.Localize(args.MessageKey, this);

            return base.Initialize(popupArgs);
        }

        [Serializable]
        public new class Args : Popup.Args
        {
            [SerializeField, LocalizationKey] private string _titleKey;
            [SerializeField, LocalizationKey] private string _messageKey;

            public string TitleKey
            {
                get { return _titleKey; }
                set { _titleKey = value; }
            }

            public string MessageKey
            {
                get { return _messageKey; }
                set { _messageKey = value; }
            }
        }
    }
}