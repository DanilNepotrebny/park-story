﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Inventory.Prices;
using DreamTeam.Inventory.Rewards;
using DreamTeam.UI;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Popups
{
    public class SingleCountedItemRewardPurchasePopup : ProductPackPurchasePopup
    {
        [SerializeField] protected GamePlace _gamePlace;

        protected CountedItemPrice _price;
        protected CountedItemReward _reward;

        protected override GamePlace GamePlace => _gamePlace;

        public override bool Initialize(Popup.Args popupArgs)
        {
            base.Initialize(popupArgs);

            var args = popupArgs as ProductPackPurchasePopup.Args;
            Assert.IsNotNull(args, $"Passed wrong popup args! Pass {nameof(ProductPackPurchasePopup.Args)} instead");

            if (args.GamePlace != GamePlace.Unknown)
            {
                _gamePlace = args.GamePlace;
            }

            _price = (CountedItemPrice)args.ProductPack.Price;
            Assert.IsNotNull(_price, $"{nameof(ProductPack)} price is not {nameof(CountedItemPrice)}!");

            Assert.IsTrue(
                args.ProductPack.Rewards.Count == 1,
                "Multiple rewards in pack. This popup works only with one reward!");
            _reward = (CountedItemReward)args.ProductPack.Rewards[0];
            Assert.IsNotNull(_reward, $"{nameof(ProductPack)} reward is not {nameof(CountedItemReward)}!");

            GetComponent<PresenterHub>()?.Initialize(_reward);

            return true;
        }

        #region Inner types

        public new class Args : ProductPackPurchasePopup.Args
        {
        }

        #endregion Inner types
    }
}