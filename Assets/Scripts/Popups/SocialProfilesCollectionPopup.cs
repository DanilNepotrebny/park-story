﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Inventory.Rewards;
using DreamTeam.InviteFriends;
using DreamTeam.SocialNetworking;
using DreamTeam.UI;
using UnityEngine;
using Zenject;

namespace DreamTeam.Popups
{
    [RequireComponent(typeof(PresenterHub))]
    public class SocialProfilesCollectionPopup : Popup
    {
        [Inject] private SocialProfileInventoryExchangeSystem _exchangeSystem;
        [Inject] private SocialProfilesSystem _socialProfilesSystem;
        [Inject] private InviteFriendsSystem _inviteFriendsSystem;

        private Args _args;
        private List<SocialProfile> _filteredProfiles = new List<SocialProfile>();
        private PresenterHub _presenterHub => GetComponent<PresenterHub>();
        private bool _wasSocialActionPerformed = false;

        public override bool Initialize(Popup.Args popupArgs)
        {
            if (base.Initialize(popupArgs))
            {
                _args = popupArgs as Args;
                if (_args != null)
                {
                    Hidden += OnHidden;

                    InitializePresenterHub();
                }
                return _args != null;
            }
            return false;
        }

        protected override void OnShowing()
        {
            base.OnShowing();

            _exchangeSystem.ProfileRegistered += OnProfileRegistered;
            _exchangeSystem.ProfileUnregistered += OnProfileUnregistered;
            _exchangeSystem.RewardsAsked += OnRewardsExchanged;
            _exchangeSystem.RewardsSent += OnRewardsExchanged;
        }

        protected override void OnHiding()
        {
            base.OnHiding();

            _exchangeSystem.ProfileRegistered -= OnProfileRegistered;
            _exchangeSystem.ProfileUnregistered -= OnProfileUnregistered;
            _exchangeSystem.RewardsAsked -= OnRewardsExchanged;
            _exchangeSystem.RewardsSent -= OnRewardsExchanged;
        }

        private void OnRewardsExchanged(SocialProfile profile, List<Reward> rewards)
        {
            _wasSocialActionPerformed = true;

            InitializePresenterHub();
        }

        private void InitializePresenterHub()
        {
            _filteredProfiles.Clear();
            _filteredProfiles.AddRange(_socialProfilesSystem.SocialProfiles.Where(item => _args.FilterCondition.Invoke(item)));
            _presenterHub.Initialize(_filteredProfiles);
        }

        private void OnProfileRegistered(SocialProfile profile)
        {
            if (_args.FilterCondition.Invoke(profile))
            {
                _filteredProfiles.Add(profile);
                _presenterHub.Initialize(_filteredProfiles);
            }
        }

        private void OnProfileUnregistered(SocialProfile profile)
        {
            if (_filteredProfiles.Contains(profile))
            {
                _filteredProfiles.Remove(profile);
                _presenterHub.Initialize(_filteredProfiles);
            }
        }

        private void OnHidden(Popup self)
        {
            Hidden -= OnHidden;

            if (!_wasSocialActionPerformed && _inviteFriendsSystem.CanShowInvitePopup())
            {
                _inviteFriendsSystem.EnqueueInvitePopup();
            }
        }

        public new class Args : Popup.Args
        {
            public Predicate<SocialProfile> FilterCondition;
        }
    }
}