﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Popups
{
    public class BoosterTipPopup : SingleCountedItemRewardPurchasePopup
    {
        [SerializeField] private Button _useExistingBoosterButton;
        [SerializeField] private Text _multiplicationTextComponent;

        [Inject] private EquippedChipBoosters _equippedChipBoosters;
        [Inject] private PopupSystem _popupSystem;

        private ChipBooster _chipBooster;
        private Args _args;

        public override bool Initialize(Popup.Args popupArgs)
        {
            if (!base.Initialize(popupArgs))
            {
                return false;
            }

            _args = popupArgs as Args;
            Assert.IsNotNull(_args, $"Passed wrong popup args! Pass {nameof(Args)} instead");

            _chipBooster = _reward.Item as ChipBooster;
            Assert.IsNotNull(_chipBooster, $"Passed wrong popup args! Reward item is not {nameof(ChipBooster)}");

            if (_chipBooster.Count > 0)
            {
                _useExistingBoosterButton.onClick.RemoveAllListeners();
                _useExistingBoosterButton.onClick.AddListener(OnUseExistingBoosterButtonClicked);

                PurchaseButton.gameObject.SetActive(false);
                _useExistingBoosterButton.gameObject.SetActive(true);
                _multiplicationTextComponent.gameObject.SetActive(true);
            }
            else
            {
                _args.PurchaseResultCallback += OnPurchaseComplete;
                PurchaseButton.gameObject.SetActive(true);
                _useExistingBoosterButton.gameObject.SetActive(false);
                _multiplicationTextComponent.gameObject.SetActive(false);
            }

            return true;
        }

        private void OnPurchaseComplete(bool isPurchased)
        {
            if (isPurchased)
            {
                _equippedChipBoosters.Equip(_chipBooster);
                _args.OnBoosterPicked?.Invoke();
            }
        }

        private void OnUseExistingBoosterButtonClicked()
        {
            _equippedChipBoosters.Equip(_chipBooster);
            _args.OnBoosterPicked?.Invoke(); // must be right after booster equip

            _popupSystem.Hide(gameObject.scene.name);
        }

        #region Inner types

        public new class Args : SingleCountedItemRewardPurchasePopup.Args
        {
            public Action OnBoosterPicked;
        }

        #endregion Inner types
    }
}
