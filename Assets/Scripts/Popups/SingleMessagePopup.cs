﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Messaging;
using DreamTeam.UI;
using UnityEngine;
using Zenject;

namespace DreamTeam.Popups
{
    [RequireComponent(typeof(PresenterHub))]
    public class SingleMessagePopup : Popup
    {
        [Inject] private MessageSystem _messageSystem;
        [Inject] private PopupSystem _popupSystem;

        private Args _args;

        public override bool Initialize(Popup.Args popupArgs)
        {
            if (base.Initialize(popupArgs))
            {
                _args = popupArgs as Args;
                GetComponent<PresenterHub>().Initialize(_args.Message);
                return _args != null;
            }
            return false;
        }

        protected override void OnShowing()
        {
            base.OnShowing();

            _messageSystem.MessageRemoved += OnMessageRemoved;
        }

        protected override void OnHiding()
        {
            base.OnHiding();

            _messageSystem.MessageRemoved -= OnMessageRemoved;
        }

        private void OnMessageRemoved(IMessage message)
        {
            if (_args.Message == message)
            {
                _popupSystem.Hide(SceneName);
            }
        }

        public new class Args : Popup.Args
        {
            public IMessage Message { get; set; }
        }
    }
}