﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.UI;
using UnityEngine.Assertions;

namespace DreamTeam.Popups
{
    public class CountedItemPopup : Popup
    {
        public override bool Initialize(Popup.Args args)
        {
            if (!base.Initialize(args))
            {
                return false;
            }

            Args popupArgs = args as Args;
            Assert.IsNotNull(popupArgs, $"Passed wrong popup args! Pass {nameof(Args)} instead");

            GetComponent<PresenterHub>()?.Initialize(popupArgs.CountedItem);
            return true;
        }

        #region Inner classes

        public new class Args : Popup.Args
        {
            public CountedItem CountedItem { get; set; }
        }

        #endregion
    }
}