﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Analytics;
using DreamTeam.Inventory;
using DreamTeam.UI;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Popups
{
    public abstract class ProductPackPurchasePopup : Popup
    {
        [SerializeField] private Button _purchaseButton;

        [Inject] private PopupSystem _popupSystem;
        [Inject] private ProductPackInstanceContainer _productPackInstanceContainer;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private Instantiator _instantiator;

        private Args _popupArgs;

        private ProductPack.Instance _productPackInstance;

        protected Button PurchaseButton => _purchaseButton;

        protected bool IsPurchased { get; private set; }

        protected virtual GamePlace GamePlace => _popupArgs.GamePlace;

        public override bool Initialize(Popup.Args popupArgs)
        {
            base.Initialize(popupArgs);

            IsPurchased = false;

            _popupArgs = popupArgs as Args;
            Assert.IsNotNull(_popupArgs, $"Passed wrong popup args! Pass {nameof(Args)} instead");
            Assert.IsNotNull(_popupArgs.ProductPack, $"Passed wrong popup args! {nameof(Args.ProductPack)} is null");

            _productPackInstance = _productPackInstanceContainer.GetInstance(
                    _popupArgs.ProductPack
                );
            _productPackInstance.Given += OnProductPackGiven;

            _purchaseButton.onClick.RemoveAllListeners();
            _purchaseButton.onClick.AddListener(
                    () => { _productPackInstance.TryPurchase(_popupArgs.GamePlace); }
                );

            GetComponent<PresenterHub>()?.Initialize(_popupArgs.ProductPack);

            Hidden += OnHidden;
            Shown += OnShown;

            return true;
        }

        private void OnShown(Popup popup)
        {
            IAnalyticsEvent purchaseOfferedEvent = _productPackInstance.CreatePurchaseOfferedEvent(
                    _instantiator,
                    GamePlace
                );
            if (purchaseOfferedEvent != null)
            {
                _analyticsSystem.Send(purchaseOfferedEvent);
            }
        }

        protected virtual void OnProductPackGiven(ProductPack.Instance pack, GamePlace place)
        {
            IsPurchased = true;

            _purchaseButton.onClick.RemoveAllListeners();
            _popupSystem.Hide(gameObject.scene.name);
        }

        private void OnHidden(Popup popup)
        {
            _productPackInstance.Given -= OnProductPackGiven;

            IAnalyticsEvent purchaseResultEvent = _productPackInstance.CreatePurchaseResultEvent(
                    _instantiator,
                    _popupArgs.GamePlace,
                    IsPurchased ? PurchaseDecision.Purchase : PurchaseDecision.Close
                );
            if (purchaseResultEvent != null)
            {
                _analyticsSystem.Send(purchaseResultEvent);
            }

            Hidden -= OnHidden;
            _popupArgs.PurchaseResultCallback?.Invoke(IsPurchased);
        }

        #region Inner classes

        public new class Args : Popup.Args
        {
            public ProductPack ProductPack { get; set; }
            public Action<bool> PurchaseResultCallback { get; set; }
            public GamePlace GamePlace { get; set; }
        }

        #endregion
    }
}