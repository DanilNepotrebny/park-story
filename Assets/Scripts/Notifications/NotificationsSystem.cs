﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.Debug;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using UTNotifications;
using Zenject;

namespace DreamTeam.Notifications
{
    public class NotificationsSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private TimeTicks _morningTime;
        [SerializeField] private TimeTicks _eveningTime;

        [Inject] private SaveManager _saveManager;
        [Inject] private CheatPanel _cheatPanel;

        private Manager _notificationsManager;

        private const string _silentSoundProfile = "silent";
        private bool _arePermissionsAsked = false;
        private bool _areNotificationsEnabled = false;
        private bool _isNotificationsLogEnabled = false;

        private List<ILocalNotificationsProvider>
            _localNotificationsProviders = new List<ILocalNotificationsProvider>();

        public bool AreNotificationsInitialized => _notificationsManager.Initialized;
        public bool AreNotificationsAllowed => _notificationsManager.NotificationsAllowed();

        public bool AreNotificationsEnabled =>
            _areNotificationsEnabled && _notificationsManager.NotificationsEnabled();

        public event Action<ReceivedNotification> LocalNotificationClickedEvent;
        public event Action<ReceivedNotification> LocalNotificationReceivedEvent;
        public event Action<bool> NotificationsStateChanged;

        public IEnumerator Initialize()
        {
            if (!AreNotificationsInitialized)
            {
                _notificationsManager.OnNotificationClicked += OnLocalNotificationClicked;
                _notificationsManager.OnNotificationsReceived += OnLocalNotificationsReceived;
                _notificationsManager.Initialize(true, 0, true);

                while (!_notificationsManager.Initialized)
                {
                    yield return null;
                }

                yield return new WaitForSeconds(1f);

                if (_arePermissionsAsked)
                {
                    SetNotificationsEnabled(_areNotificationsEnabled);
                }
                else
                {
                    _arePermissionsAsked = true;
                    SetNotificationsEnabled(AreNotificationsAllowed);
                }

                _notificationsManager.SetBadge(0);
            }
        }

        public void EnableLocalNotifications()
        {
            if (AreNotificationsInitialized &&
                AreNotificationsAllowed &&
                !AreNotificationsEnabled)
            {
                SetNotificationsEnabled(true);
            }
        }

        public void DisableLocalNotifications()
        {
            if (AreNotificationsInitialized &&
                AreNotificationsAllowed &&
                AreNotificationsEnabled)
            {
                SetNotificationsEnabled(false);
            }
        }

        public void RegisterLocalNotificationsProvider(ILocalNotificationsProvider provider)
        {
            if (!_localNotificationsProviders.Contains(provider))
            {
                _localNotificationsProviders.Add(provider);
            }
        }

        public void UnRegisterLocalNotificationsProvider(ILocalNotificationsProvider provider)
        {
            int providerIndex = _localNotificationsProviders.IndexOf(provider);
            if (providerIndex >= 0)
            {
                _localNotificationsProviders.RemoveAt(providerIndex);
            }
        }

        public void Init()
        {
            SaveGroup settingsSaveGroup = _saveManager.GetSaveGroup(SaveManager.Group.Settings);
            settingsSaveGroup.RegisterSynchronizable(nameof(NotificationsSystem), this);
            settingsSaveGroup.Loaded += OnSaveDataLoaded;

            if (_cheatPanel != null)
            {
                _cheatPanel.AddDrawer(CheatPanel.Category.LocalNotifications, DrawCheatsGUI);
            }

            _notificationsManager = Manager.Instance;
        }

        protected void OnDestroy()
        {
            if (_cheatPanel != null)
            {
                _cheatPanel.RemoveDrawer(DrawCheatsGUI);
            }
        }

        protected void OnApplicationFocus(bool isFocused)
        {
            if (AreNotificationsInitialized &&
                AreNotificationsAllowed &&
                AreNotificationsEnabled)
            {
                if (isFocused)
                {
                    CancelLocalNotifications();
                }
                else
                {
                    ScheduleLocalNotifications();
                }
            }
        }

        private void SetNotificationsEnabled(bool areNotificationsEnabled)
        {
            _areNotificationsEnabled = areNotificationsEnabled;
            _notificationsManager.SetNotificationsEnabled(areNotificationsEnabled);
            NotificationsStateChanged?.Invoke(areNotificationsEnabled);
        }

        private void OnSaveDataLoaded()
        {
            if (_arePermissionsAsked)
            {
                StartCoroutine(Initialize());
            }
        }

        private void ScheduleLocalNotifications()
        {
            List<LocalNotificationParams> parameters = new List<LocalNotificationParams>();
            foreach (ILocalNotificationsProvider provider in _localNotificationsProviders)
            {
                parameters.AddRange(provider.Provide());
            }

            parameters.Sort((x, y) => DateTime.Compare(x.TriggerTime, y.TriggerTime));

            for (int i = 0; i < parameters.Count; i++)
            {
                LocalNotificationParams parameter = parameters[i];

                if (IsNightTime(parameter.TriggerTime.TimeOfDay))
                {
                    parameter.SoundName = _silentSoundProfile;
                }

                if (_isNotificationsLogEnabled)
                {
                    UnityEngine.Debug.Log($"Notification scheduled: " +
                        $"Title: {parameter.Title}, " +
                        $"Message: {parameter.Message}, " +
                        $"Trigger time: {parameter.TriggerTime}, " +
                        $"Has sound: {!IsNightTime(parameter.TriggerTime.TimeOfDay)}");
                }

                _notificationsManager.ScheduleNotification(
                        parameter.TriggerTime,
                        parameter.Title,
                        parameter.Message,
                        i,
                        parameter.UserData,
                        parameter.SoundName,
                        i + 1
                    );
            }
        }

        private void CancelLocalNotifications()
        {
            _notificationsManager.SetBadge(0);
            _notificationsManager.CancelAllNotifications();
        }

        private void OnLocalNotificationClicked(ReceivedNotification notification)
        {
            LocalNotificationClickedEvent?.Invoke(notification);

            _notificationsManager.HideNotification(notification.id);
        }

        private void OnLocalNotificationsReceived(IList<ReceivedNotification> receivedNotifications)
        {
            foreach (ReceivedNotification notification in receivedNotifications)
            {
                LocalNotificationReceivedEvent?.Invoke(notification);

                _notificationsManager.HideNotification(notification.id);
            }
        }

        private bool IsNightTime(TimeSpan timeOfDay)
        {
            return timeOfDay < _morningTime || timeOfDay > _eveningTime;
        }

        private void DrawCheatsGUI(Rect areaRect)
        {
            GUI.enabled = false;

            GUILayout.BeginHorizontal();
            GUILayout.Toggle(AreNotificationsInitialized, AreNotificationsInitialized ? "X" : "", GUI.skin.button, GUILayout.Width(GUI.skin.button.lineHeight));
            GUILayout.Label("Are notifications initialized?");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Toggle(AreNotificationsAllowed, AreNotificationsAllowed ? "X" : "", GUI.skin.button, GUILayout.Width(GUI.skin.button.lineHeight));
            GUILayout.Label("Are notifications allowed?");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Toggle(AreNotificationsEnabled, AreNotificationsEnabled ? "X" : "", GUI.skin.button, GUILayout.Width(GUI.skin.button.lineHeight));
            GUILayout.Label("Are notifications enabled?");
            GUILayout.EndHorizontal();

            GUI.enabled = AreNotificationsInitialized && AreNotificationsAllowed && AreNotificationsEnabled;

            GUILayout.BeginHorizontal();
            _isNotificationsLogEnabled = GUILayout.Toggle(_isNotificationsLogEnabled, _isNotificationsLogEnabled ? "X" : "", GUI.skin.button, GUILayout.Width(GUI.skin.button.lineHeight));
            GUILayout.Label("Is notifications log enabled");
            GUILayout.EndHorizontal();

            GUI.enabled = true;
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncBool("_arePermissionsAsked", ref _arePermissionsAsked);
            state.SyncBool("_areLocalNotificationsEnabled", ref _areNotificationsEnabled);
        }

        public struct LocalNotificationParams
        {
            public DateTime TriggerTime;
            public string Title;
            public string Message;
            public Dictionary<string, string> UserData;
            public string SoundName;
        }
    }
}