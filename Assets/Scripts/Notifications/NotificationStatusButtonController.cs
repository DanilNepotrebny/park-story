﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Localization;
using DreamTeam.Popups;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Notifications
{
    [RequireComponent(typeof(Button))]
    public class NotificationStatusButtonController : MonoBehaviour
    {
        [SerializeField] private Text _buttonText;
        [SerializeField, LocalizationKey] private string _notificaionsOnTextKey;
        [SerializeField, LocalizationKey] private string _notificaionsOffTextKey;
        [SerializeField] private SceneReference _informationPopup;
        [SerializeField] private InformationPopup.Args _notificationsDisabledPopupArgs;

        [Inject] private NotificationsSystem _notificationsSystem;
        [Inject] private LocalizationSystem _localizationSystem;
        [Inject] private PopupSystem _popupSystem;

        private Button _notificationsEnabledButton;

        protected void OnEnable()
        {
            _notificationsEnabledButton = GetComponent<Button>();
            _notificationsEnabledButton.onClick.AddListener(OnButtonClicked);

            UpdateButtonText();
        }

        protected void OnDisable()
        {
            _notificationsEnabledButton.onClick.RemoveListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            StartCoroutine(ButtonClickRoutine());
        }

        private void UpdateButtonText()
        {            
            if (_notificationsSystem.AreNotificationsInitialized &&
                _notificationsSystem.AreNotificationsAllowed &&
                _notificationsSystem.AreNotificationsEnabled)
            {
                _buttonText.text = _localizationSystem.Localize(_notificaionsOnTextKey, this);
            }
            else
            {
                _buttonText.text = _localizationSystem.Localize(_notificaionsOffTextKey, this);
            }
        }

        private IEnumerator ButtonClickRoutine()
        {
            _notificationsEnabledButton.interactable = false;
            
            if (!_notificationsSystem.AreNotificationsInitialized)
            {
                yield return _notificationsSystem.Initialize();
            }
            else if (_notificationsSystem.AreNotificationsAllowed)
            {
                if (_notificationsSystem.AreNotificationsEnabled)
                {
                    _notificationsSystem.DisableLocalNotifications();
                }
                else
                {
                    _notificationsSystem.EnableLocalNotifications();
                }
            }
            else
            {
                _popupSystem.Show(_informationPopup.Name, _notificationsDisabledPopupArgs);
            }

            UpdateButtonText();
            
            _notificationsEnabledButton.interactable = true;
        }
    }
}