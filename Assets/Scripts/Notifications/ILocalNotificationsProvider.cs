﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;

namespace DreamTeam.Notifications
{
    public interface ILocalNotificationsProvider
    {
        IEnumerable<NotificationsSystem.LocalNotificationParams> Provide();
    }
}