﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;

namespace DreamTeam.AssetBundles
{
    public class Manifest : ISynchronizable
    {
        private Dictionary<string, Descriptor> _descriptors = new Dictionary<string, Descriptor>();

        public IEnumerable<string> BundleNames => _descriptors.Keys;

        public Descriptor GetDescriptor(string bundleName)
        {
            return _descriptors[bundleName];
        }

        public void AddDescriptor(string bundleName, Descriptor descriptor)
        {
            _descriptors.Add(bundleName, descriptor);
        }

        public List<string> GetAllDependencies(string bundleName)
        {
            List<string> dependencies = GetDescriptor(bundleName).Dependencies.ToList();

            for (int i = 0; i < dependencies.Count; i++)
            {
                string name = dependencies[i];

                foreach (string dependency in GetDescriptor(name).Dependencies)
                {
                    if (!dependencies.Contains(dependency))
                    {
                        dependencies.Add(dependency);
                    }
                }
            }

            return dependencies;
        }

        public void Sync(State state)
        {
            state.SyncObjectDictionary("descriptors", ref _descriptors, () => new Descriptor());
        }
    }
}