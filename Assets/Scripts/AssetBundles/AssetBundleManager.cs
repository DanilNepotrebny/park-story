﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.IO;
using System.Linq;
using DreamTeam.Synchronization.Nodes;
using DreamTeam.Synchronization.Serializers;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.ResourceLoading;
using UnityEngine;
using Zenject;

namespace DreamTeam.AssetBundles
{
    public partial class AssetBundleManager : MonoBehaviour
    {
        [SerializeField] private string _packagedBundlesFolder;
        [SerializeField] private string _manifestFileName;
        [SerializeField] private string _remoteBaseUrl;

        [Inject] private Instantiator _instantiator;

        private Dictionary<string, IBundle> _bundles = new Dictionary<string, IBundle>();
        private Manifest _manifest = new Manifest();

        public string PackagedBundlesPath => Path.Combine(Application.streamingAssetsPath, _packagedBundlesFolder);
        public string ManifestFileName => _manifestFileName;
        public string RemotePathModifiers => $"{EditorUtils.RuntimePlatform}/{Application.version}";

        public AssetHandle<T> LoadAssetAsync<T>(BundledAssetReference<T> reference)
            where T : Object
        {
            return GetBundle(reference.BundleName).LoadAssetAsync(reference);
        }

        public IResourceHandle LoadSingleBundle(string bundleName)
        {
            return GetBundle(bundleName).LoadSingle();
        }

        public string GetPackagedFilePath(string fileName)
        {
            return Path.Combine(PackagedBundlesPath, fileName);
        }

        public string GetRemoteFileUrl(string fileName)
        {
            return $"{_remoteBaseUrl}/{RemotePathModifiers}/{fileName}";
        }

        public long GetBytesToDownload()
        {
            return GetBundlesToDownload()
                .Select(bundle => _manifest.GetDescriptor(bundle.Name))
                .Sum(descriptor => descriptor.Size);
        }

        public BaseAsyncProcess DownloadBundles()
        {
            var downloadTasks = new List<GroupAsyncProcess.Task>();

            foreach (IBundle bundle in GetBundlesToDownload())
            {
                IResourceHandle handle = bundle.LoadSingle();
                BaseAsyncProcess process = handle.LoadProcess;
                downloadTasks.Add(
                        new GroupAsyncProcess.Task(process, _manifest.GetDescriptor(bundle.Name).Size)
                    );

                process.DoWhenCompleted(() => handle.Dispose());
            }

            return new GroupAsyncProcess(downloadTasks.ToArray());
        }

        protected void Awake()
        {
            ReadManifest();
        }

        private IBundle GetBundle(string bundleName)
        {
            IBundle bundle;

            AddSimulatedBundle(bundleName);

            if (!_bundles.TryGetValue(bundleName, out bundle))
            {
                bundle = _instantiator.Instantiate<Bundle>(bundleName, _manifest);
                bundle.Unloading += OnBundleUnloaded;

                _bundles.Add(bundleName, bundle);
            }

            return bundle;
        }

        private void ReadManifest()
        {
            bool isSimulated = false;
            SimulateManifest(ref isSimulated);
            if (isSimulated)
            {
                return;
            }

            DictionaryNode root;

            var serializer = new JsonNodeSerializer();

            using (Stream stream = File.OpenRead(Path.Combine(PackagedBundlesPath, ManifestFileName)))
            {
                root = (DictionaryNode)serializer.Deserialize(stream);
            }

            _manifest.Sync(new DeserializationState(root));
        }

        private void OnBundleUnloaded(IBundle bundle)
        {
            _bundles.Remove(bundle.Name);
        }

        private IEnumerable<IBundle> GetBundlesToDownload()
        {
            return _manifest
                .BundleNames
                .Select(GetBundle)
                .Where(bundle => !bundle.IsUpToDate);
        }

        partial void AddSimulatedBundle(string bundleName);
        partial void SimulateManifest(ref bool isSimulated);
    }
}