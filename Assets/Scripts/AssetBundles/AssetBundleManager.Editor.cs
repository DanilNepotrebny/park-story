﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using UnityEditor;

namespace DreamTeam.AssetBundles
{
    public partial class AssetBundleManager : AssetBundleManager.IEditorApi
    {
        private const string _simulationPrefsKey = "IsAssetBundleSimulation";

        bool IEditorApi.IsSimulation
        {
            get { return EditorPrefs.GetBool(_simulationPrefsKey, true); }
            set { EditorPrefs.SetBool(_simulationPrefsKey, value); }
        }

        #region Inner types

        public interface IEditorApi
        {
            bool IsSimulation { get; set; }
        }

        #endregion
    }
}

#endif