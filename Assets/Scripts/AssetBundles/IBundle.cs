﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils.ResourceLoading;
using Object = UnityEngine.Object;

namespace DreamTeam.AssetBundles
{
    public interface IBundle
    {
        string Name { get; }
        bool IsUpToDate { get; }

        event Action<IBundle> Unloading;

        AssetHandle<T> LoadAssetAsync<T>(BundledAssetReference<T> reference) where T : Object;

        IResourceHandle LoadSingle();
    }
}