﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System.IO;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.AssetBundles
{
    public partial class BundledAssetReference<T> : ISerializationCallbackReceiver, IBuildPreprocessor, BundledAssetReference<T>.IEditorApi
    {
        private string _fullAssetPath;

        private void ClearData()
        {
            _asset = null;
            _bundleName = null;
            _assetPath = null;
        }

        private void UpdateData()
        {
            if (_asset == null)
            {
                ClearData();
                return;
            }

            string path = AssetDatabase.GetAssetPath(_asset);
            if (string.IsNullOrEmpty(path))
            {
                UnityEngine.Debug.LogError("Can't find asset path. Check you are not using scene object.", _asset);
                ClearData();
                return;
            }

            _bundleName = AssetDatabase.GetImplicitAssetBundleName(path);
            if (string.IsNullOrEmpty(_bundleName))
            {
                UnityEngine.Debug.LogError($"Can't find bundle for asset at {path}. Check this asset is added to bundle.", _asset);
                ClearData();
                return;
            }

            string assetName = Path.GetFileNameWithoutExtension(path);
            _assetPath = AssetDatabase.GetAssetPathsFromAssetBundleAndAssetName(_bundleName, assetName).Length > 1 ?
                path :
                assetName;
        }

        void IEditorApi.SetAsset(T asset)
        {
            _asset = asset;
            UpdateData();
        }

        #region ISerializationCallbackReceiver

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            if (BuildUtils.IsBuilding)
            {
                return;
            }

            if (!string.IsNullOrEmpty(_fullAssetPath))
            {
                _asset = AssetDatabase.LoadAssetAtPath<T>(_fullAssetPath);
                _fullAssetPath = null;
            }

            UpdateData();
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
        }

        #endregion

        #region IBuildPreprocessor

        void IBuildPreprocessor.PreprocessBeforeBuild()
        {
            UpdateData();

            var editorApi = (AssetBundleManager.IEditorApi)SystemAssetAccessor<AssetBundleManager>.Asset;

            if (!editorApi.IsSimulation && HasValue)
            {
                _fullAssetPath = AssetDatabase.GetAssetPath(_asset);
                _asset = null;
            }
        }

        #endregion

        #region Inner types

        public interface IEditorApi
        {
            void SetAsset(T asset);
        }

        #endregion
    }
}

#endif