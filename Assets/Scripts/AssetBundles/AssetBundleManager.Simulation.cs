﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR || ASSET_BUNDLE_SIMULATION

namespace DreamTeam.AssetBundles
{
    public partial class AssetBundleManager
    {
        partial void AddSimulatedBundle(string bundleName)
        {
            #if UNITY_EDITOR
            { 
                if (!((IEditorApi)this).IsSimulation)
                {
                    return;
                }
            }
            #endif

            if (!_bundles.ContainsKey(bundleName))
            {
                _bundles.Add(bundleName, _instantiator.Instantiate<SimulatedBundle>(bundleName));
            }
        }

        partial void SimulateManifest(ref bool isSimulated)
        {
            #if UNITY_EDITOR
            {
                if (!((IEditorApi)this).IsSimulation)
                {
                    return;
                }
            }
            #endif

            isSimulated = true;
        }
    }
}

#endif