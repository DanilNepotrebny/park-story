﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.IO;
using DreamTeam.AssetBundles.Processes;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using DreamTeam.Utils.ResourceLoading;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DreamTeam.AssetBundles
{
    public partial class Bundle : IBundle
    {
        private Instantiator _instantiator;
        private AssetBundleManager _manager;
        private Manifest _manifest;
        private BaseLoadBundleProcess _singleLoadProcess;
        private int _refCounter;

        private AssetBundle _bundle => _singleLoadProcess.Bundle;

        public string Name { get; }

        public bool IsUpToDate => IsPackaged() || Caching.IsVersionCached(_url, _manifest.GetDescriptor(Name).Hash);

        private string _packagedPath => _manager.GetPackagedFilePath(Name);
        private string _url => _manager.GetRemoteFileUrl(Name);

        public event Action<IBundle> Unloading;

        public Bundle(string name, Manifest manifest, Instantiator instantiator, AssetBundleManager manager)
        {
            Name = name;
            _manifest = manifest;
            _instantiator = instantiator;
            _manager = manager;
        }

        public AssetBundleRequest LoadAssetAsync<T>(string path) where T : Object
        {
            return _bundle.LoadAssetAsync<T>(path);
        }

        public IResourceHandle LoadSingle()
        {
            UnityEngine.Debug.Log($"Bundle {Name} is loading.");

            if (_singleLoadProcess == null)
            {
                if (IsPackaged())
                {
                    _singleLoadProcess = _instantiator.Instantiate<LoadBundleFromFileProcess>(_packagedPath);
                }
                else
                {
                    Descriptor descriptor = _manifest.GetDescriptor(Name);

                    _singleLoadProcess = _instantiator.Instantiate<LoadBundleFromWebProcess>(
                            new Uri(_url),
                            descriptor.Hash,
                            descriptor.Crc
                        );
                }
            }

            _refCounter++;
            return new SingleResourceHandle(
                    _singleLoadProcess,
                    OnHandleDisposed
                );
        }

        public AssetHandle<T> LoadAssetAsync<T>(BundledAssetReference<T> reference)
            where T : Object
        {
            IResourceHandle bundleHandle = Load();
            var assetLoadProcess = _instantiator.Instantiate<LoadAssetProcess<T>>(
                    this,
                    bundleHandle.LoadProcess,
                    reference.AssetPath
                );
            return new AssetHandle<T>(assetLoadProcess, ref bundleHandle);
        }

        private bool IsPackaged()
        {
            // TODO: File.Exists() will not work for streaming assets on Android
            return File.Exists(_packagedPath);
        }

        private void OnHandleDisposed(ActionDisposable handle)
        {
            _refCounter--;
            if (_refCounter == 0)
            {
                Unload();
            }
        }

        private IResourceHandle Load()
        {
            IResourceHandle singleLoadHandle = LoadSingle();

            List<string> dependencies = _manifest.GetAllDependencies(Name);
            if (dependencies.Count == 0)
            {
                return singleLoadHandle;
            }

            List<IResourceHandle> handles = new List<IResourceHandle> { singleLoadHandle };
            foreach (string name in dependencies)
            {
                handles.Add(_manager.LoadSingleBundle(name));
            }

            IEnumerable<IResourceHandle> resourceHandles = handles;
            return new GroupResourceHandle(ref resourceHandles);
        }

        private void Unload()
        {
            UnityEngine.Debug.Log($"Bundle {Name} is unloaded.");
            Unloading?.Invoke(this);
            _bundle.Unload(true);
        }
    }
}