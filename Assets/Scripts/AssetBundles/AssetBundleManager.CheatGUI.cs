﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Text;
using DreamTeam.Debug;
using DreamTeam.Utils;
using Zenject;

namespace DreamTeam.AssetBundles
{
    public partial class AssetBundleManager
    {
        [Inject]
        private void ConstructCheats(DebugStatesSystem debugStatesSystem)
        {
            debugStatesSystem.RegisterWriter("Asset Bundles", WriteDebugInfo);
        }

        private void WriteDebugInfo(StringBuilder builder)
        {
            builder.AppendLine("\nAsset Bundle reference counters:");
            foreach (IBundle bundle in _bundles.Values)
            {
                var bundleCheatApi = bundle as Bundle.ICheatAPI;
                if (bundleCheatApi != null)
                {
                    builder.AppendParameter(bundle.Name, bundleCheatApi.RefCounter.ToString());
                }
            }
        }
    }
}