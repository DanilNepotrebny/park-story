﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.AssetBundles.Processes;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.ResourceLoading;
using Object = UnityEngine.Object;

namespace DreamTeam.AssetBundles
{
    public class AssetHandle<T> : IResourceHandle
        where T : Object
    {
        private IResourceHandle _bundleHandle;
        private BaseLoadAssetProcess<T> _loadProcess;

        public T Asset => _loadProcess.Asset;
        public BaseAsyncProcess LoadProcess => _loadProcess;

        public AssetHandle(BaseLoadAssetProcess<T> loadProcess, ref IResourceHandle bundleHandle)
        {
            _loadProcess = loadProcess;
            _bundleHandle = bundleHandle;
            bundleHandle = null;
        }

        public void Dispose()
        {
            _bundleHandle.Dispose();
            _bundleHandle = null;
        }
    }
}