﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using UnityEngine;

namespace DreamTeam.AssetBundles
{
    public class Descriptor : ISynchronizable
    {
        private List<string> _dependencies;

        public IEnumerable<string> Dependencies => _dependencies;
        public Hash128 Hash { get; private set; }
        public uint Crc { get; private set; }
        public long Size { get; private set; }

        public Descriptor(IEnumerable<string> dependencies, Hash128 hash, uint crc, long size)
        {
            _dependencies = dependencies.ToList();
            Hash = hash;
            Crc = crc;
            Size = size;
        }

        public Descriptor()
        {
            _dependencies = new List<string>();
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncStringList("dependencies", ref _dependencies, true);

            string crc = Crc.ToString();
            state.SyncString("crc", ref crc);
            Crc = uint.Parse(crc);

            string hash = Hash.ToString();
            state.SyncString("hash", ref hash);
            Hash = Hash128.Parse(hash);

            string size = Size.ToString();
            state.SyncString("size", ref size);
            Size = long.Parse(size);
        }
    }
}