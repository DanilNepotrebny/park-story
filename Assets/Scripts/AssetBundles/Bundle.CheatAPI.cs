﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.AssetBundles
{
    public partial class Bundle : Bundle.ICheatAPI
    {
        int ICheatAPI.RefCounter => _refCounter;

        #region

        public interface ICheatAPI
        {
            int RefCounter { get; }
        }

        #endregion
    }
}