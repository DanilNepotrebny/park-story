﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.AssetBundles.Editor
{
    [CustomPropertyDrawer(typeof(BundleNameAttribute))]
    public class BundleNameDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.serializedObject.isEditingMultipleObjects)
            {
                EditorGUI.BeginDisabledGroup(true);
                EditorGUI.TextField(position, label, "Multi-editing is not supported");
                EditorGUI.EndDisabledGroup();
                return;
            }

            EditorGUI.BeginProperty(position, label, property);

            string[] names = AssetDatabase.GetAllAssetBundleNames();
            int selectedIdx = Array.IndexOf(names, property.stringValue);
            int newIdx = EditorGUI.Popup(position, label.text, selectedIdx, names);
            if (selectedIdx != newIdx)
            {
                property.stringValue = names[newIdx];
            }

            EditorGUI.EndProperty();
        }
    }
}