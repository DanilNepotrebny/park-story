﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.IO;
using System.Linq;
using DreamTeam.Editor.Utils;
using DreamTeam.Synchronization.Serializers;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.AssetBundles.Editor
{
    [InitializeOnLoad]
    public static class Builder
    {
        private const string _remoteFolderName = "Remote";
        private const string _bundlesFolderName = "Bundles";

        private static AssetBundleManager _manager => SystemAssetAccessor<AssetBundleManager>.Asset;

        static Builder()
        {
            BuildUtils.PlayerBuilding += OnPlayerBuilding;
        }

        private static void OnPlayerBuilding(ref BuildPlayerOptions options, HashSet<string> defines)
        {
            if (((AssetBundleManager.IEditorApi)_manager).IsSimulation)
            {
                defines.Add("ASSET_BUNDLE_SIMULATION");
            }
            else
            {
                BuildAssetBundles();

                options.assetBundleManifestPath = GetManifestBundlePath() + ".manifest";
            }
        }

        public static void BuildAssetBundles()
        {
            string path = GetBundlesPath();
            Directory.CreateDirectory(path);

            BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget);

            FileUtils.RecreateDirectory(_manager.PackagedBundlesPath);

            CreateManifest();
            CopyBundlesToDestination();
        }

        private static void CreateManifest()
        {
            var manifest = new Manifest();

            AssetBundleManifest assetBundleManifest = AssetBundle
                .LoadFromFile(GetManifestBundlePath())
                .LoadAsset<AssetBundleManifest>("assetbundlemanifest");

            foreach (string name in AssetDatabase.GetAllAssetBundleNames())
            {
                string path = GetFileBundlePath(name);

                uint crc;
                BuildPipeline.GetCRCForAssetBundle(path, out crc);
                
                var descriptor = new Descriptor(
                        assetBundleManifest.GetDirectDependencies(name),
                        assetBundleManifest.GetAssetBundleHash(name),
                        crc,
                        new FileInfo(path).Length
                    );
                manifest.AddDescriptor(name, descriptor);
            }

            var state = new SerializationState();
            manifest.Sync(state);

            using (Stream stream = File.OpenWrite(_manager.GetPackagedFilePath(_manager.ManifestFileName)))
            {
                var serializer = new JsonNodeSerializer();
                serializer.Serialize(stream, state.Root);
            }
        }

        private static void CopyBundlesToDestination()
        {
            PackagedBundles packagedBundles = AssetDatabaseUtils
                .EnumerateAssets<PackagedBundles>()
                .First();

            string remoteDirPath = Path.Combine(
                    GetFileBundlePath(_remoteFolderName),
                    _manager.RemotePathModifiers
                );

            FileUtils.RecreateDirectory(remoteDirPath);

            foreach (string name in AssetDatabase.GetAllAssetBundleNames())
            {
                string targetPath = packagedBundles.Contains(name) ?
                    _manager.GetPackagedFilePath(name) :
                    Path.Combine(remoteDirPath, name);

                FileUtil.CopyFileOrDirectory(GetFileBundlePath(name), targetPath);
            }
        }

        private static string GetManifestBundlePath()
        {
            return GetFileBundlePath(_bundlesFolderName);
        }

        private static string GetFileBundlePath(string fileName)
        {
            return Path.Combine(GetBundlesPath(), fileName);
        }

        private static string GetBundlesPath()
        {
            string projectPath = Path.GetFullPath(Path.Combine(Application.dataPath, ".."));
            return Path.Combine(projectPath, _bundlesFolderName);
        }
    }
}
