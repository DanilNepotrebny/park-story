﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.AssetBundles.Editor
{
    public class PackagedBundles : ScriptableObject
    {
        [SerializeField, BundleName] private List<string> _bundles;

        public bool Contains(string bundleName)
        {
            return _bundles.Contains(bundleName);
        }
    }
}