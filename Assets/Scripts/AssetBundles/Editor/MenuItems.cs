﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEditor;

namespace DreamTeam.AssetBundles.Editor
{
    public static class MenuItems
    {
        private const string _simulationModeItemName = "Assets/Asset Bundles/Simulation Mode";

        private static AssetBundleManager.IEditorApi _manager => SystemAssetAccessor<AssetBundleManager>.Asset;

        [MenuItem(_simulationModeItemName)]
        public static void ToggleSimulationMode()
        {
            _manager.IsSimulation = !Menu.GetChecked(_simulationModeItemName);
        }

        [MenuItem(_simulationModeItemName, true)]
        public static bool ToggleSimulationModeValidate()
        {
            Menu.SetChecked(_simulationModeItemName, _manager.IsSimulation);
            return true;
        }

        [MenuItem("Assets/Asset Bundles/Build Asset Bundles")]
        public static void BuildAssetBundles()
        {
            Builder.BuildAssetBundles();
        }
    }
}