﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DreamTeam.AssetBundles
{
    [Serializable]
    public partial class BundledAssetReference<T> where T : Object
    {
        [SerializeField] private T _asset;
        [SerializeField, ReadOnly] private string _bundleName;
        [SerializeField, HideInInspector] private string _assetPath;

        public string BundleName => _bundleName;
        public string AssetPath => _assetPath;

        #if UNITY_EDITOR || ASSET_BUNDLE_SIMULATION
        public T SimulatedAsset => _asset;
        #endif

        public bool HasValue => !string.IsNullOrEmpty(_assetPath);

        public BundledAssetReference()
        {
        }

        public BundledAssetReference(T asset, string assetPath, string bundleName)
        {
            _asset = asset;
            _assetPath = assetPath;
            _bundleName = bundleName;
        }
    }

    [Serializable]
    public class BundledGameObjectReference : BundledAssetReference<GameObject>
    {
    }
}