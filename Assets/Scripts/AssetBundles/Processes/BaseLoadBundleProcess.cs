﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;

namespace DreamTeam.AssetBundles.Processes
{
    public abstract class BaseLoadBundleProcess : BaseAsyncProcess
    {
        public abstract AssetBundle Bundle { get; }
    }
}