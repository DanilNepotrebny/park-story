﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using UnityEngine.Networking;

namespace DreamTeam.AssetBundles.Processes
{
    public class LoadBundleFromWebProcess : BaseLoadBundleProcess
    {
        private UnityWebRequestAsyncOperation _operation;
        private AssetBundle _bundle;

        public override float Progress => _operation?.progress ?? 0f;
        public override AssetBundle Bundle => _bundle;

        public LoadBundleFromWebProcess(Uri uri, Hash128 hash, uint crc)
        {
            UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(uri, hash, crc);
            _operation = request.SendWebRequest();
            _operation.completed += OnOperationCompleted;
        }

        private void OnOperationCompleted(AsyncOperation operation)
        {
            _bundle = DownloadHandlerAssetBundle.GetContent(_operation.webRequest);
            _operation.webRequest.Dispose();

            OnCompleted();
        }
    }
}