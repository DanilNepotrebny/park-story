﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;

namespace DreamTeam.AssetBundles.Processes
{
    public abstract class BaseLoadAssetProcess<T> : BaseAsyncProcess where T : Object
    {
        public abstract T Asset { get; }
    }
}