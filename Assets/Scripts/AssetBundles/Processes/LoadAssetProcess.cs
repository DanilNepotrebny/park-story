﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;

namespace DreamTeam.AssetBundles.Processes
{
    public class LoadAssetProcess<T> : BaseLoadAssetProcess<T> where T : Object
    {
        private Bundle _bundle;
        private string _path;
        private AssetBundleRequest _loadRequest;

        public override float Progress => _loadRequest?.progress ?? 0f;
        public override T Asset => _loadRequest.asset as T;

        public LoadAssetProcess(Bundle bundle, BaseAsyncProcess loadBundleProcess, string path)
        {
            _bundle = bundle;
            _path = path;

            loadBundleProcess.DoWhenCompleted(LoadAsset);
        }

        private void LoadAsset()
        {
            _loadRequest = _bundle.LoadAssetAsync<T>(_path);
            _loadRequest.completed += op => OnCompleted();
        }
    }
}