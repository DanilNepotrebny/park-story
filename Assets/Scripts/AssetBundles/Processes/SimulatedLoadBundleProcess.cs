﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR || ASSET_BUNDLE_SIMULATION

using System.Collections;
using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;

namespace DreamTeam.AssetBundles.Processes
{
    public partial class SimulatedLoadBundleProcess : BaseAsyncProcess
    {
        private AssetBundleCreateRequest _request;

        public override float Progress => IsCompleted ? 1f : 0f;

        public SimulatedLoadBundleProcess(string name, AssetBundleManager manager)
        {
            manager.StartCoroutine(LoadBundle(name));
        }

        private IEnumerator LoadBundle(string name)
        {
            CheckBundleName(name);

            yield return new WaitForSeconds(0.1f);

            OnCompleted();
        }

        partial void CheckBundleName(string name);
    }
}

#endif