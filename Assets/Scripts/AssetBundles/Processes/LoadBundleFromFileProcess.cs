﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.AssetBundles.Processes
{
    public class LoadBundleFromFileProcess : BaseLoadBundleProcess
    {
        private AssetBundleCreateRequest _request;

        public override float Progress => _request?.progress ?? 0f;
        public override AssetBundle Bundle => _request.assetBundle;

        public LoadBundleFromFileProcess(string path)
        {
            _request = AssetBundle.LoadFromFileAsync(path);
            _request.completed += OnRequestCompleted;
        }

        private void OnRequestCompleted(AsyncOperation operation)
        {
            OnCompleted();
        }
    }
}