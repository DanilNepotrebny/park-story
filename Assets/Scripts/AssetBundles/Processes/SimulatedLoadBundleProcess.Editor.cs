﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;
using System.Linq;
using UnityEditor;

namespace DreamTeam.AssetBundles.Processes
{
    public partial class SimulatedLoadBundleProcess
    {
        partial void CheckBundleName(string name)
        {
            if (!AssetDatabase.GetAllAssetBundleNames().Contains(name))
            {
                throw new ArgumentException($"Can't load asset bundle {name}: bundle doesn't exist.");
            }
        }
    }
}

#endif