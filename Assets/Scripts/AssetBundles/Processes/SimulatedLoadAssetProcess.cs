﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR || ASSET_BUNDLE_SIMULATION

using System.Collections;
using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;

namespace DreamTeam.AssetBundles.Processes
{
    public class SimulatedLoadAssetProcess<T> : BaseLoadAssetProcess<T> where T : Object
    {
        private T _asset;

        public override T Asset => _asset;
        public override float Progress => IsCompleted ? 1f : 0f;

        public SimulatedLoadAssetProcess(BaseAsyncProcess loadBundleProcess, BundledAssetReference<T> reference, AssetBundleManager manager)
        {
            manager.StartCoroutine(LoadAsset(loadBundleProcess, reference));
        }

        private IEnumerator LoadAsset(BaseAsyncProcess loadBundleProcess, BundledAssetReference<T> reference)
        {
            yield return loadBundleProcess;
            yield return new WaitForSeconds(0.5f);

            _asset = reference.SimulatedAsset;

            OnCompleted();
        }
    }
}

#endif