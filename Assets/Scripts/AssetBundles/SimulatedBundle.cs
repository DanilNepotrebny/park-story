﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR || ASSET_BUNDLE_SIMULATION

using System;
using DreamTeam.AssetBundles.Processes;
using DreamTeam.Utils;
using DreamTeam.Utils.ResourceLoading;

namespace DreamTeam.AssetBundles
{
    public class SimulatedBundle : IBundle
    {
        private Instantiator _instantiator;

        public string Name { get; }

        public SimulatedBundle(string name, Instantiator instantiator)
        {
            Name = name;
            _instantiator = instantiator;
        }

        public IResourceHandle LoadSingle()
        {
            return new SingleResourceHandle(
                    _instantiator.Instantiate<SimulatedLoadBundleProcess>(Name),
                    bundle => { }
                );
        }

        #region IBundle

        bool IBundle.IsUpToDate => true;

        event Action<IBundle> IBundle.Unloading
        {
            add { }
            remove { }
        }

        AssetHandle<T> IBundle.LoadAssetAsync<T>(BundledAssetReference<T> reference)
        {
            IResourceHandle bundleHandle = LoadSingle();
            return new AssetHandle<T>(
                    _instantiator.Instantiate<SimulatedLoadAssetProcess<T>>(bundleHandle.LoadProcess, reference),
                    ref bundleHandle
                );
        }

        #endregion
    }
}

#endif