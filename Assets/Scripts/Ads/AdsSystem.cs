﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Location;
using DreamTeam.Debug;
using DreamTeam.IAP;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Localization;
using DreamTeam.Match3.Levels;
using DreamTeam.PersistentCommands;
using DreamTeam.PersistentCommands.Rewards;
using DreamTeam.Plugins.GoogleAdsMediationHelpSuite;
using DreamTeam.Popups;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using GleyMobileAds;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using Random = UnityEngine.Random;

namespace DreamTeam.Ads
{
    public class AdsSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        [Header("General")]
        [SerializeField] private int _rewardedVideoUnlockLevel = 88;

        [Header("Blocks")]
        [SerializeField] private int _rewardedVideoBlockSize = 2;
        [SerializeField] private TimeTicks _nonPayerRewardedVideoBlockInterval;
        [SerializeField] private TimeTicks _payerRewardedVideoBlockInterval;

        [Header("Rewards")]
        [SerializeField] private SceneReference _videoWatchRewardsScene;
        [SerializeField, LocalizationKey] private string _rewardsSceneHeaderKey;
        [SerializeField] private Reward _initialReward;
        [SerializeField] private RewardsWeightsMap _nonPayerInitialDayRewardsMapping;
        [SerializeField] private RewardsWeightsMap _nonPayerRegularDayRewardsMapping;
        [SerializeField] private RewardsWeightsMap _nonPayerBonusRewardsMapping;
        [SerializeField] private RewardsWeightsMap _payerRewardsMapping;
        [SerializeField] private TimeTicks _bonusRewardsInterval;
        [SerializeField] [Range(0f, 1f)] private float _bonusMappingProbability;

        [Header("Popups")]
        [SerializeField] private SceneReference _adsConsentPopup;

        [Inject] private SaveGroup _saveGroup;
        [Inject] private IAPUserTrackingSystem _iapUserTrackingSystem;
        [Inject] private PopupSystem _popupSystem;
        [Inject] private LocalizationSystem _localizationSystem;
        [Inject] private CheatPanel _cheatPanel;
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private Instantiator _instantiator;
        [Inject] private PersistentCommandsExecutionSystem _persistentCommands;

        #if !NO_MATCH3
        [Inject(Id = GlobalInstaller.MainLevelPackControllerId)] private SingleLevelPackController _levelPackController;
        #endif

        private int _videoBlockCounter = 0;
        private bool _isRewardedVideoBlockAvailable = true;
        private string _nextBlockAvailabilityDate = string.Empty;

        private bool _isInitialRewardGiven = false;
        private string _nonPayerDayInitialVideoWatchDate = string.Empty;
        private string _nonPayerBonusVideoWatchDate = string.Empty;

        private bool _isConsentPopupShown = false;

        private IPersistentCommand _givingCommand;

        private bool _isCheatsRewardedVideoUnlocked = false;
        private bool _emulateRewardedVideoAvailability = false;

        public event Action OnRewardedVideoStarted;
        public event Action<bool> OnRewardedVideoFinished;

        public bool IsRewardedVideoAvailable => IsRewardedUnlocked && IsRewardedVideoBlockAvailable && IsRewardedVideoAvailableNative;
        public bool IsRewardedUnlocked => _levelPackController.CurrentLevelIndex >= _rewardedVideoUnlockLevel || _isCheatsRewardedVideoUnlocked;
        public bool IsRewardedVideoBlockAvailable => _isRewardedVideoBlockAvailable;
        public bool IsRewardedVideoAvailableNative => Advertisements.Instance.IsRewardVideoAvailable() || _emulateRewardedVideoAvailability;

        public bool IsConsentPopupShown => _isConsentPopupShown;

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("AdsSystem", this);
        }

        public void ShowRewardedVideo()
        {
            Assert.IsTrue(_isConsentPopupShown, "Ads should not be shown to user before consent popup is shown!");

            OnRewardedVideoStarted?.Invoke();
            if (IsRewardedVideoAvailable)
            {
                if (_emulateRewardedVideoAvailability)
                {
                    StartCoroutine(EmulatedRewardedVideoRoutine());
                }
                else
                {
                    Advertisements.Instance.ShowRewardedVideo(OnVideoFinished, OnVideoRedirection);
                }
            }
            else
            {
                OnRewardedVideoFinished?.Invoke(false);
            }
        }

        public void ShowConsentPopup()
        {
            _popupSystem.Show(_adsConsentPopup.Name);
            StartCoroutine(WaitForConsent());
        }

        public void SetUserConsent(bool isConsent)
        {
            Advertisements.Instance.SetUserConsent(isConsent);
        }

        protected void Awake()
        {
            _cheatPanel.AddDrawer(CheatPanel.Category.Ads, DrawCheats);
        }

        protected void Start()
        {
            if (!Advertisements.Instance.UserConsentWasSet())
            {
                Advertisements.Instance.SetUserConsent(false);
            }

            Advertisements.Instance.Initialize();

            if (string.IsNullOrEmpty(_nextBlockAvailabilityDate))
            {
                _isRewardedVideoBlockAvailable = true;
            }

            if (!_isRewardedVideoBlockAvailable)
            {
                DateTime blockAvailabilityDate = DateTime.Parse(_nextBlockAvailabilityDate, CultureInfo.InvariantCulture);
                StartCoroutine(VideoUnlockingRoutine(blockAvailabilityDate));
            }
        }

        private void OnVideoFinished(bool result)
        {
            var shownEvent = _instantiator.Instantiate<RewardedVideoShownEvent>(result);
            _analyticsSystem.Send(shownEvent);

            OnRewardedVideoFinished?.Invoke(result);

            if (result)
            {
                UpdateVideoBlockData();

                List<Reward> rewards = CalculateRewards();

                _givingCommand = GiveRewardsCollectionCommand.Construct(_instantiator, rewards, ReceivingType.Earned, GamePlace.RewardedVideo);
                _persistentCommands.Register(_givingCommand);

                var args = new RewardsPopup.Args()
                {
                    RewardsTakenCallback = () => OnRewardReceived(rewards),
                    Header = _localizationSystem.Localize(_rewardsSceneHeaderKey, this),
                    Rewards = rewards,
                };

                _popupSystem.Show(_videoWatchRewardsScene.Name, args);
            }
        }

        private void OnVideoRedirection(string data)
        {
            var redirectEvent = _instantiator.Instantiate<RewardedVideoClickedEvent>();
            _analyticsSystem.Send(redirectEvent);
        }

        private void OnRewardReceived(List<Reward> rewards)
        {
            foreach (Reward reward in rewards)
            {
                reward.Receive(_instantiator, ReceivingType.Earned, GamePlace.RewardedVideo);
            }

            _persistentCommands.Unregister(_givingCommand);
        }

        private void UpdateVideoBlockData()
        {
            ++_videoBlockCounter;
            if (_videoBlockCounter >= _rewardedVideoBlockSize)
            {
                _isRewardedVideoBlockAvailable = false;
                DateTime blockAvailabilityDate = DateTime.Now;
                switch (_iapUserTrackingSystem.UserPayerType)
                {
                    case IAPUserTrackingSystem.PayerType.None:
                        blockAvailabilityDate += _nonPayerRewardedVideoBlockInterval;
                        break;

                    case IAPUserTrackingSystem.PayerType.Average:
                    case IAPUserTrackingSystem.PayerType.High:
                        blockAvailabilityDate += _payerRewardedVideoBlockInterval;
                        break;
                }

                _nextBlockAvailabilityDate = blockAvailabilityDate.ToString(CultureInfo.InvariantCulture);
                StartCoroutine(VideoUnlockingRoutine(blockAvailabilityDate));
            }
        }

        private IEnumerator VideoUnlockingRoutine(DateTime unlockDate)
        {
            yield return new WaitUntil(() => DateTime.Now > unlockDate);

            _isRewardedVideoBlockAvailable = true;
            _videoBlockCounter = 0;
        }

        private List<Reward> CalculateRewards()
        {
            List<Reward> rewards = new List<Reward>();
            switch (_iapUserTrackingSystem.UserPayerType)
            {
                case IAPUserTrackingSystem.PayerType.High:
                case IAPUserTrackingSystem.PayerType.Average:
                {
                    Reward reward = RandomUtils.RandomElement(_payerRewardsMapping);
                    rewards.Add(reward);
                    break;
                }

                case IAPUserTrackingSystem.PayerType.None:
                {
                    Reward reward = null;
                    if (!_isInitialRewardGiven)
                    {
                        reward = _initialReward;
                        _isInitialRewardGiven = true;
                        _nonPayerDayInitialVideoWatchDate = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                        _nonPayerBonusVideoWatchDate =
                            (DateTime.Now + _bonusRewardsInterval).ToString(CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        reward = RandomUtils.RandomElement(CalculateRewardsMapping());
                    }

                    rewards.Add(reward);
                    break;
                }
            }

            return rewards;
        }

        private RewardsWeightsMap CalculateRewardsMapping()
        {
            RewardsWeightsMap result;

            DateTime dayInitialRewardVideoWatchDate = DateTime.Parse(_nonPayerDayInitialVideoWatchDate, CultureInfo.InvariantCulture);
            DateTime bonusRewardsVideoWatchDate = DateTime.Parse(_nonPayerBonusVideoWatchDate, CultureInfo.InvariantCulture);
            DateTime currentDate = DateTime.Now;
            float randomRoll = Random.Range(0f, 1f);

            if (currentDate > bonusRewardsVideoWatchDate &&
                randomRoll <= _bonusMappingProbability)
            {
                result = _nonPayerBonusRewardsMapping;
                _nonPayerBonusVideoWatchDate = (DateTime.Now + _bonusRewardsInterval).ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                if (currentDate.Day == dayInitialRewardVideoWatchDate.Day)
                {
                    result = _nonPayerRegularDayRewardsMapping;
                }
                else if (currentDate.Day - dayInitialRewardVideoWatchDate.Day > 1)
                {
                    result = _nonPayerInitialDayRewardsMapping;
                    _nonPayerDayInitialVideoWatchDate = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    if (dayInitialRewardVideoWatchDate.Hour > 23 &&
                        currentDate.Hour == 0 &&
                        dayInitialRewardVideoWatchDate.Minute > currentDate.Minute)
                    {
                        result = _nonPayerRegularDayRewardsMapping;
                    }
                    else
                    {
                        result = _nonPayerInitialDayRewardsMapping;
                        _nonPayerDayInitialVideoWatchDate = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                    }
                }
            }

            return result;
        }

        private IEnumerator EmulatedRewardedVideoRoutine()
        {
            yield return new WaitForSeconds(1f);
            OnVideoFinished(true);
        }

        private IEnumerator WaitForConsent()
        {
            Popup consentPopup;
            do
            {
                consentPopup = _popupSystem.GetPopup(_adsConsentPopup.Name);
                yield return null;
            }
            while (consentPopup == null || !consentPopup.IsHidden);

            _isConsentPopupShown = true;
        }

        private void DrawCheats(Rect rect)
        {
            GUILayout.BeginHorizontal();
            _isCheatsRewardedVideoUnlocked = GUILayout.Toggle(
                    _isCheatsRewardedVideoUnlocked,
                    _isCheatsRewardedVideoUnlocked ? "X" : "",
                    GUI.skin.button,
                    GUILayout.Width(GUI.skin.button.lineHeight)
                );
            GUILayout.Label("Force unlock ads");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            _emulateRewardedVideoAvailability = GUILayout.Toggle(
                    _emulateRewardedVideoAvailability,
                    _emulateRewardedVideoAvailability ? "X" : "",
                    GUI.skin.button,
                    GUILayout.Width(GUI.skin.button.lineHeight)
                );
            GUILayout.Label("Emulate editor ads availability");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            _isRewardedVideoBlockAvailable = GUILayout.Toggle(
                    _isRewardedVideoBlockAvailable,
                    _isRewardedVideoBlockAvailable ? "X" : "",
                    GUI.skin.button,
                    GUILayout.Width(GUI.skin.button.lineHeight)
                );
            GUILayout.Label("Is video block available?");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUI.enabled = false;
            bool haveAvailableVideos = Advertisements.Instance.IsRewardVideoAvailable();
            GUILayout.Toggle(haveAvailableVideos, haveAvailableVideos ? "X" : "", GUI.skin.button, GUILayout.Width(GUI.skin.button.lineHeight));
            GUI.enabled = true;
            GUILayout.Label("(Native) Are videos available?");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUI.enabled = Suite.IsSuiteAvailable();
            if (GUILayout.Button("Show ADMOB mediation suite"))
            {
                Advertiser admobAdvertiser = Advertisements.Instance.GetRewardedAdvertisers()
                    .Find(advertiser => advertiser.advertiser == SupportedAdvertisers.Admob);
                PlatformSettings iosAdmobSettings =
                    admobAdvertiser.platformSettings.Find(settings => settings.platform == SupportedPlatforms.iOS);

                Suite.Show(iosAdmobSettings.appId.id);
            }

            GUI.enabled = true;
            GUILayout.EndHorizontal();
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncBool("IsRewardedVideoBlockAvailable", ref _isRewardedVideoBlockAvailable, true);
            state.SyncInt("RewardedVideoBlockCounter", ref _videoBlockCounter, 0);
            state.SyncString("NextRewardedVideoBlockAvailableDate", ref _nextBlockAvailabilityDate, string.Empty);
            state.SyncBool("IsInitialRewardGiven", ref _isInitialRewardGiven, false);
            state.SyncString("NonPayerDayInitialVideoWatchDate", ref _nonPayerDayInitialVideoWatchDate, string.Empty);
            state.SyncString("NonPayerBonusVideoWatchDate", ref _nonPayerBonusVideoWatchDate, string.Empty);
            state.SyncBool("IsConsentPopupShown", ref _isConsentPopupShown, false);
        }

        [Serializable]
        private class RewardsWeightsMap : KeyValueList<Reward, int>
        {
        }
    }
}