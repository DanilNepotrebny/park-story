﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam
{
    /// <summary>
    /// Component that aligns camera to the world center with offset calculated based on specified canvas position. 
    /// Camera is automatically picked up from canvas.
    /// </summary>
    [ExecuteInEditMode]
    [RequireComponent(typeof(PositionChangeNotifier))]
    public class Match3CameraAligner : MonoBehaviour
    {
        private PositionChangeNotifier _canvasPositionProvider => GetComponent<PositionChangeNotifier>();

        protected void OnEnable()
        {
            Align();
            _canvasPositionProvider.PositionChanged += Align;
        }

        protected void OnDisable()
        {
            _canvasPositionProvider.PositionChanged -= Align;
        }

        protected void OnValidate()
        {
            Align();
        }

        private void Align(Transform tr = null)
        {
            Camera foundCamera = FindCamera();
            if (foundCamera != null)
            {
                // We need to put camera into world origin + offset between canvas center and provider (in world space).
                // The idea here is that camera position and canvas center are the same
                // and provider position is bound to camera position.
                Vector3 canvasCenter = foundCamera.transform.position;
                foundCamera.transform.position = canvasCenter - _canvasPositionProvider.transform.position;
            }
        }

        private Camera FindCamera()
        {
            return GetComponentInParent<Canvas>()?.worldCamera;
        }
    }
}