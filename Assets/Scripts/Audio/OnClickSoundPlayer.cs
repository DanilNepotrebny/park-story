﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DreamTeam.Audio
{
    public class OnClickSoundPlayer : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField, RequiredField] private AudioSystemPlayer _soundPlayer;

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            _soundPlayer.Play();
        }
    }
}