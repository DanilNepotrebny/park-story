﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using DreamTeam.Audio.Effects;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.Audio
{
    public class AudioSystemPlayer : MonoBehaviour
    {
        [SerializeField] private AudioSource _source;
        [SerializeField] private bool _playOnEnable;
        [SerializeField] private bool _destroyOnStop;

        [Inject] private AudioSystem _system;

        private Queue<AudioSource> _delayedQueue = new Queue<AudioSource>();
        private BaseAudioEffect[] _playingEffects = new BaseAudioEffect[0];
        private AudioSource _playingSource;

        public AudioSource AudioSource => _source;
        public bool IsPlaying => _playingSource != null && _playingSource.isPlaying;
        public bool IsLooped => IsPlaying && _playingSource.loop;
        public bool PlaysOnEnable => _playOnEnable;

        public event Action Stoped;

        [ContextMenu(nameof(Play))]
        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void Play()
        {
            Play(_source);
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void Play(AudioSource source)
        {
            if (IsPlaying)
            {
                Stop(() => Play(source));
                return;
            }

            if (source != null && enabled)
            {
                _source = source;
                _source.CopyValuesTo(_playingSource);
                _system.Play(_playingSource);
                _playingEffects = _source.GetComponents<BaseAudioEffect>();
                PerformEffectsStarting();
                StartCoroutine(WaitForPlaying());
            }
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void Play(float delay)
        {
            Play(_source, delay);
        }

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void Play(AudioSource source, float delay)
        {
            _delayedQueue.Enqueue(source);
            Invoke(nameof(PlayDelayedQueue), delay);
        }

        [ContextMenu(nameof(Stop))]
        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void Stop()
        {
            Stop(null);
        }

        protected void Awake()
        {
            _playingSource = gameObject.AddComponent<AudioSource>();
            _playingSource.hideFlags = HideFlags.DontSave | HideFlags.NotEditable;
        }

        protected void OnDestroy()
        {
            _system.Stop(_playingSource);
            _playingSource.Dispose();
        }

        protected void OnEnable()
        {
            _playingSource.enabled = true;
            if (_playOnEnable)
            {
                Play();
            }
        }

        protected void OnDisable()
        {
            StopCoroutine(WaitForPlaying());
            _playingSource.enabled = false;
        }

        protected void Reset()
        {
            _source = GetComponentInChildren<AudioSource>();
        }

        protected void OnValidate()
        {
            if (_source != null)
            {
                _source.playOnAwake = false;
            }
        }

        private void Stop(Action stopedCallback)
        {
            if (IsPlaying)
            {
                Action unlockedCallback = () =>
                {
                    _system.Stop(_playingSource);
                    stopedCallback?.Invoke();
                };

                using (var stopingHandle = new DeferredInvocation(unlockedCallback).Start())
                {
                    PerformEffectsStoping(stopingHandle);
                }
            }
        }

        private void PlayDelayedQueue()
        {
            if (_delayedQueue.Count > 0)
            {
                Play(_delayedQueue.Dequeue());
            }
        }

        private IEnumerator WaitForPlaying()
        {
            while (IsPlaying)
            {
                PerformEffectsUpdating();
                yield return null;
            }

            Stoped?.Invoke();

            if (_destroyOnStop)
            {
                gameObject.Dispose();
            }
        }

        private void PerformEffectsStoping(IDeferredInvocationHandle stopingHandle)
        {
            foreach (BaseAudioEffect effect in _playingEffects)
            {
                effect.PerformStop(_playingSource, stopingHandle);
            }
        }

        private void PerformEffectsUpdating()
        {
            foreach (BaseAudioEffect effect in _playingEffects)
            {
                effect.PerformUpdate(_playingSource);
            }
        }

        private void PerformEffectsStarting()
        {
            foreach (BaseAudioEffect effect in _playingEffects)
            {
                effect.PerformStart(_playingSource);
            }
        }
    }
}