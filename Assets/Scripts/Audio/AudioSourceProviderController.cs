﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace DreamTeam.Audio
{
    [RequireComponent(typeof(AudioSystemPlayer))]
    public class AudioSourceProviderController : MonoBehaviour
    {
        [SerializeField] private AudioSourceProvider _provider;

        private AudioSource _currentSource;

        private AudioSystemPlayer _player => GetComponent<AudioSystemPlayer>();
        private IEnumerator<AudioSource> _playList => _provider;
        private bool _isValid => enabled && _playList != null;

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void SetProvider(AudioSourceProvider playList)
        {
            _provider = playList;
        }

        [ContextMenu(nameof(PlayNext))]
        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void PlayNext()
        {
            if (_isValid && _player.AudioSource == _currentSource)
            {
                if (_playList.MoveNext())
                {
                    _currentSource = _playList.Current;
                    _player.Play(_currentSource);
                }
            }
        }

        [ContextMenu(nameof(Stop))]
        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void Stop()
        {
            if (_isValid && _player.IsPlaying)
            {
                _player.Stoped -= PlayNext;
                _player.Stop();
            }
        }

        [ContextMenu(nameof(Play))]
        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void Play()
        {
            if (_isValid && _playList.Current != null)
            {
                _currentSource = _playList.Current;
                _player.Play(_currentSource);
                _player.Stoped += PlayNext;
            }
        }

        [ContextMenu(nameof(ResetPlayList))]
        [Obfuscation(Exclude = false, Feature = "-rename")]
        public void ResetPlayList()
        {
            if (_isValid)
            {
                _playList.Reset();
            }
        }

        protected void OnEnable()
        {
            _player.enabled = true;
            if (_player.PlaysOnEnable)
            {
                Play();
            }
        }

        protected void OnDisable()
        {
            Stop();
            _player.enabled = false;
        }
    }
}