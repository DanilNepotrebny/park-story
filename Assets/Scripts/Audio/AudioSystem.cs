﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Audio;
using Zenject;

namespace DreamTeam.Audio
{
    public class AudioSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private SaveManager.Group _saveGroup = SaveManager.Group.Settings;

        [SerializeField, RequiredField]
        private List<AudioMixerExposedParameter> _savedParameters = new List<AudioMixerExposedParameter>();

        [Header("Mixing settings:")]
        [SerializeField, RequiredField] private AudioSystemGroup[] _groups;
        [SerializeField, RequiredField] private AudioMixerSnapshot _defaultSnapshot;
        [SerializeField] private float _defaultTransitionDuration;

        [Inject] private SaveManager _saveManager;

        private List<AudioSource> _playingSources = new List<AudioSource>();
        private Dictionary<AudioMixerSnapshot, float> _playingSnapshots = new Dictionary<AudioMixerSnapshot, float>();
        private KeyValuePair<AudioMixerSnapshot, float> _defaultSnapshotPair
        {
            get { return new KeyValuePair<AudioMixerSnapshot, float>(_defaultSnapshot, 1f); }
        }

        public void Play(AudioSource source)
        {
            if (source != null)
            {
                source.Play();
                AddPlaying(source);
            }
        }

        public void Stop(AudioSource source)
        {
            if (source != null)
            {
                source.Stop();
                RemovePlaying(source);
            }
        }

        public void Init()
        {
            SaveGroup saveGroup = _saveManager.GetSaveGroup(_saveGroup);
            saveGroup.RegisterSynchronizable("AudioSystem", this);
        }

        protected void Update()
        {
            for (int index = _playingSources.Count - 1; index >= 0; index--)
            {
                AudioSource source = _playingSources[index];
                if (source == null || !source.isPlaying)
                {
                    RemovePlaying(source);
                }
            }
        }

        private void AddPlaying(AudioSource source)
        {
            if (!_playingSources.Contains(source))
            {
                _playingSources.Add(source);
                AddToGroup(source);
            }
        }

        private void RemovePlaying(AudioSource source)
        {
            _playingSources.Remove(source);
            RemoveFromGroup(source);
        }

        private void AddToGroup(AudioSource source)
        {
            if (source != null)
            {
                AudioSystemGroup group = FindGroup(source.outputAudioMixerGroup);
                if (group != null)
                {
                    group.AddSource(source, _playingSnapshots, _defaultSnapshotPair, _defaultTransitionDuration);
                }
            }
        }

        private void RemoveFromGroup(AudioSource source)
        {
            if (source != null)
            {
                AudioSystemGroup group = FindGroup(source.outputAudioMixerGroup);
                if (group != null)
                {
                    group.RemoveSource(source, _playingSnapshots, _defaultSnapshotPair, _defaultTransitionDuration);
                }
            }
        }

        private AudioSystemGroup FindGroup(AudioMixerGroup mixerGroup)
        {
            return _groups.FirstOrDefault(item => item.TargetAudioGroup == mixerGroup);
        }

        void ISynchronizable.Sync(State state)
        {
            foreach (AudioMixerExposedParameter parameter in _savedParameters)
            {
                float synchedValue = parameter.NormalizedValue;
                state.SyncFloat(parameter.ExposedParameterName, ref synchedValue);
                parameter.NormalizedValue = synchedValue;
            }
        }
    }
}