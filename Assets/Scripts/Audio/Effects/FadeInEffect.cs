﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Audio.Effects
{
    public class FadeInEffect : BaseAudioEffect
    {
        [SerializeField] private float _duration;
        [SerializeField] private AnimationCurve _curve = AnimationCurve.EaseInOut(0, 0, 1, 1);

        private float _startVolume;

        public override void PerformStart(AudioSource source)
        {
            _startVolume = source.volume;
        }

        public override void PerformUpdate(AudioSource source)
        {
            if (source.time.InRange(0, _duration))
            {
                float time = source.time / _duration;
                source.volume = _startVolume * _curve.Evaluate(time);
            }
        }

        public override void PerformStop(AudioSource source, IDeferredInvocationHandle invocation)
        {
            // DO HOTHING
        }
    }
}