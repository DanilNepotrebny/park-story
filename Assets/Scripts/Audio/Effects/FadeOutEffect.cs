﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Audio.Effects
{
    public class FadeOutEffect : BaseAudioEffect
    {
        [SerializeField] private float _duration;
        [SerializeField] private AnimationCurve _curve = AnimationCurve.EaseInOut(0, 1, 1, 0);

        private IDeferredInvocationHandle _stoping;
        private float _startVolume;
        private float _stopingTime;
        private float _startTime;
        private float _endTime;
        private float _currentTime;

        public override void PerformStart(AudioSource source)
        {
            _startVolume = source.volume;
        }

        public override void PerformUpdate(AudioSource source)
        {
            if (_stoping == null)
            {
                if (!source.loop)
                {
                    _startTime = source.clip.length - _duration;
                    _endTime = source.clip.length;
                    _currentTime = source.time;
                    EvaluateFading(source);
                }
            }
            else
            {
                _startTime = _stopingTime;
                _endTime = _startTime + _duration;
                _currentTime = Time.time;
                EvaluateFading(source);
            }

            if (_currentTime >= _endTime)
            {
                _stoping?.Unlock();
                _stoping = null;
            }
        }

        public override void PerformStop(AudioSource source, IDeferredInvocationHandle invocation)
        {
            _stoping = invocation.Lock();
            _stopingTime = Time.time;
        }

        private void EvaluateFading(AudioSource source)
        {
            if (_currentTime.InRange(_startTime, _endTime))
            {
                float normalizedTime = (_currentTime - _startTime) / (_endTime - _startTime);
                source.volume = _startVolume * _curve.Evaluate(normalizedTime);
            }
        }
    }
}