﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Invocation;
using UnityEngine;

namespace DreamTeam.Audio.Effects
{
    /// <summary>
    /// Base class to derive from if need ad some per source effect.
    /// It must be placed near the <see cref="AudioSource"/> passed to <see cref="AudioSystemPlayer"/> to be applied while playing
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public abstract class BaseAudioEffect : MonoBehaviour
    {
        /// <summary>
        /// Called when <see cref="AudioSystemPlayer"/> starts playing audio.
        /// </summary>
        /// <param name="source">started audio</param>
        /// <seealso cref="AudioSource"/>
        public abstract void PerformStart(AudioSource source);

        /// <summary>
        /// Called each frame while <see cref="AudioSystemPlayer"/> is playing audio.
        /// </summary>
        /// <param name="source">playing audio</param>
        /// <seealso cref="AudioSource"/>
        public abstract void PerformUpdate(AudioSource source);

        /// <summary>
        /// Called when <see cref="AudioSystemPlayer"/> stops playing audio.
        /// </summary>
        /// <param name="source">playing audio</param>
        /// <param name="invocation">stoping invokation</param>
        /// <seealso cref="AudioSource"/>
        /// <seealso cref="IDeferredInvocationHandle"/>
        public abstract void PerformStop(AudioSource source, IDeferredInvocationHandle invocation);
    }
}