﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Audio
{
    public class SceneBackgroundPlayerAgent : MonoBehaviour
    {
        [SerializeField, RequiredField] private AudioSourceProvider _provider;

        [Inject(Id = ProjectInstaller.BackgroundMusicPlayerId)]
        private AudioSourceProviderController _controller;

        protected void OnEnable()
        {
            _controller.SetProvider(_provider);
            _controller.PlayNext();
        }
    }
}