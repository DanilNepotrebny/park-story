﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Audio
{
    [RequireComponent(typeof(Toggle))]
    public class AudioMuteToggle : MonoBehaviour
    {
        [SerializeField, RequiredField] private AudioMixerExposedParameter _paranmeter;
        [SerializeField] private bool _isInvertedToggle;

        private Toggle _toggle => GetComponent<Toggle>();

        protected void OnEnable()
        {
            _toggle.isOn = _paranmeter.NormalizedValue <= 0 ^ _isInvertedToggle;
            _toggle.onValueChanged.AddListener(OnToggleValueChanged);
        }

        protected void OnDisable()
        {
            _toggle.onValueChanged.RemoveListener(OnToggleValueChanged);
        }

        private void OnToggleValueChanged(bool isOn)
        {
            _paranmeter.NormalizedValue = isOn ^ _isInvertedToggle ? 0 : 1;
        }
    }
}