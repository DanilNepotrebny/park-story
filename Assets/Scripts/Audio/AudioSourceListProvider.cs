﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Audio
{
    public class AudioSourceListProvider : AudioSourceProvider
    {
        [SerializeField] private bool _isLooped;
        [SerializeField] private List<AudioSourceProvider> _children;

        private int _currentIndex;

        public override AudioSource Current => _currentChild?.Current;
        public override int Count => _children.Count;

        private IEnumerator<AudioSource> _currentChild =>
            _currentIndex < _children.Count ? _children[_currentIndex] : null;

        protected override bool MoveNextEnumerator()
        {
            _currentIndex = _isLooped ? ++_currentIndex % _children.Count : ++_currentIndex;
            _currentChild?.MoveNext();
            return _currentIndex <= Count;
        }

        protected override void ResetEnumerator()
        {
            _currentChild?.Reset();
            _currentIndex = 0;
        }
    }
}