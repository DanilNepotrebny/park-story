﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Audio
{
    public abstract class AudioSourceProvider : MonoBehaviour, IEnumerator<AudioSource>, IReadOnlyCollection<AudioSource>
    {
        public abstract AudioSource Current { get; }
        object IEnumerator.Current => Current;
        public abstract int Count { get; }

        public IEnumerator<AudioSource> GetEnumerator()
        {
            return this;
        }

        protected abstract bool MoveNextEnumerator();
        protected abstract void ResetEnumerator();

        bool IEnumerator.MoveNext()
        {
            return MoveNextEnumerator();
        }

        void IEnumerator.Reset()
        {
            ResetEnumerator();
        }

        void IDisposable.Dispose()
        {
            ResetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}