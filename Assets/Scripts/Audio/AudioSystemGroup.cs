﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Audio;

namespace DreamTeam.Audio
{
    public class AudioSystemGroup : MonoBehaviour
    {
        [SerializeField, RequiredField] private AudioMixerGroup _targetAudioGroup;

        [Header("Limitation"), SerializeField] private int _maxAvailableSources;

        [Header("Mixing"), SerializeField] private AudioMixerSnapshot _snapshot;
        [SerializeField, Range(0, 1)] private float _weight = 1;
        [SerializeField] private float _transitionDuration;

        public AudioMixerGroup TargetAudioGroup => _targetAudioGroup;

        private List<AudioSource> _appliedSources = new List<AudioSource>();
        private int _sourcesLimit => _maxAvailableSources > 0 ? _maxAvailableSources : int.MaxValue;

        public void AddSource(
                AudioSource source,
                IDictionary<AudioMixerSnapshot, float> playingSnapshots,
                KeyValuePair<AudioMixerSnapshot, float> defaultSnapshot,
                float defaultTransitionDuration
            )
        {
            _appliedSources.Add(source);
            UpdateMuting();
            AddSnapshot(source, playingSnapshots);
            ApplyMixerTransition(source, playingSnapshots, defaultSnapshot, defaultTransitionDuration);
        }

        public void RemoveSource(
                AudioSource source,
                IDictionary<AudioMixerSnapshot, float> playingSnapshots,
                KeyValuePair<AudioMixerSnapshot, float> defaultSnapshot,
                float defaultTransitionDuration
            )
        {
            _appliedSources.Remove(source);
            UpdateMuting();
            RemoveSnapshot(source, playingSnapshots);
            ApplyMixerTransition(source, playingSnapshots, defaultSnapshot, defaultTransitionDuration);
        }

        private void UpdateMuting()
        {
            for (int index = 0; index < _appliedSources.Count; index++)
            {
                _appliedSources[index].mute = index >= _sourcesLimit;
            }
        }

        private void AddSnapshot(AudioSource source, IDictionary<AudioMixerSnapshot, float> playingSnapshots)
        {
            if (_snapshot == null)
            {
                return;
            }

            if (playingSnapshots.ContainsKey(_snapshot))
            {
                playingSnapshots[_snapshot] += _weight;
            }
            else
            {
                playingSnapshots.Add(_snapshot, _weight);
            }
        }

        private void RemoveSnapshot(AudioSource source, IDictionary<AudioMixerSnapshot, float> playingSnapshots)
        {
            if (_snapshot == null)
            {
                return;
            }

            if (playingSnapshots.ContainsKey(_snapshot))
            {
                playingSnapshots[_snapshot] -= _weight;
                if (playingSnapshots[_snapshot] <= 0)
                {
                    playingSnapshots.Remove(_snapshot);
                }
            }
        }

        private void ApplyMixerTransition(
                AudioSource source,
                IDictionary<AudioMixerSnapshot, float> playingSnapshots,
                KeyValuePair<AudioMixerSnapshot, float> defaultSnapshot,
                float defaultTransitionDuration
            )
        {
            if (playingSnapshots.Count == 0)
            {
                source.outputAudioMixerGroup?.audioMixer.TransitionToSnapshots(
                        new[] { defaultSnapshot.Key },
                        new[] { defaultSnapshot.Value },
                        defaultTransitionDuration
                    );
            }
            else
            {
                source.outputAudioMixerGroup?.audioMixer.TransitionToSnapshots(
                        playingSnapshots.Keys.ToArray(),
                        playingSnapshots.Values.ToArray(),
                        _transitionDuration
                    );
            }
        }
    }
}