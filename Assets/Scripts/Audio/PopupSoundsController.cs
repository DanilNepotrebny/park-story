﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using UnityEngine;
using Zenject;

namespace DreamTeam.Audio
{
    public class PopupSoundsController : MonoBehaviour
    {
        [SerializeField] private AudioSystemPlayer _showingSound;
        [SerializeField] private AudioSystemPlayer _closingSound;

        [Inject] private Popup _popup;

        protected void OnEnable()
        {
            _popup.Showing += OnPopupShowing;
            _popup.Hiding += OnPopupHiding;
        }

        protected void OnDisable()
        {
            _popup.Showing -= OnPopupShowing;
            _popup.Hiding -= OnPopupHiding;
        }

        private void OnPopupHiding(Popup popup)
        {
            _closingSound?.Play();
        }

        private void OnPopupShowing(Popup popup)
        {
            _showingSound?.Play();
        }
    }
}