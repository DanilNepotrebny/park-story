﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class SingleAudioSourceProvider : AudioSourceProvider
    {
        public override AudioSource Current => _isMoved ? GetComponent<AudioSource>() : null;
        public override int Count => 1;
        private bool _isMoved = false;

        protected override void ResetEnumerator()
        {
            _isMoved = false;
        }

        protected override bool MoveNextEnumerator()
        {
            if (!_isMoved)
            {
                _isMoved = true;
                return true;
            }

            return false;
        }
    }
}