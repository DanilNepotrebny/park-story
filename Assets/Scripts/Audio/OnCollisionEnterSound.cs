﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Audio
{
    [RequireComponent(typeof(Collider))]
    public class OnCollisionEnterSound : MonoBehaviour
    {
        [SerializeField, RequiredField] private AudioSystemPlayer _soundPlayer;

        protected void OnCollisionEnter(Collision collision)
        {
            _soundPlayer.Play();
        }
    }
}