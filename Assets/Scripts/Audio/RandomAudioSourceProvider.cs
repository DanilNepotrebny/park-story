﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Audio
{
    public class RandomAudioSourceProvider : AudioSourceProvider
    {
        [SerializeField] private ItemsList _items;

        private AudioSource _currentSource;
        private int _movesCount;

        public override AudioSource Current
        {
            get
            {
                if (_currentSource == null)
                {
                    MoveNextEnumerator();
                }

                return _currentSource;
            }
        }

        public override int Count => _items.Count;

        protected override bool MoveNextEnumerator()
        {
            _currentSource = RandomUtils.RandomElement(_items);
            _movesCount++;
            return _movesCount < Count;
        }

        protected override void ResetEnumerator()
        {
            _currentSource = null;
            _movesCount = 0;
        }

        [Serializable]
        private class ItemsList : KeyValueList<AudioSource, int>
        {
        }
    }
}