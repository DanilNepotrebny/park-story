﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using UnityEngine;
using Zenject;

namespace DreamTeam.CustomerSupport
{
    public class CustomerSupportController : MonoBehaviour
    {
        [Inject] private CustomerSupportSystem _customerSupportSystem;

        [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
        public void ShowFAQ()
        {
            _customerSupportSystem.ShowFAQ();
        }

        [Obfuscation(Exclude = false, Feature = "-rename", ApplyToMembers = false)]
        public void ShowConversation()
        {
            _customerSupportSystem.ShowConversation();
        }
    }
}