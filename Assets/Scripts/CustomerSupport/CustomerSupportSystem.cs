﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Analytics;
using Helpshift;
using UnityEngine;
using Zenject;

namespace DreamTeam.CustomerSupport
{
    public class CustomerSupportSystem : MonoBehaviour, IInitable, IDisposable
    {
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private UserSettings _userSettings;

        private HelpshiftSdk _helpshift;

        /// <summary>
        /// Open native window with FAQ
        /// </summary>
        public void ShowFAQ()
        {
            if (_helpshift != null)
            {
                _helpshift.showFAQs();
            }
            else
            {
                UnityEngine.Debug.LogWarning("Can't show FAQ. Helpshift is possibly not supported on this platform.");
            }
        }

        /// <summary>
        /// Open native window where user can communicate directly with customer support agent
        /// </summary>
        public void ShowConversation()
        {
            if (_helpshift != null)
            {
                _helpshift.showConversation();
            }
            else
            {
                UnityEngine.Debug.LogWarning(
                        "Can't connect with customer support. Helpshift is possibly not supported on this platform."
                    );
            }
        }

        private void OnUserIdInitialized()
        {
            HelpshiftUser user = new HelpshiftUser
                .Builder(_analyticsSystem.UserId, "")
                .setName(_userSettings.UserName)
                .build();

            _helpshift?.login(user);
        }

        void IInitable.Init()
        {
            _helpshift = HelpshiftSdk.getInstance();

            _analyticsSystem.UserIdInitialized += OnUserIdInitialized;
        }

        void IDisposable.Dispose()
        {
            _analyticsSystem.UserIdInitialized -= OnUserIdInitialized;

            _helpshift?.logout();
        }
    }
}