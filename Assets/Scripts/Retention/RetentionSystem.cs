﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.KeyWordsReplacement;
using DreamTeam.Localization;
using DreamTeam.Notifications;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Retention
{
    public class RetentionSystem : MonoBehaviour, ISynchronizable, ILocalNotificationsProvider, IInitable
    {
        [SerializeField] private List<RetentionNotification> _notifications = new List<RetentionNotification>();

        [Inject] private NotificationsSystem _notificationsSystem;
        [Inject] private LocalizationSystem _localizationSystem;
        [Inject] private KeyWordsReplacementSystem _keyWordsReplacementSystem;
        [Inject] private SaveGroup _saveGroup;

        public void Init()
        {
            _saveGroup.RegisterSynchronizable(nameof(RetentionSystem), this);
        }

        protected void Awake()
        {
            _notificationsSystem.RegisterLocalNotificationsProvider(this);
        }

        protected void OnDestroy()
        {
            _notificationsSystem.UnRegisterLocalNotificationsProvider(this);
        }

        void ISynchronizable.Sync(State state)
        {
            
        }

        IEnumerable<NotificationsSystem.LocalNotificationParams> ILocalNotificationsProvider.Provide()
        {            
            List<NotificationsSystem.LocalNotificationParams> result = new List<NotificationsSystem.LocalNotificationParams>();
            foreach (RetentionNotification notification in _notifications)
            {
                string title = _keyWordsReplacementSystem.ReplaceKeyWords(_localizationSystem.Localize(notification.TitleKey, this));
                string message = _keyWordsReplacementSystem.ReplaceKeyWords(_localizationSystem.Localize(notification.MessageKey, this));

                NotificationsSystem.LocalNotificationParams parameters = new NotificationsSystem.LocalNotificationParams
                {
                    TriggerTime = DateTime.Now + notification.Schedule,
                    Title = title,
                    Message = message,
                    UserData = notification.CustomData.ToDictionary(pair => pair.Key, pair => pair.Value),
                    SoundName = notification.SoundName
                };

                result.Add(parameters);
            }

            return result;
        }

        [Serializable]
        private class RetentionNotification
        {
            [SerializeField] private TimeTicks _schedule;
            [SerializeField, LocalizationKey] private string _titleKey;
            [SerializeField, LocalizationKey] private string _messageKey;
            [SerializeField] private NotificationData _customData;
            [SerializeField] private string _soundName;

            public TimeSpan Schedule => _schedule;
            public string TitleKey => _titleKey;
            public string MessageKey => _messageKey;
            public NotificationData CustomData => _customData;
            public string SoundName => _soundName;
        }

        [Serializable]
        private class NotificationData : KeyValueList<string, string>
        {
        }
    }
}