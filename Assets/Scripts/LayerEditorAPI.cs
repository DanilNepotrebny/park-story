﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DreamTeam.Location.Isometry;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

namespace DreamTeam
{
    public partial class Layer : ILayer
    {
        private MaterialPropertyBlock _propertyBlock;
        private readonly int _colorPropertyID = Shader.PropertyToID("_Color");
        private List<GameObject> _elements = new List<GameObject>();
        private List<GameObject> _prevElements = new List<GameObject>();
        private HashSet<GameObject> _removedElements = new HashSet<GameObject>();

        public IEnumerable<Transform> Children
        {
            get
            {
                foreach (Transform t in transform)
                {
                    yield return t;
                }
            }
        }

        /// <summary>
        /// Set alpha value to layer element's renderers
        /// </summary>
        public void SetAlpha(float alpha)
        {
            if (_propertyBlock == null)
            {
                _propertyBlock = new MaterialPropertyBlock();
            }

            Renderer[] renderers = GetComponentsInChildren<Renderer>();
            foreach (Renderer r in renderers)
            {
                r.GetPropertyBlock(_propertyBlock);
                Color color = r.sharedMaterial.color;
                color.a = alpha;
                _propertyBlock.SetColor(_colorPropertyID, color);
                r.SetPropertyBlock(_propertyBlock);
            }
        }

        public void Add(GameObject go)
        {
            go.transform.SetParent(transform);
        }

        public void Remove(GameObject go)
        {
            go.transform.SetParent(null);
        }

        public float GetZPosition()
        {
            return gameObject.transform.position.z;
        }

        public void UpdateElements()
        {
            CollectElements();

            foreach (GameObject element in _elements)
            {
                Undo.RecordObject(element, "Layer element modification");

                UpdateElement(element);
            }

            switch (_sortingMode)
            {
                case Mode.Ordered:
                    UpdateOrderedElements(_elements);
                    break;
                case Mode.Isometric:
                    UpdateIsometricElements(_elements);
                    break;
                default:
                    throw new InvalidEnumArgumentException();
            }

            if (_prevElements != null)
            {
                foreach (GameObject element in _prevElements.Except(_elements).Except(_removedElements))
                {
                    OnElementRemove(element);

                    _removedElements.Add(element);
                }
            }

            List<GameObject> tmp = _prevElements;
            _prevElements = _elements;
            _elements = tmp;
            _elements.Clear();

            EditorApplication.delayCall -= ClearRemovedElementsList;
            EditorApplication.delayCall += ClearRemovedElementsList;
        }

        protected void OnValidate()
        {
            if (!EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                UpdateElementsDelayed();
            }
        }

        protected void OnDisable()
        {
            EditorApplication.delayCall -= UpdateElements;
        }

        protected void OnTransformChildrenChanged()
        {
            UpdateElementsDelayed();
        }

        private void UpdateElementsDelayed()
        {
            EditorApplication.delayCall -= UpdateElements;
            EditorApplication.delayCall += UpdateElements;
        }

        private void ClearRemovedElementsList()
        {
            _removedElements.Clear();
        }

        private void CollectElements()
        {
            Stack<Transform> stackToProcess = new Stack<Transform>();

            stackToProcess.Push(transform);

            while (stackToProcess.Count > 0)
            {
                Transform t = stackToProcess.Pop();

                bool needToProcessChildren = true;
                if (t != transform)
                {
                    _elements.Add(t.gameObject);

                    if (t.GetComponent<SortingGroup>() != null)
                    {
                        needToProcessChildren = false;
                    }
                    else
                    {
                        Renderer r = t.GetComponent<Renderer>();
                        if (r != null)
                        {
                            MeshRenderer meshRenderer = t.GetComponent<Renderer>() as MeshRenderer;
                            if (meshRenderer != null &&
                                meshRenderer.sharedMaterials.Length > 1 &&
                                t.GetComponent<SortingGroup>() == null)
                            {
                                Undo.AddComponent<SortingGroup>(t.gameObject);
                                needToProcessChildren = false;
                            }
                        }
                    }
                }

                if (needToProcessChildren)
                {
                    for (int i = t.childCount - 1; i >= 0; i--)
                    {
                        stackToProcess.Push(t.GetChild(i));
                    }
                }
            }
        }

        private void UpdateOrderedElements(List<GameObject> elements)
        {
            int sortingOrder = 0;

            foreach (GameObject element in elements)
            {
                SortingGroup sortingGroup = element.GetComponent<SortingGroup>();

                if (sortingGroup != null)
                {
                    sortingGroup.sortingOrder = sortingOrder;
                }
                else
                {
                    Renderer r = element.GetComponent<Renderer>();
                    if (r == null)
                    {
                        continue;
                    }

                    r.sortingOrder = sortingOrder;
                }

                sortingOrder++;
            }
        }

        private void UpdateIsometricElements(List<GameObject> elements)
        {
            foreach (GameObject element in elements)
            {
                var sorter = element.GetComponent<IsometrySorter>();
                if (sorter != null && !IsEditorOnlyComponent(sorter) ||
                    element.GetComponent<SortingGroup>() == null && element.GetComponent<Renderer>() == null)
                {
                    continue;
                }

                IsometryObjectGroup group = FindObjectGroup(element);

                var positionChangeNotifier = EnsureEditorOnlyComponentExists<PositionChangeNotifier>(
                        group?.gameObject ?? element
                    );

                sorter = EnsureEditorOnlyComponentExists<IsometrySorter>(element);
                sorter.PositionProvider = positionChangeNotifier;
                sorter.UpdatePosition();
            }
        }

        private IsometryObjectGroup FindObjectGroup(GameObject element)
        {
            IsometryObjectGroup[] objectGroups = element.GetComponentsInParent<IsometryObjectGroup>(true);
            return objectGroups.FirstOrDefault(group => !group.IsDestroying);
        }

        private void UpdateElement([NotNull] GameObject element)
        {
            SortingGroup sortingGroup = element.GetComponent<SortingGroup>();

            if (sortingGroup != null)
            {
                sortingGroup.sortingLayerID = _sortingLayer;
            }
            else
            {
                Renderer r = element.GetComponent<Renderer>();
                if (r)
                {
                    r.sortingLayerID = _sortingLayer;
                }
            }

            var parentChangeNotifier = EnsureEditorOnlyComponentExists<TransformParentChangeNotifier>(element);
            parentChangeNotifier.TransformParentChanged -= OnElementTransformParentChanged;
            parentChangeNotifier.TransformParentChanged += OnElementTransformParentChanged;

            var childrenChangeNotifier = EnsureEditorOnlyComponentExists<TransformChildrenChangeNotifier>(element);
            childrenChangeNotifier.TransformChildrenChanged -= UpdateElementsDelayed;
            childrenChangeNotifier.TransformChildrenChanged += UpdateElementsDelayed;

            EnsureNoZTestMaterials(element);
        }

        private void OnElementTransformParentChanged([NotNull] Transform t)
        {
            if (!t.IsChildOf(transform))
            {
                OnElementRemove(t.gameObject);

                _removedElements.Add(t.gameObject);
            }
        }

        private void OnElementRemove([NotNull] GameObject go)
        {
            var parentChangeNotifier = go.GetComponent<TransformParentChangeNotifier>();
            if (parentChangeNotifier != null)
            {
                parentChangeNotifier.TransformParentChanged -= OnElementTransformParentChanged;
                RemoveEditorOnlyComponent<TransformParentChangeNotifier>(go);
            }

            var childrenChangeNotifier = go.GetComponent<TransformChildrenChangeNotifier>();
            if (childrenChangeNotifier != null)
            {
                childrenChangeNotifier.TransformChildrenChanged -= UpdateElementsDelayed;
                RemoveEditorOnlyComponent<TransformChildrenChangeNotifier>(go);
            }

            if (_sortingMode == Mode.Isometric)
            {
                RemoveEditorOnlyComponent<IsometrySorter>(go);
                RemoveEditorOnlyComponent<PositionChangeNotifier>(go);
            }
        }

        private T EnsureEditorOnlyComponentExists<T>(GameObject go) where T : MonoBehaviour
        {
            return go.EnsureComponentExists<T>(component => component.hideFlags = HideFlags.HideAndDontSave);
        }

        private bool IsEditorOnlyComponent([NotNull] MonoBehaviour component)
        {
            return (component.hideFlags & HideFlags.HideAndDontSave) == HideFlags.HideAndDontSave;
        }

        private void RemoveEditorOnlyComponent<T>(GameObject go) where T : MonoBehaviour
        {
            var component = go.GetComponent<T>();
            if (component != null && IsEditorOnlyComponent(component))
            {
                component.Dispose();
            }
        }

        private void EnsureNoZTestMaterials(GameObject go)
        {
            var renderers = go.GetComponentsInChildren<Renderer>(true);

            foreach (Renderer r in renderers)
            {
                for (int i = 0; i < r.sharedMaterials.Length; i++)
                {
                    Material currentMaterial = r.sharedMaterials[i];

                    if (currentMaterial != null)
                    {
                        EnsureNoSpriteMaterial(currentMaterial, i, r);
                        EnsureNoSpineMaterial(currentMaterial, r);
                    }
                }
            }
        }

        private void EnsureNoSpriteMaterial(Material m, int materialIdx, Renderer r)
        {
            const string defaultSpriteMaterialName = "Sprites-Default";
            const string spriteMaterialPath = "Assets/Materials/Sprites-NoZTest.mat";

            if (m.name == defaultSpriteMaterialName)
            {
                if (!EditorUtils.IsPlayingOrWillChangePlaymode())
                {
                    var material = AssetDatabase.LoadAssetAtPath<Material>(spriteMaterialPath);
                    if (material != null)
                    {
                        var so = new SerializedObject(r);
                        var prop = so.FindProperty("m_Materials").GetArrayElementAtIndex(materialIdx);
                        prop.objectReferenceValue = material;
                        so.ApplyModifiedPropertiesWithDirtyFlag();
                    }
                    else
                    {
                        UnityEngine.Debug.LogError($"Can't find {spriteMaterialPath} material", this);
                    }
                }
                else
                {
                    string errorMessage =
                        $"You can't use {defaultSpriteMaterialName} inside {nameof(Layer)} because of " +
                        $"visual glitches with sorting order. Please, use {spriteMaterialPath} insted";

                    UnityEngine.Debug.LogError(errorMessage);
                }
            }
        }

        private void EnsureNoSpineMaterial(Material m, Renderer r)
        {
            const string defaultSpineShaderName = "Custom/Skeleton-AlphaBlend";
            const string spineShaderPath = "Assets/Materials/Shaders/Skeleton-NoZTest.shader";

            if (m.shader.name == defaultSpineShaderName)
            {
                if (!EditorUtils.IsPlayingOrWillChangePlaymode())
                {
                    var shader = AssetDatabase.LoadAssetAtPath<Shader>(spineShaderPath);
                    if (shader != null)
                    {
                        var so = new SerializedObject(r);
                        m.shader = shader;
                        so.ApplyModifiedPropertiesWithDirtyFlag();
                    }
                    else
                    {
                        UnityEngine.Debug.LogError($"Can't find {spineShaderPath} shader", this);
                    }
                }
                else
                {
                    string errorMessage =
                        $"You can't use {defaultSpineShaderName} inside {nameof(Layer)} because of " +
                        $"visual glitches with sorting order. Please, use {spineShaderPath} insted";

                    UnityEngine.Debug.LogError(errorMessage);
                }
            }
        }

        #region Inner types

        public struct SortingLayerComparer : IComparer<Layer>
        {
            public int Compare(Layer x, Layer y)
            {
                return x._sortingLayer.CompareTo(y._sortingLayer);
            }
        }

        #endregion Inner types
    }
}

#endif // UNITY_EDITOR