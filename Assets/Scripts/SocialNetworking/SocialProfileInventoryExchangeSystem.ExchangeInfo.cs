// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Inventory.Items;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.Nodes;

namespace DreamTeam.SocialNetworking
{
    public partial class SocialProfileInventoryExchangeSystem
    {
        private class ExchangeInfo
        {
            private Dictionary<CountedItem, Timestamp> _register = new Dictionary<CountedItem, Timestamp>();

            public void PushSendItemRecord(CountedItem item)
            {
                if (!_register.ContainsKey(item))
                {
                    _register.Add(item, new Timestamp());
                }
                _register[item].LastSendTime = DateTime.UtcNow;
            }

            public void PushAskItemRecord(CountedItem item)
            {
                if (!_register.ContainsKey(item))
                {
                    _register.Add(item, new Timestamp());
                }
                _register[item].LastAskTime = DateTime.UtcNow;
            }

            public bool CanSendItem(CountedItem item)
            {
                Timestamp timeStamp;
                return !_register.TryGetValue(item, out timeStamp) || IsDayElapsedFrom(timeStamp.LastSendTime);
            }

            public bool CanAskItem(CountedItem item)
            {
                Timestamp time;
                return !_register.TryGetValue(item, out time) || IsDayElapsedFrom(time.LastAskTime);
            }

            public static Node ToNode(ExchangeInfo info, ISynchronizer<CountedItem> itemSynchronizer)
            {
                var result = new ListNode();
                foreach (KeyValuePair<CountedItem, Timestamp> dataPair in info._register)
                {
                    var dataPairNode = new DictionaryNode();

                    dataPairNode.SetNode("key", itemSynchronizer.Serialize(dataPair.Key));
                    dataPairNode.SetNode("value", Timestamp.ToNode(dataPair.Value));

                    result.AddNode(dataPairNode);
                }
                return result;
            }

            public static ExchangeInfo FromNode(Node node, ISynchronizer<CountedItem> itemSynchronizer)
            {
                var result = new ExchangeInfo();
                var list = node as ListNode;
                if (list != null)
                {
                    for (int index = 0; index < list.Count; index++)
                    {
                        DictionaryNode listItem = list.GetDictionary(index);
                        CountedItem item = itemSynchronizer.Deserialize(listItem.GetNode("key"));
                        Timestamp timeStamps = Timestamp.FromNode(listItem.GetNode("value"));
                        result._register.Add(item, timeStamps);
                    }
                }
                return result;
            }

            private bool IsDayElapsedFrom(DateTime dateTime)
            {
                TimeSpan delta = DateTime.UtcNow - dateTime;
                return delta.Days > 0 || delta.Hours > 0;
            }

            public class Timestamp
            {
                public DateTime LastAskTime;
                public DateTime LastSendTime;

                private const string _lastAskTimeId = "LastAskTime";
                private const string _lastSendTimeId = "LastSendTime";

                public static Node ToNode(Timestamp timeStamps)
                {
                    var result = new DictionaryNode();
                    result.SetDateTime(_lastAskTimeId, timeStamps.LastAskTime);
                    result.SetDateTime(_lastSendTimeId, timeStamps.LastSendTime);
                    return result;
                }

                public static Timestamp FromNode(Node node)
                {
                    var result = new Timestamp();
                    var dictionary = node as DictionaryNode;
                    if (dictionary != null)
                    {
                        result.LastAskTime = dictionary.GetDateTime(_lastAskTimeId);
                        result.LastSendTime = dictionary.GetDateTime(_lastSendTimeId);
                    }
                    return result;
                }
            }
        }
    }
}