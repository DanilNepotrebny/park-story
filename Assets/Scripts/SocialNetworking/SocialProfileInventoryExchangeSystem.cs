﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Localization;
using DreamTeam.Messaging;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.Nodes;
using DreamTeam.Synchronization.Serializers;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Zenject;

namespace DreamTeam.SocialNetworking
{
    public partial class SocialProfileInventoryExchangeSystem :
        MonoBehaviour,
        ISynchronizable,
        ISynchronizer<SocialProfileInventoryExchangeSystem>,
        IInitable
    {
        [Inject] private SocialProfilesSystem _socialProfilesSystem;
        [Inject] private MessageSystem _messageSystem;
        [Inject] private JsonNodeSerializer _serializer;
        [Inject] private ISynchronizer<CountedItem> _countedItemSynchronizer;
        [Inject] private SaveGroup _saveGroup;
        [Inject] private Instantiator _instantiator;
        [Inject] private LocalizationSystem _localizationSystem;

        private Dictionary<string, ExchangeInfo> _register = new Dictionary<string, ExchangeInfo>();

        public event Action<SocialProfile> ProfileRegistered;
        public event Action<SocialProfile> ProfileUnregistered;
        public event Action<SocialProfile, List<Reward>> RewardsAsked;
        public event Action<SocialProfile, List<Reward>> RewardsSent;

        public bool CanSendItem(SocialProfile profile, CountedItem item, int amount)
        {
            if (!profile.Equals(SocialProfile.Invalid) && _register.ContainsKey(profile.ID))
            {
                ExchangeInfo exchangeInfo = _register[profile.ID];
                return exchangeInfo.CanSendItem(item);
            }

            return false;
        }

        public bool CanAskItem(SocialProfile profile, CountedItem item, int amount)
        {
            if (!profile.Equals(SocialProfile.Invalid) && _register.ContainsKey(profile.ID))
            {
                ExchangeInfo exchangeInfo = _register[profile.ID];
                return exchangeInfo.CanAskItem(item);
            }

            return false;
        }

        public void SendItem(SocialProfile profile, CountedItem item, int amount)
        {
            if (CanSendItem(profile, item, amount))
            {
                Reward reward = CountedItemFixedReward.Create(item, amount, _instantiator);
                SendRewards(
                        profile,
                        new List<Reward> { reward },
                        () => { _register[profile.ID].PushSendItemRecord(item); }
                    );
            }
        }

        public void AskItem(SocialProfile profile, CountedItem item, int amount)
        {
            if (CanAskItem(profile, item, amount))
            {
                Reward reward = CountedItemFixedReward.Create(item, amount, _instantiator);
                AskRewards(
                        profile,
                        new List<Reward> { reward },
                        () => { _register[profile.ID].PushAskItemRecord(item); }
                    );
            }
        }

        public void SendRewards(SocialProfile profile, List<Reward> rewards, Action sentCallback)
        {
            RewardsMessage message = new RewardsMessage(_instantiator, rewards, GamePlace.Friend);
            IMessage messageInterface = message;

            SerializationState state = new SerializationState();
            _messageSystem.SyncMessage(state, ref messageInterface);
            JObject data = _serializer.CreateToken(state.Root) as JObject;

            _socialProfilesSystem.SendMessage(
                    profile,
                    message.Title,
                    message.Message,
                    data,
                    isSent =>
                    {
                        if (isSent)
                        {
                            sentCallback?.Invoke();
                            RewardsSent?.Invoke(profile, rewards);
                        }
                    }
                );
        }

        public void AskRewards(SocialProfile profile, List<Reward> rewards, Action askedCallback)
        {
            AskRewardsMessage message = new AskRewardsMessage(_instantiator, rewards);
            IMessage messageInterface = message;

            SerializationState state = new SerializationState();
            _messageSystem.SyncMessage(state, ref messageInterface);
            JObject data = _serializer.CreateToken(state.Root) as JObject;

            _socialProfilesSystem.SendMessage(
                    profile,
                    message.Title,
                    message.Message,
                    data,
                    isSent =>
                    {
                        if (isSent)
                        {
                            askedCallback?.Invoke();
                            RewardsAsked?.Invoke(profile, rewards);
                        }
                    }
                );
        }

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("SocialProfileInventoryExchangeSystem", this);
        }

        protected void Awake()
        {
            foreach (SocialProfile profile in _socialProfilesSystem.SocialProfiles)
            {
                RegisterProfile(profile);
            }

            _socialProfilesSystem.ProfileRegistered += RegisterProfile;
            _socialProfilesSystem.ProfileUnRegistered += UnRegisterProfile;
        }

        protected void OnDestroy()
        {
            _socialProfilesSystem.ProfileRegistered -= RegisterProfile;
            _socialProfilesSystem.ProfileUnRegistered -= UnRegisterProfile;
        }

        private void RegisterProfile(SocialProfile profile)
        {
            if (!_register.ContainsKey(profile.ID) && profile.IsVisible)
            {
                _register.Add(profile.ID, new ExchangeInfo());
                ProfileRegistered?.Invoke(profile);
            }
        }

        private void UnRegisterProfile(SocialProfile profile)
        {
            if (_register.ContainsKey(profile.ID))
            {
                _register.Remove(profile.ID);
                ProfileUnregistered?.Invoke(profile);
            }
        }

        Node ISynchronizer<SocialProfileInventoryExchangeSystem>.Serialize(SocialProfileInventoryExchangeSystem target)
        {
            var historyNode = new ListNode();
            foreach (KeyValuePair<string, ExchangeInfo> historyPair in target._register)
            {
                var historyPairNode = new DictionaryNode();
                historyPairNode.SetNode("key", new ValueNode(historyPair.Key));
                historyPairNode.SetNode("value", ExchangeInfo.ToNode(historyPair.Value, _countedItemSynchronizer));
                historyNode.AddNode(historyPairNode);
            }

            return historyNode;
        }

        SocialProfileInventoryExchangeSystem ISynchronizer<SocialProfileInventoryExchangeSystem>.Deserialize(Node node)
        {
            var list = (ListNode)node;
            for (int index = 0; index < list.Count; index++)
            {
                DictionaryNode listItem = list.GetDictionary(index);
                string id = ((ValueNode)listItem.GetNode("key")).GetString();
                ExchangeInfo info = ExchangeInfo.FromNode(listItem.GetNode("value"), _countedItemSynchronizer);
                if (_register.ContainsKey(id))
                {
                    _register[id] = info;
                }
                else
                {
                    _register.Add(id, info);
                }
            }

            return this;
        }

        void ISynchronizable.Sync(State state)
        {
            SocialProfileInventoryExchangeSystem syncRef = this;
            state.SyncObject("register", ref syncRef, this);
        }
    }
}