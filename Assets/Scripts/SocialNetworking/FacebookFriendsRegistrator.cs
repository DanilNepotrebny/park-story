﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DreamTeam.SocialNetworking.Facebook;
using DreamTeam.SocialNetworking.Facebook.Request;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Zenject;

namespace DreamTeam.SocialNetworking
{
    public class FacebookFriendsRegistrator : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private TimeTicks _friendsRequestInterval;
        [SerializeField] private TimeTicks _avatarDownloadInterval;
        [SerializeField] private FacebookFriendsDefaultData _friendsDefaultData;
        [SerializeField] private int _avatarTextureSize = 512;

        [Inject] private FacebookManager _facebookManager;
        [Inject] private SocialProfilesSystem _socialProfilesSystem;
        [Inject] private Instantiator _instantiator;
        [Inject] private SaveGroup _saveGroup;
        
        private Dictionary<string, SocialProfile> _cachedFacebookFriends = new Dictionary<string, SocialProfile>();
        private DateTime _lastFriendsRequestTime = DateTime.MinValue;

        private DateTime _lastAvatarRequestTime = DateTime.MinValue;
        private Coroutine _avatarsCoroutine = null;
        private List<string> _serializedAvatars = new List<string>();
        private Queue<string> _profilesAvatarsQueue = new Queue<string>();
        private Dictionary<string, Sprite> _loadedAvatars = new Dictionary<string, Sprite>();
        private Dictionary<int, CachedMessageData> _cachedMessages = new Dictionary<int, CachedMessageData>();

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("FacebookFriendsRegistrator", this);
        }

        protected void Start()
        {
            #if !UNITY_EDITOR
            {
                StartCoroutine(FriendsRequestingCoroutine());
                StartCoroutine(MessagesSendingCoroutine());
            }
            #endif // UNITY_EDITOR
        }

        private IEnumerator FriendsRequestingCoroutine()
        {
            while (true)
            {
                yield return new WaitUntil(() => _facebookManager.IsLoggedIn && !_facebookManager.HasProcessingRequests);
                yield return new WaitUntil(() => DateTime.Now - _lastFriendsRequestTime > _friendsRequestInterval);
                if (_facebookManager.IsLoggedIn && !_facebookManager.HasProcessingRequests)
                {
                    _lastFriendsRequestTime = DateTime.Now;
                    RequestFriendsList request = _facebookManager.RequestPlayingFriendsList();
                    yield return request;
                    if (request.Result != null &&
                        !request.Result.Cancelled &&
                        string.IsNullOrEmpty(request.Result.Error))
                    {
                        Dictionary<string, SocialProfile> newFriends;
                        ParseFriendsRequestResponse(request.Result.RawResult, out newFriends);
                        foreach (var pair in newFriends)
                        {
                            if (!_serializedAvatars.Contains(pair.Value.ID) ||
                                !File.Exists(GetAvatarPath(pair.Value.ID)))
                            {
                                _profilesAvatarsQueue.Enqueue(pair.Value.ID);

                                if (_avatarsCoroutine == null)
                                {
                                    _avatarsCoroutine = StartCoroutine(AvatarsRequestingCoroutine());
                                }
                            }
                            else
                            {
                                pair.Value.Avatar = LoadSprite(pair.Value.ID);
                            }

                            _socialProfilesSystem.RegisterProfile(pair.Value);
                        }
                    }
                }
            }
        }

        private IEnumerator MessagesSendingCoroutine()
        {
            while (true)
            {
                if (_cachedMessages.Count > 0)
                {
                    yield return new WaitUntil(() => _facebookManager.IsLoggedIn);

                    CachedMessageData messageData = _cachedMessages.First().Value;
                    RequestSendMessageToFriends request = _facebookManager.SendMessageToFriends(messageData.Receivers, messageData.Title, messageData.Message, messageData.Data);
                    request.OnSendMessageFinished += requestResult =>
                    {
                        bool result = false;
                        if (requestResult != null)
                        {
                            if (requestResult.Cancelled)
                            {
                                UnityEngine.Debug.LogError("SEND_MESSAGE - request was cancelled!");
                            }
                            else if (!string.IsNullOrEmpty(requestResult.Error))
                            {
                                UnityEngine.Debug.LogError($"SEND_MESSAGE - request error {requestResult.Error}");
                            }
                            else
                            {
                                UnityEngine.Debug.Log("SEND_MESSAGE - success!");
                                result = true;
                            }
                        }
                        else
                        {
                            UnityEngine.Debug.LogError("SEND_MESSAGE - request result is null!");
                        }

                        messageData.InvokeComplete(result);
                    };

                    _cachedMessages.Remove(messageData.GetHashCode());
                }

                yield return null;
            }
        }

        private IEnumerator AvatarsRequestingCoroutine()
        {
            while (_profilesAvatarsQueue.Count > 0)
            {
                yield return new WaitUntil(() => _facebookManager.IsLoggedIn && !_facebookManager.HasProcessingRequests);
                yield return new WaitUntil(() => DateTime.Now - _lastAvatarRequestTime > _avatarDownloadInterval);
                if (_facebookManager.IsLoggedIn && !_facebookManager.HasProcessingRequests)
                {
                    _lastAvatarRequestTime = DateTime.Now;
                    string userID = _profilesAvatarsQueue.Dequeue();
                    RequestUserPicture request = _facebookManager.RequestUserPicture(userID, _avatarTextureSize, _avatarTextureSize);
                    yield return request;
                    if (request.Result != null &&
                        !request.Result.Cancelled &&
                        string.IsNullOrEmpty(request.Result.Error))
                    {
                        string userPictureURL = null;
                        ParseAvatarRequestResponse(request.Result.RawResult, out userPictureURL);
                        if (!string.IsNullOrEmpty(userPictureURL))
                        {
                            WWW www = new WWW(userPictureURL);
                            yield return www;
                            yield return SaveTexture(www.texture, userID);
                        }
                    }
                }
            }

            _avatarsCoroutine = null;
        }

        private void ParseFriendsRequestResponse(string responseData, out Dictionary<string, SocialProfile> newFriends)
        {
            newFriends = new Dictionary<string, SocialProfile>();

            JObject response = JToken.Parse(responseData) as JObject;
            JArray data = response?["data"] as JArray;
            if (data != null)
            {
                foreach (JToken friendToken in data)
                {
                    JObject friendJson = friendToken as JObject;
                    if (friendJson != null)
                    {
                        string id = friendJson["id"].Value<string>();
                        if (!_cachedFacebookFriends.ContainsKey(id))
                        {
                            string friendName = friendJson["name"].Value<string>();
                            SocialProfile.SendMessageDelegate messagingDelegate = SendMessage;
                            SocialProfile friend = _instantiator.Instantiate<SocialProfile>(
                                    id,
                                    friendName,
                                    true,
                                    _friendsDefaultData.DefaultAvatar,
                                    messagingDelegate
                                );

                            newFriends.Add(id, friend);
                            _cachedFacebookFriends.Add(id, friend);
                        }
                    }
                }
            }
        }

        private void ParseAvatarRequestResponse(string responseData, out string userPictureURL)
        {
            userPictureURL = string.Empty;

            JObject response = JToken.Parse(responseData) as JObject;
            JObject data = response?["data"] as JObject;
            if (data != null)
            {
                try
                {
                    userPictureURL = data["url"].Value<string>();
                }
                catch (Exception exception)
                {
                    UnityEngine.Debug.LogError(exception);
                }
            }
        }

        private void SendMessage(
                SocialProfile profile,
                string title,
                string message,
                JObject data,
                Action<bool> OnComplete
            )
        {
            CachedMessageData messageData = new CachedMessageData(title, message, data.ToString(Formatting.None));
            int hash = messageData.GetHashCode();
            if (_cachedMessages.ContainsKey(hash))
            {
                _cachedMessages[hash].RegisterMessageReceiver(profile.ID, OnComplete);
            }
            else
            {
                messageData.RegisterMessageReceiver(profile.ID, OnComplete);
                _cachedMessages.Add(hash, messageData);
            }
        }

        private string GetAvatarsFolderPath()
        {
            return Application.temporaryCachePath + "/avatars/";
        }

        private string GetAvatarPath(string id)
        {
            return GetAvatarsFolderPath() + id;
        }

        private IEnumerator SaveTexture(Texture2D texture, string userID)
        {
            string folder = GetAvatarsFolderPath();
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            string path = GetAvatarPath(userID);
            byte[] bytes = texture.EncodeToJPG();
            File.WriteAllBytes(path, bytes);
            yield return new WaitForEndOfFrame();

            if (!_serializedAvatars.Contains(userID))
            {
                _serializedAvatars.Add(userID);
            }

            _cachedFacebookFriends[userID].Avatar = CreateSprite(userID, texture);
        }

        private Sprite LoadSprite(string userID)
        {
            Texture2D avatar = new Texture2D(_avatarTextureSize, _avatarTextureSize);
            byte[] bytes = File.ReadAllBytes(GetAvatarPath(userID));
            avatar.LoadImage(bytes);
            return CreateSprite(userID, avatar);
        }

        private Sprite CreateSprite(string userID, Texture2D texture)
        {
            Sprite sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
            _loadedAvatars.Add(userID, sprite);
            return sprite;
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncStringList("serializedAvatars", ref _serializedAvatars, true);
        }

        private class CachedMessageData
        {
            private HashSet<string> _receiverIDs = new HashSet<string>();
            private List<Action<bool>> _completeDelegates = new List<Action<bool>>();

            public CachedMessageData(string title, string message, string data)
            {
                Title = title;
                Message = message;
                Data = data;
            }

            public string Title { get; }
            public string Message { get; }
            public string Data { get; }
            public IEnumerable<string> Receivers => _receiverIDs;

            public void RegisterMessageReceiver(string friendID, Action<bool> completeDelegate)
            {
                if (!_receiverIDs.Contains(friendID))
                {
                    _receiverIDs.Add(friendID);
                    if (completeDelegate != null)
                    {
                        _completeDelegates.Add(completeDelegate);
                    }
                }
            }

            public void InvokeComplete(bool result)
            {
                foreach (Action<bool> completeDelegate in _completeDelegates)
                {
                    completeDelegate?.Invoke(result);
                }
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    int hashCode = (Title != null ? Title.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (Message != null ? Message.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (Data != null ? Data.GetHashCode() : 0);
                    return hashCode;
                }
            }
        }
    }
}