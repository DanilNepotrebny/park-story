﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace DreamTeam.SocialNetworking
{
    public class SocialProfilesSystem : MonoBehaviour
    {
        [SerializeField] private CharacterFriendsRegistrator _characterFriendsRegistrator;
        [SerializeField] private FacebookFriendsRegistrator _facebookFriendsRegistrator;

        private List<SocialProfile> _socialProfiles = new List<SocialProfile>();

        public event Action<SocialProfile> ProfileRegistered;
        public event Action<SocialProfile> ProfileUnRegistered;

        public void RegisterProfile(SocialProfile profile)
        {
            if (!_socialProfiles.Contains(profile))
            {
                _socialProfiles.Add(profile);
                ProfileRegistered?.Invoke(profile);
            }
        }

        public void UnRegisterProfile(SocialProfile profile)
        {
            if (_socialProfiles.Contains(profile))
            {
                _socialProfiles.Remove(profile);
                ProfileUnRegistered?.Invoke(profile);
            }
        }

        public SocialProfile FindProfile(string id)
        {
            SocialProfile result = SocialProfile.Invalid;
            int index = _socialProfiles.FindIndex(profile => profile.ID == id);
            if (index >= 0)
            {
                result = _socialProfiles[index];
            }

            return result;
        }

        public void SendMessage(
            SocialProfile profile,
            string title,
            string message,
            JObject data,
            Action<bool> OnComplete)
        {
            if (profile.MessagingDelegate != null)
            {
                profile.MessagingDelegate(profile, title, message, data, OnComplete);
            }
            else
            {
                OnComplete?.Invoke(false);
            }
        }

        public IReadOnlyCollection<SocialProfile> SocialProfiles => _socialProfiles;
    }
}