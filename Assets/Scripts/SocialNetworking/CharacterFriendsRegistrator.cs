﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.Localization;
using DreamTeam.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Zenject;

namespace DreamTeam.SocialNetworking
{
    public class CharacterFriendsRegistrator : MonoBehaviour
    {
        [SerializeField] private List<CharacterData> _characterMessagesSenders;

        [Inject] private SocialProfilesSystem _socialProfilesSystem;
        [Inject] private IReadOnlyCollection<CharacterData> _characters;
        [Inject] private Instantiator _instantiator;
        [Inject] private LocalizationSystem _localizationSystem;

        private Dictionary<string, SocialProfile> _cachedCharacterFriends = new Dictionary<string, SocialProfile>();

        protected void Start()
        {
            foreach (CharacterData characterData in _characterMessagesSenders)
            {
                bool isVisible = false;
                #if UNITY_EDITOR
                {
                    isVisible = true;
                }
                #endif // UNITY_EDITOR

                SocialProfile.SendMessageDelegate messagingDelegate = SendMessage;
                SocialProfile friend = _instantiator.Instantiate<SocialProfile>(
                        characterData.name,
                        _localizationSystem.Localize(characterData.Name, this),
                        isVisible,
                        characterData.Photo,
                        messagingDelegate
                    );

                _cachedCharacterFriends.Add(characterData.name, friend);
                _socialProfilesSystem.RegisterProfile(friend);
            }
        }

        private void SendMessage(
                SocialProfile profile,
                string title,
                string message,
                JObject data,
                Action<bool> OnComplete
            )
        {
            StartCoroutine(SendMessageInternal(profile, title, message, data, OnComplete));
        }

        private IEnumerator SendMessageInternal(
                SocialProfile profile,
                string title,
                string message,
                JObject data,
                Action<bool> OnComplete
            )
        {
            yield return new WaitForSeconds(1f);
            UnityEngine.Debug.Log($"Fake message to user {profile.ID} sent! Title: {title}, Message: {message}, Data: {data.ToString(Formatting.Indented)}");
            OnComplete?.Invoke(true);
        }
    }
}