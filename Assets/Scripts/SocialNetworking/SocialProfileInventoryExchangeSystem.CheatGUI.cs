﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Debug;
using UnityEngine;
using Zenject;

namespace DreamTeam.SocialNetworking
{
    public partial class SocialProfileInventoryExchangeSystem
    {
        [Inject] private CheatPanel _cheatPanel;

        protected void Start()
        {
            _cheatPanel.AddDrawer(CheatPanel.Category.SocialProfilesExchange, OnDrawCheatGUI);
        }

        private void OnDrawCheatGUI(Rect rect)
        {
            if (GUILayout.Button("Reset all exchange history"))
            {
                foreach (string key in _register.Keys)
                {
                    _register[key] = new ExchangeInfo();
                }
            }
        }
    }
}