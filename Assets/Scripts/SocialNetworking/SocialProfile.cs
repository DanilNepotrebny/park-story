﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace DreamTeam.SocialNetworking
{
    public class SocialProfile : IEquatable<SocialProfile>
    {
        public delegate void SendMessageDelegate(SocialProfile profile, string title, string message, JObject data, Action<bool> OnComplete);

        public static SocialProfile Invalid = new SocialProfile(string.Empty, string.Empty, false, null, null);

        public SocialProfile(string id, string name, bool isVisible, Sprite avatar, SendMessageDelegate sendMessageDelegate)
        {
            ID = id;
            Name = name;
            IsVisible = isVisible;
            Avatar = avatar;
            MessagingDelegate = sendMessageDelegate;
        }

        public string ID { get; }
        public string Name { get; }
        public bool IsVisible { get; }
        public Sprite Avatar { get; set; }
        public SendMessageDelegate MessagingDelegate { get; }

        public bool Equals(SocialProfile other)
        {
            return other != null && string.Equals(ID, other.ID);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            return obj is SocialProfile && Equals((SocialProfile)obj);
        }

        public override int GetHashCode()
        {
            return (ID != null ? ID.GetHashCode() : 0);
        }
    }
}