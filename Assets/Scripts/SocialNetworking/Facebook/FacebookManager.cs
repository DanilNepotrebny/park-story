﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.SocialNetworking.Facebook.Request;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils.AsyncProcesses;
using Facebook.Unity;
using UnityEngine;
using Zenject;

namespace DreamTeam.SocialNetworking.Facebook
{
    public class FacebookManager : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private string[] _permissions;

        [Inject] private SaveManager _saveManager;

        private List<BaseAsyncProcess> _requests = new List<BaseAsyncProcess>();
        private string _cachedUserId = string.Empty;
        private List<AppEvent> _pendingAppEvents = new List<AppEvent>();

        public AccessToken AccessToken => AccessToken.CurrentAccessToken;
        public string UserID => _cachedUserId;
        public bool IsLoggedIn => FB.IsLoggedIn;
        public bool HasProcessingRequests => _requests.Count > 0;

        public event Action LoginFinished;
        public event Action<RequestResult> FriendsInvitationFinished;

        public RequestLogin Login()
        {
            RequestLogin request = new RequestLogin(_permissions);
            request.OnLoginProcessFinished += OnLoginProcessFinished;
            _requests.Add(request);
            return request;
        }

        public RequestLogout Logout()
        {
            RequestLogout request = new RequestLogout();
            _requests.Add(request);
            return request;
        }

        public RequestInviteFriends InviteFriends(string inviteTitle, string inviteMessage, string data)
        {
            RequestInviteFriends request = new RequestInviteFriends(inviteTitle, inviteMessage, data);
            request.FriendsInvitationFinished += OnFriendsInvitationFinished;
            _requests.Add(request);
            return request;
        }

        public RequestFriendsList RequestPlayingFriendsList()
        {
            RequestFriendsList request = new RequestFriendsList();
            _requests.Add(request);
            return request;
        }

        public RequestSendMessageToFriends SendMessageToFriends(
            IEnumerable<string> friendsIDs,
            string title,
            string message,
            string data)
        {
            RequestSendMessageToFriends request = new RequestSendMessageToFriends(
                friendsIDs,
                title,
                message,
                data);
            _requests.Add(request);
            return request;
        }

        public RequestMessageBox RequestMessageBox()
        {
            RequestMessageBox request = new RequestMessageBox();
            _requests.Add(request);
            return request;
        }

        public RequestUserPicture RequestUserPicture(string userID, int width, int height)
        {
            RequestUserPicture request = new RequestUserPicture(userID, width, height);
            _requests.Add(request);
            return request;
        }

        public RequestDeleteData DeleteUserData(IEnumerable<string> dataIDs)
        {
            RequestDeleteData request = new RequestDeleteData(dataIDs);
            _requests.Add(request);
            return request;
        }

        public void Init()
        {
            _saveManager.GetSaveGroup(SaveManager.Group.Settings).RegisterSynchronizable("FacebookManager", this);

            TryActivate();
        }

        public void LogAppEvent(
                string eventName,
                float? valueToSum,
                Dictionary<string, object> eventParams
            )
        {
            if (!FB.IsInitialized)
            {
                _pendingAppEvents.Add(new AppEvent(eventName, valueToSum, eventParams));
            }
            else
            {
                FB.LogAppEvent(eventName, valueToSum, eventParams);
            }
        }

        // Unity will call OnApplicationPause(false) when an app is resumed
        // from the background
        protected void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus)
            {
                TryActivate();
            }
        }

        protected void Update()
        {
            _requests.RemoveAll(request => request.IsCompleted);
        }

        private void TryActivate()
        {
            if (!FB.IsInitialized)
            {
                FB.Init(OnInitialized);
            }
            else
            {
                OnInitialized();
            }
        }

        private void OnInitialized()
        {
            if (!FB.IsInitialized)
            {
                UnityEngine.Debug.LogError("Failed to initialize Facebook SDK");
            }
            else
            {
                FB.ActivateApp();

                if (_pendingAppEvents.Count > 0)
                {
                    foreach (AppEvent e in _pendingAppEvents)
                    {
                        FB.LogAppEvent(e.EventName, e.ValueToSum, e.Parameters);
                    }
                    _pendingAppEvents.Clear();
                }
            }
        }

        private void OnLoginProcessFinished(ILoginResult result)
        {
            if (result != null &&
                !result.Cancelled &&
                string.IsNullOrEmpty(result.Error) &&
                result.ResultDictionary != null &&
                result.ResultDictionary.ContainsKey("user_id"))
            {
                string id = result.ResultDictionary["user_id"] as string;
                if (!string.IsNullOrEmpty(id))
                {
                    _cachedUserId = id;
                    LoginFinished?.Invoke();
                }
            }
        }

        private void OnFriendsInvitationFinished(IAppRequestResult result)
        {
            FriendsInvitationFinished?.Invoke(GetResultEnum(result));
        }

        private static RequestResult GetResultEnum(IResult result)
        {
            if (result.Cancelled)
            {
                return RequestResult.Cancelled;
            }

            if (!string.IsNullOrEmpty(result.Error))
            {
                return RequestResult.Error;
            }

            return RequestResult.Success;
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncString("cachedUserID", ref _cachedUserId, string.Empty);
        }

        #region Inner types

        public enum RequestResult
        {
            Cancelled,
            Error,
            Success
        }

        private class AppEvent
        {
            public string EventName { get; }
            public float? ValueToSum { get; }
            public Dictionary<string, object> Parameters { get; }

            public AppEvent(
                    string eventName,
                    float? valueToSum,
                    Dictionary<string, object> eventParams
                )
            {
                EventName = eventName;
                ValueToSum = valueToSum;
                Parameters = eventParams;
            }
        }

        #endregion
    }
}