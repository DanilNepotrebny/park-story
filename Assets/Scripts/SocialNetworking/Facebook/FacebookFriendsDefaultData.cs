﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.SocialNetworking.Facebook
{
    [CreateAssetMenu(fileName = "FacebookFriendsDefaultData", menuName = "DreamTeam/Facebook friends default data")]
    public class FacebookFriendsDefaultData : ScriptableObject
    {
        [SerializeField] private List<Sprite> _defaultAvatars;

        public Sprite DefaultAvatar => _defaultAvatars[Random.Range(0, _defaultAvatars.Count - 1)];
    }
}