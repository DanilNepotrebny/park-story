﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.AsyncProcesses;
using Facebook.Unity;

namespace DreamTeam.SocialNetworking.Facebook.Request
{
    public class BaseRequest : BaseAsyncProcess
    {
        private float _progress;
        public override float Progress => _progress;

        protected void OnFinished(IResult result)
        {
            if (result != null && !string.IsNullOrEmpty(result.Error))
            {
                UnityEngine.Debug.LogError($"[Facebook request] {result.Error}");
            }

            _progress = 1f;
            OnCompleted();
        }
    }
}