﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using Facebook.Unity;

namespace DreamTeam.SocialNetworking.Facebook.Request
{
    public class RequestSendMessageToFriends : BaseRequest
    {
        public event Action<IAppRequestResult> OnSendMessageFinished;
        public IAppRequestResult Result { get; private set; }

        public RequestSendMessageToFriends(IEnumerable<string> friendsIDs, string title, string message, string data)
        {
            FB.AppRequest(message, friendsIDs, null, null, null, data, title, OnAppRequestFinished);
        }

        private void OnAppRequestFinished(IAppRequestResult result)
        {
            Result = result;
            OnFinished(result);
            OnSendMessageFinished?.Invoke(Result);
        }
    }
}