﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using Facebook.Unity;

namespace DreamTeam.SocialNetworking.Facebook.Request
{
    public class RequestInviteFriends : BaseRequest
    {
        public event Action<IAppRequestResult> FriendsInvitationFinished;
        public IAppRequestResult Result { get; private set; }

        public RequestInviteFriends(string title, string message, string data)
        {
            FB.AppRequest(message, null, null, null, null, data, title, OnAppRequestFinished);
        }

        private void OnAppRequestFinished(IAppRequestResult result)
        {
            Result = result;
            OnFinished(result);
            FriendsInvitationFinished?.Invoke(Result);
        }
    }
}