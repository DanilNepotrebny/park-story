﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using Facebook.Unity;

namespace DreamTeam.SocialNetworking.Facebook.Request
{
    public class RequestFriendsList : BaseRequest
    {
        private const string _apiPath = "me/friends";

        public event Action<IGraphResult> OnRequestPlayingFriendsFinished;
        public IGraphResult Result { get; private set; }

        public RequestFriendsList()
        {
            FB.API(_apiPath, HttpMethod.GET, OnRequestFinished);
        }

        private void OnRequestFinished(IGraphResult result)
        {
            Result = result;
            OnFinished(result);
            OnRequestPlayingFriendsFinished?.Invoke(Result);
        }
    }
}