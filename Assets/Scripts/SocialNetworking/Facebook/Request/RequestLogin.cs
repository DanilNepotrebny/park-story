﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using Facebook.Unity;

namespace DreamTeam.SocialNetworking.Facebook.Request
{
    public class RequestLogin : BaseRequest
    {
        public event Action<ILoginResult> OnLoginProcessFinished;
        public ILoginResult Result { get; private set; }

        public RequestLogin(IEnumerable<string> permissions)
        {
            FB.LogInWithReadPermissions(permissions, OnLoggedIn);
        }

        private void OnLoggedIn(ILoginResult result)
        {
            Result = result;
            OnFinished(result);
            OnLoginProcessFinished?.Invoke(Result);
        }
    }
}