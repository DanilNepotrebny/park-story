﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using Facebook.Unity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DreamTeam.SocialNetworking.Facebook.Request
{
    public class RequestDeleteData : BaseRequest
    {
        public event Action<IGraphResult> OnRequestDeleteDataFinished;
        public IGraphResult Result { get; private set; }

        public RequestDeleteData(IEnumerable<string> dataIDs)
        {
            Dictionary<string, string> postData = new Dictionary<string, string>();
            JArray requestBatch = new JArray();
            foreach (string dataID in dataIDs)
            {
                JObject request = new JObject();
                JToken method = new JValue("DELETE");
                JToken relativeURL = new JValue(dataID);
                request.Add("method", method);
                request.Add("relative_url", relativeURL);
                requestBatch.Add(request);
            }
            
            postData.Add("batch", requestBatch.ToString(Formatting.None));

            FB.API("/", HttpMethod.POST, OnRequestFinished, postData);
        }

        private void OnRequestFinished(IGraphResult result)
        {
            Result = result;
            OnFinished(result);
            OnRequestDeleteDataFinished?.Invoke(Result);
        }
    }
}