﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Facebook.Unity;

namespace DreamTeam.SocialNetworking.Facebook.Request
{
    public class RequestLogout : BaseRequest
    {
        public RequestLogout()
        {
            FB.LogOut();
            OnFinished(null);
        }
    }
}