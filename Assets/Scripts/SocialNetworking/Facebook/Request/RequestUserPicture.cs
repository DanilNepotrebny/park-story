﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using Facebook.Unity;

namespace DreamTeam.SocialNetworking.Facebook.Request
{
    public class RequestUserPicture : BaseRequest
    {
        private const string _apiPath = "{0}/picture?width={1}&height={2}&redirect=false";

        public event Action<IGraphResult> OnRequestUserPictureFinished;
        public IGraphResult Result { get; private set; }

        public RequestUserPicture(string userID, int width, int height)
        {
            FB.API(string.Format(_apiPath, userID, width, height), HttpMethod.GET, OnRequestFinished);
        }

        private void OnRequestFinished(IGraphResult result)
        {
            Result = result;
            OnFinished(result);
            OnRequestUserPictureFinished?.Invoke(Result);
        }
    }
}