﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using DreamTeam.Messaging;
using DreamTeam.SocialNetworking.Facebook.Request;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.Nodes;
using DreamTeam.Synchronization.Serializers;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Zenject;

namespace DreamTeam.SocialNetworking.Facebook
{
    [DisallowMultipleComponent]
    public class FacebookMessageBox : MonoBehaviour, ISynchronizable, ISynchronizationFactory<FacebookMessageBox.RawMessageData>, IInitable
    {
        [SerializeField] private TimeTicks _messageBoxRequestInterval;

        [Inject] private FacebookManager _facebookManager;
        [Inject] private MessageSystem _messageSystem;
        [Inject] private SocialProfilesSystem _socialProfilesSystem;
        [Inject] private JsonNodeSerializer _serializer;
        [Inject] private SaveGroup _saveGroup;
        [Inject] private Instantiator _instantiator;

        private Dictionary<string, RawMessageData> _receivedMessages = new Dictionary<string, RawMessageData>();
        private List<string> _pendingRemovalMessages = new List<string>();
        private DateTime _lastRequestTime = DateTime.MinValue;
        private RequestDeleteData _requestDeleteData = null;

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("facebookMessageBox", this);
            _socialProfilesSystem.ProfileRegistered += OnProfileRegistered;
        }

        protected void Start()
        {
            #if !UNITY_EDITOR
            {
                StartCoroutine(FacebookRequestingAppDataCoroutine());
                StartCoroutine(FriendsDeletingDataCoroutine());
            }
            #endif // UNITY_EDITOR
        }

        private IEnumerator FacebookRequestingAppDataCoroutine()
        {
            while (true)
            {
                yield return new WaitUntil(() => _facebookManager.IsLoggedIn && !_facebookManager.HasProcessingRequests);
                yield return new WaitUntil(() => DateTime.Now - _lastRequestTime > _messageBoxRequestInterval);
                if (_facebookManager.IsLoggedIn && !_facebookManager.HasProcessingRequests)
                {
                    _lastRequestTime = DateTime.Now;
                    RequestMessageBox request = _facebookManager.RequestMessageBox();
                    yield return request;
                    if (request.Result != null &&
                        !request.Result.Cancelled &&
                        string.IsNullOrEmpty(request.Result.Error))
                    {
                        ParseMessageBoxRequestResponse(request.Result.RawResult);
                    }
                }
            }
        }

        private IEnumerator FriendsDeletingDataCoroutine()
        {
            while (true)
            {
                if (_pendingRemovalMessages.Count > 0 && _requestDeleteData == null)
                {
                    yield return new WaitUntil(() => _facebookManager.IsLoggedIn);

                    _requestDeleteData = _facebookManager.DeleteUserData(_pendingRemovalMessages);
                    _requestDeleteData.OnRequestDeleteDataFinished += requestResult =>
                    {
                        if (requestResult != null)
                        {
                            if (requestResult.Cancelled)
                            {
                                UnityEngine.Debug.LogError("DELETE_DATA - request was cancelled!");
                            }
                            else if (!string.IsNullOrEmpty(requestResult.Error))
                            {
                                UnityEngine.Debug.LogError($"DELETE_DATA - request error {requestResult.Error}");
                            }
                            else
                            {
                                UnityEngine.Debug.Log("DELETE_DATA - success!");

                                _pendingRemovalMessages.Clear();
                            }
                        }
                        else
                        {
                            UnityEngine.Debug.LogError("DELETE_DATA - request result is null!");
                        }
                    };
                }

                if (_requestDeleteData != null && _requestDeleteData.IsCompleted)
                {
                    _requestDeleteData = null;
                }

                yield return null;
            }
        }

        private void ParseMessageBoxRequestResponse(string responseData)
        {
            JObject response = JToken.Parse(responseData) as JObject;
            JArray data = response?["data"] as JArray;
            if (data != null)
            {
                foreach (JToken messageToken in data)
                {
                    try
                    {
                        string messageID = messageToken["id"].Value<string>();
                        if (!_receivedMessages.ContainsKey(messageID) && !_pendingRemovalMessages.Contains(messageID))
                        {
                            string senderID = messageToken["from"]["id"].Value<string>();
                            string messageString = messageToken["data"].Value<string>();
                            _receivedMessages.Add(messageID, new RawMessageData
                            {
                                SenderID = senderID,
                                Data = messageString
                            });
                        }
                    }
                    catch (Exception exception)
                    {
                        UnityEngine.Debug.LogError(exception);
                    }
                }
            }

            ProcessPendingReceivedMessages();
        }

        private void ProcessPendingReceivedMessages()
        {
            foreach (KeyValuePair<string, RawMessageData> pair in _receivedMessages)
            {
                SocialProfile sender = _socialProfilesSystem.FindProfile(pair.Value.SenderID);
                if (!sender.Equals(SocialProfile.Invalid))
                {
                    try
                    {
                        DictionaryNode root;
                        using (MemoryStream memStream = new MemoryStream())
                        {
                            using (StreamWriter writer = new StreamWriter(memStream))
                            {
                                writer.Write(pair.Value.Data);
                                writer.Flush();
                                memStream.Position = 0;
                                root = (DictionaryNode)_serializer.Deserialize(memStream);
                            }
                        }

                        State deserializationState = new DeserializationState(root);
                        IMessage message = null;
                        _messageSystem.SyncMessage(deserializationState, ref message);
                        if (message != null)
                        {
                            RewardsMessage rewardsMessage = message as RewardsMessage;
                            if (rewardsMessage != null)
                            {
                                rewardsMessage.Sender = sender;
                            }

                            _messageSystem.Add(message);
                        }
                    }
                    catch (Exception exception)
                    {
                        UnityEngine.Debug.LogError($"Failed to deserialize received message! Message: {pair.Value.Data}, Exception: {exception}");
                    }
                    finally
                    {
                        _pendingRemovalMessages.Add(pair.Key);
                    }
                }
            }

            foreach (string messageID in _pendingRemovalMessages)
            {
                if (_receivedMessages.ContainsKey(messageID))
                {
                    _receivedMessages.Remove(messageID);
                }
            }
        }

        private void OnProfileRegistered(SocialProfile profile)
        {
            ProcessPendingReceivedMessages();
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncObjectDictionary("receivedMessages", ref _receivedMessages, this);
            state.SyncStringList("pendingRemovalMessages", ref _pendingRemovalMessages, true);
        }

        [Serializable]
        private class RawMessageData : ISynchronizable
        {
            public string SenderID;
            public string Data;

            public void Sync(State state)
            {
                state.SyncString("sender", ref SenderID);
                state.SyncString("data", ref Data);
            }
        }

        Node ISynchronizationFactory<RawMessageData>.Serialize(RawMessageData data)
        {
            return new NullNode();
        }

        RawMessageData ISynchronizationFactory<RawMessageData>.Deserialize(Node factoryNode)
        {
            return _instantiator.Instantiate<RawMessageData>();
        }
    }
}