﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.SocialNetworking
{
    public enum SocialNetworkType
    {
        Facebook
    }

    public static class SocialNetworkTypeExtensions
    {
        public static string ToAnalyticsId(this SocialNetworkType self)
        {
            switch (self)
            {
                case SocialNetworkType.Facebook:
                    return "facebook";
            }

            UnityEngine.Debug.LogError($"Ivalid value for {self.GetType().Name} enum.");
            return "";
        }
    }
}