﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System.Collections.Generic;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace DreamTeam.Localization
{
    public partial class LocalizationSystem : LocalizationSystem.IEditorApi
    {
        public IEnumerable<string> Keys
        {
            get
            {
                if (_currentLocalization == null)
                {
                    if (string.IsNullOrEmpty(_currentLocalizationCode))
                    {
                        _currentLocalizationCode = _defaultLocatiozationCode;
                    }

                    ChangeLocalization(_currentLocalizationCode);
                }
                
                return ((IDictionary<string, JToken>)_currentLocalization)?.Keys;
            }
        }

        string IEditorApi.LocalizeEditor([NotNull] string id)
        {
            return Localize(id, null);
        }

        void IEditorApi.OnLocalizationAssetImported(TextAsset asset)
        {
            if (_currentLocalizationCode == asset.name)
            {
                ChangeLocalization(asset.name);
            }
        }

        public interface IEditorApi
        {
            IEnumerable<string> Keys { get; }
            string LocalizeEditor([NotNull] string id);
            void OnLocalizationAssetImported(TextAsset asset);
        }
    }
}

#endif