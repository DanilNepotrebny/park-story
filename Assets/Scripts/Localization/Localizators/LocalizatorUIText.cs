﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Localization.Localizators
{
    [ExecuteInEditMode]
    public class LocalizatorUIText : LocalizatorBase
    {
        [SerializeField] [LocalizationKey] private string _textID;
        [SerializeField] private Text _textComponent;

        protected void Start()
        {
            if (_textComponent != null && !string.IsNullOrEmpty(_textID))
            {
                _textComponent.text = LocalizationSystem.Localize(_textID, _textComponent);
            }
        }

        protected void OnValidate()
        {
            if (!Application.isPlaying &&
                LocalizationSystem != null &&
                _textComponent != null &&
                !string.IsNullOrEmpty(_textID))
            {
                _textComponent.text = LocalizationSystem.Localize(_textID, _textComponent);
            }
        }
    }
}