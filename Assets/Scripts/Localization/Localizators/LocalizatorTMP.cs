﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using TMPro;
using UnityEngine;

namespace DreamTeam.Localization.Localizators
{
    [ExecuteInEditMode]
    public class LocalizatorTMP : LocalizatorBase
    {
        [SerializeField] private TMP_Text _textComponent;
        [SerializeField] [LocalizationKey] private string _textID;

        protected void Start()
        {
            if (_textComponent != null && !string.IsNullOrEmpty(_textID))
            {
                _textComponent.text = LocalizationSystem.Localize(_textID, _textComponent);
                _textComponent.ForceMeshUpdate(true);
            }
        }

        protected void OnValidate()
        {
            if (!Application.isPlaying &&
                LocalizationSystem != null &&
                _textComponent != null &&
                !string.IsNullOrEmpty(_textID))
            {
                _textComponent.text = LocalizationSystem.Localize(_textID, _textComponent);
                _textComponent.ForceMeshUpdate(true);
            }
        }
    }
}