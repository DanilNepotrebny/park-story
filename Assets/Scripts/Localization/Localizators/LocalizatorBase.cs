﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Localization.Localizators
{
    [ExecuteInEditMode]
    public abstract class LocalizatorBase : MonoBehaviour
    {
        [Inject] private LocalizationSystem _localizationSystem;

        protected LocalizationSystem LocalizationSystem => _localizationSystem;

        protected void Awake()
        {
            #if UNITY_EDITOR
            {
                if (!Application.isPlaying && _localizationSystem == null)
                {
                    _localizationSystem = SystemAssetAccessor<LocalizationSystem>.Instance;
                }
            }
            #endif // UNITY_EDITOR
        }
    }
}