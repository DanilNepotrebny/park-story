﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Localization
{
    [ExecuteInEditMode]
    public partial class LocalizationSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private string _defaultLocatiozationCode = "en";
        [SerializeField] private List<TextAsset> _localizations;

        [Inject] private SaveManager _saveManager;

        private string _currentLocalizationCode = string.Empty;
        private JObject _cachedLocalization = null;

        private JObject _currentLocalization
        {
            get
            {
                if (_cachedLocalization == null)
                {
                    if (string.IsNullOrEmpty(_currentLocalizationCode))
                    {
                        _currentLocalizationCode = _defaultLocatiozationCode;
                    }

                    ReloadCachedLocalization();
                }

                return _cachedLocalization;
            }
        }

        public void Init()
        {
            _saveManager
                .GetSaveGroup(SaveManager.Group.Localization)
                .RegisterSynchronizable("LocalizationSystem", this);
        }

        public void ChangeLocalization(string code)
        {
            _currentLocalizationCode = code;
            ReloadCachedLocalization();
        }

        public string Localize([NotNull] string id, Object errorContext)
        {
            string result = id;
            if (_currentLocalization != null)
            {
                if (_currentLocalization.ContainsKey(id))
                {
                    result = _currentLocalization[id].Value<string>();
                }
                else
                {
                    UnityEngine.Debug.LogError($"Localization key is not found: {id}", errorContext);
                }
            }
            else
            {
                UnityEngine.Debug.LogError($"Localization is not loaded!", errorContext);
            }

            return result;
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncString(nameof(_currentLocalizationCode), ref _currentLocalizationCode, _defaultLocatiozationCode);
        }

        protected void Start()
        {
            if (string.IsNullOrEmpty(_currentLocalizationCode))
            {
                _currentLocalizationCode = _defaultLocatiozationCode;
            }

            ReloadCachedLocalization();
        }

        private void ReloadCachedLocalization()
        {
            TextAsset localizationJson = _localizations.FirstOrDefault(asset => asset.name == _currentLocalizationCode);
            Assert.IsNotNull(localizationJson, $"Was not able to load localization for code {_currentLocalizationCode}");
            if (localizationJson != null)
            {
                _cachedLocalization = JToken.Parse(localizationJson.text) as JObject;
            }
        }
    }
}