﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Synchronization;
using DreamTeam.Utils;

namespace DreamTeam.PersistentCommands
{
    public interface IPersistentCommand : ISynchronizable, ICancelable
    {
        void TryExecute(Action<IPersistentCommand, bool> resultCallback);
    }
}