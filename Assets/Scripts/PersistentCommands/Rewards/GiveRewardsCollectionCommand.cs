﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using Zenject;

namespace DreamTeam.PersistentCommands.Rewards
{
    public class GiveRewardsCollectionCommand : IPersistentCommand
    {
        [Inject] private PersistentCommandsExecutionSystem _persistentCommands;
        [Inject] private ISynchronizationFactory<Reward> _rewardSyncFactory;
        [Inject] private Instantiator _instantiator;

        private List<Reward> _rewards;
        private ReceivingType _receivingType;
        private GamePlace _gamePlace;

        public static GiveRewardsCollectionCommand Construct(
                Instantiator instantiator,
                IEnumerable<Reward> rewards,
                ReceivingType receivingType,
                GamePlace gamePlace
            )
        {
            var result = instantiator.Instantiate<GiveRewardsCollectionCommand>();
            result._rewards = new List<Reward>(rewards);
            result._receivingType = receivingType;
            result._gamePlace = gamePlace;

            // Injecting rewards dependencies if they where instantiated from [SerializedField]
            instantiator.Inject(result._rewards.ToArray());

            return result;
        }

        public void Sync(State state)
        {
            state.SyncObjectList("rewards", ref _rewards, _rewardSyncFactory);
            state.SyncEnum("receivingType", ref _receivingType);
            state.SyncEnum("gamePlace", ref _gamePlace);
        }

        public void Cancel()
        {
            _persistentCommands.Unregister(this);
        }

        public void TryExecute(Action<IPersistentCommand, bool> resultCallback)
        {
            for (int index = _rewards.Count - 1; index >= 0; index--)
            {
                _rewards[index].Received += OnRewardReceived;
                _rewards[index].Receive(_instantiator, _receivingType, _gamePlace);
            }

            resultCallback.Invoke(this, _rewards.Count == 0);
        }

        private void OnRewardReceived(Reward reward)
        {
            reward.Received -= OnRewardReceived;
            _rewards.Remove(reward);
        }
    }
}