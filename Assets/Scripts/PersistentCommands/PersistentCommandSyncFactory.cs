﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Synchronization.Factories;
using DreamTeam.Utils;

namespace DreamTeam.PersistentCommands
{
    public class PersistentCommandSyncFactory : ObjectSynchronizationFactory<
        IPersistentCommand,
        PersistentCommandSyncFactory.TypeReference,
        PersistentCommandSyncFactory.TypeReferenceMap>
    {
        protected override IPersistentCommand Instantiate(Instantiator instantiator, Type type)
        {
            return instantiator.InstantiateExplicit(type) as IPersistentCommand;
        }

        [Serializable]
        public class TypeReference : TypeReference<IPersistentCommand>
        {
        }

        [Serializable]
        public class TypeReferenceMap : KeyValueList<string, TypeReference>
        {
        }
    }
}