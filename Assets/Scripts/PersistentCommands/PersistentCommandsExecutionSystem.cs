﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using UnityEngine;
using Zenject;

namespace DreamTeam.PersistentCommands
{
    [RequireComponent(typeof(PersistentCommandSyncFactory))]
    public class PersistentCommandsExecutionSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        [Inject] private SaveGroup _saveGroup;

        private int _currentCommandIndex = 0;
        private List<IPersistentCommand> _commands = new List<IPersistentCommand>();

        private PersistentCommandSyncFactory _syncFactory => GetComponent<PersistentCommandSyncFactory>();

        public void Register(IPersistentCommand command)
        {
            if (!_commands.Contains(command))
            {
                _commands.Add(command);
            }
        }

        public void Unregister(IPersistentCommand command)
        {
            _commands.Remove(command);
        }

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("PersistentCommandsSystem", this);
            _saveGroup.Loaded += OnSaveGroupLoaded;
        }

        private void OnSaveGroupLoaded()
        {
            _saveGroup.Loaded -= OnSaveGroupLoaded;
            ExecuteCurrentCommand();
        }

        private void ExecuteCurrentCommand()
        {
            if (_currentCommandIndex < _commands.Count)
            {
                _commands[_currentCommandIndex].TryExecute(OnCommandExecuted);
            }
        }

        private void OnCommandExecuted(IPersistentCommand command, bool success)
        {
            if (success)
            {
                Unregister(command);
            }

            _currentCommandIndex++;
            ExecuteCurrentCommand();
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncObjectList("commands", ref _commands, _syncFactory);
        }
    }
}