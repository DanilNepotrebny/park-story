﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Utils;

namespace DreamTeam.PersistentCommands.CountedItems
{
    public class SpendCountedItemCommand : CountedItemQuantityCommand
    {
        protected SpendCountedItemCommand()
        {
        }

        public static SpendCountedItemCommand Construct(Instantiator instantiator, CountedItem item, int count)
        {
            return Construct<SpendCountedItemCommand>(instantiator, item, count);
        }

        public override void TryExecute(Action<IPersistentCommand, bool> resultCallback)
        {
            bool result = Item.TrySpend(Count, GamePlace.Location);
            resultCallback?.Invoke(this, result);
        }
    }
}