﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory.Items;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using Zenject;

namespace DreamTeam.PersistentCommands.CountedItems
{
    public abstract class CountedItemQuantityCommand : IPersistentCommand
    {
        [Inject] private ISynchronizer<CountedItem> _itemSynchronizer;
        [Inject] private PersistentCommandsExecutionSystem _persistentCommandsExecution;

        private CountedItem _item;
        private int _count;

        protected CountedItem Item => _item;
        protected int Count => _count;

        public abstract void TryExecute(Action<IPersistentCommand, bool> resultCallback);

        protected static TCommand Construct<TCommand>(Instantiator instantiator, CountedItem item, int count)
            where TCommand : CountedItemQuantityCommand
        {
            var result = instantiator.InstantiateExplicit(typeof(TCommand)) as TCommand;
            result._item = item;
            result._count = count;
            result.Register();
            return result;
        }

        protected virtual void Sync(State state)
        {
            state.SyncObject("item", ref _item, _itemSynchronizer);
            state.SyncInt("count", ref _count);
        }

        private void Register()
        {
            _persistentCommandsExecution.Register(this);
        }

        void ISynchronizable.Sync(State state)
        {
            Sync(state);
        }

        void ICancelable.Cancel()
        {
            _persistentCommandsExecution.Unregister(this);
        }
    }
}