﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory.Items;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;

namespace DreamTeam.PersistentCommands.CountedItems
{
    public class SpendLivesCommand : SpendCountedItemCommand
    {
        private DateTime _spendTime;

        public static SpendLivesCommand Construct(Instantiator instantiator, Lives item, int count)
        {
            var result = Construct<SpendLivesCommand>(instantiator, item, count);
            result._spendTime = (item as Lives.IRestoreController).CurrentTime;
            return result;
        }

        public override void TryExecute(Action<IPersistentCommand, bool> resultCallback)
        {
            base.TryExecute(resultCallback);
            ((Lives.IRestoreController)Item).SetSpendTime(_spendTime);
        }

        protected override void Sync(State state)
        {
            base.Sync(state);
            state.SyncDateTime("spendTime", ref _spendTime);
        }
    }
}