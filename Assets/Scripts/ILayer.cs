﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam
{
    public interface ILayer
    {
        IEnumerable<Transform> Children { get; }

        /// <summary>
        /// Add game object to the layer
        /// </summary>
        void Add([NotNull] GameObject go);

        /// <summary>
        /// Remove game object from the layer
        /// </summary>
        void Remove([NotNull] GameObject go);

        /// <summary>
        /// Get layer position's z-component
        /// </summary>
        float GetZPosition();
    }
}