﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Localization;
using DreamTeam.Messaging.MessageAssets;
using DreamTeam.SocialNetworking;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using Zenject;

namespace DreamTeam.Messaging
{
    public class RewardsMessage : IMessage
    {
        [Inject] private ISynchronizationFactory<Reward> _rewardSyncFactory;
        [Inject] private Instantiator _instantiator;
        [Inject] protected SendRewardsMessageAsset _rewardsMessageData;
        [Inject] protected MessageSystem _messageSystem;
        [Inject] protected SocialProfilesSystem _socialProfilesSystem;
        [Inject] protected LocalizationSystem _localizationSystem;

        private List<Reward> _rewards;
        private string _senderID = string.Empty;
        private GamePlace _gamePlace;

        [Inject]
        public RewardsMessage()
        {
            // Default, used for serialization
        }

        public RewardsMessage(Instantiator instantiator, List<Reward> rewards, GamePlace gamePlace)
        {
            instantiator.Inject(this);

            _rewards = rewards;
            _gamePlace = gamePlace;
        }

        public virtual string Title => _localizationSystem.Localize(_rewardsMessageData.SendRewardsTitleKey, _rewardsMessageData);
        public virtual string Message => _localizationSystem.Localize(_rewardsMessageData.SendRewardsMessageKey, _rewardsMessageData);

        public ICollection<Reward> Rewards => _rewards.AsReadOnly();

        public SocialProfile Sender
        {
            get
            {
                SocialProfile profile = SocialProfile.Invalid;
                if (!string.IsNullOrEmpty(_senderID))
                {
                    profile = _socialProfilesSystem.FindProfile(_senderID);
                }

                return profile;
            }

            set
            {
                if (!Sender.Equals(value))
                {
                    if (value != null && !value.Equals(SocialProfile.Invalid))
                    {
                        _senderID = value.ID;
                    }
                    else
                    {
                        _senderID = string.Empty;
                    }
                }
            }
        }

        public virtual bool CanBeConsumed => Rewards.All(item => item.CanBeReceived);

        public virtual void Consume()
        {
            foreach (Reward reward in Rewards)
            {
                reward.Receive(_instantiator, ReceivingType.Earned, _gamePlace);
            }

            _messageSystem.Remove(this);
        }

        public virtual void NotifyConsumptionRefusal()
        {
            foreach (Reward reward in Rewards)
            {
                if (!reward.CanBeReceived)
                {
                    reward.NotifyReceivingRefusal();
                }
            }
        }

        public virtual void Sync(State state)
        {
            state.SyncObjectList("rewards", ref _rewards, _rewardSyncFactory);
            state.SyncString("senderID", ref _senderID);
            state.SyncEnum("gamePlace", ref _gamePlace);
        }
    }
}