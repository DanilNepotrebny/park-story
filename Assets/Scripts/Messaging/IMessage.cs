﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Synchronization;

namespace DreamTeam.Messaging
{
    public interface IMessage : ISynchronizable
    {
        bool CanBeConsumed { get; }
        void Consume();
        void NotifyConsumptionRefusal();
    }
}