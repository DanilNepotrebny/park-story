﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Debug;
using UnityEngine;
using Zenject;

namespace DreamTeam.Messaging
{
    public partial class MessageSystem : MessageSystem.ICheatAPI
    {
        [Inject] private CheatPanel _cheatPanel;

        private HashSet<ICheatPanelDrawable> _drawables = new HashSet<ICheatPanelDrawable>();
        private static GUISkin _cheatGUISkin;

        protected void Start()
        {
            _cheatPanel.AddDrawer(CheatPanel.Category.MessageSystem, OnDrawCheatGUI);
        }

        private void OnDrawCheatGUI(Rect rect)
        {
            GUISkin skin = GetGUISkin();
            GUILayout.BeginVertical(GUILayout.ExpandWidth(true));

            Rect drawRect = rect;
            foreach (var drawable in _drawables)
            {
                drawRect.height = drawable.GetGUIHeight(drawRect, skin);
                drawable.DrawGUI(drawRect, skin);
                drawRect.y = drawRect.yMax;
            }

            GUILayout.EndVertical();
        }

        private GUISkin GetGUISkin()
        {
            if (_cheatGUISkin == null)
            {
                _cheatGUISkin = Instantiate(GUI.skin) as GUISkin;

                int baseFontSize = GUI.skin.button.fontSize;
                int fontSize = (int)(baseFontSize * 2);

                _cheatGUISkin.button.fontSize = baseFontSize;
                _cheatGUISkin.button.fixedHeight = fontSize;
                _cheatGUISkin.button.alignment = TextAnchor.MiddleLeft;
            }

            return _cheatGUISkin;
        }

        void ICheatAPI.AddCheatDrawable(ICheatPanelDrawable sender)
        {
            _drawables.Add(sender);
        }

        public interface ICheatAPI
        {
            void AddCheatDrawable(ICheatPanelDrawable sender);
        }
    }
}