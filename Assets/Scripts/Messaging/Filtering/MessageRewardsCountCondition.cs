﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Messaging.Filtering
{
    [CreateAssetMenu(menuName = "Messages/Filters/Conditions/MessageRewardsCountCondition")]
    public class MessageRewardsCountCondition : MessageCondition
    {
        [SerializeField] private int _count = 0;
        [SerializeField] private Mode _mode = Mode.Equal;

        public override bool IsSuitable(IMessage message)
        {
            var rewardsMessage = message as RewardsMessage;
            if (rewardsMessage != null)
            {
                switch (_mode)
                {
                    case Mode.Less:
                        return rewardsMessage.Rewards.Count < _count;

                    case Mode.LessOrEqual:
                        return rewardsMessage.Rewards.Count <= _count;

                    case Mode.More:
                        return rewardsMessage.Rewards.Count > _count;

                    case Mode.MoreOrEqual:
                        return rewardsMessage.Rewards.Count >= _count;

                    case Mode.Equal:
                    default:
                        return rewardsMessage.Rewards.Count == _count;
                }
            }

            return false;
        }

        private enum Mode
        {
            Equal,
            Less,
            LessOrEqual,
            More,
            MoreOrEqual
        }
    }
}