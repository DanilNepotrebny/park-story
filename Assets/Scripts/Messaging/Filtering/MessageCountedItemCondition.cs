﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Messaging.Filtering
{
    [CreateAssetMenu(menuName = "Messages/Filters/Conditions/MessageCountedItemCondition")]
    public class MessageCountedItemCondition : MessageCondition
    {
        [SerializeField, RequiredField] private List<CountedItem> _items;

        public override bool IsSuitable(IMessage message)
        {
            var rewardMessage = message as RewardsMessage;
            if (rewardMessage != null)
            {
                foreach (Reward reward in rewardMessage.Rewards)
                {
                    var counteItemReward = reward as CountedItemReward;
                    if (counteItemReward != null && _items.Contains(counteItemReward.Item))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}