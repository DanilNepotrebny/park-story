﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Messaging.Filtering
{
    [CreateAssetMenu(menuName = "Messages/Filters/Conditions/MessageAllSubConditions")]
    public class MessageAllSubConditions : MessageCondition
    {
        [SerializeField, RequiredField] private List<MessageCondition> _subConditions;

        public override bool IsSuitable(IMessage message)
        {
            return _subConditions.All(item => item.IsSuitable(message));
        }
    }
}