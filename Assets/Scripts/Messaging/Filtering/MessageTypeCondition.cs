﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DreamTeam.Messaging.Filtering
{
    [CreateAssetMenu(menuName = "Messages/Filters/Conditions/MessageTypeCondition")]
    public class MessageTypeCondition : MessageCondition
    {
        [SerializeField] private Mode _mode;
        [SerializeField] private List<DrawableMessageType> _types;

        public override bool IsSuitable(IMessage message)
        {
            Type messageType = message.GetType();
            switch (_mode)
            {
                case Mode.NotContains:
                    return _types.All(item => !item.Type.Equals(messageType));

                case Mode.Contains:
                default:
                    return _types.Any(item => item.Type.Equals(messageType));
            }
        }

        private enum Mode
        {
            Contains,
            NotContains
        }
    }
}