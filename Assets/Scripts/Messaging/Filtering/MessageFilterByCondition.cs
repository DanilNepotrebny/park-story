﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Messaging.Filtering
{
    [CreateAssetMenu(menuName = "Messages/Filters/MessageFilterByCondition")]
    public class MessageFilterByCondition : MessageFilter
    {
        [SerializeField, RequiredField] private MessageCondition _condition;

        public override void Apply(ICollection<IMessage> source, ICollection<IMessage> result)
        {
            result.Clear();
            foreach (IMessage message in source)
            {
                if (IsSuitable(message))
                {
                    result.Add(message);
                }
            }
        }

        public override bool IsSuitable(IMessage message)
        {
            return _condition.IsSuitable(message);
        }
    }
}