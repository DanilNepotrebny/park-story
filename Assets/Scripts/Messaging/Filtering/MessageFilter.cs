﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Messaging.Filtering
{
    public abstract class MessageFilter : ScriptableObject
    {
        public abstract void Apply(ICollection<IMessage> source, ICollection<IMessage> result);
        public abstract bool IsSuitable(IMessage message);
    }
}