﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Messaging.MessageAssets;
using DreamTeam.Popups;
using DreamTeam.SocialNetworking;
using DreamTeam.SocialNetworking.Facebook;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Messaging
{
    [Obfuscation(Exclude = false, Feature = "-rename")]
    public class AskRewardsMessage : RewardsMessage
    {
        [Inject] private SocialProfileInventoryExchangeSystem _exchangeSystem;
        [Inject] private FacebookManager _facebookManager;
        [Inject] private PopupSystem _popupSystem;
        [Inject] private AskRewardsMessageAsset _asset;

        [Inject]
        public AskRewardsMessage()
        {
            // Default, used for serialization
        }

        public AskRewardsMessage(Instantiator instantiator, List<Reward> rewards) : base(instantiator, rewards, GamePlace.Unknown)
        {
        }

        public override string Title => _localizationSystem.Localize(_asset.AskRewardsTitleKey, _asset);
        public override string Message => _localizationSystem.Localize(_asset.AskRewardsMessageKey, _asset);

        public override bool CanBeConsumed => Application.internetReachability != NetworkReachability.NotReachable && _facebookManager.IsLoggedIn;

        public override void NotifyConsumptionRefusal()
        {
            var missingConnectionArgs = new InformationPopup.Args()
            {
                TitleKey = _asset.MissingConnectionPopupTitleKey,
                MessageKey = _asset.MissingConnectionPopupMessageKey
            };

            Popup popup = _popupSystem.GetPopup(_asset.MissingConnectionPopup.Name);
            if (popup == null || popup.IsHidden)
            {
                _popupSystem.Show(_asset.MissingConnectionPopup.Name, missingConnectionArgs);
            }
        }

        public override void Consume()
        {
            _exchangeSystem.SendRewards(
                    Sender,
                    Rewards.ToList(),
                    () => { _messageSystem.Remove(this); }
                );
        }
    }
}