﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Synchronization.Factories;
using DreamTeam.Utils;

namespace DreamTeam.Messaging
{
    public class MessageSynchronizationFactory :
        ObjectSynchronizationFactory<IMessage, DrawableMessageType, MessageSynchronizationFactory.KeyRewardList>
    {
        protected override IMessage Instantiate(Instantiator instantiator, Type type)
        {
            return instantiator.InstantiateExplicit(type) as IMessage;
        }

        [Serializable]
        public class KeyRewardList : KeyValueList<string, DrawableMessageType>
        {
        }
    }
}
