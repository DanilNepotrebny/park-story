﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Messaging.Filtering;
using DreamTeam.SocialNetworking;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using UnityEngine;
using Zenject;

namespace DreamTeam.Messaging
{
    [RequireComponent(typeof(MessageSynchronizationFactory))]
    public partial class MessageSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private CharacterData _defaultMessagesSender;

        [Inject] private SaveGroup _saveGroup;
        [Inject] private SocialProfilesSystem _socialProfilesSystem;

        private List<IMessage> _messages = new List<IMessage>();
        private MessageSynchronizationFactory _messageSyncFactory => GetComponent<MessageSynchronizationFactory>();

        public event Action<IMessage> MessageAdded;
        public event Action<IMessage> MessageRemoved;

        public SocialProfile DefaultMessagesSender => _socialProfilesSystem.FindProfile(_defaultMessagesSender.name);

        public void Add(IMessage message)
        {
            if (!_messages.Contains(message))
            {
                _messages.Add(message);
                MessageAdded?.Invoke(message);
            }
        }

        public void Remove(IMessage message)
        {
            if (_messages.Contains(message))
            {
                _messages.Remove(message);
                MessageRemoved?.Invoke(message);
            }
        }

        public void Filter(MessageFilter filter, ICollection<IMessage> result)
        {
            result.Clear();
            filter.Apply(_messages, result);
        }

        public void Sync(State state)
        {
            state.SyncObjectList("messages", ref _messages, _messageSyncFactory);
        }

        public void SyncMessage(State state, ref IMessage message)
        {
            state.SyncObject("0", ref message, _messageSyncFactory);
        }

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("MessageSystem", this);
        }
    }
}