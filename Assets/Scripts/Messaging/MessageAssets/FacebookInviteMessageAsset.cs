﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using UnityEngine;

namespace DreamTeam.Messaging.MessageAssets
{
    [CreateAssetMenu(menuName = "Messages/FacebookInviteMessage")]
    public class FacebookInviteMessageAsset : ScriptableObject
    {
        [SerializeField, LocalizationKey] private string _inviteTitleKey;
        [SerializeField, LocalizationKey] private string _inviteMessageKey;

        public string InviteTitleKey => _inviteTitleKey;
        public string InviteMessageKey => _inviteMessageKey;
    }
}