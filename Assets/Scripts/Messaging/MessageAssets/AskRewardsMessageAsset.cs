﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Messaging.MessageAssets
{
    [CreateAssetMenu(menuName = "Messages/AskRewardsMessage")]
    public class AskRewardsMessageAsset : ScriptableObject
    {
        [Header("Message common")]
        [SerializeField, LocalizationKey] private string _askRewardsTitleKey;
        [SerializeField, LocalizationKey] private string _askRewardsMessageKey;
        [Header("Popup related")]
        [SerializeField] private SceneReference _missingConnectionPopup;
        [SerializeField, LocalizationKey] private string _missingConnectionPopupTitleKey;
        [SerializeField, LocalizationKey] private string _missingConnectionPopupMessageKey;

        public string AskRewardsTitleKey => _askRewardsTitleKey;
        public string AskRewardsMessageKey => _askRewardsMessageKey;
        public SceneReference MissingConnectionPopup => _missingConnectionPopup;
        public string MissingConnectionPopupTitleKey => _missingConnectionPopupTitleKey;
        public string MissingConnectionPopupMessageKey => _missingConnectionPopupMessageKey;
    }
}