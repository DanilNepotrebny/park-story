﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Messaging.MessageAssets
{
    [CreateAssetMenu(menuName = "Messages/CharacterRewardsMessage")]
    public partial class CharacterRewardsMessageAsset : ScriptableObject
    {
        [SerializeField] private CharacterData _sender;
        [SerializeField, RequiredField] private List<Reward> _rewards = new List<Reward>();
        [SerializeField] private SceneReference _popupScene = new SceneReference();

        public Texture GUIIcon => _sender.Photo.texture;
        public string SenderID => _sender.name;
        public List<Reward> Rewards => _rewards;
        public SceneReference PopupScene => _popupScene;
    }
}