﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using UnityEngine;

namespace DreamTeam.Messaging.MessageAssets
{
    [CreateAssetMenu(menuName = "Messages/SendRewardsMessage")]
    public class SendRewardsMessageAsset : ScriptableObject
    {
        [SerializeField, LocalizationKey] private string _sendRewardsTitleKey;
        [SerializeField, LocalizationKey] private string _sendRewardsMessageKey;

        public string SendRewardsTitleKey => _sendRewardsTitleKey;
        public string SendRewardsMessageKey => _sendRewardsMessageKey;
    }
}