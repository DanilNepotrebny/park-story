﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Reflection;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using Zenject;

namespace DreamTeam.Messaging
{
    [Obfuscation(Exclude = false, Feature = "-rename")]
    public class CharacterRewardsMessage : RewardsMessage
    {
        private string _popupSceneName;

        [Inject]
        public CharacterRewardsMessage()
        {
            // Default, used for serialization
        }

        public CharacterRewardsMessage(Instantiator instantiator, List<Reward> rewards, GamePlace gamePlace, string popupSceneName) : base(instantiator, rewards, gamePlace)
        {
            _popupSceneName = popupSceneName;
        }

        public string PopupSceneName => _popupSceneName;

        public override bool CanBeConsumed => true;

        public override void Sync(State state)
        {
            base.Sync(state);
            state.SyncString("popupSceneName", ref _popupSceneName);
        }
    }
}