﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Localization;
using DreamTeam.Messaging.MessageAssets;
using DreamTeam.SocialNetworking.Facebook;
using DreamTeam.SocialNetworking.Facebook.Request;
using DreamTeam.Synchronization.States;
using Zenject;

namespace DreamTeam.Messaging
{
    [Obfuscation(Exclude = false, Feature = "-rename")]
    public class FacebookInviteMessage : IMessage
    {
        [Inject] private MessageSystem _messageSystem;
        [Inject] private FacebookManager _facebookManager;
        [Inject] private FacebookInviteMessageAsset _facebookInviteMessageAsset;
        [Inject] private LocalizationSystem _localizationSystem;

        public bool CanBeConsumed => _facebookManager.IsLoggedIn;

        public void Consume()
        {
            RequestInviteFriends request = _facebookManager.InviteFriends(
                    _localizationSystem.Localize(_facebookInviteMessageAsset.InviteTitleKey, _facebookInviteMessageAsset),
                    _localizationSystem.Localize(_facebookInviteMessageAsset.InviteMessageKey, _facebookInviteMessageAsset),
                    "Invite data"
                );

            request.FriendsInvitationFinished += result =>
            {
                if (result != null &&
                    !result.Cancelled &&
                    string.IsNullOrEmpty(result.Error))
                {
                    _messageSystem.Remove(this);
                }
            };
        }

        public void NotifyConsumptionRefusal()
        {
        }

        public void Sync(State state)
        {
            //Do nothing
        }
    }
}