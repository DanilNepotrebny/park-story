﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;

namespace DreamTeam.Messaging
{
    [Serializable]
    public class DrawableMessageType : TypeReference<IMessage>
    {
    }
}