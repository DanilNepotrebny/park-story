﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if !NOCLOUD

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Cloud;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Analytics.Events.Global.Tutorial;
using DreamTeam.Analytics.Events.Location;
using DreamTeam.Analytics.Events.Match3;
using DreamTeam.Inventory;
using DreamTeam.Match3.Levels;
using DreamTeam.SocialNetworking;
using UnityEngine;
using UnityEngine.Purchasing;
using Zenject;

namespace DreamTeam.SDK.Cloud
{
    public class AppsFlyerWrapper : BaseAnalyticsWrapper
    {
        [SerializeField] private PlatformCredentials _appStoreCredentials;

        [Inject] private AnalyticsSystem _analyticsSystem;

        [Inject(Id = GlobalInstaller.MainLevelPackControllerId)]
        private SingleLevelPackController _mainLevelPackController;

        protected override void Initialize()
        {
            Credentials credentials;
            #if PRODUCTION
            {
                credentials = _appStoreCredentials.ProductionCredentials;
            }
            #else
            {
                credentials = _appStoreCredentials.TestCredentials;
            }
            #endif

            AppsFlyer.setAppsFlyerKey(credentials.DevKey);
            AppsFlyer.setAppID(credentials.AppId);
            AppsFlyer.setCustomerUserID(_analyticsSystem.UserId);
            AppsFlyer.trackAppLaunch();

            LogInfo($"[AppsFlyer] DevKey: {credentials.DevKey}");
            LogInfo($"[AppsFlyer] AppID: {credentials.AppId}");
            LogInfo($"[AppsFlyer] CustomerUserID: {_analyticsSystem.UserId}");

            SubscribeForEvents();
        }

        private void SubscribeForEvents()
        {
            _analyticsSystem.AddSubscriber<LevelSucceededEvent>(OnLevelSucceeded);
            _analyticsSystem.AddSubscriber<InAppPurchasedEvent>(OnInAppPurchased);
            _analyticsSystem.AddSubscriber<TutorialEvent>(OnTutorialStepCompleted);
            _analyticsSystem.AddSubscriber<BaseCustomEvent>(OnCustomEvent);
            _analyticsSystem.AddSubscriber<RewardedVideoShownEvent>(OnRewardedVideoShown);
            _analyticsSystem.AddSubscriber<SocialNetworkLoginEvent>(OnSocialNetworkLogin);
            _analyticsSystem.AddSubscriber<FriendsInvitationEvent>(OnFriendsInvitationFinished);
            _analyticsSystem.AddSubscriber<StoreRateEvent>(OnStoreRate);
        }

        private void OnStoreRate(StoreRateEvent storeRateEvent)
        {
            var eventParams = new Dictionary<string, string>
            {
                { AFInAppEvents.LEVEL, storeRateEvent.CurrentLevelInfo.CurrentLevelId }
            };
            TrackEvent(AFInAppEvents.RATE, eventParams);
        }

        private void OnFriendsInvitationFinished(FriendsInvitationEvent friendsInvitationEvent)
        {
            TrackEvent(AFInAppEvents.INVITE, new Dictionary<string, string>());
        }

        private void OnSocialNetworkLogin(SocialNetworkLoginEvent loginEvent)
        {
            var eventParams = new Dictionary<string, string>
            {
                { AFInAppEvents.REGSITRATION_METHOD, loginEvent.NetworkType.ToAnalyticsId() }
            };

            TrackEvent(AFInAppEvents.COMPLETE_REGISTRATION, eventParams);
        }

        private void OnCustomEvent(BaseCustomEvent customEvent)
        {
            var eventParams = new Dictionary<string, string>();

            foreach (KeyValuePair<string, object> pair in customEvent.Parameters)
            {
                if (pair.Value is bool)
                {
                    eventParams.Add(pair.Key, pair.Value.ToString().ToLower());
                }
                else
                {
                    eventParams.Add(pair.Key, pair.Value.ToString());
                }
            }

            TrackEvent(customEvent.EventName, eventParams);
        }

        private void OnRewardedVideoShown(RewardedVideoShownEvent shownEvent)
        {
            if (shownEvent.IsSuccessful)
            {
                var eventParams = new Dictionary<string, string>
                {
                    { "af_adrev_ad_type", "video" }
                };

                TrackEvent("af_ad_view", eventParams);
            }
        }

        private void OnTutorialStepCompleted(TutorialEvent tutorialEvent)
        {
            var eventParams = new Dictionary<string, string>
            {
                { AFInAppEvents.CONTENT_ID, tutorialEvent.TutorialStep.Id },
                { AFInAppEvents.SUCCESS, "true" }
            };
            TrackEvent(AFInAppEvents.TUTORIAL_COMPLETION, eventParams);
        }

        private void OnInAppPurchased(InAppPurchasedEvent iapEvent)
        {
            Product product = iapEvent.Product;
            ProductMetadata productMetadata = product.metadata;
            ProductPack productPack = iapEvent.PriceInstance.ProductPackInstance.ProductPack;

            var eventParams = new Dictionary<string, string>
            {
                { AFInAppEvents.CONTENT_ID, productPack.PurchaseId },
                { AFInAppEvents.CONTENT_TYPE, productPack.PurchaseType },
                { AFInAppEvents.REVENUE, productMetadata.localizedPrice.ToString() },
                { AFInAppEvents.CURRENCY, productMetadata.isoCurrencyCode },
                { AFInAppEvents.LEVEL, iapEvent.LevelInfo.CurrentLevelId }
            };
            TrackEvent(AFInAppEvents.PURCHASE, eventParams);
        }

        private void OnLevelSucceeded(LevelSucceededEvent succeededEvent)
        {
            ICurrentLevelInfo levelInfo = succeededEvent.LevelInfo;

            int currentLevelIndex = _mainLevelPackController.CurrentLevelIndex;
            if (currentLevelIndex == levelInfo.CurrentLevelIndex)
            {
                string nextLevelId = _mainLevelPackController.GetLevelId(currentLevelIndex + 1);

                var eventParams = new Dictionary<string, string>
                {
                    { AFInAppEvents.LEVEL, nextLevelId }
                };
                TrackEvent(AFInAppEvents.LEVEL_ACHIEVED, eventParams);
            }
        }

        private void TrackEvent(string eventName, Dictionary<string, string> eventParams)
        {
            AppsFlyer.trackRichEvent(eventName, eventParams);
            LogInfo($"[AppsFlyer] {eventName}({string.Join(", ", eventParams.Select(p => $"{p.Key}: {p.Value}"))})");
        }

        #region Inner types

        [Serializable]
        public class PlatformCredentials
        {
            public Credentials TestCredentials;
            public Credentials ProductionCredentials;
        }

        [Serializable]
        public class Credentials
        {
            public string AppId;
            public string DevKey;
        }

        #endregion
    }
}

#endif