﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if !NOCLOUD

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Cloud;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Analytics.Events.Global.Tutorial;
using DreamTeam.Analytics.Events.Match3;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Prices;
using DreamTeam.Match3.Levels;
using DreamTeam.SocialNetworking;
using DreamTeam.SocialNetworking.Facebook;
using DreamTeam.Utils;
using Facebook.Unity;
using UnityEngine;
using Zenject;

namespace DreamTeam.SDK.Cloud
{
    public class FacebookAnalyticsWrapper : BaseAnalyticsWrapper
    {
        [SerializeField, RequiredField] private TutorialStep _tutorialCompletionStep;

        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private FacebookManager _facebookManager;

        [Inject(Id = GlobalInstaller.MainLevelPackControllerId)]
        private SingleLevelPackController _mainLevelPackController;

        [Inject(Id = GlobalInstaller.MoneyId)]
        private CountedItem _money;

        protected override void Initialize()
        {
            _analyticsSystem.AddSubscriber<LevelSucceededEvent>(OnLevelSucceeded);
            _analyticsSystem.AddSubscriber<TutorialEvent>(OnTutorialStepCompleted);
            _analyticsSystem.AddSubscriber<BaseCustomEvent>(OnCustomEvent);
            _analyticsSystem.AddSubscriber<SocialNetworkLoginEvent>(OnSocialNetworkLogin);
            _analyticsSystem.AddSubscriber<IngameProductPackPurchaseEvent>(OnIngameProductPackPurchased);
        }

        private void OnIngameProductPackPurchased(IngameProductPackPurchaseEvent purchaseEvent)
        {
            CountedItemPrice price = purchaseEvent.Price;
            ProductPack pack = purchaseEvent.ProductPack;

            if (!string.IsNullOrEmpty(pack.PurchaseId) &&
                !string.IsNullOrEmpty(pack.PurchaseType) &&
                price.Item == _money)
            {
                var eventParams = new Dictionary<string, object>
                {
                    { AppEventParameterName.ContentID, pack.PurchaseId },
                    { AppEventParameterName.ContentType, pack.PurchaseType }
                };
                TrackEvent(AppEventName.SpentCredits, price.Count, eventParams);
            }
        }

        private void OnSocialNetworkLogin(SocialNetworkLoginEvent loginEvent)
        {
            var eventParams = new Dictionary<string, object>
            {
                { AppEventParameterName.RegistrationMethod, loginEvent.NetworkType.ToAnalyticsId() }
            };

            TrackEvent(AppEventName.CompletedRegistration, eventParams);
        }

        private void OnCustomEvent(BaseCustomEvent customEvent)
        {
            TrackEvent(customEvent.EventName, customEvent.Parameters);
        }

        private void OnTutorialStepCompleted(TutorialEvent tutorialEvent)
        {
            if (tutorialEvent.TutorialStep == _tutorialCompletionStep)
            {
                TrackEvent(AppEventName.CompletedTutorial, new Dictionary<string, object>());
            }
        }

        private void OnLevelSucceeded(LevelSucceededEvent succeededEvent)
        {
            ICurrentLevelInfo levelInfo = succeededEvent.LevelInfo;

            int currentLevelIndex = _mainLevelPackController.CurrentLevelIndex;
            if (currentLevelIndex == levelInfo.CurrentLevelIndex)
            {
                string nextLevelId = _mainLevelPackController.GetLevelId(currentLevelIndex + 1);

                var eventParams = new Dictionary<string, object>
                {
                    { AppEventParameterName.Level, nextLevelId }
                };
                TrackEvent(AppEventName.AchievedLevel, eventParams);
            }
        }

        private void TrackEvent(string eventName, Dictionary<string, object> eventParams)
        {
            _facebookManager.LogAppEvent(eventName, null, eventParams);
            LogInfo($"[Facebook] {eventName}({string.Join(", ", eventParams.Select(p => $"{p.Key}: {p.Value}"))})");
        }

        private void TrackEvent(string eventName, float? valueToSum, Dictionary<string, object> eventParams)
        {
            _facebookManager.LogAppEvent(eventName, valueToSum, eventParams);
            LogInfo($"[Facebook] {eventName}(valueToSum: {valueToSum}, {string.Join(", ", eventParams.Select(p => $"{p.Key}: {p.Value}"))})");
        }
    }
}

#endif