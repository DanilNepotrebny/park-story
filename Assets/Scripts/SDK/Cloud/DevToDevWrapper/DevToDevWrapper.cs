﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if !NOCLOUD

using System;
using System.Collections.Generic;
using System.Linq;
using DevToDev;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Cloud;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Analytics.Events.Global.Tutorial;
using DreamTeam.Analytics.Events.Match3;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Prices;
using DreamTeam.Match3.Levels;
using DreamTeam.Notifications;
using DTDEditor;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Purchasing;
using Zenject;

namespace DreamTeam.SDK.Cloud.DevToDevWrapper
{
    public partial class DevToDevWrapper : BaseAnalyticsWrapper, IDisposable
    {
        [SerializeField] private List<PlatformCredentials> _credentialsByPlatform;

        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private NotificationsSystem _notificationsSystem;
        [Inject] private List<CountedItem> _countedItems;

        [Inject(Id = GlobalInstaller.MainLevelPackControllerId)]
        private SingleLevelPackController _mainLevelPackController;

        private Dictionary<string, int> _spentCountedItems = new Dictionary<string, int>();
        private Dictionary<string, int> _earnedCountedItems = new Dictionary<string, int>();

        private bool _isLevelStarted;

        public void Dispose()
        {
            _notificationsSystem.NotificationsStateChanged -= SetPushNotificationsEnabled;
        }

        protected override void Initialize()
        {
            DevToDev.Analytics.UserId = _analyticsSystem.UserId;

            PlatformCredentials platformCredentials = _credentialsByPlatform.FirstOrDefault(c => c.Platform == GetPlatform());
            if (platformCredentials != null)
            {
                Credentials credentials;

                #if PRODUCTION
                {
                    credentials = platformCredentials.ProductionCredentials;
                }
                #else
                {
                    credentials = platformCredentials.TestCredentials;
                }
                #endif

                if (!string.IsNullOrEmpty(credentials.Key) &&
                    !string.IsNullOrEmpty(credentials.Secret))
                {
                    PushManager.PushTokenReceived = OnPushTokenReceived;
                    PushManager.PushTokenFailed = OnPushTokenFailed;

                    _notificationsSystem.NotificationsStateChanged += SetPushNotificationsEnabled;
                    SetPushNotificationsEnabled(_notificationsSystem.AreNotificationsEnabled);

                    DevToDev.Analytics.Initialize(credentials.Key, credentials.Secret);

                    LogInfo($"[DevToDev] DeviceId: {DevToDev.Analytics.DeviceId}");
                    LogInfo($"[DevToDev] AppKey: {credentials.Key}");
                    LogInfo($"[DevToDev] AppSecret: {credentials.Secret}");

                    SubscribeForEvents();
                }
                else
                {
                    UnityEngine.Debug.LogError($"[DevToDev] Invalid credentials for {GetPlatform()} platform.", this);
                }
            }
            else
            {
                UnityEngine.Debug.LogError($"[DevToDev] Platform credentials for '{GetPlatform()}' are not specified.", this);
            }
        }

        private void OnPushTokenReceived(string pushtoken)
        {
            UnityEngine.Debug.Log($"[DevToDev] Push token received token:{pushtoken}");
        }

        private void OnPushTokenFailed(string error)
        {
            UnityEngine.Debug.Log($"[DevToDev] Push token receiving failed: {error}");
        }

        private void SetPushNotificationsEnabled(bool areNotificationsEnabled)
        {
            PushManager.PushNotificationsEnabled = areNotificationsEnabled;
        }

        private DTDPlatform GetPlatform()
        {
            #if UNITY_ANDROID
            {
			    return DTDPlatform.Android;
            }
            #elif UNITY_IOS
            {
                return DTDPlatform.iOS;
            }
            #elif UNITY_METRO
            {
			    return DTDPlatform.WinStore;
            }
            #elif UNITY_WEBPLAYER || UNITY_WEBGL
            {
			    return DTDPlatform.WebGL;
            }
            #elif UNITY_STANDALONE_OSX
            {
			    return DTDPlatform.MacOS;
            }
            #elif UNITY_STANDALONE_WIN
            {
                return DTDPlatform.Windows;
            }
            #else
            {
			    throw new InvalidOperationException("Can't detect application platform.");
            }
            #endif
        }

        private void SubscribeForEvents()
        {
            _analyticsSystem.AddSubscriber<IngameProductPackPurchaseEvent>(OnIngameProductPackPurchased);
            _analyticsSystem.AddSubscriber<CountedItemSpentEvent>(OnCountedItemSpent);
            _analyticsSystem.AddSubscriber<CountedItemReceivedEvent>(OnCountedItemReceived);
            _analyticsSystem.AddSubscriber<LevelStartedEvent>(OnLevelStarted);
            _analyticsSystem.AddSubscriber<LevelFailedEvent>(OnLevelFailed);
            _analyticsSystem.AddSubscriber<LevelSucceededEvent>(OnLevelSucceeded);
            _analyticsSystem.AddSubscriber<OutOfMovesClosedEvent>(OnOutOfMoves);
            _analyticsSystem.AddSubscriber<TutorialEvent>(OnTutorialStep);
            _analyticsSystem.AddSubscriber<BaseCustomEvent>(OnCustomEvent);
            _analyticsSystem.AddSubscriber<InAppPurchasedEvent>(OnInAppPurchased);
        }

        private void OnInAppPurchased(InAppPurchasedEvent iapEvent)
        {
            Product product = iapEvent.Product;
            DevToDev.Analytics.RealPayment(
                    product.transactionID,
                    (float)product.metadata.localizedPrice,
                    iapEvent.PriceInstance.Price.Id,
                    product.metadata.isoCurrencyCode
                );

            LogInfo(
                    $"[DevToDev] RealPayment(" +
                    $"paymentId: {product.transactionID}, " +
                    $"inAppPrice: {(float)product.metadata.localizedPrice}, " +
                    $"inAppName: {iapEvent.PriceInstance.Price.Id}, " +
                    $"inAppCurrencyISOCode: {product.metadata.isoCurrencyCode}" +
                    $")"
                );
        }

        private void OnCustomEvent(BaseCustomEvent customEvent)
        {
            CustomEventParams eventParams = new CustomEventParams();
            foreach (KeyValuePair<string, object> pair in customEvent.Parameters)
            {
                if (pair.Value is bool)
                {
                    eventParams.AddParam(pair.Key, pair.Value.ToString().ToLower());
                }
                else if (pair.Value is int)
                {
                    eventParams.AddParam(pair.Key, (int)pair.Value);
                }
                else if (pair.Value is float)
                {
                    eventParams.AddParam(pair.Key, (float)pair.Value);
                }
                else if (pair.Value is string)
                {
                    eventParams.AddParam(pair.Key, (string)pair.Value);
                }
            }

            DevToDev.Analytics.CustomEvent(customEvent.EventName, eventParams);

            IEnumerable<string> parameters = customEvent.Parameters.Select(
                    p => p.Value is bool ?
                        $"{p.Key}: {p.Value.ToString().ToLower()}" :
                        $"{p.Key}: {p.Value}"
                );
            LogInfo($"[DevToDev] {customEvent.EventName}({string.Join(", ", parameters)})");
        }

        private void OnOutOfMoves(OutOfMovesClosedEvent offerClosedEvent)
        {
            if (offerClosedEvent.Decision == PurchaseDecision.Purchase)
            {
                CountedItemPrice price = offerClosedEvent.ProductPack.Price as CountedItemPrice;
                if (price != null)
                {
                    Assert.IsTrue(price.Item.IsCurrency);
                    AddCountedItemSpending(price.Item, price.Count);
                }
            }
        }

        private void OnTutorialStep(TutorialEvent tutorial)
        {
            DevToDev.Analytics.Tutorial(tutorial.TutorialStep.StepIndex);
            LogInfo($"[DevToDev] Tutorial({tutorial.TutorialStep.name})");
        }

        private void OnIngameProductPackPurchased(IngameProductPackPurchaseEvent purchaseEvent)
        {
            CountedItemPrice price = purchaseEvent.Price;
            ProductPack pack = purchaseEvent.ProductPack;

            if (!string.IsNullOrEmpty(pack.PurchaseId) &&
                !string.IsNullOrEmpty(pack.PurchaseType))
            {
                DevToDev.Analytics.InAppPurchase(
                        pack.PurchaseId,
                        pack.PurchaseType,
                        1,
                        price.Count,
                        price.Item.Id
                    );

                LogInfo(
                        $"[DevToDev] InAppPurchase(" +
                        $"PurchaseId: {pack.PurchaseId}, " +
                        $"PurchaseType: {pack.PurchaseType}, " +
                        $"PurchaseAmount: 1, " +
                        $"PurchasePriceAmount: {price.Count}, " +
                        $"PurchasePriceCurrency: {price.Item.Id}" +
                        $")"
                    );
            }
        }

        private void OnCountedItemSpent(CountedItemSpentEvent spentEvent)
        {
            if (!spentEvent.Item.IsCurrency)
            {
                AddCountedItemSpending(spentEvent.Item, spentEvent.Count);
            }
        }

        private void AddCountedItemSpending(CountedItem item, int count)
        {
            if (!_isLevelStarted)
            {
                return;
            }

            if (!_spentCountedItems.ContainsKey(item.Id))
            {
                _spentCountedItems.Add(item.Id, count);
            }
            else
            {
                _spentCountedItems[item.Id] += count;
            }
        }

        private void OnCountedItemReceived(CountedItemReceivedEvent receivedEvent)
        {
            if (receivedEvent.Item.IsCurrency)
            {
                AccrualType accrualType;
                if (TryGetCurrencyAccrualType(receivedEvent.ReceivingType, out accrualType))
                {
                    DevToDev.Analytics.CurrencyAccrual(
                            receivedEvent.Count,
                            receivedEvent.Item.Id,
                            accrualType
                        );

                    LogInfo(
                            $"[DevToDev] CurrencyAccrual(" +
                            $"PurchasePriceAmount: {receivedEvent.Count}, " +
                            $"PurchasePriceCurrency: {receivedEvent.Item.Id}, " +
                            $"AccrualType: {accrualType}" +
                            $")"
                        );
                }
            }

            if (_isLevelStarted &&
                receivedEvent.ReceivingType == ReceivingType.Earned)
            {
                if (!_earnedCountedItems.ContainsKey(receivedEvent.Item.Id))
                {
                    _earnedCountedItems.Add(receivedEvent.Item.Id, receivedEvent.Count);
                }
                else
                {
                    _earnedCountedItems[receivedEvent.Item.Id] += receivedEvent.Count;
                }
            }
        }

        private bool TryGetCurrencyAccrualType(ReceivingType receivingType, out AccrualType accrualType)
        {
            switch (receivingType)
            {
                case ReceivingType.Earned:
                    accrualType = AccrualType.Earned;
                    return true;

                case ReceivingType.Purchased:
                    accrualType = AccrualType.Purchased;
                    return true;

                default:
                    accrualType = AccrualType.Earned;
                    return false;
            }
        }

        private void OnLevelStarted(LevelStartedEvent levelStartedEvent)
        {
            LocationEventParams eventParams = new LocationEventParams();
            DevToDev.Analytics.StartProgressionEvent(
                    levelStartedEvent.LevelInfo.CurrentLevelId,
                    eventParams
                );
            _isLevelStarted = true;

            LogInfo($"[DevToDev] StartProgressionEvent(Level: {levelStartedEvent.LevelInfo.CurrentLevelId})");
        }

        private void OnLevelFailed(LevelFailedEvent failedEvent)
        {
            OnLevelEnded(failedEvent.LevelInfo, false);
        }

        private void OnLevelSucceeded(LevelSucceededEvent succeededEvent)
        {
            ICurrentLevelInfo levelInfo = succeededEvent.LevelInfo;

            int currentLevelIndex = _mainLevelPackController.CurrentLevelIndex;
            if (currentLevelIndex == levelInfo.CurrentLevelIndex)
            {
                Dictionary<string, int> currencies = new Dictionary<string, int>();
                foreach (CountedItem item in _countedItems)
                {
                    if (item.IsCurrency)
                    {
                        currencies.Add(item.Id, item.Count);
                    }
                }

                int nextLevelIndex = currentLevelIndex + 1;
                int nextLevelNumber = nextLevelIndex + 1;

                DevToDev.Analytics.LevelUp(
                        nextLevelNumber,
                        currencies
                    );

                LogInfo(
                        $"[DevToDev] LevelUp(" +
                        $"Level: {nextLevelNumber}, " +
                        $"Currencies: {{ {string.Join(", ", currencies.Select(p => $"{p.Key}: {p.Value}"))} }}" +
                        $")"
                    );
            }

            OnLevelEnded(levelInfo, true);
        }

        private void OnLevelEnded(ICurrentLevelInfo levelInfo, bool IsSuccessful)
        {
            LocationEventParams eventParams = new LocationEventParams();
            eventParams.SetSuccessfulCompletion(IsSuccessful);
            eventParams.SetSpent(_spentCountedItems);
            eventParams.SetEarned(_earnedCountedItems);
            DevToDev.Analytics.EndProgressionEvent(
                    levelInfo.CurrentLevelId,
                    eventParams
                );

            LogInfo(
                    $"[DevToDev] EndProgressionEvent(" +
                    $"Level: {levelInfo.CurrentLevelId}, " +
                    $"IsSuccessful: {IsSuccessful}, " +
                    $"Spent: {{ {string.Join(", ", _spentCountedItems.Select(p => $"{p.Key}: {p.Value}"))} }} " +
                    $"Earned: {{ {string.Join(", ", _earnedCountedItems.Select(p => $"{p.Key}: {p.Value}"))} }}" +
                    $")"
                );

            _isLevelStarted = false;
            _spentCountedItems.Clear();
            _earnedCountedItems.Clear();
        }

        #region Inner types

        [Serializable]
        public class PlatformCredentials
        {
            public DTDPlatform Platform;
            public Credentials TestCredentials;
            public Credentials ProductionCredentials;
        }

        [Serializable]
        public class Credentials
        {
            public string Key;
            public string Secret;
        }

        #endregion
    }
}

#endif