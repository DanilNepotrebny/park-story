﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Inventory
{
    public enum PurchaseDecision
    {
        Purchase,
        Close
    }

    public static class PurchaseDecisionExtensions
    {
        public static string ToAnalyticsId(this PurchaseDecision self)
        {
            switch (self)
            {
                case PurchaseDecision.Purchase:
                    return "purchase";
                case PurchaseDecision.Close:
                    return "close";
            }

            UnityEngine.Debug.LogError($"Ivalid value for {self.GetType().Name} enum.");
            return "";
        }
    }
}