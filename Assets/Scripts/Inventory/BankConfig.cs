﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Inventory
{
    [CreateAssetMenu]
    public class BankConfig : ScriptableObject
    {
        [SerializeField] private List<ProductPack> _allProducts;
        [SerializeField] private List<ProductPack> _defaultProducts;
        [SerializeField] private ProductPack _bestValuePack;
        [SerializeField] private ProductPack _mostPopularPack;

        public IReadOnlyList<ProductPack> All => _allProducts;
        public IReadOnlyList<ProductPack> Default => _defaultProducts;
        public ProductPack BestValuePack => _bestValuePack;
        public ProductPack MostPopularPack => _mostPopularPack;
    }
}