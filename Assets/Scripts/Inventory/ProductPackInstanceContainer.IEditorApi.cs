﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using UnityEditor;

namespace DreamTeam.Inventory
{
    public partial class ProductPackInstanceContainer : ProductPackInstanceContainer.IEditorApi
    {
        void IEditorApi.RegisterProductPack(ProductPack pack)
        {
            if (pack != null && !_productPacks.Contains(pack))
            {
                Undo.RecordObject(this, "Register Product Pack");
                _productPacks.Add(pack);
            }
        }

        void IEditorApi.ClearMissingProductPacks()
        {
            Undo.RecordObject(this, "ClearMissingProductPacks");
            _productPacks.RemoveAll(pack => pack == null);
        }

        #region Inner Types

        public interface IEditorApi
        {
            void RegisterProductPack(ProductPack pack);
            void ClearMissingProductPacks();
        }

        #endregion
    }
}

#endif