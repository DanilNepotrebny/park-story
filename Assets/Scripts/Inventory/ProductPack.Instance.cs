﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Analytics;
using DreamTeam.Inventory.Prices;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Utils;
using DreamTeam.Utils.Instance;
using DreamTeam.Utils.Invocation;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Inventory
{
    public partial class ProductPack : IInstanceFactory<ProductPack.Instance>
    {
        Instance IInstanceFactory<Instance>.CreateInstance(Instantiator instantiator)
        {
            return instantiator.Instantiate<Instance>(this);
        }

        public class Instance
        {
            [Inject] private AnalyticsSystem _analyticsSystem;
            [Inject] private Instantiator _instantiator;

            private GamePlace _purchaseGamePlace;

            public ProductPack ProductPack { get; }

            public event Action<Instance, IDeferredInvocationHandle> Giving;
            public event Action<Instance, GamePlace> Given;
            public event Action<Instance, GamePlace> Purchased;

            public Price.Instance Price { get; }

            public bool IsSpecialOffer => ProductPack.IsSpecialOffer;

            protected Instance(
                    ProductPack productPack,
                    [Inject] PriceInstanceContainer priceInstanceContainer
                )
            {
                ProductPack = productPack;

                if (productPack.Price != null)
                {
                    Price = priceInstanceContainer.GetInstance(productPack.Price);
                    Price.ProductPackInstance = this;
                    Price.Payed += OnPricePayed;
                }
            }

            private void OnPricePayed(GamePlace gamePlace)
            {
                StartGivingInvocation(ReceivingType.Purchased, gamePlace);
                Purchased?.Invoke(this, gamePlace);
            }

            public void TryPurchase(GamePlace gamePlace)
            {
                Assert.IsTrue(gamePlace != GamePlace.Unknown);
                Price.TryPay(gamePlace);
            }

            public void GiveForFree(GamePlace gamePlace)
            {
                StartGivingInvocation(ReceivingType.Earned, gamePlace);
            }

            public IAnalyticsEvent CreatePurchaseOfferedEvent(
                    Instantiator instantiator,
                    GamePlace gamePlace
                )
            {
                return Price.CreatePurchaseOfferedEvent(instantiator, ProductPack, gamePlace);
            }

            public IAnalyticsEvent CreatePurchaseResultEvent(
                    Instantiator instantiator,
                    GamePlace gamePlace,
                    PurchaseDecision purchaseDecision
                )
            {
                return Price.CreatePurchaseResultEvent(instantiator, ProductPack, gamePlace, purchaseDecision);
            }

            private void StartGivingInvocation(ReceivingType receivingType, GamePlace gamePlace)
            {
                using (IDeferredInvocationHandle handle = new DeferredInvocation(() => Give(receivingType, gamePlace)).Start())
                {
                    Giving?.Invoke(this, handle);
                }
            }

            private void Give(ReceivingType receivingType, GamePlace gamePlace)
            {
                foreach (Reward reward in ProductPack.Rewards)
                {
                    reward.Receive(_instantiator, receivingType, gamePlace);
                }

                Given?.Invoke(this, gamePlace);
            }
        }
    }
}