﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using UnityEngine;

namespace DreamTeam.Inventory.Prices
{
    /// <summary>
    /// Price that is payed by spending some CountedItem.
    /// </summary>
    [CreateAssetMenu(fileName = "CountedItemPrice", menuName = "Inventory/Prices/CountedItemPrice")]
    public partial class CountedItemPrice : Price, ICountedItemQuantity
    {
        [SerializeField] private CountedItem _item;
        [SerializeField] private int _count = 1;

        public CountedItem Item => _item;
        public int Count => _count;
    }
}