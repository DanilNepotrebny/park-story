﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.IAP;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Purchasing;
using Zenject;

namespace DreamTeam.Inventory.Prices
{
    public partial class IAPPrice
    {
        public override Price.Instance CreateInstance(Instantiator instantiator)
        {
            return instantiator.Instantiate<Instance>(this);
        }

        public new class Instance : Price.Instance, ISynchronizable
        {
            [Inject] private AnalyticsSystem _analyticsSystem;
            [Inject] private Instantiator _instantiator;

            private IAPSystem _iapSystem;
            private GamePlace _pendingPurchaseGamePlace;
            private Product _product;

            #if !IAP
            private string _fakeLocalizedPriceString;
            private const int _fakeLocalizedPriceDelay = 5;
            #endif

            public IAPPrice Price { get; }

            public string LocalizedPriceString
            {
                get
                {
                    if (_product != null)
                    {
                        return _product.metadata.localizedPriceString;
                    }

                    #if IAP
                    {
                        return null;
                    }
                    #else
                    {
                        if (string.IsNullOrEmpty(_fakeLocalizedPriceString))
                        {
                            LeanTween.delayedCall(
                                    _fakeLocalizedPriceDelay,
                                    () =>
                                    {
                                        _fakeLocalizedPriceString = "$0.01";
                                        LocalizedPriceChanged?.Invoke();
                                    }
                                );
                        }

                        return _fakeLocalizedPriceString;
                    }
                    #endif
                }
            }

            public override bool CanBePayed => Application.internetReachability != NetworkReachability.NotReachable;

            public override event Action<GamePlace> Payed;

            public event Action LocalizedPriceChanged;

            protected Instance(
                    IAPPrice price,
                    [Inject] IAPSystem iapSystem
                )
            {
                Price = price;
                _iapSystem = iapSystem;
                _iapSystem.RegisterPrice(this);
            }

            public override void TryPay(GamePlace place)
            {
                _pendingPurchaseGamePlace = place;
                _iapSystem.Purchase(this);
            }

            public override IAnalyticsEvent CreatePurchaseOfferedEvent(
                    Instantiator instantiator,
                    ProductPack productPack,
                    GamePlace gamePlace
                )
            {
                return null;
            }

            public override IAnalyticsEvent CreatePurchaseResultEvent(
                    Instantiator instantiator,
                    ProductPack productPack,
                    GamePlace gamePlace,
                    PurchaseDecision purchaseDecision
                )
            {
                return null;
            }

            public void OnPurchaseCompleted()
            {
                Payed?.Invoke(_pendingPurchaseGamePlace);

                if (_product != null)
                {
                    var iapEvent = _instantiator.Instantiate<InAppPurchasedEvent>(this, _product);
                    _analyticsSystem.Send(iapEvent);
                }
            }

            public void OnProductInitialized(Product product)
            {
                _product = product;
                LocalizedPriceChanged?.Invoke();
            }

            public void Sync(State state)
            {
                state.SyncEnum("GamePlace", ref _pendingPurchaseGamePlace);
            }
        }
    }
}