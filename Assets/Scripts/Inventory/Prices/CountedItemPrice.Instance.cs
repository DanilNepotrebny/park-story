﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Utils;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Inventory.Prices
{
    public partial class CountedItemPrice
    {
        public override Price.Instance CreateInstance(Instantiator instantiator)
        {
            return instantiator.Instantiate<Instance>(this);
        }

        public new class Instance : Price.Instance
        {
            [Inject] private AnalyticsSystem _analyticsSystem;
            [Inject] private Instantiator _instantiator;

            public CountedItemPrice Price { get; }

            public override bool CanBePayed => Price.Item.IsSpendingPossible(Price.Count);

            public override event Action<GamePlace> Payed;

            protected Instance(CountedItemPrice price)
            {
                Price = price;
            }

            public override void TryPay(GamePlace gamePlace)
            {
                Assert.IsTrue(gamePlace != GamePlace.Unknown);
                bool isSuccessful = Price.Item.TrySpend(Price.Count, gamePlace);

                if (isSuccessful)
                {
                    Payed?.Invoke(gamePlace);

                    if (ProductPackInstance != null)
                    {
                        IAnalyticsEvent analyticsEvent = _instantiator.Instantiate<IngameProductPackPurchaseEvent>(
                                Price,
                                ProductPackInstance.ProductPack,
                                gamePlace
                            );
                        if (analyticsEvent != null)
                        {
                            _analyticsSystem.Send(analyticsEvent);
                        }
                    }
                }
            }

            /// <returns>Returns Null if can't be payed.</returns>
            public ICancelable TrySchedulePayment(GamePlace gamePlace)
            {
                ICancelable cancelable;
                Price.Item.TryScheduleSpending(Price.Count, gamePlace, out cancelable);
                return cancelable;
            }

            public override IAnalyticsEvent CreatePurchaseOfferedEvent(
                    Instantiator instantiator,
                    ProductPack productPack,
                    GamePlace gamePlace
                )
            {
                return instantiator.Instantiate<IngameProductPackPurchaseOfferedEvent>(Price, productPack, gamePlace);
            }

            public override IAnalyticsEvent CreatePurchaseResultEvent(
                    Instantiator instantiator,
                    ProductPack productPack,
                    GamePlace gamePlace,
                    PurchaseDecision purchaseDecision
                )
            {
                return instantiator.Instantiate<IngameProductPackPurchaseResultEvent>(
                        Price,
                        productPack,
                        gamePlace,
                        purchaseDecision
                    );
            }
        }
    }
}