﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using DreamTeam.Inventory.Items;
using UnityEditor;

namespace DreamTeam.Inventory.Prices
{
    public partial class CountedItemPrice : CountedItemPrice.IEditorApi
    {
        void IEditorApi.SetCount(int count)
        {
            Undo.RecordObject(this, "PriceSetCount");
            _count = count;
        }

        void IEditorApi.SetItem(CountedItem item)
        {
            Undo.RecordObject(this, "PriceSetItem");
            _item = item;
        }

        #region Inner Types

        public interface IEditorApi
        {
            void SetItem(CountedItem item);
            void SetCount(int count);
        }

        #endregion
    }
}

#endif