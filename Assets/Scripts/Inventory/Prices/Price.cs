﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Analytics;
using DreamTeam.Utils;
using DreamTeam.Utils.Instance;
using UnityEngine;

namespace DreamTeam.Inventory.Prices
{
    /// <summary>
    /// Represents a base class for all spendings of the player.
    /// </summary>
    public abstract class Price : ScriptableObject, IInstanceFactory<Price.Instance>
    {
        public abstract Instance CreateInstance(Instantiator instantiator);

        public abstract class Instance
        {
            public abstract event Action<GamePlace> Payed;

            public abstract bool CanBePayed { get; }

            public ProductPack.Instance ProductPackInstance { get; set; }

            public abstract void TryPay(GamePlace gamePlace);

            public abstract IAnalyticsEvent CreatePurchaseOfferedEvent(
                    Instantiator instantiator,
                    ProductPack productPack,
                    GamePlace gamePlace
                );

            public abstract IAnalyticsEvent CreatePurchaseResultEvent(
                    Instantiator instantiator,
                    ProductPack productPack,
                    GamePlace gamePlace,
                    PurchaseDecision purchaseDecision
                );
        }
    }
}