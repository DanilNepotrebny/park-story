﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Zenject;

namespace DreamTeam.Inventory.Prices
{
    /// <summary>
    /// Simple presenter for InAppPurchasePrice.
    /// </summary>
    public class IAPPricePresenter : MonoBehaviour, IPresenter<Price>
    {
        [SerializeField, RequiredField] private Text _text;
        [SerializeField, RequiredField] private GameObject _waitingIcon;

        [Inject] private PriceInstanceContainer _priceInstanceContainer;

        private IAPPrice.Instance _priceInstance;

        public void Initialize(Price model)
        {
            UnsubscribePrice();
            _priceInstance = _priceInstanceContainer.GetInstance(model) as IAPPrice.Instance;
            Assert.IsNotNull(
                    _priceInstance,
                    $"Invalid reward type. The price is {model.GetType()} and should be subclass of {nameof(IAPPrice)}"
                );
            UpdatePrice();
            SubscribePrice();
        }

        protected void OnEnable()
        {
            UpdatePrice();
            SubscribePrice();
        }

        protected void OnDisable()
        {
            UnsubscribePrice();
        }

        private void SubscribePrice()
        {
            if (_priceInstance != null)
            {
                _priceInstance.LocalizedPriceChanged += UpdatePrice;
            }
        }

        private void UnsubscribePrice()
        {
            if (_priceInstance != null)
            {
                _priceInstance.LocalizedPriceChanged -= UpdatePrice;
            }
        }

        private void UpdatePrice()
        {
            if (_priceInstance != null)
            {
                string price = _priceInstance?.LocalizedPriceString;
                if (price != null)
                {
                    _text.text = price;
                }

                _text.gameObject.SetActive(price != null);
                _waitingIcon.SetActive(price == null);
            }
        }
    }
}