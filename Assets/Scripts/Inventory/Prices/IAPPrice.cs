﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Purchasing;

namespace DreamTeam.Inventory.Prices
{
    [CreateAssetMenu(fileName = "InAppPurchasePrice", menuName = "Inventory/Prices/InAppPurchasePrice")]
    public partial class IAPPrice : Price
    {
        [SerializeField] private string _id;
        [SerializeField] private StoreIdMap _storeIds;

        public IEnumerable<KeyValuePair<Store, string>> StoreIds => _storeIds;

        public string Id => _id;
        public ProductType Type => ProductType.Consumable;

        #region Inner types

        public enum Store
        {
            AppleAppStore,
            GooglePlay
        }

        [Serializable]
        private class StoreIdMap : KeyValueList<Store, string> { }

        #endregion
    }
}