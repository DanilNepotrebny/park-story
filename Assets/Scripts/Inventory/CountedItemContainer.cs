﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory.Items;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DreamTeam.Inventory
{
    public class CountedItemContainer : ScriptableObject
    {
        [SerializeField] private List<CountedItem> _countedItems;

        public IEnumerator<CountedItem> GetEnumerator()
        {
            return _countedItems.GetEnumerator();
        }

        #if UNITY_EDITOR

        public void RegisterCountedItem(CountedItem item)
        {
            if (item != null && !_countedItems.Contains(item))
            {
                Undo.RecordObject(this, "RegisterCountedItem");
                _countedItems.Add(item);
            }
        }

        public void ClearMissingCountedItems()
        {
            Undo.RecordObject(this, "ClearMissingCountedItems");
            _countedItems.RemoveAll(item => item == null);
        }

        #endif
    }
}