﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Inventory.Prices;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Localization;
using DreamTeam.UI.DataProviders;
using UnityEngine;

namespace DreamTeam.Inventory
{
    /// <summary>
    /// Purchasable pack of the rewards with specified price.
    /// </summary>
    [CreateAssetMenu(fileName = "ProductPack", menuName = "Inventory/Product Pack")]
    public partial class ProductPack : ScriptableObject, IDescriptionProvider, ICollection<Reward>
    {
        [SerializeField, LocalizationKey] private string _name;
        [SerializeField, LocalizationKey] private string _description;
        [SerializeField] private Sprite _icon;
        [SerializeField, HideInInspector] private List<Reward> _rewards = new List<Reward>();
        [SerializeField, HideInInspector] private Price _price;
        [SerializeField] private string _purchaseId;
        [SerializeField] private string _purchaseType;
        [SerializeField] private bool _isSpecialOffer;

        public Price Price => _price;

        public IList<Reward> Rewards => _rewards.AsReadOnly();
        public bool CanBeReceived => _rewards.All(r => r.CanBeReceived);

        public string Name => _name;
        public string Description => _description;
        public Sprite Icon => _icon;

        public string PurchaseId => _purchaseId;
        public string PurchaseType => _purchaseType;

        public bool IsSpecialOffer => _isSpecialOffer;

        public IEnumerator<Reward> GetEnumerator()
        {
            return _rewards.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(Reward item)
        {
            _rewards.Add(item);
        }

        public void Clear()
        {
            _rewards.Clear();
        }

        public bool Contains(Reward item)
        {
            return _rewards.Contains(item);
        }

        public void CopyTo(Reward[] array, int arrayIndex)
        {
            _rewards.CopyTo(array, arrayIndex);
        }

        public bool Remove(Reward item)
        {
            return _rewards.Remove(item);
        }

        public int Count => _rewards.Count;

        public bool IsReadOnly => true;
    }
}