﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Prices;
using DreamTeam.Utils.Instance;

namespace DreamTeam.Inventory
{
    public class PriceInstanceContainer : ScriptableInstanceContainer<Price.Instance>
    {
    }
}