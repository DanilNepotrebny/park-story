﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using DreamTeam.Inventory.Prices;
using DreamTeam.Inventory.Rewards;
using UnityEditor;

namespace DreamTeam.Inventory
{
    public partial class ProductPack : ProductPack.IEditorApi
    {
        string IEditorApi.PurchaseId
        {
            get { return PurchaseId; }

            set
            {
                EditorUtility.SetDirty(this);
                _purchaseId = value;
            }
        }

        string IEditorApi.PurchaseType
        {
            get { return PurchaseType; }

            set
            {
                EditorUtility.SetDirty(this);
                _purchaseType = value;
            }
        }

        void IEditorApi.AddReward(Reward reward)
        {
            Undo.RecordObject(this, "ProductPackAddReward");
            _rewards.Add(reward);
        }

        void IEditorApi.SetPrice(Price price)
        {
            Undo.RecordObject(this, "ProductPackSetPrice");
            _price = price;
        }

        #region Inner Types

        public interface IEditorApi
        {
            void AddReward(Reward reward);
            void SetPrice(Price price);

            string PurchaseId { get; set; }
            string PurchaseType { get; set; }
        }

        #endregion
    }
}

#endif