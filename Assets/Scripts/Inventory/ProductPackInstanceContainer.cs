﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils.Instance;
using UnityEngine;
using Zenject;

namespace DreamTeam.Inventory
{
    public partial class ProductPackInstanceContainer : ScriptableInstanceContainer<ProductPack.Instance>
    {
        [SerializeField] private List<ProductPack> _productPacks = new List<ProductPack>();

        [Inject] private void Construct()
        {
            foreach (ProductPack pack in _productPacks)
            {
                CreateInstance(pack);
            }
        }
    }
}