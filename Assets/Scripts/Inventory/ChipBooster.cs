﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using UnityEngine;

#if !NO_MATCH3
using DreamTeam.Match3;
using DreamTeam.Match3.ChipBoosterPlacingRules;
#endif

namespace DreamTeam.Inventory
{
    [CreateAssetMenu(fileName = "ChipBooster", menuName = "Inventory/Chip Booster")]
    public class ChipBooster : Booster
    {
        #if !NO_MATCH3

        [SerializeField] private ChipComponent[] _chipPrefabs;

        [SerializeField] private BaseChipBoosterPlacingRule _placingRulePrefab;

        // Prefabs to be placed on grid by this booster
        public ChipComponent[] ChipPrefabs => _chipPrefabs;

        public BaseChipBoosterPlacingRule PlacingRulePrefab => _placingRulePrefab;

        #endif
    }
}