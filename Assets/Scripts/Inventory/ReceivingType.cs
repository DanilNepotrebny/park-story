﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Inventory
{
    public enum ReceivingType
    {
        Earned,
        Purchased,
        Other
    }

    public static class ReceivingTypeExtensions
    {
        public static string ToAnalyticsId(this ReceivingType self)
        {
            switch (self)
            {
                case ReceivingType.Earned:
                    return "earned";
                case ReceivingType.Purchased:
                    return "purchased";
            }

            UnityEngine.Debug.LogError($"Ivalid value for {self.GetType().Name} enum.");
            return "";
        }
    }
}