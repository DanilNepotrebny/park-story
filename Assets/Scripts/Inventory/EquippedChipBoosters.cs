﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Inventory
{
    [CreateAssetMenu(fileName = "EquippedChipBoosters", menuName = "Inventory/Equipped Chip Boosters Cache")]
    public class EquippedChipBoosters : ScriptableObject, IEnumerable<ChipBooster>
    {
        public Action<ChipBooster> EquippedOrUnequipped;

        private HashSet<ChipBooster> _equippedBoosters { get; } = new HashSet<ChipBooster>();

        /// <summary>
        /// Equipped ChipBooster to generate its chips. Generated chips will replace existed ones in destionation cells
        /// </summary>
        public void Equip(ChipBooster booster)
        {
            if (!IsEquipped(booster))
            {
                _equippedBoosters.Add(booster);
                EquippedOrUnequipped?.Invoke(booster);
            }
        }

        /// <summary>
        /// Unequip booster
        /// </summary>
        public void Unequip(ChipBooster booster)
        {
            if (IsEquipped(booster))
            {
                _equippedBoosters.Remove(booster);
                EquippedOrUnequipped?.Invoke(booster);
            }
        }

        /// <summary>
        /// Checks whether ChipBooster is equipped
        /// </summary>
        public bool IsEquipped(ChipBooster booster)
        {
            return _equippedBoosters.Contains(booster);
        }

        /// <summary>
        /// Checks whether any ChipBooster is equipped
        /// </summary>
        /// <returns></returns>
        public bool Any()
        {
            return _equippedBoosters.Count > 0;
        }

        public void UnequipAll()
        {
            _equippedBoosters.Clear();
        }

        public IEnumerator<ChipBooster> GetEnumerator()
        {
            return _equippedBoosters.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _equippedBoosters.GetEnumerator();
        }
    }
}