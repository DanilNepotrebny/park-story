﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Synchronization.Factories;
using DreamTeam.Utils;

namespace DreamTeam.Inventory.Rewards
{
    public class RewardSynchronizationFactory :
        ObjectSynchronizationFactory<Reward, RewardSynchronizationFactory.RewardReference, RewardSynchronizationFactory.KeyRewardList>
    {
        protected override Reward Instantiate(Instantiator instantiator, Type type)
        {
            return instantiator.InstantiateRuntimeScriptableObject(type) as Reward;
        }

        [Serializable]
        public class RewardReference : TypeReference<Reward>
        {
        }

        [Serializable]
        public class KeyRewardList : KeyValueList<string, RewardReference>
        {
        }
    }
}