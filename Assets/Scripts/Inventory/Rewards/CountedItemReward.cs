﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Inventory.Items;
using DreamTeam.Messaging;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.TransformLookup;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Inventory.Rewards
{
    public abstract partial class CountedItemReward : Reward, ICountedItemQuantity
    {
        [SerializeField, RequiredField] private CountedItem _item;

        [Inject] private ISynchronizer<CountedItem> _itemSynchronizer;
        [Inject] private Instantiator _instantiator;
        [Inject] private MessageSystem _messageSystem;

        public override bool CanBeReceived => !_item.IsLocked && _item.IsReceivingPossible(Count);

        public CountedItem Item
        {
            get { return _item; }
            protected set { _item = value; }
        }

        public override TransformLookupId TransformId => _item.TransformId;
        public abstract int Count { get; }

        public override void Sync(State state)
        {
            state.SyncObject("item", ref _item, _itemSynchronizer);
        }

        public override void OnReceive(ReceivingType receivingType, GamePlace gamePlace)
        {
            if (_item.IsReceivingPossible(Count))
            {
                _item.TryReceive(Count, receivingType, gamePlace);
            }
        }

        public override void OnReceiveDeclined(ReceivingType receivingType, GamePlace gamePlace)
        {
            if (_item.CanBeStoredAsMessage())
            {
                RewardsMessage message = new RewardsMessage(_instantiator, new List<Reward> { this }, gamePlace);
                message.Sender = _messageSystem.DefaultMessagesSender;
                _messageSystem.Add(message);
            }
        }

        public override void NotifyReceivingRefusal()
        {
            if (!CanBeReceived)
            {
                _item.ShowMaxCountPopup();
            }
        }

        protected virtual void OnValidate()
        {
            if (_item == null)
            {
                UnityEngine.Debug.LogAssertion($"Missing {nameof(_item)}", this);
            }
        }
    }
}