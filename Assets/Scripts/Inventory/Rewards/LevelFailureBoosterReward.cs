﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Inventory.Rewards
{
    public class LevelFailureBoosterReward : CountedItemReward
    {
        public override int Count => 1;
        public ChipBooster Booster => Item as ChipBooster;

        protected override void OnValidate()
        {
            base.OnValidate();

            if (Item != null && !(Item is ChipBooster))
            {
                UnityEngine.Debug.LogAssertion($"{Item.name} is not a {nameof(ChipBooster)}");
                Item = null;
            }
        }
    }
}