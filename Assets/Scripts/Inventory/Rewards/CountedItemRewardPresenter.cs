﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Inventory.Items;
using DreamTeam.UI;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace DreamTeam.Inventory.Rewards
{
    /// <summary>
    /// Simple presenter for CountedItemReward.
    /// </summary>
    [Obsolete(
            "Please use CountedItemRewardCountPresenter, CountedItemRewardItemPresenter " +
            "CountedItemRewardPresenterInitializer and CountedItemRewardPresenterHub in conjunction.",
            true
        )]
    public class CountedItemRewardPresenter : MonoBehaviour, IPresenter<Reward>
    {
        [SerializeField] private CountedItemReward _reward;
        [SerializeField] private Text _countText;

        public void Initialize(Reward model)
        {
            Assert.IsTrue(
                model is CountedItemReward,
                $"Invalid reward type. The reward is {model.GetType()} and should be subclass of {nameof(CountedItemReward)}");

            _reward = model as CountedItemReward;
        }

        /// <summary>
        /// Unity callback.
        /// </summary>
        protected void OnEnable()
        {
            var presenters = GetComponentsInChildren<IPresenter<CountedItem>>();
            foreach (var presenter in presenters)
            {
                presenter.Initialize(_reward.Item);
            }

            if (_countText != null)
            {
                _countText.text = _reward.Count.ToString();
            }
        }
    }
}