﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using UnityEditor;

namespace DreamTeam.Inventory.Rewards
{
    public partial class CountedItemFixedReward : CountedItemFixedReward.IEditorApi
    {
        void IEditorApi.SetCount(int count)
        {
            Undo.RecordObject(this, "RewardSetCount");
            _count = count;
        }

        #region Inner Types

        public new interface IEditorApi : CountedItemReward.IEditorApi
        {
            void SetCount(int count);
        }

        #endregion
    }
}

#endif