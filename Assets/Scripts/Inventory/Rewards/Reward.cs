﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.TransformLookup;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Inventory.Rewards
{
    /// <summary>
    /// Represents a base class for all resource changes that player could obtain.
    /// </summary>
    public abstract partial class Reward : ScriptableObject, ISynchronizable
    {
        public abstract bool CanBeReceived { get; }
        public abstract TransformLookupId TransformId { get; }
        public event Action Receiving;
        public event Action<Reward> Received;

        public abstract void OnReceive(ReceivingType receivingType, GamePlace gamePlace);
        public abstract void OnReceiveDeclined(ReceivingType receivingType, GamePlace gamePlace);
        public abstract void Sync(State state);
        public abstract void NotifyReceivingRefusal();

        public void Receive(Instantiator instantiator, ReceivingType receivingType, GamePlace gamePlace)
        {
            instantiator.Inject(this);

            if (CanBeReceived)
            {
                Receiving?.Invoke();
                OnReceive(receivingType, gamePlace);
                Received?.Invoke(this);
            }
            else
            {
                OnReceiveDeclined(receivingType, gamePlace);
            }
        }
    }
}