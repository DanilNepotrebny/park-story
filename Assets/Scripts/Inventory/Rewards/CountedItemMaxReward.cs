﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Inventory.Rewards
{
    [CreateAssetMenu(fileName = "CountedItemMaxReward", menuName = "Inventory/Rewards/CountedItemMaxReward")]
    public class CountedItemMaxReward : CountedItemReward
    {
        public override int Count => Item.MaxCount - Item.Count;
    }
}