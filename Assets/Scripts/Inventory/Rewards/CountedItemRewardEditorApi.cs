﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using DreamTeam.Inventory.Items;
using UnityEditor;

namespace DreamTeam.Inventory.Rewards
{
    public abstract partial class CountedItemReward : CountedItemReward.IEditorApi
    {
        void IEditorApi.SetItem(CountedItem item)
        {
            Undo.RecordObject(this, "RewardSetItem");
            _item = item;
        }

        #region Inner Types

        public interface IEditorApi
        {
            void SetItem(CountedItem item);
        }

        #endregion
    }
}

#endif