﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Inventory.Rewards
{
    /// <summary>
    /// Represents booster than can be bought for money and used on levels.
    /// </summary>
    [CreateAssetMenu(fileName = "CountedItemFixedReward", menuName = "Inventory/Rewards/CountedItemFixedReward")]
    public partial class CountedItemFixedReward : CountedItemReward
    {
        [SerializeField] private int _count = 1;

        public override int Count => _count;

        public override void Sync(State state)
        {
            base.Sync(state);
            state.SyncInt("count", ref _count);
        }

        public static Reward Create(CountedItem item, int count, Instantiator instantiator)
        {
            CountedItemFixedReward reward = instantiator.Instantiate<CountedItemFixedReward>();
            reward.Item = item;
            reward._count = count;
            return reward;
        }
    }
}