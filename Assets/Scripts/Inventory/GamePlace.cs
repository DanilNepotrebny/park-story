﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Inventory
{
    public enum GamePlace
    {
        Unknown = 0,
        Ads = 1,
        Bank = 2,
        InGame = 3,
        Briefing = 4,
        Debriefing = 5,
        BoosterTip = 6,
        OutOfMoves = 7,
        PlayMatch3Button = 8,
        Location = 9,
        Cheats = 10,
        Friend = 11,
        RewardedVideo = 12,
        DayReward = 13,
        FortuneWheel = 14,
        EndlessGrind = 15,
        Letter = 16,
        LivesRestore = 17,
        RateUs = 18,
        Options = 19
    }

    public static class GamePlaceExtensions
    {
        public static string ToAnalyticsId(this GamePlace self)
        {
            switch (self)
            {
                case GamePlace.Bank:
                    return "bank";
                case GamePlace.InGame:
                    return "ingame";
                case GamePlace.Briefing:
                    return "briefing";
                case GamePlace.Debriefing:
                    return "debriefing";
                case GamePlace.BoosterTip:
                    return "booster_tip";
                case GamePlace.OutOfMoves:
                    return "out_of_moves";
                case GamePlace.PlayMatch3Button:
                    return "play_match3_button";
                case GamePlace.Location:
                    return "location";
                case GamePlace.Friend:
                    return "friend";
                case GamePlace.RewardedVideo:
                    return "rewarded_video";
                case GamePlace.DayReward:
                    return "day_reward";
                case GamePlace.FortuneWheel:
                    return "fortune_wheel";
                case GamePlace.EndlessGrind:
                    return "endless_grind";
                case GamePlace.Letter:
                    return "letter";
                case GamePlace.LivesRestore:
                    return "lives_restore";
                case GamePlace.RateUs:
                    return "rate_us";
                case GamePlace.Options:
                    return "options";
            }

            UnityEngine.Debug.LogError($"Ivalid analytics value '{self}' for {self.GetType().Name} enum.");
            return "";
        }
    }
}