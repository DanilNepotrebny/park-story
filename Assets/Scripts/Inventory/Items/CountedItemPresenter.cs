﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DreamTeam.Inventory.Items
{
    /// <summary>
    /// Simple presenter for CountedItem.
    /// </summary>
    public class CountedItemPresenter : MonoBehaviour, IPresenter<CountedItem>
    {
        [SerializeField] private CountedItem _item;

        [SerializeField, FormerlySerializedAs("_counText")]
        private Text _countText;

        [SerializeField] private Image _iconImage;

        public void Initialize([NotNull] CountedItem model)
        {
            SetItem(model);
        }

        protected void OnEnable()
        {
            SetItem(_item);
        }

        protected void OnDisable()
        {
            UnsubscribeCounterUpdate();
        }

        private void SetItem([NotNull] CountedItem model)
        {
            UnsubscribeCounterUpdate();

            _item = model;
            SubscribeCounterUpdate();
            UpdateIcon();
        }

        private void UpdateCount(int current, int previous = 0)
        {
            _countText.text = current.ToString();
        }

        private void UpdateIcon()
        {
            if (_iconImage != null)
            {
                _iconImage.sprite = _item?.Icon;
            }
        }

        private void SubscribeCounterUpdate()
        {
            if (_item != null && _countText != null)
            {
                _item.CountChanged += UpdateCount;
                UpdateCount(_item.Count);
            }
        }

        private void UnsubscribeCounterUpdate()
        {
            if (_item != null && _countText != null)
            {
                _item.CountChanged -= UpdateCount;
            }
        }
    }
}