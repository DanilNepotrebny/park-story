﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Localization;
using DreamTeam.Notifications;
using DreamTeam.PersistentCommands.CountedItems;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Inventory.Items
{
    [CreateAssetMenu(fileName = "Lives", menuName = "Inventory/Lives")]
    public partial class Lives : CountedItem, ILocalNotificationsProvider
    {
        [Header("Lives")]
        [SerializeField]
        private TimeTicks _restoreInterval = TimeSpan.FromMinutes(30);

        [SerializeField] private int _restoreCount = 1;

        [Header("Notification")]
        [SerializeField, LocalizationKey]
        private string _notificationTitleKey;

        [SerializeField, LocalizationKey] private string _notificationMessageKey;
        [SerializeField] private string _notificationSoundName;

        private NotificationsSystem _notificationsSystem;
        private LocalizationSystem _localizationSystem;

        private DateTime _restoreTime = default(DateTime);

        public TimeSpan TimeToRestore => !IsFull ? _restoreTime - _currentTime : default(TimeSpan);

        private DateTime _currentTime => DateTime.UtcNow;

        public event Action RestoreUpdated;

        [Inject]
        private void Construct(NotificationsSystem notificationSystem, LocalizationSystem localizationSystem)
        {
            _notificationsSystem = notificationSystem;
            _localizationSystem = localizationSystem;

            _notificationsSystem.RegisterLocalNotificationsProvider(this);
        }

        public override bool TrySpend(int quantity, GamePlace gamePlace)
        {
            if (base.TrySpend(quantity, gamePlace))
            {
                if (_restoreTime == default(DateTime))
                {
                    _restoreTime = _currentTime + _restoreInterval;
                }

                return true;
            }

            return false;
        }

        protected override void Sync(State state)
        {
            base.Sync(state);
            state.SyncDateTime("restoreTime", ref _restoreTime);
            UpdateRestorationInternal();
        }

        protected override ICancelable CreateSpendCommand(int quantity)
        {
            return SpendLivesCommand.Construct(Instantiator, this, quantity);
        }

        private void UpdateRestorationInternal()
        {
            if (IsFull)
            {
                return;
            }

            int restoreTimes = 0;
            while (_currentTime >= _restoreTime)
            {
                restoreTimes++;
                _restoreTime = _restoreTime.AddTicks(_restoreInterval.Ticks);
            }

            if (restoreTimes > 0)
            {
                int receiveCount = Math.Min(_restoreCount * restoreTimes, MaxCount - Count);
                TryReceive(receiveCount, ReceivingType.Earned, GamePlace.LivesRestore);
            }

            if (IsFull)
            {
                _restoreTime = default(DateTime);
            }

            RestoreUpdated?.Invoke();
        }

        IEnumerable<NotificationsSystem.LocalNotificationParams> ILocalNotificationsProvider.Provide()
        {
            List<NotificationsSystem.LocalNotificationParams> result =
                new List<NotificationsSystem.LocalNotificationParams>();

            if (Count < MaxCount)
            {
                DateTime triggerTime = DateTime.Now + TimeToRestore;
                for (int i = Count; i < MaxCount - 1; i++)
                {
                    triggerTime += _restoreInterval;
                }

                NotificationsSystem.LocalNotificationParams parameters = new NotificationsSystem.LocalNotificationParams
                {
                    TriggerTime = triggerTime,
                    Title = _localizationSystem.Localize(_notificationTitleKey, this),
                    Message = _localizationSystem.Localize(_notificationMessageKey, this),
                    SoundName = _notificationSoundName
                };

                result.Add(parameters);
            }

            return result;
        }
    }
}