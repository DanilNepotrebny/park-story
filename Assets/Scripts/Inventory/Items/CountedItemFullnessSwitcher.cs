﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Inventory.Items
{
    public class CountedItemFullnessSwitcher : MonoBehaviour, IPresenter<CountedItem>
    {
        [SerializeField] private CountedItem _countedItem;
        [SerializeField] [MinMaxRange(0, 100)] private MinMaxRange _fullnessRange;

        private bool _targetObjectDefaultState;
 
        private bool _countedItemInRange =>
            _countedItem.FillPercent.InRange((int)_fullnessRange.rangeStart, (int)_fullnessRange.rangeEnd);

        public void Initialize(CountedItem model)
        {
            UnsubscribeCountedItem();
            _countedItem = model;
            SubscribeCountedItem();
            SwitchGameObject();
        }

        protected void Awake()
        {
            Initialize(_countedItem);
        }

        protected void OnDestroy()
        {
            UnsubscribeCountedItem();
        }

        private void SubscribeCountedItem()
        {
            if (_countedItem != null)
            {
                _countedItem.CountChanged += OnCountedItemCountChanged;
            }
        }

        private void UnsubscribeCountedItem()
        {
            if (_countedItem != null)
            {
                _countedItem.CountChanged -= OnCountedItemCountChanged;
            }
        }

        private void OnCountedItemCountChanged(int current, int previous)
        {
            SwitchGameObject();
        }

        private void SwitchGameObject()
        {
            if (_countedItem != null)
            {
                gameObject.SetActive(_countedItemInRange);
            }
        }
    }
}
