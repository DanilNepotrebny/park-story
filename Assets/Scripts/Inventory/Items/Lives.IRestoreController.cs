﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;

namespace DreamTeam.Inventory.Items
{
    public partial class Lives : Lives.IRestoreController
    {
        private DateTime _pausedTime;

        DateTime IRestoreController.CurrentTime => _currentTime;

        void IRestoreController.SetSpendTime(DateTime time)
        {
            _restoreTime = time + _restoreInterval;
        }

        void IRestoreController.SetRestorationPaused(bool isPaused)
        {
            if (IsFull)
            {
                return;
            }

            if (isPaused)
            {
                _pausedTime = new DateTime(_currentTime.Ticks);
            }
            else if (_pausedTime != default(DateTime))
            {
                _restoreTime -= _currentTime - _pausedTime;
                _pausedTime = default(DateTime);
            }
        }

        void IRestoreController.UpdateRestoration()
        {
            UpdateRestorationInternal();
        }

        public interface IRestoreController
        {
            DateTime CurrentTime { get; }
            void SetSpendTime(DateTime time);
            void SetRestorationPaused(bool isPaused);
            void UpdateRestoration();
        }
    }
}