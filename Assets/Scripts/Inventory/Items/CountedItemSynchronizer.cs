﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.Nodes;
using Zenject;

namespace DreamTeam.Inventory.Items
{
    public class CountedItemSynchronizer : ISynchronizer<CountedItem>
    {
        [Inject] private List<CountedItem> _countedItems;

        public Node Serialize(CountedItem countedItem)
        {
            return new ValueNode(countedItem.name);
        }

        public CountedItem Deserialize(Node node)
        {
            string countedItemName = ((ValueNode)node).GetString();
            return _countedItems.Find(c => c.name == countedItemName);
        }
    }
}