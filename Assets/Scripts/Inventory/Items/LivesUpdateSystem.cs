﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

namespace DreamTeam.Inventory.Items
{
    public class LivesUpdateSystem : MonoBehaviour
    {
        [Inject] private Lives _lives;

        private Lives.IRestoreController _controller => _lives as Lives.IRestoreController;

        protected void OnApplicationPause(bool isPaused)
        {
            _controller.SetRestorationPaused(isPaused);
        }

        protected void Update()
        {
            _controller.UpdateRestoration();
        }

        protected void OnEnable()
        {
            _controller.SetRestorationPaused(false);
        }

        protected void OnDisable()
        {
            _controller.SetRestorationPaused(true);
        }
    }
}