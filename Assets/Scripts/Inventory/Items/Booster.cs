﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using DreamTeam.UI.DataProviders;
using UnityEngine;

namespace DreamTeam.Inventory.Items
{
    /// <summary>
    /// Represents booster than can be bought for money and used on levels.
    /// </summary>
    [CreateAssetMenu(fileName = "Booster", menuName = "Inventory/Booster")]
    public class Booster : CountedItem, IUnlockNotificationIconProvider, IDescriptionProvider
    {
        [SerializeField] [LocalizationKey] private string _description;
        [SerializeField] private Sprite _unlockNotificationIcon;

        string IDescriptionProvider.Description => _description;

        Sprite IUnlockNotificationIconProvider.UnlockNotificationIcon => _unlockNotificationIcon;
    }
}