﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Debug;
using UnityEngine;
using Zenject;

namespace DreamTeam.Inventory.Items
{
    public partial class CountedItem
    {
        [SerializeField] private int _cheatIncrement = 1;

        private CheatPanel _cheatPanel;

        private static GUISkin _cheatGUISkin;

        [Inject]
        private void Construct(CheatPanel cheatPanel)
        {
            _cheatPanel = cheatPanel;
            _cheatPanel.AddDrawer(CheatPanel.Category.CountedItems, DrawCheatGUI);
        }

        [ContextMenu(nameof(CheatRecieve))]
        private void CheatRecieve()
        {
            TryReceive(_cheatIncrement, ReceivingType.Other, GamePlace.Cheats);
        }

        [ContextMenu(nameof(CheatSpend))]
        private void CheatSpend()
        {
            TrySpend(_cheatIncrement, GamePlace.Cheats);
        }

        [ContextMenu(nameof(CheatUnlock))]
        private void CheatUnlock()
        {
            Unlock();
        }

        private void DrawCheatGUI(Rect areaRect)
        {
            GUISkin skin = GetGUISkin();

            GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));

            Texture2D texture = null;
            if (_icon != null)
            {
                texture = _icon.texture;
            }

            GUIContent labelContent = new GUIContent(name, texture);
            GUILayout.Label(labelContent, skin.label, GUILayout.ExpandWidth(true), GUILayout.MaxWidth(400));

            int count = Count;
            string countText = GUILayout.TextField(count.ToString(), skin.textField, GUILayout.ExpandWidth(true));
            if (int.TryParse(countText, out count))
            {
                Count = count;
            }

            if (GUILayout.Button($"{_cheatIncrement:+#;-#;}", skin.button, GUILayout.ExpandWidth(false)))
            {
                TryReceive(_cheatIncrement, ReceivingType.Other, GamePlace.Cheats);
            }

            if (GUILayout.Button($"{_cheatIncrement:-#;+#;}", skin.button, GUILayout.ExpandWidth(false)))
            {
                TrySpend(_cheatIncrement, GamePlace.Cheats);
            }

            string isLockedLabel = (IsLocked) ? "Locked" : "Unlocked";
            IsLocked = GUILayout.Toggle(IsLocked, isLockedLabel, skin.button, GUILayout.Width(150));

            GUILayout.EndHorizontal();
        }

        private GUISkin GetGUISkin()
        {
            if (_cheatGUISkin == null)
            {
                _cheatGUISkin = Instantiate(GUI.skin) as GUISkin;

                int baseFontSize = GUI.skin.button.fontSize;
                int fontSize = (int)(baseFontSize * 0.8f);

                _cheatGUISkin.label.fontSize = fontSize;
                _cheatGUISkin.label.fixedHeight = baseFontSize;

                _cheatGUISkin.textField.fontSize = fontSize;

                _cheatGUISkin.button.fontSize = fontSize;
            }

            return _cheatGUISkin;
        }
    }
}