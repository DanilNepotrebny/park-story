﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Analytics;
using DreamTeam.Analytics.Events.Global;
using DreamTeam.Localization;
using DreamTeam.PersistentCommands.CountedItems;
using DreamTeam.Popups;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.TransformLookup;
using DreamTeam.Utils;
using DreamTeam.Utils.FileFolders;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using Zenject;

namespace DreamTeam.Inventory.Items
{
    /// <summary>
    /// Represents simple consumable item than can be stored in player's inventory (e.g. money, boosters etc.)
    /// </summary>
    [CreateAssetMenu(fileName = "CountedItem", menuName = "Inventory/Counted item")]
    public partial class CountedItem : ScriptableObject, ISynchronizable
    {
        [SerializeField, RequiredField] private string _id;
        [SerializeField] private int _startCount;
        [SerializeField] private int _maxCount = int.MaxValue;
        [SerializeField] private Sprite _icon;
        [SerializeField, FormerlySerializedAs("_NotEnoughPopupScene")] private SceneReference _notEnoughPopupScene;
        [SerializeField] private SceneReference _maxCountPopupScene;
        [SerializeField, HideInInspector] private ProductPack _defaultProductPack;
        [SerializeField] private TransformLookupId _transformId;
        [SerializeField] private bool _isLockedInitially;
        [SerializeField, LocalizationKey] private string _name;
        [SerializeField] private bool _isCurrency;
        [SerializeField] private bool _canBeStoredAsMessage;

        private AnalyticsSystem _analyticsSystem;

        private PopupSystem _popupSystem;
        private SaveGroup _saveGroup;
        private int _count;
        private bool _isLocked;

        public Sprite Icon => _icon;

        public ProductPack DefaultProductPack
        {
            get { return _defaultProductPack; }
            protected set { _defaultProductPack = value; }
        }

        public int MaxCount => _maxCount;
        public int FillPercent => Count * 100 / MaxCount;
        public bool IsFull => FillPercent == 100;
        public TransformLookupId TransformId => _transformId;
        public string Name => _name;

        public string Id => _id;
        public bool IsCurrency => _isCurrency;

        public int Count
        {
            get { return _count; }
            private set
            {
                if (value < 0)
                {
                    throw new ArgumentException($"{GetType().Name} count can't be negative.");
                }

                if (_count != value)
                {
                    Assert.IsTrue(
                            value <= _maxCount,
                            $"{GetType().Name} tries set count to {value}, but it can't be larger than {_maxCount}."
                        );

                    int previous = _count;
                    _count = Mathf.Min(value, _maxCount);
                    CountChanged?.Invoke(_count, previous);
                }
            }
        }

        public bool IsLocked
        {
            get { return _isLocked; }
            private set
            {
                if (_isLocked != value)
                {
                    _isLocked = value;

                    if (!_isLocked)
                    {
                        Unlocked?.Invoke(this);
                    }
                }
            }
        }

        protected Instantiator Instantiator { get; private set; }

        public event Action<CountedItem> Unlocked;
        public event CountChangedDelegate CountChanged;

        [Inject]
        private void Construct(
                PopupSystem popupSystem,
                SaveGroup saveGroup,
                AnalyticsSystem analyticsSystem,
                Instantiator instantiator
            )
        {
            _analyticsSystem = analyticsSystem;
            Instantiator = instantiator;

            _popupSystem = popupSystem;

            _saveGroup = saveGroup;
            _saveGroup.RegisterSynchronizable(Id, this);
        }

        public bool IsSpendingPossible(int quantity)
        {
            Assert.IsTrue(quantity > 0);
            return Count >= quantity;
        }

        public bool IsReceivingPossible(int quantity)
        {
            Assert.IsTrue(quantity > 0);
            return Count + quantity <= _maxCount;
        }

        public bool CanBeStoredAsMessage()
        {
            return _canBeStoredAsMessage;
        }

        public virtual bool TrySpend(int quantity, GamePlace gamePlace)
        {
            Assert.IsTrue(quantity > 0);
            Assert.IsTrue(gamePlace != GamePlace.Unknown);
            Assert.IsFalse(IsLocked, $"{name} is locked while trying to spend it. This may lead to some bugs");

            if (!IsSpendingPossible(quantity))
            {
                ShowNotEnoughPopup(gamePlace);
                return false;
            }

            Count = Count - quantity;

            _analyticsSystem.Send(Instantiator.Instantiate<CountedItemSpentEvent>(this, quantity));
            return true;
        }

        public bool TryScheduleSpending(int quantity, GamePlace gamePlace, out ICancelable cancelHandle)
        {
            Assert.IsTrue(quantity > 0);
            Assert.IsTrue(gamePlace != GamePlace.Unknown);

            if (!IsSpendingPossible(quantity))
            {
                ShowNotEnoughPopup(gamePlace);
                cancelHandle = null;
                return false;
            }

            cancelHandle = CreateSpendCommand(quantity);
            return true;
        }

        public virtual bool TryReceive(int quantity, ReceivingType receivingType, GamePlace gamePlace)
        {
            Assert.IsTrue(quantity > 0);
            Assert.IsFalse(IsLocked, $"{name} is locked while trying to receive it. This may lead to some bugs");

            if (IsReceivingPossible(quantity))
            {
                Count += quantity;
                SendAnalytics(quantity, receivingType, gamePlace);
                return true;
            }

            return false;
        }

        public void ShowNotEnoughPopup(GamePlace gamePlace)
        {
            Assert.IsTrue(gamePlace != GamePlace.Unknown);
            ProductPackPurchasePopup.Args args = new ProductPackPurchasePopup.Args
            {
                ProductPack = DefaultProductPack,
                GamePlace = gamePlace
            };
            _popupSystem.Show(_notEnoughPopupScene.Name, args);
        }

        public void ShowMaxCountPopup()
        {
            if (_maxCountPopupScene.HasValue)
            {
                _popupSystem.Show(_maxCountPopupScene.Name);
            }
        }

        public void Unlock()
        {
            IsLocked = false;
        }

        public int LoadCountFromSave(IFileFolder save)
        {
            State state = _saveGroup.CreateDeserializationState(save, this);

            int count = 0;
            SyncCount(state, ref count);

            return count;
        }

        protected void OnEnable()
        {
            Count = _startCount;
            _isLocked = _isLockedInitially;
        }

        protected virtual void Sync(State state)
        {
            int syncedCount = Count;
            SyncCount(state, ref syncedCount);
            Count = syncedCount;

            state.SyncBool("isLocked", ref _isLocked, _isLockedInitially);
        }

        protected virtual ICancelable CreateSpendCommand(int quantity)
        {
            return SpendCountedItemCommand.Construct(Instantiator, this, quantity);
        }

        private void SendAnalytics(int quantity, ReceivingType receivingType, GamePlace gamePlace)
        {
            if (receivingType != ReceivingType.Other)
            {
                _analyticsSystem.Send(
                        Instantiator.Instantiate<CountedItemReceivedEvent>(this, quantity, gamePlace, receivingType)
                    );
            }
        }

        private void SyncCount(State state, ref int count)
        {
            state.SyncInt("count", ref count);
        }

        void ISynchronizable.Sync(State state)
        {
            Sync(state);
        }

        #region

        public delegate void CountChangedDelegate(int current, int previous);

        #endregion
    }
}