﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using UnityEditor;

namespace DreamTeam.Inventory.Items
{
    public partial class CountedItem : CountedItem.IEditorApi
    {
        void IEditorApi.SetId(string id)
        {
            EditorUtility.SetDirty(this);
            _id = id;
        }

        void IEditorApi.SetDefaultProductPack(ProductPack pack)
        {
            DefaultProductPack = pack;
        }

        #region IEditorApi

        public interface IEditorApi
        {
            void SetId(string id);

            void SetDefaultProductPack(ProductPack pack);
        }

        #endregion
    }
}

#endif