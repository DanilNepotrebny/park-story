﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.Inventory.Items
{
    /// <summary>
    /// Collection of boosters grouped by usage type
    /// </summary>
    [CreateAssetMenu(fileName = "BoosterGroup", menuName = "Inventory/BoosterGroup")]
    public class BoosterGroup : ScriptableObject, IEnumerable<Booster>
    {
        [SerializeField] private List<Booster> _boosters;

        public bool Contains(Booster booster)
        {
            return _boosters.Contains(booster);
        }

        public IEnumerator<Booster> GetEnumerator()
        {
            return _boosters.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}