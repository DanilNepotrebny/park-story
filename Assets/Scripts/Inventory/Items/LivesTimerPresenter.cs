﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.UI;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Inventory.Items
{
    [RequireComponent(typeof(Text))]
    public class LivesTimerPresenter : MonoBehaviour, IPresenter<Lives>
    {
        [SerializeField] private Lives _lives;
        [SerializeField] [Multiline(3)] private string _textFormat = "{0}";
        [SerializeField] private string _timeFormat = @"mm\:ss";

        private readonly TimeSpan _timeCeilShift = new TimeSpan(0, 0, 0, 0, 999);
        private Text _text;

        public void Initialize(Lives model)
        {
            UnsubscribeLives();

            _lives = model;

            SubscribeLives();
            UpdateTimer();
        }

        protected void Awake()
        {
            _text = GetComponent<Text>();
        }

        protected void OnEnable()
        {
            Initialize(_lives);
        }

        protected void OnDisable()
        {
            UnsubscribeLives();
        }

        private void SubscribeLives()
        {
            if (_lives != null)
            {
                _lives.RestoreUpdated += UpdateTimer;
            }
        }

        private void UnsubscribeLives()
        {
            if (_lives != null)
            {
                _lives.RestoreUpdated -= UpdateTimer;
            }
        }

        private void UpdateTimer()
        {
            TimeSpan displayTime = _lives.TimeToRestore.Add(_timeCeilShift);
            _text.text = string.Format(_textFormat, displayTime.ToString(_timeFormat));
        }
    }
}