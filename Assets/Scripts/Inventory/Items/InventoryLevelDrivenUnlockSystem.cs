// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam.Inventory.Items
{
    public class InventoryLevelDrivenUnlockSystem : MonoBehaviour
    {
        [SerializeField, NonUniqueKeys] private LevelItemPairs _levelItemPairs = new LevelItemPairs();

        #if !NO_MATCH3

        [Inject(Id = GlobalInstaller.MainLevelPackControllerId)]
        private SingleLevelPackController _mainLevelPackController;

        #endif

        public int GetUnlockLevelNumber(CountedItem countedItem)
        {
            int result = int.MaxValue;

            foreach (KeyValuePair<int, CountedItem> pair in _levelItemPairs)
            {
                if (pair.Value == countedItem)
                {
                    result = Mathf.Min(result, pair.Key);
                }
            }

            return result;
        }

        #if !NO_MATCH3

        protected void Awake()
        {
            _mainLevelPackController.CurrentLevelAdvanced += OnCurrentLevelAdvanced;
        }

        protected void OnDestroy()
        {
            _mainLevelPackController.CurrentLevelAdvanced -= OnCurrentLevelAdvanced;
        }

        [ContextMenu(nameof(SortByLevel))]
        private void SortByLevel()
        {
            _levelItemPairs.Sort((A, B) => A.Key.CompareTo(B.Key));
        }

        [ContextMenu(nameof(SortByProductPack))]
        private void SortByProductPack()
        {
            _levelItemPairs.Sort((A, B) => string.Compare(A.Value.name, B.Value.name, StringComparison.Ordinal));
        }

        private void OnCurrentLevelAdvanced(int previousIndex, int currentIndex)
        {
            int previousLevelNumber = currentIndex;
            _levelItemPairs.FindAll(pair => pair.Key <= previousLevelNumber).ForEach(pair => pair.Value.Unlock());
        }

        #endif

        [Serializable, NonUniqueKeys]
        private class LevelItemPairs : KeyValueList<int, CountedItem>
        {
        }
    }
}