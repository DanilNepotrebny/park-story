﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.Inventory.Items.UnlockNotification
{
    public partial class UnlockNotificationSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private List<UnlockNotificationType> _notificationTypes;

        [Inject] private SaveGroup _saveGroup;
        [Inject] private Instantiator _instantiator;

        private Dictionary<UnlockNotificationType, Controller> _controllers =
            new Dictionary<UnlockNotificationType, Controller>();

        public event NotificationPresentingDelegate NotificationPresenting;

        public void HandleNotificationPresenting(
            UnlockNotificationType notificationType,
            CountedItem item,
            IDeferredInvocationHandle presentationEndHandle)
        {
            NotificationPresenting?.Invoke(notificationType, item, presentationEndHandle);
        }

        public void HandleNotificationPresented(UnlockNotificationType notificationType, CountedItem item)
        {
            if (_controllers.ContainsKey(notificationType))
            {
                _controllers[notificationType].OnNotificationPresented(item);
            }
            else
            {
                LogInvalidNotificationTypeError(notificationType);
            }
        }

        public void AddNotificationCallback(
            UnlockNotificationType notificationType,
            Action<CountedItem> notificationCallback)
        {
            if (_controllers.ContainsKey(notificationType))
            {
                _controllers[notificationType].AddCallback(notificationCallback);
            }
            else
            {
                LogInvalidNotificationTypeError(notificationType);
            }
        }

        public void RemoveNotificationCallback(
            UnlockNotificationType notificationType,
            Action<CountedItem> notificationCallback)
        {
            if (_controllers.ContainsKey(notificationType))
            {
                _controllers[notificationType].RemoveCallback(notificationCallback);
            }
            else
            {
                LogInvalidNotificationTypeError(notificationType);
            }
        }

        public bool HasNotification(UnlockNotificationType notificationType, CountedItem countedItem)
        {
            if (_controllers.ContainsKey(notificationType))
            {
                return _controllers[notificationType].HasNotification(countedItem);
            }

            return false;
        }

        public IEnumerable<CountedItem> GetPendingNotifications(UnlockNotificationType notificationType)
        {
            if (_controllers.ContainsKey(notificationType))
            {
                return _controllers[notificationType].GetPendingNotifications();
            }

            return Array.Empty<CountedItem>();
        }

        public void Init()
        {
            _saveGroup.RegisterSynchronizable(nameof(UnlockNotificationSystem), this);

            foreach (UnlockNotificationType type in _notificationTypes)
            {
                _controllers.Add(type, _instantiator.Instantiate<Controller>(type));
            }
        }

        private static void LogInvalidNotificationTypeError(UnlockNotificationType notificationType)
        {
            UnityEngine.Debug.LogError(
                    $"Notification type {notificationType.name} isn't added to {nameof(UnlockNotificationSystem)}."
                );
        }

        void ISynchronizable.Sync(State state)
        {
            foreach (KeyValuePair<UnlockNotificationType, Controller> pair in _controllers)
            {
                state.SyncObject(pair.Key.name, pair.Value);
            }
        }

        #region Inner types

        public delegate void NotificationPresentingDelegate(
            UnlockNotificationType type,
            CountedItem item,
            IDeferredInvocationHandle presentationEndHandle);

        #endregion
    }
}