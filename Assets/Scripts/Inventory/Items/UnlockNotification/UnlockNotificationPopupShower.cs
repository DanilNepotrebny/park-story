﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Popups;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Inventory.Items.UnlockNotification
{
    public class UnlockNotificationPopupShower : MonoBehaviour
    {
        [SerializeField] private UnlockNotificationType _notificationType;
        [SerializeField] private SceneReference _unlockNotificationPopup;

        [Inject] private PopupQueueSystem _popupQueueSystem;
        [Inject] private UnlockNotificationSystem _notificationSystem;

        protected void Awake()
        {
            _notificationSystem.AddNotificationCallback(_notificationType, ShowNotification);
        }

        protected void Start()
        {
            IEnumerable<CountedItem> items = _notificationSystem.GetPendingNotifications(_notificationType);
            foreach (CountedItem item in items)
            {
                ShowNotification(item);
            }
        }

        protected void OnDestroy()
        {
            _notificationSystem.RemoveNotificationCallback(_notificationType, ShowNotification);
        }

        protected void ShowNotification(CountedItem item)
        {
            CountedItemPopup.Args args = new CountedItemPopup.Args
            {
                CountedItem = item
            };
            _popupQueueSystem.Enqueue(
                    _unlockNotificationPopup.Name,
                    args,
                    () => _notificationSystem.HandleNotificationPresented(_notificationType, item)
                );
        }
    }
}