﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI;
using UnityEngine;
using Zenject;

namespace DreamTeam.Inventory.Items.UnlockNotification
{
    public class UnlockNotificationPresenter : MonoBehaviour, IPresenter<CountedItem>
    {
        [SerializeField] private UnlockNotificationType _notificationType;
        [SerializeField] private GameObject _notificationObject;

        [Inject] private UnlockNotificationSystem _notificationSystem;

        private CountedItem _countedItem;

        public void Initialize(CountedItem countedItem)
        {
            _countedItem = countedItem;
        }

        protected void Start()
        {
            _notificationObject.SetActive(false);

            if (_notificationSystem.HasNotification(_notificationType, _countedItem))
            {
                ShowNotification();
            }
            else
            {
                _notificationSystem.AddNotificationCallback(_notificationType, OnItemUnlocked);
            }
        }

        protected void OnDestroy()
        {
            _notificationSystem.RemoveNotificationCallback(_notificationType, OnItemUnlocked);
        }

        protected void OnItemUnlocked(CountedItem item)
        {
            if (_countedItem != item)
            {
                return;
            }

            ShowNotification();
        }

        private void ShowNotification()
        {
            _notificationObject.SetActive(true);
            _notificationSystem.HandleNotificationPresented(_notificationType, _countedItem);
        }
    }
}