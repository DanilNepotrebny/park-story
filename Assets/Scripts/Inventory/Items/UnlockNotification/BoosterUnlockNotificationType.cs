﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Inventory.Items.UnlockNotification
{
    [CreateAssetMenu(menuName = "Inventory/Unlock Notifications/Booster Unlock Notification Type")]
    public class BoosterUnlockNotificationType : UnlockNotificationType
    {
        [SerializeField] private BoosterGroup _boosterGroup;

        public override bool IsSupported(CountedItem item)
        {
            return base.IsSupported(item) && _boosterGroup.Contains((Booster)item);
        }
    }
}
