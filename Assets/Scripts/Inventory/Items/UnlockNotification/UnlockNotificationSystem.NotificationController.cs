﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using Zenject;

namespace DreamTeam.Inventory.Items.UnlockNotification
{
    public partial class UnlockNotificationSystem
    {
        private class Controller : ISynchronizable
        {
            private List<CountedItem> _countedItems;
            private ISynchronizer<CountedItem> _countedItemSynchronizer;

            private readonly HashSet<CountedItem> _pendingNotifications = new HashSet<CountedItem>();
            private Action<CountedItem> _notificationCallbacks;
            private UnlockNotificationType _notificationType;

            [Inject]
            private void Construct(List<CountedItem> countedItems, ISynchronizer<CountedItem> countedItemSynchronizer)
            {
                _countedItems = countedItems;
                _countedItemSynchronizer = countedItemSynchronizer;

                foreach (CountedItem item in _countedItems)
                {
                    if (_notificationType.IsSupported(item))
                    {
                        item.Unlocked += OnItemUnlocked;
                    }
                }
            }

            public IEnumerable<CountedItem> GetPendingNotifications()
            {
                return new List<CountedItem>(_pendingNotifications);
            }

            public Controller(UnlockNotificationType notificationType)
            {
                _notificationType = notificationType;
            }

            public void OnNotificationPresented(CountedItem item)
            {
                _pendingNotifications.Remove(item);
            }

            public void AddCallback(Action<CountedItem> notificationCallback)
            {
                _notificationCallbacks += notificationCallback;
            }

            public void RemoveCallback(Action<CountedItem> notificationCallback)
            {
                _notificationCallbacks -= notificationCallback;
            }

            public bool HasNotification(CountedItem countedItem)
            {
                return _pendingNotifications.Contains(countedItem);
            }

            void ISynchronizable.Sync(State state)
            {
                state.SyncObjectCollection("PendingNotifications", _pendingNotifications, _countedItemSynchronizer);
            }

            private void OnItemUnlocked(CountedItem item)
            {
                _pendingNotifications.Add(item);
                _notificationCallbacks?.Invoke(item);
            }
        }
    }
}