﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Inventory.Items.UnlockNotification
{
    [CreateAssetMenu(menuName = "Inventory/Unlock Notifications/Counted Item Unlock Notification Type")]
    public class UnlockNotificationType : ScriptableObject
    {
        [SerializeField] private CountedItemTypeReference _typeReference;

        public virtual bool IsSupported(CountedItem item)
        {
            return _typeReference.Type.IsInstanceOfType(item);
        }

        #region Inner types

        [Serializable]
        private class CountedItemTypeReference : TypeReference<CountedItem>
        {
        }

        #endregion
    }
}