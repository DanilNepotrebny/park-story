﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;

namespace DreamTeam.Inventory.Items.UnlockNotification
{
    public partial class UnlockNotificationSystem : UnlockNotificationSystem.IEditorApi
    {
        void IEditorApi.RegisterNotificationType(
                UnlockNotificationType notificationType
            )
        {
            if (notificationType != null && !_notificationTypes.Contains(notificationType))
            {
                EditorUtils.RecordObjectUndo(this, "RegisterNotificationType");
                _notificationTypes.Add(notificationType);
            }
        }

        void IEditorApi.ClearDeletedNotificationTypes()
        {
            EditorUtils.RecordObjectUndo(this, "ClearDeletedNotificationTypes");
            _notificationTypes.RemoveAll(item => item == null);
        }

        #region

        public interface IEditorApi
        {
            void RegisterNotificationType(
                    UnlockNotificationType notificationType
                );

            void ClearDeletedNotificationTypes();
        }

        #endregion
    }
}