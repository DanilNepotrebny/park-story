﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.TransformLookup
{
    public class TransformLookupRegistrator : MonoBehaviour
    {
        [SerializeField, RequiredField] private TransformLookupId _id;
        [SerializeField] private Transform _target;

        [Inject]
        private TransformLookupSystem _tansformLookupSystem;

        public TransformLookupId Id => _id;

        public Transform TargetTransform => _target != null ? _target : transform;

        protected void OnEnable()
        {
            _tansformLookupSystem.Register(this);
        }

        protected void OnDisable()
        {
            _tansformLookupSystem.Unregister(this);
        }
    }
}