﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.TransformLookup
{
    [CreateAssetMenu]
    public class TransformLookupId : ScriptableObject
    {
    }
}