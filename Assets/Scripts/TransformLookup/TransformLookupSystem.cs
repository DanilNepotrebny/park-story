﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using UnityEngine;

namespace DreamTeam.TransformLookup
{
    /// <summary>
    /// Allows to find <see cref="Transform"/> across scenes using <see cref="TransformLookupId"/>
    /// </summary>
    public class TransformLookupSystem : MonoBehaviour
    {
        private Dictionary<TransformLookupId, Transform> _register = new Dictionary<TransformLookupId, Transform>();

        public void Register(TransformLookupRegistrator registrator)
        {
            CheckRegistratorId(registrator);

            _register.Add(registrator.Id, registrator.TargetTransform);
        }

        public void Unregister(TransformLookupRegistrator registrator)
        {
            CheckRegistratorId(registrator);

            _register.Remove(registrator.Id);
        }

        public Transform FindTransform(TransformLookupId id)
        {
            Transform result = null;
            _register.TryGetValue(id, out result);
            return result;
        }

        private void CheckRegistratorId(TransformLookupRegistrator registrator)
        {
            if (registrator.Id == null)
            {
                throw new ArgumentNullException(
                        nameof(registrator.Id),
                        $"{GetType().Name}: Please assing {nameof(registrator.Id)} on {registrator.name}."
                    );
            }
        }
    }
}