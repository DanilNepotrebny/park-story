﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups;
using DreamTeam.Popups.Cloud;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Cloud
{
    public class ConflictPresenter : MonoBehaviour
    {
        [SerializeField] private SceneReference _scene;

        [Inject] private CloudSaveManager _cloudSaveManager;
        [Inject] private PopupQueueSystem _popupQueueSystem;

        public void StartResolve([NotNull] ResolveDelegate resolveDelegate)
        {
            _popupQueueSystem.Enqueue(_scene.Name, new SynchronizationPopup.Args { ResolveDelegate = resolveDelegate });
        }

        protected void Awake()
        {
            _cloudSaveManager.SetConflictPresenter(this);
        }

        protected void OnDestroy()
        {
            _cloudSaveManager.SetConflictPresenter(null);
        }

        #region Inner types

        public delegate void ResolveDelegate(bool useTheirs);

        #endregion
    }
}