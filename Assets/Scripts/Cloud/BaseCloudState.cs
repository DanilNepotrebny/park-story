﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using PlayFab;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace DreamTeam.Cloud
{
    public abstract class BaseCloudState : ISynchronizable
    {
        [Inject] protected CloudSaveManager Manager { get; }

        private DateTime _nextTryTime;
        private float _lastTimeout;
        private bool _hasError;

        public bool HasError => _hasError && DateTime.Now < _nextTryTime;
        public virtual bool IsPending => false;

        public virtual void OnInitialize()
        {
        }

        public virtual void OnActivated()
        {
        }

        public virtual void OnDeactivated()
        {
        }

        public virtual void OnUpdated()
        {
        }

        public virtual void OnApplicationPause(bool isPaused)
        {
        }

        protected virtual void Sync(State state)
        {
        }

        protected void WaitAfterError(PlayFabErrorCode code)
        {
            WaitAfterError(Manager.GetApiErrorTimeout(code));
        }

        protected void WaitAfterError(TimeSpan timeout)
        {
            float seconds = (float)timeout.TotalSeconds;

            if (_hasError)
            {
                seconds = Mathf.Max(seconds, _lastTimeout * Manager.ErrorRetryFactor);
            }

            _lastTimeout = seconds;

            seconds += Random.Range(0f, Manager.ErrorRandomFactor) * seconds;

            _nextTryTime = DateTime.Now.AddSeconds(seconds);
            _hasError = true;
        }

        protected void ResetError()
        {
            _hasError = false;
        }

        void ISynchronizable.Sync(State state)
        {
            Sync(state);
        }
    }
}