﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Plugins.KeyChainStore;
using DreamTeam.Utils;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.SharedModels;
using UnityEngine;
using Zenject;

namespace DreamTeam.Cloud.DeviceId
{
    public class DeviceIdManager : MonoBehaviour
    {
        [SerializeField] private KeyChain _sharedKeyChain;
        [SerializeField] private string _key;

        private IImpl _impl;

        public string Id => _impl.Id;

        [Inject]
        private void Construct(Instantiator instantiator)
        {
            #if !UNITY_EDITOR && UNITY_IOS
            {
                _impl = instantiator.Instantiate<IosDeviceIdImpl>();
            }
            #else
            {
                _impl = instantiator.Instantiate<CustomDeviceIdImpl>();
            }
            #endif
        }

        public void LoginWithDeviceId(Action<LoginResult> resultCallback, Action<PlayFabError> errorCallback, bool createAccount)
        {
            _impl.Login(resultCallback, errorCallback, createAccount);
        }

        public void LinkDeviceId(Action<PlayFabResultCommon> resultCallback, Action<PlayFabError> errorCallback, bool forceLink)
        {
            _impl.Link(resultCallback, errorCallback, forceLink);
        }

        public string RequestStoredId(string nonpersistentId)
        {
            string id = _sharedKeyChain.GetString(_key);
            if (id != null)
            {
                return id;
            }

            _sharedKeyChain.SetString(_key, nonpersistentId);
            return nonpersistentId;
        }

        #region Inner types

        public interface IImpl
        {
            string Id { get; }

            void Login(
                    Action<LoginResult> resultCallback,
                    Action<PlayFabError> errorCallback,
                    bool createAccount
                );

            void Link(
                    Action<PlayFabResultCommon> resultCallback,
                    Action<PlayFabError> errorCallback,
                    bool forceLink
                );
        }

        #endregion
    }
}