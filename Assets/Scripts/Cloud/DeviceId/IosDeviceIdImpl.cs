﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_IOS

using System;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.SharedModels;

namespace DreamTeam.Cloud.DeviceId
{
    public class IosDeviceIdImpl : StoredDeviceIdImpl
    {
        protected override string NonpersistentId => UnityEngine.iOS.Device.vendorIdentifier;

        public override void Login(
                Action<LoginResult> resultCallback,
                Action<PlayFabError> errorCallback,
                bool createAccount
            )
        {
            var request = new LoginWithIOSDeviceIDRequest
            {
                CreateAccount = createAccount,
                DeviceId = Id

            };

            PlayFabClientAPI.LoginWithIOSDeviceID(request, resultCallback, errorCallback);
        }

        public override void Link(
                Action<PlayFabResultCommon> resultCallback,
                Action<PlayFabError> errorCallback,
                bool forceLink
            )
        {
            var request = new LinkIOSDeviceIDRequest
            {
                DeviceId = Id,
                ForceLink = forceLink
            };

            PlayFabClientAPI.LinkIOSDeviceID(request, resultCallback, errorCallback);
        }
    }
}

#endif