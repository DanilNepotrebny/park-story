﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.SharedModels;
using UnityEngine;

namespace DreamTeam.Cloud.DeviceId
{
    public class CustomDeviceIdImpl : DeviceIdManager.IImpl
    {
        public string Id => SystemInfo.deviceUniqueIdentifier;

        public void Login(
                Action<LoginResult> resultCallback,
                Action<PlayFabError> errorCallback,
                bool createAccount
            )
        {
            var request = new LoginWithCustomIDRequest
            {
                CreateAccount = createAccount,
                CustomId = Id
            };

            PlayFabClientAPI.LoginWithCustomID(request, resultCallback, errorCallback);
        }

        public void Link(
                Action<PlayFabResultCommon> resultCallback,
                Action<PlayFabError> errorCallback,
                bool forceLink
            )
        {
            var request = new LinkCustomIDRequest
            {
                CustomId = Id,
                ForceLink = forceLink
            };

            PlayFabClientAPI.LinkCustomID(request, resultCallback, errorCallback);
        }
    }
}