﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.SharedModels;
using Zenject;

namespace DreamTeam.Cloud.DeviceId
{
    public abstract class StoredDeviceIdImpl : DeviceIdManager.IImpl
    {
        public string Id { get; private set; }

        protected abstract string NonpersistentId { get; }

        [Inject]
        private void Construct(DeviceIdManager manager)
        {
            Id = manager.RequestStoredId(NonpersistentId);
        }

        public abstract void Login(
                Action<LoginResult> resultCallback,
                Action<PlayFabError> errorCallback,
                bool createAccount
            );

        public abstract void Link(
                Action<PlayFabResultCommon> resultCallback,
                Action<PlayFabError> errorCallback,
                bool forceLink
            );
    }
}