﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using PlayFab;
using PlayFab.ClientModels;

namespace DreamTeam.Cloud
{
    public abstract class BaseLoginCloudState : BaseCloudState
    {
        private bool _isAccountCreationEnabled;
        private bool _isAccountNotFound;

        public override bool IsPending => (_isAccountCreationEnabled || !_isAccountNotFound) && !Manager.IsLoggedIn;

        protected abstract string DebugInfo { get; }

        public event Action<LoginResult> LoggedIn;
        public event Action NoAccountDiscovered;

        public override void OnActivated()
        {
            base.OnActivated();

            Login();
        }

        public void EnableAccountCreation()
        {
            _isAccountCreationEnabled = true;
        }

        protected abstract void Login(
                bool isAccountCreationEnabled,
                Action<LoginResult> resultCallback,
                Action<PlayFabError> errorCallback
            );

        private void Login()
        {
            UnityEngine.Debug.Log("Logging in via " + DebugInfo);

            Login(_isAccountCreationEnabled, OnLogin, OnLoginError);
        }

        private void OnLoginError(PlayFabError error)
        {
            switch (error.Error)
            {
                case PlayFabErrorCode.AccountNotFound:
                case PlayFabErrorCode.AccountDeleted:
                    UnityEngine.Debug.Log($"{error.ErrorMessage} ({DebugInfo})");
                    _isAccountNotFound = true;
                    NoAccountDiscovered?.Invoke();
                    break;

                default:
                    UnityEngine.Debug.LogError($"Error while logging via {DebugInfo}: {error.ErrorMessage}");
                    WaitAfterError(error.Error);
                    break;
            }

            Manager.SetIdleState();
        }

        private void OnLogin(LoginResult result)
        {
            UnityEngine.Debug.Log($"Logged in via {DebugInfo} Id: {result.PlayFabId}");

            ResetError();

            LoggedIn?.Invoke(result);
            
            Manager.SetIdleState();
        }
    }
}