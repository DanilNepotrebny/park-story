﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items;
using DreamTeam.Match3.Levels;
using DreamTeam.Utils.FileFolders;
using Zenject;

namespace DreamTeam.Cloud
{
    public class ServerSavePresenterInstaller : MonoInstaller, ICurrentLevelInfo, ICountedItemQuantityProvider
    {
        [Inject(Id = GlobalInstaller.MainLevelPackControllerId)]
        private SingleLevelPackController _mainLevelPackController;

        [Inject] private CloudSaveManager _cloudSaveManager;

        public LevelSettings CurrentLevel
        {
            get
            {
                IFileFolder saveFolder = _cloudSaveManager.ServerSave;

                return _mainLevelPackController.LoadCurrentLevelFromSave(saveFolder);
            }
        }

        public int CurrentLevelIndex => _mainLevelPackController.FindLevelIndex(CurrentLevel);
        public LevelPack CurrentLevelPack => _mainLevelPackController.FindLevelPack(CurrentLevel);
        public string CurrentLevelId => _mainLevelPackController.CurrentLevelId;

        public override void InstallBindings()
        {
            Container.Bind<ICurrentLevelInfo>().FromInstance(this);
            Container.Bind<ICountedItemQuantityProvider>().FromInstance(this);
        }

        public ICountedItemQuantity GetQuantity(CountedItem item)
        {
            IFileFolder saveFolder = _cloudSaveManager.ServerSave;

            int count = item.LoadCountFromSave(saveFolder);
            return new CountedItemQuantity(item, count);
        }

        #region Inner types

        private class CountedItemQuantity : ICountedItemQuantity
        {
            public int Count { get; }
            public CountedItem Item { get; }

            public CountedItemQuantity(CountedItem item, int count)
            {
                Item = item;
                Count = count;
            }
        }

        #endregion
    }
}