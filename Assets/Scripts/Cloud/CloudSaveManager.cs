﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using DreamTeam.Cloud.States;
using DreamTeam.Debug;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using DreamTeam.Utils.FileFolders;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using Zenject;
using EntityKey = PlayFab.DataModels.EntityKey;

namespace DreamTeam.Cloud
{
    public class CloudSaveManager : MonoBehaviour, ISynchronizable, IInitable
    {
        [SerializeField] private TimeTicks _saveInterval;
        [SerializeField] private TimeTicks _loadInterval;
        [SerializeField] private TimeTicks _getErrorTimeout;
        [SerializeField] private TimeTicks _putErrorTimeout;
        [SerializeField] private ErrorTimeoutMap _apiErrorTimeouts;
        [SerializeField] private TimeTicks _defaultApiErrorTimeout;
        [SerializeField] private float _errorRetryFactor;
        [SerializeField] private float _errorRandomFactor;

        [Inject] private SaveManager _saveManager;
        [Inject] private DebugStatesSystem _debugStatesSystem;
        [Inject] private Instantiator _instantiator;

        private BaseCloudState[] _states;
        private BaseCloudState _current;
        private int _profileVersion;
        private static int _serverProfileVersion;
        private bool _isDeviceLinked;
        private string _linkedFacebookId = string.Empty;
        private static EntityKey _entity = new EntityKey();
        private static string _playFabId;
        private TimeSpan? _customErrorTimeout;
        private AccountState _accountState;

        public int ServerProfileVersion
        {
            get { return _serverProfileVersion; }
            set { _serverProfileVersion = value; }
        }

        public int ProfileVersion
        {
            get { return _profileVersion; }
            set { _profileVersion = value; }
        }

        public bool IsDeviceLinked
        {
            get { return _isDeviceLinked; }
            set { _isDeviceLinked = value; }
        }

        public IFileFolder ServerSave { get; private set; }

        public string LinkedFacebookId
        {
            get { return _linkedFacebookId; }
            set { _linkedFacebookId = value; }
        }

        public AccountState AccountState
        {
            get { return _accountState; }

            private set
            {
                if (_accountState != value)
                {
                    _accountState = value;
                    AccountStateChanged?.Invoke();
                }
            }
        }

        public bool IsLoggedIn => PlayFabClientAPI.IsClientLoggedIn();
        public bool IsUpToDate => !GetState<LoadCloudState>().IsOutdated;

        public SaveManager.Group SaveGroupId => SaveManager.Group.Settings;
        public bool HasLocalSaves => _saveManager.HasFiles(new[] { SaveGroupId });

        public TimeSpan SaveInterval => _saveInterval;
        public TimeSpan LoadInterval => _loadInterval;
        public TimeSpan GetErrorTimeout => SelectErrorTimeout(_getErrorTimeout);
        public TimeSpan PutErrorTimeout => SelectErrorTimeout(_putErrorTimeout);
        public float ErrorRetryFactor => _errorRetryFactor;
        public float ErrorRandomFactor => _errorRandomFactor;

        public EntityKey Entity => _entity;

        public IEnumerable<BaseCloudState> States => _states;

        private SaveGroup _saveGroup => _saveManager.GetSaveGroup(SaveGroupId);

        public event Action AccountStateChanged;

        [Obfuscation(Exclude = false, Feature = "-rename")]
        public event Action SavesUpdated
        {
            add { GetState<LoadCloudState>().SavesUpdated += value; }
            remove { GetState<LoadCloudState>().SavesUpdated -= value; }
        }

        /// <summary>
        /// Use this to send save to the cloud as soon as posible.
        /// </summary>
        public void SendSave()
        {
            GetState<SaveCloudState>().RemoveTimeout();
        }

        public void SaveSettings()
        {
            _saveManager.Save(_saveGroup);
        }

        public void SetConflictPresenter(ConflictPresenter presenter)
        {
            GetState<ConflictCloudState>().SetPresenter(presenter);
        }

        public void SetServerSave(ref ZipFileFolder folder)
        {
            ServerSave = folder;
            folder = null;
        }

        public void DisposeServerSave()
        {
            ServerSave.Dispose();
            ServerSave = null;
        }

        public void SetState(BaseCloudState next)
        {
            if (_current == next)
            {
                return;
            }

            _current?.OnDeactivated();
            _current = next;
            _current?.OnActivated();
        }

        public void SetIdleState()
        {
            SetState(GetState<IdleCloudState>());
        }

        public T GetState<T>() where T : BaseCloudState
        {
            return Array.Find(_states, c => c is T) as T;
        }

        public void Logout()
        {
            PlayFabClientAPI.ForgetAllCredentials();

            Entity.Id = null;
            Entity.Type = null;

            _playFabId = null;

            AccountState = AccountState.Unknown;
        }

        public TimeSpan GetApiErrorTimeout(PlayFabErrorCode code)
        {
            TimeTicks ticks;
            if (!_apiErrorTimeouts.TryGetValue(code, out ticks))
            {
                ticks = _defaultApiErrorTimeout;
            }

            return SelectErrorTimeout(ticks);
        }

        public void SetCustomErrorTimeout(TimeSpan? timeout)
        {
            _customErrorTimeout = timeout;
        }

        public void EnableAccountCreation()
        {
            foreach (BaseCloudState state in _states)
            {
                var loginState = state as BaseLoginCloudState;
                loginState?.EnableAccountCreation();
            }
        }

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("CloudSave", this);
            _debugStatesSystem.RegisterWriter("Cloud saves", WriteDebugState);

            _states = new BaseCloudState[]
            {
                _instantiator.Instantiate<IdleCloudState>(),
                _instantiator.Instantiate<LoginFacebookCloudState>(),
                _instantiator.Instantiate<LoginDeviceCloudState>(),
                _instantiator.Instantiate<LinkDeviceCloudState>(),
                _instantiator.Instantiate<LinkFacebookCloudState>(),
                _instantiator.Instantiate<ConflictCloudState>(),
                _instantiator.Instantiate<LoadCloudState>(),
                _instantiator.Instantiate<SaveCloudState>()
            };

            foreach (BaseCloudState state in _states)
            {
                var loginState = state as BaseLoginCloudState;
                if (loginState != null)
                {
                    loginState.LoggedIn += OnLogin;
                    loginState.NoAccountDiscovered += OnNoAccountDiscovered;
                }

                state.OnInitialize();
            }

            SetIdleState();
        }

        private void WriteDebugState(StringBuilder builder)
        {
            builder.AppendParameter("Logged in", IsLoggedIn.ToString());
            builder.AppendParameter("Device linked", _isDeviceLinked.ToString());
            builder.AppendParameter("Profile version", _profileVersion.ToString());
            builder.AppendParameter("Server profile version", ServerProfileVersion.ToString());

            builder.AppendParameter("State", _current.GetType().Name);

            if (!string.IsNullOrEmpty(_playFabId))
            {
                builder.AppendParameter("UserId", _playFabId);
            }

            if (!string.IsNullOrEmpty(_linkedFacebookId))
            {
                builder.AppendParameter("Linked FacebookId", _linkedFacebookId);
            }
        }

        protected void Update()
        {
            foreach (BaseCloudState controller in _states)
            {
                controller.OnUpdated();
            }
        }

        protected void OnApplicationPause(bool isPaused)
        {
            foreach (BaseCloudState controller in _states)
            {
                controller.OnApplicationPause(isPaused);
            }
        }

        private TimeSpan SelectErrorTimeout(TimeSpan timeout)
        {
            return _customErrorTimeout ?? timeout;
        }

        private void OnLogin(LoginResult result)
        {
            PlayFab.ClientModels.EntityKey entity = result.EntityToken.Entity;

            Entity.Id = entity.Id;
            Entity.Type = entity.Type;

            _playFabId = result.PlayFabId;

            AccountState = AccountState.HasAccount;
        }

        private void OnNoAccountDiscovered()
        {
            if (AccountState == AccountState.Unknown)
            {
                AccountState = AccountState.HasNoAccount;
            }
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncInt("profileVersion", ref _profileVersion);
            state.SyncBool("isDeviceLinked", ref _isDeviceLinked, true);
            state.SyncString("facebookId", ref _linkedFacebookId, string.Empty);
            state.SyncObjectList("states", _states);
        }

        #region Inner types

        [Serializable]
        private class ErrorTimeoutMap : KeyValueList<PlayFabErrorCode, TimeTicks>
        {
        }

        #endregion
    }
}