﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine.SceneManagement;
using Zenject;

namespace DreamTeam.Cloud.States
{
    public class ConflictCloudState : BaseCloudState
    {
        [Inject] private SaveManager _saveManager;

        private bool _isActive;
        private ConflictPresenter _presenter;

        public override bool IsPending => Manager.IsLoggedIn && Manager.ServerSave != null;

        public override void OnActivated()
        {
            base.OnActivated();

            _isActive = true;

            TryResolve();
        }

        public override void OnDeactivated()
        {
            base.OnDeactivated();

            _isActive = false;
        }

        public void SetPresenter(ConflictPresenter presenter)
        {
            if (_presenter == presenter)
            {
                return;
            }

            _presenter = presenter;
            TryResolve();
        }

        private void TryResolve()
        {
            if (_presenter != null && _isActive)
            {
                _presenter.StartResolve(Resolve);
            }
        }

        private void Resolve(bool useServer)
        {
            if (useServer)
            {
                _saveManager.ImportFiles(Manager.ServerSave);

                Manager.ProfileVersion = Manager.ServerProfileVersion;
                Manager.GetState<SaveCloudState>().ResetTimeout();
                Manager.SaveSettings();

                // Full game reload
                _saveManager.LockSaving();
                ProjectContext.Instance.gameObject.Dispose();
                SceneManager.LoadScene(0);
            }
            else
            {
                // Overwrite server save if user chooses the local one.
                _saveManager.Save();
                Manager.SendSave();
                Manager.SetIdleState();
            }

            Manager.DisposeServerSave();
        }
    }
}