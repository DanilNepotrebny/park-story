﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.SocialNetworking.Facebook;
using Facebook.Unity;
using PlayFab;
using PlayFab.ClientModels;
using Zenject;
using LoginResult = PlayFab.ClientModels.LoginResult;

namespace DreamTeam.Cloud.States
{
    public class LoginFacebookCloudState : BaseLoginCloudState
    {
        [Inject] private FacebookManager _facebookManager;

        private string _loggingUserId;

        public override bool IsPending => base.IsPending && _facebookManager.IsLoggedIn;

        protected override string DebugInfo => "Facebook: " + _facebookManager.AccessToken.UserId;

        public override void OnInitialize()
        {
            base.OnInitialize();

            EnableAccountCreation();

            LoggedIn += OnLoggedIn;
        }

        protected override void Login(
                bool isAccountCreationEnabled,
                Action<LoginResult> resultCallback,
                Action<PlayFabError> errorCallback
            )
        {
            AccessToken token = _facebookManager.AccessToken;

            _loggingUserId = token.UserId;

            var request = new LoginWithFacebookRequest
            {
                AccessToken = token.TokenString,
                CreateAccount = isAccountCreationEnabled
            };
            PlayFabClientAPI.LoginWithFacebook(request, resultCallback, errorCallback);
        }

        private void OnLoggedIn(LoginResult result)
        {
            Manager.LinkedFacebookId = _loggingUserId;
            Manager.SaveSettings();

            _loggingUserId = string.Empty;
        }
    }
}