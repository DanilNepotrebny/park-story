﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Cloud.States
{
    public class IdleCloudState : BaseCloudState
    {
        public override void OnUpdated()
        {
            base.OnUpdated();

            #if !CLOUD_SAVES_DISABLED
            {           
                foreach (BaseCloudState controller in Manager.States)
                {
                    if (!controller.HasError && controller.IsPending)
                    {
                        Manager.SetState(controller);
                        break;
                    }
                }
            }
            #endif
        }
    }
}