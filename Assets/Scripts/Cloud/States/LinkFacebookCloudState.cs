﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.SocialNetworking.Facebook;
using Facebook.Unity;
using PlayFab;
using PlayFab.ClientModels;
using Zenject;

namespace DreamTeam.Cloud.States
{
    public class LinkFacebookCloudState : BaseCloudState
    {
        [Inject] private FacebookManager _facebookManager;

        private string _loggingUserId;

        public override bool IsPending =>
            Manager.IsLoggedIn &&
            _facebookManager.IsLoggedIn &&
            _facebookManager.AccessToken.UserId != Manager.LinkedFacebookId;

        public override void OnActivated()
        {
            base.OnActivated();

            Link();
        }

        private void Link()
        {
            AccessToken token = _facebookManager.AccessToken;

            _loggingUserId = token.UserId;

            UnityEngine.Debug.Log("Linking FacebookId " + _loggingUserId);

            var request = new LinkFacebookAccountRequest { AccessToken = token.TokenString };

            PlayFabClientAPI.LinkFacebookAccount(request, OnLinked, OnLinkFailed);
        }

        private void OnLinkFailed(PlayFabError error)
        {
            switch (error.Error)
            {
                case PlayFabErrorCode.AccountAlreadyLinked:
                case PlayFabErrorCode.LinkedAccountAlreadyClaimed:
                    UnityEngine.Debug.Log(error.ErrorMessage);

                    // Force relogin using Facebook Id.
                    Manager.Logout();

                    Manager.IsDeviceLinked = false;
                    Manager.ProfileVersion = -1;
                    Manager.GetState<LoadCloudState>().ForceCheck();

                    Manager.SetIdleState();
                    break;

                default:
                    UnityEngine.Debug.LogError("Error while linking FacebookId: " + error.ErrorMessage);

                    WaitAfterError(error.Error);
                    Manager.SetIdleState();

                    break;
            }
        }

        private void OnLinked(LinkFacebookAccountResult result)
        {
            UnityEngine.Debug.Log("FacebookId linked.");

            ResetError();

            Manager.LinkedFacebookId = _loggingUserId;
            _loggingUserId = null;

            Manager.SaveSettings();

            Manager.SetIdleState();
        }
    }
}