﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Cloud.DeviceId;
using PlayFab;
using PlayFab.ClientModels;
using Zenject;

namespace DreamTeam.Cloud.States
{
    public class LoginDeviceCloudState : BaseLoginCloudState
    {
        [Inject] private DeviceIdManager _deviceIdManager;

        protected override string DebugInfo => "DeviceId: " + _deviceIdManager.Id;

        public override void OnInitialize()
        {
            base.OnInitialize();

            LoggedIn += OnLogin;
        }

        protected override void Login(
                bool isAccountCreationEnabled,
                Action<LoginResult> resultCallback,
                Action<PlayFabError> errorCallback
            )
        {
            _deviceIdManager.LoginWithDeviceId(resultCallback, errorCallback, isAccountCreationEnabled);
        }

        private void OnLogin(LoginResult result)
        {
            Manager.IsDeviceLinked = true;
            Manager.SaveSettings();
        }
    }
}