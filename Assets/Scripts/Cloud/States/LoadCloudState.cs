﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.IO;
using System.IO.Compression;
using DreamTeam.Utils.FileFolders;
using PlayFab;
using PlayFab.DataModels;
using PlayFab.Internal;
using Zenject;

namespace DreamTeam.Cloud.States
{
    public class LoadCloudState : BaseCloudState
    {
        [Inject] private SaveManager _saveManager;

        private static DateTime _lastCheckTime;
        private static bool _isOutdated = true;

        public bool IsOutdated
        {
            get { return _isOutdated; }
            set { _isOutdated = value; }
        }

        public override bool IsPending => Manager.IsLoggedIn && IsOutdated;

        public event Action SavesUpdated;

        public override void OnActivated()
        {
            base.OnActivated();

            Load();
        }

        public override void OnApplicationPause(bool isPaused)
        {
            base.OnApplicationPause(isPaused);

            if (isPaused || IsOutdated)
            {
                return;
            }

            IsOutdated = DateTime.Now - _lastCheckTime > Manager.LoadInterval;
        }

        public void ForceCheck()
        {
            IsOutdated = true;

            ResetError();
        }

        public void ResetCheckTime()
        {
            IsOutdated = false;
            _lastCheckTime = DateTime.Now;
        }

        private void Load()
        {
            UnityEngine.Debug.Log("Start loading saves.");

            var request = new GetFilesRequest { Entity = Manager.Entity };
            PlayFabDataAPI.GetFiles(request, OnFileMetaGot, OnLoadFailed);
        }

        private void OnFileMetaGot(GetFilesResponse response)
        {
            Manager.ServerProfileVersion = response.ProfileVersion;

            if (Manager.ServerProfileVersion == Manager.ProfileVersion)
            {
                UnityEngine.Debug.Log("Saves are already up to date");

                FinishLoading();
                return;
            }

            GetFileMetadata metadata;
            if (!response.Metadata.TryGetValue("saves.zip", out metadata))
            {
                UnityEngine.Debug.Log("No cloud saves are found.");

                FinishLoading();
                return;
            }

            PlayFabHttp.SimpleGetCall(metadata.DownloadUrl, OnLoadCompleted, OnGetFailed);
        }

        private void OnLoadCompleted(byte[] buffer)
        {
            ZipFileFolder folder;

            try
            {
                folder = new ZipFileFolder(ZipArchiveMode.Read, buffer);
            }
            catch (InvalidDataException exception)
            {
                UnityEngine.Debug.LogException(exception);

                // Save is corrupted. Send new save immediately.
                _saveManager.Save();
                Manager.SendSave();
                FinishLoading();
                return;
            }

            if (Manager.HasLocalSaves)
            {
                UnityEngine.Debug.Log($"Conflict save was found: local {Manager.ProfileVersion} remote {Manager.ServerProfileVersion}");

                Manager.SetServerSave(ref folder);
            }
            else
            {
                _saveManager.ImportFiles(folder);
                folder.Dispose();

                Manager.ProfileVersion = Manager.ServerProfileVersion;
                Manager.SaveSettings();
            }

            FinishLoading();
        }

        private void FinishLoading()
        {
            ResetError();
            ResetCheckTime();

            Manager.SetIdleState();

            SavesUpdated?.Invoke();
        }

        private void OnLoadFailed(PlayFabError error)
        {
            UnityEngine.Debug.LogError(error.ErrorMessage);

            WaitAfterError(error.Error);
            Manager.SetIdleState();
        }

        private void OnGetFailed(string error)
        {
            UnityEngine.Debug.LogError(error);

            WaitAfterError(Manager.GetErrorTimeout);
            Manager.SetIdleState();
        }
    }
}