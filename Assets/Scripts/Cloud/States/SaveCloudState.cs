﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.IO.Compression;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils.FileFolders;
using PlayFab;
using PlayFab.DataModels;
using PlayFab.Internal;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Cloud.States
{
    public class SaveCloudState : BaseCloudState
    {
        private const string _saveFileName = "saves.zip";

        [Inject] private SaveManager _saveManager;

        private float _saveTimeout;
        private bool _hasPendingUpload;

        public override bool IsPending => Manager.IsLoggedIn && _saveTimeout <= 0f;

        public void RemoveTimeout()
        {
            _saveTimeout = 0f;

            ResetError();
        }

        public void ResetTimeout()
        {
            _saveTimeout = (float)Manager.SaveInterval.TotalSeconds;
        }

        public override void OnInitialize()
        {
            base.OnInitialize();

            ResetTimeout();
        }

        public override void OnActivated()
        {
            base.OnActivated();

            if (_hasPendingUpload)
            {
                AbortPendingUpload();
            }
            else
            {
                Save();
            }
        }

        public override void OnUpdated()
        {
            base.OnUpdated();

            if (_saveTimeout > 0f)
            {
                _saveTimeout -= Time.deltaTime;
            }
        }

        protected override void Sync(State state)
        {
            base.Sync(state);

            state.SyncFloat("saveTimeout", ref _saveTimeout);
        }

        private void Save()
        {
            UnityEngine.Debug.Log("Start sending saves");

            var request = new InitiateFileUploadsRequest
            {
                Entity = Manager.Entity,
                ProfileVersion = Manager.ServerProfileVersion,
                FileNames = new List<string> { _saveFileName }
            };
            PlayFabDataAPI.InitiateFileUploads(request, OnUploadInitialized, OnUploadFailed);
        }

        private void OnUploadFailed(PlayFabError error)
        {
            switch (error.Error)
            {
                case PlayFabErrorCode.EntityProfileVersionMismatch:
                    UnityEngine.Debug.Log("Found conflict while sending saves.");

                    Manager.GetState<LoadCloudState>().ForceCheck();
                    Manager.SetIdleState();

                    break;

                case PlayFabErrorCode.EntityFileOperationPending:
                    UnityEngine.Debug.Log("Previous upload was not completed.");

                    _hasPendingUpload = true;
                    AbortPendingUpload();

                    break;

                default:
                    UnityEngine.Debug.LogError("Error while uploading save: " + error.ErrorMessage);

                    WaitAfterError(error.Error);
                    if (_hasPendingUpload)
                    {
                        AbortPendingUpload();
                    }

                    break;
            }
        }

        private void OnPutFailed(string error)
        {
            UnityEngine.Debug.LogError("Error while putting save: " + error);

            WaitAfterError(Manager.PutErrorTimeout);
            AbortPendingUpload();
        }

        private void AbortPendingUpload()
        {
            Assert.IsTrue(_hasPendingUpload, "Trying to abort pending upload when there is no pending upload.");

            var request = new AbortFileUploadsRequest
            {
                Entity = Manager.Entity,
                ProfileVersion = Manager.ServerProfileVersion,
                FileNames = new List<string> { _saveFileName }
            };
            PlayFabDataAPI.AbortFileUploads(request, OnUploadAborted, OnUploadAbortFailed);
        }

        private void OnUploadAborted(AbortFileUploadsResponse response)
        {
            _hasPendingUpload = false;

            UnityEngine.Debug.Log("Upload aborted");

            Manager.ServerProfileVersion = response.ProfileVersion;
            Manager.SetIdleState();
        }

        private void OnUploadAbortFailed(PlayFabError error)
        {
            switch (error.Error)
            {
                case PlayFabErrorCode.NoEntityFileOperationPending:
                    UnityEngine.Debug.Log("No pending upload found.");
                    _hasPendingUpload = false;
                    break;

                default:
                    UnityEngine.Debug.LogError("Upload abortion failed: " + error);
                    WaitAfterError(error.Error);
                    break;
            }

            Manager.SetIdleState();
        }

        private void OnUploadInitialized(InitiateFileUploadsResponse response)
        {
            _hasPendingUpload = true;

            var folder = new ZipFileFolder(ZipArchiveMode.Create);

            _saveManager.ExportFiles(
                    folder,
                    new[] { Manager.SaveGroupId }
                );

            byte[] buffer;
            folder.Dispose(out buffer);

            PlayFabHttp.SimplePutCall(
                    response.UploadDetails[0].UploadUrl,
                    buffer,
                    OnPutSucceed,
                    OnPutFailed
                );
        }

        private void OnPutSucceed()
        {
            var request = new FinalizeFileUploadsRequest
            {
                Entity = Manager.Entity,
                FileNames = new List<string> { _saveFileName }
            };
            PlayFabDataAPI.FinalizeFileUploads(request, OnFileUploaded, OnUploadFailed);
        }

        private void OnFileUploaded(FinalizeFileUploadsResponse response)
        {
            _hasPendingUpload = false;

            Manager.ProfileVersion = response.ProfileVersion;
            Manager.ServerProfileVersion = response.ProfileVersion;

            ResetError();
            ResetTimeout();
            Manager.GetState<LoadCloudState>().ResetCheckTime();

            Manager.SaveSettings();

            UnityEngine.Debug.Log("Successfully uploaded");

            Manager.SetIdleState();
        }
    }
}