﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Cloud.DeviceId;
using PlayFab;
using PlayFab.SharedModels;
using Zenject;

namespace DreamTeam.Cloud.States
{
    public class LinkDeviceCloudState : BaseCloudState
    {
        [Inject] private DeviceIdManager _deviceIdManager;

        public override bool IsPending => Manager.IsLoggedIn && !Manager.IsDeviceLinked;

        public override void OnActivated()
        {
            base.OnActivated();

            Link();
        }

        private void Link()
        {
            UnityEngine.Debug.Log("Linking DeviceId " + _deviceIdManager.Id);

            _deviceIdManager.LinkDeviceId(OnLinked, OnLinkFailed, true);
        }

        private void OnLinkFailed(PlayFabError error)
        {
            UnityEngine.Debug.LogError("Error while linking DeviceId: " + error.ErrorMessage);

            WaitAfterError(error.Error);
            Manager.SetIdleState();
        }

        private void OnLinked(PlayFabResultCommon result)
        {
            UnityEngine.Debug.Log("DeviceId linked.");

            ResetError();

            Manager.IsDeviceLinked = true;
            Manager.SaveSettings();

            Manager.SetIdleState();
        }
    }
}