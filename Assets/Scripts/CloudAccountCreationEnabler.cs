﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using Zenject;

#if !NO_CLOUD && !NO_MATCH3
using DreamTeam.Cloud;
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam
{
    public class CloudAccountCreationEnabler : MonoBehaviour
    {
        [SerializeField] private int _minLevelIndex;

        #if !NO_CLOUD && !NO_MATCH3

        [Inject(Id = GlobalInstaller.MainLevelPackControllerId)]
        private SingleLevelPackController _mainPackController;

        [Inject] private CloudSaveManager _cloudSaveManager;

        protected void Start()
        {
            _mainPackController.CurrentLevelAdvanced += OnCurrentLevelAdvanced;

            CheckCurrentLevelId();
        }

        private void OnCurrentLevelAdvanced(int previousindex, int currentindex)
        {
            CheckCurrentLevelId();
        }

        private void CheckCurrentLevelId()
        {
            if (_mainPackController.CurrentLevelIndex > _minLevelIndex)
            {
                _cloudSaveManager.EnableAccountCreation();
                _mainPackController.CurrentLevelAdvanced -= OnCurrentLevelAdvanced;
            }
        }

        #endif
    }
}