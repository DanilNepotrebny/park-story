﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils.FileFolders;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace DreamTeam
{
    public class SaveGroup : MonoBehaviour, ISynchronizable, IStateInitializationHandler
    {
        [SerializeField] private SaveManager.Group _group;

        private SaveManager _saveManager;
        private bool _wasSynchronized;

        private readonly Dictionary<string, ISynchronizable> _synchronizables =
            new Dictionary<string, ISynchronizable>();

        public SaveManager.Group Group => _group;

        public event Action Loaded;

        public void Sync(State state)
        {
            _wasSynchronized = true;

            foreach (var pair in _synchronizables)
            {
                string id = pair.Key;
                ISynchronizable synchronizable = pair.Value;

                try
                {
                    state.SyncObject(id, synchronizable);
                }
                catch (Exception exception)
                {
                    UnityEngine.Debug.LogError("Error while synchronizing " + id, synchronizable as Object);
                    UnityEngine.Debug.LogException(exception);
                }
            }
        }

        public void OnStateInitialized()
        {
            foreach (var pair in _synchronizables)
            {
                string id = pair.Key;
                ISynchronizable synchronizable = pair.Value;
                IStateInitializationHandler handler = synchronizable as IStateInitializationHandler;

                if (handler != null)
                {
                    try
                    {
                        handler.OnStateInitialized();
                    }
                    catch (Exception exception)
                    {
                        UnityEngine.Debug.LogError(
                                "Error during OnStateInitialized for object " + id,
                                synchronizable as Object
                            );
                        UnityEngine.Debug.LogException(exception);
                    }
                }
            }
        }

        [Inject]
        private void Construct(SaveManager saveManager)
        {
            _saveManager = saveManager;
            _saveManager.RegisterGroup(this);
        }

        protected void Awake()
        {
            _saveManager.Load(this, Loaded);
        }

        protected void OnDestroy()
        {
            _saveManager.UnregisterGroup(this);
        }

        public void RegisterSynchronizable(string id, ISynchronizable saveable)
        {
            if (_wasSynchronized)
            {
                UnityEngine.Debug.LogError($"Registration of {id} happens after synchronization has occured.", saveable as Object);
                return;
            }

            if (_synchronizables.ContainsKey(id))
            {
                UnityEngine.Debug.LogError($"Save group '{name}' already contains synchrinizable with id '{id}'.", saveable as Object);
                return;
            }

            _synchronizables.Add(id, saveable);
        }

        public State CreateDeserializationState(IFileFolder folder, ISynchronizable synchronizable)
        {
            string id = _synchronizables.First(pair => pair.Value == synchronizable).Key;

            State state = _saveManager.CreateDeserializationState(this, folder);
            state.BeginDictionary(id);

            return state;
        }
    }
}