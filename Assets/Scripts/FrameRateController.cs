﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam
{
    public class FrameRateController : MonoBehaviour
    {
        [SerializeField] private int _targetFrameRate = 60;

        /// <summary>
        /// Unity callback.
        /// </summary>
        protected void Awake()
        {
            Application.targetFrameRate = _targetFrameRate;
        }
    }
}