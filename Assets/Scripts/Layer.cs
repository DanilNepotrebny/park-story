﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam
{
    [ExecuteInEditMode, DisallowMultipleComponent]
    public partial class Layer : MonoBehaviour
    {
        [SerializeField] private Mode _sortingMode;
        [SerializeField, SortingLayer] private int _sortingLayer;

        protected void Awake()
        {
            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                this.Dispose();
            }
        }

        #region Inner types

        private enum Mode
        {
            Ordered,
            Isometric
        }

        #endregion Inner types
    }
}