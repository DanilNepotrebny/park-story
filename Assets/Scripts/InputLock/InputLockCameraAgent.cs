﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using TouchScript.Gestures;
using UnityEngine;
using Zenject;

namespace DreamTeam.InputLock
{
    [RequireComponent(typeof(Camera))]
    public class InputLockCameraAgent : MonoBehaviour
    {
        [Inject] private InputLockSystem _inputLockSystem;

        protected void Awake()
        {
            _inputLockSystem.RegisterCameraGestures(GetComponents<Gesture>());
        }

        protected void OnDestroy()
        {
            _inputLockSystem.UnregisterCameraGestures(GetComponents<Gesture>());
        }
    }
}