﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils.Invocation;
using TouchScript;
using TouchScript.Gestures;
using TouchScript.Pointers;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.InputLock
{
    public class InputLockSystem : MonoBehaviour, IGestureDelegate
    {
        private Dictionary<ActionDisposable, Component[]> _allowedInputTargets =
            new Dictionary<ActionDisposable, Component[]>();

        private IEnumerable<Component> _allowedTargetSet;
        private IEnumerable<Component> _emptyTargets;
        private IEnumerable<Transform> _dummyInputTargets;

        public IDisposable LockTargets(IEnumerable<Component> allowedTargets = null)
        {
            var disposable = new ActionDisposable(UnlockTargets);

            Component[] targets;

            if (allowedTargets == null)
            {
                targets = new Component[0];
            }
            else
            {
                targets = allowedTargets.ToArray();
            }

            _allowedInputTargets.Add(disposable, targets);

            SetGesturesDelegate(targets, this);

            UpdateLockState();
            return disposable;
        }

        public void RegisterCameraGestures(Gesture[] cameraGestures)
        {
            SetGesturesDelegate(cameraGestures, this);
        }

        public void UnregisterCameraGestures(Gesture[] cameraGestures)
        {
            SetGesturesDelegate(cameraGestures, null);
        }

        protected void Awake()
        {
            _dummyInputTargets = new[] { new GameObject().transform };
        }

        private void UnlockTargets(ActionDisposable handle)
        {
            Assert.IsTrue(_allowedInputTargets.ContainsKey(handle));

            SetGesturesDelegate(_allowedInputTargets[handle], null);
            _allowedInputTargets.Remove(handle);
            UpdateLockState();
        }

        private void UpdateLockState()
        {
            _allowedTargetSet = null;
            foreach (KeyValuePair<ActionDisposable, Component[]> pair in _allowedInputTargets)
            {
                _allowedTargetSet = _allowedTargetSet == null ? pair.Value : _allowedTargetSet.Intersect(pair.Value);
            }

            if (_allowedTargetSet == null)
            {
                LayerManager.Instance.ClearExclusive();
            }
            else
            {
                IEnumerable<Transform> exclusiveTransforms = _dummyInputTargets;

                if (_allowedTargetSet.Any())
                {
                    exclusiveTransforms = _allowedTargetSet.Select(target => target.transform);
                }

                LayerManager.Instance.SetExclusive(exclusiveTransforms);
            }
        }

        private void SetGesturesDelegate(Component[] targets, IGestureDelegate gestureDelegate)
        {
            foreach (Component target in targets)
            {
                if (target != null)
                {
                    SetGesturesDelegate(target.GetComponents<Gesture>(), gestureDelegate);
                }
            }
        }

        private void SetGesturesDelegate(Gesture[] gestures, IGestureDelegate gestureDelegate)
        {
            foreach (Gesture gesture in gestures)
            {
                gesture.Delegate = gestureDelegate;
            }
        }

        #region IGestureDelegate

        bool IGestureDelegate.ShouldReceivePointer(Gesture gesture, Pointer pointer)
        {
            return _allowedTargetSet == null || _allowedTargetSet.Contains(gesture);
        }

        bool IGestureDelegate.ShouldBegin(Gesture gesture)
        {
            return _allowedTargetSet == null || _allowedTargetSet.Contains(gesture);
        }

        bool IGestureDelegate.ShouldRecognizeSimultaneously(Gesture first, Gesture second)
        {
            return true;
        }

        #endregion
    }
}