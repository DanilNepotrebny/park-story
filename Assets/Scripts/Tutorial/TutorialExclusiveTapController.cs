﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using TouchScript.Gestures;
using UnityEngine;

namespace DreamTeam.Tutorial
{
    public class TutorialExclusiveTapController : MonoBehaviour
    {
        [SerializeField] private TapGesture _gesture;

        public event Action OnTapped;

        public Component GetInputTarget()
        {
            return _gesture;
        }

        private void Awake()
        {
            _gesture.Tapped += OnGestureTapped;
        }

        private void OnGestureTapped(object sender, EventArgs e)
        {
            OnTapped?.Invoke();
        }
    }
}
