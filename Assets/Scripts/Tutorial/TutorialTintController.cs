﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.UI.SimpleAnimating;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace DreamTeam.Tutorial
{
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(SimpleAnimation))]
    public class TutorialTintController : MonoBehaviour
    {
        private Image _tintImage;
        private SimpleAnimation _simpleAnimation;
        private State _controllerState = State.Hidden;

        private Coroutine _coroutine = null;

        public event Action OnShown;
        public event Action OnHidden;

        private enum State
        {
            PrepareShow,
            Showing,
            Shown,
            PrepareHide,
            Hiding,
            Hidden
        }

        public void Show()
        {
            switch (_controllerState)
            {
                case State.PrepareHide:
                    Assert.IsNotNull(_coroutine);
                    StopCoroutine(_coroutine);
                    _controllerState = State.Shown;
                    OnShown?.Invoke();
                    break;

                case State.Hiding:
                    Assert.IsNotNull(_coroutine);
                    StopCoroutine(_coroutine);
                    _coroutine = StartCoroutine(ShowCoroutine());
                    break;

                case State.Hidden:
                    Assert.IsNull(_coroutine);
                    _coroutine = StartCoroutine(ShowCoroutine());
                    break;

                case State.Shown:
                    OnShown?.Invoke();
                    break;
            }
        }

        public void Hide()
        {
            switch (_controllerState)
            {
                case State.PrepareShow:
                    Assert.IsNotNull(_coroutine);
                    StopCoroutine(_coroutine);
                    _controllerState = State.Hidden;
                    OnHidden?.Invoke();
                    break;

                case State.Showing:
                    Assert.IsNotNull(_coroutine);
                    StopCoroutine(_coroutine);
                    _coroutine = StartCoroutine(HideCoroutine());
                    break;

                case State.Shown:
                    Assert.IsNull(_coroutine);
                    _coroutine = StartCoroutine(HideCoroutine());
                    break;

                case State.Hidden:
                    OnHidden?.Invoke();
                    break;
            }
        }

        protected void Awake()
        {
            _tintImage = GetComponent<Image>();
            _simpleAnimation = GetComponent<SimpleAnimation>();
        }

        private IEnumerator ShowCoroutine()
        {
            _controllerState = State.PrepareShow;
            yield return null;

            _controllerState = State.Showing;
            _tintImage.enabled = true;
            _simpleAnimation.Play(SimpleAnimationType.Show);
            while (_simpleAnimation.IsPlaying)
            {
                yield return null;
            }

            _controllerState = State.Shown;
            _coroutine = null;
            OnShown?.Invoke();
        }

        private IEnumerator HideCoroutine()
        {
            _controllerState = State.PrepareHide;
            yield return null;

            _controllerState = State.Hiding;
            _simpleAnimation.Play(SimpleAnimationType.Hide);
            while (_simpleAnimation.IsPlaying)
            {
                yield return null;
            }

            _controllerState = State.Hidden;
            _tintImage.enabled = false;
            _coroutine = null;
            OnHidden?.Invoke();
        }
    }
}