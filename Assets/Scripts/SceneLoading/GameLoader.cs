﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.AssetBundles;
using DreamTeam.Popups;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.UI;
using DreamTeam.Utils;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.Coroutines;
using DreamTeam.Video;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using Zenject;

#if !NO_CLOUD
using DreamTeam.Cloud;
#endif

namespace DreamTeam.SceneLoading
{
    public class GameLoader : MonoBehaviour, ISynchronizable, IInitable
    {
        [Header("Login")]
        [SerializeField] private TimeTicks _maxLoginWaitingTime;
        [SerializeField] private TimeTicks _cloudErrorTimeout;

        [Header("Loading")]
        [SerializeField] private SceneReference _globalScene;
        [SerializeField] private SceneReference _startScene;

        [Header("Animation")]
        [SerializeField] private GameLoaderAnimationSequence _mainMenuAnimationSequence;

        [Header("Popups")]
        [SerializeField] private float _gdprConfirmationPopupDelay = 1f;
        [SerializeField] private SceneReference _gdprConfirmationPopup;

        [Header("Intro video")]
        [SerializeField] private VideoClip _introVideo;
        [SerializeField] private TextAsset _introVideoSubtitles;

        [Inject] private SaveManager _saveManager;
        [Inject] private UIVideoPlayer _videoPlayer;
        [Inject] private LoadingSystem _loadingSystem;
        [Inject] private PopupSystem _popupSystem;
        [Inject] private AssetBundleManager _assetBundleManager;

        #if !NO_CLOUD
        
        [Inject] private CloudSaveManager _cloudSaveManager;

        private float _timeout;

        #endif

        private bool _hasGDPRConfirmed = false;
        private bool _hasIntroVideoPlayed = false;

        private bool _isGDPRPopupShown = false;

        public event Action OnMetagameLoadingComplete;

        public void PlayButtonPressed()
        {
            if (!_hasIntroVideoPlayed)
            {
                // $TMP: Don't show video on devices with low amount of memory because of crash.
                if (SystemInfo.systemMemorySize <= 1024)
                {
                    _hasIntroVideoPlayed = true;
                    StartMetagame();
                }
                else
                {
                    _videoPlayer.OnVideoStopped += OnIntroVideoStopped;
                    _videoPlayer.Play(_introVideo, _introVideoSubtitles);
                }
            }
            else
            {
                StartMetagame();
            }
        }

        public void Init()
        {
            _saveManager.GetSaveGroup(SaveManager.Group.Loading).RegisterSynchronizable(nameof(GameLoader), this);
        }

        protected IEnumerator Start()
        {
            #if !NO_CLOUD && !CLOUD_SAVES_DISABLED
            {
                yield return WaitForSaves();
            }
            #else
            {
                yield return null;
            }
            #endif

            long bytesToDownload = _assetBundleManager.GetBytesToDownload();
            if (bytesToDownload > 0)
            {
                // TODO: Show window with downloadable content size.
                yield return LoadWithProgressBar(_assetBundleManager.DownloadBundles());
            }

            yield return _loadingSystem.LoadScene(
                    _globalScene.AssetPath,
                    true,
                    LoadingSystem.LoadingScreenType.None,
                    LoadSceneMode.Additive
                );

            BaseAsyncProcess process = _loadingSystem.LoadScene(
                    _startScene.AssetPath,
                    false,
                    LoadingSystem.LoadingScreenType.None,
                    LoadSceneMode.Additive
                );

            yield return LoadWithProgressBar(process);
            yield return _mainMenuAnimationSequence.Play();

            if (!_hasGDPRConfirmed)
            {
                yield return new WaitForSeconds(_gdprConfirmationPopupDelay);
                _popupSystem.Show(_gdprConfirmationPopup.Name, null, () => _isGDPRPopupShown = true);
                yield return new WaitUntil(IsGDPRConfirmed);
                _hasGDPRConfirmed = true;
            }

            OnMetagameLoadingComplete?.Invoke();
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncBool("_hasGDPRConfirmed", ref _hasGDPRConfirmed);
            state.SyncBool("_hasIntroVideoPlayed", ref _hasIntroVideoPlayed);
        }

        private void OnIntroVideoStopped(VideoClip clip)
        {
            _hasIntroVideoPlayed = true;

            StartMetagame();
        }

        private void StartMetagame()
        {
            _loadingSystem.LoadScene(_startScene.AssetPath, true, LoadingSystem.LoadingScreenType.Initial);
        }

        private bool IsGDPRConfirmed()
        {
            Popup popup = _popupSystem.GetPopup(_gdprConfirmationPopup.Name);
            return popup != null && _isGDPRPopupShown && !popup.IsShown;
        }

        private BaseAsyncProcess LoadWithProgressBar(BaseAsyncProcess process)
        {
            GetComponentInChildren<PresenterHub>().Initialize(process);
            return process;
        }

        #if !NO_CLOUD

        private IEnumerator WaitForSaves()
        {
            if (_cloudSaveManager.HasLocalSaves)
            {
                yield break;
            }

            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                UnityEngine.Debug.Log("Internet is not reachable, skipping initial saves loading.");
                yield break;
            }

            if (_cloudSaveManager.AccountState == AccountState.Unknown)
            {
                UnityEngine.Debug.Log("Waiting for login.");

                _cloudSaveManager.AccountStateChanged += RemoveTimeout;
                yield return WaitCloudOperation();
                _cloudSaveManager.AccountStateChanged -= RemoveTimeout;
            }

            switch (_cloudSaveManager.AccountState)
            {
                case AccountState.HasNoAccount:
                    yield break;

                case AccountState.Unknown:
                    UnityEngine.Debug.Log("Waiting for login time out.");
                    yield break;
            }

            if (!_cloudSaveManager.IsUpToDate)
            {
                UnityEngine.Debug.Log("Waiting for saves.");

                yield return new WaitForEvent<CloudSaveManager>(
                        _cloudSaveManager,
                        nameof(_cloudSaveManager.SavesUpdated)
                    );
            }
        }

        private IEnumerator WaitCloudOperation()
        {
            _cloudSaveManager.SetCustomErrorTimeout(_cloudErrorTimeout);

            _timeout = (float)((TimeSpan)_maxLoginWaitingTime).TotalSeconds;

            while (_timeout > 0f)
            {
                _timeout -= Time.deltaTime;
                yield return null;
            }

            _cloudSaveManager.SetCustomErrorTimeout(null);
        }

        private void RemoveTimeout()
        {
            _timeout = 0f;
        }
        #endif
    }
}