﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.SceneLoading
{
    [CreateAssetMenu(fileName = "LoadingDependencies", menuName = "DreamTeam/Loading dependencies")]
    public class LoadingDependenciesData : ScriptableObject
    {
        [SerializeField] private List<SceneDependencies> _scenesDependencies;
        [SerializeField] private List<ScenesFolderDependencies> _sceneFoldersDependencies;

        public IDependencies FindSceneDependencies(string scenePath)
        {
            IDependencies result = null;

            int index = _scenesDependencies.FindIndex(data => data.Path == scenePath);
            if (index >= 0)
            {
                result = _scenesDependencies[index];
            }

            if (result == null)
            {
                index = _sceneFoldersDependencies.FindIndex(data => scenePath.StartsWith(data.FolderPath));
                if (index >= 0)
                {
                    result = _sceneFoldersDependencies[index];
                }
            }

            return result;
        }

        public interface IDependencies
        {
            List<SceneReference> Parts { get; }
            List<SceneReference> Dependencies { get; }
            bool IsShared { get; }
        }

        [Serializable]
        public abstract class BaseSceneDependencies : IDependencies
        {
            [SerializeField] private List<SceneReference> _parts;
            [SerializeField] private List<SceneReference> _dependencies;
            [SerializeField] private bool _isShared;
            
            public List<SceneReference> Parts => _parts;
            public List<SceneReference> Dependencies => _dependencies;
            public bool IsShared => _isShared;
        }

        [Serializable]
        private class SceneDependencies : BaseSceneDependencies
        {
            [SerializeField] private SceneReference _scene;

            public string Path => _scene.AssetPath;
        }

        [Serializable]
        public class ScenesFolderDependencies : BaseSceneDependencies
        {
            [SerializeField] private string _folderPath;

            public string FolderPath => _folderPath;
        }
    }
}