﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Video;
using UnityEngine;
using Zenject;

namespace DreamTeam.SceneLoading
{
    public class LoadingComponentsInstaller : MonoInstaller
    {
        [SerializeField] private GameLoader _gameLoader;

        public override void InstallBindings()
        {
            Container.Bind<GameLoader>()
                .FromInstance(_gameLoader)
                .AsSingle()
                .NonLazy();

            Container.Bind<UIVideoPlayer>()
                .FromComponentInHierarchy(null, true)
                .AsSingle()
                .NonLazy();
        }
    }
}