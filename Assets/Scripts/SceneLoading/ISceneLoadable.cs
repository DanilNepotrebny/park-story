﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.AsyncProcesses;
using JetBrains.Annotations;

namespace DreamTeam.SceneLoading
{
    public interface ISceneLoadable
    {
        [CanBeNull] BaseAsyncProcess Load();
    }
}