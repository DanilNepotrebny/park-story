﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DreamTeam.SceneLoading
{
    public class SceneLoader : MonoBehaviour
    {
        [SerializeField] private LoadingDependenciesData _dependenciesData;

        public static bool Enabled { get; set; } = true;

        protected void Start()
        {
            if (!Enabled)
            {
                return;
            }

            Enabled = false;

            BaseAsyncProcess process = SceneTreeLoader.LoadScene(
                    gameObject.scene.path,
                    _dependenciesData,
                    LoadSceneMode.Additive,
                    true
                );

            process.DoWhenCompleted(Activate);
        }

        private void Activate()
        {
            SceneTreeActivator.Activate(gameObject.scene.path, _dependenciesData);
        }
    }
}