﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.SceneLoading
{
    public class SceneRoot : MonoBehaviour
    {
        [SerializeField] private SceneRootType _rootType;

        public SceneRootType RootType => _rootType;
        public bool IsActivated => gameObject.activeSelf;

        public void Activate()
        {
            gameObject.SetActive(true);
        }

        public void Dectivate()
        {
            if (gameObject.activeInHierarchy)
            {
                gameObject.SetActive(false);
            }
        }

        protected void Awake()
        {
            gameObject.SetActive(false);
        }
    }
}