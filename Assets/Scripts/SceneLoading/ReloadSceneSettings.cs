﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.SceneManagement;

namespace DreamTeam.SceneLoading
{
    public class ReloadSceneSettings : MonoBehaviour
    {
        [SerializeField] private LoadingSystem.LoadingScreenType _loadingScreenType;

        public string ScenePath => SceneManager.GetActiveScene().path;
        public LoadingSystem.LoadingScreenType LoadingScreenType => _loadingScreenType;
    }
}