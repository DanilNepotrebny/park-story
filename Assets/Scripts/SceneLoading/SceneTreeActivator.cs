﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Utils;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace DreamTeam.SceneLoading
{
    public class SceneTreeActivator
    {
        private HashSet<string> _alreadyActivated = new HashSet<string>();
        private LoadingDependenciesData _dependenciesData;
        private SceneRootType _rootType;
        private string _scenePath;

        public SceneTreeActivator(
                string scenePath,
                LoadingDependenciesData dependenciesData,
                SceneRootType rootType = SceneRootType.Main
            )
        {
            _scenePath = scenePath;
            _dependenciesData = dependenciesData;
            _rootType = rootType;
        }

        public static void Activate(
                string scenePath,
                LoadingDependenciesData dependenciesData,
                SceneRootType rootType = SceneRootType.Main
            )
        {
            new SceneTreeActivator(scenePath, dependenciesData, rootType).Activate();
        }

        public static void DeactivateScene(string scenePath)
        {
            SceneManager.GetSceneByPath(scenePath)
                .GetComponentsOnScene<SceneRoot>()
                .ForEach(root => root.Dectivate());
        }

        public void Activate()
        {
            ActivateInternal(_scenePath);
        }

        private void ActivateInternal(string scenePath)
        {
            LoadingDependenciesData.IDependencies sceneDependencies = _dependenciesData.FindSceneDependencies(scenePath);
            if (sceneDependencies != null)
            {
                foreach (SceneReference dependence in sceneDependencies.Dependencies)
                {
                    Assert.IsTrue(dependence.HasValue, $"Missing scene reference in dependencies of {scenePath}!");
                    ActivateSceneRecursive(dependence.AssetPath);
                }
            }

            ActivateScene(scenePath);

            if (sceneDependencies != null)
            {
                foreach (SceneReference part in sceneDependencies.Parts)
                {
                    Assert.IsTrue(part.HasValue, $"Missing scene reference in parts of {scenePath}!");
                    ActivateSceneRecursive(part.AssetPath);
                }
            }
        }

        private void ActivateScene(string scenePath)
        {
            if (!_alreadyActivated.Contains(scenePath))
            {
                _alreadyActivated.Add(scenePath);

                Scene scene = SceneManager.GetSceneByPath(scenePath);
                SceneRoot root = scene.FindSceneRoot(_rootType);
                root?.Activate();
            }
        }

        private void ActivateSceneRecursive(string scenePath)
        {
            if (!_alreadyActivated.Contains(scenePath))
            {
                ActivateInternal(scenePath);
            }
        }
    }
}