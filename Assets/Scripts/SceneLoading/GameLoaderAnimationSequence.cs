﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.SocialNetworking.Facebook;
using DreamTeam.UI.SimpleAnimating;
using Spine.Unity;
using Spine.Unity.Modules;
using UnityEngine;
using Zenject;

namespace DreamTeam.SceneLoading
{
    public class GameLoaderAnimationSequence : MonoBehaviour
    {
        [Header("Main animation")]
        [SerializeField] private SkeletonGraphicMultiObject _mainMenuSkeletonGraphic;
        [SerializeField, SpineAnimation(dataField = "_mainMenuSkeletonGraphic")]
        private string _mainMenuAppearance;
        [SerializeField, SpineAnimation(dataField = "_mainMenuSkeletonGraphic")]
        private string _mainMenuIdle;

        [Header("Animations")]
        [SerializeField] private SimpleAnimation _logoAnimation;
        [SerializeField] private SimpleAnimation _progressBarAnimation;
        [SerializeField] private SimpleAnimation _playButtonAnimation;
        [SerializeField] private SimpleAnimation _facebookButtonAnimation;

        [Inject] private FacebookManager _facebookManager;

        public IEnumerator Play()
        {
            _logoAnimation.Play(SimpleAnimationType.Hide);
            _progressBarAnimation.Play(SimpleAnimationType.Hide);
            yield return new WaitWhile(() => _logoAnimation.IsPlaying || _progressBarAnimation.IsPlaying);

            _progressBarAnimation.gameObject.SetActive(false);

            _mainMenuSkeletonGraphic.AnimationState.SetAnimation(0, _mainMenuAppearance, false);
            yield return new WaitForSpineEvent(_mainMenuSkeletonGraphic.AnimationState, "logo");

            _logoAnimation.Play(SimpleAnimationType.Show);
            yield return new WaitWhile(() => _logoAnimation.IsPlaying);

            _playButtonAnimation.gameObject.SetActive(true);
            _playButtonAnimation.Play(SimpleAnimationType.Show);

            if (!_facebookManager.IsLoggedIn)
            {
                _facebookButtonAnimation.gameObject.SetActive(true);
                _facebookButtonAnimation.Play(SimpleAnimationType.Show);
            }

            StartCoroutine(PlayInternal());
        }

        private IEnumerator PlayInternal()
        {
            if (!_mainMenuSkeletonGraphic.AnimationState.GetCurrent(0).IsComplete)
            {
                yield return new WaitForSpineAnimationComplete(_mainMenuSkeletonGraphic.AnimationState.GetCurrent(0));
            }

            _mainMenuSkeletonGraphic.AnimationState.SetAnimation(0, _mainMenuIdle, true);
        }
    }
}