﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using UnityEngine;

namespace DreamTeam.SceneLoading.Screens
{
    public class LoadingScreenCommon : LoadingScreen
    {
        [SerializeField] private Animation _animation;
        [SerializeField] private string _showClipName = "Show";
        [SerializeField] private string _hideClipName = "Hide";

        private State _currentState;

        protected override IEnumerator Show()
        {
            if (_currentState == State.Hidden)
            {
                _currentState = State.ShowProcess;
                _animation.Play(_showClipName);
                yield return new WaitWhile(() => _animation.isPlaying);
                _currentState = State.Shown;
            }
        }

        protected override IEnumerator Hide()
        {
            if (_currentState == State.Shown)
            {
                _currentState = State.HideProcess;
                _animation.Play(_hideClipName);
                yield return new WaitWhile(() => _animation.isPlaying);
                _currentState = State.Hidden;
            }
        }

        #region Inner types

        private enum State
        {
            Hidden,
            ShowProcess,
            Shown,
            HideProcess
        }

        #endregion
    }
}