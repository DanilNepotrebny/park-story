﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.SceneLoading
{
    public class LoadSceneSettings : MonoBehaviour
    {
        [SerializeField] private SceneReference _sceneReference;
        [SerializeField] private LoadingSystem.LoadingScreenType _loadingScreenType;

        public string ScenePath => _sceneReference.AssetPath;
        public LoadingSystem.LoadingScreenType LoadingScreenType => _loadingScreenType;
    }
}