﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.SceneLoading
{
    public enum SceneRootType
    {
        Main,
        Context
    }
}