﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Utils;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;

namespace DreamTeam.SceneLoading
{
    public abstract class LoadingScreen : MonoBehaviour
    {
        [SerializeField, EnumFlag] private LoadingSystem.LoadingScreenType _supportedTypes;

        [Inject] private LoadingSystem _loadingSystem;

        protected void Awake()
        {
            _loadingSystem.LoadingStarting += OnLoadingStarting;
            gameObject.SetActive(false);
        }

        protected void OnDestroy()
        {
            _loadingSystem.LoadingStarting -= OnLoadingStarting;
        }

        protected abstract IEnumerator Show();
        protected abstract IEnumerator Hide();

        private void OnLoadingStarting(
                IDeferredInvocationHandle startInvocation,
                BaseAsyncProcess process,
                LoadingSystem.LoadingScreenType screenType
            )
        {
            if ((screenType & _supportedTypes) > 0)
            {
                gameObject.SetActive(true);

                process.Completed += OnCompleted;

                StartCoroutine(ShowInternal(startInvocation));
            }
        }

        private void OnCompleted()
        {
            StartCoroutine(HideInternal());
        }

        private IEnumerator ShowInternal(IDeferredInvocationHandle startInvocation)
        {
            using (startInvocation.Lock())
            {
                yield return Show();
            }
        }

        private IEnumerator HideInternal()
        {
            yield return Hide();
            gameObject.SetActive(false);
        }
    }
}