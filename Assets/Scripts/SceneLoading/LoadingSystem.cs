﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.Utils;
using DreamTeam.Utils.AsyncProcesses;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace DreamTeam.SceneLoading
{
    public class LoadingSystem : MonoBehaviour
    {
        [SerializeField] private LoadingDependenciesData _dependenciesData;

        [Inject] private Instantiator _instantiator;

        public event LoadingStartingDelegate LoadingStarting;

        public BaseAsyncProcess LoadScene(
                string scenePath,
                bool autoActivate = true,
                LoadingScreenType screenType = LoadingScreenType.None,
                LoadSceneMode mode = LoadSceneMode.Single
            )
        {
            return LoadSceneInternal(scenePath, autoActivate, screenType, mode);
        }

        public void UnloadScene(string scenePath, Action callback)
        {
            StartCoroutine(UnloadSceneInternal(scenePath, callback));
        }

        public IEnumerator UnloadSceneCoroutine(string scenePath)
        {
            yield return UnloadSceneInternal(scenePath, null);
        }

        public void ActivateScene(string scenePath)
        {
            SceneTreeActivator.Activate(scenePath, _dependenciesData);
        }

        public bool IsSceneLoaded(string scenePath)
        {
            return SceneManager.GetSceneByPath(scenePath).isLoaded;
        }

        private void OnLoadingStarted(
                DeferredAsyncProcess process,
                string scenePath,
                LoadSceneMode mode
            )
        {
            BaseAsyncProcess loadProcess = SceneTreeLoader.LoadScene(
                    scenePath,
                    _dependenciesData,
                    mode
                );

            process.BeginProcess(loadProcess);
        }

        private void OnLoadingFinished(string scenePath, bool autoActivate)
        {
            if (autoActivate)
            {
                ActivateScene(scenePath);
            }
        }

        private BaseAsyncProcess LoadSceneInternal(
                string scenePath,
                bool autoActivate,
                LoadingScreenType screenType,
                LoadSceneMode mode
            )
        {
            var process = _instantiator.Instantiate<DeferredAsyncProcess>();

            var startInvocation = new DeferredInvocation(() => OnLoadingStarted(process, scenePath, mode));
            process.Completed += () => OnLoadingFinished(scenePath, autoActivate);

            using (startInvocation.Start())
            {
                LoadingStarting?.Invoke(startInvocation, process, screenType);
            }

            return process;
        }

        private IEnumerator UnloadSceneInternal(string scenePath, Action callback)
        {
            yield return SceneTreeLoader.UnloadScene(scenePath);
            callback?.Invoke();
        }

        #region Inner types

        public delegate void LoadingStartingDelegate(IDeferredInvocationHandle startInvocation, BaseAsyncProcess process, LoadingScreenType screenType);

        [Flags]
        public enum LoadingScreenType
        {
            None = 0,
            Match3 = 1 << 0,
            Location = 1 << 1,
            Initial = 1 << 2
        }

        [Serializable]
        private class LoadingScreensMap : KeyValueList<LoadingScreenType, LoadingScreen>
        {
        }

        #endregion
    }
}
