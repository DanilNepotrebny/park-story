﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using DreamTeam.Utils.AsyncProcesses;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace DreamTeam.SceneLoading
{
    public class SceneTreeLoader
    {
        private string _scenePath;
        private LoadingDependenciesData _dependenciesData;
        private LoadSceneMode _mode;
        private bool _isForcedPreloadEnabled;

        public SceneTreeLoader(
                string scenePath,
                LoadingDependenciesData dependenciesData,
                LoadSceneMode mode,
                bool isForcedPreloadEnabled = false
            )
        {
            _scenePath = scenePath;
            _dependenciesData = dependenciesData;
            _mode = mode;
            _isForcedPreloadEnabled = isForcedPreloadEnabled;
        }

        public static BaseAsyncProcess LoadScene(
                string scenePath,
                LoadingDependenciesData dependenciesData,
                LoadSceneMode mode,
                bool isForcedPreloadEnabled = false
            )
        {
            return new SceneTreeLoader(scenePath, dependenciesData, mode, isForcedPreloadEnabled).LoadScene();
        }

        public static IEnumerator UnloadScene(string scenePath)
        {
            Scene scene = SceneManager.GetSceneByPath(scenePath);
            if (!scene.isLoaded)
            {
                yield break;
            }

            DisposeObjects(scene);

            yield return SceneManager.UnloadSceneAsync(scenePath);
            yield return Resources.UnloadUnusedAssets();
        }

        public BaseAsyncProcess LoadScene()
        {
            switch (_mode)
            {
                case LoadSceneMode.Additive:
                    return LoadAdditive();
                case LoadSceneMode.Single:
                    return LoadSingle();
                default:
                    throw new ArgumentOutOfRangeException(
                            nameof(_mode),
                            $"Can't load scene: mode {_mode} is not supported."
                        );
            }
        }

        private BaseAsyncProcess LoadAdditive()
        {
            List<string> scenesToLoad = new List<string>();
            CollectScenesToLoad(_scenePath, scenesToLoad);

            List<string> scenesToPreload;
            FilterScenes(scenesToLoad, out scenesToPreload);

            var processes = new BaseAsyncProcess[scenesToLoad.Count];

            for (int i = 0; i < processes.Length; i++)
            {
                string path = scenesToLoad[i];
                processes[i] = new AsyncOperationProcess(SceneManager.LoadSceneAsync(path, LoadSceneMode.Additive));
            }

            return CreateLoadingProcess(processes, scenesToPreload);
        }

        private BaseAsyncProcess LoadSingle()
        {
            List<string> scenesToLoad = new List<string>();
            CollectScenesToLoad(_scenePath, scenesToLoad);

            Scene loaderScene = SceneManager.CreateScene("__Loading__");
            SceneManager.SetActiveScene(loaderScene);

            List<string> scenesToUnload = CollectScenesToUnload(scenesToLoad);

            PrepareToSceneUnload(scenesToUnload, scenesToLoad);

            List<string> scenesToPreload;
            FilterScenes(scenesToLoad, out scenesToPreload, s => !scenesToUnload.Contains(s));

            var scenePrecesses = new BaseAsyncProcess[scenesToLoad.Count + scenesToUnload.Count];
            int idx = 0;

            foreach (string path in scenesToUnload)
            {
                scenePrecesses[idx++] = new AsyncOperationProcess(SceneManager.UnloadSceneAsync(path));
            }

            foreach (string path in scenesToLoad)
            {
                var process = new AsyncOperationProcess(SceneManager.LoadSceneAsync(path, LoadSceneMode.Additive));
                scenePrecesses[idx++] = process;

                if (path == _scenePath)
                {
                    process.DoWhenCompleted(() => ActivateSingleScene(loaderScene));
                }
            }

            if (!scenesToLoad.Contains(_scenePath))
            {
                ActivateSingleScene(loaderScene);
            }

            return CreateLoadingProcess(scenePrecesses, scenesToPreload);
        }

        private void FilterScenes(List<string> scenesToLoad, out List<string> scenesToPreload, Predicate<string> predicate = null)
        {
            if (_isForcedPreloadEnabled)
            {
                scenesToPreload = new List<string>(scenesToLoad);
            }
            else
            {
                scenesToPreload = scenesToLoad;
            }

            scenesToLoad.RemoveAll(
                    s =>
                    {
                        Scene scene = SceneManager.GetSceneByPath(s);
                        return scene.isLoaded && (predicate?.Invoke(s) ?? true);
                    }
                );
        }

        private void ActivateSingleScene(Scene loaderScene)
        {
            SceneManager.UnloadSceneAsync(loaderScene);

            Scene scene = SceneManager.GetSceneByPath(_scenePath);
            SceneManager.SetActiveScene(scene);
        }

        private BaseAsyncProcess CreateLoadingProcess(BaseAsyncProcess[] loadingProcesses, List<string> scenesToPreload)
        {
            var sceneLoadingProcess = new GroupAsyncProcess(loadingProcesses);
            var objectPreloadingProcess = new DeferredAsyncProcess();

            sceneLoadingProcess.DoWhenCompleted(() => CreateObjectPreloadingProcess(scenesToPreload, objectPreloadingProcess));

            var tasks = new[]
            {
                new GroupAsyncProcess.Task(sceneLoadingProcess, 0.8f),
                new GroupAsyncProcess.Task(objectPreloadingProcess, 0.2f)
            };

            var process = new GroupAsyncProcess(tasks);
            process.DoWhenCompleted(() => Resources.UnloadUnusedAssets());

            return process;
        }

        private void CreateObjectPreloadingProcess(List<string> scenesToLoad, DeferredAsyncProcess process)
        {
            SceneTreeActivator.Activate(_scenePath, _dependenciesData, SceneRootType.Context);

            IEnumerable<BaseAsyncProcess> loadableProcesses = scenesToLoad
                .Select(SceneManager.GetSceneByPath)
                .Select(scene => scene.FindSceneRoot())
                .SelectMany(root => root.GetComponentsInChildren<ISceneLoadable>(true))
                .Select(loadable => loadable.Load());

            process.BeginProcess(new GroupAsyncProcess(loadableProcesses));
        }

        private List<string> CollectScenesToUnload(List<string> scenesToLoad)
        {
            List<string> scenesToUnload = new List<string>();
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                Scene scene = SceneManager.GetSceneAt(i);

                if (ShouldUnloadScene(scene, scenesToLoad))
                {
                    scenesToUnload.Add(scene.path);
                }
            }

            scenesToUnload.Sort(new SceneUnloadingComparer(_dependenciesData));
            return scenesToUnload;
        }

        private bool ShouldUnloadScene(
                Scene scene,
                List<string> scenesToLoad
            )
        {
            if (scene == SceneManager.GetActiveScene())
            {
                return false;
            }

            LoadingDependenciesData.IDependencies dependencies = _dependenciesData.FindSceneDependencies(scene.path);

            // Quick fix for not unloading preloaded scenes on main menu.
            bool isDependency =
                dependencies != null &&
                dependencies.Dependencies.Any(reference => reference.AssetPath == _scenePath);

            if (!isDependency && !scenesToLoad.Contains(scene.path))
            {
                return true;
            }

            bool isShared = dependencies != null && dependencies.IsShared;

            // Not shared and already activated scenes should be reloaded.
            return !isShared && scene.FindSceneRoot().IsActivated;
        }

        private void CollectScenesToLoad(string scenePath, List<string> scenes)
        {
            if (scenes.Contains(scenePath))
            {
                return;
            }

            scenes.Add(scenePath);

            LoadingDependenciesData.IDependencies sceneDependencies = _dependenciesData.FindSceneDependencies(scenePath);
            if (sceneDependencies != null)
            {
                foreach (SceneReference dependence in sceneDependencies.Dependencies)
                {
                    Assert.IsTrue(dependence.HasValue, $"Missing scene reference in dependencies of {scenePath}!");
                    CollectScenesToLoad(dependence.AssetPath, scenes);
                }

                foreach (SceneReference part in sceneDependencies.Parts)
                {
                    Assert.IsTrue(part.HasValue, $"Missing scene reference in parts of {scenePath}!");
                    CollectScenesToLoad(part.AssetPath, scenes);
                }
            }
        }

        private void PrepareToSceneUnload(List<string> scenesToUnload, List<string> scenesToLoad)
        {
            foreach (string scenePath in scenesToUnload)
            {
                DisposeObjects(SceneManager.GetSceneByPath(scenePath));
                SceneTreeActivator.DeactivateScene(scenePath);

                LoadingDependenciesData.IDependencies sceneDependencies = _dependenciesData.FindSceneDependencies(scenePath);

                if (sceneDependencies?.Parts != null)
                {
                    foreach (SceneReference part in sceneDependencies.Parts)
                    {
                        if (scenesToLoad.Contains(part.AssetPath))
                        {
                            SceneTreeActivator.DeactivateScene(part.AssetPath);
                        }
                    }
                }
            }
        }

        private static void DisposeObjects(Scene scene)
        {
            ObjectLifetimeHandler[] handlers = scene
                .FindSceneRoot(SceneRootType.Context)?
                .GetComponents<ObjectLifetimeHandler>();

            if (handlers == null)
            {
                return;
            }

            foreach (ObjectLifetimeHandler handler in handlers)
            {
                handler.DisposeComponents();
            }
        }

        #region Inner types

        private class SceneUnloadingComparer : IComparer<string>
        {
            private LoadingDependenciesData _dependenciesData;
            public SceneUnloadingComparer(LoadingDependenciesData dependenciesData)
            {
                _dependenciesData = dependenciesData;
            }

            public int Compare(string scenePathA, string scenePathB)
            {
                HashSet<string> processedPaths = new HashSet<string>();
                if (ContainsDependency(scenePathA, scenePathB, false, processedPaths))
                {
                    return -1;
                }

                processedPaths.Clear();
                if (ContainsDependency(scenePathB, scenePathA, false, processedPaths))
                {
                    return 1;
                }

                if (ContainsPart(scenePathA, scenePathB))
                {
                    return 1;
                }

                if (ContainsPart(scenePathB, scenePathA))
                {
                    return -1;
                }

                return 0;
            }

            private bool ContainsDependency(string scenePath, string dependencyPath, bool checkParts, HashSet<string> processedPaths)
            {
                processedPaths.Add(scenePath);

                LoadingDependenciesData.IDependencies dependencies = _dependenciesData.FindSceneDependencies(scenePath);

                if (dependencies == null)
                {
                    return false;
                }

                IEnumerable<string> dependenciesPaths = dependencies.Dependencies.Select(sceneRef => sceneRef.AssetPath);
                if (dependenciesPaths.Contains(dependencyPath))
                {
                    return true;
                }

                foreach (string path in dependenciesPaths)
                {
                    if (!processedPaths.Contains(path) &&
                        ContainsDependency(path, dependencyPath, true, processedPaths))
                    {
                        return true;
                    }
                }

                if (checkParts)
                {
                    IEnumerable<string> partsPaths = dependencies.Parts.Select(sceneRef => sceneRef.AssetPath);
                    if (partsPaths.Contains(scenePath))
                    {
                        return true;
                    }

                    foreach (string path in partsPaths)
                    {
                        if (!processedPaths.Contains(path) &&
                            ContainsDependency(path, dependencyPath, true, processedPaths))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }

            private bool ContainsPart(string scenePath, string partPath)
            {
                LoadingDependenciesData.IDependencies dependencies = _dependenciesData.FindSceneDependencies(scenePath);

                if (dependencies == null)
                {
                    return false;
                }

                IEnumerable<string> partsPaths = dependencies.Parts.Select(sceneRef => sceneRef.AssetPath);
                if (partsPaths.Contains(partPath))
                {
                    return true;
                }

                foreach (string path in partsPaths)
                {
                    if (ContainsPart(path, partPath))
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        #endregion
    }
}