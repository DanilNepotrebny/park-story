﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Globalization;
using DreamTeam.Popups;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.InviteFriends
{
    public class InviteFriendsSystem : MonoBehaviour, ISynchronizable, IInitable
    {
        [Inject] private SaveGroup _saveGroup;
        [Inject] private PopupQueueSystem _popupQueueSystem;

        [SerializeField] private TimeTicks _invitePopupAppearInterval;
        [SerializeField] private SceneReference _invitePopup;

        private string _previousInvitePopupAppearDate = string.Empty;

        public bool CanShowInvitePopup()
        {
            bool result = true;

            if (!string.IsNullOrEmpty(_previousInvitePopupAppearDate))
            {
                DateTime previousInvitePopupAppearDate = DateTime.Parse(_previousInvitePopupAppearDate, CultureInfo.InvariantCulture);
                TimeSpan diff = DateTime.Now - previousInvitePopupAppearDate;
                result = diff.Days >= 1;
            }

            return result;
        }

        public void EnqueueInvitePopup()
        {
            _popupQueueSystem.Enqueue(_invitePopup.Name);
            _previousInvitePopupAppearDate = DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("InviteFriendsSystem", this);
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncString("previousInvitePopupAppearTime", ref _previousInvitePopupAppearDate, string.Empty);
        }
    }
}