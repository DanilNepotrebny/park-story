﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.UI.SimpleAnimating;
using UnityEngine;

namespace DreamTeam.Playables.SimpleAnimationPlayable
{
    public class SimpleAnimationClipAsset :
        BaseTimelineClipAsset<BaseTimelineMixerBehaviour, SimpleAnimationBehaviour>
    {
        [SerializeField] private ExposedReference<SimpleAnimation> _animation;

        public SimpleAnimation GetAnimation(IExposedPropertyTable resolver)
        {
            return _animation.Resolve(resolver);
        }
    }
}
