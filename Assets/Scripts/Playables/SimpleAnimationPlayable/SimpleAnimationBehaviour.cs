﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Cutscenes;
using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Playables;
using Zenject;

namespace DreamTeam.Playables.SimpleAnimationPlayable
{
    public class SimpleAnimationBehaviour : BaseTimelineClipBehaviour
    {
        [Inject] private CutsceneSystem _cutsceneSystem;

        private SimpleAnimation _simpleAnimation;
        private ActionDisposable _pauseCutsceneDisposable;

        public override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPlay(playable, frameData);

            if (Application.isPlaying)
            {
                _simpleAnimation = GetClipAsset<SimpleAnimationClipAsset>().GetAnimation(
                        Playable.GetGraph().GetResolver()
                    );
                _simpleAnimation.gameObject.SetActive(true);
                PlayAnimation(SimpleAnimationType.Show);
            }
        }

        public override void OnBehaviourPause(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPause(playable, frameData);

            if (Application.isPlaying &&
                playable.GetTime() + frameData.deltaTime >= playable.GetDuration())
            {
                PlayAnimation(SimpleAnimationType.Hide);
            }
        }

        private void PlayAnimation(SimpleAnimationType animationType)
        {
            if (_simpleAnimation != null)
            {
                _simpleAnimation.AnimationStopped += OnAnimationStopped;
                _simpleAnimation.Play(animationType);

                Assert.IsNull(_pauseCutsceneDisposable, $"_pauseCutsceneDisposable != null");
                _pauseCutsceneDisposable = _cutsceneSystem.PauseCurrentCutscene();
            }
        }

        private void OnAnimationStopped(SimpleAnimationType animationType)
        {
            _simpleAnimation.AnimationStopped -= OnAnimationStopped;
            ReleasePauseCutsceneDisposable();

            if (animationType == SimpleAnimationType.Hide)
            {
                _simpleAnimation.gameObject.SetActive(false);
            }
        }

        private void ReleasePauseCutsceneDisposable()
        {
            _pauseCutsceneDisposable?.Dispose();
            _pauseCutsceneDisposable = null;
        }
    }
}