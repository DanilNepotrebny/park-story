﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.UI.Dialogs;
using UnityEngine;
using UnityEngine.Playables;
using Zenject;

namespace DreamTeam.Playables.Dialog
{
    [Serializable]
    public class DialogMixerBehaviour : BaseTimelineMixerBehaviour
    {
        [Inject] private DialogController _dialogController;

        private Playable _rootPlayable;

        private readonly LinkedList<DialogPhraseBehaviour> _phrasesQueue = new LinkedList<DialogPhraseBehaviour>();

        private bool _isWaitingForInput;

        public override void OnGraphStart(Playable playable)
        {
            if (Application.isPlaying)
            {
                _rootPlayable = playable.GetGraph().GetRootPlayable(0);
            }
        }

        public override void OnGraphStop(Playable playable)
        {
            if (Application.isPlaying)
            {
                if (_rootPlayable.IsValid())
                {
                    _rootPlayable.SetSpeed(1.0f);
                }

                _rootPlayable = Playable.Null;
                _isWaitingForInput = false;
                _phrasesQueue.Clear();

                if (_dialogController.IsActive)
                {
                    EndDialog(false);
                }
            }
        }

        public override void OnClipBehaviourPlaying(Playable playable)
        {
            if (Application.isPlaying)
            {
                ScriptPlayable<DialogPhraseBehaviour> scriptPlayable = (ScriptPlayable<DialogPhraseBehaviour>)playable;
                DialogPhraseBehaviour phraseBehaviour = scriptPlayable.GetBehaviour();

                if (!_phrasesQueue.Contains(phraseBehaviour))
                {
                    _phrasesQueue.AddLast(phraseBehaviour);
                    if (_phrasesQueue.Count == 1)
                    {
                        ShowDialog(phraseBehaviour.Phrase);
                    }
                }
            }
        }

        public override void OnClipBehaviourPlayed(Playable playable)
        {
            if (Application.isPlaying)
            {
                ScriptPlayable<DialogPhraseBehaviour> scriptPlayable = (ScriptPlayable<DialogPhraseBehaviour>)playable;
                DialogPhraseBehaviour phraseBehaviour = scriptPlayable.GetBehaviour();

                if (_phrasesQueue.Contains(phraseBehaviour) &&
                    _phrasesQueue.First?.Value == phraseBehaviour &&
                    !_isWaitingForInput)
                {
                    WaitForInput(playable.GetHandle() == Playable.GetInput(Playable.GetInputCount() - 1).GetHandle());
                }
            }
        }

        private void ShowDialog(DialogController.PhraseSettings phrase)
        {
            _rootPlayable.SetSpeed(0);
            _dialogController.ShowPhrase(
                    phrase,
                    Playable.GetGraph().GetResolver(),
                    () => _rootPlayable.SetSpeed(1)
                );
        }

        private void WaitForInput(bool isPhraseLast)
        {
            _isWaitingForInput = true;
            _rootPlayable.SetSpeed(0);
            _dialogController.WaitForInput(
                    () =>
                    {
                        _isWaitingForInput = false;
                        _rootPlayable.SetSpeed(1);

                        _phrasesQueue.RemoveFirst();

                        if (isPhraseLast)
                        {
                            EndDialog(true);
                        }
                        else if (_phrasesQueue.Count > 0)
                        {
                            ShowDialog(_phrasesQueue.First.Value.Phrase);
                        }
                    }
                );
        }

        private void EndDialog(bool isTimelinePaused)
        {
            Action callback = null;
            if (isTimelinePaused)
            {
                _rootPlayable.SetSpeed(0);
                callback = () => _rootPlayable.SetSpeed(1);
            }

            _dialogController.EndDialog(false, callback);
        }
    }
}