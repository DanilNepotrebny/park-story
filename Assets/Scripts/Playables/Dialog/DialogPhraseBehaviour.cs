﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.UI.Dialogs;

namespace DreamTeam.Playables.Dialog
{
    [Serializable]
    public class DialogPhraseBehaviour : BaseTimelineClipBehaviour
    {
        public DialogController.PhraseSettings Phrase => GetClipAsset<DialogPhraseClipAsset>().Phrase;
    }
}