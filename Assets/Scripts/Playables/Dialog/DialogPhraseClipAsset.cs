﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.UI.Dialogs;
using UnityEngine;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.Dialog
{
    [Serializable]
    public class DialogPhraseClipAsset : BaseTimelineClipAsset<DialogMixerBehaviour, DialogPhraseBehaviour>, ITimelineClipAsset
    {
        [SerializeField] private DialogController.PhraseSettings _phrase;

        public ClipCaps clipCaps => ClipCaps.None;

        public DialogController.PhraseSettings Phrase => _phrase;
    }
}