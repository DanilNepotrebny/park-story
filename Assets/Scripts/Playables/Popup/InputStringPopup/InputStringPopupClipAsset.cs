﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Playables.Popup.BasePopupPlayables;
using UnityEngine;

namespace DreamTeam.Playables.Popup.InputStringPopup
{
    public class InputStringPopupClipAsset : BasePopupClipAsset
    {
        [SerializeField] private Popups.InputStringPopup.Args _popupArgs = new Popups.InputStringPopup.Args();

        public override Popups.Popup.Args PopupArgs => _popupArgs;
    }
}