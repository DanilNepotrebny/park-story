// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Playables.Popup.BasePopupPlayables;
using Zenject;

namespace DreamTeam.Playables.Popup.InputStringPopup
{
    public class UserNamePopupController : BasePopupPlayableController
    {
        [Inject] private UserSettings _userSettings;

        protected override void SetupPopupArgs(Popups.Popup.Args args, Action popupReleasedCallback)
        {
            base.SetupPopupArgs(args, popupReleasedCallback);

            var inputStringArgs = args as Popups.InputStringPopup.Args;
            if (inputStringArgs != null)
            {
                inputStringArgs.ValueAcceptedCallback = OnValueAccepted;
                inputStringArgs.DefaultValue = _userSettings.UserName;
            }
        }

        private void OnValueAccepted(string newValue)
        {
            _userSettings.UserName = newValue;
        }
    }
}