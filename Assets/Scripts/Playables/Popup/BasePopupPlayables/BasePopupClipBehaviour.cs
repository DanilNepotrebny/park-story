﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Playables;

namespace DreamTeam.Playables.Popup.BasePopupPlayables
{
    public class BasePopupClipBehaviour : BaseTimelineClipBehaviour
    {
        private double _previousSpeed;
        private Playable _rootPlayable;

        private BasePopupClipAsset _clipAsset;
        private BasePopupPlayableController _boundObject;

        public override void OnGraphStart(Playable playable)
        {
            base.OnGraphStart(playable);

            _rootPlayable = playable.GetGraph().GetRootPlayable(0);
        }

        public override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPlay(playable, frameData);

            if (Application.isPlaying)
            {
                _clipAsset = GetClipAsset<BasePopupClipAsset>();
                _boundObject = GetBoundObject<BasePopupPlayableController>();

                _boundObject.OpenPopup(_clipAsset.PopupName, _clipAsset.PopupArgs, OnPopupReleased);
                _previousSpeed = _rootPlayable.GetSpeed();
                _rootPlayable.SetSpeed(0);
            }
        }

        private void OnPopupReleased()
        {
            _rootPlayable.SetSpeed(_previousSpeed);
        }
    }
}