﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Popups;
using UnityEngine;
using Zenject;

namespace DreamTeam.Playables.Popup.BasePopupPlayables
{
    public class BasePopupPlayableController : MonoBehaviour
    {
        [Inject] private PopupSystem _popupSystem;

        /// <summary>
        /// Called when popup clip has started.
        /// Should be called from <see cref="BasePopupClipBehaviour"/> only.
        /// </summary>
        public void OpenPopup(string popupSceneName, Popups.Popup.Args args, Action popupReleasedCallback)
        {
            SetupPopupArgs(args, popupReleasedCallback);
            _popupSystem.Show(popupSceneName, args);
        }

        protected virtual void SetupPopupArgs(Popups.Popup.Args args, Action popupReleasedCallback)
        {
            args.ClosedCallback = popupReleasedCallback;
        }
    }
}