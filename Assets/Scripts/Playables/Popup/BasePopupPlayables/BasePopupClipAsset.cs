﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.Popup.BasePopupPlayables
{
    public class BasePopupClipAsset : BaseTimelineClipAsset<BaseTimelineMixerBehaviour, BasePopupClipBehaviour>, ITimelineClipAsset
    {
        [SerializeField] private SceneReference _popupScene;

        public ClipCaps clipCaps => ClipCaps.None;
        public string PopupName => _popupScene.Name;
        public virtual Popups.Popup.Args PopupArgs { get; } = new Popups.Popup.Args();

        protected override void OnPlayableCreated(ScriptPlayable<BasePopupClipBehaviour> scriptPlayable)
        {
            base.OnPlayableCreated(scriptPlayable);
            ClipBehaviour.RequireComponent<BasePopupPlayableController>();
        }
    }
}
