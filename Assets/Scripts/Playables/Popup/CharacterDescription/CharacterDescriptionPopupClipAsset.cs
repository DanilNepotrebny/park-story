﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Playables.Popup.BasePopupPlayables;
using DreamTeam.Popups;
using UnityEngine;

namespace DreamTeam.Playables.Popup.CharacterDescription
{
    public class CharacterDescriptionPopupClipAsset : BasePopupClipAsset
    {
        [SerializeField] private CharacterDescriptionPopup.Args _popupArgs = new CharacterDescriptionPopup.Args();

        public override Popups.Popup.Args PopupArgs => _popupArgs;
    }
}