﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Playables.Popup.BasePopupPlayables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.Popup.CharacterDescription
{
    [TrackColor(0.9960784f, 0.8039216f, 0f)]
    [TrackClipType(typeof(CharacterDescriptionPopupClipAsset))]
    public class CharacterDescriptionPopupTrack : BasePopupTrack
    {
    }
}