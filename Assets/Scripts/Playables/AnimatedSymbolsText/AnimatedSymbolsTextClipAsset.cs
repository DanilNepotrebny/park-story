﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;

namespace DreamTeam.Playables.AnimatedSymbolsText
{
    [Serializable]
    public class AnimatedSymbolsTextClipAsset : BaseTimelineClipAsset<BaseTimelineMixerBehaviour, AnimatedSymbolsTextClipBehaviour>
    {
        public enum ActionType
        {
            Show,
            Hide
        }

        [SerializeField] private ActionType _actionType;

        public ActionType CachedActionType => _actionType;
    }
}