﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.UI.SymbolAnimation;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.AnimatedSymbolsText
{
    [TrackClipType(typeof(AnimatedSymbolsTextClipAsset))]
    [TrackBindingType(typeof(AnimatedSymbolsCreator))]
    public class AnimatedSymbolsTextTrack : BaseTimelineTrackAsset<BaseTimelineMixerBehaviour, AnimatedSymbolsTextClipBehaviour>
    {
    }
}