﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.UI.SymbolAnimation;
using UnityEngine.Playables;

namespace DreamTeam.Playables.AnimatedSymbolsText
{
    public class AnimatedSymbolsTextClipBehaviour : BaseTimelineClipBehaviour
    {
        public override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPlay(playable, frameData);

            AnimatedSymbolsCreator boundObject = GetBoundObject<AnimatedSymbolsCreator>();
            AnimatedSymbolsTextClipAsset clipAsset = GetClipAsset<AnimatedSymbolsTextClipAsset>();

            IEnumerator action = null;
            switch (clipAsset.CachedActionType)
            {
                case AnimatedSymbolsTextClipAsset.ActionType.Show:
                    action = boundObject.Show((float)clipAsset.TimelineClip.duration);
                    break;

                case AnimatedSymbolsTextClipAsset.ActionType.Hide:
                    action = boundObject.Hide((float)clipAsset.TimelineClip.duration);
                    break;
            }

            boundObject.StartCoroutine(action);
        }
    }
}