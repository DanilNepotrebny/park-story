﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Location.Decorations;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Playables;

namespace DreamTeam.Playables.Decorations
{
    [Serializable]
    public class DecorationAppearanceClipBehaviour : BaseTimelineClipBehaviour
    {
        public override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPlay(playable, frameData);

            if (Application.isPlaying)
            {
                DecorationController boundObject = GetBoundObject<DecorationController>();
                if (boundObject != null)
                {
                    boundObject.Presenter.SetAppearTrigger();
                }
            }
        }
    }
}