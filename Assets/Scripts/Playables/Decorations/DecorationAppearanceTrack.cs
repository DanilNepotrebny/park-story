﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Decorations;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.Decorations
{
    [TrackColor(0.9960784f, 0.8039216f, 0.4235294f)]
    [TrackClipType(typeof(DecorationAppearanceClipAsset))]
    [TrackBindingType(typeof(DecorationController))]
    public class DecorationAppearanceTrack : BaseTimelineTrackAsset<BaseTimelineMixerBehaviour, DecorationAppearanceClipBehaviour>
    {
    }
}