﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Isometry;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.IsometryDirection
{
    [TrackColor(0f, 1f, 1f)]
    [TrackClipType(typeof(IsometryDirectionClipAsset))]
    [TrackBindingType(typeof(IsometryMovable))]
    public class IsometryDirectionTrack : BaseTimelineTrackAsset<BaseTimelineMixerBehaviour, IsometryDirectionBehaviour>
    {
    }
}