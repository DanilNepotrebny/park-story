﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Isometry;
using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Playables;

namespace DreamTeam.Playables.IsometryDirection
{
    public class IsometryDirectionBehaviour : BaseTimelineClipBehaviour
    {
        private IsometryDirectionClipAsset _clipAsset;
        private IsometryMovable _boundObject;
        private float _startAngle;

        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            base.OnBehaviourPlay(playable, info);

            _clipAsset = GetClipAsset<IsometryDirectionClipAsset>();
            _boundObject = GetBoundObject<IsometryMovable>();

            if (_boundObject != null)
            {
                ResetAngles();
            }
        }

        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            base.ProcessFrame(playable, info, playerData);

            if (!_clipAsset.TrackAsset.muted && _boundObject != null)
            {
                float progress = _clipAsset.Easing.Lerp(0f, 1f, (float)playable.GetProgress());
                float angle = Mathf.LerpAngle(_startAngle, _clipAsset.EndAngle, progress);
                _boundObject.Direction = IsometrySystem.IsometricAngleToDirection(angle);
            }
        }

        public override void OnBehaviourPause(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPause(playable, frameData);

            if (_clipAsset != null &&
                _clipAsset.TrackAsset != null &&
                !_clipAsset.TrackAsset.muted &&
                _boundObject != null &&
                playable.GetTime() + frameData.deltaTime >= playable.GetDuration())
            {
                _boundObject.Direction = IsometrySystem.IsometricAngleToDirection(_clipAsset.EndAngle);
            }
        }

        public void ResetAngles()
        {
            _startAngle = _clipAsset.UseCurrentAngle ?
                IsometrySystem.IsometricDirectionToAngle(_boundObject.Direction) :
                _clipAsset.StartAngle;
        }
    }
}