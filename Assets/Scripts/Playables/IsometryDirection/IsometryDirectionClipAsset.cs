﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.IsometryDirection
{
    [Serializable]
    public partial class IsometryDirectionClipAsset : BaseTimelineClipAsset<BaseTimelineMixerBehaviour, IsometryDirectionBehaviour>, ITimelineClipAsset
    {
        [SerializeField, HideInInspector] private bool _useCurrentAngle;
        [SerializeField, HideInInspector] private float _startAngle;
        [SerializeField, HideInInspector] private float _endAngle;

        [SerializeField] private AnimationCurve _easing = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

        public ClipCaps clipCaps => ClipCaps.SpeedMultiplier;

        public bool UseCurrentAngle => _useCurrentAngle;
        public float StartAngle => _startAngle;
        public float EndAngle => _endAngle;

        public AnimationCurve Easing => _easing;
    }
}