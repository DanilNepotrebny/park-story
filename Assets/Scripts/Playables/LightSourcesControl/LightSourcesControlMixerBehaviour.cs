﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Location.Night;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Playables;
using Zenject;

namespace DreamTeam.Playables.LightSourcesControl
{
    [Serializable]
    public class LightSourcesControlMixerBehaviour : BaseTimelineMixerBehaviour
    {
        [Inject]
        private NightSystem _nightSystem;

        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            if (Application.isPlaying)
            {
                bool isEnabled = false;
                float blendingFactor = 0;

                int inputCount = playable.GetInputCount();
                for (int i = 0; i < inputCount; i++)
                {
                    Playable inputPlayable = playable.GetInput(i);
                    float inputWeight = playable.GetInputWeight(i);
                    if (blendingFactor < inputWeight)
                    {
                        var lightSourcePlayable = (ScriptPlayable<BaseTimelineClipBehaviour>)inputPlayable;
                        BaseTimelineClipBehaviour behaviour = lightSourcePlayable.GetBehaviour();
                        LightSourcesControlClipAsset clipAsset = behaviour.GetClipAsset<LightSourcesControlClipAsset>();
                        double easeOutDuration = clipAsset.TimelineClip.easeOutDuration;

                        double blendingOutTime = inputPlayable.GetDuration() - easeOutDuration;
                        double time = inputPlayable.GetTime();

                        isEnabled = inputWeight > 0 && time < blendingOutTime;
                        blendingFactor = inputWeight;
                    }
                }

                blendingFactor = isEnabled ? blendingFactor : 1 - blendingFactor;

                foreach (LightSource lightSource in _nightSystem.LightSources)
                {
                    lightSource.SetEnabled(isEnabled, blendingFactor);
                }
            }
        }
    }
}