﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.GameControls;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.GameControls
{
    public class GameControlsModeClipAsset : BaseTimelineClipAsset<BaseTimelineMixerBehaviour, GameControlsModeBehaviour>, ITimelineClipAsset
    {
        [SerializeField] private GameControlsMode _gameControlMode;

        public ClipCaps clipCaps => ClipCaps.None;

        public GameControlsMode GameControlsMode => _gameControlMode;
    }
}
