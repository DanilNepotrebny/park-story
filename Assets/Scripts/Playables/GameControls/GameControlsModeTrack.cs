﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.GameControls
{
    [TrackColor(0.9960784f, 0.8039216f, 0f)]
    [TrackClipType(typeof(GameControlsModeClipAsset))]
    public class GameControlsModeTrack : BaseTimelineTrackAsset<BaseTimelineMixerBehaviour, GameControlsModeBehaviour>
    {
    }
}