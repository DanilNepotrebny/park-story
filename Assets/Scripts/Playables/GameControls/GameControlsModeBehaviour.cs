﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Cutscenes;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Playables;
using Zenject;

namespace DreamTeam.Playables.GameControls
{
    public class GameControlsModeBehaviour : BaseTimelineClipBehaviour
    {
        [Inject] private CutsceneSystem _cutsceneSystem;
        
        private GameControlsModeClipAsset Clip => GetClipAsset<GameControlsModeClipAsset>();

        public override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPlay(playable, frameData);

            if (Application.isPlaying)
            {
                _cutsceneSystem.OverrideGameControlsMode(Clip.GameControlsMode);
            }
        }

        public override void OnBehaviourPause(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPlay(playable, frameData);

            if (Application.isPlaying &&
                playable.GetTime() + frameData.deltaTime >= playable.GetDuration())
            {
                _cutsceneSystem.ResetGameControlsModeOverride();
            }
        }

        public override void OnGraphStop(Playable playable)
        {
            base.OnGraphStop(playable);

            if (Application.isPlaying)
            {
                _cutsceneSystem.ResetGameControlsModeOverride();
            }
        }
    }
}