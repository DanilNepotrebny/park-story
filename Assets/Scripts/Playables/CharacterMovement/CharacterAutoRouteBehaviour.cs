﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Characters;
using DreamTeam.Location.Routing;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Playables;

namespace DreamTeam.Playables.CharacterMovement
{
    public class CharacterAutoRouteBehaviour : BaseTimelineClipBehaviour
    {
        private Transform _startLocation;
        private Transform _endLocation;

        public override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            // Working in editor mode is not supported yet.
            if (!Application.isPlaying)
            {
                return;
            }

            CharacterAutoRouteClipAsset clipAsset = GetClipAsset<CharacterAutoRouteClipAsset>();

            IExposedPropertyTable resolver = playable.GetGraph().GetResolver();
            _startLocation = clipAsset.StartLocation.Resolve(resolver);
            _endLocation = clipAsset.EndLocation.Resolve(resolver);

            Character boundObject = GetBoundObject<Character>();

            if (_startLocation != null)
            {
                boundObject.transform.position = _startLocation.position;
            }

            if (_endLocation != null)
            {
                IRoute route;
                if (boundObject.RouteFinder.Find(_endLocation.position, out route))
                {
                    boundObject.RouteMover.MoveByPath(route, (float)playable.GetDuration());
                }
                else
                {
                    UnityEngine.Debug.LogError($"Cannot find path to location \"{_endLocation.name}\"!");
                }
            }
            else
            {
                UnityEngine.Debug.LogError("End location is not specified.");
            }
        }
    }
}