﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Location.Routing;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.CharacterMovement
{
    [Serializable]
    public abstract class CharacterManualRouteClipAssetBase :
        BaseTimelineClipAsset<BaseTimelineMixerBehaviour, CharacterManualRouteBehaviour>,
        ITimelineClipAsset
    {
        public abstract bool UseCurrentPosition { get; }
        public abstract IRoute GetRoute(IExposedPropertyTable resolver);

        public ClipCaps clipCaps => ClipCaps.SpeedMultiplier;
    }
}