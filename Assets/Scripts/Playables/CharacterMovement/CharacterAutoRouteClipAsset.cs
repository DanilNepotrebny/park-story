﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.CharacterMovement
{
    [Serializable]
    public class CharacterAutoRouteClipAsset : BaseTimelineClipAsset<BaseTimelineMixerBehaviour, CharacterAutoRouteBehaviour>, ITimelineClipAsset
    {
        [SerializeField] private ExposedReference<Transform> _startLocation;
        [SerializeField] private ExposedReference<Transform> _endLocation;

        public ExposedReference<Transform> StartLocation => _startLocation;
        public ExposedReference<Transform> EndLocation => _endLocation;

        public ClipCaps clipCaps => ClipCaps.SpeedMultiplier;
    }
}