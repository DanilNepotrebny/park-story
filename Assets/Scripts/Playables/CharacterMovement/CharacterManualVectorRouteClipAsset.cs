﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Location.Routing;
using UnityEngine;

namespace DreamTeam.Playables.CharacterMovement
{
    [Serializable]
    public class CharacterManualVectorRouteClipAsset : CharacterManualRouteClipAssetBase
    {
        [SerializeField] private bool _useCurrentPosition;
        [SerializeField] private Route _route;

        public override bool UseCurrentPosition => _useCurrentPosition;

        public override IRoute GetRoute(IExposedPropertyTable resolver)
        {
            return _route;
        }
    }
}