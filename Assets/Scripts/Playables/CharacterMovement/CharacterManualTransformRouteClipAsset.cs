﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Location.Routing;
using UnityEngine;

namespace DreamTeam.Playables.CharacterMovement
{
    [Serializable]
    public class CharacterManualTransformRouteClipAsset : CharacterManualRouteClipAssetBase
    {
        [SerializeField] private bool _useCurrentPosition;
        [SerializeField] private List<TransformExposedReference> _route;
        
        public override bool UseCurrentPosition => _useCurrentPosition;

        public override IRoute GetRoute(IExposedPropertyTable resolver)
        {
            List<Vector3> points = new List<Vector3>();
            foreach (TransformExposedReference reference in _route)
            {
                Transform transform = reference.Reference.Resolve(resolver);
                points.Add(transform.position);
            }

            return new DynamicRoute(points);
        }

        [Serializable]
        private struct TransformExposedReference
        {
            [SerializeField] private ExposedReference<Transform> _reference;

            public ExposedReference<Transform> Reference => _reference;
        }
    }
}