﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Location.Characters;
using DreamTeam.Location.Routing;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Playables;

namespace DreamTeam.Playables.CharacterMovement
{
    public class CharacterManualRouteBehaviour : BaseTimelineClipBehaviour
    {
        private CharacterManualRouteClipAssetBase _clipAsset;
        private CharacterRouteMover _boundObject;

        public override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            if (!Application.isPlaying)
            {
                return;
            }

            _clipAsset = GetClipAsset<CharacterManualRouteClipAssetBase>();
            _boundObject = GetBoundObject<CharacterRouteMover>();
            IExposedPropertyTable resolver = playable.GetGraph().GetResolver();

            if (_boundObject == null)
            {
                UnityEngine.Debug.LogError("Bound object is not set.");
                return;
            }

            IRoute route = _clipAsset.GetRoute(resolver);
            if (route == null)
            {
                UnityEngine.Debug.LogError("Path not specified or is not valid.");
                return;
            }

            if (_clipAsset.UseCurrentPosition)
            {
                List<Vector3> points = new List<Vector3>
                {
                    _boundObject.transform.position
                };

                for (int i = 0; i < route.PointsCount; i++)
                {
                    points.Add(route[i]);
                }

                route = new DynamicRoute(points);
            }

            if (route.PointsCount > 1)
            {
                _boundObject.MoveByPath(route, (float)playable.GetDuration());
            }
            else
            {
                UnityEngine.Debug.LogError("Path not specified or is not valid.");
            }
        }
    }
}