﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Characters;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.CharacterMovement
{
    [TrackClipType(typeof(CharacterManualVectorRouteClipAsset))]
    [TrackClipType(typeof(CharacterManualTransformRouteClipAsset))]
    [TrackBindingType(typeof(CharacterRouteMover))]
    public class CharacterManualRouteTrack : BaseTimelineTrackAsset<BaseTimelineMixerBehaviour, CharacterManualRouteBehaviour>
    {
    }
}