﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Analytics;
using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Playables;
using Zenject;

namespace DreamTeam.Playables.TutorialEvent
{
    [Serializable]
    public class TutorialEventClipBehaviour : BaseTimelineClipBehaviour
    {
        [Inject] private AnalyticsSystem _analyticsSystem;
        [Inject] private Instantiator _instantiator;

        public override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPlay(playable, frameData);

            if (Application.isPlaying)
            {
                var clip = GetClipAsset<TutorialEventClipAsset>();
                _analyticsSystem.Send(_instantiator.Instantiate<Analytics.Events.Global.Tutorial.TutorialEvent>(clip.Step));
            }
        }
    }
}