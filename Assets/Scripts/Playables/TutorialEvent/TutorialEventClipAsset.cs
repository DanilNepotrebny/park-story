﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Analytics.Events.Global.Tutorial;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.TutorialEvent
{
    [Serializable]
    public class TutorialEventClipAsset :
        BaseTimelineClipAsset<BaseTimelineMixerBehaviour, TutorialEventClipBehaviour>,
        ITimelineClipAsset
    {
        [SerializeField] private TutorialStep _step;

        public ClipCaps clipCaps => ClipCaps.None;

        public TutorialStep Step => _step;
    }
}