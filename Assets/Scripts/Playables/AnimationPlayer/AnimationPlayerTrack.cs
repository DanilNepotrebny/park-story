﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.AnimationPlayer
{
    [TrackColor(0f, 0.3972041f, 0.8161765f)]
    [TrackClipType(typeof(AnimationPlayerClipAsset))]
    [TrackBindingType(typeof(Animator))]
    public class AnimationPlayerTrack : BaseTimelineTrackAsset<AnimationPlayerMixerBehaviour, AnimationPlayerBehaviour>
    {
    }
}