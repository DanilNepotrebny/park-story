﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine.Animations;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.AnimationPlayer
{
    [Serializable]
    public class AnimationPlayerBehaviour : BaseTimelineClipBehaviour
    {
        private double _stateTime;
        private State _state;
        private double _clipTime;

        private AnimationPlayerClipAsset _clipAsset => GetClipAsset<AnimationPlayerClipAsset>();

        private bool _isFirstFrame;

        public float Weight { get; private set; }
        public TimelineClip TimelineClip => _clipAsset.TimelineClip;

        public AnimationClipPlayable CreateAnimationClipPlayable(PlayableGraph graph)
        {
            return AnimationClipPlayable.Create(graph, _clipAsset.AnimationClip);
        }

        public override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPlay(playable, frameData);

            _isFirstFrame = true;

            SetState(State.MixingIn, Playable.GetTime());
        }

        public override void OnBehaviourPause(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPause(playable, frameData);

            Weight = 0f;
        }

        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            base.ProcessFrame(playable, info, playerData);

            if (_state == State.NotPlaying)
            {
                return;
            }

            // This is imitation of Playable.GetTime() update: time delta is skipped on the first frame.
            if (!_isFirstFrame)
            {
                _stateTime += info.deltaTime;
                _clipTime += info.deltaTime;
            }
            else
            {
                _isFirstFrame = false;
            }

            Weight = Update();
        }

        private float Update()
        {
            // Update current state by playable time. Will not change state if timeline is paused.
            UpdateState(false);

            if (!_clipAsset.AnimationClip.isLooping)
            {
                // Update current state ignoring timeline pause.
                UpdateState(true);
            }

            return CalculateWeight();
        }

        private void UpdateState(bool ignorePause)
        {
            double time = ignorePause ? _clipTime : Playable.GetTime();
            double maxDuration = ignorePause ? _clipAsset.AnimationClip.averageDuration : TimelineClip.duration;

            double offset = GetStateExitTime(maxDuration) - time;
            if (offset < 0f)
            {
                SetState(GetNextState(), -offset);
            }
        }

        private double GetStateExitTime(double maxDuration)
        {
            switch (_state)
            {
                case State.MixingIn:
                    return TimelineClip.mixInDuration;

                case State.Playing:
                    return maxDuration - TimelineClip.mixOutDuration;

                case State.MixingOut:
                    return maxDuration;
            }

            throw new InvalidOperationException("Unexpected state: " + _state);
        }

        private State GetNextState()
        {
            switch (_state)
            {
                case State.MixingIn:
                    return State.Playing;

                case State.Playing:
                    return State.MixingOut;

                case State.MixingOut:
                    return State.NotPlaying;
            }

            throw new InvalidOperationException("Unexpected state: " + _state);
        }

        private float CalculateWeight()
        {
            switch (_state)
            {
                case State.MixingIn:
                    return TimelineClip.EvaluateMixIn(_stateTime + TimelineClip.start);

                case State.Playing:
                    return 1f;

                case State.MixingOut:
                    return TimelineClip.EvaluateMixOut(TimelineClip.end - TimelineClip.mixOutDuration + _stateTime);
            }

            return 0f;
        }

        private void SetState(State state, double timeOffset = 0f)
        {
            _state = state;
            _stateTime = timeOffset;
        }

        #region Inner types

        private enum State
        {
            NotPlaying,
            MixingIn,
            Playing,
            MixingOut
        }

        #endregion
    }
}