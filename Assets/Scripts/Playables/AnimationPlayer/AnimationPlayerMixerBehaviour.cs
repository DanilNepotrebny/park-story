﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace DreamTeam.Playables.AnimationPlayer
{
    public class AnimationPlayerMixerBehaviour : BaseTimelineMixerBehaviour
    {
        private Animator _boundObject;
        private PlayableGraph _graph;
        private AnimationMixerPlayable _mixer;

        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            CreateAnimationGraph(playable);
        }

        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            DestroyAnimationGraph();
        }

        public override void OnGraphStop(Playable playable)
        {
            DestroyAnimationGraph();
        }

        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            if (!_mixer.IsValid())
            {
                return;
            }

            int inputCount = _mixer.GetInputCount();
            for (int i = 0; i < inputCount; i++)
            {
                float weight;
                Playable p = _mixer.GetInput(i);
                AnimationPlayerBehaviour behaviour = GetBehaviour(playable, i);

                if (!Application.isPlaying)
                {
                    weight = playable.GetInputWeight(i);

                    double rootTime = playable.GetGraph().GetRootPlayable(0).GetTime();

                    p.SetTime(behaviour.TimelineClip.ToLocalTime(rootTime));
                }
                else
                {
                    weight = behaviour.Weight;

                    p.SetSpeed(weight > 0 ? 1f : 0f);
                }

                _mixer.SetInputWeight(i, weight);
            }
        }

        private void CreateAnimationGraph(Playable playable)
        {
            _boundObject = GetBoundObject<Animator>();

            if (_boundObject == null)
            {
                return;
            }

            int inputCount = playable.GetInputCount();

            _mixer = AnimationPlayableUtilities.PlayMixer(_boundObject, inputCount, out _graph);

            for (int i = 0; i < inputCount; i++)
            {
                AnimationPlayerBehaviour input = GetBehaviour(playable, i);
                AnimationClipPlayable clipPlayable = input.CreateAnimationClipPlayable(_graph);
                clipPlayable.SetSpeed(0);
                _mixer.ConnectInput(i, clipPlayable, 0);
            }

            #if UNITY_EDITOR
            {
                GraphVisualizerClient.Show(_graph, "Player");
            }
            #endif
        }

        private AnimationPlayerBehaviour GetBehaviour(Playable playable, int index)
        {
            var inputPlayable = (ScriptPlayable<AnimationPlayerBehaviour>)playable.GetInput(index);
            return inputPlayable.GetBehaviour();
        }

        private void DestroyAnimationGraph()
        {
            if (_graph.IsValid())
            {
                _graph.Destroy();
            }
        }
    }
}