﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.AnimationPlayer
{
    [Serializable]
    public class AnimationPlayerClipAsset : BaseTimelineClipAsset<AnimationPlayerMixerBehaviour, AnimationPlayerBehaviour>, ITimelineClipAsset
    {
        [SerializeField] private AnimationClip _animationClip;

        public ClipCaps clipCaps =>
            ClipCaps.Looping |
            ClipCaps.Extrapolation |
            ClipCaps.SpeedMultiplier |
            ClipCaps.Blending;

        public AnimationClip AnimationClip => _animationClip;

        public override double duration
        {
            get
            {
                if (_animationClip != null)
                {
                    return _animationClip.length;
                }

                return PlayableBinding.DefaultDuration;
            }
        }
    }
}