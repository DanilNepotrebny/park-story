﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.InputLock;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Playables;
using Zenject;

namespace DreamTeam.Playables.Tutorial
{
    public abstract class BaseTutorialBehaviour : BaseTimelineClipBehaviour
    {
        [Inject] private InputLockSystem _inputLockSystem;

        private bool _isFinished = true;
        private IDisposable _inputLockhandle;

        public sealed override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPlay(playable, frameData);

            if (Application.isPlaying)
            {
                _isFinished = false;

                _inputLockhandle = _inputLockSystem.LockTargets(GetInputTargets());

                OnStart();
            }
        }

        public sealed override void OnBehaviourPause(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPause(playable, frameData);

            if (Application.isPlaying &&
                playable.GetTime() + frameData.deltaTime >= playable.GetDuration())
            {
                Finish();
            }
        }

        public override void OnGraphStop(Playable playable)
        {
            base.OnGraphStop(playable);

            if (Application.isPlaying)
            {
                Finish();
            }
        }

        protected abstract IEnumerable<Component> GetInputTargets();

        protected virtual void OnStart()
        {
            // nothing to do here
        }

        protected virtual void OnFinish()
        {
            // nothing to do here
        }

        private void Finish()
        {
            if (!_isFinished)
            {
                _isFinished = true;

                if (_inputLockhandle != null)
                {
                    _inputLockhandle.Dispose();
                    _inputLockhandle = null;
                }

                OnFinish();
            }
        }
    }
}