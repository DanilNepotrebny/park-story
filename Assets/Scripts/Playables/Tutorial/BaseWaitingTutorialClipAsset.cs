﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Mirroring;
using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.Playables.Tutorial.Presenters;
using UnityEngine;

namespace DreamTeam.Playables.Tutorial
{
    public class BaseWaitingTutorialClipAsset<TWaitBehavior> :
        BaseTutorialClipAsset<TWaitBehavior>,
        IWaitingTutorialClipAsset
        where TWaitBehavior : BaseTimelineClipBehaviour, new()
    {
        [SerializeField, MirrorId] private string _pointerPresenterMirrorId;
        [SerializeField] private ExposedReference<TutorialHintPresenter> _hint;
        [SerializeField, Tooltip("In this mode showing hint will be selected between Hint and HintBottom according " +
             "to target element position on screen. In this case Hint is the top one")]
        private bool _hintTopBottomMode;
        [SerializeField] private ExposedReference<TutorialHintPresenter> _hintBottom;
        [SerializeField] private bool _isHighlightEnabled = true;

        public string PointerPresenterMirrorId => _pointerPresenterMirrorId;
        public ExposedReference<TutorialHintPresenter> Hint => _hint;
        public bool HintTopBottomMode => _hintTopBottomMode;
        public ExposedReference<TutorialHintPresenter> HintBottom => _hintBottom;
        public bool IsHighlightEnabled => _isHighlightEnabled;
    }

    public interface IWaitingTutorialClipAsset
    {
        string PointerPresenterMirrorId { get; }
        ExposedReference<TutorialHintPresenter> Hint { get; }
        bool HintTopBottomMode { get; }
        ExposedReference<TutorialHintPresenter> HintBottom { get; }
        bool IsHighlightEnabled { get; }
    }
}
