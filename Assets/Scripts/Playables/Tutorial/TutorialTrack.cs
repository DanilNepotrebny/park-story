﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.Tutorial
{
    [TrackColor(0.9960784f, 0.8039216f, 0f)]
    [TrackClipType(typeof(WaitButtonClickClipAsset))]
    [TrackClipType(typeof(WaitToggleChangedClipAsset))]
    [TrackClipType(typeof(WaitGestureClipAsset))]
    [TrackClipType(typeof(BlockInputClipAsset))]
    public partial class TutorialTrack : BaseTimelineTrackAsset<BaseTimelineMixerBehaviour, BaseTimelineClipBehaviour>
    {
        [SerializeField, SortingLayer] private int _sortingLayer;

        public int SortingLayer => _sortingLayer;
    }
}