﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Match3.Wall;
using DreamTeam.Playables.Tutorial.Presenters;
using DreamTeam.Tutorial;
using DreamTeam.Utils;
using DreamTeam.Utils.RendererSorting;
using UnityEngine;
using Zenject;

namespace DreamTeam.Playables.Tutorial.Match3
{
    public class HighlightWallsBehaviour :
        BaseWaitingTutorialBehaviour<TutorialExclusiveTapController, HighlightWallsClipAsset>
    {
        [Inject] private Instantiator _instantiator;
        [Inject] private TutorialExclusiveTapController _tapController;
        [Inject] private WallSystem _wallSystem;

        protected override IEnumerable<TutorialExclusiveTapController> WaitingObjects
        {
            get { yield return _tapController; }
        }

        protected override IEnumerable<Component> GetInputTargets()
        {
            yield return _tapController.GetInputTarget();
        }

        protected override void InitializeHighlightPresenters(List<ITutorialPresenter> presenters)
        {
            base.InitializeHighlightPresenters(presenters);

            var highlightedObjects = new List<ISortingOptionsProvider>();
            _wallSystem.ForEachWall(wall => highlightedObjects.Add(wall.Presenter));

            ITutorialPresenter presenter = _instantiator.Instantiate<TutorialHighlightObjectsPresenter>(
                    highlightedObjects,
                    ((TutorialTrack)TrackAsset).SortingLayer
                );
            presenters.Add(presenter);
        }

        protected override void SubscribeForInput(TutorialExclusiveTapController tapController)
        {
            tapController.gameObject.SetActive(true);
            tapController.OnTapped += OnTapped;
        }

        protected override void UnsubscribeFromInput(TutorialExclusiveTapController tapController)
        {
            tapController.OnTapped -= OnTapped;
            tapController.gameObject.SetActive(false);
        }

        private void OnTapped()
        {
            OnActionCompleted();
        }
    }
}