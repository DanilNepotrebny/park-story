﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.ChipTags;
using UnityEngine;

namespace DreamTeam.Playables.Tutorial.Match3
{
    public class WaitForActivationMoveClipAsset : BaseWaitingTutorialClipAsset<WaitForActivationMoveBehaviour>
    {
        [SerializeField] private WaitForActivationMoveMode _mode;

        [Header("Manual")]
        [SerializeField] private Vector2Int _activationCellCoordinate;

        [Header("Dynamic")]
        [SerializeField] private ChipTag _activationChipTag;

        [Space]
        [SerializeField, Tooltip("Do additional cutouts in tutorial tint. Activating cell will be also highlighted")]
        private Vector2Int[] _highlightingCellsCoordinates;

        public WaitForActivationMoveMode Mode => _mode;
        public Vector2Int ActivationCellCoordinate => _activationCellCoordinate;
        public ChipTag ActivationChipTag => _activationChipTag;
        public Vector2Int[] HighlightingCellsCoordinates => _highlightingCellsCoordinates;
    }

    public enum WaitForActivationMoveMode
    {
        Manual,
        Dynamic
    }
}
