﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3;
using DreamTeam.Match3.Conveyor;
using DreamTeam.Playables.Tutorial.Match3.Presenters;
using DreamTeam.Playables.Tutorial.Presenters;
using DreamTeam.Tutorial;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Playables.Tutorial.Match3
{
    public class HighlightConveyorBehaviour :
        BaseWaitingTutorialBehaviour<TutorialExclusiveTapController, HighlightConveyorClipAsset>
    {
        [Inject] private Instantiator _instantiator;
        [Inject] private TutorialExclusiveTapController _tapController;
        [Inject] private GridSystem _gridSystem;
        [Inject] private ConveyorSystem _conveyorSystem;

        protected override IEnumerable<TutorialExclusiveTapController> WaitingObjects
        {
            get { yield return _tapController; }
        }

        protected override IEnumerable<Component> GetInputTargets()
        {
            yield return _tapController.GetInputTarget();
        }

        protected override void InitializeHighlightPresenters(List<ITutorialPresenter> presenters)
        {
            base.InitializeHighlightPresenters(presenters);

            List<Vector2Int> cells = _gridSystem
                .Cells
                .Where(cell => _conveyorSystem.HasAnyBlock(cell))
                .Select(cell => cell.GridPosition)
                .ToList();
            presenters.Add(_instantiator.Instantiate<TutorialHighlightCellsPresenter>(cells));
        }

        protected override void SubscribeForInput(TutorialExclusiveTapController tapController)
        {
            tapController.gameObject.SetActive(true);
            tapController.OnTapped += OnTapped;
        }

        protected override void UnsubscribeFromInput(TutorialExclusiveTapController tapController)
        {
            tapController.OnTapped -= OnTapped;
            tapController.gameObject.SetActive(false);
        }

        private void OnTapped()
        {
            OnActionCompleted();
        }
    }
}