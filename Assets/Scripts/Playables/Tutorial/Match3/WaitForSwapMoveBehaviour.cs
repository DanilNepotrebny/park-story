﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3;
using DreamTeam.Playables.Tutorial.Match3.Presenters;
using DreamTeam.Playables.Tutorial.Presenters;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Playables.Tutorial.Match3
{
    // TODO: Refactor and collapse with HighlightCellBehaviour
    public class WaitForSwapMoveBehaviour :
        BaseWaitingTutorialBehaviour<MovesSystem, WaitForSwapMoveClipAsset>,
        ISwapRestriction
    {
        [Inject] private MovesSystem _movesSystem;
        [Inject] private SwapSystem _swapSystem;
        [Inject] private GridSystem _gridSystem;
        [Inject] private Instantiator _instantiator;

        private CellComponent _moveSourceCell => _gridSystem.GetCell(Clip.MoveCellsCoordinates[0]);
        private CellComponent _moveTargetCell => _gridSystem.GetCell(Clip.MoveCellsCoordinates[1]);

        protected override IEnumerable<MovesSystem> WaitingObjects
        {
            get { yield return _movesSystem; }
        }

        protected override IEnumerable<Component> GetInputTargets()
        {
            foreach (Vector2Int cellPos in Clip.MoveCellsCoordinates)
            {
                CellComponent cell = _gridSystem.GetCell(cellPos);
                Assert.IsNotNull(cell, $"Passed cell at {cellPos} is null");
                yield return cell.Presenter.SwapGesture;
            }
        }

        protected override void InitializeHighlightPresenters(List<ITutorialPresenter> presenters)
        {
            base.InitializeHighlightPresenters(presenters);

            var chipTagCells = new List<Vector2Int>();
            if (Clip.HighlightChipTag != null)
            {
                chipTagCells = _gridSystem
                    .Cells
                    .Where(cell => !cell.IsEmpty && cell.Chip.Tags.Has(Clip.HighlightChipTag, false))
                    .Select(cell => cell.GridPosition)
                    .ToList();
            }

            var presenter = _instantiator.Instantiate<TutorialHighlightCellsPresenter>(
                    Clip.HighlightingCellsCoordinates.Union(chipTagCells).ToList()
                );

            presenters.Add(presenter);

            var move = new MovesSystem.Move
            {
                Type = MovesSystem.MoveType.Swap,
                Source = _moveSourceCell,
                Target = _moveTargetCell,
                SourceMatch = Clip
                    .HighlightingCellsCoordinates
                    .Select(coord => _gridSystem.GetCell(coord))
                    .Except(new[] { _moveSourceCell, _moveTargetCell })
                    .Select(cell => cell.Chip)
                    .ToList()
            };
            presenters.Add(_instantiator.Instantiate<TutorialMoveSuggestionPresenter>(move));
        }

        protected override void SubscribeForInput(MovesSystem movesSystem)
        {
            movesSystem.MoveSucceeding += OnMoveSucceeding;
            _swapSystem.RegisterSwapRestriction(this);
        }

        protected override void UnsubscribeFromInput(MovesSystem movesSystem)
        {
            _swapSystem.UnregisterSwapRestriction(this);
            movesSystem.MoveSucceeding -= OnMoveSucceeding;
        }

        private void OnMoveSucceeding(List<Vector2Int> cellsPositions)
        {
            OnActionCompleted();
        }

        #region ISwapRestriction

        public bool CanBeSwapped(CellComponent from, CellComponent to)
        {
            return (_moveSourceCell == from && _moveTargetCell == to) ||
                (_moveTargetCell == from && _moveSourceCell == to);
        }

        #endregion ISwapRestriction
    }
}