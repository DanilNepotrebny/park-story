﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Playables.Tutorial.Match3.Presenters;
using DreamTeam.Playables.Tutorial.Presenters;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Playables.Tutorial.Match3
{
    // TODO: Refactor and collapse with HighlightCellBehaviour
    public class WaitForActivationMoveBehaviour :
        BaseWaitingTutorialBehaviour<MovesSystem, WaitForActivationMoveClipAsset>
    {
        [Inject] private MovesSystem _movesSystem;
        [Inject] private GridSystem _gridSystem;
        [Inject] private Instantiator _instantiator;

        private CellComponent _activatingCell
        {
            get
            {
                switch (Clip.Mode)
                {
                    case WaitForActivationMoveMode.Manual:
                        return _gridSystem.GetCell(Clip.ActivationCellCoordinate);
                    case WaitForActivationMoveMode.Dynamic:
                        return GetCellsWithChipTag(Clip.ActivationChipTag).First();
                    default:
                        throw new ArgumentOutOfRangeException(nameof(Clip.Mode), Clip.Mode, "Unhandled Mode type");
                }
            }
        }

        protected override IEnumerable<MovesSystem> WaitingObjects
        {
            get { yield return _movesSystem; }
        }

        protected override IEnumerable<Component> GetInputTargets()
        {
            yield return _activatingCell.Presenter.DoubleTapGesture;
        }

        protected override void InitializeHighlightPresenters(List<ITutorialPresenter> presenters)
        {
            base.InitializeHighlightPresenters(presenters);

            var presenter = _instantiator.Instantiate<TutorialHighlightCellsPresenter>(
                    Clip.HighlightingCellsCoordinates.Union(new[] { _activatingCell.GridPosition }).ToList()
                );

            presenters.Add(presenter);

            var move = new MovesSystem.Move
            {
                Type = MovesSystem.MoveType.Activation,
                Source = _activatingCell,
                SourceMatch = new List<ChipComponent>()
            };
            presenters.Add(_instantiator.Instantiate<TutorialMoveSuggestionPresenter>(move));
        }

        protected override void SubscribeForInput(MovesSystem waitingObject)
        {
            waitingObject.MoveSucceeding += OnMoveSucceeding;
        }

        protected override void UnsubscribeFromInput(MovesSystem waitingObject)
        {
            waitingObject.MoveSucceeding -= OnMoveSucceeding;
        }

        private void OnMoveSucceeding(List<Vector2Int> cellsPositions)
        {
            OnActionCompleted();
        }

        private List<CellComponent> GetCellsWithChipTag([NotNull] ChipTag chipTag)
        {
            return _gridSystem
                .Cells
                .Where(cell => !cell.IsEmpty && cell.Chip.Tags.Has(chipTag, false))
                .ToList();
        }
    }
}