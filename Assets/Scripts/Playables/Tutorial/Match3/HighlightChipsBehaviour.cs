﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Playables.Tutorial.Match3.Presenters;
using DreamTeam.Playables.Tutorial.Presenters;
using DreamTeam.Tutorial;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace DreamTeam.Playables.Tutorial.Match3
{
    public class HighlightChipsBehaviour :
        BaseWaitingTutorialBehaviour<TutorialExclusiveTapController, HighlightChipsClipAsset>
    {
        [Inject] private Instantiator _instantiator;
        [Inject] private TutorialExclusiveTapController _tapController;
        [Inject] private GridSystem _gridSystem;

        protected override IEnumerable<TutorialExclusiveTapController> WaitingObjects
        {
            get { yield return _tapController; }
        }

        protected override IEnumerable<Component> GetInputTargets()
        {
            yield return _tapController.GetInputTarget();
        }

        protected override void InitializeHighlightPresenters(List<ITutorialPresenter> presenters)
        {
            base.InitializeHighlightPresenters(presenters);

            List<Vector2Int> cells = GetCellsWithChipTag(Clip.ChipTag);
            presenters.Add(_instantiator.Instantiate<TutorialHighlightCellsPresenter>(cells));
        }

        protected override TutorialHintPresenter SelectHintToShow(
            TutorialHintPresenter hintTop,
            TutorialHintPresenter hintBottom)
        {
            int cellsOnGridTop = 0;
            int cutoutValue = _gridSystem.Height / 2;
            List<Vector2Int> cells = GetCellsWithChipTag(Clip.ChipTag);
            foreach (Vector2Int cellCoordinate in cells)
            {
                cellsOnGridTop += (cellCoordinate.y >= cutoutValue) ? 1 : -1;
            }

            return (cellsOnGridTop > 0) ? hintBottom : hintTop;
        }

        protected override void SubscribeForInput(TutorialExclusiveTapController tapController)
        {
            tapController.gameObject.SetActive(true);
            tapController.OnTapped += OnTapped;
        }

        protected override void UnsubscribeFromInput(TutorialExclusiveTapController tapController)
        {
            tapController.OnTapped -= OnTapped;
            tapController.gameObject.SetActive(false);
        }

        private void OnTapped()
        {
            OnActionCompleted();
        }

        private List<Vector2Int> GetCellsWithChipTag([NotNull] ChipTag chipTag)
        {
            return _gridSystem
                .Cells
                .Where(cell => !cell.IsEmpty && cell.Chip.Tags.Has(chipTag, false))
                .Select(cell => cell.GridPosition)
                .ToList();
        }
    }
}