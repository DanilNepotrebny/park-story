﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.ChipTags;
using UnityEngine;

namespace DreamTeam.Playables.Tutorial.Match3
{
    public class WaitForSwapMoveClipAsset :
        BaseWaitingTutorialClipAsset<WaitForSwapMoveBehaviour>
    {
        [SerializeField] private Vector2Int[] _moveCellsCoordinates;
        [SerializeField] private Vector2Int[] _highlightingCellsCoordinates;
        [SerializeField, Tooltip("Cells with this chip tag will be additionally highlighted")] 
        private ChipTag _highlightChipTag;

        public Vector2Int[] MoveCellsCoordinates => _moveCellsCoordinates;
        public Vector2Int[] HighlightingCellsCoordinates => _highlightingCellsCoordinates;
        public ChipTag HighlightChipTag => _highlightChipTag;
    }
}
