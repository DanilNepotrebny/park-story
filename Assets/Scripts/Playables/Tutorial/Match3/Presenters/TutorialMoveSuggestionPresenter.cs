﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using DreamTeam.Playables.Tutorial.Presenters;
using Zenject;

namespace DreamTeam.Playables.Tutorial.Match3.Presenters
{
    public class TutorialMoveSuggestionPresenter : ITutorialPresenter
    {
        [Inject(Id = Match3Installer.TutorialMovePointerPresenterId)]
        private TutorialMovePointerPresenter _movePointer;

        [Inject] private AutoHintSystem _autoHintSystem;

        private readonly MovesSystem.Move _move;

        public TutorialMoveSuggestionPresenter(MovesSystem.Move move)
        {
            _move = move;
        }

        void ITutorialPresenter.OnStart()
        {
            _movePointer.PlayAnimation(_move);
            _autoHintSystem.OverrideAndPlayMove(_move);
        }

        void ITutorialPresenter.OnFinish()
        {
            _autoHintSystem.ResetMoveOverride();
            _movePointer.StopAnimation();
        }

        void ITutorialPresenter.OnActionCompleted()
        {
            _autoHintSystem.ResetMoveOverride();
            _movePointer.StopAnimation();
        }
    }
}
