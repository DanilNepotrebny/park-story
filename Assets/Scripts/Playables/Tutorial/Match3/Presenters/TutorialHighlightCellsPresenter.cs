﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Match3;
using DreamTeam.Playables.Tutorial.Presenters;
using UnityEngine;
using Zenject;

namespace DreamTeam.Playables.Tutorial.Match3.Presenters
{
    public class TutorialHighlightCellsPresenter : TutorialHighlightAreaPresenter
    {
        [Inject(Id = Match3Installer.TutorialCellCutoffPrefabId)]
        private GameObject _cellCutoffPrefab;

        [Inject] private GridSystem _gridSystem;

        private readonly IList<Vector2Int> _cellCoordinates;

        public TutorialHighlightCellsPresenter(IList<Vector2Int> cellCoordinates)
        {
            _cellCoordinates = cellCoordinates;
        }

        [Inject] private void Initialize()
        {
            CutoutDataList = new List<CutoutData>(_cellCoordinates.Count);
            foreach (Vector2Int cellPos in _cellCoordinates)
            {
                CellComponent cell = _gridSystem.GetCell(cellPos);
                if (cell == null)
                {
                    UnityEngine.Debug.LogWarning($"There is no cell at {cellPos}");
                    continue;
                }

                var cutoutData = new CutoutData
                {
                    Prefab = _cellCutoffPrefab,
                    Transform = cell.transform
                };
                CutoutDataList.Add(cutoutData);
            }
        }
    }
}
