﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using DreamTeam.Match3;
using DreamTeam.Utils;
using DreamTeam.Utils.Coroutines;
using JetBrains.Annotations;
using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Playables.Tutorial.Match3.Presenters
{
    public class TutorialMovePointerPresenter : MonoBehaviour
    {
        [SerializeField, RequiredField] private SkeletonGraphic _skeleton;
        [SerializeField, SpineAnimation] private string _appearAnimationName;
        [SerializeField, SpineAnimation] private string _disappearAnimationName;
        [SerializeField, SpineAnimation] private string _idleAnimationName;
        [SerializeField, SpineAnimation] private string _activationAnimationName;
        [SerializeField, Min(0)] private float _moveAnimationDuration = 1;
        [SerializeField, Min(0)] private float _appearAnimationDelay = 0;

        private MovesSystem.Move _move;
        private Coroutine _coroutine;
        private LTDescr _moveLTD;

        public void PlayAnimation([NotNull] MovesSystem.Move move)
        {
            if (!enabled)
            {
                UnityEngine.Debug.LogWarning(
                    $"{nameof(TutorialMovePointerPresenter)}::{nameof(PlayAnimation)}() call on disabled component");
                return;
            }

            StopAnimation();

            _move = move;
            Assert.IsNull(_coroutine, "_coroutine != null");
            _coroutine = StartCoroutine(PlayAnimationInternal());
        }

        public void StopAnimation()
        {
            if (!enabled)
            {
                UnityEngine.Debug.LogWarning(
                    $"{nameof(TutorialMovePointerPresenter)}::{nameof(StopAnimation)}() call on disabled component");
                return;
            }

            _skeleton.AnimationState?.ClearTracks();
            _skeleton.gameObject.SetActive(false);

            if (_moveLTD != null)
            {
                LeanTween.cancel(_moveLTD.id);
                _moveLTD = null;
            }

            if (_coroutine != null)
            {
                StopCoroutine(_coroutine);
                _coroutine = null;
            }
        }

        protected void OnDisable()
        {
            StopAnimation();
        }

        private IEnumerator PlayAnimationInternal()
        {
            Assert.IsNotNull(_move, "_move == null");

            while (true)
            {
                yield return new WaitForSeconds(_appearAnimationDelay);

                _skeleton.transform.position = _move.Source.Presenter.transform.position;
                _skeleton.gameObject.SetActive(true);

                TrackEntry trackEntry = _skeleton.AnimationState.SetAnimation(0, _appearAnimationName, false);

                yield return new WaitForEvent<TrackEntry, TrackEntry>(trackEntry, nameof(trackEntry.Complete));

                switch (_move.Type)
                {
                    case MovesSystem.MoveType.Swap:
                        yield return StartCoroutine(PlaySwapMoveAnimation(
                            _move.Source.Presenter.transform.position,
                            _move.Target.Presenter.transform.position));
                        break;
                    case MovesSystem.MoveType.Activation:
                        yield return PlayActivationMoveAnimation();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(_move.Type), _move.Type, "Unhandled enum type");
                }

                trackEntry = _skeleton.AnimationState.SetAnimation(0, _disappearAnimationName, false);

                yield return new WaitForEvent<TrackEntry, TrackEntry>(trackEntry, nameof(trackEntry.Complete));
            }
        }

        private IEnumerator PlaySwapMoveAnimation(Vector3 from, Vector3 to)
        {
            _skeleton.AnimationState.SetAnimation(0, _idleAnimationName, true);

            _skeleton.gameObject.transform.position = from;
            _moveLTD = LeanTween.move(_skeleton.gameObject, to, _moveAnimationDuration);

            yield return new WaitUntil(() => !LeanTween.isTweening(_moveLTD.id));

            _moveLTD = null;
        }

        private IEnumerator PlayActivationMoveAnimation()
        {
            TrackEntry trackEntry = _skeleton.AnimationState.SetAnimation(0, _activationAnimationName, false);

            yield return new WaitForEvent<TrackEntry, TrackEntry>(trackEntry, nameof(trackEntry.Complete));
        }
    }
}
