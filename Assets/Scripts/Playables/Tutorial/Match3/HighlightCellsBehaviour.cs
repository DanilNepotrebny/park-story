﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Playables.Tutorial.Match3.Presenters;
using DreamTeam.Playables.Tutorial.Presenters;
using DreamTeam.Tutorial;
using DreamTeam.Utils;
using UnityEngine;
using Zenject;

namespace DreamTeam.Playables.Tutorial.Match3
{
    public class HighlightCellsBehaviour :
        BaseWaitingTutorialBehaviour<TutorialExclusiveTapController, HighlightCellsClipAsset>
    {
        [Inject] private TutorialExclusiveTapController _tapController;
        [Inject] private Instantiator _instantiator;

        protected override IEnumerable<TutorialExclusiveTapController> WaitingObjects
        {
            get { yield return _tapController; }
        }

        protected override IEnumerable<Component> GetInputTargets()
        {
            yield return _tapController.GetInputTarget();
        }

        protected override void InitializeHighlightPresenters(List<ITutorialPresenter> presenters)
        {
            base.InitializeHighlightPresenters(presenters);

            presenters.Add(
                    _instantiator.Instantiate<TutorialHighlightCellsPresenter>(Clip.HighlightingCellsCoordinates)
                );
        }

        protected override void SubscribeForInput(TutorialExclusiveTapController tapController)
        {
            tapController.gameObject.SetActive(true);
            tapController.OnTapped += OnTapped;
        }

        protected override void UnsubscribeFromInput(TutorialExclusiveTapController tapController)
        {
            tapController.OnTapped -= OnTapped;
            tapController.gameObject.SetActive(false);
        }

        private void OnTapped()
        {
            OnActionCompleted();
        }
    }
}