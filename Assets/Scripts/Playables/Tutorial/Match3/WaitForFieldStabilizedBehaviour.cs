﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Cutscenes;
using DreamTeam.Match3.FieldStabilization;
using DreamTeam.Utils.Invocation;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Playables.Tutorial.Match3
{
    public class WaitForFieldStabilizedBehaviour : BaseBlockInputBehaviour
    {
        [Inject] private FieldStabilizationSystem _fieldStabilizationSystem;
        [Inject] private CutsceneSystem _cutsceneSystem;

        private ActionDisposable _pauseCutsceneDisposable;

        protected override void OnStart()
        {
            base.OnStart();

            if (_fieldStabilizationSystem.IsFieldStable)
            {
                return;
            }

            Assert.IsNull(_pauseCutsceneDisposable, $"_pauseCutsceneDisposable != null");
            _pauseCutsceneDisposable = _cutsceneSystem.PauseCurrentCutscene();

            _fieldStabilizationSystem.AddStabilizationHandler(
                OnFieldStabilized,
                FieldStabilizationHandlerOrder.WaitForFieldStabilizedBehaviour);
        }

        protected override void OnFinish()
        {
            base.OnFinish();
            
            RemoveStabilizationHandler();
            ReleasePauseCutsceneDisposable();
        }

        private void OnFieldStabilized()
        {
            RemoveStabilizationHandler();
            ReleasePauseCutsceneDisposable();
        }

        private void RemoveStabilizationHandler()
        {
            _fieldStabilizationSystem.RemoveStabilizationHandler(OnFieldStabilized);
        }

        private void ReleasePauseCutsceneDisposable()
        {
            _pauseCutsceneDisposable?.Dispose();
            _pauseCutsceneDisposable = null;
        }
    }
}
