﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.MagicHat.Placing;
using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.Requirements.ChipConsumption;
using DreamTeam.Playables.Tutorial.Match3.Presenters;
using DreamTeam.Playables.Tutorial.Presenters;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Playables.Tutorial.Match3
{
    public class HighlightMagicHatSuccessfulSwapBehaviour :
        BaseWaitingTutorialBehaviour<MovesSystem, HighlightMagicHatSuccessfulSwapClipAsset>,
        ISwapRestriction
    {
        [Inject] private MovesSystem _movesSystem;
        [Inject] private GridSystem _gridSystem;
        [Inject] private SwapSystem _swapSystem;
        [Inject] private Instantiator _instantiator;

        [Inject(Id = LevelSettings.GoalInstancesId)]
        private IReadOnlyList<Requirement.Instance> _goals;

        private List<CellComponent> _magicHatMoveCellsBackingField;

        private IEnumerable<ChipConsumptionRequirement.Instance> _chipGoals =>
            _goals.OfType<ChipConsumptionRequirement.Instance>();

        private CellComponent MagicHatCell => MagicHatMoveCellCoordinates[0];
        private CellComponent MagicHatTargetCell => MagicHatMoveCellCoordinates[1];

        private List<CellComponent> MagicHatMoveCellCoordinates
        {
            get
            {
                if (_magicHatMoveCellsBackingField == null)
                {
                    _magicHatMoveCellsBackingField = GetMagicHatWithTargetCells();
                }

                return _magicHatMoveCellsBackingField;
            }
        }

        protected override IEnumerable<MovesSystem> WaitingObjects
        {
            get { yield return _movesSystem; }
        }

        protected override IEnumerable<Component> GetInputTargets()
        {
            foreach (CellComponent cell in MagicHatMoveCellCoordinates)
            {
                yield return cell.Presenter.SwapGesture;
            }
        }

        protected override void InitializeHighlightPresenters(List<ITutorialPresenter> presenters)
        {
            base.InitializeHighlightPresenters(presenters);

            var presenter = _instantiator.Instantiate<TutorialHighlightCellsPresenter>(
                    MagicHatMoveCellCoordinates.Select(cell => cell.GridPosition).ToList()
                );

            presenters.Add(presenter);

            var move = new MovesSystem.Move
            {
                Type = MovesSystem.MoveType.Swap,
                Source = MagicHatCell,
                Target = MagicHatTargetCell,
                SourceMatch = new List<ChipComponent>(),
                TargetMatch = new List<ChipComponent>()
            };
            presenters.Add(_instantiator.Instantiate<TutorialMoveSuggestionPresenter>(move));
        }

        protected override void SubscribeForInput(MovesSystem movesSystem)
        {
            _swapSystem.RegisterSwapRestriction(this);
            movesSystem.MoveSucceeding += OnMoveSucceeding;
        }

        protected override void UnsubscribeFromInput(MovesSystem movesSystem)
        {
            movesSystem.MoveSucceeding -= OnMoveSucceeding;
            _swapSystem.UnregisterSwapRestriction(this);
        }

        protected override TutorialHintPresenter SelectHintToShow(
            TutorialHintPresenter hintTop,
            TutorialHintPresenter hintBottom)
        {
            List<CellComponent> cells = GetMagicHatWithTargetCells();
            CellComponent magicHatCell = cells[0];
            CellComponent targetCell = cells[1];
            Assert.IsTrue(magicHatCell.Equals(GetCellWithChip(Clip.MagicHatChipTag)));

            int cutoutValue = _gridSystem.Height / 2;
            if (magicHatCell.GridPosition.y == cutoutValue)
            {
                return (magicHatCell.GridPosition.y > targetCell.GridPosition.y) ? hintTop : hintBottom;
            }

            return (magicHatCell.GridPosition.y > cutoutValue) ? hintBottom : hintTop;
        }

        private void OnMoveSucceeding(List<Vector2Int> cellsPositions)
        {
            OnActionCompleted();
        }

        private CellComponent GetCellWithChip([NotNull] ChipTag chipTag)
        {
            CellComponent chipCell = _gridSystem
                .Cells
                .First(cell => !cell.IsEmpty && cell.Chip.Tags.Has(chipTag, false));

            Assert.IsNotNull(chipCell, $"Didn't find a cell with {chipTag.name}");
            return chipCell;
        }

        private List<CellComponent> GetMagicHatWithTargetCells()
        {
            CellComponent cellToSwap = null;
            CellComponent magicHatCell = GetCellWithChip(Clip.MagicHatChipTag);
            foreach (ChipConsumptionRequirement.Instance goal in _chipGoals)
            {
                cellToSwap = NearRequiredChipMagicHatPlacingRule.FindValidRequirementCellAround(
                    magicHatCell,
                    goal,
                    _swapSystem);

                if (cellToSwap != null)
                {
                    break;
                }
            }

            if (cellToSwap == null)
            {
                cellToSwap = NearRequiredChipMagicHatPlacingRule.FindValidCellAround(magicHatCell, _swapSystem);
            }

            if (cellToSwap == null)
            {
                throw new NullReferenceException("Didn't find cell to swap with MagicHat");
            }

            return new List<CellComponent> { magicHatCell, cellToSwap };
        }

        #region ISwapRestriction

        public bool CanBeSwapped(CellComponent from, CellComponent to)
        {
            return (MagicHatCell == from && MagicHatTargetCell == to) ||
                (MagicHatTargetCell == from && MagicHatCell == to);
        }

        #endregion ISwapRestriction
    }
}