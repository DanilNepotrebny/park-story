﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Playables.Tutorial.Match3
{
    public class HighlightCellsClipAsset : BaseWaitingTutorialClipAsset<HighlightCellsBehaviour>
    {
        [SerializeField] private Vector2Int[] _highlightingCellsCoordinates;

        public Vector2Int[] HighlightingCellsCoordinates => _highlightingCellsCoordinates;
    }
}