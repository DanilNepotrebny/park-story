﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.ChipTags;
using UnityEngine;

namespace DreamTeam.Playables.Tutorial.Match3
{
    public class HighlightChipsClipAsset :
        BaseWaitingTutorialClipAsset<HighlightChipsBehaviour>
    {
        [SerializeField] private ChipTag _chipTag;

        public ChipTag ChipTag => _chipTag;
    }
}
