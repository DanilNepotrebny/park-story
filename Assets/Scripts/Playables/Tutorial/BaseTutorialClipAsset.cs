﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.Tutorial
{
    public abstract class BaseTutorialClipAsset<TWaitBehaviour> :
        BaseTimelineClipAsset<BaseTimelineMixerBehaviour, TWaitBehaviour>,
        ITimelineClipAsset
        where TWaitBehaviour : BaseTimelineClipBehaviour, new()
    {
        public ClipCaps clipCaps => ClipCaps.None;
    }
}
