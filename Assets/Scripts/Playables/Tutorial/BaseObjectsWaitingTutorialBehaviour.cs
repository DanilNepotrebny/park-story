﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Playables.Tutorial.Presenters;
using DreamTeam.Utils;
using DreamTeam.Utils.RendererSorting;
using UnityEngine;
using Zenject;

namespace DreamTeam.Playables.Tutorial
{
    public abstract class BaseObjectsWaitingTutorialBehaviour<TWaitingObject> :
        BaseWaitingTutorialBehaviour<TWaitingObject, IObjectsWaitingTutorialClipAsset>
        where TWaitingObject : Component
    {
        [Inject] private Instantiator _instantiator;

        private IEnumerable<TWaitingObject> _waitingObjectsCache;
        private IEnumerable<Component> _inputTargets;

        protected virtual bool IsHighlightManual => Clip.HaveObjectsToHighlight;

        protected override IEnumerable<TWaitingObject> WaitingObjects
        {
            get
            {
                if (_waitingObjectsCache == null)
                {
                    _waitingObjectsCache = ResolveMirrorInstances<TWaitingObject>(Clip.WaitingObjectIds);
                }
                return _waitingObjectsCache;
            }
        }

        protected override void InitializeHighlightPresenters(List<ITutorialPresenter> presenters)
        {
            base.InitializeHighlightPresenters(presenters);

            CreateHighlightObjects(presenters);
            CreateHighlightCutouts(presenters);
        }

        protected virtual IEnumerable<ISortingOptionsProvider> FindHighlightedObjects()
        {
            return null;
        }

        protected override IEnumerable<Component> GetInputTargets()
        {
            if (_inputTargets == null)
            {
                _inputTargets = SelectInputTargets();

                IEnumerable<Component> additionalInputTargets =
                    ResolveMirrorInstances<Component>(Clip.AdditionalInputTargetObjectIds);

                _inputTargets = _inputTargets.Union(additionalInputTargets);
            }
            return _inputTargets;
        }

        protected override void OnFinish()
        {
            base.OnFinish();

            _waitingObjectsCache = null;
            _inputTargets = null;
        }

        protected virtual IEnumerable<Component> SelectInputTargets()
        {
            return WaitingObjects;
        }

        private void CreateHighlightObjects(List<ITutorialPresenter> presenters)
        {
            IEnumerable<ISortingOptionsProvider> highlightedObjects;
            if (IsHighlightManual)
            {
                highlightedObjects = ResolveMirrorInstances<ISortingOptionsProvider>(Clip.HighlightedObjectIds);
            }
            else
            {
                highlightedObjects = FindHighlightedObjects();
            }

            if (highlightedObjects != null)
            {
                ITutorialPresenter presenter = _instantiator.Instantiate<TutorialHighlightObjectsPresenter>(
                        highlightedObjects,
                        ((TutorialTrack)TrackAsset).SortingLayer
                    );
                presenters.Add(presenter);
            }
            else
            {
                UnityEngine.Debug.LogError(
                        $"Cutscene '{Director.gameObject.scene.name}': Objects to highlight are not found."
                    );
            }
        }

        private void CreateHighlightCutouts(List<ITutorialPresenter> presenters)
        {
            if (!Clip.HaveCutoutsToHighlight)
            {
                return;
            }

            var cutoutDataList = new List<TutorialHighlightAreaPresenter.CutoutData>(Clip.Cutouts.Count);
            cutoutDataList.AddRange(
                    Clip.Cutouts.Select(
                            cutoutMirrorData => new TutorialHighlightAreaPresenter.CutoutData
                            {
                                Prefab = cutoutMirrorData.Prefab,
                                Transform = ResolveMirrorInstance<Transform>(cutoutMirrorData.TransformMirrorId)
                            }
                        )
                );
            presenters.Add(_instantiator.Instantiate<TutorialHighlightAreaPresenter>(cutoutDataList));
        }
    }
}