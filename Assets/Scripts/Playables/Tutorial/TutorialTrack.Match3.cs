﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if !NO_MATCH3

using DreamTeam.Playables.Tutorial.Match3;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.Tutorial
{
    [TrackClipType(typeof(WaitForSwapMoveClipAsset))]
    [TrackClipType(typeof(WaitForActivationMoveClipAsset))]
    [TrackClipType(typeof(WaitForFieldStabilizedClipAsset))]
    [TrackClipType(typeof(HighlightHiddenObjectsClipAsset))]
    [TrackClipType(typeof(HighlightCellsClipAsset))]
    [TrackClipType(typeof(HighlightChipsClipAsset))]
    [TrackClipType(typeof(HighlightConveyorClipAsset))]
    [TrackClipType(typeof(HighlightWallsClipAsset))]
    [TrackClipType(typeof(HighlightMagicHatSuccessfulSwapClipAsset))]
    public partial class TutorialTrack
    {
    }
}

#endif