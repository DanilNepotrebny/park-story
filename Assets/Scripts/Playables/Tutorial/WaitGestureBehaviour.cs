﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using TouchScript.Gestures;

namespace DreamTeam.Playables.Tutorial
{
    public class WaitGestureBehaviour : BaseObjectsWaitingTutorialBehaviour<Gesture>
    {
        private bool _deactivateGestureGameObjectOnUnsubscribe;

        protected override void SubscribeForInput(Gesture gesture)
        {
            if (!gesture.gameObject.activeInHierarchy)
            {
                _deactivateGestureGameObjectOnUnsubscribe = true;
                gesture.gameObject.SetActive(true);
            }
            gesture.StateChanged += OnGestureFieldStateChanged;
        }

        protected override void UnsubscribeFromInput(Gesture gesture)
        {
            gesture.StateChanged -= OnGestureFieldStateChanged;
            if (_deactivateGestureGameObjectOnUnsubscribe)
            {
                _deactivateGestureGameObjectOnUnsubscribe = false;
                gesture.gameObject.SetActive(false);
            }
        }

        protected override bool IsHighlightManual => true;

        private void OnGestureFieldStateChanged(object sender, GestureStateChangeEventArgs eventArgs)
        {
            if (eventArgs.State == Gesture.GestureState.Ended ||
                eventArgs.State == Gesture.GestureState.Recognized)
            {
                OnActionCompleted();
            }
        }
    }
}