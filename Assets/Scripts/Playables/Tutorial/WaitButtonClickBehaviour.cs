﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using DreamTeam.Utils.RendererSorting;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Playables.Tutorial
{
    public class WaitButtonClickBehaviour : BaseObjectsWaitingTutorialBehaviour<Button>
    {
        protected override void SubscribeForInput(Button button)
        {
            button.onClick.AddListener(OnButtonClicked);
        }

        protected override void UnsubscribeFromInput(Button button)
        {
            button.onClick.RemoveListener(OnButtonClicked);
        }

        protected override IEnumerable<Component> SelectInputTargets()
        {
            return WaitingObjects.Select(b => b.targetGraphic);
        }

        protected override IEnumerable<ISortingOptionsProvider> FindHighlightedObjects()
        {
            return SelectInputTargets()
                .Select(
                    targetGraphic => targetGraphic.GetComponentInParent<ISortingOptionsProvider>(true)
                );
        }

        private void OnButtonClicked()
        {
            OnActionCompleted();
        }
    }
}