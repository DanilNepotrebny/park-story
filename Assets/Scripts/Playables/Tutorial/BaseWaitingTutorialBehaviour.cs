﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Cutscenes;
using DreamTeam.InputLock;
using DreamTeam.Mirroring;
using DreamTeam.Playables.Tutorial.Presenters;
using DreamTeam.Utils.Invocation;
using JetBrains.Annotations;
using ModestTree;
using UnityEngine;
using UnityEngine.Playables;
using Zenject;

namespace DreamTeam.Playables.Tutorial
{
    public abstract class BaseWaitingTutorialBehaviour<TWaitingObject, TClipAsset> :
        BaseTutorialBehaviour
        where TWaitingObject : Component
        where TClipAsset : class, IWaitingTutorialClipAsset
    {
        [Inject] private CutsceneSystem _cutsceneSystem;
        [Inject] private MirrorObjectsRegistrator _mirrorObjectRegistrator;
        [Inject] private InputLockSystem _inputLockSystem;

        private List<ITutorialPresenter> _presenters = new List<ITutorialPresenter>();
        private ActionDisposable _pauseCutsceneDisposable;

        private IDisposable _inputLockhandle;

        private bool _isActionCompleted;

        protected TClipAsset Clip => GetClipAsset<TClipAsset>();

        protected abstract IEnumerable<TWaitingObject> WaitingObjects { get; }

        protected override void OnStart()
        {
            base.OnStart();

            Assert.IsNull(_pauseCutsceneDisposable, $"_pauseCutsceneDisposable != null");
            _pauseCutsceneDisposable = _cutsceneSystem.PauseCurrentCutscene();
            WaitingObjects.ForEach(SubscribeForInput);

            _presenters.Clear();
            InitializePresenters(_presenters);

            foreach (ITutorialPresenter controller in _presenters)
            {
                controller.OnStart();
            }
        }

        protected override void OnFinish()
        {
            base.OnFinish();

            if (!_isActionCompleted)
            {
                WaitingObjects.ForEach(UnsubscribeFromInput);
            }

            ReleasePauseCutsceneDisposable();

            if (_inputLockhandle != null)
            {
                _inputLockhandle.Dispose();
                _inputLockhandle = null;
            }

            foreach (ITutorialPresenter controller in _presenters)
            {
                controller.OnFinish();
            }
            _presenters.Clear();
            _presenters = null;
        }

        protected virtual void OnActionCompleted()
        {
            if (!_cutsceneSystem.IsPlaying || _isActionCompleted)
            {
                return;
            }

            _isActionCompleted = true;

            WaitingObjects.ForEach(UnsubscribeFromInput);
            _inputLockhandle = _inputLockSystem.LockTargets();

            ReleasePauseCutsceneDisposable();

            foreach (ITutorialPresenter controller in _presenters)
            {
                controller.OnActionCompleted();
            }
        }

        protected virtual void InitializePresenters(List<ITutorialPresenter> presenters)
        {
            if (Clip.IsHighlightEnabled)
            {
                InitializeHighlightPresenters(presenters);
            }

            TutorialHintPresenter hintPresenter = Clip.Hint.Resolve(Playable.GetGraph().GetResolver());
            if (hintPresenter != null)
            {
                if (Clip.HintTopBottomMode)
                {
                    TutorialHintPresenter hintBottom = Clip.HintBottom.Resolve(Playable.GetGraph().GetResolver());
                    Assert.IsNotNull(hintBottom, "hintBottom == NULL");

                    if (hintBottom != null)
                    {
                        hintPresenter = SelectHintToShow(hintPresenter, hintBottom);
                    }
                }

                presenters.Add(hintPresenter);
            }

            if (!string.IsNullOrEmpty(Clip.PointerPresenterMirrorId))
            {
                IEnumerable<TutorialPointerPresenter> pointerPresenters =
                    ResolveMirrorInstances<TutorialPointerPresenter>(
                        new List<string>
                        {
                            Clip.PointerPresenterMirrorId
                        });
                pointerPresenters.ForEach(presenters.Add);
            }
        }

        protected virtual void InitializeHighlightPresenters(List<ITutorialPresenter> presenters)
        {
            // override in childs
        }

        protected virtual TutorialHintPresenter SelectHintToShow(
            [NotNull] TutorialHintPresenter hintTop,
            [NotNull] TutorialHintPresenter hintBottom)
        {
            return hintTop;
        }

        protected IEnumerable<TInstance> ResolveMirrorInstances<TInstance>(IEnumerable<string> objectIds)
            where TInstance : class
        {
            foreach (string objectId in objectIds)
            {
                if (objectId == string.Empty)
                {
                    UnityEngine.Debug.LogWarning($"'{Asset.name}' has empty mirror ID");
                    continue;
                }

                IEnumerable<TInstance> objects = _mirrorObjectRegistrator.GetInstanceObjects<TInstance>(objectId);
                foreach (TInstance obj in objects)
                {
                    yield return obj;
                }
            }
        }

        protected TInstance ResolveMirrorInstance<TInstance>(string objectId)
            where TInstance : class
        {
            var @object = _mirrorObjectRegistrator.GetInstanceObject<TInstance>(objectId);
            return @object;
        }

        protected abstract void SubscribeForInput(TWaitingObject waitingObject);
        protected abstract void UnsubscribeFromInput(TWaitingObject waitingObject);

        private void ReleasePauseCutsceneDisposable()
        {
            _pauseCutsceneDisposable?.Dispose();
            _pauseCutsceneDisposable = null;
        }
    }
}
