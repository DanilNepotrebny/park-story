﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils.RendererSorting;
using UnityEngine;
using UnityEngine.UI;

namespace DreamTeam.Playables.Tutorial
{
    public class WaitToggleChangedBehaviour : BaseObjectsWaitingTutorialBehaviour<Toggle>
    {
        protected override void SubscribeForInput(Toggle toggle)
        {
            toggle.onValueChanged.AddListener(OnToggleValueChanged);
        }

        protected override void UnsubscribeFromInput(Toggle toggle)
        {
            toggle.onValueChanged.RemoveListener(OnToggleValueChanged);
        }

        protected override IEnumerable<Component> SelectInputTargets()
        {
            return WaitingObjects.Select(toggle => toggle.targetGraphic);
        }

        protected override IEnumerable<ISortingOptionsProvider> FindHighlightedObjects()
        {
            return SelectInputTargets().Select(
                    targetGraphic => targetGraphic.GetComponentInParent<ISortingOptionsProvider>()
                );
        }

        private void OnToggleValueChanged(bool value)
        {
            OnActionCompleted();
        }
    }
}