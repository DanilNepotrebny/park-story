﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using DreamTeam.Cutscenes;
using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Playables.Tutorial.Presenters
{
    public class TutorialHintPresenter : MonoBehaviour, ITutorialPresenter
    {
        [Inject] private CutsceneSystem _cutsceneSystem;

        [SerializeField] private SimpleAnimation _animation;

        private State _presenterState = State.Hidden;
        private Coroutine _coroutine = null;
        private ActionDisposable _pauseCutsceneDisposable;

        public void OnStart()
        {
            Show();
        }

        public void OnFinish()
        {
            Hide();
            ReleasePauseCutsceneDisposable();
        }

        public void OnActionCompleted()
        {
            Hide();
            ReleasePauseCutsceneDisposable();
        }

        private void Show()
        {
            switch (_presenterState)
            {
                case State.PrepareHide:
                    Assert.IsNotNull(_coroutine);
                    StopCoroutine(_coroutine);
                    _presenterState = State.Shown;
                    break;

                case State.Hiding:
                    Assert.IsNotNull(_coroutine);
                    StopCoroutine(_coroutine);
                    _coroutine = StartCoroutine(ShowCoroutine());
                    break;

                case State.Hidden:
                    Assert.IsNull(_coroutine);
                    _coroutine = StartCoroutine(ShowCoroutine());
                    break;
            }
        }

        private void Hide()
        {
            switch (_presenterState)
            {
                case State.PrepareShow:
                    Assert.IsNotNull(_coroutine);
                    StopCoroutine(_coroutine);
                    _presenterState = State.Hidden;
                    break;

                case State.Showing:
                    Assert.IsNotNull(_coroutine);
                    StopCoroutine(_coroutine);
                    _coroutine = StartCoroutine(HideCoroutine());
                    break;

                case State.Shown:
                    Assert.IsNull(_coroutine);
                    _coroutine = StartCoroutine(HideCoroutine());
                    break;
            }
        }

        private IEnumerator ShowCoroutine()
        {
            _presenterState = State.PrepareShow;
            yield return null;

            _presenterState = State.Showing;
            _animation.gameObject.SetActive(true);
            PauseCurrentCutscene();
            _animation.Play(SimpleAnimationType.Show);
            while (_animation.IsPlaying)
            {
                yield return null;
            }

            ReleasePauseCutsceneDisposable();
            _coroutine = null;
            _presenterState = State.Shown;
        }

        private IEnumerator HideCoroutine()
        {
            _presenterState = State.PrepareHide;
            yield return null;

            _presenterState = State.Hiding;
            PauseCurrentCutscene();
            _animation.Play(SimpleAnimationType.Hide);
            while (_animation.IsPlaying)
            {
                yield return null;
            }

            _animation.gameObject.SetActive(false);
            ReleasePauseCutsceneDisposable();
            _coroutine = null;
            _presenterState = State.Hidden;
        }

        private void PauseCurrentCutscene()
        {
            Assert.IsNull(_pauseCutsceneDisposable, $"_pauseCutsceneDisposable != null");
            _pauseCutsceneDisposable = _cutsceneSystem.PauseCurrentCutscene();
        }

        private void ReleasePauseCutsceneDisposable()
        {
            _pauseCutsceneDisposable?.Dispose();
            _pauseCutsceneDisposable = null;
        }

        private enum State
        {
            PrepareShow,
            Showing,
            Shown,
            PrepareHide,
            Hiding,
            Hidden
        }
    }
}