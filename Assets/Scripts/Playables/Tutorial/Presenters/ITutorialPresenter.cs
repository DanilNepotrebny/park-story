﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Playables.Tutorial.Presenters
{
    public interface ITutorialPresenter
    {
        void OnStart();
        void OnFinish();
        void OnActionCompleted();
    }
}