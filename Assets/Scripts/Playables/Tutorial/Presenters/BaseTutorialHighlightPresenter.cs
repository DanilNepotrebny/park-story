﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Cutscenes;
using DreamTeam.Tutorial;
using DreamTeam.Utils.Invocation;
using Zenject;

namespace DreamTeam.Playables.Tutorial.Presenters
{
    public abstract class BaseTutorialHighlightPresenter : ITutorialPresenter
    {
        [Inject] private CutsceneSystem _cutsceneSystem;
        [Inject] private TutorialTintController _tutorialTintController;

        private bool _isHidden;
        private ActionDisposable _pauseCutsceneDisposable;

        #region ITutorialPresenter

        public virtual void OnStart()
        {
            PauseCurrentCutscene();

            _tutorialTintController.OnShown += OnTintShown;
            _tutorialTintController.Show();
        }

        public virtual void OnFinish()
        {
            PauseCurrentCutscene();

            _tutorialTintController.OnHidden += OnTintHidden;
            _tutorialTintController.Hide();
        }

        public virtual void OnActionCompleted()
        {
            PauseCurrentCutscene();

            _tutorialTintController.OnHidden += OnTintHidden;
            _tutorialTintController.Hide();
        }

        #endregion ITutorialPresenter

        protected virtual void OnTintShown()
        {
            _tutorialTintController.OnShown -= OnTintShown;
            ReleasePauseCutsceneDisposable();
        }

        protected virtual void OnTintHidden()
        {
            _tutorialTintController.OnHidden -= OnTintHidden;
            ReleasePauseCutsceneDisposable();
        }

        private void PauseCurrentCutscene()
        {
            if (_pauseCutsceneDisposable != null)
            {
                _pauseCutsceneDisposable = _cutsceneSystem.PauseCurrentCutscene();
            }
        }

        private void ReleasePauseCutsceneDisposable()
        {
            _pauseCutsceneDisposable?.Dispose();
            _pauseCutsceneDisposable = null;
        }
    }
}
