﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils.RendererSorting;

namespace DreamTeam.Playables.Tutorial.Presenters
{
    public class TutorialHighlightObjectsPresenter : BaseTutorialHighlightPresenter
    {
        private List<SortingOptions> _sortingOptions;
        private readonly IEnumerable<ISortingOptionsProvider> _targetObjects;
        private readonly int _sortingLayer;

        public TutorialHighlightObjectsPresenter(IEnumerable<ISortingOptionsProvider> targetObjects, int sortingLayer)
        {
            _targetObjects = targetObjects;
            _sortingLayer = sortingLayer;
        }

        public override void OnStart()
        {
            base.OnStart();

            _sortingOptions = _targetObjects
                .Where(provider => provider?.SortingOptions != null)
                .Select(provider => provider.SortingOptions)
                .ToList();

            SortingOptions.PushToSortingLayer(_sortingOptions, _sortingLayer);
        }

        protected override void OnTintHidden()
        {
            base.OnTintHidden();

            if (_sortingOptions != null)
            {
                SortingOptions.PopFromSortingLayer(_sortingOptions);
                _sortingOptions = null;
            }
        }
    }
}