﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace DreamTeam.Playables.Tutorial.Presenters
{
    public class TutorialHighlightAreaPresenter : BaseTutorialHighlightPresenter
    {
        [Inject] private Instantiator _instantiator;

        private readonly List<FollowingCutout> _cutoutsCache = new List<FollowingCutout>();

        protected IList<CutoutData> CutoutDataList { get; set; }

        public TutorialHighlightAreaPresenter(IList<CutoutData> cutoutDataList = null)
        {
            CutoutDataList = cutoutDataList ?? new List<CutoutData>();
        }

        public override void OnStart()
        {
            base.OnStart();

            CreateCutouts();
        }

        public override void OnFinish()
        {
            DestroyCutouts();

            base.OnFinish();
        }

        private void CreateCutouts()
        {
            Assert.IsTrue(_cutoutsCache.Count == 0, "_cutoutsCache.Count != 0");
            _cutoutsCache.AddRange(CutoutDataList.Select(CreateCutout));
        }

        private void DestroyCutouts()
        {
            foreach (FollowingCutout cutout in _cutoutsCache)
            {
                cutout.TargetPositionNotifier.PositionChanged -= OnCutoutTargetPositionChanged;
                cutout.GameObject.Dispose();
            }
            _cutoutsCache.Clear();
        }

        private FollowingCutout CreateCutout(CutoutData data)
        {
            var posChangeNotifier = data.Transform.GetOrAddComponent<PositionChangeNotifier>();
            posChangeNotifier.PositionChanged += OnCutoutTargetPositionChanged;

            return new FollowingCutout
            {
                GameObject = _instantiator.Instantiate(data.Prefab, data.Transform.position),
                TargetPositionNotifier = posChangeNotifier
            };
        }

        private void OnCutoutTargetPositionChanged(Transform transform)
        {
            FollowingCutout cutout = _cutoutsCache.FirstOrDefault(c => c.TargetPositionNotifier.transform == transform);
            if (cutout != null)
            {
                cutout.GameObject.transform.position = transform.position;
            }
        }

        #region Inner types

        public struct CutoutData
        {
            public GameObject Prefab { get; set; }
            public Transform Transform { get; set; }
        }

        private class FollowingCutout
        {
            public GameObject GameObject { get; set; }
            public PositionChangeNotifier TargetPositionNotifier { get; set; }
        }

        #endregion Inner types
    }
}
