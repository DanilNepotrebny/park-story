﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI.SimpleAnimating;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace DreamTeam.Playables.Tutorial.Presenters
{
    public class TutorialPointerPresenter : MonoBehaviour, ITutorialPresenter
    {
        [SerializeField, RequiredField] private SimpleAnimation _animation;

        private bool _isStarted;

        public void OnStart()
        {
            ShowPointer();
        }

        public void OnFinish()
        {
            FinishAnimation();
        }

        public void OnActionCompleted()
        {
            FinishAnimation();
        }

        private void ShowPointer()
        {
            Assert.IsFalse(_isStarted, "Animation already started");
            _isStarted = true;

            bool isAnimationStarted = _animation.Play(SimpleAnimationType.Show);
            Assert.IsTrue(isAnimationStarted, $"Tutorial pointer animation has not been started");
            _animation.AnimationFinished += OnShowAnimationFinished;
        }

        private void OnShowAnimationFinished(ISimpleAnimationPlayer player, SimpleAnimationType animationType)
        {
            Assert.IsTrue(animationType == SimpleAnimationType.Show, "animationType != SimpleAnimationType.Show");
            player.AnimationFinished -= OnShowAnimationFinished;
            player.Play(SimpleAnimationType.Idle);
        }

        private void HidePointer()
        {
            _animation.Play(SimpleAnimationType.Hide);
        }

        private void FinishAnimation()
        {
            if (_isStarted)
            {
                _isStarted = false;
                _animation.Stop();

                HidePointer();
            }
        }
    }
}
