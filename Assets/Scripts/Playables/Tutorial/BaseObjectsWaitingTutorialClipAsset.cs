﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Mirroring;
using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.Playables.Tutorial.Presenters;
using UnityEngine;
using UnityEngine.Serialization;

namespace DreamTeam.Playables.Tutorial
{
    public abstract class BaseObjectsWaitingTutorialClipAsset<TWaitBehaviour> :
        BaseTutorialClipAsset<TWaitBehaviour>,
        IObjectsWaitingTutorialClipAsset
        where TWaitBehaviour : BaseTimelineClipBehaviour, new()
    {
        [SerializeField, MirrorId] private List<string> _waitingObjectIds;
        [SerializeField, MirrorId] private List<string> _additionalInputTargetObjectIds;
        [SerializeField, MirrorId] private string _pointerPresenterMirrorId;
        [SerializeField] private ExposedReference<TutorialHintPresenter> _hint;
        [SerializeField, Tooltip("In this mode showing hint will be selected between Hint and HintBottom according " +
             "to target element position on screen. In this case Hint is the top one")]
        private bool _hintTopBottomMode;
        [SerializeField] private ExposedReference<TutorialHintPresenter> _hintBottom;
        [SerializeField] private bool _isHighlightEnabled;
        [FormerlySerializedAs("_isHighlightManual"), SerializeField]
        private bool _haveObjectsToHighlight;
        [SerializeField, MirrorId] private List<string> _highlightedObjectIds;
        [SerializeField] private bool _haveCutoutsToHighlight;
        [SerializeField] private List<CutoutMirrorData> _cutouts;

        public IList<string> WaitingObjectIds => _waitingObjectIds;
        public IList<string> AdditionalInputTargetObjectIds => _additionalInputTargetObjectIds;
        public string PointerPresenterMirrorId => _pointerPresenterMirrorId;
        public ExposedReference<TutorialHintPresenter> Hint => _hint;
        public bool HintTopBottomMode => _hintTopBottomMode;
        public ExposedReference<TutorialHintPresenter> HintBottom => _hintBottom;
        public bool IsHighlightEnabled => _isHighlightEnabled;
        public bool HaveObjectsToHighlight => _haveObjectsToHighlight;
        public IList<string> HighlightedObjectIds => _highlightedObjectIds;
        public bool HaveCutoutsToHighlight => _haveCutoutsToHighlight;
        public IList<CutoutMirrorData> Cutouts => _cutouts;
    }

    public interface IObjectsWaitingTutorialClipAsset :
        IWaitingTutorialClipAsset
    {
        IList<string> WaitingObjectIds { get; }
        IList<string> AdditionalInputTargetObjectIds { get; }
        bool HaveObjectsToHighlight { get; }
        IList<string> HighlightedObjectIds { get; }
        bool HaveCutoutsToHighlight { get; }
        IList<CutoutMirrorData> Cutouts { get; }
    }

    [Serializable]
    public struct CutoutMirrorData
    {
        [SerializeField] private GameObject _prefab;
        [SerializeField, MirrorId] private string _transformMirrorId;

        public GameObject Prefab => _prefab;
        public string TransformMirrorId => _transformMirrorId;
    }
}
