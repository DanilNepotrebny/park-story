﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.Playables;

namespace DreamTeam.Playables
{
    [RequireComponent(typeof(PlayableDirector))]
    public class PlayableFixedUpdater : MonoBehaviour
    {
        [SerializeField] private float _targetFps = 60f;
        [SerializeField] private float _maxEvaluatesPerFrame = 3;

        private PlayableDirector _director;
        private float _targetTimeDelta;
        private float _dt = 0f;

        protected void Awake()
        {
            _director = GetComponent<PlayableDirector>();
            _director.timeUpdateMode = DirectorUpdateMode.Manual;

            _targetTimeDelta = 1f / _targetFps;
        }

        protected void LateUpdate()
        {
            PlayableGraph graph = _director.playableGraph;
            if (graph.IsValid())
            {
                _dt += Time.deltaTime;
                int counter = 0;
                while (_dt > _targetTimeDelta && counter < _maxEvaluatesPerFrame)
                {
                    graph.Evaluate(_targetTimeDelta);
                    _dt -= _targetTimeDelta;
                    ++counter;
                }
            }
        }
    }
}