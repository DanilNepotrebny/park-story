﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.NightControl
{
    [Serializable]
    public class NightControlClipAsset : BaseTimelineClipAsset<NightControlMixerBehaviour, BaseTimelineClipBehaviour>, ITimelineClipAsset
    {
        public ClipCaps clipCaps => ClipCaps.Blending;
    }
}
