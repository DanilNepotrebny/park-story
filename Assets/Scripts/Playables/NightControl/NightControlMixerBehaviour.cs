﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Location.Night;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Playables;
using Zenject;

namespace DreamTeam.Playables.NightControl
{
    [Serializable]
    public class NightControlMixerBehaviour : BaseTimelineMixerBehaviour
    {
        [Inject]
        private NightSystem _nightSystem;

        public override void OnGraphStart(Playable playable)
        {
            if (Application.isPlaying)
            {
                _nightSystem.gameObject.SetActive(true);
                _nightSystem.NightFactor = 0;
            }
        }

        public override void OnGraphStop(Playable playable)
        {
            if (Application.isPlaying)
            {
                _nightSystem.gameObject.SetActive(false);
                _nightSystem.NightFactor = 0;
            }
        }

        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            if (Application.isPlaying)
            {
                float nightFactor = 0;
                int inputCount = playable.GetInputCount();
                for (int i = 0; i < inputCount; i++)
                {
                    nightFactor = Mathf.Max(playable.GetInputWeight(i), nightFactor);
                }

                _nightSystem.NightFactor = nightFactor;
            }
        }
    }
}