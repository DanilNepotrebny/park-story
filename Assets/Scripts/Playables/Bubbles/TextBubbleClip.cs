﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Localization;
using DreamTeam.Location.Bubbles;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;

namespace DreamTeam.Playables.Bubbles
{
    public class TextBubbleClip : BaseTimelineClipAsset<BaseTimelineMixerBehaviour, TextBubbleClipBehaviour>
    {
        [SerializeField] private ExposedReference<Transform> _followTarget;
        [SerializeField, LocalizationKey] private string _bubbleText;
        [SerializeField] private Vector2 _preferedDirection;
        [SerializeField] private Vector2 _shiftPosition;
        [SerializeField] private ExposedObstacleReference[] _ignoreObstacles = new ExposedObstacleReference[0];

        public TextBubbleArgs GetBubbleArgs(IExposedPropertyTable resolver)
        {
            Transform followTarget = _followTarget.Resolve(resolver);
            BaseBubbleObstacle[] ignoreList = Array.ConvertAll(_ignoreObstacles, item => item.Resolve(resolver));

            return new TextBubbleArgs(followTarget, _bubbleText, _preferedDirection, ignoreList, _shiftPosition);
        }
    }
}