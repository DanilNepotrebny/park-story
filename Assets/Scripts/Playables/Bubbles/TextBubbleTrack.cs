﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Bubbles;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.Bubbles
{
    [TrackClipType(typeof(TextBubbleClip))]
    [TrackBindingType(typeof(TextBubbleHint))]
    public class TextBubbleTrack : BaseTimelineTrackAsset<BaseTimelineMixerBehaviour, TextBubbleClipBehaviour>
    {
    }
}