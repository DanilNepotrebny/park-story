﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Location.Bubbles;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;

namespace DreamTeam.Playables.Bubbles
{
    public class PrefabBubbleClip : BaseTimelineClipAsset<BaseTimelineMixerBehaviour, PrefabBubbleClipBehaviour>
    {
        [SerializeField] private ExposedReference<Transform> _followTarget;
        [SerializeField] private RectTransform _contentPrefab;
        [SerializeField] private Vector2 _preferedDirection;
        [SerializeField] private Vector2 _shiftPosition;
        [SerializeField] private ExposedObstacleReference[] _ignoreObstacles = new ExposedObstacleReference[0];

        public PrefabBubbleArgs GetBubbleArgs(IExposedPropertyTable resolver)
        {
            Transform followTarget = _followTarget.Resolve(resolver);
            RectTransform content = Instantiate(_contentPrefab) as RectTransform;
            BaseBubbleObstacle[] ignoreList = Array.ConvertAll(_ignoreObstacles, item => item.Resolve(resolver));

            return new PrefabBubbleArgs(followTarget, content, _preferedDirection, ignoreList, _shiftPosition);
        }
    }
}