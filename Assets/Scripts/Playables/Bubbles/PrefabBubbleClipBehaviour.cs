﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Bubbles;
using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Playables;

namespace DreamTeam.Playables.Bubbles
{
    public class PrefabBubbleClipBehaviour : BaseTimelineClipBehaviour
    {
        public override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPlay(playable, frameData);

            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                PrefabBubbleClip clipAsset = GetClipAsset<PrefabBubbleClip>();
                PrefabBubbleHint boundObject = GetBoundObject<PrefabBubbleHint>();

                IExposedPropertyTable resolver = playable.GetGraph().GetResolver();
                boundObject.Show(clipAsset.GetBubbleArgs(resolver));
            }
        }

        public override void OnBehaviourPause(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPause(playable, frameData);

            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                PrefabBubbleHint boundObject = GetBoundObject<PrefabBubbleHint>();
                boundObject.Hide();
            }
        }
    }
}
