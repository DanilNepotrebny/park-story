﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Bubbles;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.Bubbles
{
    [TrackClipType(typeof(PrefabBubbleClip))]
    [TrackBindingType(typeof(PrefabBubbleHint))]
    public class PrefabBubbleTrack : BaseTimelineTrackAsset<BaseTimelineMixerBehaviour, PrefabBubbleClipBehaviour>
    {
    }
}
