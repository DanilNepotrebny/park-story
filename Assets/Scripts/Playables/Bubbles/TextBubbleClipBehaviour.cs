﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Bubbles;
using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Playables;

namespace DreamTeam.Playables.Bubbles
{
    public class TextBubbleClipBehaviour : BaseTimelineClipBehaviour
    {
        public override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPlay(playable, frameData);

            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                TextBubbleClip clipAsset = GetClipAsset<TextBubbleClip>();
                TextBubbleHint boundObject = GetBoundObject<TextBubbleHint>();

                IExposedPropertyTable resolver = playable.GetGraph().GetResolver();
                boundObject.Show(clipAsset.GetBubbleArgs(resolver));
            }
        }

        public override void OnBehaviourPause(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPause(playable, frameData);

            if (EditorUtils.IsPlayingOrWillChangePlaymode())
            {
                TextBubbleHint boundObject = GetBoundObject<TextBubbleHint>();
                boundObject.Hide();
            }
        }
    }
}
