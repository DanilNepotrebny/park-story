﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Location.Bubbles;
using UnityEngine;

namespace DreamTeam.Playables.Bubbles
{
    [Serializable]
    public struct ExposedObstacleReference
    {
        [SerializeField] private ExposedReference<BaseBubbleObstacle> _reference;

        public BaseBubbleObstacle Resolve(IExposedPropertyTable resolver)
        {
            return _reference.Resolve(resolver);
        }
    }
}