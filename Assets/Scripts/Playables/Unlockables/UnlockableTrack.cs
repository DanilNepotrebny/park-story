﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Unlockables;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.Unlockables
{
    [TrackColor(0.9960784f, 0.8039216f, 0.4235294f)]
    [TrackClipType(typeof(UnlockableClipAsset))]
    [TrackBindingType(typeof(UnlockableControllerBase))]
    public class UnlockableTrack : BaseTimelineTrackAsset<BaseTimelineMixerBehaviour, UnlockableClipBehaviour>
    {
    }
}