﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Playables.BaseTimelinePlayables;
using UnityEngine;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.Unlockables
{
    [Serializable]
    public class UnlockableClipAsset : BaseTimelineClipAsset<BaseTimelineMixerBehaviour, UnlockableClipBehaviour>,
        ITimelineClipAsset
    {
        [SerializeField] private ActionType _clipActionType;

        public ClipCaps clipCaps => ClipCaps.None;

        public ActionType ClipActionType => _clipActionType;

        public enum ActionType
        {
            Lock,
            Sync,
            Increment,
            Decrement
        }
    }
}