﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Location.Unlockables;
using DreamTeam.Playables.BaseTimelinePlayables;
using DreamTeam.Utils;
using UnityEngine.Playables;

namespace DreamTeam.Playables.Unlockables
{
    [Serializable]
    public class UnlockableClipBehaviour : BaseTimelineClipBehaviour
    {
        public override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPlay(playable, frameData);

            UnlockableClipAsset clipAsset = GetClipAsset<UnlockableClipAsset>();
            UnlockableControllerBase boundObject = GetBoundObject<UnlockableControllerBase>();

            switch (clipAsset.ClipActionType)
            {
                case UnlockableClipAsset.ActionType.Lock:
                    boundObject.LockSafe();
                    break;

                case UnlockableClipAsset.ActionType.Sync:
                    boundObject.SyncSafe();
                    break;

                case UnlockableClipAsset.ActionType.Increment:
                    boundObject.IncrementStageSafe();
                    break;

                case UnlockableClipAsset.ActionType.Decrement:
                    boundObject.DecrementStageSafe();
                    break;
            }
        }

        public override void OnBehaviourPause(Playable playable, FrameData frameData)
        {
            base.OnBehaviourPause(playable, frameData);

            if (!EditorUtils.IsPlayingOrWillChangePlaymode() && Director != null)
            {
                UnlockableControllerBase boundObject = GetBoundObject<UnlockableControllerBase>();
                boundObject.Sync();
            }
        }
    }
}