﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.BaseTimelinePlayables
{
    public abstract class BaseTimelineBehaviour : PlayableBehaviour
    {
        public Playable Playable { get; private set; }
        public PlayableDirector Director { get; private set; }
        public TrackAsset TrackAsset { get; private set; }

        public void InitializeBehaviour(Playable playable, PlayableDirector director, TrackAsset trackAsset)
        {
            Playable = playable;
            Director = director;
            TrackAsset = trackAsset;

            var diContainer = RequireComponent<PlayableDIContainer>();
            if (diContainer != null)
            {
                diContainer.Inject(this);
            }
        }

        public T GetBoundObject<T>()
            where T : Object
        {
            return Director.GetGenericBinding(TrackAsset) as T;
        }

        public T RequireComponent<T>() where T : Component
        {
            var result = Director.gameObject.GetComponent<T>();

            if (result == null)
            {
                if (Application.isEditor && !Application.isPlaying)
                {
                    result = Director.gameObject.AddComponent<T>();
                }
            }

            Assert.IsNotNull(
                    result,
                    $"Can't get or add component {nameof(T)} on {Director.name}. " +
                    $"Please add it manually"
                );

            return result;
        }
    }
}