﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace DreamTeam.Playables.BaseTimelinePlayables
{
    public class PlayableDIContainer : MonoBehaviour
    {
        private DiContainer _diContainer;
        private Queue<object> _injectionQueue = new Queue<object>();

        [Inject]
        private void Construct(DiContainer diContainer)
        {
            _diContainer = diContainer;

            while (_injectionQueue.Count > 0)
            {
                Inject(_injectionQueue.Dequeue());
            }
        }

        public void Inject(object injectable)
        {
            if (Application.isPlaying)
            {
                if (_diContainer != null)
                {
                    _diContainer.Inject(injectable);
                }
                else
                {
                    if (!_injectionQueue.Contains(injectable))
                    {
                        _injectionQueue.Enqueue(injectable);
                    }
                }
            }
        }
    }
}