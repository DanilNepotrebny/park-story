﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.BaseTimelinePlayables
{
    public abstract class BaseTimelineClipAsset<TMixerBehaviour, TClipBehaviour> : PlayableAsset, ITimelineClipAsset<TMixerBehaviour>
        where TMixerBehaviour : BaseTimelineMixerBehaviour, new()
        where TClipBehaviour : BaseTimelineClipBehaviour, new()
    {
        public TrackAsset TrackAsset { get; private set; }
        public TimelineClip TimelineClip { get; private set; }
        public TMixerBehaviour MixerBehaviour { get; private set; }
        public TClipBehaviour ClipBehaviour { get; private set; }

        public sealed override Playable CreatePlayable(PlayableGraph graph, GameObject go)
        {
            ScriptPlayable<TClipBehaviour> scriptPlayable = Utils.CreateBehaviour<TClipBehaviour>(graph, go.GetComponent<PlayableDirector>(), TrackAsset);
            OnPlayableCreated(scriptPlayable);
            return scriptPlayable;
        }

        public void Initialize(TrackAsset trackAsset, TimelineClip timelineClip, TMixerBehaviour mixer)
        {
            TrackAsset = trackAsset;
            TimelineClip = timelineClip;
            MixerBehaviour = mixer;
        }

        protected virtual void OnPlayableCreated(ScriptPlayable<TClipBehaviour> scriptPlayable)
        {
            ClipBehaviour = scriptPlayable.GetBehaviour();
            ClipBehaviour.InitializeClipBehaviour(this, MixerBehaviour);
        }
    }

    public interface ITimelineClipAsset<in TMixerBehaviour>
    {
        void Initialize(TrackAsset trackAsset, TimelineClip timelineClip, TMixerBehaviour mixer);
    }
}