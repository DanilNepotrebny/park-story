﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine.Playables;

namespace DreamTeam.Playables.BaseTimelinePlayables
{
    public class BaseTimelineMixerBehaviour : BaseTimelineBehaviour
    {
        public virtual void OnClipBehaviourPlaying(Playable playable)
        {
        }

        public virtual void OnClipBehaviourPlayed(Playable playable)
        {
        }
    }
}