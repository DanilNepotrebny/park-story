﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.BaseTimelinePlayables
{
    public static class Utils
    {
        public static ScriptPlayable<TBehaviour> CreateBehaviour<TBehaviour>(PlayableGraph graph, PlayableDirector director, TrackAsset trackAsset, int inputCount = 0)
            where TBehaviour : BaseTimelineBehaviour, new()
        {
            ScriptPlayable<TBehaviour> playable = ScriptPlayable<TBehaviour>.Create(graph, inputCount);
            TBehaviour behaviour = playable.GetBehaviour();
            behaviour.InitializeBehaviour(playable, director, trackAsset);
            return playable;
        }
    }
}