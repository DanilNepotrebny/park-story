﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace DreamTeam.Playables.BaseTimelinePlayables
{
    public abstract class BaseTimelineTrackAsset<TMixerBehaviour, TClipBehaviour> : TrackAsset
        where TMixerBehaviour : BaseTimelineMixerBehaviour, new()
        where TClipBehaviour : BaseTimelineClipBehaviour, new()
    {
        public TMixerBehaviour MixerBehaviour { get; private set; }

        public sealed override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            ScriptPlayable<TMixerBehaviour> scriptPlayable = Utils.CreateBehaviour<TMixerBehaviour>(graph, go.GetComponent<PlayableDirector>(), this, inputCount);
            MixerBehaviour = scriptPlayable.GetBehaviour();

            foreach (TimelineClip clip in GetClips())
            {
                var clipAsset = clip.asset as ITimelineClipAsset<TMixerBehaviour>;
                if (clipAsset == null)
                {
                    throw new InvalidOperationException();
                }

                clipAsset.Initialize(this, clip, MixerBehaviour);
            }

            return scriptPlayable;
        }
    }
}