﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine.Playables;

namespace DreamTeam.Playables.BaseTimelinePlayables
{
    public class BaseTimelineClipBehaviour : BaseTimelineBehaviour
    {
        public PlayableAsset Asset { get; private set; }
        public BaseTimelineMixerBehaviour Mixer { get; private set; }

        public void InitializeClipBehaviour(PlayableAsset asset, BaseTimelineMixerBehaviour mixer)
        {
            Asset = asset;
            Mixer = mixer;
        }

        public T GetClipAsset<T>() where T : class
        {
            return Asset as T;
        }

        public override void OnBehaviourPlay(Playable playable, FrameData frameData)
        {
            Mixer.OnClipBehaviourPlaying(playable);
        }

        public override void OnBehaviourPause(Playable playable, FrameData frameData)
        {
            Mixer.OnClipBehaviourPlayed(playable);
        }
    }
}