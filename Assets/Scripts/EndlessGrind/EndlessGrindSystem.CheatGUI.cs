﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Debug;
using UnityEngine;
using Zenject;

namespace DreamTeam.EndlessGrind
{
    public partial class EndlessGrindSystem
    {
        [Inject] private CheatPanel _cheatPanel;

        private const string _debugTimeFormat = @"dd\:hh\:mm\:ss";

        private bool _forceCheatRestart;

        private void AwakeCheats()
        {
            _cheatPanel.AddDrawer(CheatPanel.Category.EndlessGrind, OnDrawCheatGUI);
            bool.TryParse(PlayerPrefs.GetString(nameof(_forceCheatRestart)), out _forceCheatRestart);
        }

        private void OnDestroyCheats()
        {
            _cheatPanel.RemoveDrawer(OnDrawCheatGUI);
            PlayerPrefs.SetString(nameof(_forceCheatRestart), _forceCheatRestart.ToString());
        }

        private void OnDrawCheatGUI(Rect rect)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label($"Time till end: {TimeTillEndEvent.ToString(_debugTimeFormat)}");
            GUILayout.Label($"Time till restart: {TimeTillRestartEvent.ToString(_debugTimeFormat)}");
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Force event start"))
            {
                StartEvent();
            }

            if (GUILayout.Button("Force event stop"))
            {
                StopEvent();
                SetRestartEventTime(_forceCheatRestart ? DateTime.UtcNow + _eventRestartInterval : default(DateTime));
            }

            string toggleState = _forceCheatRestart ? "X" : "  ";
            _forceCheatRestart = GUILayout.Toggle(
                _forceCheatRestart,
                $"Force event restart [{toggleState}] ",
                GUI.skin.button);
        }
    }
}
