﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory;
using DreamTeam.Utils;
using Spine.Unity;
using UnityEngine;

namespace DreamTeam.EndlessGrind
{
    [CreateAssetMenu(menuName = "Endless grind/Chest")]
    public class EndlessGrindChest : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField, RequiredField] private ProductPack _productPack;

        [Space] [SerializeField, RequiredField] private SkeletonDataAsset _skeletonData;
        [SerializeField, SpineSkin(dataField = "_skeletonData")] private string _skin;

        public string Name => _name;
        public ProductPack ProductPack => _productPack;
        public string Skin => _skin;
    }
}