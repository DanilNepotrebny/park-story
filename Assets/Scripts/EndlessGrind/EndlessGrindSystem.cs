﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections;
using System.Collections.Generic;
using DreamTeam.GameControls;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Prices;
using DreamTeam.Localization;
using DreamTeam.Notifications;
using DreamTeam.Synchronization;
using DreamTeam.Synchronization.States;
using DreamTeam.Utils;
using DreamTeam.Utils.Invocation;
using UnityEngine;
using Zenject;
#if !NO_MATCH3
using DreamTeam.Match3.Levels;

#endif

namespace DreamTeam.EndlessGrind
{
    public partial class EndlessGrindSystem : MonoBehaviour, ISynchronizable, ILocalNotificationsProvider, IInitable
    {
        [SerializeField, RequiredField, ReadOnly] private GameControlsMode _controlsMode;

        [SerializeField] private TimeTicks _eventDuration;
        [SerializeField] private TimeTicks _eventRestartInterval;
        [SerializeField] private CountedItemPrice _skipRestartingPrice;

        [SerializeField, RequiredField] private List<EndlessGrindChest> _chests;

        [Header("Notifications")]
        [SerializeField]
        private TimeTicks _notificationSchedule;

        [SerializeField, LocalizationKey] private string _notificationTitleKey;
        [SerializeField, LocalizationKey] private string _notificationMessageKey;
        [SerializeField] private string _notificationSoundName;

        [Inject] private SaveGroup _saveGroup;
        [Inject] private GameControlsSystem _gameControls;
        [Inject] private NotificationsSystem _notificationsSystem;
        [Inject] private Lives _livesItem;
        [Inject] private LocalizationSystem _localizationSystem;
        [Inject] private PriceInstanceContainer _priceInstanceContainer;

        #if !NO_MATCH3
        [Inject(Id = "MainLevelPackController")] private SingleLevelPackController _mainPackController;
        #endif

        private ActionDisposable _modeDisableHandle;

        private DateTime _endEventTime = default(DateTime);
        private DateTime _restartEventTime = default(DateTime);

        private Price.Instance _skipRestartingPriceInstance;

        public ICountedItemQuantity SkipRestartingQuantity => _skipRestartingPrice;

        public IReadOnlyCollection<EndlessGrindChest> Chests => _chests.AsReadOnly();
        public TimeSpan TimeTillEndEvent => _isEventStarted ? _endEventTime - DateTime.UtcNow : TimeSpan.Zero;

        public TimeSpan TimeTillRestartEvent =>
            IsEventRestarting ? _restartEventTime - DateTime.UtcNow : TimeSpan.Zero;

        public bool IsGrindAvailable => IsEventRestarting || _isEventStarted;

        public bool IsEventRestarting =>
            !_isEventStarted && _restartEventTime != default(DateTime) && DateTime.UtcNow < _restartEventTime;

        private bool _isEventStarted => _endEventTime != default(DateTime) && DateTime.UtcNow < _endEventTime;

        private bool _shouldRestart
        {
            get
            {
                bool shouldRestart = false;

                shouldRestart |= _forceCheatRestart;

                #if !NO_MATCH3
                {
                    shouldRestart |= _mainPackController.IsCompleted;
                }
                #endif

                return shouldRestart;
            }
        }

        public event Action IsEventRestartingChanged;
        public event Action EventUpdating;
        public event Action EventStoped;

        public void Init()
        {
            _saveGroup.RegisterSynchronizable("EndlessGrindSystem", this);
        }

        public void TrySkipRestarting()
        {
            _skipRestartingPriceInstance.TryPay(GamePlace.Location);
        }

        private void OnSkipRestartingPricePayed(GamePlace gamePlace)
        {
            _restartEventTime = default(DateTime);
        }

        protected void Awake()
        {
            _skipRestartingPriceInstance = _priceInstanceContainer.GetInstance(_skipRestartingPrice);
            _skipRestartingPriceInstance.Payed += OnSkipRestartingPricePayed;

            _notificationsSystem.RegisterLocalNotificationsProvider(this);

            AwakeCheats();

            StartCoroutine(UpdateEvent());
        }

        protected void OnDestroy()
        {
            ReleaseModeDisableHandle();

            _skipRestartingPriceInstance.Payed -= OnSkipRestartingPricePayed;

            _notificationsSystem.UnRegisterLocalNotificationsProvider(this);

            OnDestroyCheats();
        }

        private IEnumerator UpdateEvent()
        {
            EventUpdating?.Invoke();
            SetControlsModeEnabled(_isEventStarted);
            yield return new WaitWhile(() => _isEventStarted);

            StopEvent();

            if (_shouldRestart)
            {
                yield return new WaitWhile(() => IsEventRestarting);
                if (_shouldRestart)
                {
                    StartEvent();
                }
            }
            else
            {
                SetRestartEventTime(default(DateTime));
            }
        }

        private void StartEvent()
        {
            SetEndEventTime(DateTime.UtcNow + _eventDuration);
            SetRestartEventTime(_endEventTime + _eventRestartInterval);
            StartCoroutine(UpdateEvent());
        }

        private void StopEvent()
        {
            if (_endEventTime != default(DateTime))
            {
                SetControlsModeEnabled(false);

                SetEndEventTime(default(DateTime));
                EventStoped?.Invoke();
            }
        }

        private void SetControlsModeEnabled(bool isEnabled)
        {
            if (isEnabled)
            {
                _modeDisableHandle = _modeDisableHandle ?? _gameControls.EnableMode(_controlsMode);
            }
            else
            {
                ReleaseModeDisableHandle();
            }
        }

        private void ReleaseModeDisableHandle()
        {
            _modeDisableHandle?.Dispose();
            _modeDisableHandle = null;
        }

        private void SetEndEventTime(DateTime time)
        {
            _endEventTime = time;
        }

        private void SetRestartEventTime(DateTime time)
        {
            if (_restartEventTime != time)
            {
                _restartEventTime = time;
                IsEventRestartingChanged?.Invoke();
            }
        }

        void ISynchronizable.Sync(State state)
        {
            state.SyncDateTime("endEventTime", ref _endEventTime);
            state.SyncDateTime("restartEventTime", ref _restartEventTime);
        }

        IEnumerable<NotificationsSystem.LocalNotificationParams> ILocalNotificationsProvider.Provide()
        {
            var result = new List<NotificationsSystem.LocalNotificationParams>();

            if (_isEventStarted && _livesItem.Count > 0)
            {
                NotificationsSystem.LocalNotificationParams parameters = new NotificationsSystem.LocalNotificationParams
                {
                    TriggerTime = DateTime.Now + _notificationSchedule,
                    Title = _localizationSystem.Localize(_notificationTitleKey, this),
                    Message = _localizationSystem.Localize(_notificationMessageKey, this),
                    SoundName = _notificationSoundName
                };

                result.Add(parameters);
            }

            return result;
        }
    }
}