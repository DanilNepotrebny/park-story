﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using DreamTeam.UI.Dialogs;
using Spine.Unity;
using UnityEngine;

namespace DreamTeam
{
    [CreateAssetMenu(menuName = "Characters/Character data")]
    public partial class CharacterData : ScriptableObject, IHasSkeletonDataAsset
    {
        [SerializeField, LocalizationKey] private string _name;
        [SerializeField] private SkeletonDataAsset _skeletonDataAsset;
        [SerializeField] private DialogController.CharacterSide _defaultSide;
        [SerializeField] private Sprite _photo;
        [SerializeField, LocalizationKey] private string _description;
        [SerializeField, SpineBone(dataField = nameof(_skeletonDataAsset))] private string _bubbleBoneName;
        [SerializeField, SpineAnimation(dataField = nameof(_skeletonDataAsset))] private string _dialogIdleAnimationName;

        public string Name => _name;
        public SkeletonDataAsset SkeletonDataAsset => _skeletonDataAsset;
        public DialogController.CharacterSide DefaultSide => _defaultSide;
        public Sprite Photo => _photo;
        public string Description => _description;
        public string BubbleBoneName => _bubbleBoneName;
        public string DialogIdleAnimationName => _dialogIdleAnimationName;
    }
}