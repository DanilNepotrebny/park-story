﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DreamTeam
{
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(ScreenOrientationNotifier))]
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    public class RectTransformFitToParentScaler : UIBehaviour
    {
        [SerializeField] private FitType _fitType;
        [SerializeField] private VerticalAlign _verticalAling;
        [SerializeField] private HorizontalAlign _horizontalAlign;

        private RectTransform _cachedMine;
        private ScreenOrientationNotifier _cachedNotifier;
        private DrivenRectTransformTracker _tracker;

        private RectTransform _mine
        {
            get
            {
                if (_cachedMine == null)
                {
                    _cachedMine = GetComponent<RectTransform>();
                }

                return _cachedMine;
            }
        }

        private RectTransform _parent
        {
            get
            {
                RectTransform result = null;
                if (_mine.parent != null)
                {
                    result = _mine.parent as RectTransform;
                }

                return result;
            }
        }

        private ScreenOrientationNotifier _notifier
        {
            get
            {
                if (_cachedNotifier == null)
                {
                    _cachedNotifier = GetComponent<ScreenOrientationNotifier>();
                }

                return _cachedNotifier;
            }
        }

        protected override void OnEnable()
        {
            _notifier.OrientationChanged += OnOrientationChanged;

            UpdateScale();
        }

        protected override void OnDisable()
        {
            _notifier.OrientationChanged -= OnOrientationChanged;

            _tracker.Clear();
        }

        protected override void OnTransformParentChanged()
        {
            UpdateScale();
        }

        protected override void OnRectTransformDimensionsChange()
        {
            UpdateScale();
        }

        #if UNITY_EDITOR
        protected override void OnValidate()
        {
            UpdateScale();
        }
        #endif // UNITY_EDITOR

        private void OnOrientationChanged(ScreenOrientationNotifier.Orientation orientation)
        {
            UpdateScale();
        }

        private void UpdateScale()
        {
            if (enabled &&
                _parent != null &&
                _mine.rect != Rect.zero)
            {
                _tracker.Clear();
                _tracker.Add(
                    this,
                    _mine,
                    DrivenTransformProperties.Rotation |
                    DrivenTransformProperties.AnchoredPosition3D |
                    DrivenTransformProperties.Scale |
                    DrivenTransformProperties.AnchorMin |
                    DrivenTransformProperties.AnchorMax |
                    DrivenTransformProperties.Pivot);

                _mine.anchorMin = new Vector2(0.5f, 0.5f);
                _mine.anchorMax = new Vector2(0.5f, 0.5f);
                _mine.pivot = new Vector2(0.5f, 0.5f);

                float scale = 1f;
                switch (_fitType)
                {
                    case FitType.Horizontal:
                        scale = _parent.rect.size.x / _mine.rect.size.x;
                        break;

                    case FitType.Vertical:
                        scale = _parent.rect.size.y / _mine.rect.size.y;
                        break;

                    case FitType.Envelope:
                        if (_parent.rect.size.x > _parent.rect.size.y)
                        {
                            scale = _parent.rect.size.x / _mine.rect.size.x;
                        }
                        else
                        {
                            scale = _parent.rect.size.y / _mine.rect.size.y;
                        }

                        break;
                }

                _mine.localScale = new Vector3(scale, scale, 1f);

                float positionX = 0f;
                float xSizeDelta = _mine.rect.size.x * scale - _parent.rect.size.x;
                if (xSizeDelta > 0)
                {
                    switch (_horizontalAlign)
                    {
                        case HorizontalAlign.Left:
                            positionX = -xSizeDelta / 2f;
                            break;

                        case HorizontalAlign.Right:
                            positionX = xSizeDelta / 2f;
                            break;
                    }
                }

                float positionY = 0f;
                float ySizeDelta = _mine.rect.size.y * scale - _parent.rect.size.y;
                if (ySizeDelta > 0)
                {
                    switch (_verticalAling)
                    {
                        case VerticalAlign.Bottom:
                            positionY = ySizeDelta / 2f;
                            break;

                        case VerticalAlign.Top:
                            positionY = -ySizeDelta / 2f;
                            break;
                    }
                }

                _mine.anchoredPosition = new Vector2(positionX, positionY);
            }
            else
            {
                _tracker.Clear();
            }
        }

        private enum FitType
        {
            Vertical,
            Horizontal,
            Envelope
        }

        private enum VerticalAlign
        {
            Center,
            Top,
            Bottom
        }

        private enum HorizontalAlign
        {
            Center,
            Left,
            Right
        }
    }
}