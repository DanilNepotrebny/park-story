﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DreamTeam.Editor
{
    public class LayersPanel : IDisposable
    {
        private float _topLayerAlpha;
        private List<Layer> _layers;
        private static readonly Layer.SortingLayerComparer _layersComparer = new Layer.SortingLayerComparer();
        private Layer _selectedLayer;

        public Layer SelectedLayer
        {
            get { return _selectedLayer; }
            private set
            {
                if (_selectedLayer == value)
                {
                    return;
                }

                _selectedLayer = value;
                SelectedLayerChanged?.Invoke(_selectedLayer);
            }
        }

        private float TopLayerAlpha
        {
            get { return _topLayerAlpha; }
            set
            {
                if (Mathf.Approximately(_topLayerAlpha, value))
                {
                    return;
                }

                _topLayerAlpha = value;
                UpdateLayersAlpha();
            }
        }

        public event Action<Layer> SelectedLayerChanged;

        public LayersPanel()
        {
            _layers = new List<Layer>();

            EditorSceneManager.sceneOpened += OnSceneOpened;
            EditorSceneManager.sceneClosed += OnSceneClosed;
        }

        public void Dispose()
        {
            EditorSceneManager.sceneOpened -= OnSceneOpened;
            EditorSceneManager.sceneClosed -= OnSceneClosed;

            if (_layers != null)
            {
                foreach (Layer layer in _layers)
                {
                    UnsubscribeFromDestroying(layer);
                }

                _layers.Clear();
                _layers = null;
            }
        }

        public void Refresh()
        {
            foreach (Layer layer in _layers)
            {
                if (layer != null)
                {
                    UnsubscribeFromDestroying(layer);
                }
            }

            _layers.Clear();

            for (int i = 0; i < EditorSceneManager.loadedSceneCount; ++i)
            {
                Scene scene = SceneManager.GetSceneAt(i);
                if (!scene.IsValid() || !scene.isLoaded)
                {
                    continue;
                }

                GameObject[] gameObjects = scene.GetRootGameObjects();
                if (gameObjects == null)
                {
                    continue;
                }

                foreach (GameObject gameObject in gameObjects)
                {
                    Layer[] gridLayers = gameObject.GetComponentsInChildren<Layer>();
                    foreach (Layer layer in gridLayers)
                    {
                        SubscribeToDestroying(layer);
                    }

                    _layers.AddRange(gridLayers);
                }
            }

            if (SelectedLayer == null && _layers.Count > 0)
            {
                SelectedLayer = _layers[0];
            }

            UpdateLayersAlpha();
        }

        public void Paint()
        {
            if (_layers == null)
            {
                _layers = new List<Layer>();
                Refresh();
            }

            bool updateLayers = false;

            if (GUILayout.Button("Create layer"))
            {
                var go = new GameObject("Layer");
                go.AddComponent<DestroyNotifier>();
                Layer layer = go.AddComponent<Layer>();
                SubscribeToDestroying(layer);
                _layers.Add(layer);
                Selection.activeGameObject = go;

                updateLayers = true;
            }

            if (_layers.Count > 0)
            {
                GUILayout.BeginHorizontal();
                {
                    GUILayout.Label("Layer to draw on:");
                    int selectedIndex = 0;
                    var layerNames = new string[_layers.Count];

                    for (int i = 0; i < _layers.Count; ++i)
                    {
                        layerNames[i] = $"{_layers[i].gameObject.scene.name}_{_layers[i].name}";
                        if (SelectedLayer != null && _layers[i] == SelectedLayer)
                        {
                            selectedIndex = i;
                        }
                    }

                    int newSelectedIndex = EditorGUILayout.Popup(selectedIndex, layerNames);
                    if (newSelectedIndex != selectedIndex)
                    {
                        SelectedLayer = _layers[newSelectedIndex];
                        updateLayers = true;
                    }

                    if (GUILayout.Button("Refresh"))
                    {
                        updateLayers = true;
                    }
                }
                GUILayout.EndHorizontal();

                if (_layers.Count > 1)
                {
                    TopLayerAlpha = EditorGUILayout.Slider("Top layers alpha", TopLayerAlpha, 0, 1);
                }
            }

            if (updateLayers)
            {
                Refresh();
            }
        }

        private void UpdateLayersAlpha()
        {
            _layers.Sort(_layersComparer);
            bool isTopLayer = false;
            foreach (var layer in _layers)
            {
                layer.SetAlpha(isTopLayer ? TopLayerAlpha : 1);
                if (layer == SelectedLayer)
                {
                    isTopLayer = true;
                }
            }
        }

        private void OnSceneOpened(Scene scene1, OpenSceneMode openSceneMode)
        {
            Refresh(); // TODO: Is everything okay with scene loaded with some specific mode? May loaded scene be final merged one?
        }

        private void OnSceneClosed(Scene scene1)
        {
            Refresh();
        }

        private void SubscribeToDestroying([NotNull] Component item)
        {
            DestroyNotifier destroyNotifier = item.GetComponent<DestroyNotifier>();
            if (destroyNotifier == null)
            {
                destroyNotifier = item.gameObject.AddComponent<DestroyNotifier>();
                UnityEngine.Debug.Log($"Added missed {nameof(DestroyNotifier)} component to {item.name} layer");
            }

            destroyNotifier.Destroying += OnLayerDestroying;
        }

        private void UnsubscribeFromDestroying([NotNull] Component item)
        {
            DestroyNotifier destroyNotifier = item.GetComponent<DestroyNotifier>();
            if (destroyNotifier != null)
            {
                destroyNotifier.Destroying -= OnLayerDestroying;
            }
        }

        private void OnLayerDestroying(GameObject layerGO)
        {
            _layers.RemoveAll(l => l == null || l.gameObject == layerGO);
        }
    }
}