﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_IOS

using System.IO;
using System.Linq;
using DreamTeam.Editor.Utils;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

namespace DreamTeam.Editor
{
    public partial class RequiredDeviceCapabilities
    {
        [PostProcessBuild(1)]
        public static void OnPostProcessBuild(BuildTarget target, string path)
        {
            if (target != BuildTarget.iOS)
            {
                return;
            }

            RequiredDeviceCapabilities asset = AssetDatabaseUtils
                .EnumerateAssets<RequiredDeviceCapabilities>()
                .FirstOrDefault();

            if (asset == null)
            {
                return;
            }

            string[] capabilities = asset._requiredCapabilities;
            if (capabilities.Length == 0)
            {
                return;
            }

            string plistPath = Path.Combine(path, "Info.plist");
            var plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));
            PlistElementDict rootDict = plist.root;

            const string capsKey = "UIRequiredDeviceCapabilities";
            PlistElement capsElement;
            PlistElementArray capsArray = rootDict.values.TryGetValue(capsKey, out capsElement) ?
                capsElement.AsArray() :
                rootDict.CreateArray(capsKey);

            foreach (string capability in capabilities)
            {
                capsArray.AddString(capability);
            }

            File.WriteAllText(plistPath, plist.WriteToString());
        }
    }
}

#endif