﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Editor.AssetInstanceTools;
using DreamTeam.Editor.PositionTransformers;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.AssetBrushes
{
    public class PreviewBrush : BaseBrush
    {
        protected readonly ToolPalette _toolPalette;

        private readonly int _previewLayerName = SortingLayer.NameToID("EditorGameObjectPreview");

        public PreviewBrush(GUIContent guiContent, ToolPalette toolPalette, BaseTransformer transformer = null)
            : base(guiContent, transformer)
        {
            _toolPalette = toolPalette;
        }

        public override void Activate()
        {
            base.Activate();

            Selection.selectionChanged += OnSelectedObjectChanged;

            if (Selection.activeObject != null)
            {
                _toolPalette.ActivateTool(Selection.activeObject);
            }
        }

        public override void Deactivate()
        {
            base.Deactivate();

            _toolPalette.DeactivateTool();
            Selection.selectionChanged -= OnSelectedObjectChanged;
        }

        protected override void PrepareAction()
        {
            base.PrepareAction();

            UpdatePreview();
        }

        protected override bool ContinueAction()
        {
            bool isSuccessful = base.ContinueAction();
            if (isSuccessful)
            {
                UpdatePreview();
            }

            return isSuccessful;
        }

        private void OnSelectedObjectChanged()
        {
            if (Selection.activeObject != null)
            {
                _toolPalette.ActivateTool(Selection.activeObject);
            }
        }

        private void UpdatePreview()
        {
            GameObject preview = _toolPalette.UpdatePreview(WorldPosition);
            if (preview == null)
            {
                return;
            }

            Renderer[] renderers = preview.GetComponentsInChildren<Renderer>();
            foreach (Renderer r in renderers)
            {
                r.sortingLayerID = _previewLayerName;
            }
        }
    }
}