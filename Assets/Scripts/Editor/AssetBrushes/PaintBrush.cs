﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Editor.AssetInstanceTools;
using DreamTeam.Editor.PositionTransformers;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.AssetBrushes
{
    public class PaintBrush : PreviewBrush
    {
        private readonly bool _replaceExistedInCell;
        private readonly BaseTool _eraseTool;

        public PaintBrush(
            GUIContent guiContent,
            BaseTransformer positionTransformer,
            ToolPalette toolPalette,
            bool replaceExistedInCell = false,
            BaseTool eraseTool = null) :
            base(guiContent, toolPalette, positionTransformer)
        {
            _eraseTool = eraseTool;
            _replaceExistedInCell = replaceExistedInCell;
        }

        protected override void DrawSceneGUI()
        {
            base.DrawSceneGUI();

            if (Selection.activeObject == null)
            {
                return;
            }

            Texture preview = AssetPreview.GetAssetPreview(Selection.activeObject);
            if (preview == null)
            {
                return;
            }

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            Rect r = EditorGUILayout.BeginVertical(
                GUILayout.MaxWidth(preview.width),
                GUILayout.MaxHeight(preview.height));
            GUILayout.FlexibleSpace();
            EditorGUI.DrawTextureTransparent(r, preview);
            GUILayout.Space(preview.width);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndVertical();

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        protected override void BeginAction()
        {
            base.BeginAction();

            if (_replaceExistedInCell)
            {
                RemoveAllExistedInCell(WorldPosition);
            }

            _toolPalette.ApplyTool(WorldPosition, Layer);
        }

        protected override bool ContinueAction()
        {
            if (!base.ContinueAction())
            {
                return false;
            }

            if (_replaceExistedInCell)
            {
                RemoveAllExistedInCell(WorldPosition);
            }

            _toolPalette.ContinueApplyingTool(WorldPosition, Layer);
            return true;
        }

        private void RemoveAllExistedInCell(Vector3 targetPos)
        {
            if (Layer == null)
            {
                return;
            }

            _eraseTool.Erase(targetPos, Layer);
        }
    }
}