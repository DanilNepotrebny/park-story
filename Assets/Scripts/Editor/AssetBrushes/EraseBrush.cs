﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Editor.AssetInstanceTools;
using DreamTeam.Editor.PositionTransformers;
using UnityEngine;

namespace DreamTeam.Editor.AssetBrushes
{
    public class EraseBrush : BaseBrush
    {
        private readonly BaseTool _tool;

        public EraseBrush(GUIContent guiContent, BaseTool tool, BaseTransformer positionTransformer = null) :
            base(guiContent, positionTransformer)
        {
            _tool = tool;
        }

        public override void Activate()
        {
            base.Activate();

            _tool.Activate();
        }

        public override void Deactivate()
        {
            base.Deactivate();

            _tool.Deactivate();
        }

        protected override void BeginAction()
        {
            base.BeginAction();

            _tool.Erase(WorldPosition, Layer);
        }

        protected override bool ContinueAction()
        {
            if (!base.ContinueAction())
            {
                return false;
            }

            _tool.Erase(WorldPosition, Layer);
            return true;
        }
    }
}