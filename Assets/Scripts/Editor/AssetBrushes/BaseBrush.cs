﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Editor.PositionTransformers;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.AssetBrushes
{
    /// <summary> Base class for all asset brushes. </summary>
    [Serializable]
    public abstract class BaseBrush
    {
        private Vector3 _worldPosition;

        public ILayer Layer { get; set; }

        protected BaseTransformer PositionTransformer { get; }

        /// <summary> Get brush world position. May be transformed by PositionTransformer </summary>
        protected Vector3 WorldPosition
        {
            get { return PositionTransformer?.GetTransformed(_worldPosition, Layer) ?? _worldPosition; }
            private set { _worldPosition = value; }
        }

        /// <summary> Gui content used for button </summary>
        public GUIContent GuiContent { get; }

        public BaseBrush(GUIContent guiContent, BaseTransformer positionTransformer = null)
        {
            GuiContent = guiContent;
            PositionTransformer = positionTransformer;
        }

        public virtual void OnGUI()
        {
            DrawSceneGUI();
        }

        public void HandleEvent(Event e)
        {
            HandleEventInternal(e);

            switch (e.type)
            {
                case EventType.MouseMove:
                {
                    e.Use();
                    PrepareAction();
                    SceneView.RepaintAll();
                }
                    break;

                case EventType.MouseDown:
                    if (IsAcceptableMouseEvent(e))
                    {
                        e.Use();
                        BeginAction();
                    }

                    break;

                case EventType.MouseUp:
                    if (IsAcceptableMouseEvent(e))
                    {
                        e.Use();
                        EndAction();
                    }

                    break;

                case EventType.MouseDrag:
                    if (IsAcceptableMouseEvent(e))
                    {
                        e.Use();
                        ContinueAction();
                    }

                    break;

                case EventType.MouseEnterWindow:
                case EventType.MouseLeaveWindow:
                {
                    e.Use();
                    CancelAction();
                }
                    break;

                case EventType.KeyDown:
                case EventType.KeyUp:
                case EventType.ScrollWheel:
                case EventType.Repaint:
                case EventType.Layout:
                case EventType.DragUpdated:
                case EventType.DragPerform:
                case EventType.DragExited:
                case EventType.Ignore:
                case EventType.Used:
                case EventType.ValidateCommand:
                case EventType.ExecuteCommand:
                case EventType.ContextClick:
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public virtual void Activate()
        {
            SceneView.onSceneGUIDelegate += OnSceneGUI;
        }

        public virtual void Deactivate()
        {
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
        }

        protected virtual void DrawSceneGUI()
        {
            GUILayout.BeginHorizontal();
            {
                GUILayout.FlexibleSpace();

                GUIStyle titleStyle = new GUIStyle(GUI.skin.label) { richText = true };

                GUILayout.Label("<b>" + GuiContent.text + "</b>", titleStyle);
                GUILayout.FlexibleSpace();
            }
            GUILayout.EndHorizontal();
        }

        // Override those methods in childs

        protected virtual void HandleEventInternal(Event e)
        {
            // nothing to do
        }

        protected virtual void BeginAction()
        {
            // nothing to do
        }

        protected virtual bool ContinueAction()
        {
            return true;
        }

        protected virtual void EndAction()
        {
            // nothing to do
        }

        protected virtual void CancelAction()
        {
            // nothing to do
        }

        protected virtual void PrepareAction()
        {
            // nothing to do
        }

        private static bool IsAcceptableMouseEvent(Event e)
        {
            return e.button != (int)MouseButtonType.Right &&
                e.button != (int)MouseButtonType.Middle;
        }

        private void OnSceneGUI(SceneView aView)
        {
            WorldPosition = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition).origin;

            int controlID = GUIUtility.GetControlID(FocusType.Passive);
            HandleUtility.AddDefaultControl(controlID);

            Handles.BeginGUI();
            {
                Rect rect = new Rect(10, 20, 150, 150);

                GUILayout.BeginArea(rect);
                {
                    Rect r = EditorGUILayout.BeginVertical();
                    {
                        GUI.color = Color.red;
                        GUI.Box(r, GUIContent.none);

                        GUI.color = Color.white;

                        DrawSceneGUI();
                    }
                    EditorGUILayout.EndVertical();
                }
                GUILayout.EndArea();
            }
            Handles.EndGUI();
        }
    }
}