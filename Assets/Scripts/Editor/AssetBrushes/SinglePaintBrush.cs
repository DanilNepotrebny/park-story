﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Editor.AssetInstanceTools;
using DreamTeam.Editor.PositionTransformers;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.AssetBrushes
{
    public class SinglePaintBrush : PreviewBrush
    {
        public SinglePaintBrush(GUIContent guiContent, ToolPalette toolPalette, BaseTransformer transformer = null)
            : base(guiContent, toolPalette, transformer)
        {
        }

        protected override void DrawSceneGUI()
        {
            base.DrawSceneGUI();

            if (Selection.activeObject == null)
            {
                return;
            }

            Texture preview = AssetPreview.GetAssetPreview(Selection.activeObject);
            if (preview == null)
            {
                return;
            }

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            Rect r = EditorGUILayout.BeginVertical(
                GUILayout.MaxWidth(preview.width),
                GUILayout.MaxHeight(preview.height));
            GUILayout.FlexibleSpace();
            EditorGUI.DrawTextureTransparent(r, preview);
            GUILayout.Space(preview.width);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndVertical();

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        protected override void EndAction()
        {
            base.EndAction();

            _toolPalette.ApplyTool(WorldPosition, Layer);
        }
    }
}