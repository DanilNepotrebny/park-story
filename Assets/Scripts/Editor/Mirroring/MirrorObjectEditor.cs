﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Mirroring;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Mirroring
{
    [CustomEditor(typeof(MirrorObject))]
    public class MirrorObjectEditor : UnityEditor.Editor
    {
        private SerializedProperty _assetProperty;
        private SerializedProperty _mirrorObjectProperty;
        private SerializedProperty _mirrorFlowProperty;
        private SerializedProperty _instanceIdProperty;

        private MirrorObjectsIdentifiersAsset _identifiersAsset;
        private Object _mirrorObject;

        private string _filter;
        private int _selectedIdentifierIndex;
        private List<string> _identifierPopupOptions = new List<string>();
        private string _newId;
        private bool _addButtonPressed = false;

        private MirrorObject CachedMirrorObject => target as MirrorObject;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            if (_identifiersAsset != null)
            {
                GUI.enabled = false;
                EditorGUILayout.ObjectField("Identifiers asset", _identifiersAsset, typeof(MirrorObjectsIdentifiersAsset), false);
                GUI.enabled = true;

                _mirrorObject = _mirrorObjectProperty.objectReferenceValue;
                Object mirrorObject = EditorGUILayout.ObjectField(
                        "Mirror object: ",
                        _mirrorObject,
                        typeof(Object),
                        true
                    );

                if (mirrorObject != _mirrorObject)
                {
                    _mirrorObject = mirrorObject;
                    if (EditorUtility.IsPersistent(_mirrorObject))
                    {
                        _mirrorObject = null;
                    }

                    _mirrorObjectProperty.objectReferenceValue = _mirrorObject;
                    serializedObject.ApplyModifiedProperties();
                }

                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(_mirrorFlowProperty);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }

                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField("Mirror id", GUILayout.Width(EditorGUIUtility.labelWidth)); 

                _filter = EditorGUILayout.TextArea(_filter);

                FillPopupData(ref _selectedIdentifierIndex, ref _identifierPopupOptions);

                bool isIdSet = true;
                if (_selectedIdentifierIndex < 0)
                {
                    _identifierPopupOptions.Insert(0, MirrorObjectsIdentifiersAsset.NotSetIdentifier);
                    _selectedIdentifierIndex = 0;
                    isIdSet = false;
                }

                GUI.color = isIdSet ? Color.white : Color.red;

                int index = EditorGUILayout.Popup(_selectedIdentifierIndex, _identifierPopupOptions.ToArray());

                if (index != _selectedIdentifierIndex)
                {
                    _instanceIdProperty.stringValue = _identifierPopupOptions[index];
                    serializedObject.ApplyModifiedProperties();
                }

                GUI.color = Color.white;

                EditorGUILayout.EndHorizontal();

                bool isDuplicate = _identifiersAsset.Contains(_newId);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Create new ID: ", GUILayout.Width(EditorGUIUtility.labelWidth));

                if (_addButtonPressed ||
                    Event.current.keyCode == KeyCode.Return &&
                    !string.IsNullOrEmpty(_newId) &&
                    !isDuplicate)
                {
                    _addButtonPressed = false;
                    GUIUtility.keyboardControl = 0;
                    _identifiersAsset.Identifiers.Add(_newId);
                    _identifiersAsset.Identifiers.Sort();
                    _instanceIdProperty.stringValue = _newId;
                    EditorUtility.SetDirty(target);
                    EditorUtils.RecordObjectUndo(_identifiersAsset, "Added mirror identifier");
                    serializedObject.ApplyModifiedProperties();
                    _newId = string.Empty;
                    GUI.color = Color.white;
                }

                GUI.color = isDuplicate ? Color.red : Color.white;
                _newId = EditorGUILayout.TextField(_newId);
                GUI.color = Color.white;

                GUIStyle addButtonStyle = new GUIStyle(EditorStyles.miniButton);
                addButtonStyle.alignment = TextAnchor.MiddleCenter;
                addButtonStyle.stretchWidth = false;

                GUI.enabled = !isDuplicate;
                if (GUILayout.Button("Add", addButtonStyle))
                {
                    _addButtonPressed = true;
                }
                GUI.enabled = true;

                EditorGUILayout.EndHorizontal();
            }
        }

        protected void OnEnable()
        {
            _assetProperty = serializedObject.FindProperty("_asset");
            _mirrorObjectProperty = serializedObject.FindProperty("_mirroredObject");
            _mirrorFlowProperty = serializedObject.FindProperty("_mirrorFlow");
            _instanceIdProperty = serializedObject.FindProperty("_instanceId");

            _identifiersAsset = MirrorObjectsIdentifiersAsset.FindAsset();
            _assetProperty.objectReferenceValue = _identifiersAsset;
            serializedObject.ApplyModifiedProperties();
        }

        private void FillPopupData(ref int selectedIndex, ref List<string> options)
        {
            options.Clear();
            options.AddRange(
                !string.IsNullOrEmpty(_filter) ?
                    _identifiersAsset.Identifiers.Where(option => option.Contains(_filter)) :
                    _identifiersAsset.Identifiers);

            selectedIndex = options.IndexOf(_instanceIdProperty.stringValue);
        }
    }
}