﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Mirroring;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Mirroring
{
    [CustomEditor(typeof(MirrorObjectsIdentifiersAsset))]
    public class MirrorObjectsIdentifiersAssetEditor : UnityEditor.Editor
    {
        private SerializedProperty _identifiersListProperty;
        private string _newId;

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.LabelField("IDENTIFIERS SET");

            EditorGUI.BeginChangeCheck();

            GUIStyle removeButtonStyle = new GUIStyle(EditorStyles.label);
            removeButtonStyle.alignment = TextAnchor.MiddleCenter;
            removeButtonStyle.stretchWidth = false;

            MirrorObjectsIdentifiersAsset asset = target as MirrorObjectsIdentifiersAsset;

            for (int i = 0; i < _identifiersListProperty.arraySize; i++)
            {
                SerializedProperty elementProperty = _identifiersListProperty.GetArrayElementAtIndex(i);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PropertyField(elementProperty, GUIContent.none);

                if (GUILayout.Button("X", removeButtonStyle))
                {
                    _identifiersListProperty.DeleteArrayElementAtIndex(i);
                }

                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();

            bool isDublicate = asset.Contains(_newId);
            GUI.color = isDublicate ? Color.red : Color.white;
            _newId = EditorGUILayout.TextField(_newId);
            GUI.color = Color.white;

            GUIStyle addButtonStyle = new GUIStyle(EditorStyles.miniButton);
            addButtonStyle.alignment = TextAnchor.MiddleCenter;
            addButtonStyle.stretchWidth = false;

            GUI.enabled = !isDublicate;
            if (GUILayout.Button("Add", addButtonStyle))
            {
                asset.Identifiers.Add(_newId);
                EditorUtility.SetDirty(target);
                _newId = string.Empty;
            }
            GUI.enabled = true;

            EditorGUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck())
            {
                asset.Identifiers.Sort();
                serializedObject.ApplyModifiedProperties();
            }
        }

        protected void OnEnable()
        {
            _identifiersListProperty = serializedObject.FindProperty("_identifiers");
        }
    }
}