﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Mirroring.Resolvers;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Mirroring.Resolvers
{
    [CustomEditor(typeof(CinemachineVirtualCameraMirrorsResolver))]
    public class CinemachineVirtualCameraMirrorsResolverEditor : UnityEditor.Editor
    {
        private bool _isBindingsOpen = false;

        private CinemachineVirtualCameraMirrorsResolver.IEditorApi Resolver =>
            target as CinemachineVirtualCameraMirrorsResolver.IEditorApi;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = false;

            DrawBindingData();

            GUI.enabled = true;

            serializedObject.Update();
        }

        protected void OnEnable()
        {
            EditorApplication.update += OnEditorUpdate;
        }

        protected void OnDisable()
        {
            EditorApplication.update -= OnEditorUpdate;
        }

        private void DrawBindingData()
        {
            _isBindingsOpen = EditorGUILayout.Foldout(_isBindingsOpen, "Bindings");
            if (_isBindingsOpen)
            {
                DrawBind("Follow", Resolver.FollowBind.MirrorId);
                DrawBind("Look At", Resolver.LookAtBind.MirrorId);
            }
        }

        private void DrawBind(string label, string mirrorId)
        {
            GUIStyle typeFieldStyle = new GUIStyle(EditorStyles.miniButton);
            typeFieldStyle.alignment = TextAnchor.MiddleLeft;

            GUIStyle assignedIdFieldStyle = new GUIStyle(EditorStyles.miniButton);
            assignedIdFieldStyle.alignment = TextAnchor.MiddleLeft;

            EditorGUILayout.BeginHorizontal();
            if (!string.IsNullOrEmpty(mirrorId))
            {
                GUI.color = Color.green;
                EditorGUILayout.TextField(label);
                EditorGUILayout.TextField("Mirror will be resolved in runtime");
                EditorGUILayout.TextField(typeof(Transform).Name, typeFieldStyle);
                EditorGUILayout.TextField(mirrorId, assignedIdFieldStyle);
                GUI.color = Color.white;
            }
            else
            {
                GUI.color = Color.grey;
                EditorGUILayout.TextField(label);
                EditorGUILayout.TextField("Nothing to resolve");
                EditorGUILayout.TextField(typeof(Transform).Name, typeFieldStyle);
                EditorGUILayout.TextField("Empty", assignedIdFieldStyle);
                GUI.color = Color.white;
            }

            EditorGUILayout.EndHorizontal();
        }

        private void OnEditorUpdate()
        {
            if (!Application.isPlaying)
            {
                Resolver.SaveLinks();
            }
        }
    }
}