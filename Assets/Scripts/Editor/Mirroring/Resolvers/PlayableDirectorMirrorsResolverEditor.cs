﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Linq;
using DreamTeam.Mirroring.Resolvers;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace DreamTeam.Editor.Mirroring.Resolvers
{
    [CustomEditor(typeof(PlayableDirectorMirrorsResolver))]
    public class PlayableDirectorMirrorsResolverEditor : UnityEditor.Editor
    {
        private bool _isBindingsOpen = false;
        private bool _isReferencesOpen = false;

        private PlayableDirectorMirrorsResolver.IEditorApi Resolver =>
            target as PlayableDirectorMirrorsResolver.IEditorApi;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = false;

            DrawBindingData();
            DrawReferenceData();

            GUI.enabled = true;

            serializedObject.Update();
        }

        protected void OnEnable()
        {
            EditorApplication.update += OnEditorUpdate;
        }

        protected void OnDisable()
        {
            EditorApplication.update -= OnEditorUpdate;
        }

        private void DrawBindingData()
        {
            _isBindingsOpen = EditorGUILayout.Foldout(_isBindingsOpen, "Bindings");
            if (_isBindingsOpen)
            {
                TimelineAsset asset = Resolver.Director.playableAsset as TimelineAsset;
                if (asset != null && Resolver.MirrorBinds.Count > 0)
                {
                    GUIStyle typeFieldStyle = new GUIStyle(EditorStyles.miniButton);
                    typeFieldStyle.alignment = TextAnchor.MiddleLeft;

                    GUIStyle assignedIdFieldStyle = new GUIStyle(EditorStyles.miniButton);
                    assignedIdFieldStyle.alignment = TextAnchor.MiddleLeft;

                    for (int i = 0; i < Resolver.MirrorBinds.Count; i++)
                    {
                        PlayableDirectorMirrorsResolver.BindData data = Resolver.MirrorBinds[i];
                        PlayableBinding binding = asset.outputs.ElementAt(data.OutputIndex);

                        EditorGUILayout.BeginHorizontal();

                        if (!string.IsNullOrEmpty(data.MirrorId))
                        {
                            GUI.color = Color.green;
                            EditorGUILayout.TextField("Mirror will be resolved in runtime");
                            EditorGUILayout.TextField(binding.outputTargetType.Name, typeFieldStyle);
                            EditorGUILayout.TextField(data.MirrorId, assignedIdFieldStyle);
                            GUI.color = Color.white;
                        }
                        else
                        {
                            GUI.color = Color.grey;
                            EditorGUILayout.TextField("Nothing to resolve");
                            EditorGUILayout.TextField(binding.outputTargetType.Name, typeFieldStyle);
                            EditorGUILayout.TextField("Empty", assignedIdFieldStyle);
                            GUI.color = Color.white;
                        }

                        EditorGUILayout.EndHorizontal();
                    }
                }
                else
                {
                    EditorGUILayout.LabelField("Please set \"TimelineAsset\" with defined tracks in the \"Playable Director\"!");
                }
            }
        }

        private void DrawReferenceData()
        {
            _isReferencesOpen = EditorGUILayout.Foldout(_isReferencesOpen, "Exposed references");
            if (_isReferencesOpen)
            {
                TimelineAsset asset = Resolver.Director.playableAsset as TimelineAsset;
                if (asset != null)
                {
                    if (Resolver.MirrorReferences.Count > 0)
                    {
                        GUIStyle typeFieldStyle = new GUIStyle(EditorStyles.miniButton);
                        typeFieldStyle.alignment = TextAnchor.MiddleLeft;

                        GUIStyle assignedIdFieldStyle = new GUIStyle(EditorStyles.miniButton);
                        assignedIdFieldStyle.alignment = TextAnchor.MiddleLeft;

                        SerializedProperty referencesData = serializedObject.FindProperty("_mirrorReferences");
                        for (int index = 0; index < Resolver.MirrorReferences.Count; index++)
                        {
                            SerializedProperty elementProperty = referencesData.GetArrayElementAtIndex(index);
                            SerializedProperty exposedNameProperty =
                                elementProperty.FindPropertyRelative("_exposedName");
                            SerializedProperty mirrorIdProperty = elementProperty.FindPropertyRelative("_mirrorId");

                            Assert.IsFalse(
                                string.IsNullOrEmpty(mirrorIdProperty.stringValue),
                                "[PROGRAMMERS] incorrect mirror data serialization!");

                            if (!string.IsNullOrEmpty(mirrorIdProperty.stringValue))
                            {
                                EditorGUILayout.BeginHorizontal();
                                GUI.color = Color.green;
                                EditorGUILayout.TextField("Mirror will be resolved in runtime");
                                EditorGUILayout.TextField(exposedNameProperty.stringValue, typeFieldStyle);
                                EditorGUILayout.TextField(mirrorIdProperty.stringValue, assignedIdFieldStyle);
                                GUI.color = Color.white;
                                EditorGUILayout.EndHorizontal();
                            }
                        }
                    }
                    else
                    {
                        EditorGUILayout.LabelField("No exposed references with Mirror component found!");
                    }
                }
                else
                {
                    EditorGUILayout.LabelField("Please set \"TimelineAsset\" with defined tracks in the \"Playable Director\"!");
                }
            }
        }

        private void OnEditorUpdate()
        {
            if (!Application.isPlaying)
            {
                Resolver.SaveLinks();
            }
        }
    }
}