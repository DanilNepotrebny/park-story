﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Mirroring;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Mirroring
{
    [CustomPropertyDrawer(typeof(MirrorIdAttribute))]
    public class MirrorIdPropertyDrawer : PropertyDrawer
    {
        private MirrorObjectsIdentifiersAsset _identifiersAsset;
        private string _filter;
        private int _selectedIdentifierIndex;
        private List<string> _identifierPopupOptions = new List<string>();

        public override void OnGUI(Rect drawRect, SerializedProperty property, GUIContent label)
        {
            if (property.type != "string")
            {
                UnityEngine.Debug.LogWarning("Use only with string type");
            }
            else
            {
                if (_identifiersAsset == null)
                {
                    _identifiersAsset = MirrorObjectsIdentifiersAsset.FindAsset();
                }

                if (_identifiersAsset != null)
                {
                    Rect currentDrawRect = drawRect;

                    currentDrawRect.size = new Vector2(EditorGUIUtility.labelWidth, drawRect.size.y);
                    EditorGUI.LabelField(currentDrawRect, label);

                    drawRect.position = new Vector2(drawRect.position.x + currentDrawRect.size.x, drawRect.position.y);
                    drawRect.size = new Vector2(drawRect.size.x - currentDrawRect.size.x, drawRect.size.y);

                    currentDrawRect.position = drawRect.position;
                    currentDrawRect.size = new Vector2(100f, drawRect.size.y);
                    _filter = EditorGUI.TextArea(currentDrawRect, _filter);

                    drawRect.position = new Vector2(drawRect.position.x + currentDrawRect.size.x, drawRect.position.y);
                    drawRect.size = new Vector2(drawRect.size.x - currentDrawRect.size.x, drawRect.size.y);
                    currentDrawRect.position = drawRect.position;
                    currentDrawRect.size = drawRect.size;

                    FillPopupData(property.stringValue, ref _selectedIdentifierIndex, ref _identifierPopupOptions);

                    bool isIdSet = true;
                    if (_selectedIdentifierIndex < 0)
                    {
                        _identifierPopupOptions.Insert(0, MirrorObjectsIdentifiersAsset.NotSetIdentifier);
                        _selectedIdentifierIndex = 0;
                        isIdSet = false;
                    }

                    GUI.color = isIdSet ? Color.white : Color.red;

                    int index = EditorGUI.Popup(
                        currentDrawRect,
                        _selectedIdentifierIndex,
                        _identifierPopupOptions.ToArray());

                    if (index != _selectedIdentifierIndex)
                    {
                        property.stringValue = _identifierPopupOptions[index];
                    }

                    GUI.color = Color.white;
                }
            }
        }

        private void FillPopupData(string currentValue, ref int selectedIndex, ref List<string> options)
        {
            options.Clear();

            options.AddRange(
                !string.IsNullOrEmpty(_filter) ?
                    _identifiersAsset.Identifiers.Where(option => option.Contains(_filter)) :
                    _identifiersAsset.Identifiers);

            selectedIndex = options.IndexOf(currentValue);
        }
    }
}