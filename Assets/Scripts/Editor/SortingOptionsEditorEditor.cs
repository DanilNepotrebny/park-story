﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor
{
    [CustomEditor(typeof(SortingOptionsEditor), true)]
    public class SortingOptionsEditorEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            EditorGUILayout.HelpBox("This component is auxiliary and will not be saved.", MessageType.Error);

            SortingOptionsEditor targetObject = (SortingOptionsEditor)target;
            Renderer[] renderers = targetObject.GetComponentsInChildren<Renderer>(true);

            EditorGUI.BeginChangeCheck();
            base.OnInspectorGUI();

            if (EditorGUI.EndChangeCheck())
            {
                foreach (Renderer r in renderers)
                {
                    r.sortingLayerID = targetObject.SortingLayer;
                    r.sortingOrder = targetObject.SortingOrder;
                }
            }

            foreach (Renderer r in renderers)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.LabelField(r.name);
                EditorGUI.indentLevel++;
                EditorGUILayout.LabelField($"SortingLayer: {r.sortingLayerName}");
                EditorGUILayout.LabelField($"SortingOrder: {r.sortingOrder}");
                EditorGUI.indentLevel--;
                EditorGUILayout.Space();
                EditorGUI.indentLevel--;
            }
        }
    }
}