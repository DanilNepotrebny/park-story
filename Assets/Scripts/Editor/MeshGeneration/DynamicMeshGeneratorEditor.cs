﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.MeshGeneration;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace DreamTeam.Editor.MeshGeneration
{
    [CustomEditor(typeof(DynamicMeshGenerator))]
    public class DynamicMeshGeneratorEditor : UnityEditor.Editor
    {
        private DynamicMeshGenerator.IEditorAPI _meshGenerator => target as DynamicMeshGenerator.IEditorAPI;

        private ReorderableList _objects;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            bool isInitListRequired = false;

            _objects.DoLayoutList();

            if (GUILayout.Button("Add mesh point"))
            {
                GameObject newPoint = new GameObject($"Mesh point {_meshGenerator.MeshObjects.Count}");
                newPoint.transform.SetParent(_meshGenerator.CachedTransform);
                newPoint.transform.localPosition = Vector3.zero;

                newPoint.AddComponent<DynamicMeshObject>();
                isInitListRequired = true;
            }

            for (int i = 0; i < _objects.list.Count; i++)
            {
                DynamicMeshObject meshObject = _objects.list[i] as DynamicMeshObject;
                if (meshObject == null || meshObject.transform.GetSiblingIndex() != i)
                {
                    meshObject?.transform.SetSiblingIndex(i);
                    isInitListRequired = true;
                }
            }

            if (isInitListRequired)
            {
                _meshGenerator.Refresh(); 
                InitList();
            }
        }

        protected void OnEnable()
        {
            InitList();
        }

        private void InitList()
        {
            _objects = new ReorderableList(_meshGenerator.MeshObjects, typeof(DynamicMeshObject), true, true, false, false);
            _objects.drawElementBackgroundCallback = DrawElementBackgroundCallback;
            _objects.drawHeaderCallback = DrawHeaderCallback;
            _objects.drawElementCallback += DrawElementCallback;
        }

        private void DrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            GUI.enabled = false;
            Rect elementRect = new Rect(new Vector2(rect.position.x, rect.position.y + 1f), new Vector2(rect.size.x, EditorGUIUtility.singleLineHeight));
            EditorGUI.ObjectField(elementRect, _meshGenerator.MeshObjects[index], typeof(DynamicMeshObject), true);
            GUI.enabled = true;
        }

        private void DrawHeaderCallback(Rect rect)
        {
            GUI.Label(rect, "Mesh points");
        }

        private void DrawElementBackgroundCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            Rect elementRect = new Rect(new Vector2(rect.position.x + 3f, rect.position.y), new Vector2(rect.size.x - 7f, rect.size.y - 3f));
            Color32 color = isFocused ? new Color32(71, 71, 71, 167) : new Color32(71, 71, 71, 127);
            EditorGUI.DrawRect(elementRect, color);
        }
    }
}