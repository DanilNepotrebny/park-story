﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI.SymbolAnimation;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.SymbolAnimation
{
    [CustomEditor(typeof(AnimatedSymbolsCreator))]
    public class AnimatedSymbolsCreatorEditor : UnityEditor.Editor
    {
        private new AnimatedSymbolsCreator target => base.target as AnimatedSymbolsCreator;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Separator();

            bool isGUIEnabled = GUI.enabled;

            GUI.enabled = Application.isPlaying && target.State == AnimatedSymbolsCreator.AnimationState.Hidden;
            if (GUILayout.Button("Show symbols"))
            {
                target.StartCoroutine(target.Show());
            }

            GUI.enabled = Application.isPlaying && target.State == AnimatedSymbolsCreator.AnimationState.Idle;
            if (GUILayout.Button("Hide symbols"))
            {
                target.StartCoroutine(target.Hide());
            }

            GUI.enabled = isGUIEnabled;
        }
    }
}