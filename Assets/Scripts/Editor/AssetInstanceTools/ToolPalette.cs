﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Editor.AssetInstanceTools
{
    public class ToolPalette
    {
        private readonly BaseTool[] _creators;
        private Object _currentAsset;

        public BaseTool CurrentTool { get; private set; }

        public ToolPalette(BaseTool[] creators)
        {
            _creators = creators;
        }

        public GameObject UpdatePreview(Vector3 position)
        {
            return CurrentTool?.UpdatePreview(_currentAsset, position);
        }

        public void ApplyTool(Vector3 position, [CanBeNull] ILayer layer = null)
        {
            CurrentTool?.Apply(_currentAsset, position, layer);
        }

        public void ContinueApplyingTool(Vector3 position, [CanBeNull] ILayer layer = null)
        {
            CurrentTool?.ContinueApplying(_currentAsset, position, layer);
        }

        public void ActivateTool([NotNull] Object asset)
        {
            DeactivateTool();

            _currentAsset = asset;
            CurrentTool = FindAssetInstanceCreator(asset);
            if (CurrentTool != null)
            {
                CurrentTool.Activate();
            }
            else
            {
                UnityEngine.Debug.LogWarning($"Didn't find proper tool for the chosen {asset.name} asset");
            }
        }

        public void DeactivateTool()
        {
            CurrentTool?.Deactivate();
            _currentAsset = null;
        }

        private BaseTool FindAssetInstanceCreator([NotNull] Object asset)
        {
            return _creators.FirstOrDefault(creator => creator.IsSupported(asset));
        }
    }
}