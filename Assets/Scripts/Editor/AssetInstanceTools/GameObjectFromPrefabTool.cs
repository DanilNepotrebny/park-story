﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.AssetInstanceTools
{
    public class GameObjectFromPrefabTool : GameObjectTool
    {
        public override bool IsSupported(Object asset)
        {
            PrefabType prefabType = PrefabUtility.GetPrefabType(asset);
            bool isSupported = base.IsSupported(asset);
            isSupported &= prefabType == PrefabType.Prefab || prefabType == PrefabType.ModelPrefab;
            return isSupported;
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return false;
        }
    }
}