﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.AssetInstanceTools
{
    public abstract class BaseTool
    {
        private readonly List<Vector3> _appliedPositions = new List<Vector3>();

        protected GameObject _preview;
        protected Object _lastPreviewAsset;

        /// <summary>
        /// Checks whether this asset supported by this InstanceCreator.
        /// </summary>
        public abstract bool IsSupported([NotNull] Object asset);

        protected abstract string UndoMessageForCreate { get; }

        protected bool IsInContinuousMode => _appliedPositions.Count > 0;
        protected Vector3 LastPosition => _appliedPositions.Last();

        /// <summary>
        /// Creates GameObject in the scene from the asset.
        /// </summary>
        public virtual bool Apply([NotNull] Object asset, Vector3 position, [CanBeNull] ILayer layer = null)
        {
            _appliedPositions.Clear();

            return ApplyInternal(asset, position, layer);
        }

        public virtual bool ContinueApplying([NotNull] Object asset, Vector3 position, [CanBeNull] ILayer layer = null)
        {
            if (_appliedPositions.Contains(position))
            {
                return false;
            }

            return ApplyInternal(asset, position, layer);
        }

        public virtual void Erase(Vector3 position, [CanBeNull] ILayer layer = null)
        {
            // nothing to do
        }

        public virtual void Modify(Object asset, Vector3 position, [CanBeNull] ILayer layer = null)
        {
            // nothing to do
        }

        public virtual void Activate()
        {
            // nothing to do
        }

        public virtual void Deactivate()
        {
            DestroyPreview();
        }

        public virtual GameObject UpdatePreview([NotNull] Object asset, Vector3 position)
        {
            if (_preview == null || _lastPreviewAsset != asset || NeedReconstructPreview(asset, position))
            {
                _preview = ConstructPreview(asset, position);
            }

            _lastPreviewAsset = asset;

            return _preview;
        }

        protected virtual GameObject ConstructPreview([NotNull] Object asset, Vector3 position)
        {
            DestroyPreview();

            _preview = ConstructGameObject(asset, position);
            _preview.hideFlags = HideFlags.HideAndDontSave;

            return _preview;
        }

        protected virtual bool NeedReconstructPreview([NotNull] Object asset, Vector3 position)
        {
            return false;
        }

        protected virtual bool CanCreate(Object asset, Vector3 position, [CanBeNull] ILayer layer = null)
        {
            return true;
        }

        protected abstract bool CanModify(Object asset, Vector3 position, [CanBeNull] ILayer layer = null);

        protected virtual void OnApplyFailed()
        {
            // nothing to do
        }

        /// <summary>
        /// Constructs GameObject without setting it up.
        /// </summary>
        protected abstract GameObject ConstructGameObject([NotNull] Object asset, Vector3 position);

        /// <summary>
        /// Setups the GameObject using the asset.
        /// </summary>
        protected virtual void SetupGameObject(GameObject go, Object asset)
        {
            // nothing to do
        }

        private bool ApplyInternal([NotNull] Object asset, Vector3 position, [CanBeNull] ILayer layer = null)
        {
            // Remove preview so it may not be accidentally found by GetComponent in tool logics.
            DestroyPreview();

            bool hasSucceeded = false;

            if (CanModify(asset, position, layer))
            {
                Modify(asset, position, layer);
                hasSucceeded = true;
            }
            else if (CanCreate(asset, position, layer))
            {
                GameObject go = ConstructGameObject(asset, position);
                SetupGameObject(go, asset);

                layer?.Add(go);

                Undo.RegisterCreatedObjectUndo(go, UndoMessageForCreate);

                hasSucceeded = true;
            }

            if (hasSucceeded)
            {
                _appliedPositions.Add(position);
            }
            else
            {
                OnApplyFailed();
            }

            return hasSucceeded;
        }

        private void DestroyPreview()
        {
            if (_preview != null)
            {
                Object.DestroyImmediate(_preview.gameObject);
            }
        }
    }
}