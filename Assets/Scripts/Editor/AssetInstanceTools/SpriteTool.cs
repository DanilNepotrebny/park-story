﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Linq;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.AssetInstanceTools
{
    public class SpriteTool : EmptyObjectTool
    {
        /// <summary>
        /// Checks whether this asset supported by this InstanceCreator.
        /// </summary>
        /// <inheritdoc />
        public override bool IsSupported(Object asset)
        {
            return base.IsSupported(asset) && GetSprite(asset) != null;
        }

        protected override string UndoMessageForCreate => "Sprite Creation";

        protected override GameObject ConstructGameObject(Object asset, Vector3 position)
        {
            GameObject go = base.ConstructGameObject(asset, position);

            SpriteRenderer sr = go.AddComponent<SpriteRenderer>();
            sr.sprite = GetSprite(asset);

            return go;
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return false;
        }

        private static Sprite GetSprite(Object asset)
        {
            if (!(asset is Texture2D))
            {
                return asset as Sprite;
            }

            string spriteSheet = AssetDatabase.GetAssetPath(asset);
            Sprite[] sprites = AssetDatabase.LoadAllAssetsAtPath(spriteSheet).OfType<Sprite>().ToArray();

            return sprites.Length == 1 ? sprites[0] : null;
        }
    }
}