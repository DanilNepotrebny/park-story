﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Editor.PositionTransformers;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.AssetInstanceTools
{
    public class EraseCellGameObjectsTool : BaseTool
    {
        public GridTransformer GridTransformer;

        public override void Erase(Vector3 position, ILayer layer = null)
        {
            base.Erase(position, layer);

            if (layer == null || GridTransformer == null)
            {
                return;
            }

            List<GameObject> objToRemove = (from t in layer.Children
                let transformedPos = GridTransformer.GetTransformed(t.position, layer)
                where Mathf.Approximately(position.x, transformedPos.x) &&
                    Mathf.Approximately(position.y, transformedPos.y) &&
                    Mathf.Approximately(position.z, transformedPos.z)
                select t.gameObject).ToList();

            Undo.IncrementCurrentGroup();
            int groupIndex = Undo.GetCurrentGroup();
            // ReSharper disable once ForCanBeConvertedToForeach
            for (int i = 0; i < objToRemove.Count; ++i)
            {
                GameObject obj = objToRemove[i];
                layer.Remove(obj);
                Undo.DestroyObjectImmediate(obj);
            }

            Undo.CollapseUndoOperations(groupIndex);
        }

        public override bool IsSupported(Object asset)
        {
            return asset is GameObject;
        }

        protected override string UndoMessageForCreate => "Erase metagame game object";

        protected override GameObject ConstructGameObject(Object asset, Vector3 position)
        {
            return null;
        }

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            return false;
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return false;
        }
    }
}