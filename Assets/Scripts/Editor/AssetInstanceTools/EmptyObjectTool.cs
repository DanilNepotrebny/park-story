﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.AssetInstanceTools
{
    public abstract class EmptyObjectTool : BaseTool
    {
        /// <summary>
        /// Checks whether this asset supported by this InstanceCreator.
        /// </summary>
        /// <inheritdoc />
        public override bool IsSupported(Object asset)
        {
            return PrefabUtility.GetPrefabType(asset) == PrefabType.None;
        }

        /// <summary>
        /// Constructs GameObject without setting it up.
        /// </summary>
        /// <inheritdoc />
        protected override GameObject ConstructGameObject(Object asset, Vector3 position)
        {
            var go = new GameObject(asset.name);
            go.transform.position = position;
            return go;
        }
    }
}