﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DreamTeam.Editor.AssetInstanceTools
{
    public abstract class GameObjectTool : BaseTool
    {
        /// <summary>
        /// Checks whether this asset supported by this InstanceCreator.
        /// </summary>
        /// <inheritdoc />
        public override bool IsSupported(Object asset)
        {
            return asset is GameObject;
        }

        protected override string UndoMessageForCreate => "GameObject Creation";

        /// <summary>
        /// Constructs GameObject without setting it up.
        /// </summary>
        /// <inheritdoc />
        protected override GameObject ConstructGameObject(Object asset, Vector3 position)
        {
            if (asset == null)
            {
                return new GameObject();
            }

            GameObject go = null;
            PrefabType prefabType = PrefabUtility.GetPrefabType(asset);
            switch (prefabType)
            {
                case PrefabType.Prefab:
                case PrefabType.ModelPrefab:
                    go = (GameObject)PrefabUtility.InstantiatePrefab(asset);
                    break;

                case PrefabType.PrefabInstance:
                case PrefabType.ModelPrefabInstance:
                    go = (GameObject)PrefabUtility.InstantiatePrefab(PrefabUtility.GetCorrespondingObjectFromSource(asset));
                    break;

                case PrefabType.None:
                case PrefabType.MissingPrefabInstance:
                case PrefabType.DisconnectedPrefabInstance:
                case PrefabType.DisconnectedModelPrefabInstance:
                    go = Object.Instantiate(asset as GameObject);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            go.transform.position = position;
            return go;
        }
    }
}