﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using Google.GData.Client;
using Google.GData.Spreadsheets;
using Newtonsoft.Json.Linq;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.LocalizationImport
{
    public class LocalizationImportAPI
    {
        private LocalizationImportOptions _importOptions;

        private bool _isFinished = false;
        private SpreadsheetsService _service;
        private OAuth2Parameters _authParameters;
        private readonly object _apiThreadLock = new object();
        private Thread _apiThread;
        private Queue<LocalizationImportAPICommandBase> _apiCommands = new Queue<LocalizationImportAPICommandBase>();
        private Queue<LocalizationImportAPICommandBase> _completedApiCommands = new Queue<LocalizationImportAPICommandBase>();

        private const string _accessTokenKey = "LocalizationImportAccessToken";
        private const string _refreshTokenKey = "LocalizationImportRefreshToken";
        private const string _refreshTimeOutKey = "LocalizationImportRefreshTimeout";

        private string _accessToken
        {
            get { return EditorPrefs.GetString(_accessTokenKey, string.Empty); }
            set { EditorPrefs.SetString(_accessTokenKey, value); }
        }

        private string _refreshToken
        {
            get { return EditorPrefs.GetString(_refreshTokenKey, string.Empty); }
            set { EditorPrefs.SetString(_refreshTokenKey, value); }
        }

        private DateTime _refreshTimeOut
        {
            get
            {
                DateTime result = DateTime.Now;
                if (EditorPrefs.HasKey(_refreshTimeOutKey))
                {
                    string refreshTimeOutString = EditorPrefs.GetString(_refreshTimeOutKey, DateTime.Now.ToString(CultureInfo.InvariantCulture));
                    result = DateTime.Parse(refreshTimeOutString, CultureInfo.InvariantCulture);
                }
                return result;
            }
            set
            {
                EditorPrefs.SetString(_refreshTimeOutKey, value.ToString(CultureInfo.InvariantCulture));
            }
        }

        public LocalizationImportAPI()
        {
            _importOptions = LocalizationImportOptions.GetOptionsAsset();
            _authParameters = CreateAuthParameters();

            if (HasAccessToken())
            {
                _service = new SpreadsheetsService("dreamteam-localization-import")
                {
                    RequestFactory = new GOAuth2RequestFactory("structuredcontent", "dreamteam-localization-import", _authParameters)
                };
            }

            _apiThread = new Thread(APIThread);
            _apiThread.Start();
        }

        public void Finish()
        {
            _isFinished = true;
            _apiThread.Join();
        }

        public void Update()
        {
            lock (_apiThreadLock)
            {
                while (_completedApiCommands.Count > 0)
                {
                    var command = _completedApiCommands.Dequeue();
                    command.PostProcess();
                }
            }
        }

        public bool HasAccessToken()
        {
            return !string.IsNullOrEmpty(_accessToken);
        }

        public void OpenAuthUrl()
        {
            Application.OpenURL(OAuthUtil.CreateOAuth2AuthorizationUrl(_authParameters));
        }

        public void Login(string userToken)
        {
            _authParameters.AccessCode = userToken;
            OAuthUtil.GetAccessToken(_authParameters);

            if (!string.IsNullOrEmpty(_authParameters.AccessToken))
            {
                _accessToken = _authParameters.AccessToken;
                _refreshToken = _authParameters.RefreshToken;
                _refreshTimeOut = _authParameters.TokenExpiry;

                _service = new SpreadsheetsService("dreamteam-localization-import")
                {
                    RequestFactory = new GOAuth2RequestFactory("structuredcontent", "dreamteam-localization-import", _authParameters)
                };
            }
            else
            {
                Logout();
            }
        }

        public void Logout()
        {
            _accessToken = string.Empty;
            _refreshToken = string.Empty;
            _refreshTimeOut = DateTime.Now;

            _service = null;
        }

        public void RetrieveSpreadSheets(Action<List<SpreadsheetEntry>> successCallback, Action failCallback)
        {
            _apiCommands.Enqueue(
                new LocalizationImportAPIRetrieveSpreadSheetsCommand(
                    _service,
                    successCallback,
                    () =>
                    {
                        Logout();
                        failCallback?.Invoke();
                    })
                );
        }

        public void ImportWorkSheet(WorksheetEntry workSheet, string importPath, Action<List<string>> successCallback, Action failCallback)
        {
            _apiCommands.Enqueue(
                new LocalizationImportAPIImportCommand(
                    _service,
                    workSheet,
                    importPath,
                    successCallback,
                    () =>
                    {
                        Logout();
                        failCallback?.Invoke();
                    })
                );
        }

        private void APIThread()
        {
            while (!_isFinished)
            {
                bool hasCommands;
                lock (_apiThreadLock)
                {
                    hasCommands = _apiCommands.Count > 0;
                }

                if (hasCommands)
                {
                    LocalizationImportAPICommandBase command;
                    lock (_apiThreadLock)
                    {
                        command = _apiCommands.Dequeue();
                    }

                    command.Process();

                    lock (_apiThreadLock)
                    {
                        _completedApiCommands.Enqueue(command);
                    }
                }
            }
        }

        private OAuth2Parameters CreateAuthParameters()
        {
            return new OAuth2Parameters
            {
                ClientId = GetClientID(),
                ClientSecret = GetClientSecret(),
                RedirectUri = GetRedirectUri(),
                Scope = "https://spreadsheets.google.com/feeds",
                RefreshToken = _refreshToken,
                AccessToken = _accessToken,
                AccessType = "offline",
                TokenType = "refresh"
            };
        }

        private JObject GetClientSettingsRoot()
        {
            return _importOptions.ClientData["installed"] as JObject;
        }

        private string GetClientID()
        {
            return GetClientSettingsRoot()["client_id"].Value<string>();
        }

        private string GetClientSecret()
        {
            return GetClientSettingsRoot()["client_secret"].Value<string>();
        }

        private string GetRedirectUri()
        {
            return GetClientSettingsRoot()["redirect_uris"][0].Value<string>();
        }
    }
}