﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using System.IO;
using DreamTeam.Utils;
using Google.GData.Spreadsheets;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.LocalizationImport
{
    public class LocalizationImportWindow : EditorWindow
    {
        private static readonly Vector2 _windowSize = new Vector2(300f, 150f);

        private LocalizationImportAPI _api;

        private bool _isFocused;
        private WindowState _state = WindowState.NotAuthorized;

        private LocalizationImportOptions _importOptions;
        private string _userToken;
        private List<SpreadsheetEntry> _availableSpreadSheets = new List<SpreadsheetEntry>();
        private List<string> _availableSpreadSheetNames = new List<string>();
        private int _selectedSpreadSheetIndex;
        private SpreadsheetEntry _selectedSpreadSheet;
        private List<WorksheetEntry> _availableWorkSheets = new List<WorksheetEntry>();
        private List<string> _availableWorkSheetNames = new List<string>();
        private int _selectedWorkSheetIndex;
        private WorksheetEntry _selectedWorkSheet;

        [MenuItem("Tools/Localization import")]
        public static void ShowWindow()
        {
            EditorWindow window = GetWindow(typeof(LocalizationImportWindow));
            window.titleContent.text = "Localization";
            window.minSize = _windowSize;
            window.maxSize = _windowSize;

            Rect position = window.position;
            window.position = position;
            position.center =
                new Rect(0f, 0f, UnityEngine.Screen.currentResolution.width, UnityEngine.Screen.currentResolution.height).
                    center;
        }

        protected void OnGUI()
        {
            GUI.enabled = _isFocused;

            if (_importOptions != null)
            {
                switch (_state)
                {
                    case WindowState.NotAuthorized:
                        DrawNotAuthorized();
                        break;

                    case WindowState.NotLoggedIn:
                        DrawNotLoggedIn();
                        break;

                    case WindowState.RetrieveWorkBooksInProgress:
                        DrawRetrieveWorkBooksInProgress();
                        break;

                    case WindowState.LocalizationSheetIsNotSelected:
                        DrawLocalizationSheetIsNotSelected();
                        break;

                    case WindowState.Import:
                        DrawImport();
                        break;

                    case WindowState.ImportInProgress:
                        DrawImportInProgress();
                        break;
                }
            }
            else
            {
                GUIStyle labelStyle = new GUIStyle(EditorStyles.textArea);
                labelStyle.alignment = TextAnchor.MiddleCenter;
                labelStyle.fontSize = 20;
                EditorGUILayout.LabelField("Import options is not loaded...", labelStyle);

                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();

                if (GUILayout.Button("Reset import options"))
                {
                    _importOptions = LocalizationImportOptions.GetOptionsAsset();
                }

                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
            }

            GUI.enabled = true;
        }

        protected void Awake()
        {
            _importOptions = LocalizationImportOptions.GetOptionsAsset();
            _api = new LocalizationImportAPI();

            InitWindowState();
        }

        protected void OnDestroy()
        {
            if (_api != null)
            {
                _api.Finish();
                _api = null;
            }
        }

        protected void OnFocus()
        {
            _isFocused = true;
        }

        protected void OnLostFocus()
        {
            _isFocused = false;
        }

        protected void Update()
        {
            if (_api == null)
            {
                _api = new LocalizationImportAPI();
            }

            _api?.Update();
        }

        private void InitWindowState()
        {
            _state = WindowState.NotAuthorized;

            if (_api.HasAccessToken())
            {
                _api.RetrieveSpreadSheets(OnSpreadSheetsRetrieved, OnSpreadSheetFailedToRetrieve);
                _state = WindowState.RetrieveWorkBooksInProgress;
            }
        }

        private void DrawNotAuthorized()
        {
            DrawHeader("Authorization");
            GUILayout.FlexibleSpace();

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Authorize with google"))
            {
                _api.OpenAuthUrl();
                _state = WindowState.NotLoggedIn;
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        private void DrawNotLoggedIn()
        {
            DrawHeader("Waiting for login...");
            GUILayout.FlexibleSpace();

            EditorGUILayout.BeginHorizontal();

            _userToken = EditorGUILayout.TextField("OAuth Token: ", _userToken);

            GUI.enabled = !string.IsNullOrEmpty(_userToken) && _isFocused;

            if (GUILayout.Button("Login"))
            {
                _api.Login(_userToken);
                _userToken = string.Empty;

                if (_api.HasAccessToken())
                {
                    _api.RetrieveSpreadSheets(OnSpreadSheetsRetrieved, OnSpreadSheetFailedToRetrieve);
                    _state = WindowState.RetrieveWorkBooksInProgress;
                }
                else
                {
                    UnityEngine.Debug.LogWarning("Login was not successful! Make sure key is correct!");
                }
            }

            GUI.enabled = _isFocused;

            EditorGUILayout.EndHorizontal();
        }

        private void DrawRetrieveWorkBooksInProgress()
        {
            GUI.enabled = false;

            DrawHeader("Retrieving account spread sheets...");

            GUI.enabled = _isFocused;
        }

        private void DrawLocalizationSheetIsNotSelected()
        {
            DrawHeader("Please select localization sheet");
            GUILayout.FlexibleSpace();

            DrawSheetSelection();

            GUI.enabled = false;
            DrawImportPath();
            DrawImportButton();
            GUI.enabled = _isFocused;

            GUILayout.FlexibleSpace();
            DrawLogoutButton();
        }

        private void DrawImport()
        {
            DrawHeader("Import localization");
            GUILayout.FlexibleSpace();

            DrawSheetSelection();
            DrawImportPath();

            GUI.color = Color.green;
            DrawImportButton();
            GUI.color = Color.white;

            GUILayout.FlexibleSpace();
            DrawLogoutButton();
        }

        private void DrawImportInProgress()
        {
            GUI.enabled = false;

            DrawHeader("Import in progress...");

            GUI.enabled = _isFocused;
        }

        private void DrawHeader(string header)
        {
            GUIStyle labelStyle = new GUIStyle(EditorStyles.textArea)
            {
                alignment = TextAnchor.MiddleCenter,
            };
            EditorGUILayout.LabelField(header, labelStyle);
        }

        private void DrawSheetSelection()
        {
            GUILayout.BeginHorizontal();

            int newSpreadSheetIndex = EditorGUILayout.Popup(_selectedSpreadSheetIndex, _availableSpreadSheetNames.ToArray());
            if (newSpreadSheetIndex != _selectedSpreadSheetIndex)
            {
                _selectedSpreadSheetIndex = newSpreadSheetIndex;
                if (_selectedSpreadSheetIndex > 0)
                {
                    _selectedSpreadSheet = _availableSpreadSheets[_selectedSpreadSheetIndex - 1];
                    FillWorkSheetNamesList();
                }
                else
                {
                    _selectedSpreadSheet = null;
                }

                _state = WindowState.LocalizationSheetIsNotSelected;
            }

            GUILayout.EndHorizontal();

            GUI.enabled = _selectedSpreadSheetIndex > 0 && _isFocused;

            GUILayout.BeginHorizontal();

            if (_availableWorkSheetNames.Count == 0)
            {
                _availableWorkSheetNames.Add("None");
            }

            int newWorkSheetIndex = EditorGUILayout.Popup(_selectedWorkSheetIndex, _availableWorkSheetNames.ToArray());
            if (newWorkSheetIndex != _selectedWorkSheetIndex)
            {
                _selectedWorkSheetIndex = newWorkSheetIndex;
                if (_selectedWorkSheetIndex > 0)
                {
                    _selectedWorkSheet = _availableWorkSheets[_selectedWorkSheetIndex - 1];
                    _state = WindowState.Import;
                }
                else
                {
                    _selectedWorkSheet = null;
                    _state = WindowState.LocalizationSheetIsNotSelected;
                }
            }

            GUILayout.EndHorizontal();

            GUI.enabled = _isFocused;
        }

        private void DrawLogoutButton()
        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.FlexibleSpace();
            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Logout"))
            {
                _api.Logout();

                _selectedSpreadSheetIndex = 0;
                _selectedSpreadSheet = null;
                _selectedWorkSheetIndex = 0;
                _selectedWorkSheet = null;

                _state = WindowState.NotAuthorized;
            }

            GUILayout.EndHorizontal();
        }

        private void DrawImportPath()
        {
            GUILayout.BeginHorizontal();

            if (string.IsNullOrEmpty(_importOptions.ImportPath))
            {
                _importOptions.ImportPath = "/.";
                EditorUtils.RecordObjectUndo(_importOptions, "Stored localization import data");
            }

            EditorGUILayout.LabelField($"Assets{_importOptions.ImportPath}");
            if (GUILayout.Button("Select folder"))
            {
                string path = EditorUtility.OpenFolderPanel("Select import folder", Application.dataPath + _importOptions.ImportPath, "/.");
                if (string.IsNullOrEmpty(path))
                {
                    path = Path.GetFullPath(_importOptions.ImportPath);
                }

                _importOptions.ImportPath = IsProjectSubPath(path) ? GetProjectRelativePath(path) : "/.";
                EditorUtils.RecordObjectUndo(_importOptions, "Stored localization import data");
            }

            GUILayout.EndHorizontal();
        }

        private void DrawImportButton()
        {
            if (GUILayout.Button("Import localization"))
            {
                _api.ImportWorkSheet(_selectedWorkSheet, Application.dataPath + _importOptions.ImportPath, OnImportSuccess, OnImportFail);
                _state = WindowState.ImportInProgress;
            }
        }

        private void FillWorkSheetNamesList()
        {
            _availableWorkSheets.Clear();
            _availableWorkSheetNames.Clear();
            _selectedWorkSheetIndex = 0;

            _availableWorkSheets = _selectedSpreadSheet.Worksheets.Entries.Select(entry => entry as WorksheetEntry).ToList();
            _availableWorkSheetNames = _availableWorkSheets.Select(workSheet => workSheet.Title.Text).ToList();
            _availableWorkSheetNames.Insert(0, "None");
        }

        private void OnSpreadSheetsRetrieved(List<SpreadsheetEntry> entries)
        {
            _availableSpreadSheetNames.Clear();
            _selectedSpreadSheetIndex = 0;

            _availableSpreadSheets = new List<SpreadsheetEntry>(entries);
            _availableSpreadSheetNames = _availableSpreadSheets.Select(spreadSheet => spreadSheet.Title.Text).ToList();
            _availableSpreadSheetNames.Insert(0, "None");

            if (!string.IsNullOrEmpty(_importOptions.LocalizationSpreadSheetName) && _availableSpreadSheetNames.Contains(_importOptions.LocalizationSpreadSheetName))
            {
                _selectedSpreadSheetIndex = _availableSpreadSheetNames.IndexOf(_importOptions.LocalizationSpreadSheetName);
                _selectedSpreadSheet = _availableSpreadSheets[_selectedSpreadSheetIndex - 1];

                FillWorkSheetNamesList();

                if (!string.IsNullOrEmpty(_importOptions.LocalizationWorkSheetName) && _availableWorkSheetNames.Contains(_importOptions.LocalizationWorkSheetName))
                {
                    _selectedWorkSheetIndex = _availableWorkSheetNames.IndexOf(_importOptions.LocalizationWorkSheetName);
                    _selectedWorkSheet = _availableWorkSheets[_selectedWorkSheetIndex - 1];

                    _state = WindowState.Import;
                }
                else
                {
                    _importOptions.LocalizationWorkSheetName = string.Empty;
                    _selectedWorkSheetIndex = 0;
                    _selectedWorkSheet = null;

                    _state = WindowState.LocalizationSheetIsNotSelected;
                    EditorUtils.RecordObjectUndo(_importOptions, "Stored localization import data");
                }
            }
            else
            {
                _importOptions.LocalizationSpreadSheetName = string.Empty;
                _importOptions.LocalizationWorkSheetName = string.Empty;
                _selectedSpreadSheetIndex = 0;
                _selectedSpreadSheet = null;
                _selectedWorkSheetIndex = 0;
                _selectedWorkSheet = null;

                _state = WindowState.LocalizationSheetIsNotSelected;
                EditorUtils.RecordObjectUndo(_importOptions, "Stored localization import data");
            }

            Repaint();
        }

        private void OnSpreadSheetFailedToRetrieve()
        {
            _state = WindowState.NotAuthorized;

            Repaint();
        }

        private void OnImportSuccess(List<string> availableKeys)
        {
            _importOptions.LocalizationSpreadSheetName = _selectedSpreadSheet.Title.Text;
            _importOptions.LocalizationWorkSheetName = _selectedWorkSheet.Title.Text;

            EditorUtils.RecordObjectUndo(_importOptions, "Stored localization import data");
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
            AssetDatabase.SaveAssets();

            _state = WindowState.Import;
        }

        private void OnImportFail()
        {
            _state = WindowState.NotAuthorized;

            Repaint();
        }

        private bool IsProjectSubPath(string path)
        {
            bool result = false;
            DirectoryInfo projectDirInfo = new DirectoryInfo(Application.dataPath);
            DirectoryInfo customPathInfo = new DirectoryInfo(path);
            while (customPathInfo.Parent != null)
            {
                if (customPathInfo.Parent.FullName == projectDirInfo.FullName)
                {
                    result = true;
                    break;
                }
                else
                {
                    customPathInfo = customPathInfo.Parent;
                }
            }

            return result;
        }

        private string GetProjectRelativePath(string path)
        {
            return path.Substring(Application.dataPath.Length);
        }

        private enum WindowState
        {
            NotAuthorized,
            NotLoggedIn,
            RetrieveWorkBooksInProgress,
            LocalizationSheetIsNotSelected,
            Import,
            ImportInProgress
        }
    }
}