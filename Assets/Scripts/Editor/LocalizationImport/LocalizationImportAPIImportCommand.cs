﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Google.GData.Spreadsheets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DreamTeam.Editor.LocalizationImport
{
    public class LocalizationImportAPIImportCommand : LocalizationImportAPICommandBase
    {
        private WorksheetEntry _worksheet;
        private string _importPath;
        private Action<List<string>> _successDelegate;
        private Action _failDelegate;
        private List<string> _availableKeys = new List<string>();
        private Status _status = Status.InProgress;

        public LocalizationImportAPIImportCommand(
            SpreadsheetsService service,
            WorksheetEntry worksheet,
            string importPath,
            Action<List<string>> successDelegate,
            Action failDelegate) : base(service)
        {
            _worksheet = worksheet;
            _importPath = importPath;
            _successDelegate = successDelegate;
            _failDelegate = failDelegate;
        }

        public override void Process()
        {
            try
            {
                uint maxRow = 0;
                uint maxColumn = 0;
                QuerySheetSize(ref maxRow, ref maxColumn);

                if (maxRow > 1 && maxColumn > 2)
                {
                    List<CellEntry> cells = new List<CellEntry>();
                    QueryCells(maxRow, maxColumn, ref cells);
                    ImportLanguages(cells);
                    _status = Status.Success;
                }
                else
                {
                    _status = Status.Fail;
                }
            }
            catch (Exception)
            {
                _status = Status.Fail;
            }
        }

        public override void PostProcess()
        {
            switch (_status)
            {
                case Status.Success:
                    _successDelegate?.Invoke(_availableKeys);
                    break;

                case Status.Fail:
                    UnityEngine.Debug.LogError("Exception! Reauthorize required!");
                    _failDelegate?.Invoke();
                    break;
            }
        }

        private void QuerySheetSize(ref uint maxRow, ref uint maxColumn)
        {
            CellQuery query = new CellQuery(_worksheet.CellFeedLink);
            CellFeed feed = _service.Query(query);

            for (int i = 0; i < feed.Entries.Count; ++i)
            {
                CellEntry cellEntry = feed.Entries[i] as CellEntry;
                if (cellEntry != null)
                {
                    if (cellEntry.Row > maxRow)
                    {
                        maxRow = cellEntry.Row;
                    }

                    if (cellEntry.Column > maxColumn)
                    {
                        maxColumn = cellEntry.Column;
                    }
                }
            }
        }

        private void QueryCells(uint maxRow, uint maxColumn, ref List<CellEntry> cells)
        {
            var query = new CellQuery(_worksheet.CellFeedLink)
            {
                ReturnEmpty = ReturnEmptyCells.yes,
                MaximumRow = maxRow,
                MaximumColumn = maxColumn
            };
            CellFeed feed = _service.Query(query);

            for (int i = 0; i < feed.Entries.Count; ++i)
            {
                CellEntry cell = feed.Entries[i] as CellEntry;
                if (cell == null)
                {
                    continue;
                }

                cells.Add(cell);
            }
        }

        private void ImportLanguages(List<CellEntry> cells)
        {
            const int initialIndex = 1;

            _availableKeys = cells
                .Where(
                    cell => !string.IsNullOrEmpty(cell.Value) &&
                    cell.Column == initialIndex &&
                    cell.Row != initialIndex)
                .Select(cell => cell.Value)
                .ToList();

            List<CellEntry> languages = cells.Where(cell => cell.Row == initialIndex && cell.Column != initialIndex).ToList();
            List<CellEntry> keys = cells.Where(cell => cell.Column == initialIndex && cell.Row != initialIndex).ToList();

            foreach (CellEntry languageCell in languages)
            {
                List<CellEntry> languageValues = cells.Where(cell => cell.Row > initialIndex && cell.Column == languageCell.Column).ToList();
                ImportLanguage(languageCell.Value, keys, languageValues);
            }
        }

        private void ImportLanguage(string language, List<CellEntry> keys, List<CellEntry> values)
        {
            JObject localization = new JObject();
            for (int i = 0; i < keys.Count; i++)
            {
                if (!string.IsNullOrEmpty(keys[i].Value))
                {
                    string val = string.IsNullOrEmpty(values[i].Value) ? "NOT FOUND" : values[i].Value;
                    JToken value = new JValue(Regex.Unescape(val));
                    localization.Add(keys[i].Value, value);
                }
            }

            if (!Directory.Exists(_importPath))
            {
                Directory.CreateDirectory(_importPath);
            }

            string filePath = _importPath + Path.DirectorySeparatorChar + $"{language}.json";
            using (FileStream fileStream = File.Create(filePath))
            {
                using (StreamWriter streamWriter = new StreamWriter(fileStream))
                {
                    using (JsonTextWriter jsonWriter = new JsonTextWriter(streamWriter))
                    {
                        jsonWriter.Formatting = Formatting.Indented;
                        localization.WriteTo(jsonWriter);
                    }
                }
            }
        }

        private enum Status
        {
            InProgress,
            Success,
            Fail
        }
    }
}