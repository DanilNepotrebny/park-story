﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Newtonsoft.Json.Linq;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.LocalizationImport
{
    [CreateAssetMenu(fileName = "Localization import options", menuName = "DreamTeam/Localization import options")]
    public class LocalizationImportOptions : ScriptableObject
    {
        [SerializeField] private TextAsset _clientDataAsset;

        [SerializeField, HideInInspector] private string _localizationSpreadSheetName;
        [SerializeField, HideInInspector] private string _localizationWorkSheetName;
        [SerializeField, HideInInspector] private string _importPath;

        private static LocalizationImportOptions _instance;

        public JObject ClientData { get; private set; }

        public string LocalizationSpreadSheetName
        {
            get { return _localizationSpreadSheetName; }
            set { _localizationSpreadSheetName = value; }
        }

        public string LocalizationWorkSheetName
        {
            get { return _localizationWorkSheetName; }
            set { _localizationWorkSheetName = value; }
        }

        public string ImportPath
        {
            get { return _importPath; }
            set { _importPath = value; }
        }

        public static LocalizationImportOptions GetOptionsAsset()
        {
            if (_instance == null)
            {
                _instance = AssetDatabase.LoadAssetAtPath<LocalizationImportOptions>("Assets/Settings/Cloud/Editor/LocalizationImportOptions.asset");
            }

            return _instance;
        }

        protected void OnDisable()
        {
            _instance = null;
        }

        protected void OnValidate()
        {
            if (_clientDataAsset != null)
            {
                ClientData = JToken.Parse(_clientDataAsset.text) as JObject;
            }
            else
            {
                ClientData = null;
            }
        }
    }
}