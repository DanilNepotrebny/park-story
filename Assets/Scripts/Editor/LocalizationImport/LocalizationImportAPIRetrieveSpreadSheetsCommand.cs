﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using Google.GData.Spreadsheets;

namespace DreamTeam.Editor.LocalizationImport
{
    public class LocalizationImportAPIRetrieveSpreadSheetsCommand : LocalizationImportAPICommandBase
    {
        private Action<List<SpreadsheetEntry>> _successDelegate;
        private Action _failDelegate;
        private List<SpreadsheetEntry> _sheetEntries = new List<SpreadsheetEntry>();
        private Status _status = Status.InProgress;

        public LocalizationImportAPIRetrieveSpreadSheetsCommand(
            SpreadsheetsService service,
            Action<List<SpreadsheetEntry>> successDelegate,
            Action failDelegate) : base(service)
        {
            _successDelegate = successDelegate;
            _failDelegate = failDelegate;
        }

        public override void Process()
        {
            try
            {
                var spreadsheetQuery = new SpreadsheetQuery();
                SpreadsheetFeed spreadsheetFeed = _service.Query(spreadsheetQuery);

                foreach (var entry in spreadsheetFeed.Entries)
                {
                    _sheetEntries.Add(entry as SpreadsheetEntry);
                }

                _status = Status.Success;
            }
            catch (Exception)
            {
                _status = Status.Fail;
            }
        }

        public override void PostProcess()
        {
            switch (_status)
            {
                case Status.Success:
                    _successDelegate?.Invoke(_sheetEntries);
                    break;

                case Status.Fail:
                    UnityEngine.Debug.LogError("Exception! Reauthorize required!");
                    _failDelegate?.Invoke();
                    break;
            }
        }

        private enum Status
        {
            InProgress,
            Success,
            Fail
        }
    }
}