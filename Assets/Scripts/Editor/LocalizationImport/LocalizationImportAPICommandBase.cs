﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using Google.GData.Spreadsheets;

namespace DreamTeam.Editor.LocalizationImport
{
    public abstract class LocalizationImportAPICommandBase
    {
        protected SpreadsheetsService _service;

        protected LocalizationImportAPICommandBase(SpreadsheetsService service)
        {
            _service = service;
        }

        public abstract void Process();
        public abstract void PostProcess();
    }
}