﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Location.Isometry;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Isometry
{
    [CustomEditor(typeof(IsometryMovable))]
    public class IsometryMovableEditor : UnityEditor.Editor
    {
        private float _currentAngle;
        private IsometryMovable _isometryMovable => target as IsometryMovable;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            float angle = EditorGUILayout.Slider("Isometry angle: ", _currentAngle, 0f, 360f);
            angle = Mathf.Clamp(angle, 0f, 360f);
            if (Math.Abs(angle - _currentAngle) > Mathf.Epsilon)
            {
                _currentAngle = angle;
                _isometryMovable.Direction = IsometrySystem.IsometricAngleToDirection(_currentAngle);
                serializedObject.ApplyModifiedProperties();
            }
        }

        protected void OnEnable()
        {
            _currentAngle = IsometrySystem.IsometricDirectionToAngle(_isometryMovable.Direction);
        }
    }
}