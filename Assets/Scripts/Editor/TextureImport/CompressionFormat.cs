﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.ComponentModel;

namespace DreamTeam.Editor.TextureImport
{
    public enum CompressionFormat
    {
        None,
        Pvrtc
    }

    public static class CompressionFormatExtensions
    {
        public static bool HasPotRestriction(this CompressionFormat self)
        {
            switch (self)
            {
                case CompressionFormat.None:
                    return false;
                case CompressionFormat.Pvrtc:
                    return true;
                default:
                    throw new InvalidEnumArgumentException(nameof(self), (int)self, typeof(CompressionFormat));
            }
        }

        public static bool HasSquareRestriction(this CompressionFormat self)
        {
            switch (self)
            {
                case CompressionFormat.None:
                    return false;
                case CompressionFormat.Pvrtc:
                    return true;
                default:
                    throw new InvalidEnumArgumentException(nameof(self), (int)self, typeof(CompressionFormat));
            }
        }
    }
}