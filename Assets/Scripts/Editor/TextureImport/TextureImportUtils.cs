﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using DreamTeam.Editor.Utils;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;
using UnityEngine.U2D;
using Graphics = System.Drawing.Graphics;
using WrapMode = System.Drawing.Drawing2D.WrapMode;

namespace DreamTeam.Editor.TextureImport
{
    public static class TextureImportUtils
    {
        [MenuItem("Tools/Graphics import/Scale sprite to 2x")]
        private static void ScaleImportMenuItem()
        {
            AssetDatabase.StartAssetEditing();

            ILookup<string, SpriteShape> spriteShapes = AssetDatabaseUtils
                .EnumerateAssets<SpriteShape>()
                .ToLookup(ss => AssetDatabase.GetAssetPath(ss.fillTexture));

            int length = Selection.assetGUIDs.Length;
            for (int i = 0; i < length; i++)
            {
                string guid = Selection.assetGUIDs[i];
                string path = AssetDatabase.GUIDToAssetPath(guid);

                bool isCancelled = EditorUtility.DisplayCancelableProgressBar(
                        "Scaling sprites",
                        path,
                        (float)i / length
                    );

                if (isCancelled)
                {
                    break;
                }

                try
                {
                    ScaleSprite(path, PixelDensity.X2, spriteShapes);
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.LogError("Exception while scaling " + path, Selection.objects[i]);
                    UnityEngine.Debug.LogException(e);
                }
            }

            EditorUtility.ClearProgressBar();

            AssetDatabase.StopAssetEditing();
            AssetDatabase.SaveAssets();
        }

        private static void ResizeImage(string path, ref float scaleFactor)
        {
            Bitmap result;

            using (Image image = Image.FromFile(path))
            {
                scaleFactor = Mathf.Max(
                        Mathf.Round(image.Width * scaleFactor) / image.Width,
                        Mathf.Round(image.Height * scaleFactor) / image.Height
                    );

                int width = Mathf.RoundToInt(image.Width * scaleFactor);
                int height = Mathf.RoundToInt(image.Height * scaleFactor);

                result = new Bitmap(width, height, image.PixelFormat);

                // Set the resolutions the same to avoid cropping due to resolution differences
                result.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                // Use a graphics object to draw the resized image into the bitmap
                using (Graphics graphics = Graphics.FromImage(result))
                {
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    using (var attributes = new ImageAttributes())
                    {
                        // To remove filtering artifacts on the edge
                        attributes.SetWrapMode(WrapMode.TileFlipXY);

                        graphics.DrawImage(
                                image,
                                new Rectangle(0, 0, result.Width, result.Height),
                                0f,
                                0f,
                                image.Width,
                                image.Height,
                                GraphicsUnit.Pixel,
                                attributes
                            );
                    }
                }
            }

            result.Save(path, ImageFormat.Png);
            result.Dispose();
        }

        private static void ScaleSprite(string path, PixelDensity target, ILookup<string, SpriteShape> spriteShapes)
        {
            var importer = AssetImporter.GetAtPath(path) as TextureImporter;

            if (importer == null)
            {
                throw new InvalidOperationException("Can't obtain importer for " + path);
            }

            if (importer.spriteImportMode == SpriteImportMode.None)
            {
                throw new InvalidOperationException(path + " is not a sprite");
            }

            if (importer.spriteImportMode != SpriteImportMode.Single)
            {
                throw new NotSupportedException($"Can't scale {path}: sprite mode {importer.spriteImportMode} is not supported.");
            }

            float scaleFactor = target.ToFloat() / PixelDensity.X2_88.ToFloat();
            ResizeImage(path, ref scaleFactor);
            importer.spritePixelsPerUnit *= scaleFactor;
            importer.spriteBorder *= scaleFactor;

            importer.SaveAndReimport();

            foreach (SpriteShape ss in spriteShapes[path])
            {
                EditorUtils.RecordObjectUndo(ss, "Scale sprite shape");
                ss.fillPixelsPerUnit = importer.spritePixelsPerUnit;
            }
        }
    }
}