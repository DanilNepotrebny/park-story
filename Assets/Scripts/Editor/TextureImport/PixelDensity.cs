﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.ComponentModel;

namespace DreamTeam.Editor.TextureImport
{
    public enum PixelDensity
    {
        X1,
        X2,
        X2_88,
        X4
    }

    public static class PixelDensityExtensions
    {
        public static float ToFloat(this PixelDensity self)
        {
            switch (self)
            {
                case PixelDensity.X1:
                    return 1f;
                case PixelDensity.X2:
                    return 2f;
                case PixelDensity.X2_88:
                    return 2.88f;
                case PixelDensity.X4:
                    return 4f;
                default:
                    throw new InvalidEnumArgumentException(nameof(self), (int)self, typeof(PixelDensity));
            }
        }
    }
}