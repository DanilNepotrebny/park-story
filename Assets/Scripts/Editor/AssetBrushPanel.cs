﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Editor.AssetBrushes;
using UnityEngine;

namespace DreamTeam.Editor
{
    [Serializable]
    public class AssetBrushPanel : IDisposable
    {
        [SerializeField] private BaseBrush[] _brushes;
        [SerializeField] private int _selectedButtonIdx;

        private GUIContent[] _brushButtonsContent;
        private GUIStyle _toolbarStyle;
        private LayersPanel _layersPanel;
        private Vector2 _scrollPos;

        public ILayer SelectedLayer => _layersPanel.SelectedLayer;

        public bool IsRepaintNeeded { get; private set; }

        private BaseBrush ActiveBrush
        {
            get
            {
                int selectedBrushIdx = _selectedButtonIdx - 1;

                if (_brushes != null && selectedBrushIdx >= 0 && selectedBrushIdx < _brushes.Length)
                {
                    return _brushes[selectedBrushIdx];
                }

                return null;
            }
        }

        public AssetBrushPanel(BaseBrush[] brushes)
        {
            _brushes = brushes;

            _brushButtonsContent = new GUIContent[_brushes.Length + 1];
            _brushButtonsContent[0] = new GUIContent("No brush");
            for (int i = 0; i < _brushes.Length; ++i)
            {
                _brushButtonsContent[i + 1] = _brushes[i].GuiContent;
            }

            _layersPanel = new LayersPanel();
            _layersPanel.SelectedLayerChanged += OnSelectedLayerChanged;
            _layersPanel.Refresh();
        }

        public virtual void Dispose()
        {
            SetActiveBrush(0);

            _layersPanel?.Dispose();
            _layersPanel = null;
            _toolbarStyle = null;
            _brushButtonsContent = null;
            _brushes = null;
        }

        public void HandleEvent(Event e)
        {
            if (e.isKey && e.keyCode == KeyCode.Escape)
            {
                SetActiveBrush(0);
                return;
            }

            ActiveBrush?.HandleEvent(e);
        }

        public void OnHierarchyChange()
        {
            _layersPanel.Refresh();
        }

        public void OnGUI()
        {
            _layersPanel.Paint();

            if (_toolbarStyle == null)
            {
                _toolbarStyle = new GUIStyle(GUI.skin.button)
                {
                    fixedWidth = 128,
                    fixedHeight = 64,
                    imagePosition = ImagePosition.ImageAbove,
                    wordWrap = true
                };
            }

            _scrollPos = GUILayout.BeginScrollView(_scrollPos);
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            SetActiveBrush(GUILayout.SelectionGrid(_selectedButtonIdx, _brushButtonsContent, 2, _toolbarStyle));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndScrollView();

            ActiveBrush?.OnGUI();
        }

        private void SetActiveBrush(int buttonIdx)
        {
            if (buttonIdx == _selectedButtonIdx)
            {
                return;
            }

            ActiveBrush?.Deactivate();

            _selectedButtonIdx = buttonIdx;
            if (ActiveBrush != null)
            {
                ActiveBrush.Layer = _layersPanel.SelectedLayer;
            }

            ActiveBrush?.Activate();

            IsRepaintNeeded = true;
        }

        private void OnSelectedLayerChanged(Layer layer)
        {
            if (ActiveBrush != null)
            {
                ActiveBrush.Layer = layer;
            }
        }
    }
}