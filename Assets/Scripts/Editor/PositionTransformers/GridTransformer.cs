﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Editor.PositionTransformers
{
    /// <summary>
    /// Snaps position according to grid
    /// </summary>
    public class GridTransformer : BaseTransformer
    {
        private readonly Func<Vector3, Vector2Int> _worldToGrid;
        private readonly Func<Vector2Int, Vector3> _gridToWorld;

        public GridTransformer(Func<Vector3, Vector2Int> worldToGrid, Func<Vector2Int, Vector3> gridToWorld)
        {
            _worldToGrid = worldToGrid;
            _gridToWorld = gridToWorld;
        }

        /// <summary>
        /// Returns transformed position
        /// </summary>
        public override Vector3 GetTransformed(Vector3 position, ILayer layer = null)
        {
            Vector3 transformedPos = _gridToWorld(_worldToGrid(position));
            if (layer != null)
            {
                transformedPos.z = layer.GetZPosition();
            }

            return transformedPos;
        }
    }
}