﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace DreamTeam.Editor.PositionTransformers
{
    /// <summary>
    /// Base class for all position transformers
    /// </summary>
    public abstract class BaseTransformer
    {
        public abstract Vector3 GetTransformed(Vector3 position, ILayer layer = null);
    }
}