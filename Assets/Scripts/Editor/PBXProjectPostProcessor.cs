﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_IOS

using System.IO;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

namespace DreamTeam.Editor
{
    public class PBXProjectPostProcessor
    {
        private static List<ProcessProjectDelegate> _projectProcessors = new List<ProcessProjectDelegate>();
        private static List<ProcessCapabilitiesDelegate> _capabilitiesProcessors = new List<ProcessCapabilitiesDelegate>();

        public static void AddProjectProcessor(ProcessProjectDelegate processor)
        {
            _projectProcessors.Add(processor);
        }

        public static void AddCapabilitiesProcessor(ProcessCapabilitiesDelegate processor)
        {
            _capabilitiesProcessors.Add(processor);
        }

        [PostProcessBuild(1)]
        public static void OnPostProcessBuild(BuildTarget target, string path)
        {
            if (target == BuildTarget.iOS)
            {
                string projectPath = PBXProject.GetPBXProjectPath(path);

                if (_projectProcessors.Count > 0)
                {
                    PBXProject project = new PBXProject();
                    project.ReadFromString(File.ReadAllText(projectPath));
                    string targetName = PBXProject.GetUnityTargetName();
                    string targetGUID = project.TargetGuidByName(targetName);

                    foreach (ProcessProjectDelegate processor in _projectProcessors)
                    {
                        processor?.Invoke(project, targetGUID);
                    }

                    File.WriteAllText(projectPath, project.WriteToString());
                }

                if (_capabilitiesProcessors.Count > 0)
                {
                    string bundleId = PlayerSettings.applicationIdentifier;
                    string productName = bundleId.Substring(bundleId.LastIndexOf(".") + 1);
                    string projectName = "Unity-iPhone";

                    var projCapability = new ProjectCapabilityManager(projectPath, $"{projectName}/{productName}.entitlements", projectName);

                    foreach (ProcessCapabilitiesDelegate processor in _capabilitiesProcessors)
                    {
                        processor?.Invoke(projCapability);
                    }

                    projCapability.WriteToFile();
                }
            }
        }

        #region Inner types

        public delegate void ProcessProjectDelegate(PBXProject project, string targetGUID);

        public delegate void ProcessCapabilitiesDelegate(ProjectCapabilityManager capabilityManager);
    
        #endregion
    }
}

#endif