﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if ENABLE_VSTU

using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using SyntaxTree.VisualStudio.Unity.Bridge;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor
{
    [InitializeOnLoad]
    public static class CsprojPostprocessor
    {
        static CsprojPostprocessor()
        {
            ProjectFilesGenerator.ProjectFileGeneration += GenerateProjectFile;
        }

        private static string GenerateProjectFile(string name, string content)
        {
            if (name.Contains("Assembly-CSharp.csproj") || name.Contains("Assembly-CSharp-Editor.csproj"))
            {
                content = AddReferences(content);
            }

            return content;
        }

        private static string AddReferences(string content)
        {
            List<string> references = ReadReferences();
            if (references.Count == 0)
            {
                return content;
            }

            var builder = new StringBuilder(content);

            const string projectTag = "</Project>";

            builder.Remove(builder.Length - projectTag.Length, projectTag.Length);

            builder.AppendLine("  <ItemGroup>");

            foreach (string reference in references)
            {
                builder.AppendLine($"    <Reference Include=\"{reference}\" />");
            }

            builder.AppendLine("  </ItemGroup>");
            builder.AppendLine(projectTag);

            return builder.ToString();
        }

        private static List<string> ReadReferences()
        {
            var references = new List<string>();

            // See https://docs.unity3d.com/Manual/dotnetProfileAssemblies.html for information.
            string path = Path.Combine(Application.dataPath, "mcs.rsp");

            if (File.Exists(path))
            {
                using (Stream stream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        var pattern = new Regex("^-r:([\\w\\.]+)$");

                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            Match match = pattern.Match(line);
                            if (match.Success)
                            {
                                references.Add(match.Groups[1].Captures[0].ToString());
                            }
                        }
                    }
                }
            }

            return references;
        }
    }
}

#endif