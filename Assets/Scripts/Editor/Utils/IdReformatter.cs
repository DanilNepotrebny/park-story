﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Text;

namespace DreamTeam.Editor.Utils
{
    public static class IdReformatter
    {
        public static string ReformatId(string id)
        {
            StringBuilder idBuilder = new StringBuilder(id.Length);
            for (int i = 0; i < id.Length; i++)
            {
                char c = id[i];

                if (i > 0)
                {
                    char prev = id[i - 1];
                    if (char.IsLetter(c) && char.IsUpper(c) &&
                        (!char.IsLetter(prev) || char.IsLower(prev)) &&
                        prev != '_')
                    {
                        idBuilder.Append('_');
                    }
                }

                idBuilder.Append(char.ToLower(c));
            }
            return idBuilder.ToString();
        }
    }
}