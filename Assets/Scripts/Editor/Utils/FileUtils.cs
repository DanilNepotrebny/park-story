﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;

namespace DreamTeam.Editor.Utils
{
    public static class FileUtils
    {
        public static IEnumerable<FileInfo> EnumerateFiles(this DirectoryInfo self, string[] includes, string[] excludes = null)
        {
            foreach (FileSystemInfo fileSystemInfo in EnumerateFileSystemInfos(self, includes, excludes))
            {
                var file = fileSystemInfo as FileInfo;
                if (file != null)
                {
                    yield return file;
                }
            }
        }

        public static IEnumerable<FileSystemInfo> EnumerateFileSystemInfos(this DirectoryInfo self, string[] includes, string[] excludes = null)
        {
            if (includes.Length == 0)
            {
                yield break;
            }

            if (excludes == null)
            {
                excludes = Array.Empty<string>();
            }

            var includesRegex = new Regex(string.Join("|", includes));
            var excludesRegex = new Regex(string.Join("|", excludes));

            foreach (FileSystemInfo file in self.EnumerateFileSystemInfos("*.*", SearchOption.AllDirectories))
            {
                string relativePath = file.GetRelativePath(self).Replace("\\", "/");

                if (includesRegex.IsMatch(relativePath) &&
                    (excludes.Length == 0 || !excludesRegex.IsMatch(relativePath)))
                {
                    yield return file;
                }
            }
        }

        public static string GetRelativePath(this FileSystemInfo self, DirectoryInfo dir)
        {
            return GetRelativePath(self.FullName, dir.FullName);
        }

        public static string GetRelativePath(string fileName, string dir)
        {
            if (string.IsNullOrEmpty(dir))
            {
                return fileName;
            }

            // Very naive implementation, will work only for simple cases.
            string directoryPath = dir + Path.DirectorySeparatorChar;
            return fileName.Remove(0, directoryPath.Length);
        }

        public static int ExecuteProcess(string path, string arguments, out string output)
        {
            Process process = ExecuteProcess(path, arguments, null);
            output = process.StandardOutput.ReadToEnd().TrimEnd();
            return process.ExitCode;
        }

        public static int ExecuteProcess(string path, string arguments)
        {
            Process process = ExecuteProcess(
                    path,
                    arguments,
                    (sender, args) =>
                    {
                        if (args.Data != null)
                        {
                            UnityEngine.Debug.Log(args.Data);
                        }
                    }
                );

            return process.ExitCode;
        }

        public static bool ShowOpenFolderDialog(out string folder)
        {
            folder = EditorUtility.OpenFolderPanel("Select folder", ".", string.Empty);
            return !string.IsNullOrEmpty(folder);
        }

        public static void CopyFiles(this DirectoryInfo self, string dstDirPath, string[] includes, string[] excludes = null)
        {
            foreach (FileInfo file in self.EnumerateFiles(includes, excludes))
            {
                string fileName = file.GetRelativePath(self);
                string destPath = Path.Combine(dstDirPath, fileName);
                string destDir = Path.GetDirectoryName(destPath);
                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }

                FileUtil.CopyFileOrDirectory(file.FullName, destPath);
            }
        }

        public static void DeleteFiles(this DirectoryInfo self, string[] includes, string[] excludes = null)
        {
            foreach (FileSystemInfo file in self.EnumerateFileSystemInfos(includes, excludes).Reverse())
            {
                var dir = file as DirectoryInfo;
                if (dir == null || dir.GetFileSystemInfos().Length == 0)
                {
                    file.Delete();
                }
            }
        }

        public static string ReadText(string path)
        {
            using (FileStream stream = File.OpenRead(path))
            {
                using (var streamReader = new StreamReader(stream))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }

        public static void WriteText(string path, string text)
        {
            using (FileStream stream = File.Create(path))
            {
                using (var streamWriter = new StreamWriter(stream))
                {
                    streamWriter.Write(text);
                }
            }
        }

        /// <summary>
        /// More robust analogue of <see cref="Path.GetTempFileName"/>. Note that temporary file will not
        /// be created automatically, function just returns random temporary file name.
        /// </summary>
        /// <param name="extension">File extension (in some cases random file should have specific extension)</param>
        /// <returns>Unique random name for a temporary file.</returns>
        public static string GetTempFileName(string extension = "tmp")
        {
            int attemptCount = 10;
            while (true)
            {
                string fileName = Path.GetRandomFileName();
                fileName = Path.ChangeExtension(fileName, extension);
                fileName = Path.Combine(Path.GetTempPath(), fileName);

                if (!File.Exists(fileName))
                {
                    return fileName;
                }

                if (--attemptCount == 0)
                {
                    throw new IOException("No unique temporary file name is available.");
                }
            }
        }

        public static void RecreateDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }

            Directory.CreateDirectory(path);
        }

        private static Process ExecuteProcess(
                string path,
                string arguments,
                DataReceivedEventHandler outputDataReceivedEventHandler
            )
        {
            #if UNITY_EDITOR_OSX
            {
                if (path.EndsWith(".app"))
                {
                    arguments = $"-a '{path}' -n -W -j --args {arguments}";
                    path = "open";
                }
            }
            #endif

            var processStartInfo = new ProcessStartInfo
            {
                FileName = path,
                Arguments = arguments,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                CreateNoWindow = true
            };

            Process process = Process.Start(processStartInfo);

            if (outputDataReceivedEventHandler != null)
            {
                process.OutputDataReceived += outputDataReceivedEventHandler;
                process.BeginOutputReadLine();
            }

            process.ErrorDataReceived += (sender, args) =>
            {
                if (args.Data != null)
                {
                    UnityEngine.Debug.LogError(args.Data);
                }
            };
            process.BeginErrorReadLine();

            process.WaitForExit();

            if (process.ExitCode != 0)
            {
                throw new InvalidOperationException($"Process {path} failed with code {process.ExitCode}");
            }

            return process;
        }
    }
}