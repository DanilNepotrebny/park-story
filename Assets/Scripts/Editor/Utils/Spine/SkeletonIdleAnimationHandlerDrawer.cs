﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Spine;
using UnityEditor;

namespace DreamTeam.Editor.Utils.Spine
{
    [CustomEditor(typeof(SkeletonIdleAnimationHandler))]
    public class SkeletonIdleAnimationHandlerDrawer : UnityEditor.Editor
    {
        private SerializedProperty _switchOnTime;
        private SerializedProperty _switchTime;

        /// <summary>
        /// Unity callback
        /// </summary>
        protected void OnEnable()
        {
            _switchOnTime = serializedObject.FindProperty("_switchOnTime");
            _switchTime = serializedObject.FindProperty("_switchTime");
        }

        /// <inheritdoc />
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Separator();

            EditorGUILayout.PropertyField(_switchOnTime);
            serializedObject.ApplyModifiedProperties();

            if (_switchOnTime.boolValue)
            {
                EditorGUILayout.PropertyField(_switchTime);
                serializedObject.ApplyModifiedProperties();
            }
        }
    }
}