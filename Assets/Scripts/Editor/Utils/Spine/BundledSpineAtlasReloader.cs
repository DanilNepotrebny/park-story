﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Spine;
using UnityEditor;

namespace DreamTeam.Editor.Utils.Spine
{
    [InitializeOnLoad]
    public class BundledSpineAtlasReloader
    {
        static BundledSpineAtlasReloader()
        {
            EditorApplication.playModeStateChanged += OnPlaymodeStateChanged;
        }

        private static void OnPlaymodeStateChanged(PlayModeStateChange state)
        {
            string[] atlases = AssetDatabase.FindAssets($"t:{nameof(BundledSpineAtlasAsset)}");

            foreach (string atlasPath in atlases)
            {
                var atlasAsset = AssetDatabaseUtils.LoadAssetByGuid<BundledSpineAtlasAsset>(atlasPath);

                switch (state)
                {
                    case PlayModeStateChange.EnteredEditMode:
                        atlasAsset.UpdateAllPageMaterials();
                        break;
                    case PlayModeStateChange.EnteredPlayMode:
                        atlasAsset.UpdateAllPageMaterials();
                        break;
                }
            }
        }
    }
}