﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Spine;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils.Spine
{
    [CustomPropertyDrawer(typeof(SpineEventsTrigger.AnimationEntry))]
    public class SpineEventsTriggerAnimationEntryDrawer : PropertyDrawer
    {
        private const string _spineEventPropName = "_spineAnimationName";
        private const string _staredCallbackPropName = "_startedCallback";
        private const string _endedCallbackPropName = "_endedCallback";

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty spineAnim = property.FindPropertyRelative(_spineEventPropName);
            SerializedProperty startedCallback = property.FindPropertyRelative(_staredCallbackPropName);
            SerializedProperty endedCallback = property.FindPropertyRelative(_endedCallbackPropName);

            var spineAnimLabel = new GUIContent(spineAnim.displayName);
            var startedCallBackLabel = new GUIContent(startedCallback.displayName);
            var endedCallBackLabel = new GUIContent(endedCallback.displayName);

            float spineAnimHeight = EditorGUI.GetPropertyHeight(spineAnim, spineAnimLabel);
            float startedCallbackHeight = EditorGUI.GetPropertyHeight(startedCallback, startedCallBackLabel);
            float endedCallbackHeight = EditorGUI.GetPropertyHeight(endedCallback, endedCallBackLabel);

            Rect spineAnimPos = EditorGUIUtils.SubstractRect(ref position, spineAnimHeight);
            Rect startedCallbackEventPos = EditorGUI.IndentedRect(
                    EditorGUIUtils.SubstractRect(ref position, startedCallbackHeight)
                );
            Rect endedCallbackEventPos = EditorGUI.IndentedRect(
                    EditorGUIUtils.SubstractRect(ref position, endedCallbackHeight)
                );

            EditorGUI.PropertyField(spineAnimPos, spineAnim, spineAnimLabel);
            EditorGUI.PropertyField(startedCallbackEventPos, startedCallback, startedCallBackLabel);
            EditorGUI.PropertyField(endedCallbackEventPos, endedCallback, endedCallBackLabel);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty spineAnim = property.FindPropertyRelative(_spineEventPropName);
            SerializedProperty startedCallback = property.FindPropertyRelative(_staredCallbackPropName);
            SerializedProperty endedCallback = property.FindPropertyRelative(_endedCallbackPropName);

            var spineAnimLabel = new GUIContent(spineAnim.displayName);
            var startedCallbackLabel = new GUIContent(startedCallback.displayName);
            var endedCallBackLabel = new GUIContent(endedCallback.displayName);

            return
                EditorGUI.GetPropertyHeight(spineAnim, spineAnimLabel) +
                EditorGUI.GetPropertyHeight(startedCallback, startedCallbackLabel) +
                EditorGUI.GetPropertyHeight(endedCallback, endedCallBackLabel) +
                EditorGUIUtility.singleLineHeight;
        }
    }
}