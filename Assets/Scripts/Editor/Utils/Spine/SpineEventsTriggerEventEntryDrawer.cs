﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Spine;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils.Spine
{
    [CustomPropertyDrawer(typeof(SpineEventsTrigger.EventEntry))]
    public class SpineEventsTriggerEventEntryDrawer : PropertyDrawer
    {
        private const string _spineEventPropName = "_spineEventName";
        private const string _occuredCallbackPropName = "_occuredCallback";

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty spineEvent = property.FindPropertyRelative(_spineEventPropName);
            SerializedProperty occuredCallbackEvent = property.FindPropertyRelative(_occuredCallbackPropName);

            var spineEventLabel = new GUIContent(spineEvent.displayName);
            var occuredCallbackLabel = new GUIContent(occuredCallbackEvent.displayName);

            float spineEventHeight = EditorGUI.GetPropertyHeight(spineEvent, spineEventLabel);
            float occuredCallbackHeight = EditorGUI.GetPropertyHeight(occuredCallbackEvent, occuredCallbackLabel);

            Rect spineEventPos = EditorGUIUtils.SubstractRect(ref position, spineEventHeight);
            Rect occuredCallbackPos = EditorGUI.IndentedRect(
                    EditorGUIUtils.SubstractRect(ref position, occuredCallbackHeight)
                );

            EditorGUI.PropertyField(spineEventPos, spineEvent, spineEventLabel);
            EditorGUI.PropertyField(occuredCallbackPos, occuredCallbackEvent, occuredCallbackLabel);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty spineEvent = property.FindPropertyRelative(_spineEventPropName);
            SerializedProperty occuredCallbackEvent = property.FindPropertyRelative(_occuredCallbackPropName);

            var spineEventLabel = new GUIContent(spineEvent.displayName);
            var occuredCallbackLabel = new GUIContent(occuredCallbackEvent.displayName);

            return
                EditorGUI.GetPropertyHeight(spineEvent, spineEventLabel) +
                EditorGUI.GetPropertyHeight(occuredCallbackEvent, occuredCallbackLabel) +
                EditorGUIUtility.singleLineHeight;
        }
    }
}