﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils
{
    [CustomPropertyDrawer(typeof(RequiredFieldAttribute))]
    public class RequiredFieldAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Color defaultColor = GUI.color;
            GUI.color = GetColor(property, Color.red, defaultColor);
            EditorGUI.PropertyField(position, property, label, false);
            GUI.color = defaultColor;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, false);
        }

        private Color GetColor(SerializedProperty property, Color warningColor, Color defaultColor)
        {
            switch (property.propertyType)
            {
                case SerializedPropertyType.String:
                    return string.IsNullOrEmpty(property.stringValue) ? warningColor : defaultColor;

                case SerializedPropertyType.ObjectReference:
                    return property.objectReferenceValue == null ? warningColor : defaultColor;

                default:
                    return defaultColor;
            }
        }
    }
}