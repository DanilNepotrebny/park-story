﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils.Movement;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils.Movement
{
    /// <summary>
    /// Custom editor for a <see cref="MovementSequence"/>. Adds play / stop button and some additional controls to default inspector.
    /// </summary>
    [CustomEditor(typeof(MovementSequence)), CanEditMultipleObjects]
    public class MovementSequenceEditor : UnityEditor.Editor
    {
        private const float _targetStartOffsetY = 5f;

        private Vector3 _targetPosition;

        /// <inheritdoc />
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (CanBePlayed())
            {
                if (!HasPlayingSequence())
                {
                    if (GUILayout.Button("Start"))
                    {
                        PlaySequences();
                    }
                }
                else
                {
                    if (GUILayout.Button("Stop"))
                    {
                        StopSequences();
                    }
                }

                bool drawsPath = false;
                ForEachMover(
                        mover =>
                        {
                            if (mover.DrawPath)
                            {
                                drawsPath = true;
                            }
                        }
                    );
                bool drawsPathNew = EditorGUILayout.Toggle("Draw path", drawsPath);
                if (drawsPathNew != drawsPath)
                {
                    ForEachMover(mover => mover.DrawPath = drawsPathNew);
                }
            }
        }

        /// <summary>
        /// Unity callback.
        /// </summary>
        protected void OnEnable()
        {
            SceneView.onSceneGUIDelegate += HanldeSceneGui;

            var sequence = target as MovementSequence;

            if (sequence.IsMoving())
            {
                _targetPosition = sequence.TargetPosition;
            }
            else
            {
                _targetPosition = sequence.transform.position;
                _targetPosition.y += _targetStartOffsetY;
            }
        }

        /// <summary>
        /// Unity callback.
        /// </summary>
        protected void OnDisable()
        {
            SceneView.onSceneGUIDelegate -= HanldeSceneGui;
        }

        private void ForEachMover(Action<MovementSequence> action)
        {
            foreach (var target in targets)
            {
                action(target as MovementSequence);
            }
        }

        private void HanldeSceneGui(SceneView sceneView)
        {
            if (CanBePlayed())
            {
                _targetPosition = Handles.PositionHandle(_targetPosition, Quaternion.identity);
            }
        }

        private void OnTweenFinished(MovementSequence mover)
        {
            ClearEffects(mover);
            mover.Move(mover.StartPosition, _targetPosition);
        }

        private bool CanBePlayed()
        {
            if (!Application.isPlaying)
            {
                return false;
            }

            foreach (var target in targets)
            {
                if (PrefabUtility.GetPrefabType(target) == PrefabType.Prefab)
                {
                    return false;
                }
            }

            return true;
        }

        private bool HasPlayingSequence()
        {
            bool hasPlaying = false;
            ForEachMover(
                    mover =>
                    {
                        if (mover.IsMoving())
                        {
                            hasPlaying = true;
                        }
                    }
                );
            return hasPlaying;
        }

        private void PlaySequences()
        {
            StopSequences();

            ForEachMover(
                    mover =>
                    {
                        mover.Finished += OnTweenFinished;
                        mover.Move(_targetPosition);
                    }
                );
        }

        private void StopSequences()
        {
            ForEachMover(
                    mover =>
                    {
                        if (mover.IsMoving())
                        {
                            mover.Finished -= OnTweenFinished;
                            mover.Stop();
                            mover.transform.position = mover.StartPosition;
                            ClearEffects(mover);
                        }
                    }
                );
        }

        private void ClearEffects(MovementSequence sequence)
        {
            foreach (var trail in sequence.GetComponentsInChildren<TrailRenderer>())
            {
                trail.Clear();
            }

            foreach (var particleSystem in sequence.GetComponentsInChildren<ParticleSystem>())
            {
                particleSystem.Clear();
            }
        }
    }
}