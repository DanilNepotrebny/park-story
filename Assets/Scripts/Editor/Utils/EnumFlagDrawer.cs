﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Text.RegularExpressions;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils
{
    [CustomPropertyDrawer(typeof(EnumFlagAttribute))]
    public class EnumFlagDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.serializedObject.isEditingMultipleObjects)
            {
                EditorGUI.BeginDisabledGroup(true);
                EditorGUI.TextField(position, label, "Multi-editing is not supported");
                EditorGUI.EndDisabledGroup();
                return;
            }

            Enum targetEnum = (Enum)Enum.ToObject(fieldInfo.FieldType, property.intValue);
            string propName = FormatName(property.name);
            EditorGUI.BeginProperty(position, label, property);
            Enum enumNew = EditorGUI.EnumFlagsField(position, propName, targetEnum);
            property.intValue = (int)Convert.ChangeType(enumNew, targetEnum.GetType());
            EditorGUI.EndProperty();
        }

        /// <summary>
        /// This format the name in the same way Unity formats every property name
        /// </summary>
        private static string FormatName(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                return propertyName;
            }

            int index = propertyName.IndexOf("_", StringComparison.Ordinal);
            if (index != -1 && (index != 1 || index != 2) && propertyName.Length > (index + 1))
            {
                propertyName = propertyName.Substring(index + 1);
            }

            string formattedPropertyName = char.ToUpper(propertyName[0]).ToString();
            if (propertyName.Length > 1)
            {
                formattedPropertyName = string.Concat(
                    formattedPropertyName,
                    Regex.Replace(propertyName.Substring(1), "([a-z])([A-Z])", "$1 $2"));
            }

            return formattedPropertyName;
        }
    }
}