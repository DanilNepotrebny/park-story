﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils
{
    [CustomPropertyDrawer(typeof(MinMaxRangedFloat))]
    public class MinMaxRangedFloatDrawer : PropertyDrawer
    {
        private float _lineHeight => EditorGUIUtility.singleLineHeight;
        private float _verticalSpace => EditorGUIUtility.standardVerticalSpacing;
        private float _horizontalSpace => EditorGUIUtility.standardVerticalSpacing * 2;
        private const float _floatFieldWidth = 30;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty minLimitProp = property.FindPropertyRelative("_minLimit");
            SerializedProperty maxLimitProp = property.FindPropertyRelative("_maxLimit");
            SerializedProperty minValueProp = property.FindPropertyRelative("_minValue");
            SerializedProperty maxValueProp = property.FindPropertyRelative("_maxValue");

            MinMaxRangedFloat range = new MinMaxRangedFloat(
                    minLimitProp.floatValue,
                    maxLimitProp.floatValue,
                    minValueProp.floatValue,
                    maxValueProp.floatValue
                );

            Rect labelPos = new Rect(position.x, position.y, position.width * 0.2f, _lineHeight);
            Rect minLimitPos = new Rect(labelPos.xMax, position.y, _floatFieldWidth, _lineHeight);
            Rect sliderPos = new Rect(
                    minLimitPos.xMax + _horizontalSpace,
                    position.y,
                    position.xMax - minLimitPos.xMax - _floatFieldWidth - _horizontalSpace * 2,
                    _lineHeight
                );
            Rect maxLimitPos = new Rect(sliderPos.xMax + _horizontalSpace, position.y, _floatFieldWidth, _lineHeight);

            EditorGUI.LabelField(labelPos, label);
            range.MinLimit = EditorGUI.FloatField(minLimitPos, GUIContent.none, range.MinLimit);
            range.MaxLimit = EditorGUI.FloatField(maxLimitPos, GUIContent.none, range.MaxLimit);
            DrawMinMaxSlider(sliderPos, ref range);

            minLimitProp.floatValue = range.MinLimit;
            maxLimitProp.floatValue = range.MaxLimit;
            minValueProp.floatValue = range.MinValue;
            maxValueProp.floatValue = range.MaxValue;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return _lineHeight + _verticalSpace;
        }

        private void DrawMinMaxSlider(Rect position, ref MinMaxRangedFloat range)
        {
            float minValue = range.MinValue;
            float maxValue = range.MaxValue;

            Rect minPos = new Rect(position.x, position.y, _floatFieldWidth, position.height);
            Rect sliderPos = new Rect(
                    minPos.xMax + _horizontalSpace,
                    position.y,
                    position.xMax - minPos.xMax - _floatFieldWidth - _horizontalSpace * 2,
                    position.height
                );
            Rect maxPos = new Rect(sliderPos.xMax + _horizontalSpace, position.y, _floatFieldWidth, position.height);

            minValue = EditorGUI.FloatField(minPos, GUIContent.none, minValue);
            maxValue = EditorGUI.FloatField(maxPos, GUIContent.none, maxValue);
            EditorGUI.MinMaxSlider(
                sliderPos,
                GUIContent.none,
                ref minValue,
                ref maxValue,
                range.MinLimit,
                range.MaxLimit);

            range.MinValue = minValue;
            range.MaxValue = maxValue;
        }
    }
}