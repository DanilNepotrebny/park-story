﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils
{
    public static class PrefabContextMenu
    {
        private const string _applyMenuPath = "GameObject/Apply selected prefabs";

        [MenuItem(_applyMenuPath, false)]
        private static void ApplySelectedPrefabs()
        {
            var processedPrefabs = new List<Object>();

            foreach (Object item in Selection.objects)
            {
                if (!processedPrefabs.Contains(item))
                {
                    GameObject root = PrefabUtility.FindRootGameObjectWithSameParentPrefab(item as GameObject);
                    Object asset = PrefabUtility.GetCorrespondingObjectFromSource(item);
                    PrefabUtility.ReplacePrefab(root, asset, ReplacePrefabOptions.ConnectToPrefab);
                    processedPrefabs.Add(root);
                }
            }
        }

        [MenuItem(_applyMenuPath, true)]
        private static bool ApplySelectedPrefabsValidation()
        {
            return Selection.objects.All(IsValidToApplyPrefab);
        }

        private static bool IsValidToApplyPrefab(Object target)
        {
            return
                PrefabUtility.GetPrefabType(target) == PrefabType.PrefabInstance ||
                PrefabUtility.GetPrefabType(target) == PrefabType.DisconnectedPrefabInstance;
        }
    }
}