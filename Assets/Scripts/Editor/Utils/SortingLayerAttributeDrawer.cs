﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Reflection;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils
{
    [CustomPropertyDrawer(typeof(SortingLayerAttribute))]
    public class SortingLayerAttributeDrawer : PropertyDrawer
    {
        private static MethodInfo _sortingLayerField;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType != SerializedPropertyType.Integer)
            {
                EditorGUI.LabelField(position, label.text, "Use sorting layer attribute for int fields.");
            }
            else
            {
                if (_sortingLayerField == null)
                {
                    _sortingLayerField = typeof(EditorGUI).GetMethod(
                        "SortingLayerField",
                        BindingFlags.NonPublic | BindingFlags.Static);
                }

                if (_sortingLayerField != null)
                {
                    _sortingLayerField.Invoke(
                        null,
                        new object[] { position, label, property, EditorStyles.popup, EditorStyles.label });
                }
                else
                {
                    EditorGUI.PropertyField(position, property, label);
                }
            }
        }
    }
}