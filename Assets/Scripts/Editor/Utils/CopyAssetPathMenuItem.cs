﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using JetBrains.Annotations;
using UnityEditor;

namespace DreamTeam.Editor.Utils
{
    public class CopyAssetPathMenuItem
    {
        [MenuItem("Assets/Copy asset path")]
        private static void CopyAssetPath()
        {
            CopyAssetsData(AssetDatabase.GUIDToAssetPath);
        }

        [MenuItem("Assets/Copy asset guid")]
        private static void CopyAssetGuid()
        {
            CopyAssetsData(guid => guid);
        }

        private static void CopyAssetsData([NotNull] Func<string, string> predicate)
        {
            string str = "";
            foreach (string guid in Selection.assetGUIDs)
            {
                str += predicate(guid) + "\n";
            }
            EditorGUIUtility.systemCopyBuffer = str.TrimEnd('\n');
        }
    }
}