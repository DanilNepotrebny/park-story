﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace DreamTeam.Editor.Utils
{
    public class ForceReserializeSelectedScenesMenuItem : BaseScenesProcessor
    {
        [MenuItem("Assets/Force reserialize selected scenes", true, 0)]
        private static bool ValidateReserializeSelectedAssets()
        {
            return Selection.objects.Any(obj => obj is SceneAsset);
        }

        [MenuItem("Assets/Force reserialize selected scenes", false, 1)]
        private static void MenuItem()
        {
            ProcessSelectedScenes(SaveScene);
        }

        private static void SaveScene()
        {
            EditorSceneManager.SaveScene(SceneManager.GetActiveScene());
        }
    }
}
