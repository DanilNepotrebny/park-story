﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Editor.Utils
{
    public static class GitUtils
    {
        public static string GetHead()
        {
            string head;
            FileUtils.ExecuteProcess("git", "rev-parse --short HEAD", out head);
            return head;
        }

        public static bool HasChanges()
        {
            string changedFiles;
            FileUtils.ExecuteProcess("git", "status --porcelain", out changedFiles);
            return !string.IsNullOrEmpty(changedFiles);
        }

        public static void CheckoutBranch(string name, string revision)
        {
            FileUtils.ExecuteProcess("git", $"checkout -B {name} {revision}");
        }
    }
}