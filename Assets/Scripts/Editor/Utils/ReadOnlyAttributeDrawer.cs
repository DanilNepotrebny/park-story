﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils
{
    [CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
    public class ReadOnlyAttributeDrawer : DecoratorDrawer
    {
        public override void OnGUI(Rect position)
        {
            GUI.enabled = false;
            base.OnGUI(position);
        }

        public override float GetHeight()
        {
            return 0;
        }
    }
}