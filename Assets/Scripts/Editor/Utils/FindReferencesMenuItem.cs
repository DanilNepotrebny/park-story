﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DreamTeam.Editor.Utils
{
    public class FindReferencesMenuItem
    {
        [MenuItem("Assets/Find references in project")]
        private static void FindReferences()
        {
            var assetsDir = new DirectoryInfo(Application.dataPath);
            var filters = new []
            {
                ".asset",
                ".prefab",
                ".unity",
                ".mat"
            };

            List<Node> sourceNodes = Selection.assetGUIDs
                .Select(Node.FromGuid)
                .ToList();

            Dictionary<string, Node> nodes = sourceNodes.ToDictionary(node => node.Path);

            var nodesToProcess = new List<Node>(sourceNodes);
            var nextNodes = new ConcurrentBag<Tuple<Node, string>>();
            var fileCache = new ConcurrentDictionary<string, byte[]>();

            Stopwatch sw = new Stopwatch();
            sw.Start();

            string[] assets = assetsDir
                .EnumerateFiles("*.*", SearchOption.AllDirectories)
                .Select(file => file.FullName)
                .Where(file => filters.Any(file.EndsWith))
                .ToArray();

            while (nodesToProcess.Count > 0)
            {
                Parallel.ForEach(assets, file =>
                {
                    byte[] text;
                    if (!fileCache.TryGetValue(file, out text))
                    {
                        text = File.ReadAllBytes(file);
                        fileCache[file] = text;
                    }

                    foreach (Node node in nodesToProcess)
                    {
                        if (!node.ContainsIn(text))
                        {
                            continue;
                        }

                        string assetPath = Path.Combine("Assets", FileUtils.GetRelativePath(file, assetsDir.FullName));
                        nextNodes.Add(new Tuple<Node, string>(node, assetPath));
                    }
                });

                nodesToProcess.Clear();

                while (!nextNodes.IsEmpty)
                {
                    Tuple<Node, string> tmp;
                    nextNodes.TryTake(out tmp);

                    Node child;
                    if (!nodes.TryGetValue(tmp.Item2, out child))
                    {
                        child = Node.FromPath(tmp.Item2);
                        nodes.Add(tmp.Item2, child);

                        if (!tmp.Item2.EndsWith(".unity"))
                        {
                            nodesToProcess.Add(child);
                        }
                    }

                    tmp.Item1.AddChild(child);
                }
            }

            sw.Stop();

            UnityEngine.Debug.Log($"Found references: (elapsed {sw.Elapsed.TotalSeconds} seconds)");
            foreach (List<Node> referencePath in sourceNodes.SelectMany(n => n.GetReferencePaths()))
            {
                string message = string.Join("\n", referencePath.Select(node => node.Path));
                Object sourceAsset = AssetDatabase.LoadAssetAtPath<Object>(referencePath.First().Path);

                string rootAsset = referencePath.Last().Path;
                if (rootAsset.EndsWith(".unity") || rootAsset.Contains("/Resources/"))
                {
                    UnityEngine.Debug.Log(message, sourceAsset);
                }
                else
                {
                    UnityEngine.Debug.LogWarning(message, sourceAsset);
                }
            }
        }

        private class Node
        {
            public string Guid
            {
                get { return _guid; }
                private set
                {
                    if (_guid == value)
                    {
                        return;
                    }

                    _guid = value;
                    _finder = new BoyerMoore(Encoding.UTF8.GetBytes(_guid));
                }
            }

            public string Path { get; }

            private string _guid;
            private ReaderWriterLockSlim _lock = new ReaderWriterLockSlim();
            private HashSet<Node> _children = new HashSet<Node>();

            private BoyerMoore _finder;

            private Node(string path, string guid)
            {
                Path = path;
                Guid = guid;
            }

            public static Node FromPath(string path)
            {
                return new Node(path, AssetDatabase.AssetPathToGUID(path));
            }

            public static Node FromGuid(string guid)
            {
                return new Node(AssetDatabase.GUIDToAssetPath(guid), guid);
            }

            public override bool Equals(object other)
            {
                var otherNode = other as Node;
                return otherNode != null && otherNode.Guid == Guid;
            }

            public override int GetHashCode()
            {
                return Guid.GetHashCode();
            }

            public bool ContainsIn(byte[] text)
            {
                return _finder.Search(text) >= 0;
            }

            public void AddChild(Node child)
            {
                _lock.EnterWriteLock();
                try
                {
                    _children.Add(child);
                }
                finally
                {
                    _lock.ExitWriteLock();
                }
            }

            public IEnumerable<List<Node>> GetReferencePaths()
            {
                var paths = new List<Node>();
                foreach (List<Node> path in FillReferencePaths(paths))
                {
                    yield return path;
                }
            }

            private IEnumerable<List<Node>> FillReferencePaths(List<Node> pathStack)
            {
                pathStack.Add(this);

                if (_children.Count == 0)
                {
                    yield return pathStack;
                }

                foreach (Node child in _children)
                {
                    if (pathStack.Contains(child))
                    {
                        continue;
                    }

                    foreach (List<Node> path in child.FillReferencePaths(pathStack))
                    {
                        yield return path;
                    }
                }

                pathStack.RemoveAt(pathStack.Count - 1);
            }
        }

        private class BoyerMoore
        {
            private int[] _jumpTable;
            private byte[] _pattern;
            private int _patternLength;

            public BoyerMoore([NotNull] byte[] pattern)
            {
                _pattern = pattern;
                _jumpTable = new int[256];
                _patternLength = _pattern.Length;

                for (int index = 0; index < _jumpTable.Length; index++)
                {
                    _jumpTable[index] = _patternLength;
                }

                for (int index = 0; index < _patternLength - 1; index++)
                {
                    _jumpTable[_pattern[index]] = _patternLength - index - 1;
                }
            }

            public unsafe int Search([NotNull] byte[] searchArray, int startIndex = 0)
            {
                if (_patternLength > searchArray.Length)
                {
                    throw new ArgumentException("Search Pattern length exceeds search array length.");
                }

                int index = startIndex;
                int limit = searchArray.Length - _patternLength;
                int patternLengthMinusOne = _patternLength - 1;
                fixed (byte* pointerToByteArray = searchArray)
                {
                    byte* pointerToByteArrayStartingIndex = pointerToByteArray + startIndex;
                    fixed (byte* pointerToPattern = _pattern)
                    {
                        while (index <= limit)
                        {
                            int j = patternLengthMinusOne;
                            while (j >= 0 && pointerToPattern[j] == pointerToByteArrayStartingIndex[index + j])
                            {
                                j--;
                            }

                            if (j < 0)
                            {
                                return index;
                            }

                            index += Math.Max(_jumpTable[pointerToByteArrayStartingIndex[index + j]] - _patternLength + 1 + j, 1);
                        }
                    }
                }
                return -1;
            }
        }
    }
}