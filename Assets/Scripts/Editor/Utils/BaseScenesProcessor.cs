﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace DreamTeam.Editor.Utils
{
    public class BaseScenesProcessor
    {
        protected static void ProcessSelectedScenes(Action sceneProcessor)
        {
            IEnumerable<SceneAsset> scenes = Selection.objects.OfType<SceneAsset>();

            float scenesCount = scenes.Count();
            float sceneCounter = 1;

            foreach (SceneAsset scene in scenes)
            {
                bool isCancelled = EditorUtility.DisplayCancelableProgressBar(
                        "Process scenes",
                        $"Process {scene.name} ({sceneCounter}/{scenesCount})",
                        sceneCounter / scenesCount
                    );
                if (isCancelled)
                {
                    break;
                }

                try
                {
                    EditorSceneManager.OpenScene(AssetDatabase.GetAssetOrScenePath(scene));
                    sceneProcessor();
                    EditorSceneManager.SaveOpenScenes();
                    sceneCounter++;
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.Log(e.ToString());
                    break;
                }
            }

            EditorUtility.ClearProgressBar();
        }
    }
}