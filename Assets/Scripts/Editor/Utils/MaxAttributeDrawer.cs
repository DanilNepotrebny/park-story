﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils
{
    [CustomPropertyDrawer(typeof(MaxAttribute))]
    public class MaxAttributeDrawer : PropertyDrawer
    {
        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            MaxAttribute maxAttribute = (MaxAttribute)attribute;

            switch (property.propertyType)
            {
                case SerializedPropertyType.Float:
                    EditorGUI.PropertyField(position, property, label, false);
                    property.floatValue = Mathf.Min(property.floatValue, maxAttribute.max);
                    break;

                case SerializedPropertyType.Integer:
                    EditorGUI.PropertyField(position, property, label, false);
                    property.intValue = Mathf.Min(property.intValue, (int)maxAttribute.max);
                    break;

                default:
                    EditorGUI.LabelField(position, label.text, "Use Max attribute with float or int.");
                    break;
            }
        }
    }
}