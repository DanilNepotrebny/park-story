﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils
{
    [CustomPropertyDrawer(typeof(TimeTicks))]
    public class TimeTicksDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            position = EditorGUI.PrefixLabel(position, label);

            SerializedProperty ticks = property.FindPropertyRelative("_ticks");
            TimeSpan timeSpan = TimeSpan.FromTicks(ticks.longValue);

            var labels = new GUIContent[]
            {
                new GUIContent("D"),
                new GUIContent("H"),
                new GUIContent("M"),
                new GUIContent("S"),
                new GUIContent("m")
            };
            var values = new int[]
            {
                timeSpan.Days,
                timeSpan.Hours,
                timeSpan.Minutes,
                timeSpan.Seconds,
                timeSpan.Milliseconds
            };

            EditorGUI.MultiIntField(position, labels, values);

            ticks.longValue = new TimeSpan(values[0], values[1], values[2], values[3], values[4]).Ticks;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }
    }
}