﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Tweening;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils.Tweening
{
    /// <summary>
    /// Custom property drawer for an <see cref="Easing"/>. Displays enum of standard easing types and curve editor if custom easing is selected.
    /// </summary>
    [CustomPropertyDrawer(typeof(Easing))]
    public class EasingDrawer : PropertyDrawer
    {
        private float _lineSpace => EditorGUIUtility.standardVerticalSpacing;

        /// <inheritdoc />
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var type = GetType(property);
            GUIContent typeLabel = GetLabel(label, type);

            position.height = base.GetPropertyHeight(type, label);

            // Draw enum editor
            var oldValue = (LeanTweenType)type.enumValueIndex;
            EditorGUI.PropertyField(position, type, typeLabel);
            var newValue = (LeanTweenType)type.enumValueIndex;

            if (newValue != oldValue)
            {
                if (oldValue == LeanTweenType.animationCurve)
                {
                    // Clear
                    GetCurve(property).animationCurveValue = new AnimationCurve();
                }

                if (newValue == LeanTweenType.animationCurve)
                {
                    // Create default curve
                    GetCurve(property).animationCurveValue =
                        (oldValue == LeanTweenType.linear || oldValue == LeanTweenType.notUsed) ?
                            AnimationCurve.Linear(0f, 0f, 1f, 1f) :
                            AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
                }
            }

            if (newValue == LeanTweenType.animationCurve)
            {
                // Draw curve editor
                position.y += position.height + _lineSpace;

                var curve = GetCurve(property);
                var curveLabel = GetLabel(label, curve);
                EditorGUI.PropertyField(position, curve, curveLabel);
            }
        }

        /// <inheritdoc />
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var type = GetType(property);

            float result = (base.GetPropertyHeight(type, label) + _lineSpace);

            if (type.enumValueIndex == (int)LeanTweenType.animationCurve)
            {
                result += base.GetPropertyHeight(GetCurve(property), GUIContent.none) + _lineSpace;
            }

            return result;
        }

        private SerializedProperty GetCurve(SerializedProperty property)
        {
            return property.FindPropertyRelative("_curve");
        }

        private SerializedProperty GetType(SerializedProperty property)
        {
            return property.FindPropertyRelative("_type");
        }

        private GUIContent GetLabel(GUIContent baseLabel, SerializedProperty property)
        {
            return new GUIContent($"{baseLabel.text} {property.displayName}");
        }
    }
}