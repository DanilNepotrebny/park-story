﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.UI;
using UnityEditor;

namespace DreamTeam.Editor.Utils.UI
{
    [CustomEditor(typeof(NotifyingText))]
    public class NotifyingTextEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }
    }
}