﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils
{
    /// <summary>
    /// Drawer for scene reference. Works via <see cref="SceneAsset"/>.
    /// </summary>
    [CustomPropertyDrawer(typeof(SceneReference), true)]
    public class SceneReferenceDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var sceneReference = property.GetValue() as SceneReference;

            if (sceneReference == null)
            {
                return;
            }

            SceneAsset oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(sceneReference.AssetPath);

            EditorGUI.BeginChangeCheck();
            var newScene = EditorGUI.ObjectField(position, label, oldScene, typeof(SceneAsset), false) as SceneAsset;

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(property.serializedObject.targetObject, "Set scene reference");
                ((SceneReference.IEditorApi)sceneReference).SetAssetPath(AssetDatabase.GetAssetPath(newScene));
                EditorUtility.SetDirty(property.serializedObject.targetObject);
            }
        }
    }
}