﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Utils;
using DreamTeam.Utils.Binding;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DreamTeam.Editor.Utils.Binding
{
    [CustomEditor(typeof(OneWayBinding)), CanEditMultipleObjects]
    public class OneWayBindingEditor : UnityEditor.Editor
    {
        private List<string> _visibleProperties = new List<string>();

        public override void OnInspectorGUI()
        {
            SerializedProperty property = serializedObject.GetIterator();
            List<string> propertyNames = new List<string>();
            if (property.NextVisible(true))
            {
                do
                {
                    propertyNames.Add(property.name);
                }
                while (property.NextVisible(false));
            }

            foreach (string name in propertyNames)
            {
                if (name == "_adapter" ||
                    name == "_source" ||
                    name == "_target")
                {
                    CreatablePropertyField(serializedObject.FindProperty(name));
                }
                else
                {
                    EditorGUILayout.PropertyField(serializedObject.FindProperty(name), true);
                }
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void CreatablePropertyField(SerializedProperty property)
        {
            Object value = property.objectReferenceValue;

            if (value != null)
            {
                bool isMine = (value as Component).gameObject ==
                    (property.serializedObject.targetObject as Component).gameObject;
                if (!isMine)
                {
                    EditorGUILayout.PropertyField(property, true);
                    return;
                }

                bool isHidden = value.hideFlags.HasFlag(HideFlags.HideInInspector);

                EditorGUILayout.BeginHorizontal();

                if (isHidden)
                {
                    GUIStyle foldoutStyle = new GUIStyle(EditorStyles.foldout);
                    foldoutStyle.stretchWidth = false;
                    EditorGUIUtility.fieldWidth = 0.001f;

                    ShowProperty(
                            property,
                            EditorGUILayout.Foldout(IsPropertyVisible(property), GUIContent.none, foldoutStyle)
                        );

                    GUILayout.Space(-4f);
                }

                EditorGUILayout.PropertyField(property, true);

                if (GUILayout.Button(isHidden ? ">" : "<", EditorStyles.label, GUILayout.ExpandWidth(false)))
                {
                    value.hideFlags ^= HideFlags.HideInInspector;
                }

                EditorGUIUtility.fieldWidth = 0f;
                EditorGUILayout.EndHorizontal();

                if (isHidden && IsPropertyVisible(property))
                {
                    EditorGUI.indentLevel++;

                    List<Object> objects = new List<Object>();
                    foreach (Object target in property.serializedObject.targetObjects)
                    {
                        Object obj = new SerializedObject(target).FindProperty(property.propertyPath).objectReferenceValue;
                        if (obj != null && obj.GetType() == value.GetType())
                        {
                            objects.Add(obj);
                        }
                    }

                    UnityEditor.Editor editor = CreateEditor(objects.ToArray());
                    editor.DrawDefaultInspector();

                    EditorGUI.indentLevel--;
                }

                // Property is cleared or another object is dragged into
                if (property.objectReferenceValue != value)
                {
                    // TODO: Correctly process multiple object selection
                    Undo.DestroyObjectImmediate(value);
                }
            }
            else
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.PropertyField(property, true);
                if (GUILayout.Button("+", EditorStyles.label, GUILayout.ExpandWidth(false)))
                {
                    Type propertyType = ReflectionUtils.FindField(target, property.name).FieldType;

                    var types = from t in propertyType.Assembly.GetTypes()
                        where !t.IsAbstract && t.IsSubclassOf(propertyType)
                        select t;

                    GenericMenu menu = new GenericMenu();
                    foreach (var type in types)
                    {
                        menu.AddItem(new GUIContent(type.Name), false, o => CreateBehaviour(o, property), type);
                    }

                    menu.ShowAsContext();
                }

                EditorGUILayout.EndHorizontal();
            }
        }

        private void CreateBehaviour(object type, SerializedProperty property)
        {
            foreach (Object target in property.serializedObject.targetObjects)
            {
                Object obj = Undo.AddComponent((target as MonoBehaviour).gameObject, type as Type);
                obj.hideFlags |= HideFlags.HideInInspector;

                // Setting property.objectReferenceValue doesn't work for some reason, so using reflection
                ReflectionUtils.FindField(target, property.name).SetValue(target, obj);
            }

            ShowProperty(property, true);
        }

        private void ShowProperty(SerializedProperty property, bool isVisible)
        {
            bool wasVisible = IsPropertyVisible(property);
            if (isVisible != wasVisible)
            {
                if (isVisible)
                {
                    _visibleProperties.Add(property.name);
                }
                else
                {
                    _visibleProperties.Remove(property.name);
                }
            }
        }

        private bool IsPropertyVisible(SerializedProperty property)
        {
            return _visibleProperties.Contains(property.name);
        }
    }
}