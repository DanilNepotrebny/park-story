﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils.Binding;
using UnityEditor;

namespace DreamTeam.Editor.Utils.Binding
{
    [CustomEditor(typeof(EditorOneWayBinding)), CanEditMultipleObjects]
    public class EditorOneWayBindingEditor : OneWayBindingEditor
    {
    }
}