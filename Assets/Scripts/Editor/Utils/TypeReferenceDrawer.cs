﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Linq;
using DreamTeam.Utils;
using DreamTeam.Utils.Internal;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils
{
    [CustomPropertyDrawer(typeof(DrawableTypeReference), true)]
    public class TypeReferenceDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Type valueType = GetValueType(property);
            Type typeReferenceType = valueType.FindParent(typeof(TypeReference<>));
            Type refType = typeReferenceType.GetGenericArguments()[0];

            SerializedProperty typeStringProperty = property.FindPropertyRelative("_serializedType");

            bool abstractAllowed = fieldInfo.GetCustomAttributes(typeof(NonUniqueKeysAttribute), true).Length > 0 ||
                valueType.GetCustomAttributes(typeof(AllowAbstractTypesAttribute), true).Length > 0;

            Func<Type, bool> filter = t => ReflectionUtils.StaticInvoke<bool>(
                    typeReferenceType,
                    nameof(TypeReference<object>.IsValidType),
                    false,
                    t,
                    abstractAllowed
                );

            Type[] types = refType.Assembly.GetTypes().Where(filter).ToArray();

            int selectedIndex = 0;
            for (int i = 0; i < types.Length; ++i)
            {
                string serializedString = ReflectionUtils.StaticInvoke<string>(
                    typeReferenceType,
                    "Serialize",
                    false,
                    types[i]);
                if (serializedString == typeStringProperty.stringValue)
                {
                    selectedIndex = i + 1;
                    break;
                }
            }

            string[] names = { "None" };
            names = names.Concat(from t in types select t.FullName.Substring(t.Namespace.Length + 1).Replace('+', '.'))
                .ToArray();

            int newIndex = EditorGUI.Popup(
                    position,
                    selectedIndex,
                    names
                );
            if (newIndex != selectedIndex)
            {
                string serializedString = string.Empty;
                if (newIndex > 0)
                {
                    serializedString = ReflectionUtils.StaticInvoke<string>(
                        typeReferenceType,
                        "Serialize",
                        false,
                        types[newIndex - 1]);
                }

                typeStringProperty.stringValue = serializedString;
            }

            property.serializedObject.ApplyModifiedProperties();
        }

        public Type GetValueType(SerializedProperty property)
        {
            SerializedProperty parent = property.GetParent();
            if (parent != null && parent.isArray)
            {
                return parent.GetParent().GetArrayElementValueType();
            }
            else
            {
                return property.GetValueType();
            }
        }
    }
}