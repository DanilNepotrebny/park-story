﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils
{
    [CustomPropertyDrawer(typeof(MinAttribute))]
    public class MinAttributeDrawer : PropertyDrawer
    {
        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            MinAttribute minAttribute = (MinAttribute)attribute;

            switch (property.propertyType)
            {
                case SerializedPropertyType.Float:
                    EditorGUI.PropertyField(position, property, label, false);
                    property.floatValue = Mathf.Max(property.floatValue, minAttribute.min);
                    break;

                case SerializedPropertyType.Integer:
                    EditorGUI.PropertyField(position, property, label, false);
                    property.intValue = Mathf.Max(property.intValue, (int)minAttribute.min);
                    break;

                default:
                    EditorGUI.LabelField(position, label.text, "Use Min attribute with float or int.");
                    break;
            }
        }
    }
}