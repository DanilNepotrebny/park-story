﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Utils
{
    [CustomPropertyDrawer(typeof(MinMaxRangeAttribute))]
    public class MinMaxRangeDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) + 16;
        }

        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Now draw the property as a Slider or an IntSlider based on whether it’s a float or integer.
            if (property.type != "MinMaxRange")
            {
                UnityEngine.Debug.LogWarning("Use only with MinMaxRange type");
            }
            else
            {
                var range = attribute as MinMaxRangeAttribute;
                var minValue = property.FindPropertyRelative("rangeStart");
                var maxValue = property.FindPropertyRelative("rangeEnd");
                float newMin = minValue.floatValue;
                float newMax = maxValue.floatValue;

                float xDivision = position.width * 0.33f;
                float yDivision = position.height * 0.5f;
                EditorGUI.LabelField(
                    new Rect(position.x, position.y, xDivision, yDivision),
                    label);

                EditorGUI.LabelField(
                    new Rect(position.x, position.y + yDivision, position.width, yDivision),
                    range.minLimit.ToString("0.##"));

                EditorGUI.LabelField(
                    new Rect(position.x + position.width - 28, position.y + yDivision, position.width, yDivision),
                    range.maxLimit.ToString("0.##"));

                EditorGUI.MinMaxSlider(
                    new Rect(position.x + 16, position.y + yDivision, position.width - 52, yDivision),
                    ref newMin,
                    ref newMax,
                    range.minLimit,
                    range.maxLimit);

                EditorGUI.LabelField(
                    new Rect(position.x + xDivision - 7, position.y, xDivision, yDivision),
                    "From: ");

                newMin = Mathf.Clamp(
                    EditorGUI.FloatField(
                        new Rect(position.x + xDivision + 30, position.y, xDivision - 30, yDivision),
                        newMin),
                    range.minLimit,
                    newMax);

                EditorGUI.LabelField(
                    new Rect(position.x + xDivision * 2f + 7, position.y, xDivision, yDivision),
                    "To: ");

                newMax = Mathf.Clamp(
                    EditorGUI.FloatField(
                        new Rect(position.x + xDivision * 2f + 30, position.y, xDivision - 30, yDivision),
                        newMax),
                    newMin,
                    range.maxLimit);

                minValue.floatValue = newMin;
                maxValue.floatValue = newMax;
            }
        }
    }
}