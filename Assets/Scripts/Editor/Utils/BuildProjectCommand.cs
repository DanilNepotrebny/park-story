﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using DreamTeam.Utils;
using UnityEditor;

namespace DreamTeam.Editor.Utils
{
    public static class BuildProjectCommand
    {
        public static void Build()
        {
            MethodBase method = MethodBase.GetCurrentMethod();
            string methodName = $"{method.ReflectedType.FullName}.{method.Name}";

            string[] fullArgs = Environment.GetCommandLineArgs();
            int idx = Array.IndexOf(fullArgs, methodName);

            Dictionary<string, string> args = ParseArguments(string.Join(" ", fullArgs.Skip(idx + 1)));

            if (!args.ContainsKey("outPath"))
            {
                throw new ArgumentException("Out path is not specified.");
            }

            string provisionPath;
            if (args.TryGetValue("provision", out provisionPath))
            {
                UpdateProvision(provisionPath);
            }

            BuildUtils.BuildProject(args["outPath"], args.GetValueOrDefault("defines") ?? string.Empty);
        }

        private static Dictionary<string, string> ParseArguments(string args)
        {
            var result = new Dictionary<string, string>();
            var regex = new Regex(
                    "(?<=(?:^|\\s)[-/])(?<name>\\w+)(?:=\"?(?<value>.+?)\"?(?:$|\\s))?"
                );

            foreach (Match match in regex.Matches(args))
            {
                result.Add(match.Groups["name"].Value, match.Groups["value"].Value);
            }

            return result;
        }

        private static void UpdateProvision(string path)
        {
            Type type = Type.GetType("UnityEditor.PlatformSupport.ProvisioningProfile, UnityEditor");

            object profile = ReflectionUtils.StaticInvoke<object>(type, "ParseProvisioningProfileAtPath", false, path);
            string uuid = ReflectionUtils.GetPropertyValue<string>(profile, "UUID");
            var profileType = ReflectionUtils.GetPropertyValue<ProvisioningProfileType>(profile, "type");

            UnityEngine.Debug.Log($"Changing provisioning profile to {profileType}: {uuid}");

            PlayerSettings.iOS.iOSManualProvisioningProfileID = uuid;
            PlayerSettings.iOS.iOSManualProvisioningProfileType = profileType;
        }
    }
}