﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.IO;
using System.Linq;
using DreamTeam.Editor.TextureImport;
using DreamTeam.Editor.Utils;
using DreamTeam.Utils;
using DreamTeam.Utils.Spine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Spine.Unity;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.SpineImport
{
    public static class SpineImporter
    {
        private const string _spinePathKey = "SpineImporterSpinePath";
        private const string _sourcesPathKey = "SpineImporterSourcesPath";
        private const string _spineExportSettingsGuid = "5cf62ec89d7a3a549ac20f696bede964";

        public static string SpinePath
        {
            get { return EditorPrefs.GetString(_spinePathKey); }
            set { EditorPrefs.SetString(_spinePathKey, value); }
        }

        public static string SourcesPath
        {
            get { return EditorPrefs.GetString(_sourcesPathKey); }
            set { EditorPrefs.SetString(_sourcesPathKey, value); }
        }

        public static void ImportSpine(SpineImportSettings settings, string path)
        {
            var exportSettings = AssetDatabaseUtils.LoadAssetByGuid<TextAsset>(_spineExportSettingsGuid);
            var json = JToken.Parse(exportSettings.text) as JObject;

            JToken textureSettings = json["packAtlas"];
            ApplyTextureSettings(settings, textureSettings);

            if (settings.AtlasImportSource != AtlasImportSource.Project)
            {
                json["packAtlas"] = null;
            }

            string projectExportSettingsPath = SaveJson(json);
            string projectName = Path.GetFileNameWithoutExtension(settings.ProjectPath);
            string projectPath = GetSourcePath(settings.ProjectPath);

            if (!File.Exists(projectPath))
            {
                throw new InvalidOperationException($"Ivalid project path '{projectPath}'.");
            }

            string args = $"-i \"{projectPath}\" -o \"{path}\" -e {projectExportSettingsPath}";

            if (settings.AtlasImportSource == AtlasImportSource.Folder)
            {
                string atlasPackSettingsPath = SaveJson(textureSettings);
                string imagesPath = GetSourcePath(settings.ImagesPath);

                if (!Directory.Exists(imagesPath))
                {
                    throw new InvalidOperationException($"Ivalid images path '{imagesPath}'.");
                }

                args += $" -i \"{imagesPath}\" -o \"{path}\" -n {projectName} -p {atlasPackSettingsPath}";
            }
            
            FileUtils.ExecuteProcess(SpinePath, args);
            AssetDatabase.Refresh();

            string dataName = projectName + "_SkeletonData.asset";
            var dataAsset = AssetDatabase.LoadAssetAtPath<SkeletonDataAsset>(Path.Combine(path, dataName));
            if (dataAsset != null)
            {
                // Set skeleton scale. Game units are in 2.88x so target density here is always 2.88x
                EditorUtils.RecordObjectUndo(dataAsset, "Import spine skeleton");
                dataAsset.scale = 0.01f * PixelDensity.X2_88.ToFloat() / settings.SourceDensity.ToFloat();

                settings.Save(dataAsset);
            }
            else
            {
                UnityEngine.Debug.LogError($"Can't find {dataName}. Please make sure that project and skeleton has the same name.");
            }

            if (settings.AtlasImportSource != AtlasImportSource.None)
            {
                string atlasName = projectName + "_Atlas.asset";
                string atlasPath = Path.Combine(path, atlasName);
                var atlasAsset = AssetDatabase.LoadAssetAtPath<AtlasAssetBase>(atlasPath);
                if (atlasAsset != null)
                {
                    foreach (Material m in atlasAsset.Materials)
                    {
                        // Change default shader because we don't want premultiplied alpha
                        if (m.shader.name == "Spine/Skeleton")
                        {
                            EditorUtils.RecordObjectUndo(m, "Change spine material shader");
                            m.shader = Shader.Find("Custom/Skeleton-AlphaBlend");
                        }
                    }

                    if (settings.UsesAssetBundles)
                    {
                        string atlasSourceName = projectName + ".atlas.txt";
                        var atlasSourceAsset = AssetDatabase.LoadAssetAtPath<TextAsset>(Path.Combine(path, atlasSourceName));

                        var pageMaterialsData = new BundledSpineAtlasAsset.PageMaterialData[atlasAsset.MaterialCount];

                        int i = 0;
                        foreach (Material m in atlasAsset.Materials)
                        {
                            var materialData = new BundledSpineAtlasAsset.PageMaterialData();
                            pageMaterialsData[i] = materialData;

                            string materialName = BundledSpineAtlasAsset.GetMaterialNameFromTextureName(
                                    m.mainTexture.name
                                );

                            var dataEditorApi = (BundledSpineAtlasAsset.PageMaterialData.IEditorApi)materialData;
                            dataEditorApi.SetName(materialName);

                            AssetImporter materialImporter = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(m));
                            materialImporter.assetBundleName = materialName;
                            materialImporter.SaveAndReimport();

                            var materialRef = (BundledSpineAtlasAsset.MaterialReference.IEditorApi)materialData.MaterialRef;
                            materialRef.SetAsset(m);

                            i++;
                        }

                        var bundledAtlasAsset = BundledSpineAtlasAsset.CreateInstance(atlasSourceAsset, pageMaterialsData);

                        string bundledAtlasName = projectName + "_BundledAtlas.asset";
                        AssetDatabase.CreateAsset(bundledAtlasAsset, Path.Combine(path, bundledAtlasName));

                        if (dataAsset != null)
                        {
                            EditorUtils.RecordObjectUndo(dataAsset, "Set bundled atlas to skeleton data asset");
                            dataAsset.atlasAssets = new AtlasAssetBase[] { bundledAtlasAsset };
                        }

                        AssetDatabase.DeleteAsset(atlasPath);
                    }
                }
                else
                {
                    UnityEngine.Debug.LogError(
                        $"Can't find {atlasName}. Please make sure that project and skeleton has the same name.");
                }
            }

            AssetDatabase.SaveAssets();
        }

        public static void ValidateImportSettings(SpineImportSettings settings, string path)
        {
            string projectPath = GetSourcePath(settings.ProjectPath);

            if (!File.Exists(projectPath))
            {
                throw new InvalidOperationException($"Ivalid project path '{projectPath}'.");
            }

            if (settings.AtlasImportSource == AtlasImportSource.Folder)
            {
                string imagesPath = GetSourcePath(settings.ImagesPath);

                if (!Directory.Exists(imagesPath))
                {
                    throw new InvalidOperationException($"Ivalid images path '{imagesPath}'.");
                }
            }
        }

        private static string GetSourcePath(string path)
        {
            if (path == null)
            {
                throw new InvalidOperationException($"Ivalid project path '{path}'");
            }

            return Path.Combine(SourcesPath, path);
        }

        private static string SaveJson(JToken json)
        {
            string path = FileUtils.GetTempFileName("json");
            using (FileStream stream = File.Create(path))
            {
                using (var streamWriter = new StreamWriter(stream))
                {
                    using (var jsonWriter = new JsonTextWriter(streamWriter))
                    {
                        json.WriteTo(jsonWriter);
                    }
                }
            }

            return path;
        }

        private static void ApplyTextureSettings(SpineImportSettings settings, JToken textureSettings)
        {
            textureSettings["maxWidth"] = settings.MaxTextureSize;
            textureSettings["maxHeight"] = settings.MaxTextureSize;

            float scaleFactor = settings.TargetDensity.ToFloat() / settings.SourceDensity.ToFloat();
            textureSettings["scale"][0] = scaleFactor;

            CompressionFormat format = settings.CompressionFormat;
            textureSettings["pot"] = format.HasPotRestriction();
            textureSettings["square"] = format.HasSquareRestriction();
        }

        [MenuItem("Tools/Graphics import/Spine/Reimport all")]
        private static void ImportAll()
        {
            string[] assetPathes = AssetDatabase.FindAssets($"t:{nameof(SkeletonDataAsset)}");
            ProcessSkeletonDataAssets(
                    assetPathes.Select(AssetDatabaseUtils.LoadAssetByGuid<SkeletonDataAsset>).ToArray(),
                    ImportSpine,
                    "Importing spine animations",
                    "Importing"
                );
        }

        [MenuItem("Tools/Graphics import/Spine/Validate all")]
        private static void ValidateAll()
        {
            string[] assetPathes = AssetDatabase.FindAssets($"t:{nameof(SkeletonDataAsset)}");
            ProcessSkeletonDataAssets(
                    assetPathes.Select(AssetDatabaseUtils.LoadAssetByGuid<SkeletonDataAsset>).ToArray(),
                    ValidateImportSettings,
                    "Validations spine import settings",
                    "Validating "
                );
        }

        [MenuItem("Assets/Spine/Reimport selected skeleton data")]
        private static void ImportSelected()
        {
            ProcessSkeletonDataAssets(
                    Selection.objects.Cast<SkeletonDataAsset>().ToArray(),
                    ImportSpine,
                    "Importing spine animations",
                    "Importing"
                );
        }

        [MenuItem("Assets/Spine/Validate import settings for selected skeleton data")]
        private static void ValidateImportSettings()
        {
            ProcessSkeletonDataAssets(
                    Selection.objects.Cast<SkeletonDataAsset>().ToArray(),
                    ValidateImportSettings,
                    "Validations spine import settings",
                    "Validating "
                );
        }

        private static void ProcessSkeletonDataAssets(
                SkeletonDataAsset[] assets,
                Action<SpineImportSettings, string> action,
                string title,
                string actionDescription
            )
        {
            int counter = 0;
            foreach (SkeletonDataAsset skeletonDataAsset in assets)
            {
                string path = AssetDatabaseUtils.GetAssetDirPath(skeletonDataAsset);

                bool isCancelled = EditorUtility.DisplayCancelableProgressBar(
                        title,
                        $"{actionDescription} {path} ({counter}/{assets.Length})",
                        (float)counter / assets.Length
                    );
                if (isCancelled)
                {
                    break;
                }

                try
                {
                    SpineImportSettings settings = new SpineImportSettings();
                    settings.Load(skeletonDataAsset);
                    action?.Invoke(settings, path);
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.LogException(e, skeletonDataAsset);
                }

                counter++;
            }

            EditorUtility.ClearProgressBar();
        }
    }
}