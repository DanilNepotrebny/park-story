﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Linq;
using DreamTeam.Editor.TextureImport;
using DreamTeam.Editor.Utils;
using Spine.Unity;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.SpineImport
{
    public class SpineImportWindow : EditorWindow
    {
        private SpineImportSettings _settings = new SpineImportSettings();
        private Object _selectedAsset;

        protected void Update()
        {
            var asset = AssetDatabaseUtils.LoadAssetByGuid<Object>(Selection.assetGUIDs.FirstOrDefault());
            if (asset != _selectedAsset)
            {
                _selectedAsset = asset;

                var skeletonData = _selectedAsset as SkeletonDataAsset;
                if (skeletonData != null)
                {
                    _settings.Load(skeletonData);
                }

                Repaint();
            }
        }

        protected void OnGUI()
        {
            SpineImporter.SpinePath = EditorGUIUtils.FilePathField("Spine path", SpineImporter.SpinePath, "exe");
            SpineImporter.SourcesPath = EditorGUIUtils.FolderPathField("Sources path", SpineImporter.SourcesPath);

            string path = AssetDatabaseUtils.GetAssetDirPath(_selectedAsset);
            if (path == null)
            {
                EditorGUILayout.LabelField("Please, select asset or folder.");
                return;
            }

            GUI.enabled = false;
            {
                var skeletonData = _selectedAsset as SkeletonDataAsset;
                if (skeletonData != null)
                {
                    EditorGUILayout.ObjectField("Skeleton", skeletonData, typeof(SkeletonDataAsset), false);
                }
                else
                {
                    EditorGUILayout.TextField("Import into", path);
                }
            }
            GUI.enabled = true;

            _settings.ProjectPath = EditorGUIUtils.FilePathField("Project path", _settings.ProjectPath, "spine", SpineImporter.SourcesPath);
            _settings.SourceDensity = (PixelDensity)EditorGUILayout.EnumPopup("Source density", _settings.SourceDensity);
            _settings.TargetDensity = (PixelDensity)EditorGUILayout.EnumPopup("Target density", _settings.TargetDensity);

            _settings.AtlasImportSource = (AtlasImportSource)EditorGUILayout.EnumPopup("Atlas import source", _settings.AtlasImportSource);
            if (_settings.AtlasImportSource == AtlasImportSource.Folder)
            {
                _settings.ImagesPath = EditorGUIUtils.FolderPathField("Images path", _settings.ImagesPath, SpineImporter.SourcesPath);
            }

            _settings.CompressionFormat = (CompressionFormat)EditorGUILayout.EnumPopup("Compression format", _settings.CompressionFormat);

            if (_settings.AtlasImportSource != AtlasImportSource.None)
            {
                _settings.MaxTextureSize = EditorGUILayout.IntField("Max texture size", _settings.MaxTextureSize);
            }

            _settings.UsesAssetBundles = EditorGUILayout.Toggle("Uses Asset Bundles", _settings.UsesAssetBundles);

            if (GUILayout.Button("Import"))
            {
                SpineImporter.ImportSpine(_settings, path);
            }
        }

        [MenuItem("Tools/Graphics import/Spine/Import settings")]
        private static void ShowWindow()
        {
            EditorWindow window = GetWindow(typeof(SpineImportWindow));
            window.titleContent.text = "Spine Importer";
        }
    }
}