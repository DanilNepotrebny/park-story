﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

namespace DreamTeam.Editor.SpineImport
{
    public enum AtlasImportSource
    {
        None,
        Project,
        Folder
    }
}