﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Editor.TextureImport;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Spine.Unity;
using UnityEditor;

namespace DreamTeam.Editor.SpineImport
{
    public class SpineImportSettings
    {
        public string ProjectPath { get; set; }
        public AtlasImportSource AtlasImportSource { get; set; }
        public string ImagesPath { get; set; }
        public PixelDensity SourceDensity { get; set; }
        public PixelDensity TargetDensity { get; set; }
        public CompressionFormat CompressionFormat { get; set; }
        public int MaxTextureSize { get; set; }
        public bool UsesAssetBundles { get; set; }

        public SpineImportSettings()
        {
            Clear();
        }

        public void Save(SkeletonDataAsset asset)
        {
            AssetImporter importer = GetImporter(asset);
            importer.userData = JsonConvert.SerializeObject(this);
            importer.SaveAndReimport();
        }

        public void Load(SkeletonDataAsset asset)
        {
            Clear();

            AssetImporter importer = GetImporter(asset);
            if (!string.IsNullOrEmpty(importer.userData))
            {
                JsonConvert.PopulateObject(
                        importer.userData,
                        this,
                        new JsonSerializerSettings { ObjectCreationHandling = ObjectCreationHandling.Replace }
                    );
            }
        }

        private void Clear()
        {
            ProjectPath = null;
            AtlasImportSource = AtlasImportSource.Project;
            ImagesPath = null;
            SourceDensity = PixelDensity.X4;
            TargetDensity = PixelDensity.X2;
            CompressionFormat = CompressionFormat.Pvrtc;
            MaxTextureSize = 2048;
            UsesAssetBundles = false;
        }

        [NotNull]
        private static AssetImporter GetImporter(SkeletonDataAsset asset)
        {
            string path = AssetDatabase.GetAssetPath(asset);
            AssetImporter importer = AssetImporter.GetAtPath(path);
            if (importer == null)
            {
                throw new InvalidOperationException($"Can't obtain importer at {path}.");
            }

            return importer;
        }
    }
}