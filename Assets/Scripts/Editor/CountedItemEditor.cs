﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Editor.Utils;
using DreamTeam.Inventory.Items;
using UnityEditor;

namespace DreamTeam.Editor
{
    [CustomEditor(typeof(CountedItem), true)]
    public class CountedItemEditor : UnityEditor.Editor
    {
        private EditorGUIUtils.FoldableEditorState _defaultProductPackState = new EditorGUIUtils.FoldableEditorState();

        /// <summary>
        /// Unity calback
        /// </summary>
        /// <inheritdoc />
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Separator();

            EditorGUIUtils.CreatableScriptableObjectPropertyField(
                    serializedObject.FindProperty("_defaultProductPack"),
                    true,
                    _defaultProductPackState
                );
        }
    }
}