﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR && UNITY_IOS

using UnityEditor;
using UnityEditor.iOS.Xcode;

namespace DreamTeam.Editor
{
    [InitializeOnLoad]
    public class PushNotificationsBuildConfigurator
    {
        static PushNotificationsBuildConfigurator()
        {
            PBXProjectPostProcessor.AddCapabilitiesProcessor(ProcessCapabilities);
        }

        private static void ProcessCapabilities(ProjectCapabilityManager capabilitiesManager)
        {
            #if PRODUCTION
            {
                capabilitiesManager.AddPushNotifications(false);
            }
            #else
            {
                capabilitiesManager.AddPushNotifications(true);
            }
            #endif

            capabilitiesManager.AddBackgroundModes(BackgroundModesOptions.RemoteNotifications);
        }
    }
}

#endif