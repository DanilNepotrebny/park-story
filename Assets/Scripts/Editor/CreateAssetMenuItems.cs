﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.IO;
using DreamTeam.Clothing;
using DreamTeam.Editor.Utils;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Inventory.Prices;
using DreamTeam.Inventory.Rewards;
using DreamTeam.Location.Decorations;
using DreamTeam.Location.Unlockables;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

#if !NO_MATCH3
using DreamTeam.Match3.Levels;
#endif

namespace DreamTeam.Editor
{
    internal static class CreateAssetMenuItems
    {
        #if !NO_MATCH3
        /// <summary>
        /// Creates menu for automagical creation of all match 3 level parts.
        /// </summary>
        [MenuItem("Assets/Create/Match 3/Levels/Level", false)]
        private static void CreateLevel()
        {
            const string sceneDefaultName = "Level1.unity";
            const string sceneTemplatePath = "Assets/Scenes/LevelTemplate.unity";

            // Create scene from template
            string scenePathAndName = GenerateAssetName(sceneDefaultName);
            AssetDatabase.CopyAsset(sceneTemplatePath, scenePathAndName);
            AssetDatabase.SaveAssets();
            Selection.activeObject = AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePathAndName);
        }
        #endif

        [MenuItem("Assets/Create/Location/Unlockable", false)]
        private static void CreateUnlockable()
        {
            const string unlockableDefaultName = "Unlockable1.asset";
            const string unlockableSystemName = "Assets/Unlockables/Prefabs/UnlockableSystem.prefab";

            var systemPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(unlockableSystemName);

            // Create unlockable
            string settingsPathAndName = GenerateAssetName(unlockableDefaultName);
            Unlockable unlockable = ScriptableObject.CreateInstance<Unlockable>();
            AssetDatabase.CreateAsset(unlockable, settingsPathAndName);

            // Add unlockable into system
            var system = systemPrefab.GetComponent<UnlockableSystem>();
            (system as UnlockableSystem.IEditorAPI).AddUnlockable(unlockable);
            EditorUtility.SetDirty(system);

            // Save changes and finish
            AssetDatabase.SaveAssets();
            Selection.activeObject = unlockable;
        }

        [MenuItem("Assets/Create/Clothing/Model part", false)]
        public static void CreateClothingModelPart()
        {
            var selectedAsset = Selection.activeObject as GameObject;

            string settingsPathAndName = GenerateAssetName("ClothingModelPart1.asset");

            Object result;
            if (MeshClothingModelPart.IsAssetCompatible(selectedAsset))
            {
                result = MeshClothingModelPart.CreateAsset(selectedAsset);
            }
            else if (SkinnedClothingModelPart.IsAssetCompatible(selectedAsset))
            {
                result = SkinnedClothingModelPart.CreateAsset(selectedAsset);
            }
            else
            {
                throw new InvalidOperationException("Incompatible asset " + selectedAsset);
            }

            // Save changes and finish
            AssetDatabase.CreateAsset(result, settingsPathAndName);
            AssetDatabase.SaveAssets();
            Selection.activeObject = result;
        }

        [MenuItem("Assets/Create/Clothing/Model part", true)]
        public static bool ValidateClothingModelPart()
        {
            var selectedAsset = Selection.activeObject as GameObject;
            if (selectedAsset == null)
            {
                return false;
            }

            if (PrefabUtility.GetPrefabType(selectedAsset) != PrefabType.ModelPrefab)
            {
                return false;
            }

            return MeshClothingModelPart.IsAssetCompatible(selectedAsset) ||
                SkinnedClothingModelPart.IsAssetCompatible(selectedAsset);
        }

        [MenuItem("Assets/Create/Location/DecorationSkin", false)]
        private static void CreateDecorationSkin()
        {
            const string defaultName = "DecorationSkin1.asset";
            const string moneyAssetPath = "Assets/Settings/Inventory/Money.asset";

            DecorationSkin decorationSkin = ScriptableObject.CreateInstance<DecorationSkin>();

            string settingsPathAndName = GenerateAssetName(defaultName);
            AssetDatabase.CreateAsset(decorationSkin, settingsPathAndName);

            ProductPack productPack = ScriptableObject.CreateInstance<ProductPack>();

            ((DecorationSkin.IEditorApi)decorationSkin).SetDefaultProductPack(productPack);

            // Add default reward
            CountedItemFixedReward reward = ScriptableObject.CreateInstance<CountedItemFixedReward>();

            var rewardEditorApi = ((CountedItemFixedReward.IEditorApi)reward);
            rewardEditorApi.SetItem(decorationSkin);
            rewardEditorApi.SetCount(1);

            var productPackEditorApi = ((ProductPack.IEditorApi)productPack);
            productPackEditorApi.AddReward(reward);

            // Add default price
            CountedItemPrice price = ScriptableObject.CreateInstance<CountedItemPrice>();

            CountedItem moneyAsset = AssetDatabase.LoadAssetAtPath<CountedItem>(moneyAssetPath);

            var priceEditorApi = ((CountedItemPrice.IEditorApi)price);
            priceEditorApi.SetItem(moneyAsset);
            priceEditorApi.SetCount(10);
            productPackEditorApi.SetPrice(price);

            AssetDatabaseUtils.AddSubAsset(
                    "DefaultProductPack",
                    productPack,
                    decorationSkin
                );
            AssetDatabaseUtils.AddSubAsset(
                    "DefaultProductPackReward",
                    reward,
                    decorationSkin
                );
            AssetDatabaseUtils.AddSubAsset(
                    "DefaultProductPackPrice",
                    price,
                    decorationSkin
                );

            // Save changes and finish
            AssetDatabase.SaveAssets();
            Selection.activeObject = decorationSkin;
        }

        private static string GetCurrentFolder()
        {
            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (string.IsNullOrEmpty(path))
            {
                path = "Assets";
            }
            else if (!AssetDatabase.IsValidFolder(path))
            {
                path = Path.GetDirectoryName(path);
            }

            return path;
        }

        private static string GenerateAssetName(string name)
        {
            return AssetDatabase.GenerateUniqueAssetPath(Path.Combine(GetCurrentFolder(), name));
        }
    }
}