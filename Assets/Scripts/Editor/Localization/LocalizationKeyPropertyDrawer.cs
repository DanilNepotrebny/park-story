﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using DreamTeam.Localization;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Localization
{
    [CustomPropertyDrawer(typeof(LocalizationKeyAttribute))]
    public class LocalizationKeyPropertyDrawer : PropertyDrawer
    {
        private string _filter;
        private int _selectedIdentifierIndex;
        private List<string> _identifierPopupOptions = new List<string>();

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float result = 0f;

            LocalizationSystem.IEditorApi system = GetLocalizationSystem();
            if (system != null && system.Keys != null)
            {
                result = EditorGUIUtility.singleLineHeight * 3f + 8f;
            }
            else
            {
                result = EditorGUIUtility.singleLineHeight * 2f + 7f;
            }

            return result;
        }

        public override void OnGUI(Rect drawRect, SerializedProperty property, GUIContent label)
        {
            if (property.type != "string")
            {
                UnityEngine.Debug.LogWarning("Use only with MinMaxRange type");
            }
            else
            {
                LocalizationSystem.IEditorApi system = GetLocalizationSystem();

                EditorGUI.DrawRect(drawRect, new Color(0, 0, 0, 0.1f));

                float offsetX = 0f;
                float offsetY = 2f;
                float propertyHeight = system?.Keys != null ?
                    (drawRect.size.y - 8f) / 3f :
                    (drawRect.size.y - 7f) / 2f;

                Rect currentDrawRect = drawRect;
                currentDrawRect.position = new Vector2(drawRect.position.x + offsetX, drawRect.position.y + offsetY);
                currentDrawRect.size = new Vector2(drawRect.size.x, propertyHeight);
                EditorGUI.LabelField(currentDrawRect, $"Localize {label.text}", EditorStyles.boldLabel);
                offsetY += propertyHeight + 1f;

                if (system?.Keys != null)
                {
                    offsetX = 0f;

                    float labelSize = EditorGUIUtility.labelWidth;
                    float filterSize = 100f;

                    currentDrawRect.position = new Vector2(drawRect.position.x + offsetX, drawRect.position.y + offsetY);
                    currentDrawRect.size = new Vector2(labelSize, propertyHeight);
                    EditorGUI.LabelField(currentDrawRect, "Key");
                    offsetX += labelSize;

                    currentDrawRect.position = new Vector2(drawRect.position.x + offsetX, drawRect.position.y + offsetY);
                    currentDrawRect.size = new Vector2(filterSize, propertyHeight);
                    _filter = EditorGUI.TextArea(currentDrawRect, _filter);
                    offsetX += filterSize + 1f;

                    FillPopupData(system, property.stringValue, ref _selectedIdentifierIndex, ref _identifierPopupOptions);

                    bool isIdSet = true;
                    if (_selectedIdentifierIndex < 0)
                    {
                        _identifierPopupOptions.Insert(0, "None");
                        _selectedIdentifierIndex = 0;
                        isIdSet = false;
                    }

                    GUI.color = isIdSet ? Color.white : Color.red;

                    currentDrawRect.position = new Vector2(drawRect.position.x + offsetX, drawRect.position.y + offsetY);
                    currentDrawRect.size = new Vector2(drawRect.size.x - offsetX, propertyHeight);
                    int index = EditorGUI.Popup(
                        currentDrawRect,
                        _selectedIdentifierIndex,
                        _identifierPopupOptions.ToArray());
                    offsetY += propertyHeight + 1f;

                    if (index != _selectedIdentifierIndex)
                    {
                        property.stringValue = _identifierPopupOptions[index];
                    }

                    offsetX = 0f;

                    currentDrawRect.position = new Vector2(drawRect.position.x + offsetX, drawRect.position.y + offsetY);
                    currentDrawRect.size = new Vector2(labelSize, propertyHeight);
                    EditorGUI.LabelField(currentDrawRect, "Preview");
                    offsetX += labelSize;

                    currentDrawRect.position = new Vector2(drawRect.position.x + offsetX, drawRect.position.y + offsetY);
                    currentDrawRect.size = new Vector2(drawRect.size.x - offsetX, propertyHeight);
                    EditorGUI.LabelField(currentDrawRect, system.LocalizeEditor(property.stringValue), EditorStyles.textArea);

                    GUI.color = Color.white;
                }
                else
                {
                    offsetX = 0f;

                    currentDrawRect.position = new Vector2(drawRect.position.x + offsetX, drawRect.position.y + offsetY);
                    currentDrawRect.size = new Vector2(drawRect.size.x, propertyHeight);
                    EditorGUI.LabelField(currentDrawRect, "Localization system is loading...");
                }
            }
        }

        private void FillPopupData(LocalizationSystem.IEditorApi system, string currentValue, ref int selectedIndex, ref List<string> options)
        {
            options.Clear();

            IEnumerable<string> keys = system.Keys;

            if (!string.IsNullOrEmpty(_filter))
            {
                keys = keys.Where(option => option.Contains(_filter));
            }

            options.AddRange(keys);

            selectedIndex = options.IndexOf(currentValue);
        }

        private LocalizationSystem.IEditorApi GetLocalizationSystem()
        {
            return !EditorUtils.IsPlayingOrWillChangePlaymode() ?
                SystemAssetAccessor<LocalizationSystem>.Instance :
                GameObject.FindObjectOfType<LocalizationSystem>();
        }
    }
}