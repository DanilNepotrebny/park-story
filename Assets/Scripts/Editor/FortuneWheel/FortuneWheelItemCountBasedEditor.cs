﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Popups.FortuneWheel.Items;
using UnityEditor;

namespace DreamTeam.Editor.FortuneWheel
{
    [CustomEditor(typeof(FortuneWheelItemCountBased))]
    public class FortuneWheelItemCountBasedEditor : FortuneWheelItemBaseEditor
    {
    }
}