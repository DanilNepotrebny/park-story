﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Editor.Utils;
using DreamTeam.Popups.FortuneWheel.Items;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.FortuneWheel
{
    [CustomEditor(typeof(FortuneWheelItemBase))]
    public class FortuneWheelItemBaseEditor : UnityEditor.Editor
    {
        [SerializeField]
        private List<EditorGUIUtils.FoldableEditorState> _editorState = new List<EditorGUIUtils.FoldableEditorState>();

        private SerializedProperty _sectorProperty;
        private SerializedProperty _iconProperty;
        private SerializedProperty _textProperty;
        private SerializedProperty _rewardsPresentationProperty;
        private SerializedProperty _rewardsProperty;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            EditorGUILayout.PropertyField(_sectorProperty);
            EditorGUILayout.PropertyField(_iconProperty);
            EditorGUILayout.PropertyField(_textProperty);
            EditorGUILayout.PropertyField(_rewardsPresentationProperty);

            EditorGUIUtils.CreatableScriptableObjectList(_rewardsProperty, _editorState);

            serializedObject.ApplyModifiedProperties();
        }

        protected void OnEnable()
        {
            _sectorProperty = serializedObject.FindProperty("_sector");
            _iconProperty = serializedObject.FindProperty("_icon");
            _textProperty = serializedObject.FindProperty("_text");
            _rewardsPresentationProperty = serializedObject.FindProperty("_rewardsPresentationSequence");
            _rewardsProperty = serializedObject.FindProperty("_rewards");
        }
    }
}