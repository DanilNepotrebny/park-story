﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Editor.GridEditor;
using DreamTeam.Utils;

namespace DreamTeam.Editor.Metagame
{
    /// <summary>
    /// Asset for metagame grid editor settings
    /// </summary>
    public class MetagameSettings : BaseSettings
    {
        /// <summary>
        /// Width of a cell in units
        /// </summary>
        [Min(0.01f)] public float cellWidth;

        /// <summary>
        /// Height of a cell in units
        /// </summary>
        [Min(0.01f)] public float cellHeight;
    }
}