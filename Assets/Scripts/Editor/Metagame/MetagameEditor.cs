﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Linq;
using DreamTeam.Editor.AssetBrushes;
using DreamTeam.Editor.AssetInstanceTools;
using DreamTeam.Editor.GridEditor;
using DreamTeam.Editor.PositionTransformers;
using DreamTeam.Editor.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Metagame
{
    public class MetagameEditor : BaseGridEditorWindow
    {
        private MetagameSettings _settings;

        /// <summary>
        /// Creates brushes to be used in the editor.
        /// </summary>
        protected override BaseBrush[] CreateAssetBrushes()
        {
            ToolPalette toolPalette = new ToolPalette(
                    new BaseTool[]
                    {
                        new SpriteTool(),
                        new GameObjectFromPrefabTool()
                    }
                );

            GridTransformer gridTransformer = new GridTransformer(WorldToGrid, GridToWorld);

            return new BaseBrush[]
            {
                new PaintBrush(
                        new GUIContent(
                                "Grid Paint Brush",
                                (Texture)EditorGUIUtility.Load("Icons/GridAssetBrushIcon.png"),
                                "Grid snapped paint brush."
                            ),
                        gridTransformer,
                        toolPalette
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Erase Brush",
                                (Texture)EditorGUIUtility.Load("Icons/EraseCellIcon.png"),
                                "Grid snapped erase brush."
                            ),
                        new EraseCellGameObjectsTool { GridTransformer = gridTransformer },
                        gridTransformer
                    ),
                new SinglePaintBrush(
                        new GUIContent(
                                "Freehand Paint Brush",
                                (Texture)EditorGUIUtility.Load("Icons/SimpleAssetBrushIcon.png"),
                                "Freehand paint brush."
                            ),
                        toolPalette
                    ),
                new PaintBrush(
                        new GUIContent(
                                "Grid Replace Brush",
                                (Texture)EditorGUIUtility.Load("Icons/GridAssetBrushIcon.png"),
                                "Grid snapped replace brush."
                            ),
                        gridTransformer,
                        toolPalette,
                        true,
                        new EraseCellGameObjectsTool { GridTransformer = gridTransformer }
                    )
            };
        }

        /// <summary>
        /// Finds and chaches settings in AssetDatabase
        /// </summary>
        protected override BaseSettings GetBaseSettings()
        {
            return GetSettings();
        }

        /// <summary>
        /// Finds and chaches settings in AssetDatabase
        /// </summary>
        protected MetagameSettings GetSettings()
        {
            if (_settings == null)
            {
                _settings = AssetDatabaseUtils.LoadOrCreateAsset<MetagameSettings>("Assets/Settings/Editor");
            }

            return _settings;
        }

        protected override void DrawGridLine(Vector3 begin, Vector3 end)
        {
            ILayer layer = AssetBrushPanel?.SelectedLayer;
            if (layer != null)
            {
                begin.z = end.z = layer.GetZPosition();
            }

            base.DrawGridLine(begin, end);
        }

        protected override void CalculateSceneViewGridBounds(out Vector2Int min, out Vector2Int max)
        {
            min = new Vector2Int();
            max = new Vector2Int();

            float screenViewWidth = SceneView.currentDrawingSceneView.camera.pixelWidth;
            float screenViewHeight = SceneView.currentDrawingSceneView.camera.pixelHeight;

            Vector2Int grid1 = WorldToGrid(
                SceneView.currentDrawingSceneView.camera.ScreenPointToRay(new Vector2(0f, 0f)).origin);
            Vector2Int grid2 = WorldToGrid(
                SceneView.currentDrawingSceneView.camera.ScreenPointToRay(new Vector2(0, screenViewHeight)).origin);
            Vector2Int grid3 = WorldToGrid(
                SceneView.currentDrawingSceneView.camera.ScreenPointToRay(new Vector2(screenViewWidth, 0)).origin);
            Vector2Int grid4 = WorldToGrid(
                SceneView.currentDrawingSceneView.camera
                    .ScreenPointToRay(new Vector2(screenViewWidth, screenViewHeight))
                    .origin);

            min.x = Mathf.Min(grid1.x, grid2.x, grid3.x, grid4.x);
            min.y = Mathf.Min(grid1.y, grid2.y, grid3.y, grid4.y);
            max.x = Mathf.Max(grid1.x, grid2.x, grid3.x, grid4.x);
            max.y = Mathf.Max(grid1.y, grid2.y, grid3.y, grid4.y);
        }

        protected override Vector2 WorldToGridF(Vector3 worldPosition)
        {
            float halfCellWidth = 0.5f * _settings.cellWidth;
            float halfCellHeight = 0.5f * _settings.cellHeight;
            return new Vector2(
                0.5f * (worldPosition.x / halfCellWidth + worldPosition.y / halfCellHeight),
                0.5f * (worldPosition.y / halfCellHeight - worldPosition.x / halfCellWidth));
        }

        protected override Vector3 GridToWorldF(Vector2 gridPosition)
        {
            return new Vector3(
                0.5f * (gridPosition.x - gridPosition.y) * _settings.cellWidth,
                0.5f * (gridPosition.x + gridPosition.y) * _settings.cellHeight,
                0f);
        }

        protected Vector2Int WorldToGrid(Vector3 worldPosition)
        {
            Vector2 gridPosF = WorldToGridF(worldPosition);
            return new Vector2Int(Mathf.RoundToInt(gridPosF.x), Mathf.RoundToInt(gridPosF.y));
        }

        protected Vector3 GridToWorld(Vector2Int gridPosition)
        {
            return GridToWorldF(new Vector2(gridPosition.x, gridPosition.y));
        }

        [MenuItem("Tools/Metagame/GridEditor")]
        private static void OpenWindow()
        {
            var inspectorType = typeof(UnityEditor.Editor).Assembly.GetTypes()
                .FirstOrDefault(t => t.Name == "InspectorWindow");

            GetWindow<MetagameEditor>("Metagame Grid Editor", inspectorType);
        }
    }
}