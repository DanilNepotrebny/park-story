﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using Cinemachine.Utility;
using DreamTeam.SceneLoading;
using DreamTeam.Utils;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DreamTeam.Editor.Metagame
{
    internal static class ValidateLocationsMenuItem
    {
        private const string _locationCommonGuid = "7d6edf902425fdb449ef657afc8e14e0";
        private const float _maxScale = 0.9f;

        [MenuItem("Tools/Metagame/Validate locations")]
        private static void ValidateLocations()
        {
            var data = SystemAssetAccessor<LoadingDependenciesData>.Asset;
            string locationCommonPath = AssetDatabase.GUIDToAssetPath(_locationCommonGuid);

            IEnumerable<string> scenePathes = data
                .FindSceneDependencies(locationCommonPath)
                .Parts
                .Select(reference => reference.AssetPath)
                .Prepend(locationCommonPath);

            var scenes = new List<Scene>();

            foreach (string path in scenePathes)
            {
                scenes.Add(EditorSceneManager.OpenScene(path, OpenSceneMode.Additive));
            }

            var maxScales = new Dictionary<Sprite, Vector3>();

            foreach (Scene scene in scenes)
            {
                foreach (var renderer in scene.GetComponentsOnScene<SpriteRenderer>())
                {
                    Vector3 s = renderer.transform.lossyScale.Abs();
                    Vector3 max;

                    if (renderer.sprite == null)
                    {
                        UnityEngine.Debug.LogWarning($"Sprite renderer {renderer.name} has no sprite specified", renderer);
                        continue;
                    }

                    max = maxScales.TryGetValue(renderer.sprite, out max) ?
                        Vector3.Max(max, s) :
                        s;

                    maxScales[renderer.sprite] = max;
                }
            }

            foreach (var kv in maxScales)
            {
                Sprite sprite = kv.Key;
                Vector3 maxScale = kv.Value;

                float max = Mathf.Min(maxScale.x, maxScale.y);

                if (max < _maxScale)
                {
                    UnityEngine.Debug.LogWarning($"'{sprite.name}' max scale is {max}, image could be smaller", sprite);
                }
            }
        }
    }
}