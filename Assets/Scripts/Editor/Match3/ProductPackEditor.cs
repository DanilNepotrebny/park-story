﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Editor.Utils;
using DreamTeam.Inventory;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Match3
{
    [CustomEditor(typeof(ProductPack))]
    public class ProductPackEditor : UnityEditor.Editor
    {
        [SerializeField]
        private List<EditorGUIUtils.FoldableEditorState> _editorState = new List<EditorGUIUtils.FoldableEditorState>();

        private EditorGUIUtils.FoldableEditorState _priceState = new EditorGUIUtils.FoldableEditorState();

        /// <summary>
        /// Unity calback
        /// </summary>
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUIUtils.CreatableScriptableObjectList(serializedObject.FindProperty("_rewards"), _editorState);

            EditorGUIUtils.CreatableScriptableObjectPropertyField(
                    serializedObject.FindProperty("_price"),
                    true,
                    _priceState
                );
        }
    }
}