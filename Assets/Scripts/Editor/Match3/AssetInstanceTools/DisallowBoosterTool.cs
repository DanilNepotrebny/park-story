﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class DisallowBoosterTool : BaseCellTool
    {
        public DisallowBoosterTool(GridSystem.EditorApi gridApi) :
            base(gridApi)
        {
        }

        /// <inheritdoc />
        protected override string UndoMessageForCreate => "Mark cell allow generate booster at start";

        /// <inheritdoc />
        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return FindCell(position) != null;
        }

        protected override void Modify(CellComponent cell)
        {
            (cell as CellComponent.IEditorAPI).SetAllowBoosterChipGeneration(false);
        }

        protected override void Erase(CellComponent cell)
        {
            (cell as CellComponent.IEditorAPI).SetAllowBoosterChipGeneration(true);
        }
    }
}