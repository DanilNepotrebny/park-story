﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using DreamTeam.Match3.Teleportation;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class TeleportTool : GridSystemTool
    {
        private Teleport _teleport;

        private static GameObject _teleportOutPrefab;

        protected override string UndoMessageForCreate => null;

        static TeleportTool()
        {
            _teleportOutPrefab =
                AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Match3/Teleport/TeleportOutView.prefab");
        }

        public TeleportTool(GridSystem.EditorApi gridApi) :
            base(gridApi)
        {
        }

        /// <summary>
        /// Checks whether this asset supported by this InstanceCreator.
        /// </summary>
        /// <inheritdoc />
        public override bool IsSupported(Object asset)
        {
            return base.IsSupported(asset) && (asset as GameObject)?.GetComponent<Teleport>() != null;
        }

        public override void Erase(Vector3 position, ILayer layer = null)
        {
            Teleport teleport = FindComponentInCell<Teleport>(position);
            TeleportViewSelector presenter;

            if (teleport != null)
            {
                presenter = teleport.TargetCell.GetComponentInChildren<TeleportViewSelector>();
            }
            else
            {
                presenter = FindComponentInCell<TeleportViewSelector>(position);
                teleport =
                    (FindComponentInCell<TeleportPresenter>(position) as TeleportPresenter.IEditorApi)
                    ?.Teleport as Teleport;
                presenter = teleport != null ? presenter : null;
            }

            if (teleport != null)
            {
                layer?.Remove(teleport.gameObject);
                Undo.DestroyObjectImmediate(teleport.gameObject);
            }

            if (presenter != null)
            {
                layer?.Remove(presenter.gameObject);
                Undo.DestroyObjectImmediate(presenter.gameObject);
            }
        }

        public override void Deactivate()
        {
            base.Deactivate();

            if (_teleport != null)
            {
                Object.DestroyImmediate(_teleport.gameObject);
                _teleport = null;
            }
        }

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            bool canCreate = false;
            if (base.CanCreate(asset, position, layer))
            {
                CellComponent cell = FindCell(position);
                if (cell != null)
                {
                    if (_teleport == null)
                    {
                        canCreate = FindComponentInCell<Teleport>(position) == null;
                    }
                    else
                    {
                        canCreate = FindComponentInCell<TeleportPresenter>(position) == null;
                    }
                }
            }

            return canCreate;
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return false;
        }

        protected override GameObject ConstructGameObject(Object asset, Vector3 position)
        {
            if (_teleport != null && (_teleport as Teleport.IEditorApi).IsSeparateOut)
            {
                asset = _teleportOutPrefab;
            }

            return base.ConstructGameObject(asset, position);
        }

        public override GameObject UpdatePreview(Object asset, Vector3 position)
        {
            if (_teleport != null)
            {
                asset = _teleportOutPrefab;
            }

            GameObject go = base.UpdatePreview(asset, position);

            CellComponent cell = FindCell(position);
            go.transform.SetParent(cell?.transform);

            return go;
        }

        /// <summary>
        /// Setups the GameObject using the asset.
        /// </summary>
        /// <inheritdoc />
        protected override void SetupGameObject(GameObject go, Object asset)
        {
            base.SetupGameObject(go, asset);

            if (_teleport == null)
            {
                _teleport = go.GetComponent<Teleport>();

                var teleportEditor = _teleport as Teleport.IEditorApi;
                if (teleportEditor.IsSeparateOut)
                {
                    go.hideFlags |= HideFlags.HideAndDontSave;
                    go.transform.SetParent(FindCell(go.transform.position).transform);
                }
                else
                {
                    CellComponent cell = FindCell(_teleport.transform.position);

                    Vector2Int gridPos = GridApi.Grid.WorldToGrid(_teleport.transform.position);
                    Vector2Int targetGridPos =
                        gridPos +
                        new Vector2Int(teleportEditor.Length + 1, teleportEditor.Length + 1) *
                        cell.Gravity.GetOffset();

                    teleportEditor.TargetCell = GridApi.Grid.GetCell(targetGridPos);

                    if (teleportEditor.TargetCell != null)
                    {
                        Undo.RegisterCreatedObjectUndo(_teleport.gameObject, "Create Teleport");
                        Undo.SetTransformParent(
                            _teleport.transform,
                            cell.transform,
                            "Set Teleport Out as a child of the Cell");
                    }
                    else
                    {
                        UnityEngine.Debug.LogError(
                            $"You can't set teleport in {gridPos}. It's end doen't connect to other cell in {targetGridPos}");
                        _teleport.gameObject.Dispose();
                    }

                    _teleport = null;
                }
            }
            else
            {
                _teleport.gameObject.hideFlags ^= HideFlags.HideAndDontSave;

                Undo.RegisterCreatedObjectUndo(_teleport.gameObject, "Create Teleport");
                Undo.RegisterCreatedObjectUndo(go, "Create Teleport");

                CellComponent cell = FindCell(go.transform.position);

                (_teleport as Teleport.IEditorApi).TargetCell = cell;

                Undo.SetTransformParent(go.transform, cell.transform, "Set Teleport Out as a child of the Cell");

                cell = FindCell(_teleport.transform.position);
                Undo.SetTransformParent(_teleport.transform, cell.transform, "Set Teleport as a child of the Cell");

                _teleport = null;
            }
        }
    }
}