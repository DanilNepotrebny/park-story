﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using DreamTeam.Match3.Conveyor;
using JetBrains.Annotations;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class ConveyorChipSlotTool : GridSystemTool
    {
        public ConveyorChipSlotTool(GridSystem.EditorApi gridApi) :
            base(gridApi)
        {
        }

        public override bool IsSupported(Object asset)
        {
            return true;
        }

        public override void Modify(Object asset, Vector3 position, [CanBeNull] ILayer layer = null)
        {
            ConveyorBlock conveyorBlock = FindComponentInCell<ConveyorBlock>(position);
            if (conveyorBlock != null)
            {
                var editorApi = (ConveyorBlock.IEditorApi)conveyorBlock;
                editorApi.HasChipSlot = !editorApi.HasChipSlot;
            }
        }

        public override void Erase(Vector3 position, ILayer layer = null)
        {
            var cell = (CellComponent.IEditorAPI)FindCell(position);
            if (cell.ChipContainer.Chip != null)
            {
                return;
            }

            ConveyorBlock conveyorBlock = FindComponentInCell<ConveyorBlock>(position);
            if (conveyorBlock != null)
            {
                var editorApi = (ConveyorBlock.IEditorApi)conveyorBlock;
                editorApi.HasChipSlot = true;
            }
        }

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            return false;
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            var cell = (CellComponent.IEditorAPI)FindCell(position);
            return FindComponentInCell<ConveyorBlock>(position) != null &&
                cell.ChipContainer.Chip == null;
        }
    }
}