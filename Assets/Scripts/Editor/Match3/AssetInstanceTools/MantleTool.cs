﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3;
using DreamTeam.Match3.Bezel;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.SpecialChips.Mantle;
using DreamTeam.Utils;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Tilemaps;
using Object = UnityEngine.Object;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class MantleTool : BaseGridBezelTool
    {
        private CellComponent _cell;
        private MantleComponent _selectedMantle;

        public MantleComponent SelectedMantle
        {
            get { return _selectedMantle; }
            private set
            {
                if (_selectedMantle == value)
                {
                    return;
                }

                _selectedMantle?.OnDeselected();
                value?.OnSelected();

                _selectedMantle = value;
            }
        }

        private static Match3Settings _settings => Match3Settings.GetOrLoad();

        public MantleTool(GridSystem.EditorApi gridApi) :
            base(gridApi, _settings.MantleBezelBrush)
        {
        }

        public override bool IsSupported(Object asset)
        {
            return base.IsSupported(asset) &&
                PrefabUtility.GetPrefabType(asset) == PrefabType.Prefab &&
                GetStubChip(asset) != null;
        }

        public override void Deactivate()
        {
            base.Deactivate();

            Validate();
            SelectedMantle = null;
            _cell = null;
            MantleSetupWindow.CloseWindow();
        }

        public override bool Apply(Object asset, Vector3 position, ILayer layer = null)
        {
            _cell = FindCell(position);
            MantleStubChipComponent stub = FindStubChip(_cell);
            SelectedMantle = stub != null ? stub.Mantle : SelectedMantle;

            return base.Apply(asset, position, layer);
        }

        public override bool ContinueApplying(Object asset, Vector3 position, ILayer layer = null)
        {
            _cell = FindCell(position);

            return base.ContinueApplying(asset, position, layer);
        }

        public override void Erase(Vector3 position, ILayer layer = null)
        {
            CellComponent cell = FindCell(position);
            MantleStubChipComponent stub = FindStubChip(cell);
            if (stub == null)
            {
                return;
            }

            Undo.IncrementCurrentGroup();
            int undoGroupIndex = Undo.GetCurrentGroup();
            {
                Assert.IsNotNull(stub.Mantle);
                SelectedMantle = stub.Mantle;

                base.Erase(position, layer); // do erase when SelectedMantle is presented

                Undo.RegisterCompleteObjectUndo(SelectedMantle.gameObject, $"Removed stub from MantleComponent");
                SelectedMantle.Remove(stub);

                PushOnTopIntoCellWithUndo(stub.Chip);

                Undo.DestroyObjectImmediate(stub.gameObject);

                if (SelectedMantle.IsEmpty())
                {
                    Undo.DestroyObjectImmediate(SelectedMantle.Presenter.BezelTilemap.gameObject);
                    Undo.DestroyObjectImmediate(SelectedMantle.gameObject);
                    SelectedMantle = null;
                }
                else
                {
                    EditorUtils.MarkPrefabInstanceAsDirty(SelectedMantle.gameObject);
                }
            }
            Undo.CollapseUndoOperations(undoGroupIndex);
        }

        public override void Modify(Object asset, Vector3 position, ILayer layer = null)
        {
            MantleStubChipComponent stub = FindStubChip(_cell);
            if (stub != null)
            {
                if (ReferenceEquals(stub.Mantle, SelectedMantle))
                {
                    return;
                }

                Erase(position, layer);
                Apply(asset, position, layer);
                return;
            }

            base.Modify(asset, position, layer);

            GameObject go = ConstructGameObject(asset, position);
            SetupGameObject(go, asset);
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return _cell != null && SelectedMantle != null;
        }

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            return base.CanCreate(asset, position, layer) &&
                _cell != null &&
                SelectedMantle == null;
        }

        protected override void SetupGameObject(GameObject stubGO, Object asset)
        {
            Undo.IncrementCurrentGroup();
            int undoGroupIndex = Undo.GetCurrentGroup();
            {
                if (SelectedMantle == null)
                {
                    MantleComponent mantle = CreateMantleObjectWithUndo();
                    EditorUtils.RecordObjectUndo(mantle.Presenter.gameObject,
                        $"Set bezel tilemap to {nameof(MantlePresenter)}");
                    mantle.Presenter.BezelTilemap = CreateTilemapObjectWithUndo();

                    SelectedMantle = mantle;

                    MantleSetupWindow.Setup(SelectedMantle, OnCompleteMantleSetup);
                }

                // must be after SelectedMantle is created. BezelTilemap is used in BaseGridBezelTool
                base.SetupGameObject(stubGO, asset);

                Undo.RegisterCreatedObjectUndo(stubGO, $"Created {nameof(MantleStubChipComponent)}");
                MantleStubChipComponent.IEditorApi stub = GetStubChip(stubGO);

                EditorUtils.RecordObjectUndo(SelectedMantle.gameObject, $"Adding {nameof(MantleStubChipComponent)}");
                SelectedMantle.Add(stub);

                EditorUtils.RecordObjectUndo(stubGO, $"Set {nameof(MantleComponent)} to {nameof(MantleStubChipComponent)}");
                stub.SetMantle(SelectedMantle);

                PushOnTopIntoCellWithUndo(stub.Chip);
            }
            Undo.CollapseUndoOperations(undoGroupIndex);

            Undo.FlushUndoRecordObjects();
            EditorUtils.MarkPrefabInstanceAsDirty(SelectedMantle.gameObject);
        }

        private void OnCompleteMantleSetup([NotNull] MantleComponent mantle)
        {
            EditorUtils.MarkPrefabInstanceAsDirty(mantle.gameObject);
        }

        protected override Tilemap GetTilemap()
        {
            return SelectedMantle?.Presenter.BezelTilemap;
        }

        private void PushOnTopIntoCellWithUndo([NotNull] ChipComponent chip)
        {
            Transform stubTransform = chip.gameObject.transform;
            CellComponent cell = FindCell(stubTransform.position);
            Undo.SetTransformParent(stubTransform, cell.transform, "Set stub chip as a child of the Cell");

            var cellContainer = cell.GetComponent<ChipContainerComponent>();
            if (cellContainer.Chip != null)
            {
                // if cell already has some chip, then we should put them into our container
                var stubContainer = chip.GetComponent<ChipContainerComponent>();
                cellContainer.SwapChipsWithUndo(stubContainer);
            }

            foreach (ChipContainerComponent containerComponent in cellContainer)
            {
                Undo.RecordObject(containerComponent, "Put stub into cell");
            }
            cellContainer.PushChip(chip);
        }

        private MantleComponent CreateMantleObjectWithUndo()
        {
            var mantleComponent = (MantleComponent)PrefabUtility.InstantiatePrefab(_settings.MantlePrefab);

            Undo.RegisterCreatedObjectUndo(mantleComponent.gameObject, $"Created {nameof(MantleComponent)}");
            Undo.SetTransformParent(mantleComponent.transform, GridApi.Grid.transform,
                $"Set {nameof(MantleComponent)} to be a child of {nameof(GridSystem)}");

            return mantleComponent;
        }

        private Tilemap CreateTilemapObjectWithUndo()
        {
            var bezelTilemap = (Tilemap)PrefabUtility.InstantiatePrefab(_settings.MantleBezelTilemapPrefab);
            Undo.RegisterCreatedObjectUndo(bezelTilemap.gameObject, $"Created {nameof(Tilemap)}");

            BezelSystem bezelSystem = GetBezelSystem();
            Undo.SetTransformParent(bezelTilemap.transform, bezelSystem.transform,
                $"Set {nameof(Tilemap)} to be a child of {nameof(BezelSystem)}");

            // after putting into grid position changes. We don't need that
            EditorUtils.RecordObjectUndo(bezelTilemap.gameObject, $"Reset position of {nameof(Tilemap)}");
            bezelTilemap.transform.localPosition = Vector3.zero;

            return bezelTilemap;
        }

        private void Validate()
        {
            if (SelectedMantle == null)
            {
                return;
            }

            int missingStubs = SelectedMantle.CalculateStubsCountToBeValidShape(GridApi.Grid);
            if (missingStubs != 0)
            {
                UnityEngine.Debug.LogAssertion($"Designer, Mantle has a wrong shape");
            }
        }

        private static MantleStubChipComponent.IEditorApi GetStubChip([NotNull] Object asset)
        {
            var assetGO = asset as GameObject;
            return assetGO != null ? assetGO.GetComponent<MantleStubChipComponent>() : null;
        }

        private static MantleStubChipComponent FindStubChip([CanBeNull] CellComponent cell)
        {
            if (cell == null)
            {
                return null;
            }

            var cellContainer = cell.GetComponent<ChipContainerComponent>();
            if (cellContainer == null || cellContainer.Chip == null)
            {
                return null;
            }

            var stub = cellContainer.Chip.GetComponent<MantleStubChipComponent>();
            return stub;
        }
    }

    public class MantleSetupWindow : EditorWindow
    {
        private MantleComponent _mantle;
        private Action<MantleComponent> _onComplete;
        private bool _isEnabled;

        public static void Setup([NotNull] MantleComponent mantle, Action<MantleComponent> onComplete)
        {
            var window = GetWindow<MantleSetupWindow>();
            window._mantle = mantle;
            window._onComplete = onComplete;

            window.Show();
        }

        public static void CloseWindow()
        {
            var window = GetWindow<MantleSetupWindow>();
            if (window._isEnabled)
            {
                window.Close();
            }
        }

        protected void Awake()
        {
            name = "Setup Mantle";
            wantsMouseEnterLeaveWindow = true;
        }

        protected void OnGUI()
        {
            int chipsAmount = EditorGUILayout.IntField("Chips amount:", _mantle.ChipsConsumeAmount);
            _mantle.SetChipsConsumeAmount(chipsAmount);

            var chipToConsume = (ChipTag)EditorGUILayout.ObjectField("Consume chip tag:", _mantle.ChipToConsume, typeof(ChipTag), false);
            _mantle.SetChipToConsume(chipToConsume);

            var consumeDirection = (Direction)EditorGUILayout.EnumPopup("Consume direction:", _mantle.ConsumeDirection);
            _mantle.SetConsumeDirection(consumeDirection);
        }

        protected void OnEnable()
        {
            _isEnabled = true;
        }

        protected void OnDisable()
        {
            _isEnabled = false;
            if (_mantle == null)
            {
                return;
            }

            _onComplete?.Invoke(_mantle);
            _mantle = null;
        }
    }
}
