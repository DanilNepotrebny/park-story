﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3;
using DreamTeam.Match3.Bezel;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;
using Object = UnityEngine.Object;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public abstract class BaseGridBezelTool : GridSystemTool, IDisposable
    {
        private readonly BezelBrush _bezelBrush;
        private BezelSystem _bezelSystem;

        public BaseGridBezelTool(GridSystem.EditorApi gridApi, BezelBrush bezelBrush) :
            base(gridApi)
        {
            _bezelBrush = bezelBrush;

            EditorApplication.hierarchyChanged += PickFieldBezelSystem;

            PickFieldBezelSystem();
        }

        public override void Erase(Vector3 position, ILayer layer = null)
        {
            Vector2Int gridPos = GridApi.Grid.WorldToGrid(position);
            UpdateFieldBezel(gridPos, false);
        }

        public void UpdateFieldBezel(Vector2Int position, bool isCellAdded)
        {
            if (isCellAdded)
            {
                Paint(position);
            }
            else
            {
                Erase(position);
            }
        }

        /// <summary>
        /// Setups the GameObject using the asset.
        /// </summary>
        /// <inheritdoc />
        protected override void SetupGameObject(GameObject go, Object asset)
        {
            base.SetupGameObject(go, asset);

            Vector2Int gridPos = GridApi.Grid.WorldToGrid(go.transform.position);
            UpdateFieldBezel(gridPos, true);
        }

        // override in child to provide custom Tilemap
        protected virtual Tilemap GetTilemap()
        {
            return null;
        }

        protected BezelSystem GetBezelSystem()
        {
            return _bezelSystem;
        }

        private void Paint(Vector2Int position)
        {
            Tilemap tilemap = GetTilemap();
            if (tilemap != null)
            {
                _bezelSystem.Paint(_bezelBrush, position, tilemap);
            }
            else
            {
                _bezelSystem.Paint(_bezelBrush, position);
            }
        }

        private void Erase(Vector2Int position)
        {
            Tilemap tilemap = GetTilemap();
            if (tilemap != null)
            {
                _bezelSystem.Erase(_bezelBrush, position, tilemap);
            }
            else
            {
                _bezelSystem.Erase(_bezelBrush, position);
            }
        }

        private void PickFieldBezelSystem()
        {
            _bezelSystem = Object.FindObjectOfType<BezelSystem>();
        }

        void IDisposable.Dispose()
        {
            EditorApplication.hierarchyChanged -= PickFieldBezelSystem;

            _bezelSystem = null;
        }
    }
}