﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Editor.AssetInstanceTools;
using DreamTeam.Match3;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public abstract class GridSystemTool : GameObjectTool
    {
        protected GridSystem.EditorApi GridApi { get; }

        public GridSystemTool(GridSystem.EditorApi gridApi)
        {
            GridApi = gridApi;
        }

        public override GameObject UpdatePreview(Object asset, Vector3 position)
        {
            GameObject preview = base.UpdatePreview(asset, position);
            preview.transform.position = AlignToGrid(position);
            return preview;
        }

        public override bool Apply(Object asset, Vector3 position, ILayer layer = null)
        {
            return base.Apply(asset, AlignToGrid(position), layer);
        }

        public override bool ContinueApplying(Object asset, Vector3 position, ILayer layer = null)
        {
            return base.ContinueApplying(asset, AlignToGrid(position), layer);
        }

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            if (!base.CanCreate(asset, position, layer))
            {
                return false;
            }

            Vector2Int gridPos = GridApi.Grid.WorldToGrid(position);
            return GridApi.Grid.IsInBounds(gridPos);
        }

        protected CellComponent FindCell(Vector3 position)
        {
            Vector2Int gridPos = GridApi.Grid.WorldToGrid(position);
            return GridApi.Grid.GetCell(gridPos);
        }

        protected T FindComponentInCell<T>(Vector3 position) where T : Component
        {
            CellComponent cell = FindCell(position);
            return cell != null ? cell.GetComponentInChildren<T>() : null;
        }

        protected virtual Vector3 AlignToGrid(Vector3 position)
        {
            Vector2Int gridPos = GridApi.Grid.WorldToGrid(position);
            Vector3 gridWorldPos = GridApi.Grid.GridToWorld(gridPos);
            return gridWorldPos;
        }
    }
}