﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class DisallowMoveGenerationTool : BaseCellTool
    {
        public DisallowMoveGenerationTool(GridSystem.EditorApi gridApi) :
            base(gridApi)
        {
        }

        protected override string UndoMessageForCreate => "Remove applied disallow move generation tool from cell";

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return FindCell(position) != null;
        }

        protected override void Modify(CellComponent cell)
        {
            (cell as CellComponent.IEditorAPI).SetMoveGenerationEnabled(false);
        }

        protected override void Erase(CellComponent cell)
        {
            (cell as CellComponent.IEditorAPI).SetMoveGenerationEnabled(true);
        }
    }
}
