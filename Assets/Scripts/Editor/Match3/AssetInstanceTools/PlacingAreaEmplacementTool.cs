﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Match3;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Object = UnityEngine.Object;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public abstract class PlacingAreaEmplacementTool : GridSystemTool
    {
        private readonly bool _placeOnArea;
        private IPlacingAreaEditorApi _placingArea;

        public PlacingAreaEmplacementTool(GridSystem.EditorApi gridApi, bool placeOnArea) :
            base(gridApi)
        {
            _placeOnArea = placeOnArea;
        }

        public override bool IsSupported(Object asset)
        {
            return base.IsSupported(asset) && GetPlaceableComponent(asset) != null;
        }

        public override void Deactivate()
        {
            base.Deactivate();

            _placingArea = null;
        }

        protected void ErasePlaceable(IPlaceable placeable, Vector3 position)
        {
            _placingArea = FindPlacingArea(placeable.GetElementType(), position);

            if (_placingArea != null && _placeOnArea)
            {
                EditorUtils.RecordObjectUndo(_placingArea.gameObject, $"Removing placeable from Placing Area");
                _placingArea.Remove(placeable);
                EditorUtils.MarkPrefabInstanceAsDirty(_placingArea.gameObject);
            }
        }

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            if (!base.CanCreate(asset, position, layer))
            {
                return false;
            }

            Vector2Int pointerGridPos = GridApi.Grid.WorldToGrid(position);
            IPlaceable placeable = GetPlaceableComponent(asset);
            Type elementType = placeable.GetElementType();
            var areasCounter = new Dictionary<IPlacingAreaEditorApi, int>();

            foreach (Vector2Int gridPos in placeable.Bounds.allPositionsWithin)
            {
                HashSet<IPlacingAreaEditorApi> foundAreas = PlacingAreaTool.FindAreas(gridPos + pointerGridPos);
                foundAreas.RemoveWhere(area => area.GetElementType() != elementType);

                foreach (IPlacingAreaEditorApi area in foundAreas)
                {
                    if (!areasCounter.ContainsKey(area))
                    {
                        areasCounter[area] = 1;
                    }
                    else
                    {
                        areasCounter[area] += 1;
                    }
                }
            }

            if (areasCounter.Count == 0)
            {
                return _placeOnArea;
            }

            int sufficientAreasCount = areasCounter.Count(pair => pair.Value == placeable.Square);
            if (areasCounter.Count == sufficientAreasCount)
            {
                if (sufficientAreasCount > 1)
                {
                    UnityEngine.Debug.LogWarning($"Too many sufficient aries to place. Choosing the very first found");
                }

                _placingArea = areasCounter.First().Key;
                return true;
            }

            UnityEngine.Debug.LogWarning($"Can't place '{placeable.GetElementType().Name}' on the edge of Placing Area");
            return false;
        }

        protected override void SetupGameObject(GameObject go, Object asset)
        {
            base.SetupGameObject(go, asset);

            IPlaceable placeable = GetPlaceableComponent(go);
            _placingArea = FindPlacingArea(placeable.GetElementType(), go.transform.position);
            if (_placingArea != null && _placeOnArea)
            {
                EditorUtils.RecordObjectUndo(_placingArea.gameObject, $"Adding put placeable onto Placing Area");
                _placingArea.Add(placeable);
                EditorUtils.MarkPrefabInstanceAsDirty(_placingArea.gameObject);
            }
        }

        private IPlacingAreaEditorApi FindPlacingArea(Type areaType, Vector3 position)
        {
            Vector2Int gridPos = GridApi.Grid.WorldToGrid(position);
            HashSet<IPlacingAreaEditorApi> foundAreas = PlacingAreaTool.FindAreas(gridPos);
            foundAreas.RemoveWhere(a => a.GetElementType() != areaType);
            Assert.IsTrue(foundAreas.Count <= 1);
            return foundAreas.FirstOrDefault();
        }

        protected static IPlaceable GetPlaceableComponent(Object @object)
        {
            return (@object as GameObject)?.GetComponent<IPlaceable>();
        }
    }
}
