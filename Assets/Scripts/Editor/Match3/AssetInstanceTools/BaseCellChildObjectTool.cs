﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public abstract class BaseCellChildObjectTool<TComponent> : GridSystemTool
        where TComponent : Component
    {
        protected BaseCellChildObjectTool(GridSystem.EditorApi gridApi) :
            base(gridApi)
        {
        }

        public override bool IsSupported(Object asset)
        {
            return base.IsSupported(asset) && (asset as GameObject)?.GetComponent<TComponent>() != null;
        }

        public override void Erase(Vector3 position, ILayer layer = null)
        {
            TComponent c = FindComponentInCell<TComponent>(position);
            if (c != null)
            {
                layer?.Remove(c.gameObject);
                Undo.DestroyObjectImmediate(c.gameObject);
            }
        }

        public override GameObject UpdatePreview(Object asset, Vector3 position)
        {
            GameObject go = base.UpdatePreview(asset, position);

            CellComponent cell = FindCell(position);
            go.transform.SetParent(cell?.transform);

            return go;
        }

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            CellComponent cell = FindCell(position);
            return base.CanCreate(asset, position, layer) &&
                cell != null &&
                FindComponentInCell<TComponent>(position) == null;
        }

        protected override void SetupGameObject(GameObject go, Object asset)
        {
            base.SetupGameObject(go, asset);

            CellComponent cell = FindCell(go.transform.position);
            Undo.SetTransformParent(go.transform, cell.transform, $"Set {typeof(TComponent).Name} as a child of the Cell");
        }
    }
}