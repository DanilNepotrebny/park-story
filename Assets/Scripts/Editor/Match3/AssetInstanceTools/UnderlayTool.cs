﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class UnderlayTool : BaseCellChildObjectTool<Underlay>
    {
        public UnderlayTool(GridSystem.EditorApi gridApi) :
            base(gridApi)
        {
        }

        public override void Modify(Object asset, Vector3 position, ILayer layer = null)
        {
            Underlay underlay = FindComponentInCell<Underlay>(position);
            if (underlay != null)
            {
                Undo.RecordObject(underlay, "Increase underlay health");
                underlay.GetEditorApi().Health++;

                EditorSceneManager.MarkSceneDirty(underlay.gameObject.scene);
            }
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            Underlay underlay = FindComponentInCell<Underlay>(position);
            if (underlay == null)
            {
                return false;
            }

            string p1 = GetObjectAssetPath(underlay.gameObject);
            string p2 = GetObjectAssetPath(asset);
            return p1 == p2;
        }

        private static string GetObjectAssetPath(Object obj)
        {
            Object asset = obj;

            PrefabType assetPrefabType = PrefabUtility.GetPrefabType(obj);
            if (assetPrefabType == PrefabType.PrefabInstance ||
                assetPrefabType == PrefabType.ModelPrefabInstance ||
                assetPrefabType == PrefabType.DisconnectedPrefabInstance ||
                assetPrefabType == PrefabType.DisconnectedModelPrefabInstance)
            {
                asset = PrefabUtility.GetCorrespondingObjectFromSource(obj);
            }

            return AssetDatabase.GetAssetPath(asset);
        }
    }
}