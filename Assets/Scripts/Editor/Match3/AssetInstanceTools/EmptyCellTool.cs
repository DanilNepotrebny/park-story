﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class EmptyCellTool : BaseCellTool
    {
        public EmptyCellTool(GridSystem.EditorApi gridApi) :
            base(gridApi)
        {
        }

        public void OnChipCreated(CellComponent cell)
        {
            if (!CanMarkCellEmpty(cell))
            {
                Erase(cell.transform.position);
            }
        }

        /// <inheritdoc />
        protected override string UndoMessageForCreate => "Mark cell empty on start";

        /// <inheritdoc />
        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return CanMarkCellEmpty(FindCell(position));
        }

        protected override void Modify(CellComponent cell)
        {
            (cell as CellComponent.IEditorAPI).SetHasGeneratedChip(false);
        }

        protected override void Erase(CellComponent cell)
        {
            (cell as CellComponent.IEditorAPI).SetHasGeneratedChip(true);
        }

        private bool CanMarkCellEmpty(CellComponent cell)
        {
            return cell != null && cell.GetComponent<ChipContainerComponent>().CanInsertChip();
        }
    }
}