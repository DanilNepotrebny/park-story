﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using DreamTeam.Match3.SpecialChips.PopcornBucket;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class ChipWayoutTool : GridSystemTool
    {
        protected override string UndoMessageForCreate => "Add chip wayout";

        public ChipWayoutTool(GridSystem.EditorApi gridApi) :
            base(gridApi)
        {
        }

        public override bool IsSupported(Object asset)
        {
            return base.IsSupported(asset) && ((GameObject)asset).GetComponent<ChipWayout>() != null;
        }

        public override GameObject UpdatePreview(Object asset, Vector3 position)
        {
            GameObject go = base.UpdatePreview(asset, position);

            CellComponent cell = FindCell(position);
            go.transform.SetParent(cell?.transform);

            return go;
        }

        public override void Modify(Object asset, Vector3 position, ILayer layer = null)
        {
            Erase(position, layer);
        }

        public override void Erase(Vector3 position, ILayer layer = null)
        {
            var wayout = FindCell(position)?.GetComponentInChildren<ChipWayout>();
            if (wayout != null)
            {
                Undo.DestroyObjectImmediate(wayout.gameObject);
            }
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return FindCell(position)?.GetComponentInChildren<ChipWayout>() != null;
        }

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            if (!base.CanCreate(asset, position, layer))
            {
                return false;
            }

            CellComponent cell = FindCell(position);
            if (cell == null)
            {
                return false;
            }

            return cell.GetComponentInChildren<ChipWayout>() == null;
        }

        protected override void SetupGameObject(GameObject go, Object asset)
        {
            base.SetupGameObject(go, asset);

            CellComponent cell = FindCell(go.transform.position);
            Undo.SetTransformParent(go.transform, cell.transform, "Insert chip wayout into cell");
        }
    }
}