﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using DreamTeam.Match3.ChipGeneration;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class GeneratorTool : BaseCellChildObjectTool<GeneratorComponent>
    {
        public GeneratorTool(GridSystem.EditorApi gridApi) :
            base(gridApi)
        {
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return false;
        }
    }
}