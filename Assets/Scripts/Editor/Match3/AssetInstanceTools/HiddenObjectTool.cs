﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Linq;
using DreamTeam.Match3;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class HiddenObjectTool : PlacingAreaEmplacementTool
    {
        public HiddenObjectTool(GridSystem.EditorApi gridApi) :
            base(gridApi, true)
        {
        }

        public override bool IsSupported(Object asset)
        {
            return base.IsSupported(asset) && (asset as GameObject)?.GetComponent<HiddenObject>() != null;
        }

        public override void Erase(Vector3 position, ILayer layer = null)
        {
            base.Erase(position, layer);

            Vector2Int gridPos = GridApi.Grid.WorldToGrid(position);
            RectInt bounds = new RectInt(gridPos, new Vector2Int(1, 1));

            HiddenObject hiddenObject = GetHiddenObject(bounds);
            if (hiddenObject != null)
            {
                ErasePlaceable(hiddenObject, position);
                layer?.Remove(hiddenObject.gameObject);
                Undo.DestroyObjectImmediate(hiddenObject.gameObject);
            }
        }

        public override GameObject UpdatePreview(Object asset, Vector3 position)
        {
            GameObject preview = base.UpdatePreview(asset, position);

            IPlaceable placeable = GetPlaceableComponent(preview);
            placeable.Place(GridApi.Grid.WorldToGrid(position));

            return preview;
        }

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            if (!base.CanCreate(asset, position, layer))
            {
                return false;
            }

            Vector2Int gridPos = GridApi.Grid.WorldToGrid(position);
            HiddenObject hiddenObject = (asset as GameObject)?.GetComponent<HiddenObject>();
            if (hiddenObject == null)
            {
                return false;
            }

            RectInt bounds = new RectInt(gridPos, hiddenObject.Bounds.size);

            for (int x = bounds.x; x < bounds.xMax; ++x)
            {
                for (int y = bounds.y; y < bounds.yMax; ++y)
                {
                    CellComponent cell = GridApi.Grid.GetCell(new Vector2Int(x, y));
                    if (cell == null)
                    {
                        return false;
                    }
                }
            }

            return GetHiddenObject(bounds) == null;
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return false;
        }

        protected override void SetupGameObject(GameObject go, Object asset)
        {
            Vector2Int gridPos = GridApi.Grid.WorldToGrid(go.transform.position);
            GetPlaceableComponent(go).Place(gridPos);

            Undo.SetTransformParent(go.transform, GridApi.Grid.transform, "Set HiddenObject transform parent");

            base.SetupGameObject(go, asset);
        }

        private static HiddenObject GetHiddenObject(RectInt bounds)
        {
            HiddenObject[] hiddenObjects = Object.FindObjectsOfType<HiddenObject>();
            return hiddenObjects.FirstOrDefault(hiddenObject => hiddenObject.Bounds.Intersects(bounds));
        }
    }
}