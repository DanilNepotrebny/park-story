﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Match3;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public abstract class BaseCellTool : GridSystemTool
    {
        public BaseCellTool(GridSystem.EditorApi gridApi) :
            base(gridApi)
        {
        }

        /// <inheritdoc />
        public override bool IsSupported(Object asset)
        {
            return true;
        }

        /// <inheritdoc />
        public override void Erase(Vector3 position, ILayer layer = null)
        {
            ChangeProperty(position, Erase);
        }

        /// <inheritdoc />
        public override void Modify(Object asset, Vector3 position, ILayer layer = null)
        {
            ChangeProperty(position, Modify);
        }

        /// <inheritdoc />
        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            return false;
        }

        /// <inheritdoc />
        protected override GameObject ConstructGameObject(Object asset, Vector3 position)
        {
            var go = new GameObject();
            go.transform.position = position;
            return go;
        }

        protected abstract void Modify(CellComponent cell);
        protected abstract void Erase(CellComponent cell);

        private void ChangeProperty(Vector3 cellPos, Action<CellComponent> changer)
        {
            CellComponent cell = FindCell(cellPos);
            if (cell != null)
            {
                Undo.RecordObject(cell, UndoMessageForCreate);
                changer.Invoke(cell);

                EditorSceneManager.MarkSceneDirty(cell.gameObject.scene);
            }
        }
    }
}