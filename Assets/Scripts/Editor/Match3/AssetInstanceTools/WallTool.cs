﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using DreamTeam.Match3.Wall;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class WallTool : GridSystemTool
    {
        private const float DetectionAngle = 45f;
        private const string VerticalPostfix = "_V";
        private const string HorizontalPostfix = "_H";
        private const int PostfixLength = 2;
        private const string AssetsLocation = "Assets/Prefabs/Match3/";
        private const string AssetExtension = ".prefab";

        private Vector3 _pointerPos;

        private static WallSystem.IEditorApi WallSystem => WallSystemProvider.Get();

        public WallTool(GridSystem.EditorApi gridApi) :
            base(gridApi)
        {
        }

        public override bool IsSupported(Object asset)
        {
            return base.IsSupported(asset) && ((GameObject)asset).GetComponent<Wall>();
        }

        public override bool Apply(Object asset, Vector3 position, ILayer layer = null)
        {
            _pointerPos = position;
            return base.Apply(SelectProperAsset(asset, position), position, layer);
        }

        public override bool ContinueApplying(Object asset, Vector3 position, ILayer layer = null)
        {
            _pointerPos = position;
            return base.ContinueApplying(SelectProperAsset(asset, position), position, layer);
        }

        public override GameObject UpdatePreview(Object asset, Vector3 position)
        {
            _pointerPos = position;
            return base.UpdatePreview(SelectProperAsset(asset, position), position);
        }

        public override void Erase(Vector3 position, ILayer layer = null)
        {
            _pointerPos = position;
            Undo.IncrementCurrentGroup();
            int undoGroupIndex = Undo.GetCurrentGroup();
            {
                Undo.RegisterCompleteObjectUndo(WallSystem.gameObject, "Remove wall from WallSystem");
                WallSystem.Position pos = CreateWallPosition(position);
                Wall removedWall = WallSystem.RemoveWall(pos);
                if (removedWall == null)
                {
                    return;
                }

                layer?.Remove(removedWall.gameObject);
                Undo.DestroyObjectImmediate(removedWall.gameObject);
            }
            Undo.CollapseUndoOperations(undoGroupIndex);
        }

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            if (!base.CanCreate(asset, position, layer))
            {
                return false;
            }

            WallSystem.Position wallPos = CreateWallPosition(_pointerPos);
            if (WallSystem.HasWall(wallPos))
            {
                return false;
            }

            CellComponent cell1 = GridApi.Grid.GetCell(wallPos.Point1);
            CellComponent cell2 = GridApi.Grid.GetCell(wallPos.Point2);
            return cell1 != null || cell2 != null;
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return false;
        }

        protected override GameObject ConstructGameObject(Object asset, Vector3 position)
        {
            GameObject constructed = base.ConstructGameObject(asset, position);
            var wall = (Wall.IEditorApi)constructed.GetComponent<Wall>();
            wall.Position = CreateWallPosition(_pointerPos);
            return constructed;
        }

        protected override void SetupGameObject(GameObject go, Object asset)
        {
            Undo.IncrementCurrentGroup();
            int undoGroupIndex = Undo.GetCurrentGroup();
            {
                Undo.RegisterCreatedObjectUndo(go, "Create wall game object");

                var wall = go.GetComponent<Wall>();
                Undo.RegisterCompleteObjectUndo(wall.gameObject, "Change wall name");
                go.name = $"{go.name}_{wall.Position.Point1.ToString()}-{wall.Position.Point2.ToString()}";

                Undo.RegisterCompleteObjectUndo(WallSystem.gameObject, "Add wall to WallSystem");
                WallSystem.AddWall(wall.Position, wall);

                Undo.SetTransformParent(wall.transform, GridApi.Grid.transform, "Set wall to be child of grid");
            }
            Undo.CollapseUndoOperations(undoGroupIndex);

            Undo.FlushUndoRecordObjects();

            EditorSceneManager.MarkAllScenesDirty();
        }

        protected override bool NeedReconstructPreview(Object asset, Vector3 position)
        {
            return true;
        }

        protected override Vector3 AlignToGrid(Vector3 position)
        {
            Vector3 gridPos = base.AlignToGrid(position);
            position.z = gridPos.z;

            Vector2 dir = (position - gridPos).normalized;
            dir = dir.CalculateNearestAxis(DetectionAngle).GetOffset();

            var wallOffset = new Vector3(dir.x * GridApi.CellWidth, dir.y * GridApi.CellHeight) * 0.5f;
            return gridPos + wallOffset;
        }

        private WallSystem.Position CreateWallPosition(Vector3 pointerPos)
        {
            Vector3 wallPos = AlignToGrid(pointerPos);
            Vector3 posShift = wallPos - pointerPos;
            Vector2Int point1Pos = GridApi.Grid.WorldToGrid(pointerPos);
            Vector2Int point2Pos = GridApi.Grid.WorldToGrid(wallPos + posShift);

            return new WallSystem.Position(point1Pos, point2Pos);
        }

        private Object SelectProperAsset(Object asset, Vector3 pointerPos)
        {
            bool isVertical = asset.name.EndsWith(VerticalPostfix);
            bool isHorizontal = asset.name.EndsWith(HorizontalPostfix);
            if (!isVertical && !isHorizontal)
            {
                UnityEngine.Debug.LogWarning(
                    $"Selected '{asset.name}' doesn't support {nameof(WallTool)} " +
                    $"naming convention. It should end with '{HorizontalPostfix}' for horizontal walls and " +
                    $"'{VerticalPostfix}' for vertical walls");
                return asset;
            }

            WallSystem.Position wallPos = CreateWallPosition(pointerPos);
            string assetBaseName = asset.name.Substring(0, asset.name.Length - PostfixLength);
            Direction dir = (wallPos.Point1 - wallPos.Point2).GetDirection().Rotate90CW();

            string dirPostfix = string.Empty;
            if (dir == Direction.Up || dir == Direction.Down)
            {
                dirPostfix = VerticalPostfix;
            }
            else if (dir == Direction.Left || dir == Direction.Right)
            {
                dirPostfix = HorizontalPostfix;
            }

            // TODO: Use AssetDatabase.GetAssetPath. This will allow to remove hardcoded walls path
            string assetPath = $"{AssetsLocation}{assetBaseName}{dirPostfix}{AssetExtension}";
            asset = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);

            return asset;
        }
    }
}