﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Editor.AssetInstanceTools;
using DreamTeam.Match3;
using DreamTeam.Utils;
using JetBrains.Annotations;
using ModestTree;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class PlacingAreaTool : GridSystemTool
    {
        private IPlacingAreaEditorApi _selectedArea;
        private Vector2Int _cellGridPos;
        private CellComponent _cell;
        private readonly ToolPalette _toolPalette;
        private bool _isContinueApplying;

        public IPlacingAreaEditorApi SelectedArea
        {
            get { return _selectedArea; }
            private set
            {
                if (_selectedArea == value)
                {
                    return;
                }

                _selectedArea?.OnDeselected();
                value?.OnSelected();

                _selectedArea = value;
            }
        }

        private Match3Settings _settings => Match3Settings.GetOrLoad();

        public PlacingAreaTool(GridSystem.EditorApi gridApi, ToolPalette toolPalette) :
            base(gridApi)
        {
            _toolPalette = toolPalette;
        }

        public static HashSet<IPlacingAreaEditorApi> FindAreas(Vector2Int pos)
        {
            IEnumerable<IPlacingAreaEditorApi> foundAreas = Object.FindObjectsOfType<BasePlacingArea>()
                ?.Cast<IPlacingAreaEditorApi>()
                .Where(area => area.Contains(pos));

            return foundAreas?.ToHashSet() ?? new HashSet<IPlacingAreaEditorApi>();
        }

        public override bool IsSupported(Object asset)
        {
            return base.IsSupported(asset) &&
                PrefabUtility.GetPrefabType(asset) == PrefabType.Prefab &&
                GetAreaComponent(asset) != null;
        }

        public override void Deactivate()
        {
            base.Deactivate();

            _toolPalette.DeactivateTool();
            SelectedArea = null;
            _cell = null;
        }

        public override bool Apply(Object asset, Vector3 position, ILayer layer = null)
        {
            _isContinueApplying = false;
            _cell = FindCell(position);
            _cellGridPos = GridApi.Grid.WorldToGrid(position);
            SelectedArea = FindArea(_cellGridPos, asset) ?? SelectedArea;

            return base.Apply(asset, position, layer);
        }

        public override bool ContinueApplying(Object asset, Vector3 position, ILayer layer = null)
        {
            _isContinueApplying = true;
            _cell = FindCell(position);
            _cellGridPos = GridApi.Grid.WorldToGrid(position);

            return base.ContinueApplying(asset, position, layer);
        }

        public override void Erase(Vector3 position, ILayer layer = null)
        {
            base.Erase(position, layer);

            CellComponent cell = FindCell(position);
            if (cell == null)
            {
                return;
            }

            Object asset = null;
            Undo.IncrementCurrentGroup();
            int undoGroupIndex = Undo.GetCurrentGroup();
            {
                Vector2Int gridPos = GridApi.Grid.GetGridPosition(cell);
                IEnumerable<IPlacingAreaEditorApi> areas = FindAreas(gridPos);
                if (areas != null)
                {
                    SelectedArea = areas.FirstOrDefault();
                    if (SelectedArea != null)
                    {
                        Undo.RegisterCompleteObjectUndo(SelectedArea.gameObject, $"Removed cell from Placing Area");
                        SelectedArea.Shrink(gridPos);
                        asset = PrefabUtility.GetCorrespondingObjectFromSource(SelectedArea.gameObject);

                        if (SelectedArea.Square == 0)
                        {
                            Undo.DestroyObjectImmediate(SelectedArea.gameObject);
                        }
                        else
                        {
                            EditorUtils.MarkPrefabInstanceAsDirty(SelectedArea.gameObject);
                        }
                    }
                }
            }
            Undo.CollapseUndoOperations(undoGroupIndex);

            if (asset != null)
            {
                OnAreaShrank(asset, position);
            }
        }

        public override void Modify(Object asset, Vector3 position, ILayer layer = null)
        {
            if (SelectedArea.Contains(_cellGridPos))
            {
                return; // such modification is just area selection to modify
            }

            base.Modify(asset, position, layer);

            EditorUtils.RecordObjectUndo(SelectedArea.gameObject, "Expand Placing Area");
            SelectedArea.Expand(_cellGridPos);

            EditorUtils.MarkPrefabInstanceAsDirty(SelectedArea.gameObject);

            OnAreaExpanded(asset, position);
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return _cell != null && SelectedArea != null;
        }

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            return base.CanCreate(asset, position, layer) &&
                _cell != null &&
                SelectedArea == null;
        }

        protected override void SetupGameObject(GameObject go, Object asset)
        {
            base.SetupGameObject(go, asset);

            Undo.IncrementCurrentGroup();
            int undoGroupIndex = Undo.GetCurrentGroup();
            {
                Undo.RegisterCreatedObjectUndo(go, $"Created '{GetAreaComponent(asset).GetType()}' object");

                EditorUtils.RecordObjectUndo(go, $"Expanding placing area");
                SelectedArea = GetAreaComponent(go);
                SelectedArea.Expand(_cellGridPos);

                Undo.SetTransformParent(go.transform, GridApi.Grid.transform,
                    $"Set {GetAreaComponent(asset).GetType()} to be child of grid");
            }
            Undo.CollapseUndoOperations(undoGroupIndex);

            Undo.FlushUndoRecordObjects();
            EditorSceneManager.MarkAllScenesDirty();

            OnAreaExpanded(asset, go.transform.position);
        }

        private void OnAreaExpanded([NotNull] Object asset, Vector3 position)
        {
            ApplyLinkedAsset(asset, position);
            TryExpandOnExistingPlaceable(asset);
        }

        private void OnAreaShrank([NotNull] Object asset, Vector3 position)
        {
            EraseLinkedAsset(asset, position);
            TryShrinkOnExistingPlaceable(GridApi.Grid.WorldToGrid(position));
        }

        private void ApplyLinkedAsset([NotNull] Object asset, Vector3 position)
        {
            Object linkedAsset = _settings.LinkedAssets.FindFirstOrDefault(asset);
            if (linkedAsset == null)
            {
                return;
            }

            // if linked asset was painted before - skip
            Vector2Int gridPos = GridApi.Grid.WorldToGrid(position);
            Transform cellTransform = GridApi.Grid.GetCell(gridPos).transform;
            for (int i = 0; i < cellTransform.childCount; ++i)
            {
                GameObject child = cellTransform.GetChild(i).gameObject;
                if (linkedAsset == PrefabUtility.GetCorrespondingObjectFromSource(child))
                {
                    return;
                }
            }

            _toolPalette.ActivateTool(linkedAsset);
            if (_isContinueApplying)
            {
                _toolPalette.ContinueApplyingTool(position);
            }
            else
            {
                _toolPalette.ApplyTool(position);
            }
        }

        private void EraseLinkedAsset([NotNull] Object asset, Vector3 position)
        {
            Object linkedAsset = _settings.LinkedAssets.FindFirstOrDefault(asset);
            if (linkedAsset == null)
            {
                return;
            }

            _toolPalette.ActivateTool(linkedAsset);
            _toolPalette.CurrentTool.Erase(position);
        }

        private void TryExpandOnExistingPlaceable([NotNull] Object asset)
        {
            Transform gridTransform = GridApi.Grid.transform;
            for (int i = 0; i < gridTransform.childCount; ++i)
            {
                GameObject child = gridTransform.GetChild(i).gameObject;
                var placeable = child.GetComponent<IPlaceable>();
                if (placeable != null && SelectedArea.GetElementType() == placeable.GetElementType())
                {
                    Vector2Int intersectionPoint = Vector2Int.zero;
                    if (Intersects(SelectedArea, placeable.Bounds, ref intersectionPoint))
                    {
                        foreach (Vector2Int placeableCellPos in placeable.Bounds.allPositionsWithin)
                        {
                            if (!SelectedArea.Contains(placeableCellPos))
                            {
                                _cellGridPos = placeableCellPos;
                                var cellWorldPos = GridApi.Grid.GridToWorld(placeableCellPos);
                                _cell = FindCell(cellWorldPos);
                                Modify(asset, cellWorldPos);
                                SelectedArea.Add(placeable);
                                return;
                            }
                        }
                    }
                }
            }
        }

        private void TryShrinkOnExistingPlaceable(Vector2Int gridPos)
        {
            Transform gridTransform = GridApi.Grid.transform;
            for (int i = 0; i < gridTransform.childCount; ++i)
            {
                GameObject child = gridTransform.GetChild(i).gameObject;
                var placeable = child.GetComponent<IPlaceable>();
                if (placeable != null && SelectedArea.GetElementType() == placeable.GetElementType())
                {
                    bool containsPoint = Contains(placeable.Bounds, gridPos);
                    if (containsPoint)
                    {
                        Vector2Int intersectionPoint = Vector2Int.zero;
                        if (Intersects(SelectedArea, placeable.Bounds, ref intersectionPoint))
                        {
                            Erase(GridApi.Grid.GridToWorld(intersectionPoint));
                            if (!Intersects(SelectedArea, placeable.Bounds, ref intersectionPoint))
                            {
                                SelectedArea.Remove(placeable);
                            }

                            return;
                        }
                    }
                }
            }
        }

        private bool Intersects([NotNull] IPlacingAreaEditorApi area, RectInt bounds, ref Vector2Int intersectionPoint)
        {
            foreach (Vector2Int point in bounds.allPositionsWithin)
            {
                if (area.Contains(point))
                {
                    intersectionPoint = point;
                    return true;
                }
            }

            return false;
        }

        private bool Contains(RectInt bounds, Vector2Int pos)
        {
            foreach (Vector2Int point in bounds.allPositionsWithin)
            {
                if (point == pos)
                {
                    return true;
                }
            }

            return false;
        }

        private static IPlacingAreaEditorApi GetAreaComponent([NotNull] Object asset)
        {
            return (asset as GameObject)?.GetComponent<IPlacingAreaEditorApi>();
        }

        private static IPlacingAreaEditorApi FindArea(Vector2Int pos, Object validationAsset)
        {
            HashSet<IPlacingAreaEditorApi> foundAreas = FindAreas(pos);
            IPlacingAreaEditorApi assetArea = GetAreaComponent(validationAsset);

            Type assetAreaElementType = assetArea?.GetElementType();
            foundAreas.RemoveWhere(area => area.GetElementType() != assetAreaElementType);

            if (foundAreas.Count > 1)
            {
                UnityEngine.Debug.LogAssertion($"Found too many sufficient areas to operate with");
            }

            return foundAreas.FirstOrDefault();
        }
    }
}
