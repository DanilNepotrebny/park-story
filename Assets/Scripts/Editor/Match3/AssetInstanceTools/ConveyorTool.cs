﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using DreamTeam.Match3.Bezel;
using DreamTeam.Match3.Conveyor;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class ConveyorTool : BaseGridBezelTool
    {
        private ConveyorBlock _selBlock;

        private ConveyorBlock _selectedBlock
        {
            get { return _selBlock; }
            set
            {
                if (_selBlock != null)
                {
                    ((ConveyorBlock.IEditorApi)_selBlock).IsSelected = false;
                }

                _selBlock = value;

                if (_selBlock != null)
                {
                    ((ConveyorBlock.IEditorApi)_selBlock).IsSelected = true;
                }
            }
        }

        public ConveyorTool(GridSystem.EditorApi gridApi, BezelBrush bezelBrush) :
            base(gridApi, bezelBrush)
        {
        }

        public override bool IsSupported(Object asset)
        {
            return base.IsSupported(asset) && (asset as GameObject)?.GetComponent<ConveyorBlock>() != null;
        }

        public override void Modify(Object asset, Vector3 position, [CanBeNull] ILayer layer = null)
        {
            _selectedBlock = FindComponentInCell<ConveyorBlock>(position);
        }

        public override void Erase(Vector3 position, ILayer layer = null)
        {
            base.Erase(position, layer);

            ConveyorBlock conveyorBlock = FindComponentInCell<ConveyorBlock>(position);
            if (conveyorBlock != null)
            {
                ConveyorBlock prevBlock = ((ConveyorBlock.IEditorApi)conveyorBlock).PreviousBlock;

                ((ConveyorBlock.IEditorApi)prevBlock).NextBlock = conveyorBlock.NextBlock;

                RecalculateTransportationDirection(prevBlock.NextBlock);
                RecreateBlockViews(prevBlock);

                layer?.Remove(conveyorBlock.gameObject);
                Undo.DestroyObjectImmediate(conveyorBlock.gameObject);
            }
        }

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            CellComponent cell = FindCell(position);
            return base.CanCreate(asset, position, layer) &&
                cell != null &&
                FindComponentInCell<ConveyorBlock>(position) == null;
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return FindComponentInCell<ConveyorBlock>(position) != null;
        }

        protected override void SetupGameObject(GameObject go, Object asset)
        {
            base.SetupGameObject(go, asset);

            CellComponent cell = FindCell(go.transform.position);
            Undo.SetTransformParent(go.transform, cell.transform, "Set ConveyorBlock as a child of the Cell");

            ConveyorBlock conveyorBlock = go.GetComponent<ConveyorBlock>();

            if (_selectedBlock == null)
            {
                _selectedBlock = conveyorBlock;
            }

            Undo.RecordObject(conveyorBlock, "Conveyor creation");
            Undo.RecordObject(_selectedBlock, "Conveyor creation");

            ((ConveyorBlock.IEditorApi)conveyorBlock).NextBlock = _selectedBlock.NextBlock;
            ((ConveyorBlock.IEditorApi)_selectedBlock).NextBlock = conveyorBlock;

            RecalculateTransportationDirection(conveyorBlock);
            RecreateBlockViews(conveyorBlock);

            _selectedBlock = conveyorBlock;
        }

        protected override void OnApplyFailed()
        {
            _selectedBlock = null;
        }

        private void RecalculateTransportationDirection(ConveyorBlock block)
        {
            CalculateTransportationDirection(
                    ((ConveyorBlock.IEditorApi)block).PreviousBlock,
                    GridApi.Grid,
                    true
                );

            CalculateTransportationDirection(
                    block,
                    GridApi.Grid,
                    false
                );

            CalculateTransportationDirection(
                    block.NextBlock,
                    GridApi.Grid,
                    true
                );
        }

        private void RecreateBlockViews(ConveyorBlock conveyorBlock)
        {
            ((ConveyorBlock.IEditorApi)conveyorBlock).PreviousBlock.GetComponentInChildren<ConveyorBlockView>()
                .RecreateView(GridApi.Grid);
            conveyorBlock.GetComponentInChildren<ConveyorBlockView>().RecreateView(GridApi.Grid);
            conveyorBlock.NextBlock.GetComponentInChildren<ConveyorBlockView>().RecreateView(GridApi.Grid);
        }

        private static void CalculateTransportationDirection(ConveyorBlock block, GridSystem grid, bool ignoreCycle)
        {
            Direction direction;

            ConveyorBlock next = block.NextBlock;

            if (block == next)
            {
                direction = block.TransportationDirection != Direction.None ?
                    block.TransportationDirection :
                    Direction.Right;
            }
            else if (!ConveyorBlock.HasDirectTransition(block, next, grid) ||
                !ignoreCycle &&
                block == next.NextBlock)
            {
                ConveyorBlock prevBlock = ((ConveyorBlock.IEditorApi)block).PreviousBlock;
                if (!ConveyorBlock.HasDirectTransition(prevBlock, block, grid))
                {
                    direction = block.TransportationDirection != Direction.None ?
                        block.TransportationDirection :
                        Direction.Right;
                }
                else
                {
                    direction = prevBlock.TransportationDirection;
                }
            }
            else
            {
                direction = ConveyorBlock.GetDirectionTo(block, next, grid);
            }

            ((ConveyorBlock.IEditorApi)block).TransportationDirection = direction;
        }
    }
}