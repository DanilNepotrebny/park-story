﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class GravityTool : BaseCellTool
    {
        private static bool _drawDownGravityGizmo;

        public GravityTool(GridSystem.EditorApi gridApi) :
            base(gridApi)
        {
        }

        public override void Activate()
        {
            base.Activate();

            _drawDownGravityGizmo = true;
        }

        public override void Deactivate()
        {
            base.Deactivate();

            _drawDownGravityGizmo = false;
        }

        protected override string UndoMessageForCreate => "Change gravity";

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return FindCell(position) != null;
        }

        protected override void Modify(CellComponent cell)
        {
            var editorApi = cell as CellComponent.IEditorAPI;

            Direction newGravity;

            if (IsInContinuousMode)
            {
                Vector2Int lastPosition = GridApi.Grid.WorldToGrid(LastPosition);
                Vector2Int currentPosition = GridApi.Grid.WorldToGrid(cell.transform.position);

                Vector2Int direction = currentPosition - lastPosition;
                if (direction.magnitude > 1)
                {
                    // Skip diagonal or not normalized directions.
                    return;
                }

                newGravity = direction.GetDirection();

                // Also update previous cell.
                CellComponent lastCell = GridApi.Grid.GetCell(lastPosition);
                (lastCell as CellComponent.IEditorAPI).Gravity = newGravity;
            }
            else
            {
                newGravity = editorApi.Gravity.Rotate90CW();
            }

            editorApi.Gravity = newGravity;
        }

        protected override void Erase(CellComponent cell)
        {
            var editorApi = cell as CellComponent.IEditorAPI;
            editorApi.Gravity = Direction.Down;
        }

        [DrawGizmo(GizmoType.InSelectionHierarchy | GizmoType.NotInSelectionHierarchy)]
        private static void DrawHandles(CellComponent cell, GizmoType gizmoType)
        {
            if (cell.Gravity == Direction.Down && !_drawDownGravityGizmo)
            {
                return;
            }

            Gizmos.DrawIcon(cell.transform.position, $"Gravity{cell.Gravity}.png");
        }
    }
}