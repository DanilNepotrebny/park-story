﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Linq;
using DreamTeam.Match3;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class ChipTool : GridSystemTool
    {
        private readonly EmptyCellTool _emptyCellTool;
        private readonly Type[] _skipChipContainersOnErasing;

        public ChipTool(GridSystem.EditorApi gridApi,
            EmptyCellTool emptyCellTool,
            Type[] skipChipContainersOnErasing = null) :
            base(gridApi)
        {
            _emptyCellTool = emptyCellTool;
            _skipChipContainersOnErasing = skipChipContainersOnErasing;
        }

        /// <summary>
        /// Checks whether this asset supported by this InstanceCreator.
        /// </summary>
        /// <inheritdoc />
        public override bool IsSupported(Object asset)
        {
            return base.IsSupported(asset) && ((GameObject)asset).GetComponent<ChipComponent>() != null;
        }

        public override void Erase(Vector3 position, ILayer layer = null)
        {
            CellComponent cell = FindCell(position);
            if (cell == null)
            {
                return;
            }

            var container = cell.GetComponent<ChipContainerComponent>();
            if (container == null)
            {
                return;
            }

            if (container.Chip != null && _skipChipContainersOnErasing != null)
            {
                if (_skipChipContainersOnErasing.
                    Select(type => container.Chip.GetComponent(type)).
                    Any(foundComponent => foundComponent != null))
                {
                    container = container.Chip.GetComponent<ChipContainerComponent>();
                    if (container == null)
                    {
                        return;
                    }
                }
            }

            Undo.RecordObject(container, "Сhip Removing");

            ChipComponent poppedChip = container.PopChip();
            if (poppedChip != null)
            {
                layer?.Remove(poppedChip.gameObject);
                Undo.DestroyObjectImmediate(poppedChip.gameObject);
            }
        }

        protected override string UndoMessageForCreate => "Chip Creation";

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            if (!base.CanCreate(asset, position, layer))
            {
                return false;
            }

            CellComponent cell = FindCell(position);
            ChipContainerComponent container = cell?.GetComponent<ChipContainerComponent>();

            return container?.CanInsertChip() ?? false;
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            ChipComponent topChip = FindTopChip(position);
            if (topChip == null)
            {
                return false;
            }

            string p1 = GetObjectAssetPath(topChip.gameObject);
            string p2 = GetObjectAssetPath(asset);
            return p1 == p2;
        }

        private static string GetObjectAssetPath(Object obj)
        {
            Object asset = obj;

            PrefabType assetPrefabType = PrefabUtility.GetPrefabType(obj);
            if (assetPrefabType == PrefabType.PrefabInstance ||
                assetPrefabType == PrefabType.ModelPrefabInstance ||
                assetPrefabType == PrefabType.DisconnectedPrefabInstance ||
                assetPrefabType == PrefabType.DisconnectedModelPrefabInstance)
            {
                asset = PrefabUtility.GetCorrespondingObjectFromSource(obj);
            }

            return AssetDatabase.GetAssetPath(asset);
        }

        public override void Modify(Object asset, Vector3 position, ILayer layer = null)
        {
            ChipComponent topChip = FindTopChip(position);
            if (topChip != null)
            {
                Undo.RecordObject(topChip, "Increase chip health");
                topChip.HealthController.GetEditorApi().MaxHealth += 1;

                EditorSceneManager.MarkSceneDirty(topChip.gameObject.scene);
            }
        }

        /// <summary>
        /// Setups the GameObject using the asset.
        /// </summary>
        /// <inheritdoc />
        protected override void SetupGameObject(GameObject go, Object asset)
        {
            base.SetupGameObject(go, asset);

            ChipComponent chip = go.GetComponent<ChipComponent>();
            CellComponent cell = GridApi.Grid.GetCell(GridApi.Grid.WorldToGrid(go.transform.position));
            if (cell != null)
            {
                ChipContainerComponent container = ((CellComponent.IEditorAPI)cell).ChipContainer;
                foreach (ChipContainerComponent c in container)
                {
                    Undo.RecordObject(c, UndoMessageForCreate);
                }

                container.PushChip(chip);
            }

            _emptyCellTool.OnChipCreated(cell);
        }

        private ChipComponent FindTopChip(Vector3 position)
        {
            CellComponent cell = FindCell(position);
            ChipContainerComponent cellContainer = cell?.GetComponent<ChipContainerComponent>();

            return cellContainer?.GetTopChip();
        }
    }
}