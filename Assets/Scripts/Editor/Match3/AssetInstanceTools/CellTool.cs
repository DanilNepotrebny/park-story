﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3;
using DreamTeam.Match3.Bezel;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetInstanceTools
{
    public class CellTool : BaseGridBezelTool
    {
        protected override string UndoMessageForCreate => "Cell Creation";

        public CellTool(GridSystem.EditorApi gridApi, BezelBrush bezelBrush) :
            base(gridApi, bezelBrush)
        {
        }

        /// <summary>
        /// Checks whether this asset supported by this InstanceCreator.
        /// </summary>
        /// <inheritdoc />
        public override bool IsSupported(Object asset)
        {
            return base.IsSupported(asset) && ((GameObject)asset).GetComponent<CellComponent>() != null;
        }

        public override void Erase(Vector3 position, ILayer layer = null)
        {
            base.Erase(position, layer);

            Vector2Int gridPos = GridApi.Grid.WorldToGrid(position);
            CellComponent cell = GridApi.Grid.GetCell(gridPos);
            if (cell == null)
            {
                return;
            }

            Undo.RecordObject(GridApi.Grid, "Cell Removing");
            GridApi[gridPos.x, gridPos.y] = null;

            layer?.Remove(cell.gameObject);
            Undo.DestroyObjectImmediate(cell.gameObject);
        }

        protected override bool CanCreate(Object asset, Vector3 position, ILayer layer = null)
        {
            return base.CanCreate(asset, position, layer) &&
                FindCell(position) == null;
        }

        protected override bool CanModify(Object asset, Vector3 position, ILayer layer = null)
        {
            return false;
        }

        /// <summary>
        /// Setups the GameObject using the asset.
        /// </summary>
        /// <inheritdoc />
        protected override void SetupGameObject(GameObject go, Object asset)
        {
            base.SetupGameObject(go, asset);

            CellComponent cell = go.GetComponent<CellComponent>();
            GridApi.InsertCell(GridApi.Grid.WorldToGrid(go.transform.position), cell);
        }
    }
}