﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Linq;
using DreamTeam.Editor.AssetBrushes;
using DreamTeam.Editor.AssetInstanceTools;
using DreamTeam.Editor.GridEditor;
using DreamTeam.Editor.Match3.AssetInstanceTools;
using DreamTeam.Editor.PositionTransformers;
using DreamTeam.Match3;
using DreamTeam.Match3.SpecialChips.Mantle;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DreamTeam.Editor.Match3
{
    /// <summary>
    /// Editor window for editing match-3 grid.
    /// </summary>
    public class Match3GridEditor : BaseGridEditorWindow
    {
        private GridSystem.EditorApi _gridApi;
        private CellTool _cellTool;

        private static Match3Settings _settings => Match3Settings.GetOrLoad();

        /// <summary>
        /// Creates brushes to be used in the editor.
        /// </summary>
        protected override BaseBrush[] CreateAssetBrushes()
        {
            _cellTool = new CellTool(_gridApi, _settings.FieldBezelBrush);

            var emptyCellTool = new EmptyCellTool(_gridApi);
            var disallowBoosterTool = new DisallowBoosterTool(_gridApi);
            var disallowMoveGenerationTool = new DisallowMoveGenerationTool(_gridApi);

            var toolPalette = new ToolPalette(
                    new BaseTool[]
                    {
                        new GeneratorTool(_gridApi),
                        new MantleTool(_gridApi),
                        new ChipTool(_gridApi, emptyCellTool),
                        _cellTool,
                        new UnderlayTool(_gridApi),
                        new HiddenObjectTool(_gridApi),
                        new TeleportTool(_gridApi),
                        new WallTool(_gridApi),
                        new ConveyorTool(_gridApi, _settings.ConveyorBezelBrush),
                        new ChipWayoutTool(_gridApi),
                        new PlacingAreaTool(
                                _gridApi,
                                new ToolPalette(
                                        new BaseTool[]
                                        {
                                            new UnderlayTool(_gridApi),
                                        }
                                    )
                            ),
                    }
                );

            return new BaseBrush[]
            {
                new PaintBrush(
                        new GUIContent(
                                "Paint Grid Brush",
                                (Texture)EditorGUIUtility.Load("Icons/GridAssetBrushIcon.png"),
                                "Grid snapped paint brush."
                            ),
                        null,
                        toolPalette
                    ),
                new PaintBrush(
                        new GUIContent(
                                "Empty Cell Tool",
                                (Texture)EditorGUIUtility.Load("Icons/EmptyCellIcon.png"),
                                "Tool for marking grid cells to be empty on level start."
                            ),
                        null,
                        new ToolPalette(
                                new BaseTool[]
                                {
                                    emptyCellTool
                                }
                            )
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Erase Empty Cell Tool",
                                (Texture)EditorGUIUtility.Load("Icons/EmptyCellIcon.png"),
                                "Tool for unmarking grid cells to be empty on level start."
                            ),
                        emptyCellTool
                    ),
                new PaintBrush(
                        new GUIContent(
                                "Disallow booster generation",
                                (Texture)EditorGUIUtility.Load("Icons/BoosterDisallowIcon.png"),
                                "Tool for marking grid cells to disallow generate booster on level start."
                            ),
                        null,
                        new ToolPalette(
                                new BaseTool[]
                                {
                                    disallowBoosterTool
                                }
                            )
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Remove 'Disallow booster generation'",
                                (Texture)EditorGUIUtility.Load("Icons/BoosterAllowIcon.png"),
                                "Tool for marking grid cells to allow generate booster on level start."
                            ),
                        disallowBoosterTool
                    ),
                new PaintBrush(
                        new GUIContent(
                                "Disallow move generation",
                                (Texture)EditorGUIUtility.Load("Icons/CellDisallowMoveGeneration.png"),
                                "Tool to disallow move generation in cell"
                            ),
                        null,
                        new ToolPalette(
                                new BaseTool[]
                                {
                                    disallowMoveGenerationTool
                                }
                            )
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Remove 'Disallow move generation'",
                                (Texture)EditorGUIUtility.Load("Icons/CellAllowMoveGeneration.png"),
                                "Tool to remove disallowed move generation in cell"
                            ),
                        disallowMoveGenerationTool
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Cell Erase Brush",
                                (Texture)EditorGUIUtility.Load("Icons/EraseCellIcon.png"),
                                "Grid snapped erase brush for cells."
                            ),
                        new CellTool(_gridApi, _settings.FieldBezelBrush)
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Chip Erase Brush",
                                (Texture)EditorGUIUtility.Load("Icons/EraseCellIcon.png"),
                                "Grid snapped erase brush for chips."
                            ),
                        new ChipTool(_gridApi, emptyCellTool, new[] { typeof(MantleStubChipComponent) })
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Underlay Erase Brush",
                                (Texture)EditorGUIUtility.Load("Icons/EraseCellIcon.png"),
                                "Grid snapped erase brush for underlays."
                            ),
                        new UnderlayTool(_gridApi)
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Hidden Objects Erase Brush",
                                (Texture)EditorGUIUtility.Load("Icons/EraseCellIcon.png"),
                                "Grid snapped erase brush for hidden objects."
                            ),
                        new HiddenObjectTool(_gridApi)
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Teleport Erase Brush",
                                (Texture)EditorGUIUtility.Load("Icons/EraseCellIcon.png"),
                                "Grid snapped erase brush for teleports."
                            ),
                        new TeleportTool(_gridApi)
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Wall Erase Brush",
                                (Texture)EditorGUIUtility.Load("Icons/EraseCellIcon.png"),
                                "Grid snapped erase brush for walls."
                            ),
                        new WallTool(_gridApi)
                    ),
                new PaintBrush(
                        new GUIContent(
                                "Conveyor Chip Slot",
                                (Texture)EditorGUIUtility.Load("Icons/ConveyorChipSlot.png"),
                                "Tool for creating and removing chip slots in conveyor blocks."
                            ),
                        new GridTransformer(WorldToGrid, GridToWorld),
                        new ToolPalette(
                                new BaseTool[]
                                {
                                    new ConveyorChipSlotTool(_gridApi)
                                }
                            )
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Conveyor Erase Brush",
                                (Texture)EditorGUIUtility.Load("Icons/EraseCellIcon.png"),
                                "Grid snapped erase brush for conveyors."
                            ),
                        new ConveyorTool(_gridApi, _settings.ConveyorBezelBrush)
                    ),
                new PaintBrush(
                        new GUIContent(
                                "Gravity Brush",
                                (Texture)EditorGUIUtility.Load("Icons/GravityIcon.png"),
                                "Tool for changing gravity in cells."
                            ),
                        new GridTransformer(WorldToGrid, GridToWorld),
                        new ToolPalette(new BaseTool[] { new GravityTool(_gridApi) })
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Placing Area Erase Brush",
                                (Texture)EditorGUIUtility.Load("Icons/EraseCellIcon.png"),
                                "Grid snapped erase brush for placing areas"
                            ),
                        new PlacingAreaTool(
                                _gridApi,
                                new ToolPalette(
                                        new BaseTool[]
                                        {
                                            new UnderlayTool(_gridApi),
                                        }
                                    )
                            )
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Mantle Erase Brush",
                                (Texture)EditorGUIUtility.Load("Icons/EraseCellIcon.png"),
                                "Grid snapped erase brush for mantle chip"
                            ),
                        new MantleTool(_gridApi)
                    ),
                new EraseBrush(
                        new GUIContent(
                                "Generator Erase Brush",
                                (Texture)EditorGUIUtility.Load("Icons/EraseCellIcon.png"),
                                "Grid snapped erase brush for generator"
                            ),
                        new GeneratorTool(_gridApi)
                    ),
            };
        }

        /// <summary>
        /// Finds and cached settings in AssetDatabase
        /// </summary>
        protected override BaseSettings GetBaseSettings()
        {
            return _settings;
        }

        public override void OnEnable()
        {
            PickGridFromScene();

            base.OnEnable();

            EditorApplication.hierarchyChanged += OnHierarchyWindowChanged;
            EditorSceneManager.sceneSaving += OnSceneSaving;
        }

        private void OnSceneSaving(Scene scene, string path)
        {
            // TODO: Move this into separate class with full editor lifetime.

            if (_gridApi == null)
            {
                return;
            }

            int oldWidth = _gridApi.Width;
            int oldHeight = _gridApi.Height;

            _gridApi.OptimizeBounds();

            if (oldHeight != _gridApi.Height ||
                oldWidth != _gridApi.Width)
            {
                for (int y = 0; y < oldHeight; y++)
                {
                    for (int x = 0; x < oldWidth; x++)
                    {
                        CellComponent cell = _gridApi.Grid.GetCell(new Vector2Int(x, y));
                        _cellTool.UpdateFieldBezel(new Vector2Int(x, y), cell != null);
                    }
                }
            }
        }

        public override void OnDisable()
        {
            EditorSceneManager.sceneSaving -= OnSceneSaving;
            EditorApplication.hierarchyChanged -= OnHierarchyWindowChanged;

            base.OnDisable();
        }

        protected override void OnSettingsGUI()
        {
            EditorGUI.BeginChangeCheck();

            int newWidth = EditorGUILayout.DelayedIntField("Grid width", _gridApi.Width);
            int newHeight = EditorGUILayout.DelayedIntField("Grid height", _gridApi.Height);

            if (EditorGUI.EndChangeCheck())
            {
                RecreateGrid(newWidth, newHeight);
            }

            EditorGUI.BeginChangeCheck();

            float newCellWidth = EditorGUILayout.FloatField("Cell width", _gridApi.CellWidth);
            float newCellHeight = EditorGUILayout.FloatField("Cell height", _gridApi.CellHeight);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(_gridApi.Grid, "Changed cell dimensions");

                _gridApi.CellWidth = newCellWidth;
                _gridApi.CellHeight = newCellHeight;

                for (int y = 0; y < _gridApi.Height; y++)
                {
                    for (int x = 0; x < _gridApi.Width; x++)
                    {
                        CellComponent cell = _gridApi[x, y];
                        if (cell != null)
                        {
                            Undo.RecordObject(cell.transform, "Changed cell dimensions");
                            cell.transform.position = GridToWorld(new Vector2Int(x, y));
                        }
                    }
                }

                UpdateGridAlignment();
            }

            if (GUILayout.Button("Align To Origin"))
            {
                UpdateGridAlignment();
            }
        }

        private void RecreateGrid(int newWidth, int newHeight)
        {
            int oldWidth = _gridApi.Width;
            int oldHeight = _gridApi.Height;

            int minWidth = Mathf.Min(oldWidth, newWidth);
            int minHeight = Mathf.Min(oldHeight, newHeight);
            int destroyedHeight = Mathf.Max(oldHeight - newHeight, 0);
            int additionalHeight = Mathf.Max(newHeight - oldHeight, 0);

            var oldCells = new CellComponent[minWidth, minHeight];

            for (int y = destroyedHeight; y < oldHeight; y++)
            {
                for (int x = 0; x < minWidth; x++)
                {
                    oldCells[x, y - destroyedHeight] = _gridApi[x, y];
                }
            }

            DestroyCells(0, 0, newWidth, destroyedHeight);
            DestroyCells(newWidth, destroyedHeight, oldWidth, oldHeight);
            DestroyCells(newWidth, 0, oldWidth, destroyedHeight);

            for (int y = 0; y < oldHeight; y++)
            {
                for (int x = 0; x < oldWidth; x++)
                {
                    CellComponent cell = _gridApi[x, y];
                    if (cell != null)
                    {
                        _cellTool.UpdateFieldBezel(new Vector2Int(x, y), false);
                    }
                }
            }

            Undo.RecordObject(_gridApi.Grid, "Changed grid dimensions");
            _gridApi.Cells = new CellComponent[newWidth * newHeight];
            _gridApi.Width = newWidth;
            _gridApi.Height = newHeight;

            for (int y = 0; y < minHeight; y++)
            {
                for (int x = 0; x < minWidth; x++)
                {
                    CellComponent cell = oldCells[x, y];
                    if (cell != null)
                    {
                        Vector2Int gridPos = new Vector2Int(x, y + additionalHeight);
                        _gridApi.InsertCell(gridPos, cell);
                        _cellTool.UpdateFieldBezel(gridPos, true);
                    }
                }
            }

            // Create new cells
            CreateCells(0, 0, oldWidth, additionalHeight);
            CreateCells(oldWidth, additionalHeight, newWidth, newHeight);
            CreateCells(oldWidth, 0, newWidth, additionalHeight);

            UpdateGridAlignment();
        }

        private void UpdateGridAlignment()
        {
            GridSystem gridSystem = _gridApi.Grid;
            var gridCenter = new Vector2(
                    -(gridSystem.Width - 1) * 0.5f,
                    -(gridSystem.Height - 1) * 0.5f
                );

            Undo.RecordObject(gridSystem.transform.parent, "Align Grid");
            gridSystem.transform.parent.position =
                gridSystem.GridToWorldF(gridCenter) - gridSystem.transform.parent.position;
        }

        [MenuItem("Tools/Match3/GridEditor")]
        private static void OpenWindow()
        {
            Type inspectorType = typeof(UnityEditor.Editor).Assembly.GetTypes()
                .FirstOrDefault(t => t.Name == "InspectorWindow");
            GetWindow<Match3GridEditor>("Match3 Grid Editor", inspectorType);
        }

        private void PickGridFromScene()
        {
            _gridApi = FindObjectOfType<GridSystem>()?.GetEditorApi();

            SetError(_gridApi == null ? "This scene doesn't contain GridSystem component" : null);
        }

        protected override void CalculateSceneViewGridBounds(out Vector2Int min, out Vector2Int max)
        {
            // Workaround for grid component getting lost when exiting Play mode.
            if (_gridApi == null || _gridApi.Grid == null)
            {
                PickGridFromScene();
                ResetAssetBrushPanel();
            }

            min = new Vector2Int();
            max = new Vector2Int(_gridApi.Width - 1, _gridApi.Height - 1);
        }

        private void OnHierarchyWindowChanged()
        {
            GridSystem oldGrid = _gridApi?.Grid;

            PickGridFromScene();

            if (_gridApi != null && (oldGrid == null || oldGrid != _gridApi.Grid))
            {
                ResetAssetBrushPanel();
            }

            Repaint();
        }

        private void CreateCells(int x1, int y1, int x2, int y2)
        {
            for (int y = y1; y < y2; y++)
            {
                for (int x = x1; x < x2; x++)
                {
                    _cellTool.Apply(_settings.CellPrefab, GridToWorld(new Vector2Int(x, y)));
                }
            }
        }

        private void DestroyCells(int x1, int y1, int x2, int y2)
        {
            if (x2 <= x1 || y2 <= y1)
            {
                return;
            }

            for (int y = y1; y < y2; y++)
            {
                for (int x = x1; x < x2; x++)
                {
                    _cellTool.Erase(GridToWorld(new Vector2Int(x, y)));
                }
            }
        }

        protected override Vector2 WorldToGridF(Vector3 pos)
        {
            return _gridApi.Grid.WorldToGridF(pos);
        }

        protected override Vector3 GridToWorldF(Vector2 pos)
        {
            return _gridApi.Grid.GridToWorldF(pos);
        }

        protected Vector2Int WorldToGrid(Vector3 pos)
        {
            return _gridApi.Grid.WorldToGrid(pos);
        }

        protected Vector3 GridToWorld(Vector2Int pos)
        {
            return _gridApi.Grid.GridToWorld(pos);
        }
    }
}