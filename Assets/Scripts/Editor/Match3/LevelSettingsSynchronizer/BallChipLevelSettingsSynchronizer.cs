﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Runtime.CompilerServices;
using UnityEditor;

namespace DreamTeam.Editor.Match3.LevelSettingsSynchronizer
{
    [InitializeOnLoad]
    public class BallChipLevelSettingsSynchronizer :
        BaseChipCleanupLevelSettingsSynchronizer<BallChipLevelSettingsSynchronizer>
    {
        static BallChipLevelSettingsSynchronizer()
        {
            GetChipTag = () => Match3Settings.GetOrLoad().ChipsData.BallChip.ChipTag;
            GetHelperPrefab = () => Match3Settings.GetOrLoad().ChipsData.BallChip.Helper;

            RuntimeHelpers.RunClassConstructor(
                    typeof(BaseChipCleanupLevelSettingsSynchronizer<BallChipLevelSettingsSynchronizer>).TypeHandle
                );
        }
    }
}