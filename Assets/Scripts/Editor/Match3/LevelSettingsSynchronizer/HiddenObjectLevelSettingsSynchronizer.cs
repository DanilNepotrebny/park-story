﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Linq;
using System.Runtime.CompilerServices;
using DreamTeam.Match3;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using JetBrains.Annotations;
using UnityEditor;

namespace DreamTeam.Editor.Match3.LevelSettingsSynchronizer
{
    [InitializeOnLoad]
    public class HiddenObjectLevelSettingsSynchronizer :
        BaseLevelSettingsSynchronizer<HiddenObjectRequirement, HiddenObjectLevelSettingsSynchronizer>
    {
        static HiddenObjectLevelSettingsSynchronizer()
        {
            FindRequirement = FindHiddenObjectRequirement;
            GetRequirementCount = GetHiddenObjectsCountOnScene;

            RuntimeHelpers.RunClassConstructor(
                    typeof(BaseLevelSettingsSynchronizer<HiddenObjectRequirement, HiddenObjectLevelSettingsSynchronizer>).TypeHandle
                );
        }

        private static Requirement FindHiddenObjectRequirement([NotNull] LevelSettings settings)
        {
            return settings.Goals.FirstOrDefault(r => r is HiddenObjectRequirement);
        }

        private static int GetHiddenObjectsCountOnScene(string scenePath)
        {
            HiddenObject[] hiddenObjects = FindObjectsOfType<HiddenObject>();
            int result = hiddenObjects.Count(ho => ho.gameObject.scene.path.Equals(scenePath));
            return result;
        }
    }
}