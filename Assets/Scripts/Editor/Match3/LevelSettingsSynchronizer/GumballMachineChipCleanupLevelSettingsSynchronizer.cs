﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using DreamTeam.Match3;
using UnityEditor;

namespace DreamTeam.Editor.Match3.LevelSettingsSynchronizer
{
    [InitializeOnLoad]
    public class GumballMachineChipCleanupLevelSettingsSynchronizer :
        BaseChipCleanupLevelSettingsSynchronizer<GumballMachineChipCleanupLevelSettingsSynchronizer>
    {
        static GumballMachineChipCleanupLevelSettingsSynchronizer()
        {
            GetChipTag = () => Match3Settings.GetOrLoad().ChipsData.GumballChip.ChipTag;
            GetHelperPrefab = () => Match3Settings.GetOrLoad().ChipsData.GumballChip.Helper;

            RuntimeHelpers.RunClassConstructor(
                    typeof(BaseChipCleanupLevelSettingsSynchronizer<GumballMachineChipCleanupLevelSettingsSynchronizer>).TypeHandle
                );

            // must be after base ctor call to overwrite delegate
            GetRequirementCount = GetGumballsRequirementCount;
        }

        private static int GetGumballsRequirementCount(string scenePath)
        {
            List<ChipComponent> chips = FindObjectsOfType<ChipComponent>().ToList();
            chips.RemoveAll(
                    chip =>
                        PrefabUtility.GetPrefabType(chip.gameObject) == PrefabType.Prefab ||
                        !chip.gameObject.scene.path.Equals(scenePath) ||
                        !chip.Tags.Has(GetChipTag(), false)
                );

            return chips.Count;
        }
    }
}