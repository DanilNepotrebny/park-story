﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using DreamTeam.Match3;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.SpecialChips.GumballMachine;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine.Assertions;

namespace DreamTeam.Editor.Match3.LevelSettingsSynchronizer
{
    [InitializeOnLoad]
    public class GumballConsumptionLevelSettingsSynchronizer :
        BaseLevelSettingsSynchronizer<GumballConsumptionRequirement, GumballConsumptionLevelSettingsSynchronizer>
    {
        private static ChipTag _gumballMachineChipTag => Match3Settings.GetOrLoad().ChipsData.GumballChip.ChipTag;

        static GumballConsumptionLevelSettingsSynchronizer()
        {
            FindRequirement = FindGumballConsumptionGoal;
            GetRequirementCount = GetGumballsCountOnScene;
            IsSynchronizationApplied = IsGumballConsumptionGoalExist;
            OnRequirementAdded = OnChipCleanupRequirementAdded;

            RuntimeHelpers.RunClassConstructor(
                    typeof(BaseLevelSettingsSynchronizer<
                        GumballConsumptionRequirement,
                        GumballConsumptionLevelSettingsSynchronizer>).TypeHandle
                );
        }

        private static Requirement FindGumballConsumptionGoal(LevelSettings settings)
        {
            return settings.Goals.OfType<GumballConsumptionRequirement>().FirstOrDefault();
        }

        private static bool IsGumballConsumptionGoalExist([NotNull] LevelSettings settings)
        {
            return settings
                .Goals
                .OfType<GumballConsumptionRequirement>()
                .FirstOrDefault(r => r.SynchronizeGumballsCount) != null;
        }

        private static int GetGumballsCountOnScene(string scenePath)
        {
            List<ChipComponent> chips = FindObjectsOfType<ChipComponent>().ToList();
            chips.RemoveAll(
                    chip =>
                        !chip.Tags.Has(_gumballMachineChipTag, false) ||
                        !chip.gameObject.scene.path.Equals(scenePath) ||
                        PrefabUtility.GetPrefabType(chip.gameObject) == PrefabType.Prefab
                );

            Assert.IsTrue(chips.Count > 0, $"Didn't find any '{_gumballMachineChipTag.name}' chip on grid");

            int gumballsAmount = chips.Sum(chip => chip.GetComponent<GumballMachineComponent>().InitialGumballsCount);
            return gumballsAmount;
        }

        private static void OnChipCleanupRequirementAdded([NotNull] GumballConsumptionRequirement requirement)
        {
            requirement.SetGumballMachineChipTagInEditor(_gumballMachineChipTag);
        }
    }
}