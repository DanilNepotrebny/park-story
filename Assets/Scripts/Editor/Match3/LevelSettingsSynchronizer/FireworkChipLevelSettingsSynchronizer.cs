﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Runtime.CompilerServices;
using UnityEditor;

namespace DreamTeam.Editor.Match3.LevelSettingsSynchronizer
{
    [InitializeOnLoad]
    public class FireworkChipLevelSettingsSynchronizer :
        BaseChipCleanupLevelSettingsSynchronizer<FireworkChipLevelSettingsSynchronizer>
    {
        static FireworkChipLevelSettingsSynchronizer()
        {
            GetChipTag = () => Match3Settings.GetOrLoad().ChipsData.FireworkChip.ChipTag;
            GetHelperPrefab = () => Match3Settings.GetOrLoad().ChipsData.FireworkChip.Helper;

            RuntimeHelpers.RunClassConstructor(
                    typeof(BaseChipCleanupLevelSettingsSynchronizer<FireworkChipLevelSettingsSynchronizer>).TypeHandle
                );
        }
    }
}