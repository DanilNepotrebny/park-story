﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Runtime.CompilerServices;
using UnityEditor;

namespace DreamTeam.Editor.Match3.LevelSettingsSynchronizer
{
    [InitializeOnLoad]
    public class StatueChipLevelSettingsSynchronizer :
        BaseChipCleanupLevelSettingsSynchronizer<StatueChipLevelSettingsSynchronizer>
    {
        static StatueChipLevelSettingsSynchronizer()
        {
            GetChipTag = () => Match3Settings.GetOrLoad().ChipsData.StatueChip.ChipTag;
            GetHelperPrefab = () => Match3Settings.GetOrLoad().ChipsData.StatueChip.Helper;

            RuntimeHelpers.RunClassConstructor(
                    typeof(BaseChipCleanupLevelSettingsSynchronizer<StatueChipLevelSettingsSynchronizer>).TypeHandle
                );
        }
    }
}