﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Runtime.CompilerServices;
using UnityEditor;

namespace DreamTeam.Editor.Match3.LevelSettingsSynchronizer
{
    [InitializeOnLoad]
    public class CommonBoxChipLevelSettingsSynchronizer :
        BaseChipCleanupLevelSettingsSynchronizer<CommonBoxChipLevelSettingsSynchronizer>
    {
        static CommonBoxChipLevelSettingsSynchronizer()
        {
            GetChipTag = () => Match3Settings.GetOrLoad().ChipsData.CommonBoxChip.ChipTag;
            GetHelperPrefab = () => Match3Settings.GetOrLoad().ChipsData.CommonBoxChip.Helper;

            RuntimeHelpers.RunClassConstructor(
                    typeof(BaseChipCleanupLevelSettingsSynchronizer<CommonBoxChipLevelSettingsSynchronizer>).TypeHandle
                );
        }
    }
}