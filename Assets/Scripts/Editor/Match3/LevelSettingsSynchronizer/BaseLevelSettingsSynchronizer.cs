﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Linq;
using DreamTeam.Editor.Utils;
using DreamTeam.Match3;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using JetBrains.Annotations;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace DreamTeam.Editor.Match3.LevelSettingsSynchronizer
{
    public class BaseLevelSettingsSynchronizer<RequirementType, ChildType> : MonoBehaviour
        where RequirementType : CountedRequirement, CountedRequirement.IEditorApi
    {
        /// [Assign in child!] Delegate to find proper requirement out of Level Settings
        protected static FindRequirementDelegate FindRequirement;

        /// [Assign in child!] Delegate to get requirement count to synchronize
        protected static GetRequirementCountDelegate GetRequirementCount;

        /// Callback is called when requirement was added to Level Settings
        protected static OnRequirementAddedDelegate OnRequirementAdded;

        /// Callback is called when requirement helper is not assigned to goal
        protected static OnRequirementHelperIsNullDelegate OnRequirementHelperIsNull;

        /// Delegate to check whether synchronization is applied (True by default)
        protected static IsSynchronizationAppliedDelegate IsSynchronizationApplied;

        static BaseLevelSettingsSynchronizer()
        {
            EditorSceneManager.sceneOpened += OnSceneOpened;
            EditorSceneManager.sceneSaving += OnSceneSaving;
        }

        private static void OnSceneOpened(Scene scene, OpenSceneMode mode)
        {
            SyncLevelSetting(scene.path);
        }

        private static void OnSceneSaving(Scene scene, string path)
        {
            SyncLevelSetting(scene.path);
        }

        private static void SyncLevelSetting(string scenePath)
        {
            if (!IsLevelScene(scenePath))
            {
                return;
            }

            LevelSettings settings = FindAndLoadSettings(scenePath);
            if (settings == null)
            {
                return;
            }

            if (IsSynchronizationApplied != null &&
                !IsSynchronizationApplied(settings))
            {
                return;
            }

            Assert.IsNotNull(GetRequirementCount, $"Missing delegate for {nameof(GetRequirementCount)}");
            int requirementCount = GetRequirementCount(settings.ScenePath);
            SyncLevelRequirements(requirementCount, settings);
        }

        private static void SyncLevelRequirements(int requirementCount, [NotNull] LevelSettings settings)
        {
            Assert.IsNotNull(FindRequirement, $"Missing delegate for {nameof(FindRequirement)}");
            var goal = FindRequirement(settings) as RequirementType;

            if (requirementCount > 0)
            {
                if (goal == null)
                {
                    goal = AssetDatabaseUtils.CreateSubAsset<RequirementType>(typeof(RequirementType).Name, settings);
                    (settings as LevelSettings.IEditorAPI).AddGoal(goal);
                    OnRequirementAdded?.Invoke(goal);
                }

                goal.SetRequiredCountInEditor(requirementCount);

                if (goal.DefaultHelperPrefab == null)
                {
                    OnRequirementHelperIsNull?.Invoke(goal);
                }
            }
            else
            {
                (settings as LevelSettings.IEditorAPI).RemoveGoal(goal);
            }

            EditorUtility.SetDirty(settings);
            AssetDatabase.SaveAssets();
        }

        private static LevelSettings FindAndLoadSettings(string scenePath)
        {
            return AssetDatabaseUtils
                .EnumerateAssets<LevelSettings>()
                .FirstOrDefault(levelSettings => levelSettings.ScenePath.Equals(scenePath));
        }

        private static bool IsLevelScene(string scenePath)
        {
            GridSystem[] gridSystems = FindObjectsOfType<GridSystem>();
            return gridSystems.Any(system => system.gameObject.scene.path.Equals(scenePath));
        }

        #region Inner types

        public delegate int GetRequirementCountDelegate(string scenePath);

        public delegate Requirement FindRequirementDelegate([NotNull] LevelSettings settings);

        public delegate void OnRequirementAddedDelegate([NotNull] RequirementType requirement);

        public delegate void OnRequirementHelperIsNullDelegate([NotNull] RequirementType requirement);

        public delegate bool IsSynchronizationAppliedDelegate([NotNull] LevelSettings settings);

        #endregion Inner types
    }
}
