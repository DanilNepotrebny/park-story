﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Runtime.CompilerServices;
using UnityEditor;

namespace DreamTeam.Editor.Match3.LevelSettingsSynchronizer
{
    [InitializeOnLoad]
    public class KeyChipLevelSettingsSynchronizer :
        BaseChipCleanupLevelSettingsSynchronizer<KeyChipLevelSettingsSynchronizer>
    {
        static KeyChipLevelSettingsSynchronizer()
        {
            GetChipTag = () => Match3Settings.GetOrLoad().ChipsData.KeyChip.ChipTag;
            GetHelperPrefab = () => Match3Settings.GetOrLoad().ChipsData.KeyChip.Helper;

            RuntimeHelpers.RunClassConstructor(
                    typeof(BaseChipCleanupLevelSettingsSynchronizer<KeyChipLevelSettingsSynchronizer>).TypeHandle
                );
        }
    }
}