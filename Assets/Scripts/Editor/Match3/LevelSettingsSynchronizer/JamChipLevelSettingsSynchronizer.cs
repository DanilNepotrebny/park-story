﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Runtime.CompilerServices;
using UnityEditor;

namespace DreamTeam.Editor.Match3.LevelSettingsSynchronizer
{
    [InitializeOnLoad]
    public class JamChipLevelSettingsSynchronizer :
        BaseChipCleanupLevelSettingsSynchronizer<JamChipLevelSettingsSynchronizer>
    {
        static JamChipLevelSettingsSynchronizer()
        {
            GetChipTag = () => Match3Settings.GetOrLoad().ChipsData.JamChip.ChipTag;
            GetHelperPrefab = () => Match3Settings.GetOrLoad().ChipsData.JamChip.Helper;

            RuntimeHelpers.RunClassConstructor(
                    typeof(BaseChipCleanupLevelSettingsSynchronizer<JamChipLevelSettingsSynchronizer>).TypeHandle
                );
        }
    }
}