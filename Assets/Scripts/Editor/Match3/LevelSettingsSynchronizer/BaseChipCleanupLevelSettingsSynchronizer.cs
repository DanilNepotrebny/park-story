﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using DreamTeam.Match3;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.Levels;
using DreamTeam.Match3.Requirements;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine.Assertions;

namespace DreamTeam.Editor.Match3.LevelSettingsSynchronizer
{
    public class BaseChipCleanupLevelSettingsSynchronizer<ChildType> :
        BaseLevelSettingsSynchronizer<ChipCleanupRequirement, ChildType>
    {
        /// [Assign in child!] Delegate to get required chip ChipTag
        protected static GetChipTagDelegate GetChipTag;

        /// Delegate to get requirement helper prefab
        protected static GetRequirementHelperDelegate GetHelperPrefab;

        static BaseChipCleanupLevelSettingsSynchronizer()
        {
            FindRequirement = FindCleanupChipGoal;
            GetRequirementCount = GetCleanupChipsCountOnScene;
            IsSynchronizationApplied = IsCleanupChipsGoalExist;
            OnRequirementAdded = OnChipCleanupRequirementAdded;
            OnRequirementHelperIsNull = OnChipCleanupRequirementHelperIsNull;

            RuntimeHelpers.RunClassConstructor(
                    typeof(BaseLevelSettingsSynchronizer<ChipCleanupRequirement, ChildType>).TypeHandle
                );
        }

        private static Requirement FindCleanupChipGoal(LevelSettings settings)
        {
            return settings
                .Goals
                .OfType<ChipCleanupRequirement>()
                .FirstOrDefault(req => req.RequiredChip == GetChipTag());
        }

        private static bool IsCleanupChipsGoalExist([NotNull] LevelSettings settings)
        {
            return settings
                .Goals
                .OfType<ChipCleanupRequirement>()
                .Any(req => req.RequiredChip == GetChipTag());
        }

        private static int GetCleanupChipsCountOnScene(string scenePath)
        {
            List<ChipComponent> chips = FindObjectsOfType<ChipComponent>().ToList();
            chips.RemoveAll(
                    chip =>
                        !chip.Tags.Has(GetChipTag(), false) ||
                        !chip.gameObject.scene.path.Equals(scenePath) ||
                        PrefabUtility.GetPrefabType(chip.gameObject) == PrefabType.Prefab
                );

            Assert.IsTrue(chips.Count > 0, $"Didn't find '{GetChipTag().name}' chips on grid");
            return chips.Count;
        }

        private static void OnChipCleanupRequirementAdded([NotNull] ChipCleanupRequirement requirement)
        {
            requirement.SetRequiredChipInEditor(GetChipTag());
            if (GetHelperPrefab != null)
            {
                requirement.SetDefaultHelperInEditor(GetHelperPrefab());
            }
        }

        private static void OnChipCleanupRequirementHelperIsNull([NotNull] ChipCleanupRequirement requirement)
        {
            if (GetHelperPrefab != null)
            {
                requirement.SetDefaultHelperInEditor(GetHelperPrefab());
            }
        }

        #region Inner types

        public delegate ChipTag GetChipTagDelegate();

        public delegate RequirementHelper GetRequirementHelperDelegate();

        #endregion
    }
}