﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Runtime.CompilerServices;
using UnityEditor;

namespace DreamTeam.Editor.Match3.LevelSettingsSynchronizer
{
    [InitializeOnLoad]
    public class IceChipLevelSettingsSynchronizer :
        BaseChipCleanupLevelSettingsSynchronizer<IceChipLevelSettingsSynchronizer>
    {
        static IceChipLevelSettingsSynchronizer()
        {
            GetChipTag = () => Match3Settings.GetOrLoad().ChipsData.IceChip.ChipTag;
            GetHelperPrefab = () => Match3Settings.GetOrLoad().ChipsData.IceChip.Helper;

            RuntimeHelpers.RunClassConstructor(
                    typeof(BaseChipCleanupLevelSettingsSynchronizer<IceChipLevelSettingsSynchronizer>).TypeHandle
                );
        }
    }
}