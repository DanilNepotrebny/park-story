﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Runtime.CompilerServices;
using UnityEditor;

namespace DreamTeam.Editor.Match3.LevelSettingsSynchronizer
{
    [InitializeOnLoad]
    public class ColorBoxChipLevelSettingsSynchronizer :
        BaseChipCleanupLevelSettingsSynchronizer<ColorBoxChipLevelSettingsSynchronizer>
    {
        static ColorBoxChipLevelSettingsSynchronizer()
        {
            GetChipTag = () => Match3Settings.GetOrLoad().ChipsData.ColorBoxChip.ChipTag;
            GetHelperPrefab = () => Match3Settings.GetOrLoad().ChipsData.ColorBoxChip.Helper;

            RuntimeHelpers.RunClassConstructor(
                    typeof(BaseChipCleanupLevelSettingsSynchronizer<ColorBoxChipLevelSettingsSynchronizer>).TypeHandle
                );
        }
    }
}