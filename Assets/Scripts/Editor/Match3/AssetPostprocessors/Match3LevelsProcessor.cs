﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DreamTeam.Editor.AssetPostprocessors;
using DreamTeam.Match3.Levels;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetPostprocessors
{
    [InitializeOnLoad]
    public class Match3LevelsProcessor
    {
        private const string _sceneTemplatePath = "Assets/Scenes/LevelTemplate.unity";

        private static HashSet<string> _importedLevelPaths = new HashSet<string>();
        private static HashSet<string> _deletedLevelPaths = new HashSet<string>();
        private static Dictionary<string, string> _movedLevelPaths = new Dictionary<string, string>();

        private static bool _isProcessingLevels = false;

        static Match3LevelsProcessor()
        {
            UnityAssetsProcessor.AddProcessor(new SceneProcessor());
            UnityAssetsProcessor.AddProcessor(new LevelSettingsProcessor());
        }

        private static void FlushImportedLevels()
        {
            _isProcessingLevels = true;

            foreach (string levelPath in _importedLevelPaths)
            {
                SetupLevel(levelPath);
            }
            _importedLevelPaths.Clear();

            _isProcessingLevels = false;
        }

        private static void FlushDeletedLevels()
        {
            _isProcessingLevels = true;

            foreach (string levelPath in _deletedLevelPaths)
            {
                DeleteLevel(levelPath);
            }
            _deletedLevelPaths.Clear();

            _isProcessingLevels = false;
        }

        private static void FlushMovedLevels()
        {
            _isProcessingLevels = true;

            foreach (KeyValuePair<string, string> pair in _movedLevelPaths)
            {
                MoveLevel(pair.Key, pair.Value);
            }
            _movedLevelPaths.Clear();

            _isProcessingLevels = false;
        }
        
        private static void SetupLevel([NotNull] string levelPath)
        {
            string levelSettingsPath = Path.ChangeExtension(levelPath, "asset");
            LevelSettings levelSettings = AssetDatabase.LoadAssetAtPath<LevelSettings>(levelSettingsPath);
            if (levelSettings == null)
            {
                levelSettings = ScriptableObject.CreateInstance<LevelSettings>();
                AssetDatabase.CreateAsset(levelSettings, levelSettingsPath);
            }

            string levelScenePath = Path.ChangeExtension(levelPath, "unity");
            SceneAsset levelScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(levelScenePath);
            if (levelScene == null)
            {
                AssetDatabase.CopyAsset(_sceneTemplatePath, levelScenePath);
            }

            if (EditorBuildSettings.scenes.Count(scene => scene.path == levelScenePath) == 0)
            {
                var scenes = EditorBuildSettings.scenes;
                Array.Resize(ref scenes, scenes.Length + 1);
                scenes[scenes.Length - 1] = new EditorBuildSettingsScene(levelScenePath, true);
                EditorBuildSettings.scenes = scenes;
            }

            ((LevelSettings.IEditorAPI)levelSettings).SetSceneAssetPath(levelScenePath);
            AddLevelToLevelPack(levelSettingsPath, levelSettings);
        }

        private static void DeleteLevel([NotNull] string levelPath)
        {
            string levelSettingsPath = Path.ChangeExtension(levelPath, "asset");
            string levelScenePath = Path.ChangeExtension(levelPath, "unity");

            RemoveFromLevelPack(levelScenePath);

            AssetDatabase.DeleteAsset(levelSettingsPath);
            AssetDatabase.DeleteAsset(levelScenePath);
            EditorBuildSettings.scenes = EditorBuildSettings.scenes.Where(scene => scene.path != levelScenePath).ToArray();
        }

        private static void MoveLevel(string levelPath, string oldLevelPath)
        {
            string levelSettingsPath = Path.ChangeExtension(levelPath, "asset");
            LevelSettings levelSettings = AssetDatabase.LoadAssetAtPath<LevelSettings>(levelSettingsPath);
            if (levelSettings == null)
            {
                AssetDatabase.MoveAsset(Path.ChangeExtension(oldLevelPath, "asset"), levelSettingsPath);
                levelSettings = AssetDatabase.LoadAssetAtPath<LevelSettings>(levelSettingsPath);
            }

            string levelScenePath = Path.ChangeExtension(levelPath, "unity");
            string oldLevelScenePath = Path.ChangeExtension(oldLevelPath, "unity");
            SceneAsset levelScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(levelScenePath);
            if (levelScene == null)
            {
                AssetDatabase.MoveAsset(oldLevelScenePath, levelScenePath);
            }

            if (EditorBuildSettings.scenes.Count(scene => scene.path == levelScenePath) == 0)
            {
                foreach (EditorBuildSettingsScene buildScene in EditorBuildSettings.scenes)
                {
                    if (buildScene.path == oldLevelScenePath)
                    {
                        buildScene.path = levelScenePath;
                        break;
                    }
                }
            }

            RemoveFromLevelPack(oldLevelScenePath);
            ((LevelSettings.IEditorAPI)levelSettings).SetSceneAssetPath(levelScenePath);
            AddLevelToLevelPack(levelSettingsPath, levelSettings);
        }

        private static void AddLevelToLevelPack(string levelSettingsPath, LevelSettings levelSettings)
        {
            string levelPackPath = Path.GetDirectoryName(levelSettingsPath) + ".asset";
            var levelPack = AssetDatabase.LoadAssetAtPath<LevelPack>(levelPackPath);
            if (levelPack == null)
            {
                levelPack = ScriptableObject.CreateInstance<LevelPack>();
                ((LevelPack.IEditorApi)levelPack).AddLevel(levelSettings);
                AssetDatabase.CreateAsset(levelPack, levelPackPath);
            }
            else
            {
                ((LevelPack.IEditorApi)levelPack).AddLevel(levelSettings);
                EditorUtility.SetDirty(levelPack);
            }
        }

        private static void RemoveFromLevelPack(string levelScenePath)
        {
            string levelPackPath = Path.GetDirectoryName(levelScenePath) + ".asset";
            var levelPack = AssetDatabase.LoadAssetAtPath<LevelPack>(levelPackPath);
            if (levelPack != null)
            {
                ((LevelPack.IEditorApi)levelPack).RemoveLevel(levelScenePath);
                EditorUtility.SetDirty(levelPack);
            }
        }

        private static void AddImportedLevelPath([NotNull] string assetPath)
        {
            if (_isProcessingLevels)
            {
                return;
            }

            if (_importedLevelPaths.Count == 0)
            {
                EditorApplication.delayCall += FlushImportedLevels;
            }

            _importedLevelPaths.Add(Path.ChangeExtension(assetPath, ""));
        }

        private static void AddDeletedLevelPath([NotNull] string assetPath)
        {
            if (_isProcessingLevels)
            {
                return;
            }

            if (_deletedLevelPaths.Count == 0)
            {
                EditorApplication.delayCall += FlushDeletedLevels;
            }

            _deletedLevelPaths.Add(Path.ChangeExtension(assetPath, ""));
        }

        private static void AddMovedLevelPath([NotNull] string assetPath, [NotNull] string oldAssetPath)
        {
            if (_isProcessingLevels)
            {
                return;
            }

            if (_movedLevelPaths.Count == 0)
            {
                EditorApplication.delayCall += FlushMovedLevels;
            }

            string levelPath = Path.ChangeExtension(assetPath, "");

            if (!_movedLevelPaths.ContainsKey(levelPath))
            {
                _movedLevelPaths.Add(assetPath, oldAssetPath);
            }
        }

        #region Inner types

        private class SceneProcessor : BaseAssetProcessor<SceneAsset>
        {
            protected override string AssetFolder => "Assets/Scenes/Levels";
            protected override string AssetExtension => ".unity";

            protected override void OnAssetImported(SceneAsset importedAsset, string assetPath)
            {
                AddImportedLevelPath(assetPath);
            }

            protected override void OnAssetDeleted(string assetPath)
            {
                AddDeletedLevelPath(assetPath);
            }

            protected override void OnAssetMoved(SceneAsset asset, string assetPath, string oldAssetPath)
            {
                AddMovedLevelPath(assetPath, oldAssetPath);
            }
        }

        private class LevelSettingsProcessor : BaseAssetProcessor<LevelSettings>
        {
            protected override string AssetFolder => "Assets/Scenes/Levels";
            protected override string AssetExtension => ".asset";

            protected override void OnAssetImported(LevelSettings importedAsset, string assetPath)
            {
                AddImportedLevelPath(assetPath);
            }

            protected override void OnAssetDeleted(string assetPath)
            {
                AddDeletedLevelPath(assetPath);
            }

            protected override void OnAssetMoved(LevelSettings asset, string assetPath, string oldAssetPath)
            {
                AddMovedLevelPath(assetPath, oldAssetPath);
            }
        }

        #endregion
    }
}