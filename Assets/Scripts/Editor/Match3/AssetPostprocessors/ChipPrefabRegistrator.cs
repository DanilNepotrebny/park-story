﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Editor.AssetPostprocessors;
using DreamTeam.Match3;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Editor.Match3.AssetPostprocessors
{
    public class ChipPrefabRegistrator : BaseAssetProcessor<GameObject>
    {
        protected override string AssetExtension => ".prefab";

        private ChipPrefabsCollector.IEditorApi _chipPrefabsCollector => SystemAssetAccessor<ChipPrefabsCollector>.Asset;

        public override void Finish()
        {
            _chipPrefabsCollector.CleanUp();
        }

        protected override void OnAssetImported(GameObject asset, string assetPath)
        {
            if (EditorUtils.IsPrefab(asset) &&
                asset.GetComponent<ChipComponent>() != null)
            {
                _chipPrefabsCollector.RegisterChipPrefab(asset);
            }
        }
    }
}
