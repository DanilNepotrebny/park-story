﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Match3.ChipGeneration;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace DreamTeam.Editor.Match3.ChipGeneration
{
    [CustomEditor(typeof(GenerationSystem))]
    public class GenerationSystemEditor : UnityEditor.Editor
    {
        private ReorderableList _genRulesList;
        private const string GenerationRulesPropertyName = "_generationRules";
        private const string GenerationRulesHeader = "Generation rules (higher applies first)";

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();
            _genRulesList.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }

        protected void OnEnable()
        {
            _genRulesList = new ReorderableList(
                    serializedObject,
                    serializedObject.FindProperty(GenerationRulesPropertyName),
                    true,
                    true,
                    true,
                    true)
                {
                    drawElementCallback = GenerationRulesDrawElementCallback,
                    drawHeaderCallback = GenerationRulesDrawHeaderCallback
                };
        }

        private void GenerationRulesDrawHeaderCallback(Rect rect)
        {
            EditorGUI.LabelField(rect, GenerationRulesHeader);
        }

        private void GenerationRulesDrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            var element = _genRulesList.serializedProperty.GetArrayElementAtIndex(index);

            rect.y += 2;

            float stageFieldWidth = 120;
            SerializedProperty stageProperty = element.FindPropertyRelative("Stage");
            stageProperty.intValue = EditorGUI.MaskField(
                new Rect(rect.x, rect.y, stageFieldWidth, EditorGUIUtility.singleLineHeight),
                stageProperty.intValue,
                stageProperty.enumNames);

            float ruleFieldOffsetX = stageFieldWidth + 5;
            float ruleFieldWidth = rect.width - ruleFieldOffsetX;
            EditorGUI.PropertyField(
                new Rect(rect.x + ruleFieldOffsetX, rect.y, ruleFieldWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("ChipRule"),
                GUIContent.none);
        }
    }
}