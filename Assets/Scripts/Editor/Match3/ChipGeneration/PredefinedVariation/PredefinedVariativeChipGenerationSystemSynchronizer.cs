﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Match3.ChipGeneration;
using DreamTeam.Match3.ChipGeneration.PredefinedVariation;
using DreamTeam.Utils;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DreamTeam.Editor.Match3.ChipGeneration.PredefinedVariation
{
    [InitializeOnLoad]
    public static class PredefinedVariativeChipGenerationSystemSynchronizer
    {
        private static HashSet<int> _predefinedVariationIds = new HashSet<int>();

        static PredefinedVariativeChipGenerationSystemSynchronizer()
        {
            EditorSceneManager.sceneOpened += OnSceneOpened;
            EditorSceneManager.sceneSaving += OnSceneSaving;
            PredefinedVariativeChip.EditorStaticAPI.Created += OnPredefinedVariativeChipCreated;
            PredefinedVariativeChip.EditorStaticAPI.Destroyed += OnPredefinedVariativeChipDestroyed;
        }

        private static void OnPredefinedVariativeChipDestroyed(PredefinedVariativeChip predefinedChip)
        {
            Sync(predefinedChip.gameObject.scene);
        }

        private static void OnPredefinedVariativeChipCreated(PredefinedVariativeChip predefinedChip)
        {
            Sync(predefinedChip.gameObject.scene);
        }

        private static void OnSceneSaving(Scene scene, string path)
        {
            Sync(scene);
        }

        private static void OnSceneOpened(Scene scene, OpenSceneMode mode)
        {
            Sync(scene);
        }

        private static void Sync(Scene scene)
        {
            if (scene != default(Scene))
            {
                FillPredefinedVariationIds(scene);
                PredefinedVariativeChipGenerationSystem system = GetSystemOnScene(scene);
                if (_predefinedVariationIds.Count > 0)
                {
                    system = system ?? CreateSystemOnScene(scene);
                    (system as PredefinedVariativeChipGenerationSystem.IEditorAPI).SyncChipsVariations(
                        _predefinedVariationIds);
                }
                else
                {
                    if (system != null)
                    {
                        if (system.gameObject.GetComponents<Component>().Length > 2)
                        {
                            system.Dispose();
                        }
                        else
                        {
                            system.gameObject.Dispose();
                        }
                    }
                }
            }
        }

        private static PredefinedVariativeChipGenerationSystem GetSystemOnScene(Scene scene)
        {
            var systems = Object.FindObjectsOfType<PredefinedVariativeChipGenerationSystem>();
            foreach (var item in systems)
            {
                if (item.gameObject.scene.name == scene.name)
                {
                    return item;
                }
            }

            return null;
        }

        private static PredefinedVariativeChipGenerationSystem CreateSystemOnScene(Scene scene)
        {
            GameObject newSystemGO = null;
            var generationSystems = Object.FindObjectsOfType<GenerationSystem>();
            foreach (var item in generationSystems)
            {
                if (item.gameObject.scene.name == scene.name)
                {
                    newSystemGO = item.gameObject;
                }
            }

            if (newSystemGO == null)
            {
                newSystemGO = new GameObject(nameof(PredefinedVariativeChipGenerationSystem));
                SceneManager.MoveGameObjectToScene(newSystemGO, scene);
            }

            return newSystemGO.AddComponent<PredefinedVariativeChipGenerationSystem>();
        }

        private static void FillPredefinedVariationIds(Scene scene)
        {
            _predefinedVariationIds.Clear();
            var chips = Object.FindObjectsOfType<PredefinedVariativeChip>();

            foreach (var chip in chips)
            {
                if (chip.gameObject.scene.name == scene.name)
                {
                    _predefinedVariationIds.Add(chip.VariationId);
                }
            }
        }
    }
}