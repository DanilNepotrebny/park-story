﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Linq;
using DreamTeam.Editor.Utils;
using DreamTeam.Match3;
using DreamTeam.Match3.ChipGeneration;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Match3
{
    public class UpgradeGeneratorsMenuItem : BaseScenesProcessor
    {
        private static GameObject _generatorPrefab;

        [MenuItem("Assets/Upgrade generators", true, 0)]
        private static bool ValidateReserializeSelectedAssets()
        {
            return Selection.objects.Any(obj => obj is SceneAsset);
        }

        [MenuItem("Assets/Upgrade generators", false, 1)]
        private static void MenuItem()
        {
            _generatorPrefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Match3/Generator.prefab");
            if (_generatorPrefab == null)
            {
                UnityEngine.Debug.LogError("Invalid Generator Path");
                return;
            }

            ProcessSelectedScenes(UpgradeScene);
        }

        private static void UpgradeScene()
        {
            CellComponent[] cells = Object.FindObjectsOfType<CellComponent>();

            foreach (CellComponent cell in cells)
            {
                GeneratorComponent generator = cell.GetComponent<GeneratorComponent>();
                if (generator != null)
                {
                    GameObject newGeneratorGo = (GameObject)PrefabUtility.InstantiatePrefab(_generatorPrefab);
                    newGeneratorGo.transform.position = generator.transform.position;
                    Undo.SetTransformParent(newGeneratorGo.transform, generator.transform, "Set Transform");
                    GeneratorComponent newGenerator = newGeneratorGo.GetComponent<GeneratorComponent>();
                    Undo.RecordObject(newGenerator, "Generator Settings Copy");
                    EditorUtility.CopySerialized(generator, newGenerator);
                    Undo.DestroyObjectImmediate(generator);
                }
            }
        }
    }
}