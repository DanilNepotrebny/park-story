﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using DreamTeam.Editor.Utils;
using DreamTeam.Match3.ChipTags;
using UnityEditor;

namespace DreamTeam.Editor.Match3
{
    /// <summary>
    /// Automatically creates asset for each non-abstract ChipTag class on script recompilation.
    /// </summary>
    [InitializeOnLoad]
    public static class ChipTagUtils
    {
        static ChipTagUtils()
        {
            AssemblyReloadEvents.afterAssemblyReload += OnAssemblyReloaded;
        }

        private static void OnAssemblyReloaded()
        {
            IEnumerable<Type> tagTypes =
                from type in typeof(ChipTag).Assembly.GetTypes()
                where !type.IsAbstract && (type == typeof(ChipTag) || type.IsSubclassOf(typeof(ChipTag)))
                select type;

            foreach (Type type in tagTypes)
            {
                AssetDatabaseUtils.LoadOrCreateAsset("Assets/Settings/ChipTags", type);
            }
        }
    }
}