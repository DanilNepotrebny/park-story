﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using DreamTeam.Editor.Utils;
using DreamTeam.Match3.Levels;
using UnityEditor;

namespace DreamTeam.Editor.Match3
{
    [CustomEditor(typeof(LevelSettings))]
    public class LevelSettingsEditor : UnityEditor.Editor
    {
        private readonly List<EditorGUIUtils.FoldableEditorState> _goalFoldoutState =
            new List<EditorGUIUtils.FoldableEditorState>();

        private readonly List<EditorGUIUtils.FoldableEditorState> _limitationFoldoutState =
            new List<EditorGUIUtils.FoldableEditorState>();

        /// <summary>
        /// Unity calback
        /// </summary>
        /// <inheritdoc />
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Separator();

            EditorGUIUtils.CreatableScriptableObjectList(serializedObject.FindProperty("_goals"), _goalFoldoutState);
            EditorGUIUtils.CreatableScriptableObjectList(
                serializedObject.FindProperty("_limitations"),
                _limitationFoldoutState);
        }
    }
}