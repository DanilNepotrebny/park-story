﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Editor.GridEditor;
using DreamTeam.Editor.Utils;
using DreamTeam.Match3.Bezel;
using DreamTeam.Match3.ChipTags;
using DreamTeam.Match3.Requirements;
using DreamTeam.Match3.SpecialChips.Mantle;
using DreamTeam.Utils;
using UnityEngine;
using UnityEngine.Tilemaps;
using Object = UnityEngine.Object;

namespace DreamTeam.Editor.Match3
{
    /// <summary>
    /// Asset for match-3 grid editor settings
    /// </summary>
    public class Match3Settings : BaseSettings
    {
        [SerializeField] private GameObject _cellPrefab;
        [SerializeField] private BezelBrush _fieldBezelBrush;
        [SerializeField] private BezelBrush _conveyorBezelBrush;

        [Header("Mantle chip")]
        [SerializeField] private BezelBrush _mantleBezelBrush;
        [SerializeField] private MantleComponent _mantlePrefab;
        [SerializeField] private Tilemap _mantleBezelTilemapPrefab;

        [Space, SerializeField] private ChipsStaticDataFacade _chipsData;

        [Header("Placing area")]
        [SerializeField] private PlacingAreaLinkedAssets _linkedAssets;

        private static Match3Settings _instance;

        public GameObject CellPrefab => _cellPrefab;
        public BezelBrush FieldBezelBrush => _fieldBezelBrush;
        public BezelBrush ConveyorBezelBrush => _conveyorBezelBrush;

        public BezelBrush MantleBezelBrush => _mantleBezelBrush;
        public MantleComponent MantlePrefab => _mantlePrefab;
        public Tilemap MantleBezelTilemapPrefab => _mantleBezelTilemapPrefab;

        public ChipsStaticDataFacade ChipsData => _chipsData;
        public PlacingAreaLinkedAssets LinkedAssets => _linkedAssets;

        public static Match3Settings GetOrLoad()
        {
            if (_instance == null)
            {
                _instance = AssetDatabaseUtils.LoadOrCreateAsset<Match3Settings>("Assets/Settings/Editor");
            }

            return _instance;
        }

        #region Inner types

        [Serializable] public class PlacingAreaLinkedAssets : KeyValueList<Object, Object>{};

        [Serializable] public struct ChipsStaticDataFacade
        {
            [SerializeField] private ChipStaticData _jamChip;
            [SerializeField] private ChipStaticData _commonBoxChip;
            [SerializeField] private ChipStaticData _colorBoxChip;
            [SerializeField] private ChipStaticData _iceChip;
            [SerializeField] private ChipStaticData _fireworkChip;
            [SerializeField] private ChipStaticData _gumballChip;
            [SerializeField] private ChipStaticData _statueChip;
            [SerializeField] private ChipStaticData _keyChip;
            [SerializeField] private ChipStaticData _ballChip;

            public ChipStaticData JamChip => _jamChip;
            public ChipStaticData CommonBoxChip => _commonBoxChip;
            public ChipStaticData ColorBoxChip => _colorBoxChip;
            public ChipStaticData IceChip => _iceChip;
            public ChipStaticData FireworkChip => _fireworkChip;
            public ChipStaticData GumballChip => _gumballChip;
            public ChipStaticData StatueChip => _statueChip;
            public ChipStaticData KeyChip => _keyChip;
            public ChipStaticData BallChip => _ballChip;
        }

        [Serializable]
        public struct ChipStaticData
        {
            [SerializeField, RequiredField] private ChipTag _chipTag;
            [SerializeField, RequiredField] private RequirementHelper _helper;

            public ChipTag ChipTag => _chipTag;
            public RequirementHelper Helper => _helper;
        }

        #endregion
    }
}