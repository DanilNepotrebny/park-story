﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Editor.Utils;
using DreamTeam.Inventory;
using DreamTeam.Inventory.Items;
using DreamTeam.Location.Decorations;
using DreamTeam.Utils;
using UnityEditor;

namespace DreamTeam.Editor.AssetPostprocessors
{
    [InitializeOnLoad]
    public class CountedItemRegistrator : BaseAssetProcessor<CountedItem>
    {
        protected override string AssetExtension => ".asset";

        private CountedItemContainer _container => SystemAssetAccessor<CountedItemContainer>.Asset;

        static CountedItemRegistrator()
        {
            UnityAssetsProcessor.AddProcessor(new CountedItemRegistrator());
        }

        public override void Finish()
        {
            _container.ClearMissingCountedItems();
        }

        protected override void OnAssetImported(CountedItem importedAsset, string assetPath)
        {
            _container.RegisterCountedItem(importedAsset);

            if (string.IsNullOrEmpty(importedAsset.Id))
            {
                ((CountedItem.IEditorApi)importedAsset).SetId(
                        IdReformatter.ReformatId(importedAsset.name)
                    );
            }

            if (importedAsset.DefaultProductPack != null)
            {
                ProductPack.IEditorApi productPack = importedAsset.DefaultProductPack;
                if (string.IsNullOrEmpty(productPack.PurchaseId))
                {
                    productPack.PurchaseId = importedAsset.Id;
                }

                if (importedAsset is DecorationSkin)
                {
                    productPack.PurchaseType = "decoration_skin";
                }
            }
        }
    }
}