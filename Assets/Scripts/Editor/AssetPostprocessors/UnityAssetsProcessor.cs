﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using UnityEditor;

namespace DreamTeam.Editor.AssetPostprocessors
{
    public class UnityAssetsProcessor : AssetPostprocessor
    {
        private static List<AssetsChange> _assetsToProcess = new List<AssetsChange>();
        
        private static List<IAssetProcessor> _assetProcessor = new List<IAssetProcessor>();

        public static void AddProcessor(IAssetProcessor processor)
        {
            _assetProcessor.Add(processor);
        }

        private static void OnPostprocessAllAssets(
                string[] importedAssets,
                string[] deletedAssets,
                string[] movedAssets,
                string[] movedFromAssetPaths
            )
        {
            _assetsToProcess.Add(
                    new AssetsChange(importedAssets, deletedAssets, movedAssets, movedFromAssetPaths)
                );

            EditorApplication.delayCall -= ProcessAssets;
            EditorApplication.delayCall += ProcessAssets;
        }

        private static void ProcessAssets()
        {
            foreach (var registrator in _assetProcessor)
            {
                if (registrator.Initialize())
                {
                    foreach (AssetsChange changedAssets in _assetsToProcess)
                    {
                        registrator.ProcessAssets(changedAssets);
                    }

                    registrator.Finish();
                }
                else
                {
                    UnityEngine.Debug.LogError(
                        $"[PROGRAMMERS] UnityAssetsRegistrator::OnPostprocessAllAssets - cannot initialize {registrator.GetType()}!");
                }
            }

            _assetsToProcess.Clear();
        }

        #region

        public class AssetsChange
        {
            public string[] ImportedAssets { get; }
            public string[] DeletedAssets { get; }
            public string[] MovedAssets { get; }
            public string[] MovedFromAssetPaths { get; }

            public AssetsChange(
                    string[] importedAssets,
                    string[] deletedAssets,
                    string[] movedAssets,
                    string[] movedFromAssetPaths
                )
            {
                ImportedAssets = importedAssets;
                DeletedAssets = deletedAssets;
                MovedAssets = movedAssets;
                MovedFromAssetPaths = movedFromAssetPaths;
            }
        }

        #endregion
    }
}