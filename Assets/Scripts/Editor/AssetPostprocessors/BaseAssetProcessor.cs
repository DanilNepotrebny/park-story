﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using JetBrains.Annotations;
using UnityEditor;
using Object = UnityEngine.Object;

namespace DreamTeam.Editor.AssetPostprocessors
{
    public abstract class BaseAssetProcessor<TAsset> : IAssetProcessor
        where TAsset : Object
    {
        protected virtual string AssetFolder => "Assets";
        protected abstract string AssetExtension { get; }

        public virtual bool Initialize()
        {
            return true;
        }

        public void ProcessAssets(UnityAssetsProcessor.AssetsChange changedAssets)
        {
            ProcessAssets(changedAssets.DeletedAssets, ProcessDeletedAssets, changedAssets);
            ProcessAssets(changedAssets.MovedAssets, ProcessMovedAssets, changedAssets);
            ProcessAssets(changedAssets.ImportedAssets, ProcessImportedAssets, changedAssets);
        }

        public virtual void Finish()
        {
        }

        protected virtual void OnAssetImported([NotNull] TAsset asset, string assetPath)
        {
        }

        protected virtual void OnAssetDeleted(string assetPath)
        {
        }

        protected virtual void OnAssetMoved([NotNull] TAsset asset, string assetPath, string oldAssetPath)
        {
        }

        private void ProcessAssets(
                string[] assetPaths,
                Action<UnityAssetsProcessor.AssetsChange, int> assetProcessAction,
                UnityAssetsProcessor.AssetsChange changedAssets
            )
        {
            int assetsCount = assetPaths.Length;

            for (int i = 0; i < assetsCount; i++)
            {
                string assetPath = assetPaths[i];

                if (!assetPath.StartsWith(AssetFolder) || !assetPath.EndsWith(AssetExtension))
                {
                    continue;
                }

                bool isCancelled = EditorUtility.DisplayCancelableProgressBar(
                        $"[{GetType().Name}] Process asset",
                        $"{assetPath} : ({i}/{assetsCount})",
                        (float)i / assetsCount
                    );

                if (isCancelled)
                {
                    break;
                }

                assetProcessAction?.Invoke(changedAssets, i);
            }

            EditorUtility.ClearProgressBar();
        }

        private void ProcessImportedAssets(UnityAssetsProcessor.AssetsChange changedAssets, int index)
        {
            string path = changedAssets.ImportedAssets[index];
            TAsset asset = AssetDatabase.LoadAssetAtPath<TAsset>(path);

            if (asset != null)
            {
                OnAssetImported(asset, path);
            }
        }

        private void ProcessMovedAssets(UnityAssetsProcessor.AssetsChange changedAssets, int index)
        {
            string path = changedAssets.MovedAssets[index];
            TAsset asset = AssetDatabase.LoadAssetAtPath<TAsset>(path);

            if (asset != null)
            {
                OnAssetMoved(asset, path, changedAssets.MovedFromAssetPaths[index]);
            }
        }

        private void ProcessDeletedAssets(UnityAssetsProcessor.AssetsChange changedAssets, int index)
        {
            OnAssetDeleted(changedAssets.DeletedAssets[index]);
        }
    }
}