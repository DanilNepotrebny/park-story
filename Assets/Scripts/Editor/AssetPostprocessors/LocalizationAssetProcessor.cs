﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Localization;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.AssetPostprocessors
{
    [InitializeOnLoad]
    public class LocalizationAssetProcessor : BaseAssetProcessor<TextAsset>
    {
        protected override string AssetExtension => ".json";

        static LocalizationAssetProcessor()
        {
            UnityAssetsProcessor.AddProcessor(new LocalizationAssetProcessor());
        }

        protected override void OnAssetImported(TextAsset importedAsset, string assetPath)
        {
            LocalizationSystem.IEditorApi localizationSystem = SystemAssetAccessor<LocalizationSystem>.Instance;
            localizationSystem.OnLocalizationAssetImported(importedAsset);
        }
    }
}