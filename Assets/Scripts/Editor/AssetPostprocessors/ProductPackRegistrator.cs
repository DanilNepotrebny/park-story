﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Editor.Utils;
using DreamTeam.Inventory;
using DreamTeam.Utils;
using UnityEditor;

namespace DreamTeam.Editor.AssetPostprocessors
{
    [InitializeOnLoad]
    public class ProductPackRegistrator : BaseAssetProcessor<ProductPack>
    {
        protected override string AssetExtension => ".asset";

        private ProductPackInstanceContainer.IEditorApi _instanceContainer => SystemAssetAccessor<ProductPackInstanceContainer>.Asset;

        static ProductPackRegistrator()
        {
            UnityAssetsProcessor.AddProcessor(new ProductPackRegistrator());
        }

        public override void Finish()
        {
            _instanceContainer.ClearMissingProductPacks();
        }

        protected override void OnAssetImported(ProductPack importedAsset, string assetPath)
        {
            _instanceContainer.RegisterProductPack(importedAsset);

            var editorApi = ((ProductPack.IEditorApi)importedAsset);

            if (string.IsNullOrEmpty(importedAsset.PurchaseId))
            {
                editorApi.PurchaseId = importedAsset.name;
            }

            editorApi.PurchaseId = IdReformatter.ReformatId(importedAsset.PurchaseId);
            editorApi.PurchaseType = IdReformatter.ReformatId(importedAsset.PurchaseType);
        }
    }
}