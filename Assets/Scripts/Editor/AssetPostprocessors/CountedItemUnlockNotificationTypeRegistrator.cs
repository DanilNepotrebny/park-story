﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Inventory.Items.UnlockNotification;
using DreamTeam.Utils;
using UnityEditor;

namespace DreamTeam.Editor.AssetPostprocessors
{
    [InitializeOnLoad]
    public class CountedItemUnlockNotificationTypeRegistrator : BaseAssetProcessor<UnlockNotificationType>
    {
        protected override string AssetExtension => ".asset";

        private UnlockNotificationSystem.IEditorApi _system => SystemAssetAccessor<UnlockNotificationSystem>.Asset;

        static CountedItemUnlockNotificationTypeRegistrator()
        {
            UnityAssetsProcessor.AddProcessor(new CountedItemUnlockNotificationTypeRegistrator());
        }

        public override void Finish()
        {
            _system.ClearDeletedNotificationTypes();
        }

        protected override void OnAssetImported(UnlockNotificationType importedAsset, string assetPath)
        {
            _system.RegisterNotificationType(importedAsset);
        }
    }
}