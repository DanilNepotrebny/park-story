﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.UI.RewardPresenters;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace DreamTeam.Editor.UI.Presenters
{
    [CustomEditor(typeof(RewardsCollectionPresenter))]
    [CanEditMultipleObjects]
    public class RewardsCollectionPresenterEditor : UnityEditor.Editor
    {
        private ReorderableList _indexesList;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            SerializedProperty selectionTypeProperty = serializedObject.FindProperty("_selectionType");
            if (selectionTypeProperty.hasMultipleDifferentValues)
            {
                EditorGUILayout.LabelField("Editing multiple different selection types is not supported!", EditorStyles.miniTextField);
            }
            else
            {
                serializedObject.Update();

                RewardsCollectionPresenter.SelectionType selectionType = (RewardsCollectionPresenter.SelectionType)selectionTypeProperty.enumValueIndex;
                switch (selectionType)
                {
                    case RewardsCollectionPresenter.SelectionType.Indexes:
                        _indexesList.DoLayoutList();
                        break;

                    case RewardsCollectionPresenter.SelectionType.Range:
                        SerializedProperty rangeProperty = serializedObject.FindProperty("_range");
                        EditorGUILayout.PropertyField(rangeProperty);
                        break;
                }

                serializedObject.ApplyModifiedProperties();
            }
        }

        protected void OnEnable()
        {
            _indexesList = new ReorderableList(
                    serializedObject,
                    serializedObject.FindProperty("_indexes"),
                    false,
                    true,
                    true,
                    true)
                {
                    drawHeaderCallback = DrawHeaderCallback,
                    drawElementCallback = DrawElementCallback
                };
        }

        private void DrawHeaderCallback(Rect rect)
        {
            GUI.Label(rect, "Rewards indexes");
        }

        private void DrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            SerializedProperty indexProperty = _indexesList.serializedProperty.GetArrayElementAtIndex(index);
            EditorGUI.PropertyField(rect, indexProperty);
        }
    }
}