﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if !DLL_BUILD

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DreamTeam.Editor.Utils;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using PackageInfo = UnityEditor.PackageManager.PackageInfo;

namespace DreamTeam.Editor.DllProjectBuilding
{
    public class DllBuilder
    {
        private string[] _scriptsToCompile =
        {
            "Assets/Scripts/.*\\.cs$",
            "Assets/Plugins/.*\\.cs$"
        };

        private string[] _scriptsToExclude =
        {
            ".*/Tests/.+",
            ".*/Match3/.+",
            ".*/Cloud/.+",
            "Assets/Scripts/Editor/LocalizationImport/.+",
            "Assets/Plugins/ThirdParty/DevToDev/.+"
        };

        private string[] _defines =
        {
            "DEBUG",
            "TRACE",
            "UNITY_5_3_OR_NEWER",
            "UNITY_5_4_OR_NEWER",
            "UNITY_5_5_OR_NEWER",
            "UNITY_5_6_OR_NEWER",
            "UNITY_2017_1_OR_NEWER",
            "UNITY_2017_2_OR_NEWER",
            "UNITY_2017_3_OR_NEWER",
            "UNITY_2017_4_OR_NEWER",
            "UNITY_2018_1_OR_NEWER",
            "UNITY_2018_2_OR_NEWER",
            "UNITY_2018_2_3",
            "UNITY_2018_2",
            "UNITY_2018",
            "PLATFORM_ARCH_64",
            "UNITY_64",
            "ENABLE_AUDIO",
            "ENABLE_CACHING",
            "ENABLE_CLOTH",
            "ENABLE_DUCK_TYPING",
            "ENABLE_MICROPHONE",
            "ENABLE_MULTIPLE_DISPLAYS",
            "ENABLE_PHYSICS",
            "ENABLE_SPRITES",
            "ENABLE_GRID",
            "ENABLE_TILEMAP",
            "ENABLE_TERRAIN",
            "ENABLE_TEXTURE_STREAMING",
            "ENABLE_DIRECTOR",
            "ENABLE_UNET",
            "ENABLE_LZMA",
            "ENABLE_UNITYEVENTS",
            "ENABLE_WEBCAM",
            "ENABLE_WWW",
            "ENABLE_CLOUD_SERVICES_COLLAB",
            "ENABLE_CLOUD_SERVICES_COLLAB_SOFTLOCKS",
            "ENABLE_CLOUD_SERVICES_ADS",
            "ENABLE_CLOUD_HUB",
            "ENABLE_CLOUD_PROJECT_ID",
            "ENABLE_CLOUD_SERVICES_USE_WEBREQUEST",
            "ENABLE_CLOUD_SERVICES_UNET",
            "ENABLE_CLOUD_SERVICES_BUILD",
            "ENABLE_CLOUD_LICENSE",
            "ENABLE_EDITOR_HUB",
            "ENABLE_EDITOR_HUB_LICENSE",
            "ENABLE_WEBSOCKET_CLIENT",
            "ENABLE_DIRECTOR_AUDIO",
            "ENABLE_DIRECTOR_TEXTURE",
            "ENABLE_TIMELINE",
            "ENABLE_EDITOR_METRICS",
            "ENABLE_EDITOR_METRICS_CACHING",
            "ENABLE_MANAGED_JOBS",
            "ENABLE_MANAGED_TRANSFORM_JOBS",
            "ENABLE_MANAGED_ANIMATION_JOBS",
            "INCLUDE_DYNAMIC_GI",
            "INCLUDE_GI",
            "ENABLE_MONO_BDWGC",
            "PLATFORM_SUPPORTS_MONO",
            "RENDER_SOFTWARE_CURSOR",
            "INCLUDE_PUBNUB",
            "ENABLE_VIDEO",
            "ENABLE_PACKMAN",
            "ENABLE_CUSTOM_RENDER_TEXTURE",
            "ENABLE_LOCALIZATION",
            "PLATFORM_STANDALONE_WIN",
            "PLATFORM_STANDALONE",
            "UNITY_STANDALONE_WIN",
            "UNITY_STANDALONE",
            "ENABLE_SUBSTANCE",
            "ENABLE_RUNTIME_GI",
            "ENABLE_MOVIES",
            "ENABLE_NETWORK",
            "ENABLE_CRUNCH_TEXTURE_COMPRESSION",
            "ENABLE_UNITYWEBREQUEST",
            "ENABLE_CLOUD_SERVICES",
            "ENABLE_CLOUD_SERVICES_ANALYTICS",
            "ENABLE_CLOUD_SERVICES_PURCHASING",
            "ENABLE_CLOUD_SERVICES_CRASH_REPORTING",
            "ENABLE_OUT_OF_PROCESS_CRASH_HANDLER",
            "ENABLE_EVENT_QUEUE",
            "ENABLE_CLUSTER_SYNC",
            "ENABLE_CLUSTERINPUT",
            "ENABLE_VR",
            "ENABLE_AR",
            "ENABLE_WEBSOCKET_HOST",
            "ENABLE_MONO",
            "NET_4_6",
            "ENABLE_PROFILER",
            "DEBUG",
            "TRACE",
            "UNITY_ASSERTIONS",
            "UNITY_EDITOR",
            "UNITY_EDITOR_64",
            "UNITY_EDITOR_WIN",
            "ENABLE_UNITY_COLLECTIONS_CHECKS",
            "ENABLE_BURST_AOT",
            "UNITY_TEAM_LICENSE",
            "NO_MATCH3",
            "NO_CLOUD",
            "DLL_BUILD"
        };

        private string[] _libs =
        {
            "mscorlib.dll", "System.dll", "System.Core.dll", "System.Runtime.Serialization.dll",
            "System.Xml.dll", "System.Xml.Linq.dll", "System.Numerics.dll", "System.Numerics.Vectors.dll",
            "Microsoft.Win32.Primitives.dll", "netstandard.dll", "System.AppContext.dll",
            "System.Collections.Concurrent.dll", "System.Collections.dll",
            "System.Collections.NonGeneric.dll", "System.Collections.Specialized.dll",
            "System.ComponentModel.Annotations.dll", "System.ComponentModel.dll",
            "System.ComponentModel.EventBasedAsync.dll", "System.ComponentModel.Primitives.dll",
            "System.ComponentModel.TypeConverter.dll", "System.Console.dll", "System.Data.Common.dll",
            "System.Diagnostics.Contracts.dll", "System.Diagnostics.Debug.dll",
            "System.Diagnostics.FileVersionInfo.dll", "System.Diagnostics.Process.dll",
            "System.Diagnostics.StackTrace.dll", "System.Diagnostics.TextWriterTraceListener.dll",
            "System.Diagnostics.Tools.dll", "System.Diagnostics.TraceSource.dll",
            "System.Drawing.Primitives.dll", "System.Dynamic.Runtime.dll",
            "System.Globalization.Calendars.dll", "System.Globalization.dll",
            "System.Globalization.Extensions.dll", "System.IO.Compression.ZipFile.dll", "System.IO.dll",
            "System.IO.FileSystem.dll", "System.IO.FileSystem.DriveInfo.dll",
            "System.IO.FileSystem.Primitives.dll", "System.IO.FileSystem.Watcher.dll",
            "System.IO.IsolatedStorage.dll", "System.IO.MemoryMappedFiles.dll", "System.IO.Pipes.dll",
            "System.IO.UnmanagedMemoryStream.dll", "System.Linq.dll", "System.Linq.Expressions.dll",
            "System.Linq.Parallel.dll", "System.Linq.Queryable.dll", "System.Net.Http.Rtc.dll",
            "System.Net.NameResolution.dll", "System.Net.NetworkInformation.dll", "System.Net.Ping.dll",
            "System.Net.Primitives.dll", "System.Net.Requests.dll", "System.Net.Security.dll",
            "System.Net.Sockets.dll", "System.Net.WebHeaderCollection.dll",
            "System.Net.WebSockets.Client.dll", "System.Net.WebSockets.dll", "System.ObjectModel.dll",
            "System.Reflection.dll", "System.Reflection.Emit.dll",
            "System.Reflection.Emit.ILGeneration.dll", "System.Reflection.Emit.Lightweight.dll",
            "System.Reflection.Extensions.dll", "System.Reflection.Primitives.dll",
            "System.Resources.Reader.dll", "System.Resources.ResourceManager.dll",
            "System.Resources.Writer.dll", "System.Runtime.CompilerServices.VisualC.dll",
            "System.Runtime.dll", "System.Runtime.Extensions.dll", "System.Runtime.Handles.dll",
            "System.Runtime.InteropServices.dll", "System.Runtime.InteropServices.RuntimeInformation.dll",
            "System.Runtime.InteropServices.WindowsRuntime.dll", "System.Runtime.Numerics.dll",
            "System.Runtime.Serialization.Formatters.dll", "System.Runtime.Serialization.Json.dll",
            "System.Runtime.Serialization.Primitives.dll", "System.Runtime.Serialization.Xml.dll",
            "System.Security.Claims.dll", "System.Security.Cryptography.Algorithms.dll",
            "System.Security.Cryptography.Csp.dll", "System.Security.Cryptography.Encoding.dll",
            "System.Security.Cryptography.Primitives.dll",
            "System.Security.Cryptography.X509Certificates.dll", "System.Security.Principal.dll",
            "System.Security.SecureString.dll", "System.ServiceModel.Duplex.dll",
            "System.ServiceModel.Http.dll", "System.ServiceModel.NetTcp.dll",
            "System.ServiceModel.Primitives.dll", "System.ServiceModel.Security.dll",
            "System.Text.Encoding.dll", "System.Text.Encoding.Extensions.dll",
            "System.Text.RegularExpressions.dll", "System.Threading.dll", "System.Threading.Overlapped.dll",
            "System.Threading.Tasks.dll", "System.Threading.Tasks.Parallel.dll",
            "System.Threading.Thread.dll", "System.Threading.ThreadPool.dll", "System.Threading.Timer.dll",
            "System.ValueTuple.dll", "System.Xml.ReaderWriter.dll", "System.Xml.XDocument.dll",
            "System.Xml.XmlDocument.dll", "System.Xml.XmlSerializer.dll", "System.Xml.XPath.dll",
            "System.Xml.XPath.XDocument.dll", "UnityEngine.dll", "UnityEditor.dll"
        };

        private string[] _pluginLibs =
        {
            "ThirdParty/Newtonsoft.Json/Newtonsoft.Json.dll",
            "ThirdParty/FacebookSDK/Plugins/Facebook.Unity.dll"
        };

        private string[] _extensionLibs =
        {
            "GUISystem/UnityEngine.UI.dll",
            "GUISystem/Editor/UnityEditor.UI.dll",
            "Timeline/RuntimeEditor/UnityEngine.Timeline.dll",
            "Timeline/Editor/UnityEditor.Timeline.dll",
            "GUISystem/UnityEngine.UI.dll",
            "GUISystem/Editor/UnityEditor.UI.dll",
            "TestRunner/net35/unity-custom/nunit.framework.dll",
            "TestRunner/UnityEngine.TestRunner.dll",
            "TestRunner/Editor/UnityEditor.TestRunner.dll"
        };

        public void Build(string outPath, string[] packagesToCompile)
        {
            string mscPath = Path.Combine(
                    EditorApplication.applicationContentsPath,
                    "MonoBleedingEdge",
                    "bin",
                    "mcs"
                );

            #if UNITY_EDITOR_WIN
            {
                mscPath += ".bat";
            }
            #endif

            string paramsFile = FileUtils.GetTempFileName();

            using (FileStream stream = File.Create(paramsFile))
            {
                using (var streamWriter = new StreamWriter(stream))
                {
                    streamWriter.WriteLine("-target:library");
                    streamWriter.WriteLine("-langversion:6");
                    streamWriter.WriteLine("-optimize+");
                    streamWriter.WriteLine("-debug-");
                    streamWriter.WriteLine("-nostdlib");
                    streamWriter.WriteLine("-out:" + outPath);
                    streamWriter.WriteLine("-nowarn:0169");
                    streamWriter.WriteLine("-d:" + string.Join(";", _defines));

                    foreach (string folder in GetReferenceFolders())
                    {
                        streamWriter.WriteLine($"-lib:\"{folder}\"");
                    }

                    foreach (string reference in GetReferences())
                    {
                        streamWriter.WriteLine("-r:" + reference);
                    }

                    var dir = new DirectoryInfo(".");

                    foreach (FileInfo file in dir.EnumerateFiles(_scriptsToCompile, _scriptsToExclude))
                    {
                        streamWriter.WriteLine("\"" + file.FullName + "\"");
                    }

                    PackageCollection packages = RequestPackages();

                    foreach (string packageName in packagesToCompile)
                    {
                        PackageInfo package = packages.FirstOrDefault(p => p.name == packageName);
                        if (package == null)
                        {
                            throw new Exception("Can't find required package " + packageName);
                        }

                        var packageDir = new DirectoryInfo(package.resolvedPath);
                        foreach (FileInfo file in packageDir.EnumerateFiles(new[] { ".*\\.cs$" }))
                        {
                            streamWriter.WriteLine("\"" + file.FullName + "\"");
                        }
                    }
                }
            }

            FileUtils.ExecuteProcess(mscPath, $"@{paramsFile} @Assets/mcs.rsp");
            UnityEngine.Debug.Log("DLL building succeed.");
        }

        public IEnumerable<string> GetReferenceFolders()
        {
            var folders = new HashSet<string>();

            string monoPath = Path.Combine(
                    EditorApplication.applicationContentsPath,
                    "MonoBleedingEdge",
                    "lib",
                    "mono",
                    "4.7.1-api"
                );

            folders.Add(monoPath);
            folders.Add(Path.Combine(monoPath, "Facades"));

            folders.Add(Path.Combine(EditorApplication.applicationContentsPath, "Managed"));

            folders.UnionWith(GetAdditionalLibraries().Select(Path.GetDirectoryName));

            return folders;
        }

        private IEnumerable<string> GetReferences()
        {
            return _libs.Concat(
                    GetAdditionalLibraries().Select(Path.GetFileName)
                );
        }

        private IEnumerable<string> GetAdditionalLibraries()
        {
            // Add plugins
            IEnumerable<string> libraries = _pluginLibs.Select(
                    lib => Path.GetFullPath(Path.Combine("Assets", "Plugins", lib))
                );

            string extensionsPath = Path.Combine(
                    EditorApplication.applicationContentsPath,
                    "UnityExtensions",
                    "Unity"
                );

            // Add extensions
            libraries = libraries.Concat(
                    _extensionLibs.Select(lib => Path.Combine(extensionsPath, lib))
                );

            // Add packages
            libraries = libraries.Concat(FindPackageLibraries());

            return libraries;
        }

        private PackageCollection RequestPackages()
        {
            ListRequest request = Client.List();
            while (!request.IsCompleted)
            {
            }

            return request.Result;
        }

        private IEnumerable<string> FindPackageLibraries()
        {
            foreach (PackageInfo package in RequestPackages())
            {
                var packageDir = new DirectoryInfo(package.resolvedPath);
                foreach (FileInfo file in packageDir.EnumerateFiles(new[] { ".*\\.dll$" }))
                {
                    string fullName = file.FullName;

                    string databaseName = Path.Combine("Packages", package.name, FileUtils.GetRelativePath(fullName, packageDir.FullName));
                    databaseName = databaseName.Replace("\\", "/");

                    var importer = AssetImporter.GetAtPath(databaseName) as PluginImporter;
                    if (importer == null)
                    {
                        UnityEngine.Debug.LogError("Can't obtain importer at " + databaseName);
                        continue;
                    }

                    if (!importer.isNativePlugin)
                    {
                        yield return fullName;
                    }
                }
            }
        }
    }
}

#endif