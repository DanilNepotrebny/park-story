﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if !DLL_BUILD

using System.Collections.Generic;
using System.IO;
using DreamTeam.Editor.Utils;
using UnityEditor;

namespace DreamTeam.Editor.DllProjectBuilding
{
    public static class ProjectUnpacker
    {
        private static string[] _includeList =
        {
            "Assets/.*"
        };

        private static string[] _excludeList =
        {
            "Assets/Plugins/.*",
            "Assets/Scripts/.*",
            "Assets/mcs\\.rsp$",
            "Assets/mcs\\.rsp\\.meta$",
            ".*/Match3/.+",
            ".*/Match3\\.meta$",
            ".*/Cloud/.+",
            ".*/Cloud\\.meta$",
            "Assets/Scenes/Levels/.+",
            "Assets/Scenes/Levels\\.meta$",
            "Assets/Prefabs/GlobalContext\\.prefab$",
            "Assets/Prefabs/GlobalContext\\.prefab\\.meta$"
        };

        [MenuItem("Tools/DLL/Unpack project")]
        private static void UnpackFromDll()
        {
            if (GitUtils.HasChanges())
            {
                UnityEngine.Debug.LogError("Please, commit or revert your changes before unpacking.");
                return;
            }

            string dstDirPath;
            if (!FileUtils.ShowOpenFolderDialog(out dstDirPath))
            {
                return;
            }

            string revision = FileUtils.ReadText(Path.Combine(dstDirPath, ProjectPacker.RevisionFileName));
            GitUtils.CheckoutBranch("outsource", revision);

            var srcDir = new DirectoryInfo(".");
            srcDir.DeleteFiles(_includeList, _excludeList);

            var dstDir = new DirectoryInfo(dstDirPath);
            dstDir.CopyFiles(srcDir.FullName, _includeList, _excludeList);

            var metaConverter = new ScriptsMetadataConverter();

            List<ScriptMetadata> srcMetadata = metaConverter.ReadScriptMetadata(Path.Combine(dstDirPath, ProjectPacker.MetadataFileName));
            UnityEngine.Debug.Log("Loaded metadata: " + srcMetadata.Count);

            foreach (ScriptMetadata meta in srcMetadata)
            {
                meta.SwapData();
            }

            metaConverter.ConvertAssets(
                    srcMetadata,
                    new IMetadataConverter[] { new ScriptsGuidConverter(), new ScriptsQualifiedNameConverter() }
                );

            UnityEngine.Debug.Log("Project unpacked.");
        }
    }
}

#endif