﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if !DLL_BUILD

using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using DreamTeam.Editor.Utils;

namespace DreamTeam.Editor.DllProjectBuilding
{
    public class DllObfuscator
    {
        public void Obfuscate(string dllPath, IEnumerable<string> referenceFolders)
        {
            #if UNITY_EDITOR_WIN
            {
                string dllFolder = Path.GetDirectoryName(dllPath);

                XElement project = XElement.Load("Tools/ConfuserEx/template.crproj");
                project.SetAttributeValue("baseDir", dllFolder);
                project.SetAttributeValue("outputDir", dllFolder);

                var module = new XElement("module");
                module.SetAttributeValue("path", Path.GetFileName(dllPath));
                project.Add(module);

                foreach (string folder in referenceFolders)
                {
                    var probe = new XElement("probePath", folder);
                    project.Add(probe);
                }

                string projectPath = FileUtils.GetTempFileName("crproj");
                project.Save(projectPath);

                string confuserPath = Path.Combine(new DirectoryInfo(".").FullName, "Tools/ConfuserEx/Confuser.CLI.exe");

                FileUtils.ExecuteProcess(confuserPath, "-n " + projectPath);
                UnityEngine.Debug.Log("Obfuscation completed.");
            }
            #else
            {
                UnityEngine.Debug.LogWarning("Obfuscation is not supported on this platform.");
            }
            #endif
        }
    }
}

#endif