﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DreamTeam.Utils;

namespace DreamTeam.Editor.DllProjectBuilding
{
    public class ScriptsGuidConverter : IMetadataConverter
    {
        private Regex _regex = new Regex("{fileID: ([0-9\\-]+), guid: ([A-Za-z0-9]+).*}", RegexOptions.Multiline);

        public bool ConvertAsset(StringBuilder builder, List<ScriptMetadata> metadata)
        {
            bool wasChanged = false;

            Match[] guidMatches = _regex.Matches(builder.ToString()).OfType<Match>().Reverse().ToArray();
            foreach (Match match in guidMatches)
            {
                Group fileIdGroup = match.Groups[1];
                Group guidGroup = match.Groups[2];

                long fileId = long.Parse(fileIdGroup.Value);
                string guid = guidGroup.Value;

                ScriptMetadata meta = metadata.Find(m => m.OriginalFileId == fileId && m.OriginalGuid == guid);
                if (meta != null)
                {
                    wasChanged = true;

                    builder.ReplaceGroup(guidGroup, meta.DllGuid);
                    builder.ReplaceGroup(fileIdGroup, meta.DllFileId.ToString());
                }
            }

            return wasChanged;
        }
    }
}