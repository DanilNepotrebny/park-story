﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.IO;
using System.Reflection;
using DreamTeam.Editor.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEditor;
using UnityEngine.Assertions;

namespace DreamTeam.Editor.DllProjectBuilding
{
    [Obfuscation(Exclude = false, ApplyToMembers = false, Feature = "-rename")]
    public static class ProjectPacker
    {
        public const string MetadataFileName = ".metadata";
        public const string RevisionFileName = ".revision";

        private static string[] _includeList =
        {
            "Assets/.*",
            "ProjectSettings/.*",
            "Packages/.*"
        };

        private static string[] _excludeList =
        {
            "Assets/mcs\\.rsp$",
            "Assets/mcs\\.rsp\\.meta$",
            ".*\\.cs$",
            ".*\\.cs\\.meta$",
            ".*\\.java$",
            ".*\\.java\\.meta$",
            ".*\\.h$",
            ".*\\.h\\.meta$",
            ".*\\.mm?$",
            ".*\\.mm?\\.meta$",
            ".*\\.c$",
            ".*\\.c\\.meta$",
            ".*\\.[ch]pp$",
            ".*\\.[ch]pp\\.meta$",
            ".*/Match3/.+",
            ".*/Match3\\.meta$",
            ".*/Cloud/.+",
            ".*/Cloud\\.meta$",
            "Assets/Scenes/Levels/.+",
            "Assets/Scenes/Levels\\.meta$"
        };

        private static string[] _packagesToCompile =
        {
            "com.unity.cinemachine",
            "com.unity.textmeshpro"
        };

        private const string _assemblyName = "DT";

        #if !DLL_BUILD

        [MenuItem("Tools/DLL/Pack project")]
        private static void PackIntoDll()
        {
            if (GitUtils.HasChanges())
            {
                UnityEngine.Debug.LogError("Please, commit or revert your changes before packing.");
                return;
            }

            string dstDirPath;
            if (!FileUtils.ShowOpenFolderDialog(out dstDirPath))
            {
                return;
            }

            var srcDir = new DirectoryInfo(".");
            var dstDir = new DirectoryInfo(dstDirPath);

            dstDir.DeleteFiles(_includeList);
            UnityEngine.Debug.Log("Cleared " + dstDirPath);

            srcDir.CopyFiles(dstDirPath, _includeList, _excludeList);
            UnityEngine.Debug.Log("Project copied");

            BuildDll(dstDirPath);

            RemoveCompiledPackages(dstDirPath);
            UnityEngine.Debug.Log("Removed compiled packages");

            var metaConverter = new ScriptsMetadataConverter();
            metaConverter.WriteScriptMetadata(
                    Path.Combine(dstDirPath, MetadataFileName),
                    metaConverter.CollectScriptMetadata()
                );

            UnityEngine.Debug.Log("Metadata written");

            FileUtils.WriteText(Path.Combine(dstDirPath, RevisionFileName), GitUtils.GetHead());

            string parameters =
                $"-batchmode -nographics -projectPath {dstDirPath} " +
                $"-executeMethod {typeof(ProjectPacker).FullName}.{nameof(FinishPacking)} " +
                "-silent-crashes -quit";

            FileUtils.ExecuteProcess(EditorApplication.applicationPath, parameters);
            UnityEngine.Debug.Log("Packing completed");
        }

        [MenuItem("Tools/DLL/Build DLL")]
        private static void BuildDll()
        {
            string dstDirPath;
            if (FileUtils.ShowOpenFolderDialog(out dstDirPath))
            {
                BuildDll(dstDirPath);
            }
        }

        private static void BuildDll(string projectPath)
        {
            string dllPath = Path.Combine(projectPath, "Assets", "Plugins", _assemblyName + ".dll");

            var dllBuilder = new DllBuilder();
            dllBuilder.Build(dllPath, _packagesToCompile);

            var dllObfuscator = new DllObfuscator();
            dllObfuscator.Obfuscate(dllPath, dllBuilder.GetReferenceFolders());
        }

        private static void RemoveCompiledPackages(string projectPath)
        {
            string path = Path.Combine(projectPath, "Packages", "manifest.json");

            JToken manifest;

            using (FileStream stream = File.OpenRead(path))
            {
                using (var streamReader = new StreamReader(stream))
                {
                    using (var jsonReader = new JsonTextReader(streamReader))
                    {
                        manifest = JToken.ReadFrom(jsonReader);
                    }
                }
            }

            var dependencies = manifest["dependencies"] as JObject;
            foreach (string packageName in _packagesToCompile)
            {
                Assert.IsTrue(dependencies.ContainsKey(packageName));
                dependencies.Remove(packageName);
            }

            using (FileStream stream = File.Create(path))
            {
                using (StreamWriter streamWriter = new StreamWriter(stream))
                {
                    using (JsonTextWriter jsonWriter = new JsonTextWriter(streamWriter))
                    {
                        jsonWriter.Formatting = Formatting.Indented;
                        manifest.WriteTo(jsonWriter);
                    }
                }
            }
        }

        #endif

        [Obfuscation(Exclude = false, Feature = "-rename")]
        private static void FinishPacking()
        {
            var metaConverter = new ScriptsMetadataConverter();

            List<ScriptMetadata> srcMetadata = metaConverter.ReadScriptMetadata(MetadataFileName);
            UnityEngine.Debug.Log("Loaded metadata: " + srcMetadata.Count);

            List<ScriptMetadata> dstMetadata = metaConverter.CollectScriptMetadata();
            UnityEngine.Debug.Log("Collected metadata: " + dstMetadata.Count);

            metaConverter.FillDllData(srcMetadata, dstMetadata);
            metaConverter.WriteScriptMetadata(MetadataFileName, srcMetadata);
            metaConverter.ApplyExecutionOrder(srcMetadata);
            metaConverter.ConvertAssets(
                    srcMetadata,
                    new IMetadataConverter[] { new ScriptsGuidConverter(), new ScriptsQualifiedNameConverter() }
                );
        }
    }
}