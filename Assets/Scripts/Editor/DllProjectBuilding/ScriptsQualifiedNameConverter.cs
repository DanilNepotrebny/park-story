﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DreamTeam.Utils;

namespace DreamTeam.Editor.DllProjectBuilding
{
    public class ScriptsQualifiedNameConverter : IMetadataConverter
    {
        private const string _fullNameId = "FullName";
        private const string _qualifiedNameId = "QualifiedName";
        private const string _indentId = "Indent";
        private static string _regexPattern =
            $"^(?<{_indentId}>[\\s-]*)[\\w]+: (?<{_qualifiedNameId}>(?<{_fullNameId}>[\\w+.-]+),\\s*" +
            "[\\w-]+,\\s*" +
            "Version=[0-9.]+,\\s*" +
            "Culture=.+?,\\s*" +
            "PublicKeyToken=[A-Za-z0-9]+)\\s*$";

        private const int _maxLength = 80;
        private const int _indendSize = 2;

        private Regex _regex = new Regex(_regexPattern, RegexOptions.Multiline);

        public bool ConvertAsset(StringBuilder builder, List<ScriptMetadata> metadata)
        {
            bool wasChanged = false;

            Match[] classMatches = _regex.Matches(builder.ToString()).OfType<Match>().Reverse().ToArray();
            foreach (Match match in classMatches)
            {
                string fullName = match.Groups[_fullNameId].Value;
                Type type = FindType(fullName);
                if (type == null)
                {
                    UnityEngine.Debug.LogWarning("Can't find type: " + fullName);
                    continue;
                }

                string newStr = ChangeQualifiedName(match.Groups[0].ToString(), type.AssemblyQualifiedName);
                builder.ReplaceGroup(match.Groups[0], newStr);
                wasChanged = true;
            }

            return wasChanged;
        }

        private string ChangeQualifiedName(string str, string newQualifiedName)
        {
            var builder = new StringBuilder(str);
            Match match = _regex.Match(str);

            builder.ReplaceGroup(match.Groups[_qualifiedNameId], newQualifiedName);

            // It seems that unity YAML serializer has a maximum string line length. Words over the limit are
            // moved to the next line with indent. To produce no changes when unpacking from DLL project
            // we imitate this behaviour after name has changed.

            string indent = new string(' ', match.Groups[_indentId].Length + _indendSize);

            int currentLength = 0;
            for (int i = 0; i < builder.Length; i++)
            {
                if (currentLength > _maxLength && char.IsWhiteSpace(builder[i]))
                {
                    builder.Remove(i, 1);
                    builder.Insert(i, "\n" + indent);

                    currentLength = 0;
                }
                else
                {
                    currentLength++;
                }
            }

            return builder.ToString();
        }

        private Type FindType(string fullName)
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(a => a.GetTypes())
                .FirstOrDefault(t => t.FullName == fullName);
        }
    }
}