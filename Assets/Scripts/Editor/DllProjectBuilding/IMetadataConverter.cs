﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Text;

namespace DreamTeam.Editor.DllProjectBuilding
{
    public interface IMetadataConverter
    {
        bool ConvertAsset(StringBuilder builder, List<ScriptMetadata> metadata);
    }
}