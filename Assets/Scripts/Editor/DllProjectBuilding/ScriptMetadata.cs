﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.DllProjectBuilding
{
    public class ScriptMetadata
    {
        public string FullName { get; private set; }

        public string OriginalGuid { get; private set; }
        public long OriginalFileId { get; private set; }

        public string DllGuid { get; private set; }
        public long DllFileId { get; private set; }

        private int _executionOrder;
        private MonoScript _script;

        public static ScriptMetadata CreateFromScript(MonoScript script)
        {
            string guid;
            long fileId;

            if (!AssetDatabase.TryGetGUIDAndLocalFileIdentifier(script, out guid, out fileId))
            {
                UnityEngine.Debug.LogError($"Can't get guid or file id from {script.name}", script);
                return null;
            }

            Type type = script.GetClass();

            if (type != null &&
                !type.IsAbstract &&
                !type.IsSubclassOf(typeof(UnityEditor.Editor)) &&
                !type.IsSubclassOf(typeof(EditorWindow)) &&
                (type.IsSubclassOf(typeof(ScriptableObject)) ||
                type.IsSubclassOf(typeof(Component))))
            {
                return new ScriptMetadata
                {
                    FullName = type.FullName,
                    OriginalGuid = guid,
                    OriginalFileId = fileId,
                    _executionOrder = MonoImporter.GetExecutionOrder(script),
                    _script = script
                };
            }

            return null;
        }

        public void FillDllData(ScriptMetadata other)
        {
            DllFileId = other.OriginalFileId;
            DllGuid = other.OriginalGuid;
            _script = other._script;
        }

        public void SwapData()
        {
            string tmpGuid = DllGuid;
            DllGuid = OriginalGuid;
            OriginalGuid = tmpGuid;

            long tmpId = DllFileId;
            DllFileId = OriginalFileId;
            OriginalFileId = tmpId;
        }

        public bool IsSame(ScriptMetadata other)
        {
            return FullName == other.FullName;
        }

        public bool IsRedundant()
        {
            return OriginalFileId == DllFileId && OriginalGuid == DllGuid;
        }

        public bool HasDllData()
        {
            return !string.IsNullOrEmpty(DllGuid) && DllFileId != 0;
        }

        public void ApplyExecutionOrder()
        {
            if (_executionOrder != 0)
            {
                MonoImporter.SetExecutionOrder(_script, _executionOrder);
            }
        }

        public static ScriptMetadata ReadFromFile(StreamReader reader)
        {
            string[] tokens = reader.ReadLine().Split(' ');
            ScriptMetadata meta = new ScriptMetadata
            {
                FullName = tokens[0],
                _executionOrder = int.Parse(tokens[1]),
                OriginalGuid = tokens[2],
                OriginalFileId = long.Parse(tokens[3])
            };

            if (tokens.Length > 4)
            {
                meta.DllGuid = tokens[4];
                meta.DllFileId = long.Parse(tokens[5]);
            }

            return meta;
        }

        public void WriteToFile(StreamWriter writer)
        {
            string format = "{0} {1} {2} {3}";
            if (!string.IsNullOrEmpty(DllGuid))
            {
                format += " {4} {5}";
            }

            writer.WriteLine(format, FullName, _executionOrder, OriginalGuid, OriginalFileId, DllGuid, DllFileId);
        }
    }
}