﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.IO;
using System.Text;
using DreamTeam.Editor.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.DllProjectBuilding
{
    public class ScriptsMetadataConverter
    {
        private string[] _assetsToConvert =
        {
            "Assets/.*\\.anim$",
            "Assets/.*\\.prefab$",
            "Assets/.*\\.asset$",
            "Assets/.*\\.unity$",
            "Assets/.*\\.playable$",
            "Assets/.*\\.controller$"
        };

        public List<ScriptMetadata> CollectScriptMetadata()
        {
            var metadata = new List<ScriptMetadata>();

            foreach (var script in Resources.FindObjectsOfTypeAll<MonoScript>())
            {
                ScriptMetadata meta = ScriptMetadata.CreateFromScript(script);
                if (meta == null)
                {
                    continue;
                }

                if (metadata.Find(m => m.IsSame(meta)) != null)
                {
                    // For unknown reason Unity has classes with identical names but different guids. It seems that
                    // the first one of those is used when serializing assets, so we skip duplicates.
                    continue;
                }

                metadata.Add(meta);
            }

            return metadata;
        }

        public List<ScriptMetadata> ReadScriptMetadata(string path)
        {
            var result = new List<ScriptMetadata>();

            using (var reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    ScriptMetadata meta = ScriptMetadata.ReadFromFile(reader);
                    result.Add(meta);
                }
            }

            return result;
        }

        public void WriteScriptMetadata(string path, List<ScriptMetadata> metadata)
        {
            using (var writer = new StreamWriter(path))
            {
                foreach (ScriptMetadata meta in metadata)
                {
                    meta.WriteToFile(writer);
                }
            }
        }

        public void FillDllData(List<ScriptMetadata> srcMetadata, List<ScriptMetadata> dstMetadata)
        {
            foreach (ScriptMetadata srcMeta in srcMetadata)
            {
                ScriptMetadata dstMeta = dstMetadata.Find(m => m.IsSame(srcMeta));
                if (dstMeta == null)
                {
                    UnityEngine.Debug.LogWarning("Can't find meta for " + srcMeta.FullName);
                    continue;
                }

                srcMeta.FillDllData(dstMeta);
            }

            srcMetadata.RemoveAll(m => m.IsRedundant() || !m.HasDllData());
        }

        public void ApplyExecutionOrder(List<ScriptMetadata> metadata)
        {
            AssetDatabase.StartAssetEditing();

            foreach (ScriptMetadata meta in metadata)
            {
                meta.ApplyExecutionOrder();
            }

            AssetDatabase.StopAssetEditing();
        }

        public void ConvertAssets(List<ScriptMetadata> metadata, IEnumerable<IMetadataConverter> converters)
        {
            var dir = new DirectoryInfo(".");
            var builder = new StringBuilder();

            foreach (FileInfo file in dir.EnumerateFiles(_assetsToConvert))
            {
                builder.Clear();

                builder.Append(FileUtils.ReadText(file.FullName));

                bool wasChanged = false;

                foreach (IMetadataConverter converter in converters)
                {
                    if (converter.ConvertAsset(builder, metadata))
                    {
                        wasChanged = true;
                    }
                }

                if (wasChanged)
                {
                    FileUtils.WriteText(file.FullName, builder.ToString());
                    UnityEngine.Debug.Log("Converted metadata: " + file.FullName);
                }
            }
        }
    }
}