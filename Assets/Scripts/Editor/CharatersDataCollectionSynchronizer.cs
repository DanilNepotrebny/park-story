﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEditor;

namespace DreamTeam.Editor
{
    public class CharatersDataCollectionSynchronizer : AssetPostprocessor
    {
        private static CharactersDataCollection _syncTarget = null;

        private static void OnPostprocessAllAssets(
            string[] importedAssets,
            string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            if (deletedAssets.Length > 0)
            {
                ModifySyncTarget(target => target.ClearDeletedAssets());
            }

            foreach (string path in importedAssets)
            {
                var character = AssetDatabase.LoadAssetAtPath<CharacterData>(path);
                if (character != null)
                {
                    ModifySyncTarget(target => target.Add(character));
                }
            }
        }

        private static CharactersDataCollection GetSyncTarget()
        {
            if (_syncTarget == null)
            {
                string[] guids = AssetDatabase.FindAssets($"t:{typeof(CharactersDataCollection)}");
                if (guids.Length > 0)
                {
                    string path = AssetDatabase.GUIDToAssetPath(guids[0]);
                    _syncTarget = AssetDatabase.LoadAssetAtPath<CharactersDataCollection>(path);
                }
            }

            return _syncTarget;
        }

        private static void ModifySyncTarget(Action<CharactersDataCollection> action)
        {
            if (GetSyncTarget() != null)
            {
                action.Invoke(GetSyncTarget());
                EditorUtility.SetDirty(GetSyncTarget());
            }
        }
    }
}