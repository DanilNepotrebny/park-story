﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR && UNITY_IOS && IAP

using UnityEditor;
using UnityEditor.iOS.Xcode;

namespace DreamTeam.Editor
{
    [InitializeOnLoad]
    public class IAPBuildConfigurator
    {
        static IAPBuildConfigurator()
        {
            PBXProjectPostProcessor.AddCapabilitiesProcessor(ProcessCapabilities);
        }

        private static void ProcessCapabilities(ProjectCapabilityManager capabilitiesManager)
        {
            capabilitiesManager.AddInAppPurchase();
        }
    }
}

#endif