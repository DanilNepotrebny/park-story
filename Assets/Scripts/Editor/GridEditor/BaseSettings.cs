﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEngine;

namespace DreamTeam.Editor.GridEditor
{
    /// <summary>
    /// Asset for base grid editor settings
    /// </summary>
    /// <inheritdoc />
    public class BaseSettings : ScriptableObject
    {
        /// <summary>
        /// Toggle for drawing grid in Scene View
        /// </summary>
        public bool isGridVisible = false;

        /// <summary>
        /// Color for grid drawing
        /// </summary>
        public Color gridColor = Color.white;

        /// <summary>
        /// Delimiter drawing period in cells. Set 0 for disable delimiter drawing
        /// </summary>
        [Min(0)] public int gridDelimiterSize = 0;

        /// <summary>
        /// Color for grid delimiter drawing
        /// </summary>
        public Color gridDelimiterColor = Color.gray;
    }
}