﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using DreamTeam.Editor.AssetBrushes;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.GridEditor
{
    public abstract class BaseGridEditorWindow : EditorWindow
    {
        [SerializeField] private bool _isSettingsVisible;
        [SerializeField] private AssetBrushPanel _assetBrushPanel;

        private BaseBrush[] _brushes;
        private string _errorMessage;

        protected AssetBrushPanel AssetBrushPanel => _assetBrushPanel;

        /// <summary>
        /// Finds settings in AssetDatabase.
        /// </summary>
        protected abstract BaseSettings GetBaseSettings();

        /// <summary>
        /// Creates brushes to be used in the editor.
        /// </summary>
        protected abstract BaseBrush[] CreateAssetBrushes();

        public virtual void OnEnable()
        {
            SceneView.onSceneGUIDelegate += OnSceneGUI;

            InitAssetBrushPanel();
        }

        public virtual void OnDisable()
        {
            SceneView.onSceneGUIDelegate -= OnSceneGUI;

            ShutdownAssetBrushPanel();
        }

        public virtual void OnGUI()
        {
            if (EditorApplication.isPlaying)
            {
                EditorGUILayout.LabelField("Disable play mode to edit");
                return;
            }

            if (HasError())
            {
                EditorGUILayout.LabelField(_errorMessage);
                return;
            }

            BaseSettings settings = GetBaseSettings();

            _isSettingsVisible = EditorGUILayout.Foldout(_isSettingsVisible, "Settings");
            if (_isSettingsVisible)
            {
                EditorGUI.indentLevel++;

                UnityEditor.Editor settingsEditor = UnityEditor.Editor.CreateEditor(settings);
                settingsEditor.DrawDefaultInspector();
                EditorGUI.indentLevel--;

                OnSettingsGUI();
            }

            EditorGUILayout.Separator();

            EditorGUILayout.Separator();

            _assetBrushPanel?.OnGUI();

            SceneView.RepaintAll();
        }

        protected abstract void CalculateSceneViewGridBounds(out Vector2Int min, out Vector2Int max);

        protected virtual void DrawGridLine(Vector3 begin, Vector3 end)
        {
            Handles.DrawLine(begin, end);
        }

        protected void OnHierarchyChange()
        {
            _assetBrushPanel?.OnHierarchyChange();
        }

        protected void ResetAssetBrushPanel()
        {
            ShutdownAssetBrushPanel();
            InitAssetBrushPanel();
        }

        protected virtual void OnSettingsGUI()
        {
        }

        protected abstract Vector2 WorldToGridF(Vector3 worldPosition);
        protected abstract Vector3 GridToWorldF(Vector2 gridPosition);

        /// <summary>
        /// Disables editor functionality with specified error. Pass null to make it enabled. Error message
        /// will be drawn instead of window content.
        /// </summary>
        protected void SetError(string message)
        {
            _errorMessage = message;
        }

        private bool HasError()
        {
            return !string.IsNullOrEmpty(_errorMessage);
        }

        private void InitAssetBrushPanel()
        {
            _brushes = CreateAssetBrushes();
            _assetBrushPanel = new AssetBrushPanel(_brushes);
        }

        private void ShutdownAssetBrushPanel()
        {
            _assetBrushPanel?.Dispose();

            if (_brushes != null)
            {
                foreach (BaseBrush brush in _brushes)
                {
                    (brush as IDisposable)?.Dispose();
                }

                _brushes = null;
            }
        }

        private void DrawGrid(Vector2Int minGrid, Vector2Int maxGrid, [NotNull] BaseSettings settings)
        {
            int delimSize = settings.gridDelimiterSize;
            Color color = settings.gridColor;
            Color delimiColor = settings.gridDelimiterColor;

            for (int x = minGrid.x; x <= maxGrid.x; ++x)
            {
                Handles.color = (delimSize == 0 || x % delimSize == 0) ? color : delimiColor;

                Vector3 lineBegin = GridToWorldF(new Vector2(x - 0.5f, minGrid.y - 0.5f));
                Vector3 lineEnd = GridToWorldF(new Vector2(x - 0.5f, maxGrid.y - 0.5f));

                DrawGridLine(lineBegin, lineEnd);
            }

            for (int y = minGrid.y; y <= maxGrid.y; ++y)
            {
                Handles.color = (delimSize == 0 || y % delimSize == 0) ? color : delimiColor;

                Vector3 line1 = GridToWorldF(new Vector2(minGrid.x - 0.5f, y - 0.5f));
                Vector3 line2 = GridToWorldF(new Vector2(maxGrid.x - 0.5f, y - 0.5f));

                DrawGridLine(line1, line2);
            }
        }

        private void OnSceneGUI(SceneView aView)
        {
            if (HasError())
            {
                return;
            }

            if (Event.current != null && _assetBrushPanel != null)
            {
                _assetBrushPanel.HandleEvent(Event.current);
                if (_assetBrushPanel.IsRepaintNeeded)
                {
                    Repaint();
                }
            }

            BaseSettings settings = GetBaseSettings();
            if (settings == null || !settings.isGridVisible)
            {
                return;
            }

            Vector2Int minGrid;
            Vector2Int maxGrid;

            CalculateSceneViewGridBounds(out minGrid, out maxGrid);

            maxGrid.x++;
            maxGrid.y++;

            DrawGrid(minGrid, maxGrid, settings);
        }
    }
}