﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_IOS

using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

namespace DreamTeam.Editor.Analytics
{
    [InitializeOnLoad]
    public class DevToDevPBXPostProcessor
    {
        static DevToDevPBXPostProcessor()
        {
            PBXProjectPostProcessor.AddProjectProcessor(AddFrameworks);
        }

        private static void AddFrameworks(PBXProject project, string targetGUID)
        {
            project.AddFrameworkToProject(targetGUID, "libz.tbd", false);
            project.AddFrameworkToProject(targetGUID, "UserNotifications.framework", true);
        }
    }
}

#endif