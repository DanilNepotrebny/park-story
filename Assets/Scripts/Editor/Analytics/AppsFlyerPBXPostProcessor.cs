﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_IOS

using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

namespace DreamTeam.Editor.Analytics
{
    [InitializeOnLoad]
    public class AppsFlyerPBXPostProcessor
    {
        static AppsFlyerPBXPostProcessor()
        {
            PBXProjectPostProcessor.AddProjectProcessor(AddFrameworks);
        }

        private static void AddFrameworks(PBXProject project, string targetGUID)
        {
            project.AddFrameworkToProject(targetGUID, "Security.framework", true);
            project.AddFrameworkToProject(targetGUID, "AdSupport.framework", true);
            project.AddFrameworkToProject(targetGUID, "iAd.framework", true);
        }
    }
}

#endif