﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Routing;
using DreamTeam.Playables.CharacterMovement;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Playables.CharacterMovement
{
    [CustomEditor(typeof(CharacterManualVectorRouteClipAsset))]
    public class CharacterManualVectorRouteClipAssetDrawer : BaseTimelineEditor
    {
        private readonly Color _routeColor = Color.red;

        protected override void OnEnable()
        {
            base.OnEnable();

            SceneView.onSceneGUIDelegate += OnSceneGUI;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            SceneView.onSceneGUIDelegate -= OnSceneGUI;
        }

        private void OnSceneGUI(SceneView aView)
        {
            CharacterManualVectorRouteClipAsset clipAsset = (CharacterManualVectorRouteClipAsset)target;
            Route route = clipAsset.GetRoute(null) as Route;

            if (route == null)
            {
                return;
            }

            Handles.color = _routeColor;

            for (int i = 0; i < route.PointsCount; ++i)
            {
                EditorGUI.BeginChangeCheck();
                Vector3 newPoint = Handles.FreeMoveHandle(
                        route[i],
                        Quaternion.identity,
                        0.1f,
                        Vector3.one,
                        Handles.DotHandleCap
                    );

                if (EditorGUI.EndChangeCheck())
                {
                    route[i] = newPoint;

                    serializedObject.ApplyModifiedPropertiesWithDirtyFlag();
                    Repaint();
                }

                if (i != 0)
                {
                    Handles.DrawLine(route[i - 1], route[i]);
                }
            }

            Handles.color = Color.white;
        }
    }
}