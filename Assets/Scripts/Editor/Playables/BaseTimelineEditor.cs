﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Utils;
using UnityEditor;

namespace DreamTeam.Editor.Playables
{
    public class BaseTimelineEditor : UnityEditor.Editor
    {
        protected virtual void OnEnable()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
        }

        private void OnPlayModeStateChanged(PlayModeStateChange playModeStateChange)
        {
            // Workaround for weird unity bug: when starting play mode with selected clip
            // current clip editor will not be destroyed and will be duplicated after clip is
            // selected again.
            if (playModeStateChange == PlayModeStateChange.EnteredPlayMode)
            {
                this.Dispose();
            }
        }

        protected virtual void OnDisable()
        {
            EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
        }
    }
}