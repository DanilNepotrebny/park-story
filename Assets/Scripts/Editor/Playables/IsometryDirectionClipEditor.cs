﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Location.Isometry;
using DreamTeam.Playables.IsometryDirection;
using DreamTeam.Utils;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;

namespace DreamTeam.Editor.Playables
{
    [CustomEditor(typeof(IsometryDirectionClipAsset))]
    public class IsometryDirectionClipAssetEditor : BaseTimelineEditor
    {
        private readonly Color _startAngleColor = Color.red;
        private readonly Color _endAngleColor = Color.yellow;

        private IsometrySystem _isometrySystem;
        private IsometryDirectionClipAsset _clipAsset;
        private IsometryDirectionBehaviour _currentBehaviour;
        private IsometryMovable _boundObject;

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();

            base.OnInspectorGUI();

            SerializedProperty useCurrentAngle = serializedObject.FindProperty("_useCurrentAngle");

            EditorGUILayout.PropertyField(useCurrentAngle);
            if (!useCurrentAngle.boolValue)
            {
                DrawPropertyField("_startAngle", _startAngleColor);
            }

            DrawPropertyField("_endAngle", _endAngleColor);

            if (_boundObject != null)
            {
                EditorGUILayout.FloatField(
                    "Current angle",
                    IsometrySystem.IsometricDirectionToAngle(_boundObject.Direction));
            }

            if (EditorGUI.EndChangeCheck())
            {
                ApplyModifiedProperties();
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            SceneView.onSceneGUIDelegate += OnSceneGUIDelegate;

            _isometrySystem = SystemAssetAccessor<IsometrySystem>.Instance;
            _clipAsset = target as IsometryDirectionClipAsset;
            _currentBehaviour = _clipAsset.ClipBehaviour;
            _boundObject = _currentBehaviour.GetBoundObject<IsometryMovable>();
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            SceneView.onSceneGUIDelegate -= OnSceneGUIDelegate;
        }

        private void OnSceneGUIDelegate(SceneView sceneView)
        {
            // Workaround for bound object getting lost when exiting play mode.
            if (_isometrySystem == null || _currentBehaviour == null || _boundObject == null)
            {
                return;
            }

            EditorGUI.BeginChangeCheck();

            if (!_clipAsset.UseCurrentAngle)
            {
                Handles.color = _startAngleColor;
                DoAngleHandle("_startAngle");
            }

            Handles.color = _endAngleColor;
            DoAngleHandle("_endAngle");

            if (EditorGUI.EndChangeCheck())
            {
                ApplyModifiedProperties();
                Repaint();
            }
        }

        private void DoAngleHandle(string propertyName)
        {
            SerializedProperty property = serializedObject.FindProperty(propertyName);

            Vector3 position = _boundObject.transform.position;
            Vector3 direction = IsometrySystem.IsometricAngleToDirection(property.floatValue);

            Handles.matrix = Matrix4x4.Translate(position) *
                _isometrySystem.IsometryToScreenMatrix *
                Matrix4x4.Translate(-position);

            EditorGUI.BeginChangeCheck();

            Vector3 endPoint = Handles.FreeMoveHandle(
                    position + direction,
                    Quaternion.identity,
                    0.1f,
                    Vector3.one,
                    Handles.DotHandleCap
                );

            if (EditorGUI.EndChangeCheck())
            {
                direction = endPoint - position;
                float newAngle = IsometrySystem.IsometricDirectionToAngle(direction);
                property.floatValue = newAngle;
            }

            Handles.DrawLine(position, position + direction);
        }

        private void DrawPropertyField(string propertyName, Color color)
        {
            SerializedProperty property = serializedObject.FindProperty(propertyName);

            EditorGUILayout.BeginHorizontal();

            Rect rect = EditorGUILayout.GetControlRect(
                false,
                GUILayout.Width(EditorGUIUtility.singleLineHeight),
                GUILayout.ExpandWidth(false));
            rect.x = EditorGUI.IndentedRect(rect).x;
            EditorGUI.DrawRect(rect, color);

            EditorGUILayout.PropertyField(property);

            EditorGUILayout.EndHorizontal();
        }

        private void ApplyModifiedProperties()
        {
            serializedObject.ApplyModifiedPropertiesWithDirtyFlag();

            if (_boundObject != null)
            {
                _currentBehaviour.ResetAngles();
                _currentBehaviour.Playable.GetGraph().Evaluate();
            }
        }
    }
}