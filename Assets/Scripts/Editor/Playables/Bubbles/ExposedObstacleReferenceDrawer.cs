﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using DreamTeam.Playables.Bubbles;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Editor.Playables.Bubbles
{
    [CustomPropertyDrawer(typeof(ExposedObstacleReference))]
    public class ExposedObstacleReferenceDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty referenceProperty = GetReferenceProperty(property);
            Color defaultColor = GUI.color;

            GUI.color = referenceProperty.exposedReferenceValue == null ? Color.red : defaultColor;
            EditorGUI.PropertyField(position, referenceProperty, label);
            GUI.color = defaultColor;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(GetReferenceProperty(property), label);
        }

        private SerializedProperty GetReferenceProperty(SerializedProperty property)
        {
            return property.FindPropertyRelative("_reference");
        }
    }
}