#import "GoogleMobileAdsMediationTestSuite/GoogleMobileAdsMediationTestSuite.h"

extern "C"
{
    void GoogleAdsMediationHelpSuite_Show(const char* appId);
}

void GoogleAdsMediationHelpSuite_Show(const char* appId)
{
    UIViewController* controller = UnityGetGLViewController();
    NSString* appIdObjC = [NSString stringWithUTF8String:appId];
    [GoogleMobileAdsMediationTestSuite presentWithAppID:appIdObjC
                                       onViewController:controller
                                               delegate:nil];
}
