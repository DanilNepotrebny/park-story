﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace DreamTeam.Plugins.GoogleAdsMediationHelpSuite
{
    public static class Suite
    {
        public static bool IsSuiteAvailable()
        {
            #if !UNITY_EDITOR && UNITY_IOS
            {
                return true;
            }
            #else
            {
                return false;
            }
            #endif // !UNITY_EDITOR && UNITY_IOS
        }

        public static void Show(string appID)
        {
            #if !UNITY_EDITOR && UNITY_IOS
            {
                if (IsSuiteAvailable())
                {
                    GoogleAdsMediationHelpSuite_Show(appID);
                }
            }
            #endif // !UNITY_EDITOR && UNITY_IOS
        }
        
        #if !UNITY_EDITOR && UNITY_IOS

        [DllImport("__Internal")]
        private static extern void GoogleAdsMediationHelpSuite_Show(string appID);

        #endif // !UNITY_EDITOR && UNITY_IOS
    }
}
