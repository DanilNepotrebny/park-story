#import "UICKeyChainStore.h"

typedef void (*ErrorDelegatePtr)(const char*);

extern "C"
{
    char* KeyChain_GetString(const char* key, const char* service, const char* accessGroup, ErrorDelegatePtr errorDelegate);
    void KeyChain_SetString(const char* key, const char* value, const char* service, const char* accessGroup, ErrorDelegatePtr errorDelegate);
    void KeyChain_RemoveKey(const char* key, const char* service, const char* accessGroup, ErrorDelegatePtr errorDelegate);
}

NSString* ToNSString(const char* str)
{
    if (str == NULL)
    {
        return nil;
    }
    
    return [NSString stringWithCString:str encoding:NSUTF8StringEncoding];
}

char* FromNSString(NSString* nsStr)
{
    if (nsStr == nil)
    {
        return NULL;
    }

    const char* str = [nsStr UTF8String];

    char* res = (char*)malloc(strlen(str) + 1);
    strcpy(res, str);

    return res;
}

void LogError(ErrorDelegatePtr errorDelegate, NSError* error)
{
    if (error != nil)
    {
        errorDelegate(FromNSString(error.localizedDescription));
    }
}

char* KeyChain_GetString(const char* key, const char* service, const char* accessGroup, ErrorDelegatePtr errorDelegate)
{
    NSError* error;
    NSString* result = [UICKeyChainStore stringForKey:ToNSString(key) service:ToNSString(service) accessGroup:ToNSString(accessGroup) error:&error];

    LogError(errorDelegate, error);

    return FromNSString(result);
}

void KeyChain_SetString(const char* key, const char* value, const char* service, const char* accessGroup, ErrorDelegatePtr errorDelegate)
{
    NSError* error;

    [UICKeyChainStore setString:ToNSString(value) forKey:ToNSString(key) service:ToNSString(service) accessGroup:ToNSString(accessGroup) error:&error];

    LogError(errorDelegate, error);
}

void KeyChain_RemoveKey(const char* key, const char* service, const char* accessGroup, ErrorDelegatePtr errorDelegate)
{
    NSError* error;

    [UICKeyChainStore removeItemForKey:ToNSString(key) service:ToNSString(service) accessGroup:ToNSString(accessGroup) error:&error];

    LogError(errorDelegate, error);
}
