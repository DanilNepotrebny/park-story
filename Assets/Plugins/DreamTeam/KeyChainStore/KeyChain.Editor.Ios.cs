﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR && UNITY_IOS

using System;
using UnityEditor;
using System.Collections.Generic;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

namespace DreamTeam.Plugins.KeyChainStore
{
    public partial class KeyChain
    {
        [PostProcessBuild]
        private static void PostBuildActions(BuildTarget buildTarget, string path)
        {
            if (buildTarget != BuildTarget.iOS)
            {
                return;
            }

            string[] guids = AssetDatabase.FindAssets("t:" + nameof(KeyChain));
            if (guids.Length == 0)
            {
                return;
            }

            var accessGroups = new List<string>();

            foreach (string guid in guids)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guid);
                var keyChain = AssetDatabase.LoadAssetAtPath<KeyChain>(assetPath);
                if (keyChain == null)
                {
                    throw new InvalidOperationException("Can't prepare keychain. Unexpected asset at " + assetPath);
                }

                string group = keyChain._sharedAccessGroup;

                if (!IsValidAccessGroup(group))
                {
                    throw new InvalidOperationException("Can't prepare shared keychain: access group has no Team Id prefix at " + assetPath);
                }

                accessGroups.Add(group);
            }

            if (accessGroups.Count == 0)
            {
                return;
            }

            string bundleId = PlayerSettings.applicationIdentifier;
            string productName = bundleId.Substring(bundleId.LastIndexOf(".") + 1);
            string projectName = "Unity-iPhone";

            string projPath = path + $"/{projectName}.xcodeproj/project.pbxproj";
            var projCapability = new ProjectCapabilityManager(projPath, $"{projectName}/{productName}.entitlements", projectName);

            projCapability.AddKeychainSharing(accessGroups.ToArray());

            projCapability.WriteToFile();
        }
    }
}

#endif