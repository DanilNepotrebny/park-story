﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;

namespace DreamTeam.Plugins.KeyChainStore
{
    [CreateAssetMenu]
    public partial class KeyChain : ScriptableObject
    {
        [SerializeField] private string _sharedAccessGroup;

        public string GetString(string key)
        {
            return GetStringImpl(key);
        }

        public void SetString(string key, string value)
        {
            SetStringImpl(key, value);
        }

        public void RemoveKey(string key)
        {
            RemoveKeyImpl(key);
        }

        #if !UNITY_EDITOR && !UNITY_IOS

        private string GetStringImpl(string key)
        {
            throw new PlatformNotSupportedException();
        }

        private void SetStringImpl(string key, string value)
        {
            throw new PlatformNotSupportedException();
        }

        private void RemoveKeyImpl(string key)
        {
            throw new PlatformNotSupportedException();
        }

        #endif
    }
}
