﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if UNITY_EDITOR

using System;
using UnityEditor;
using UnityEngine;

namespace DreamTeam.Plugins.KeyChainStore
{
    public partial class KeyChain
    {
        private const string _keyPrefix = "keychain";

        protected void OnValidate()
        {
            if (!IsValidAccessGroup(_sharedAccessGroup))
            {
                _sharedAccessGroup = GetTeamIdPrefix() + _sharedAccessGroup;
            }
        }

        private string GetStringImpl(string key)
        {
            key = GetPrefsKey(key);

            if (IsShared())
            {
                return EditorPrefs.HasKey(key) ? EditorPrefs.GetString(key) : null;
            }

            return PlayerPrefs.HasKey(key) ? PlayerPrefs.GetString(key) : null;
        }

        private void SetStringImpl(string key, string value)
        {
            key = GetPrefsKey(key);

            if (IsShared())
            {
                EditorPrefs.SetString(key, value);
            }
            else
            {
                PlayerPrefs.SetString(key, value);
            }
        }

        private void RemoveKeyImpl(string key)
        {
            key = GetPrefsKey(key);

            if (IsShared())
            {
                EditorPrefs.DeleteKey(key);
            }
            else
            {
                PlayerPrefs.DeleteKey(key);
            }
        }

        private string GetPrefsKey(string key)
        {
            string prefix = IsShared() ? _sharedAccessGroup : _keyPrefix;
            return $"{prefix}.{key}";
        }

        private bool IsShared()
        {
            return !string.IsNullOrEmpty(_sharedAccessGroup);
        }

        private static bool IsValidAccessGroup(string group)
        {
            if (string.IsNullOrEmpty(group))
            {
                return true;
            }

            return group.StartsWith(GetTeamIdPrefix());
        }

        private static string GetTeamIdPrefix()
        {
            string teamId = PlayerSettings.iOS.appleDeveloperTeamID;
            if (string.IsNullOrEmpty(teamId))
            {
                throw new InvalidOperationException("There is no Team Id in player settings.");
            }

            return teamId + ".";
        }
    }
}

#endif