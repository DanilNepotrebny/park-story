﻿// Copyright © Dream Team Studio - All Rights Reserved.
// Unauthorized copying of this file via any medium is strictly prohibited.
// Proprietary and confidential.

#if !UNITY_EDITOR && UNITY_IOS

using AOT;
using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace DreamTeam.Plugins.KeyChainStore
{
    public partial class KeyChain
    {
        private string GetStringImpl(string key)
        {
            return KeyChain_GetString(key, null, GetAccessGroup(), GetErrorDelegate());
        }

        private void SetStringImpl(string key, string value)
        {
            KeyChain_SetString(key, value, null, GetAccessGroup(), GetErrorDelegate());
        }

        private void RemoveKeyImpl(string key)
        {
            KeyChain_RemoveKey(key, null, GetAccessGroup(), GetErrorDelegate());
        }

        private static IntPtr GetErrorDelegate()
        {
            return Marshal.GetFunctionPointerForDelegate((ErrorDelegate)LogError);
        }

        private string GetAccessGroup()
        {
            return string.IsNullOrEmpty(_sharedAccessGroup) ? null : _sharedAccessGroup;
        }

        [MonoPInvokeCallback(typeof(ErrorDelegate))]
        private static void LogError(string message)
        {
            Debug.LogError("KeyChain error: " + message);
        }

        [DllImport("__Internal")]
        private static extern string KeyChain_GetString(string key, string service, string accessGroup, IntPtr errorDelegate);

        [DllImport("__Internal")]
        private static extern void KeyChain_SetString(string key, string value, string service, string accessGroup, IntPtr errorDelegate);

        [DllImport("__Internal")]
        private static extern void KeyChain_RemoveKey(string key, string service, string accessGroup, IntPtr errorDelegate);

        #region Inner types

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate void ErrorDelegate(string message);

        #endregion
    }
}

#endif