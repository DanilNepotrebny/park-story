﻿using System.Collections.Generic;

#if USE_ADMOB
using GoogleMobileAds.Api;
using GoogleMobileAds.Api.Mediation.AdColony;
using GoogleMobileAds.Api.Mediation.AppLovin;
using GoogleMobileAds.Api.Mediation.Chartboost;
using GoogleMobileAds.Api.Mediation.UnityAds;
using GoogleMobileAds.Api.Mediation.Vungle;

namespace GleyMobileAds
{
	public static class CustomAdmobAdRequestExtension
	{
	    public static AdRequest.Builder UpdateMediationExtras(this AdRequest.Builder builder, string consent)
	    {
	        builder.AddAddColonyMediationExtras(consent);
	        builder.AddApplovinMediationExtras(consent);
	        builder.AddChartboostMediationExtras(consent);
	        builder.AddUnityMediationExtras(consent);
	        builder.AddVungleMediationExtras(consent);
	        return builder;
	    }

	    public static AdRequest.Builder AddAddColonyMediationExtras(this AdRequest.Builder builder, string consent)
        {
            AdColonyMediationExtras adcolonyExtras = new AdColonyMediationExtras();
            adcolonyExtras.SetGDPRRequired(true);
            adcolonyExtras.SetGDPRConsentString(consent);
            adcolonyExtras.SetShowPrePopup(false);
            adcolonyExtras.SetShowPostPopup(false);
            
            #if !PRODUCTION
            {
                adcolonyExtras.SetTestMode(true);
            }
            #endif // !PRODUCTION

            builder.AddMediationExtras(adcolonyExtras);
            return builder;
        }

        public static AdRequest.Builder AddApplovinMediationExtras(this AdRequest.Builder builder, string consent)
        {
            GoogleMobileAds.Api.Mediation.AppLovin.AppLovin.SetHasUserConsent(consent == "1");
            return builder;
        }

        public static AdRequest.Builder AddChartboostMediationExtras(this AdRequest.Builder builder, string consent)
        {
            Chartboost.RestrictDataCollection(consent == "0");
            return builder;
        }

        public static AdRequest.Builder AddUnityMediationExtras(this AdRequest.Builder builder, string consent)
        {
            UnityAds.SetGDPRConsentMetaData(consent == "1");
            return builder;
        }

        public static AdRequest.Builder  AddVungleMediationExtras(this AdRequest.Builder builder, string consent)
        {
            VungleInterstitialMediationExtras vungleExtras = new VungleInterstitialMediationExtras();
            AdvertiserSettings vungleProviderSettings = Advertisements.Instance.adSettings.advertiserSettings.Find(
                    settings => settings.advertiser == SupportedAdvertisers.Vungle
                );
            PlatformSettings vunglePlatformSettings = vungleProviderSettings.platformSettings.Find(
                    settings => settings.platform == SupportedPlatforms.iOS
                );
            
            List<string> vunglePlacements = new List<string>();
            if (vunglePlatformSettings.hasBanner && !string.IsNullOrEmpty(vunglePlatformSettings.idBanner.id))
            {
                vunglePlacements.Add(vunglePlatformSettings.idBanner.id);
            }

            if (vunglePlatformSettings.hasInterstitial && !string.IsNullOrEmpty(vunglePlatformSettings.idInterstitial.id))
            {
                vunglePlacements.Add(vunglePlatformSettings.idInterstitial.id);
            }

            if (vunglePlatformSettings.hasRewarded && !string.IsNullOrEmpty(vunglePlatformSettings.idRewarded.id))
            {
                vunglePlacements.Add(vunglePlatformSettings.idRewarded.id);
            }
            
            GoogleMobileAds.Api.Mediation.Vungle.Vungle.UpdateConsentStatus(consent == "1" ? VungleConsent.ACCEPTED : VungleConsent.DENIED);
            vungleExtras.SetAllPlacements(vunglePlacements.ToArray());
            
            builder.AddMediationExtras(vungleExtras);
            return builder;
        }
	}
}

#endif // USE_ADMOB
