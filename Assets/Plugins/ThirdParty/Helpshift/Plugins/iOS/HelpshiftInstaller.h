
// Use Helpshift installer when you have custom AppController (instead of HsUnityAppController). In this case
// remove HsUnityAppController from XCode project 

#import <Foundation/Foundation.h>

@interface HelpshiftUnityInstaller : NSObject

+ (BOOL) installDefault;

@end
