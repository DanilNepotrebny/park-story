
#import "HelpshiftAll.h"

@implementation HelpshiftUnityInstaller

+ (BOOL) validateInstallConfig:(NSString *)configuration {
    return (configuration != nil && [configuration isKindOfClass:[NSString class]] && configuration.length > 0 &&
            [configuration stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0);
}

+ (NSDictionary*) readDefaultConfig {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"HelpshiftInstallConfig" ofType:@"json"];

    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
        NSError *error =  nil;
        NSMutableDictionary *installConfig = [[NSJSONSerialization JSONObjectWithData:[myJSON dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error] mutableCopy];

        if (installConfig == nil) {
            if (error != nil) {
                NSLog(@"Error occurred in loading install config %@", error.localizedDescription);
            }
            return nil;
        }

        NSString *apiKey = [installConfig objectForKey:@"__hs__apiKey"];
        NSString *appId = [installConfig objectForKey:@"__hs__appId"];
        NSString *domainName = [installConfig objectForKey:@"__hs__domainName"];

        if (![self validateInstallConfig:apiKey] ||
            ![self validateInstallConfig:appId] ||
            ![self validateInstallConfig:domainName]) {
            return nil;
        }
        return installConfig;
    }

    return nil;
}

+ (NSMutableDictionary*) dictionaryByRemovingNullValues:(NSDictionary*)inDict {
    NSMutableDictionary *output = [[NSMutableDictionary alloc] init];
    if (inDict) {
        for (NSString *key in inDict) {
            id value = [inDict objectForKey:key];
            if (value && ![value isKindOfClass:[NSNull class]]) {
                [output setObject:value forKey:key];
            }
        }
    }
    return output;
}

+ (void) initializeHelpshiftWithDefaultConfig:(NSDictionary*)config {
    NSMutableDictionary *configDictionary = [self dictionaryByRemovingNullValues:config];
    NSString *apiKey = [config objectForKey:@"__hs__apiKey"];
    NSString *appId = [config objectForKey:@"__hs__appId"];
    NSString *domainName = [config objectForKey:@"__hs__domainName"];

    [configDictionary removeObjectsForKeys:@[@"__hs__apiKey", @"__hs__appId", @"__hs__domainName"]];


    [HelpshiftCore initializeWithProvider:[HelpshiftAll sharedInstance]];
    [HelpshiftCore installForApiKey:apiKey domainName:domainName appID:appId withOptions:configDictionary];
}

+ (BOOL) installDefault {
    NSDictionary *installConfig = [self readDefaultConfig];
    if (installConfig != nil) {
        [self initializeHelpshiftWithDefaultConfig:installConfig];
        return YES;
    }
    return NO;
}

@end
