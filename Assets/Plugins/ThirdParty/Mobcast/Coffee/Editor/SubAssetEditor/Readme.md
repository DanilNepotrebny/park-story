SubAssetEditor
===

Editor for SubAsset in project.  
'SubAsset' is an asset that is stored in the same file as the main asset.  
It is good to organize multiple assets, to manage many assets.  
For example, you can integrate Animation assets and AnimatorController asset into one file.

![image](https://user-images.githubusercontent.com/12690315/30265492-32755304-9717-11e7-8bca-f7a472a56be8.png)



## Requirement

* Unity 5.3+ *(included Unity 2017.x)*
* No other SDK are required



## Usage

1. Download [SubAssetEditor.unitypackage](https://github.com/mob-sakai/SubAssetEditor/master/SubAssetEditor.unitypackage) and install to your project.
1. From the menu, click `Coffee > Sub Asset Editor`.
1. Select asset in project window.
1. Enjoy!


## Release Notes

### ver.0.2.0

* Fixed: Infinity loop on Windows
* Fixed: GUI on Unity 2017+

### ver.0.1.0

* Feature: Add sub assets
    * Referencing objects with replacing
    * D&D objects  
![image](https://user-images.githubusercontent.com/12690315/30268506-e47a2a30-9720-11e7-9805-ea3c8fc3de80.png)
* Feature: Edit sub assets
    * Check to be referenced by main asset  
![image](https://user-images.githubusercontent.com/12690315/30268752-afe0ac6c-9721-11e7-8076-3a9bb9c10330.png)
    * Delete
    * Rename  
![image](https://user-images.githubusercontent.com/12690315/30268492-da73c122-9720-11e7-97fc-6731011825e3.png)
    * Export as new asset in project  
![image](https://user-images.githubusercontent.com/12690315/30268554-1659a17a-9721-11e7-80c2-6f8e400037f0.png)



## License
MIT



## Author
[mob-sakai](https://github.com/mob-sakai)
