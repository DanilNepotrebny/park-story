﻿#if UNITY_EDITOR || (!UNITY_IOS && !UNITY_ANDROID && !UNITY_WSA && !UNITY_METRO) || ((UNITY_WSA || UNITY_METRO) && ENABLE_IL2CPP)

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace UTNotifications
{
    public class ManagerImpl : Manager
    {
        private const string _enabledOptionName = "_UT_NOTIFICATIONS_ENABLED";
        private const string _badgeCountOptionName = "_UT_NOTIFICATIONS_BADGE_COUNT";

        public override bool Initialize(bool willHandleReceivedNotifications, int startId, bool incrementalId)
        {
            Initialized = true;
            SetNotificationsEnabled(UnityEngine.PlayerPrefs.GetInt(_enabledOptionName, 1) != 0);
            return true;
        }

        public override void PostLocalNotification(string title, string text, int id, IDictionary<string, string> userData, string notificationProfile, int badgeNumber, ICollection<Button> buttons)
        {
            CheckInitialized();
            Debug.Log($"UT Notification posted!\nTitle: {title}\nMessage: {text}");
        }

        public override void ScheduleNotification(int triggerInSeconds, string title, string text, int id, IDictionary<string, string> userData, string notificationProfile, int badgeNumber, ICollection<Button> buttons)
        {
            CheckInitialized();
            Debug.Log($"UT Notification scheduled!\nTitle: {title}\nMessage: {text}\n Trigger in seconds:{triggerInSeconds}");
        }

        public override void ScheduleNotificationRepeating(int firstTriggerInSeconds, int intervalSeconds, string title, string text, int id, IDictionary<string, string> userData, string notificationProfile, int badgeNumber, ICollection<Button> buttons)
        {
            CheckInitialized();
            Debug.Log($"UT Repeating notification scheduled!\nTitle: {title}\nMessage: {text}\n Trigger in seconds:{firstTriggerInSeconds}\nInterval {intervalSeconds}");
        }

        public override bool NotificationsEnabled()
        {
            return UnityEngine.PlayerPrefs.GetInt(_enabledOptionName, 1) != 0;
        }

        public override bool NotificationsAllowed()
        {
            return true;
        }

        public override void SetNotificationsEnabled(bool enabled)
        {
            UnityEngine.PlayerPrefs.SetInt(_enabledOptionName, enabled ? 1 : 0);
        }

        public override bool PushNotificationsEnabled()
        {
            NotSupported();
            return false;
        }

        public override bool SetPushNotificationsEnabled(bool enable)
        {
            NotSupported();
            return false;
        }

        public override void CancelNotification(int id)
        {
            CheckInitialized();
        }

        public override void HideNotification(int id)
        {
            CheckInitialized();
        }

        public override void HideAllNotifications()
        {
            CheckInitialized();
        }

        public override void CancelAllNotifications()
        {
            CheckInitialized();
        }

        public override int GetBadge()
        {
            CheckInitialized();
            return UnityEngine.PlayerPrefs.GetInt(_badgeCountOptionName, 0);
        }

        public override void SetBadge(int bandgeNumber)
        {
            CheckInitialized();
            UnityEngine.PlayerPrefs.SetInt(_badgeCountOptionName, enabled ? 1 : 0);
        }
    }
}
#endif