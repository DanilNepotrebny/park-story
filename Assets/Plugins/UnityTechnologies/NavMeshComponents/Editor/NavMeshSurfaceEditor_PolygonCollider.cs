using System.Linq;
using UnityEngine;

namespace UnityEditor.AI
{
    partial class NavMeshSurfaceEditor
    {
        private const string _dataName = "__NavMeshData";

        private static void PreparePolygonColliders()
        {
            var polygonColliders = FindObjectsOfType<PolygonCollider2D>();
            foreach (PolygonCollider2D c in polygonColliders)
            {
                CreateMeshData(c);
            }
        }

        private static void CleanPolygonColliders()
        {
            var polygonColliders = FindObjectsOfType<PolygonCollider2D>();
            foreach (PolygonCollider2D c in polygonColliders)
            {
                DestroyMeshData(c);
            }
        }

        private static void CreateMeshData(PolygonCollider2D polygonCollider)
        {
            var dataObject = new GameObject(_dataName);

            Transform dataTransform = dataObject.transform;
            dataTransform.SetParent(polygonCollider.transform, false);
            dataTransform.localPosition = polygonCollider.offset;

            var meshCollider = dataObject.AddComponent<MeshCollider>();
            var mesh = new Mesh();

            mesh.vertices = polygonCollider.points
                .Select(v => (Vector3)v)
                .ToArray();

            var triangulator = new Triangulator(polygonCollider.points);
            mesh.triangles = triangulator.Triangulate();

            meshCollider.sharedMesh = mesh;

            // Re-enable collider to make it visible to NavMesh baking
            meshCollider.enabled = false;
            meshCollider.enabled = true;
        }

        private static void DestroyMeshData(PolygonCollider2D polygonCollider)
        {
            Transform meshTransform = polygonCollider.transform.Find(_dataName);
            if (meshTransform != null)
            {
                DestroyImmediate(meshTransform.gameObject);
            }
            else
            {
                Debug.LogError("Couldn't find mesh to clean up after NavMesh baking.", polygonCollider);
            }
        }
    }
}
